<?php

class Reff extends CI_Model {

    public function __construct() {
        parent::__construct();
		$this->zona_waktu();
    }

	public function path(){

		return "/cbtptk/__statics/upload/";
	}

	public function owner(){

		return "Kementerian Agama ";
	}

	public function set(){
		$ajaran = $this->db->query("select * from pengaturan where status='1'")->row();
		return $ajaran->id;
      
	}
	public function nama(){
		$ajaran = $this->db->query("select * from pengaturan where status='1'")->row();
		return $ajaran->nama;
      
	}

	public function zona_waktu(){
		
		
			
		return date_default_timezone_set("Asia/Jakarta");	
		
	}
	
	
	public function ajaran(){
		
	
		return 2020;
	}
	public function ajaran2($tmmadrasah_id){
		
		$ajaran = $this->db->query("select ajaran from tm_madrasah where id='".$tmmadrasah_id."'")->row();
		return $ajaran->ajaran;
	}
	
	public function semester(){
		
		
		return 1;
	}
	
	public function semester2(){
		$arr = array("1"=>"Ganjil","2"=>"Genap");
		$ajaran = $this->db->query("select semester from tm_madrasah where id='".$_SESSION['tmmadrasah_id']."'")->row();
		return $arr[$ajaran->semester];
	}
	
	public function kelas(){
		
		
		return  $this->db->query("select id,nama from tm_kelas  order by urutan asc")->result();
		
		 
	}
	public function kelasshow($p){
		
		
			 $kelas = array("1"=>"Kelompok A",
						   "2"=>"Kelompok B",
						   "3"=>"Kelas I",
						   "4"=>"Kelas II",
						   "5"=>"Kelas III",
						   "6"=>"Kelas IV",
						   "7"=>"Kelas V",
						   "8"=>"Kelas VI",
						   "9"=>"Kelas VII",
						   "10"=>"Kelas VIII",
						   "11"=>"Kelas IX",
						   "12"=>"Kelas X",
						   "13"=>"Kelas XI",
						   "14"=>"Kelas XII",
			);
			return $kelas[$p];
	}
	public function jabataneksekutif(){
		
		return array("1"=>"Kepala Madrasah","2"=>"Wakil Kepala Madrasah","3"=>"Pengawas Madrasah");
	}
	
	public function jabataneksekutifshow($p){
		
		$show= array("1"=>"Kepala Madrasah","2"=>"Wakil Kepala Madrasah","3"=>"Pengawas Madrasah");
		return $show[$p];
	}
	public function notifikasi($keterangan,$status,$trkelas_id,$user,$link){
		$this->zona_waktu();
		$this->db->set("keterangan",$keterangan);
		$this->db->set("status",$status);
		$this->db->set("trkelas_id",$trkelas_id);
		$this->db->set("user",$user);
		$this->db->set("link",$link);
		$this->db->set("tanggal",date("Y-m-d H:i:s"));
		$this->db->insert("notifikasi");
		
	}
	public function nsm($tmyayasan_id){
		
		$madrasah = $this->db->get_where("tm_madrasah",array("tmyayasan_id"=>$tmyayasan_id))->row();
		
		$jenjangkode = $this->kodejenjang($madrasah->jenjang);
		$statuskode  ="2";
		$provkode    = $this->get_kondisi(array("id"=>$madrasah->kota_id),"kota","kodensm");
		$urutan      = $this->urutan_nsm($madrasah->kota_id);
		
		return $jenjangkode.$statuskode.$provkode.$urutan;
		
	}
	public function urutan_nsm($kota_id){
		 
		   $dpendaftar = $this->db->query("SELECT COUNT(id) as jml FROM madrasah WHERE kota_id='".$kota_id."'")->row();
		   
		   $pendaftar = $dpendaftar->jml; 
		   
		         if(strlen($pendaftar)==1){
					 return  "000".$pendaftar;
				 }else if(strlen($pendaftar)==2){
					 
					 return "00".$pendaftar;
				 }else if(strlen($pendaftar)==3){
					 
					 return "0".$pendaftar;
				 }else if(strlen($pendaftar)==4){
					 
					 return $pendaftar;
				 }
		
	}
	
	
	public function surat_yayasan($yayasan){
		
		$yayasan = $this->db->get_where("tm_yayasan",array("id"=>$yayasan))->row();
		
		$jenjang = $this->get_kondisi(array("tmyayasan_id"=>$yayasan->id),"tm_madrasah","jenjang");
		$tanggal = $this->formattanggaldb($this->get_kondisi(array("tmyayasan_id"=>$yayasan->id),"tm_madrasah","tgl_terima"));
		
		$nomor   = explode("-",$yayasan->noreg);
		$no      = $nomor[1];
		$jenjang = $this->jenjangsingkat($jenjang);
		$tgl   = $this->no_tgl($tanggal);
		return  $no."/".$nomor[0]."-".$jenjang."/".$tgl;
		
		
		
		
	}
	
	public function nomor_sk($provinsi_id,$tanggal){
		$this->db->select("count(id) as jml");
		$this->db->from("tr_piagam");
		$this->db->where("provinsi_id",$provinsi_id);
		
		$data  = $this->db->get()->row();
		
		$nomor = $this->no_maxp($data->jml+1);
		
		$p2    ="";
		$tgl   = explode("-",$tanggal);
		    
			
		return $nomor." TAHUN ".$tgl[2];
			
			
	}
	
	
	public function nomor_piagam($provinsi_id,$tanggal){
		$this->db->select("count(id) as jml");
		$this->db->from("tr_piagam");
		$this->db->where("provinsi_id",$provinsi_id);
		
		$data  = $this->db->get()->row();
		
		$nomor = $this->no_maxp($data->jml+1);
		
		$p2    ="";
		$tgl   = explode("-",$tanggal);
		    
			
		return $nomor."/IPM/".$tgl[2];
			
			
	}
	
	
	public function nomor_surat($jenis,$param,$kolom,$jenjang,$tanggal){
		$this->db->select("count(id) as jml");
		$this->db->from("tr_surat");
		$this->db->where($kolom,$param);
		$this->db->where("jenis",$jenis);
		$data  = $this->db->get()->row();
		
		$nomor = $this->no_max($data->jml+1);
		
		$p2    ="";
		$tgl   = $this->no_tgl($tanggal);
		    if($kolom=="provinsi_id"){
				
				$kode = $this->get_kondisi(array("id"=>$param),"provinsi","kode");
				$p2   = "KW.".$kode;
			}else if($kolom=="kota_id"){
				
				$kode = $this->get_kondisi(array("id"=>$param),"kota","kode");
				$p2   = "KK.".$kode;
			} 
			
		return $nomor."/".$p2."/".strtoupper($jenis)."-".$jenjang."/".$tgl;
			
			
	}
	
	public function no_tgl($tgl){
		
		$pecah = explode("-",$tgl);
		return $pecah[0]."/".$this->bulanromawi($pecah[1])."/".$pecah[2];
		
	}
	public function no_max($pendaftar){
		
		         if(strlen($pendaftar)==1){
					 return  "B-0000".$pendaftar;
				 }else if(strlen($pendaftar)==2){
					 
					 return "B-000".$pendaftar;
				 }else if(strlen($pendaftar)==3){
					 
					 return "B-00".$pendaftar;
				 }else if(strlen($pendaftar)==4){
					 
					 return "B-0".$pendaftar;
				 }else if(strlen($pendaftar)==5){
					 
					 $nopen = "B-".$pendaftar;
				 }
		
	}
	
	public function no_maxp($pendaftar){
		
		         if(strlen($pendaftar)==1){
					 return  "0000".$pendaftar;
				 }else if(strlen($pendaftar)==2){
					 
					 return "000".$pendaftar;
				 }else if(strlen($pendaftar)==3){
					 
					 return "00".$pendaftar;
				 }else if(strlen($pendaftar)==4){
					 
					 return "0".$pendaftar;
				 }else if(strlen($pendaftar)==5){
					 
					 $nopen = "".$pendaftar;
				 }
		
	}
	public function penyelenggara(){
		
		return array("1"=>"Org.Keagamaan","2"=>"Yayasan","3"=>"Perorangan");

	}
	public function penyelenggaranama($p){
		
		$array=  array("1"=>"Org.Keagamaan","2"=>"Yayasan","3"=>"Perorangan");
		return $array[$p];

	}
	public function statusdetail(){
	 return $status = array("1"=>"Anda belum menyelesaikan pendaftaran",
															"2"=>"Penerimaan Dokumen oleh Kankemenag Kabupaten/kota",
															"3"=>"Verifikasi Dokumen oleh Kankemenag Kabupaten/kota",
															"4"=>"Verifikasi Lapangan oleh Kankemenag Kabupaten/kota",
															"5"=>"Pengiriman Dokumen ke Kanwil Provinsi",
															"6"=>"Penerimaan Dokumen di Kanwil Provinsi",
															"7"=>"Rapat Pertimbangan Oleh Kanwil Provinsi ",
															"8"=>"Operasional/Penerbitan SK Oleh Kanwil Provinsi"
															
															);
													
	}
	
	public function status(){
	 return $status = array("0"=>"Belum Selesai",
															"1"=>"Verifikasi Dokumen",
															"2"=>"Verifikasi Lapangan ",															
															"3"=>"Rekomendasi Ke Kanwil",
															"4"=>"Penerimaan Dokumen Oleh Kanwil",
															"5"=>"Rapat Pertimbangan Oleh Kanwil ",
															"6"=>"Operasional"
															
															);
													
	}
	
	public function statusnama($status){
	                                         $array = array("0"=>"Belum Selesai",
															"1"=>"Verifikasi Dokumen",
															"2"=>"Verifikasi Lapangan ",
															"3"=>"Rekomendasi Ke Kanwil",
															"4"=>"Penerimaan Dokumen(Kanwil)",
															"5"=>"Rapat Pertimbangan(Kanwil) ",
															"6"=>"Operasional (Kanwil)"
															
															);
					return $array[$status];
													
	}
	
	public function statuswarna($status){
	                                         $array = array("0"=>"bg-red",
															"1"=>"bg-cyan",
															"2"=>"bg-yellow ",
															"3"=>"bg-blue",
															"4"=>"bg-indigo",
															"5"=>"bg-orange ",
															"6"=>"bg-black"
															
															);
					return $array[$status];
													
	}
	public function organisasi(){
		
		return array("1"=>"Al-Khariat","2"=>"Al-Washilah","3"=>"DDI",
		"4"=>"GUPPI",
		"5"=>"Hidayatullah",
		"6"=>"Mathlaul Anwar",
		"7"=>"Muhammadiyah",
		"8"=>"NU",
		"9"=>"NW",
		"10"=>"Persis",
		"11"=>"Perti",
		"12"=>"PUI",
		"13"=>"Mandiri",
		"14"=>"Lainnya"
		);

	}
	
	public function organisasinama($p){
		
		$array= array("1"=>"Al-Khariat","2"=>"Al-Washilah","3"=>"DDI",
		"4"=>"GUPPI",
		"5"=>"Hidayatullah",
		"6"=>"Mathlaul Anwar",
		"7"=>"Muhammadiyah",
		"8"=>"NU",
		"9"=>"NW",
		"10"=>"Persis",
		"11"=>"Perti",
		"12"=>"PUI",
		"13"=>"Mandiri",
		"14"=>"Lainnya"
		);
   return $array[$p];
	}
	
	public function geografi(){
		
		return array("1"=>"Pantai",
		"2"=>"Dataran Rendah",
		"3"=>"Dataran Tinggi");
		

	}
	
	public function potensi(){
		
		return array("1"=>"Pertanian",
		"2"=>"Perikanan",
		"3"=>"Perkebunan/Kehutanan",
		"4"=>"Pertambangan",
		"5"=>"Industri",
		"6"=>"Pariwisata",
		"7"=>"Perdagangan",
		"8"=>"Lainnya");
		

	}
	
	public function wilayah(){
		
		return array("1"=>"Perkotaan",
		"2"=>"Transisi (Kota-Desa)",
		"3"=>"Pedesaan",
		"4"=>"Terpencil",
		"5"=>"Perbatasan Negara Lain");
		

	}
	public function jarak(){
		
		return array("1"=>" < 1KM",
		"2"=>" 1-10 KM",
		"3"=>" 11-20 KM",
		"4"=>" 21-40 KM",
		"5"=>" > 40 KM");
		

	}
	
		public function jaraknama($p){
		
		$array= array("1"=>" < 1KM",
		"2"=>" 1-10 KM",
		"3"=>" 11-20 KM",
		"4"=>" 21-40 KM",
		"5"=>" > 40 KM");
		
         return $array[$p];
	}
     public function jenjang(){
		
		
		return array("1"=>"Raudatul Athfal","2"=>"Madrasah Ibtidayah","3"=>"Madrasah Tsanawiyah","4"=>"Madrasah Aliyah","5"=>"Madrasah Aliyah Kejuruan");
	}
	public function namajenjang($jenjang){
		
		
		$array = array("1"=>"Raudatul Athfal","2"=>"Madrasah Ibtidayah","3"=>"Madrasah Tsanawiyah","4"=>"Madrasah Aliyah","5"=>"Madrasah Aliyah Kejuruan");
	
	    return $array[$jenjang];
	}
	
	 public function jenjangsingkat($jenjang){
		
		
		$array= array("1"=>"RA","2"=>"MI","3"=>"MTs","4"=>"MA","5"=>"MAK");
		
	    return $array[$jenjang];
	}
	 public function jenjangsingkat2($jenjang,$status){
		
		if($status==1){
		$array= array("1"=>"RAN","2"=>"MIN","3"=>"MTsN","4"=>"MAN","5"=>"MAKN");
		}else{
		$array= array("1"=>"RA","2"=>"MI","3"=>"MTs","4"=>"MA","5"=>"MAK");
		}
		
	    return $array[$jenjang];
	}
	
	public function kodejenjang($jenjang){
		
		
		$array= array("1"=>"101","2"=>"111","3"=>"121","4"=>"131");
		
	    return $array[$jenjang];
	}
	public function countm($provinsi,$jenjang,$status){
		
		$data = $this->query("select count(id) as de from tm_madrasah where jenjang='".$jenjang."' and status='1' and provinsi='".$provinsi."' and kategori='".$status."'")->row();
		return $data->de;
	}
	
	public function countmj($jenjang,$status){
		
		$data = $this->query("select count(id) as de from tm_madrasah where jenjang='".$jenjang."' and status='1'  and kategori='".$status."'")->row();
		return $data->de;
	}
    public function log($keterangan,$status,$tmmadrasah_id=null,$link){
		$this->zona_waktu();
		$this->db->set("status",$status);
		$this->db->set("keterangan",$keterangan);
		$this->db->set("link",$link);
		$this->db->set("tanggal",date("Y-m-d H:i:s"));
		$this->db->set("tmmadrasah_id",$tmmadrasah_id);
		$this->db->insert("log");
		
		
	}
	
	
  // Baru
  
	   function hitung_umur($tanggal_lahir) {
		list($year,$month,$day) = explode("-",$tanggal_lahir);
		$year_diff  = date("Y") - $year;
		$month_diff = date("m") - $month;
		$day_diff   = date("d") - $day;
		if ($month_diff < 0) $year_diff--;
			elseif (($month_diff==0) && ($day_diff < 0)) $year_diff--;
		return $year_diff;
	}
	
	public function selisihumur($tgllahir){
		$tanggal = new DateTime($tgllahir);

			
			$today = new DateTime('today');

			
			$y = $today->diff($tanggal)->y;

			
			$m = $today->diff($tanggal)->m;

			
			$d = $today->diff($tanggal)->d;
			return  " " . $y . " tahun " . $m . " bulan " . $d . " hari";

	}


    function getImgPost($post)
    {
    	$pecah=explode("<img", $post);
    	if(!isset($pecah[1]))
    	{
    		return base_url()."__statics/img/logo.png";
    	}
    	$get1=$pecah[1];
    	$pecah2=explode("src=",$get1);
    	$get2=$pecah2[1];
    	$pecah3=explode("/>", $get2);
    	$pecah4=explode(" ", $pecah3[0]);
    	$return=str_replace("'","",$pecah4[0]);
    	return str_replace('"','',$return);


    }
   function gofield($tbl,$field,$where)
  {
  	$this->db->select($field);
  	$this->db->where($where);
  	$return=$this->db->query("select * from ".$tbl." where ".$where." ")->row();
  return isset($return->$field)?($return->$field):"";
  }
 
 
	 public function indukmenu(){
		
		return $this->db->query("select a.id as id_menu, a.nama as menu, a.link,a.icon,b.* FROM tm_menu a INNER JOIN hak_akses b ON b.tmmenu_id=a.id  where b.tmgrup_id='".$_SESSION['grup']."' and parent_id is NULL order  by a.urutan asc");
	}
	
	public function submenu($tmmenu_id){
		
		return $this->db->query("select a.nama as menu, a.link,a.icon,b.* FROM tm_menu a INNER JOIN hak_akses b ON b.tmmenu_id=a.id  where b.tmgrup_id='".$_SESSION['grup']."' and parent_id='".$tmmenu_id."' order  by a.urutan asc");
	}
  
  public function notif($keterangan,$link,$status=null){
		
		if($status !=null){
			
			//$this->db->set("status",$status);
		}
		
		$this->db->set("link",$link);
		$this->db->set("keterangan",$keterangan);
		$this->db->set("tanggal",date("Y-m-d H:i:s"));
		
		
	    $this->db->insert("tm_notif");
		
		
	}
	
	

	
	
  public function query($query){
	  
	  		return $this->db->query($query);

  }
  
	
   public function formatuang($jumlah){
	   
	   return "Rp. ". number_format($jumlah, 0 , ',' , '.');
   }
   public function formatuang2($jumlah){
	   
	   return  number_format($jumlah, 0 , ',' , '.');
   }
  
	public function get_where($table,$where){
		
		$this->db->where($where);
		return $this->db->get($table)->row();
		
	}
	public function get_wherearray($table,$where){
		
		$this->db->where($where);
		return $this->db->get($table)->result();
		
	}
	
	public function get_where2($table,$where){
		
		$this->db->where($where);
		return $this->db->get($table);
		
	}
	
	
	public function hapus($table,$where){
		
		$this->db->where($where);
		$this->db->delete($table);
		
	}
	
	public function id($table){
		
		$this->db->select("MAX(id) as id");
		$this->db->from($table);
		
		
		$sql = $this->db->get()->row();
		
		return ($sql->id);
		
	}
	
	public function id2($table){
		
		$this->db->select("MAX(id) as id");
		$this->db->from($table);

		$sql = $this->db->get()->row();
		
		return ($sql->id+1);
		
	}
	public function id3($table,$kolom,$where){
		
		$this->db->select("MAX($kolom) as id");
		$this->db->where($where);
		$this->db->from($table);

		$sql = $this->db->get()->row();
		
		return ($sql->id+1);
		
	}
	
	public function get2($table){
		
		return $this->db->get($table);
	}
	
	public function get($param,$where){
	
		
		//$this->db->select("*");
		//$this->db->from("public.tm_jenjang a");
		$condition = "1=1";
		if($where !=null){
			
			$condition .=" and ".$where;
			
		}
		$limit="";
		if(!empty($param['limit'])){
			
			$limit =" limit ".$param['limit'];
			
		}
		return $this->db->query("select * from ".$param['table']." where ".$condition." order by ".$param['order']." ".$param['by']." ".$limit."");
		//$this->db->order_by("".$param['order']."","".$param['by']."");
		//return ($this->db->get()->num_rows() >0) ? $this->db->get() :"";
		
	}
	
	public function getlimit($param,$where){
	
		$condition = "1=1";
		if($where !=null){
			
			$condition .="and ".$where;
			
		}
		return $this->db->query("select * from ".$param['table']." where ".$condition." order by ".$param['order']." ".$param['by']." limit ".$param['limit']."");
		
		
	}
	
	
	
	public function update($table,$data,$where){
		
		
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	
	public function insert($table,$data){
		
		 
		    
			$this->db->insert($table,$data);
		
	}
	

   
	
	 
	
 
	public function ip_lokal(){
		
		$ip_public   		         = $_SERVER['REMOTE_ADDR'];
		$hostname  			         = gethostbyaddr($ip_public);
		return $ip_private  		 = gethostbyname(trim($hostname));
	}
	
	
	public function hostname(){
		
		$ip_public   		 = $_SERVER['REMOTE_ADDR'];
		return $hostname  			 = gethostbyaddr($ip_public);
		$ip_private  		 = gethostbyname(trim($hostname));
	}
	
		public function pengunjung($browser){
		 if(!$this->session->userdata("tamubaiksnpdb")){
			  $pengunjung = array('tamubaiksnpdb'        => $this->get_id(30));
			  $this->session->set_userdata($pengunjung);
		 }
		  $ip 		= $_SESSION['tamubaiksnpdb'];
		
		 if($this->cek_pengunjung($ip,$browser) ==0):
		
		   $this->db->set("ip",$ip);
		   $this->db->set("browser",$browser);
		  
		   $this->db->set("tanggal",date("Y-m-d"));
		   
		   $this->db->insert("tm_pengunjung");
		
		  
		 
		 
		 endif;
		
	}
		public function cek_pengunjung($ip,$browser){
		
		$this->db->where("ip",$ip);
		$this->db->where("browser",$browser);
		$this->db->where("tanggal",date("Y-m-d"));
		
		return $this->db->get("tm_pengunjung")->num_rows();
		
		
	}
	   public function count_pengunjung($where,$param){
		
		$q = $this->db->query("select count(id) as jumlah from tm_pengunjung where $where(tanggal)='".$param."' and YEAR(tanggal)='".date("Y")."'")->row();
		  if(count($q) >0){
			  
			  return $q->jumlah;
		  }
		
	}
	
	
	public function get_pekerjaan(){
		
		$sql = array("PNS","TNI/POLRI","WIRASWASTA","BURUH","LAINNYA");
		return $sql;
	
	}
	
	
	public function get_pendidikan(){
		
		$this->db->order_by("id","asc");
		$sql = $this->db->get("tm_pendidikan")->result();
		return $sql;
	
	}
	
		public function get_jabatan(){
		
		$this->db->order_by("id","asc");
		$sql = $this->db->get("tm_jabatan")->result();
		return $sql;
	
	}
	
	public function get_kondisi($where,$table,$kolom){
		
		$this->db->select($kolom);
		$this->db->where($where);
		$sql = $this->db->get($table)->row();
		
		if(count($sql)>0):
		return $sql->$kolom;
		else:
		return "";
		endif;
		
	}
	
	
	
	public function agama(){
		
		
		
		return array("1"=>"Islam","2"=>"Kristen Protestan","3"=>"Katholik","4"=>"Hindu","5"=>"Buddha","6"=>"Kong Hu cu","7"=>"Lainnya");
		
		
		
		
	}
	
	public function gender(){
		
		
		
		return array("L"=>"Laki - Laki","P"=>"Perempuan");
		
		
		
		
	}
	
	public function provinsi(){
		
		return $this->db->query("SELECT * FROM inf_lokasi WHERE lokasi_kabupatenkota='00' order by lokasi_nama asc")->result();
		
	}
	
	public function golongan_darah(){
		
		
		
		return array("A","B","AB","O");
		
		
		
		
	}
	public function statuspernikahan(){
		
		
		
		return array("Belum Menikah","Sudah Menikah","Duda","Janda");
		
		
		
		
	}
	
		public function pendidikan(){
		
		
		
		return array("< SMA/MA ","SMA/MA/Sederajat","Diploma 1 (D1)","Diploma 2 (D2)","Diploma 3 (D3)","Sarjana (S1)","Magister (S2)","Doktor (S3)");
		
		
		
		
	}
	 public function jabatan(){
		
		
		
		return array("Kepala Madrasah","Wakil Kepala Madrasah","Kepala Tata Usaha","Bendahara","Administrasi/TU","Pustakawan","Laboran","Instruktur Ekstrakulikuler","Tenaga Bimbingan dan Konseling","Tenaga Kebersihan","Tenaga Keamanan");
		
		
		
		
	}
	
    public function pekerjaan(){
		
		
		
		return array("Pensiunan","PNS","TNI/POLRI","Anggota DPRD/DPR RI","Honorer","Wiraswasta","Lainnnya");
		
		
		
		
	}
	
	public function statuspegawai(){
		
		
		
		return array("PNS","Honorer");
		
		
		
		
	}
	
	function get_id($length = 5){
		$password = "";
		$possible = "0123456789BCDFGHJKMNPQRSTVWXYZ"; //no vowels

		$i = 0;

		while ($i < $length) {
			$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
			if (!strstr($password, $char)) {
				$password .= $char;
				$i++;
			}
		}
		 $password= $password;
		
		return $password;
	}	
	
	
	function randomangka($length = 5){
		$password = "";
		$possible = "0123456789"; //no vowels

		$i = 0;

		while ($i < $length) {
			$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
			if (!strstr($password, $char)) {
				$password .= $char;
				$i++;
			}
		}
		 $password= $password;
		
		return $password;
	}	
	
	
	
	function Terbilang($x)
	{
		if($x=="-"){ return false; }
	  $abil = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
	  if ($x < 12)
		return " " . $abil[$x];
	  elseif ($x < 20)
		return $this->Terbilang($x - 10) . " Belas";
	  elseif ($x < 100)
		return $this->Terbilang($x / 10) . " Puluh" . $this->Terbilang($x % 10);
	  elseif ($x < 200)
		return " Seratus" . $this->Terbilang($x - 100);
	  elseif ($x < 1000)
		return $this->Terbilang($x / 100) . " Ratus" . $this->Terbilang($x % 100);
	  elseif ($x < 2000)
		return " Seribu" . $this->Terbilang($x - 1000);
	  elseif ($x < 1000000)
		return $this->Terbilang($x / 1000) . " Ribu" . $this->Terbilang($x % 1000);
	  elseif ($x < 1000000000)
		return $this->Terbilang($x / 1000000) . " Juta" . $this->Terbilang($x % 1000000);
	  elseif ($x < 1000000000000)
		return $this->Terbilang($x / 1000000000) . " Milyar " . $this->Terbilang(substr($x,1));
	   
	}
	
	
	
	function formattanggaldb($tanggal){
		
		if($tanggal=="0000-00-00" or $tanggal=="00-00-0000" or $tanggal==""){
			
			return false;
		}
		$pecah = explode("-",$tanggal);
		return $pecah[2]."-".$pecah[1]."-".$pecah[0];
		
		
		
	}
	function formattimestamp($tanggal){
		
		
		$pecah = explode(" ",$tanggal);
		return $this->formattanggalstring($pecah[0])." Pukul ".$pecah[1];
		
		
		
	}
	function formattimestampbulan($tanggal){
		
		
		$pecah = explode(" ",$tanggal);
		return $this->formattanggalstring($pecah[0]);
		
		
		
	}
	
	function bulanajaran(){
		
		return $databulan= array(
					"07"=>"Juli",
					"08"=>"Agustus",
					"09"=>"September",
					"10"=>"Oktober",
					"11"=>"November",
					"12"=>"Desember",
					"01"=>"Januari",
					"02"=>"Februari",
					"03"=>"Maret",
					"04"=>"April",
					"05"=>"Mei",
					"06"=>"Juni");
		
		
	}
	
	function formattanggalstring($tanggal){
		
		if($tanggal=="0000-00-00" or $tanggal=="00-00-0000" or $tanggal==""){
			
			return "-";
			
		}else{
		
		$pecah = explode("-",$tanggal);
		return $pecah[2]." ".$this->bulan($pecah[1])." ".$pecah[0];
		}
		
		
	}
	
   function postlearning($tanggal){
		
		
		
		$pecah   = explode(" ",$tanggal);
		$tanggal = $pecah[0];
		$jam     = $pecah[1];
		
		$pecahtgl  = explode("-",$tanggal);
		return $pecahtgl[2]."-".$this->bulan($pecahtgl[1])."-".$pecahtgl[0]." Pukul ".$jam;
		
		
		
	}
	
	function bulanromawi($param){
		
		$databulan= array("01"=>"I",
					"02"=>"II",
					"03"=>"III",
					"04"=>"IV",
					"05"=>"V",
					"06"=>"VI",
					"07"=>"VII",
					"08"=>"VIII",
					"09"=>"IX",
					"10"=>"X",
					"11"=>"XI",
					"12"=>"XII");
		
		return $databulan[$param];
	}
	
	function arraybulan(){
		
		$databulan= array("01"=>"Januari",
					"02"=>"Februari",
					"03"=>"Maret",
					"04"=>"April",
					"05"=>"Mei",
					"06"=>"Juni",
					"07"=>"Juli",
					"08"=>"Agustus",
					"09"=>"September",
					"10"=>"Oktober",
					"11"=>"November",
					"12"=>"Desember");
		
		return $databulan;
	}
	function bulan($bulan){
		
		$databulan= array("01"=>"Januari",
					"02"=>"Februari",
					"03"=>"Maret",
					"04"=>"April",
					"05"=>"Mei",
					"06"=>"Juni",
					"07"=>"Juli",
					"08"=>"Agustus",
					"09"=>"September",
					"10"=>"Oktober",
					"11"=>"November",
					"12"=>"Desember");
		
		return $databulan[$bulan];
	}
	
	function bulanbalik($bulan){
		
		$databulan= array("Januari"=>"01",
					"Februari"=>"02",
					"Maret"=>"03",
					"April"=>"04",
					"Mei"=>"05",
					"Juni"=>"06",
					"Juli"=>"07",
					"Agustus"=>"08",
					"September"=>"09",
					"Oktober"=>"10",
					"November"=>"11",
					"Desember"=>"12");
		
		return $databulan[$bulan];
	}
	
	function getstatusabsen($tmsiswa_id,$tanggal,$valpel){
		
		$this->db->where("tmsiswa_id",$tmsiswa_id);
		$this->db->where("tanggal",$tanggal);
		$this->db->where("absensi",$valpel);
		
		$query = $this->db->get("tr_absensi");
		
		if($query->num_rows() >0){
			
			 return "<span class='glyphicon glyphicon-ok'></span>";
			
		}else{
			
			return "-";
		}
		
	}
	
	function getstatusabsenstudentprivate($tmsiswa_id,$tanggal,$valpel){
		
		$this->db->where("tmsiswa_id",$tmsiswa_id);
		$this->db->where("tanggal",$tanggal);
		$this->db->where("tmpelajaran_id",$valpel);
		
		$query = $this->db->get("tr_absensi");
		
		if($query->num_rows() >0){
			 $row = $query->row();
			 return $row->absensi;
			 
			
		}else{
			
			return "-";
		}
		
	}
	
	
	public function nilai_kuantitif(){
		
		return array("A"=>"Baik Sekali","B"=>"Baik","C"=>"Cukup","D"=>"Kurang","E"=>"Kurang Sekali");
		
		
	}
	
   public function balik_nilai_kuantitif($nilai){
		
		$arr=  array("A"=>"Baik Sekali","B"=>"Baik","C"=>"Cukup","D"=>"Kurang","E"=>"Kurang Sekali");
		if($nilai!=""){
			return $arr[$nilai];
			
		}
		
		
	}
	
	
	public function session_learning(){
		
		 $session_siswa           = $this->session->userdata("id_learning_siswa");
		 $session_guru            = $this->session->userdata("id_learning");
		// $data['status_learning'] = $this->session->userdata("status_learning");
		return $data['sessionaktif']    = isset($session_siswa) ? $session_siswa  : $session_guru;
		
		
	}
	
	function timeAgo($date,$display = array('Year', 'Month', 'Day', 'Hour', 'Minute', 'Second'), $ago = ''){
			
	$this->zona_waktu();
        $timestamp = strtotime($date);
        $timestamp = (int) $timestamp;
        $current_time = time();
        $diff = $current_time - $timestamp;

        //intervals in seconds
        $intervals = array(
            'year' => 31556926, 'month' => 2629744, 'week' => 604800, 'day' => 86400, 'hour' => 3600, 'minute' => 60
        );

        //now we just find the difference
        if ($diff == 0) {
            return ' Baru Saja ';
        }

        if ($diff < 60) {
            return $diff == 1 ? $diff . ' Detik Yang Lalu ' : $diff . '  Detik Yang Lalu ';
        }

        if ($diff >= 60 && $diff < $intervals['hour']) {
            $diff = floor($diff / $intervals['minute']);
            return $diff == 1 ? $diff . ' Menit Yang Lalu ' : $diff . '  Menit Yang Lalu ';
        }

        if ($diff >= $intervals['hour'] && $diff < $intervals['day']) {
            $diff = floor($diff / $intervals['hour']);
            return $diff == 1 ? $diff . ' Jam Yang Lalu ' : $diff . '  Jam Yang Lalu ';
        }

        if ($diff >= $intervals['day'] && $diff < $intervals['week']) {
            $diff = floor($diff / $intervals['day']);
            return $diff == 1 ? $diff . ' Hari Yang Lalu ' : $diff . '  Hari Yang Lalu ';
        }

        if ($diff >= $intervals['week'] && $diff < $intervals['month']) {
            $diff = floor($diff / $intervals['week']);
            return $diff == 1 ? $diff . ' Minggu Yang Lalu ' : $diff . '  Minggu Yang Lalu ';
        }

        if ($diff >= $intervals['month'] && $diff < $intervals['year']) {
            $diff = floor($diff / $intervals['month']);
            return $diff == 1 ? $diff . ' Bulan Yang Lalu ' : $diff . '  Bulan Yang Lalu';
        }

        if ($diff >= $intervals['year']) {
            $diff = floor($diff / $intervals['year']);
            return $diff == 1 ? $diff . ' Tahun Yang Lalu ' : $diff . '  Tahun Yang Lalu ';
        }
		
	}
	
	function jumlah_hari_bulan(){
		$this->zona_waktu();
		 $kalender = CAL_GREGORIAN;
		 $bulan    = date("m");
		 $tahun    = date("Y");
		 
		 return cal_days_in_month($kalender,$bulan,$tahun);
		
		
		
		
	}
	
	function kategori_pembayaran(){
		
		return array("1"=>"Perhari",
		"2"=>"Perminggu",
		"3"=>"Perbulan",
		"4"=>"Persemester",
		"5"=>"Pertahun",
		"7"=>"Per 2 Tahun",
		"8"=>"Per 3 Tahun",
		"6"=>"Persekali");
		
	}
	
	function kategori_tagihan(){
		
		return array("1"=>"Umum",
		"2"=>"Ekstrakulikuler",
		"3"=>"SPP"
		);
		
	}

	public function get_max_id($id,$table){
		
		$sql = $this->db->query("select max(".$id.") as maxid from ".$table."")->row();
		    
			if(count($sql) >0){
				
				return $sql->maxid;
				
			}
		
		
	}
	public function get_max_id2($id,$table){
		
		$sql = $this->db->query("select max(".$id.") as maxid from ".$table."")->row();
		    
			if(count($sql) >0){
				
				return $sql->maxid+1;
				
			}
		
		
	}
	public function count_siswa(){
		
		return $this->db->query("SELECT 	COUNT(
					CASE
						WHEN id IN (SELECT tmsiswa_id FROM tr_kelas WHERE tmkelas_id IN(34,35,38,39,22,23,24,25,26))
						THEN 0
						ELSE NULL
					END
				) AS 'pyp',
				COUNT(
										CASE
						WHEN id IN (SELECT tmsiswa_id FROM tr_kelas WHERE tmkelas_id IN(27,28,29,30))
						THEN 0
						ELSE NULL
					END
				) AS 'myp',
				COUNT(
											CASE
						WHEN id IN (SELECT tmsiswa_id FROM tr_kelas WHERE tmkelas_id IN(31,32,33))
						THEN 0
						ELSE NULL
					END
				) AS 'sma',
				COUNT(
					CASE
						WHEN id IS NOT NULL
						THEN 0
						ELSE NULL
					END
				) AS 'total' FROM tm_siswa WHERE status_persyaratan='2' and ajaran='".$this->tahun_aktif()."'")->row();
		
	}
	function mobile()
	{
						$useragent=$_SERVER['HTTP_USER_AGENT'];
                       if(preg_match('/(android|bb\d+|meego).+mobile|Android|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
						{ return 2;}else{ return 1; }
                   
	}
	
	
	public function statistikpendaftar($status){
		 $ajaran = $this->Acuan_model->ajaran();
		  if($status==null){
			  
			  $d= $this->db->query("select count(id) as jumlah from tm_siswa where ajaran='".$ajaran."'")->row(); return $d->jumlah;
			  
		  }
		  
		    if($status==2){
			  
					  $d= $this->db->query("select count(id) as jumlah from tm_siswa where ajaran='".$ajaran."' and tmmadrasah_id='2'")->row(); return $d->jumlah;
		  
		  }
		    if($status==3){
			  
				  $d= $this->db->query("select count(id) as jumlah from tm_siswa where ajaran='".$ajaran."' and tmmadrasah_id='3'")->row(); return $d->jumlah;
			  
		  }
		  
		
		
		
	}
	public function sessionhabis(){
		
		return '<br><br><br><br><br><br><br><br><br><body style="background:black;color:white"><center>
                   <div class="four-zero-four-container">
					<div class="error-code">404</div>
					<div class="error-message">Session Anda sudah habis, silahkan kembali login </div>
					<div class="button-place">
						<a href="'.site_url().'login/peserta" class="btn btn-default btn-lg waves-effect">Klik disini Menuju Halaman Login </a>
					</div>
				</div></center></body>';
		
		
	}
	
	public function tgl_sebutan($tgl){
		
   $bulan = array ('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
   $bulan_pendek = array ('Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sep','Okt','Nov','Des');
   $hari = array('Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu');
   $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");

   $ts = strtotime($tgl);
      return $hari[date('w',$ts)].' tanggal '.$this->Terbilang(date('j',$ts)).' bulan '.$bulan[date('n',$ts)-1].' tahun '.$this->Terbilang(date('Y',$ts));
    
		
		
	}
	
	
	
	
	function kode_kelas($id){
		$hitungid = strlen($id);
		$jumlah = (7-$hitungid);
		$kode   = $id.$this->get_id($jumlah);
		return $kode;
	}

   function token($id){
		$hitungid = strlen($id);
		$jumlah = (5-$hitungid);
		$kode   = $id.$this->get_id($jumlah);
		return $kode;
	}


  function predikat($nilai,$trkelas_id){
	    
		 
	  
	     $interval = $this->Reff->get_kondisi(array("id"=>$trkelas_id),"tr_kelas","interval");
	     $kkm      = $this->Reff->get_kondisi(array("id"=>$trkelas_id),"tr_kelas","kkm");
		 
				$intervala = (100 - ($interval * 1))." - ".(100);
															 $intervalb = (100 - ($interval * 2))." - ".($intervala-1);
															 $intervalc = (100 - ($interval * 3))." - ".($intervalb-1);
															 $intervald = " < ". $kkm;									
															 
															
															 
															 
	  
		 if($nilai >= (100 - ($interval * 1)) && $nilai <=100){
			 return "A";
		 }else if($nilai >= (100 - ($interval * 2)) && $nilai <= ($intervala-1)){
			 
			return "B"; 
		 }else if($nilai >= (100 - ($interval * 3)) && $nilai <=($intervalb-1)){
			 
			return "C"; 
		  }else if($nilai  < $kkm ){
			 
			return "D"; 
		 }
	
   }
	
	
	

}
