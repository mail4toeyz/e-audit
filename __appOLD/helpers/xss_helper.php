<?php

 function json($instance, $data = []) {
    return $instance->output
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
  }
  
function isLogin(){
    $ci =& get_instance();
	if(empty($ci->session->userdata("is_login"))){
		$ajax = $ci->input->get_post("ajax",true);		 
		if(!empty($ajax)){
	   
		   echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("login")."'>disini </a></center>";
			  exit();
		 }else{
			redirect(site_url("login"));
		 }
	
		
     }
	

  }

function app(){
	echo "e-Audit";
}
function c($str){
    echo htmlentities($str, ENT_QUOTES, 'UTF-8');
}
function deuleu($str){
    echo htmlentities($str, ENT_QUOTES, 'UTF-8');
}

function xss($str){
	
	
	return htmlspecialchars(strip_tags($str));
}
function jenistugasp($tugas){
	
	 $data = array("1"=>"Tes Tertulis","2"=>"Tes Lisan","3"=>"Penugasan");
			return $data[$tugas];
	
}

function jenistugask($tugas){
	
	 $kelas = array("0"=>"Remedial","1"=>"Ulangan Harian (UH)","2"=>"KUIS","3"=>"Tugas","4"=>"Unjuk Kerja (Praktek)","5"=>"PORTOFOLIO","6"=>"PROYEK");
			
	
}

function jenisketerampilan($tugas){
	
	 $kelas = array("1"=>"Unjuk Kerja / Praktek","2"=>"Proyek","3"=>"Portofolio","4"=>"Produk");
			
	
}


function showketerampilan($tugas){
	
	 $kelas = array("1"=>"Unjuk Kerja / Praktek","2"=>"Proyek","3"=>"Portofolio","4"=>"Produk");
			return $kelas[$tugas];
	
}


function predikat($nilai){
	 if($nilai >= 93 && $nilai <=100){
	     return "A";
     }else if($nilai >= 84 && $nilai <=92){
		 
		return "B"; 
	 }else if($nilai >= 75 && $nilai <=83){
		 
		return "C"; 
	  }else if($nilai  < 75 ){
		 
		return "D"; 
	 }
	
}
function xssArray($str){
	
	
	return $newArray = array_map(function($v){
    return trim(strip_tags($v));
       }, $str);
}


	function timeAgo($date,$display = array('Year', 'Month', 'Day', 'Hour', 'Minute', 'Second'), $ago = ''){
			
	date_default_timezone_set("Asia/Jakarta");
        $timestamp = strtotime($date);
        $timestamp = (int) $timestamp;
        $current_time = time();
        $diff = $current_time - $timestamp;

        //intervals in seconds
        $intervals = array(
            'year' => 31556926, 'month' => 2629744, 'week' => 604800, 'day' => 86400, 'hour' => 3600, 'minute' => 60
        );

        //now we just find the difference
        if ($diff == 0) {
            return ' Baru Saja ';
        }

        if ($diff < 60) {
            return $diff == 1 ? $diff . ' Detik Yang Lalu ' : $diff . '  Detik Yang Lalu ';
        }

        if ($diff >= 60 && $diff < $intervals['hour']) {
            $diff = floor($diff / $intervals['minute']);
            return $diff == 1 ? $diff . ' Menit Yang Lalu ' : $diff . '  Menit Yang Lalu ';
        }

        if ($diff >= $intervals['hour'] && $diff < $intervals['day']) {
            $diff = floor($diff / $intervals['hour']);
            return $diff == 1 ? $diff . ' Jam Yang Lalu ' : $diff . '  Jam Yang Lalu ';
        }

        if ($diff >= $intervals['day'] && $diff < $intervals['week']) {
            $diff = floor($diff / $intervals['day']);
            return $diff == 1 ? $diff . ' Hari Yang Lalu ' : $diff . '  Hari Yang Lalu ';
        }

        if ($diff >= $intervals['week'] && $diff < $intervals['month']) {
            $diff = floor($diff / $intervals['week']);
            return $diff == 1 ? $diff . ' Minggu Yang Lalu ' : $diff . '  Minggu Yang Lalu ';
        }

        if ($diff >= $intervals['month'] && $diff < $intervals['year']) {
            $diff = floor($diff / $intervals['month']);
            return $diff == 1 ? $diff . ' Bulan Yang Lalu ' : $diff . '  Bulan Yang Lalu';
        }

        if ($diff >= $intervals['year']) {
            $diff = floor($diff / $intervals['year']);
            return $diff == 1 ? $diff . ' Tahun Yang Lalu ' : $diff . '  Tahun Yang Lalu ';
        }
		
	}
   function kelas(){
		
		
		return array("10"=>"X (Sepuluh)","11"=>"XI (Sebelas)","12"=>"XII (Dua Belas)");
	}
	 function kelasshow($p){
		
		
		    $kelas = array("1"=>"Kelompok A",
						   "2"=>"Kelompok B",
						   "3"=>"Kelas I",
						   "4"=>"Kelas II",
						   "5"=>"Kelas III",
						   "6"=>"Kelas IV",
						   "7"=>"Kelas V",
						   "8"=>"Kelas VI",
						   "9"=>"Kelas VII",
						   "10"=>"Kelas VIII",
						   "11"=>"Kelas IX",
						   "12"=>"Kelas X",
						   "13"=>"Kelas XI",
						   "14"=>"Kelas XII",
			);
			return $kelas[$p];
	}
	
	
	
	function formattanggaldb($tanggal){
		
		if($tanggal=="0000-00-00" or $tanggal=="00-00-0000" or $tanggal==""){
			
			return false;
		}
		$pecah = explode("-",$tanggal);
		return $pecah[2]."-".$pecah[1]."-".$pecah[0];
		
		
		
	}
	
	function formattanggaldb2($tanggal){
		
		if($tanggal=="0000-00-00 00:00:00" or $tanggal==""){
			
			return false;
		}
		$pecah0 = explode(" ",$tanggal);
		$pecah = explode("-",$pecah0[0]);
		return $pecah[2]."-".$pecah[1]."-".$pecah[0]." ".$pecah0[1];
		
		
		
	}
	
	
	function formattimestamp($tanggal){
		
		
		$pecah = explode(" ",$tanggal);
		return formattanggalstring($pecah[0])." Pukul ".$pecah[1];
		
		
		
	}
	function formattimestampbulan($tanggal){
		
		
		$pecah = explode(" ",$tanggal);
		return formattanggalstring($pecah[0]);
		
		
		
	}
	
	function formattanggalstring($tanggal){
		
		if($tanggal=="0000-00-00" or $tanggal=="00-00-0000" or $tanggal==""){
			
			return "-";
			
		}else{
		
		$pecah = explode("-",$tanggal);
		return $pecah[2]." ".bulan($pecah[1])." ".$pecah[0];
		}
		
		
	}
	
	
	function arraybulan(){
		
		$databulan= array("01"=>"Januari",
					"02"=>"Februari",
					"03"=>"Maret",
					"04"=>"April",
					"05"=>"Mei",
					"06"=>"Juni",
					"07"=>"Juli",
					"08"=>"Agustus",
					"09"=>"September",
					"10"=>"Oktober",
					"11"=>"November",
					"12"=>"Desember");
		
		return $databulan;
	}
	function bulan($bulan){
		
		$databulan= array("01"=>"Januari",
					"02"=>"Februari",
					"03"=>"Maret",
					"04"=>"April",
					"05"=>"Mei",
					"06"=>"Juni",
					"07"=>"Juli",
					"08"=>"Agustus",
					"09"=>"September",
					"10"=>"Oktober",
					"11"=>"November",
					"12"=>"Desember");
		
		return $databulan[$bulan];
	}
	
	function bulanajaran(){
		
		return $databulan= array(
					"07"=>"Juli",
					"08"=>"Agustus",
					"09"=>"September",
					"10"=>"Oktober",
					"11"=>"November",
					"12"=>"Desember",
					"01"=>"Januari",
					"02"=>"Februari",
					"03"=>"Maret",
					"04"=>"April",
					"05"=>"Mei",
					"06"=>"Juni");
		
		
	}
	
	function bulanajaranshow($bulan){
		
		 $databulan= array(
					"07"=>"Juli",
					"08"=>"Agustus",
					"09"=>"September",
					"10"=>"Oktober",
					"11"=>"November",
					"12"=>"Desember",
					"01"=>"Januari",
					"02"=>"Februari",
					"03"=>"Maret",
					"04"=>"April",
					"05"=>"Mei",
					"06"=>"Juni");
		
		return $databulan[$bulan];
	}
	
	
	function bulanbalik($bulan){
		
		$databulan= array("Januari"=>"01",
					"Februari"=>"02",
					"Maret"=>"03",
					"April"=>"04",
					"Mei"=>"05",
					"Juni"=>"06",
					"Juli"=>"07",
					"Agustus"=>"08",
					"September"=>"09",
					"Oktober"=>"10",
					"November"=>"11",
					"Desember"=>"12");
		
		return $databulan[$bulan];
	}
	
	 function tgl_sebutan($tgl){
		
   $bulan = array ('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
   $bulan_pendek = array ('Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sep','Okt','Nov','Des');
   $hari = array('Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu');
   $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");

   $ts = strtotime($tgl);
      return $hari[date('w',$ts)].', '.formattanggalstring($tgl);
    
		
		
	}
	
	 function cekurl($url){

         $curl = curl_init($url); 
  
// Use curl_setopt() to set an option for cURL transfer 
				curl_setopt($curl, CURLOPT_NOBODY, true); 
				  
				// Use curl_exec() to perform cURL session 
				$result = curl_exec($curl); 
				  
				if ($result !== false) { 
					  
					// Use curl_getinfo() to get information 
					// regarding a specific transfer 
					$statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);  
					
					if ($statusCode == 404) { 
						return "0";
					} 
					else { 
						return "1";
					} 
				} 
				else { 
					return "0";
				} 
     }
	 
	 
	 function semester($param){
		 
		 $arr = array("1"=>"Ganjil","2"=>"Genap");
		 return $arr[$param];
		 
	 }
	
