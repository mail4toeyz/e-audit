<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publik extends CI_Controller {

    public function __construct()
      {
        parent::__construct();
		
            $this->load->helper('exportpdf_helper'); 
			
			$this->load->model('M_web',"m");
		
	  }
	
	
	public function js_nosurat(){
		
		echo $this->Reff->nomor_surat($_POST['jenis'],$_POST['param'],$_POST['kolom'],$_POST['jenjang'],$_POST['tgl']);
		
	}
	
	public function kota(){
		
		$id = $this->input->get_post("id",TRUE);
		 $kota = $this->Reff->query("select id,nama from kota where provinsi_id='".$id."' order by nama asc")->result();
		                                                     ?><option value="">- Pilih Kabupaten/ Kota -</option><?php
															    foreach($kota as $row){
																	?><option value="<?php echo $row->id; ?>"><?php echo strtoupper($row->nama); ?></option><?php 
																}
																
																?><option value="0">LAINNYA  </option><?php
	}
	
    public function kecamatan(){
		
		$id = $this->input->get_post("id",TRUE);
		 $kota = $this->Reff->query("select id,nama from kecamatan where kota_id='".$id."' order by nama asc")->result();
															      ?><option value="">== Pilih Kecamatan ==</option><?php
																foreach($kota as $row){
																	?><option value="<?php echo $row->id; ?>"><?php echo strtoupper($row->nama); ?></option><?php 
																}
																
																	?><option value="0">LAINNYA </option><?php
	}
	
	  public function desa(){
		
		$id = $this->input->get_post("id",TRUE);
		 $kota = $this->Reff->query("select id,nama from desa where kecamatan_id='".$id."' order by nama asc")->result();
															      ?><option value="">== Pilih Kelurahan/Desa ==</option><?php
																foreach($kota as $row){
																	?><option value="<?php echo $row->id; ?>"><?php echo strtoupper($row->nama); ?></option><?php 
																}
																
																	?><option value="0">Lainnya </option><?php
	}
	
	
	 public function rombel(){
		 $ajaran = $this->Reff->ajaran();
		$id = $this->input->get_post("id",TRUE);
		 $kota = $this->Reff->query("select id,nama from tr_ruangkelas where trkelas_id='".$id."' and tmmadrasah_id='".$_SESSION['tmmadrasah_id']."' and ajaran='".$ajaran."' order by nama asc")->result();
		                                                     ?><option value="">- Pilih Rombel -</option><?php
															    foreach($kota as $row){
																	?><option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option><?php 
																}
																
																
	}
	
	public function generatekota(){
		
		 $kota = $this->Reff->query("select * from propinsis  order by nama asc")->result();
		                                              
															    foreach($kota as $row){
																	$this->db->set("username","PROV-".$row->kode);
																	$this->db->set("password","123456");
																	$this->db->where("id",$row->id);
																	$this->db->update("propinsis");
																	
																	
																}
	}
	
	public function kd(){
		
		$trkelas_id = $this->input->get_post("trkelas_id",TRUE);
		$kode = $this->input->get_post("kode",TRUE);
		 $data = $this->Reff->query("select id,kd,kode,urutan from tr_kompetensidasar where trkelas_id='".$trkelas_id."' and kode='".$kode."' order by urutan asc")->result();
		                                                   
																if(count($data) ==0){
																	
																	?>1<?php
																	
																}else{

															?><option value="">== Pilih Kompetensi Dasar ==</option><?php
															    foreach($data as $row){
																	?><option value="<?php echo $row->id; ?>"> <?php echo ($row->kd); ?></option><?php 
																}
																}
																
	}
													
	public function jenistugas(){
		
		
		$kode = $this->input->get_post("kode",TRUE);
		 if($kode==3){
			 $data = array("1"=>"Ulangan Harian (UH)","2"=>"KUIS","3"=>"Tugas","0"=>"Remedial");
		 }else if($kode==4){
			 
			 $data = array("4"=>"Unjuk Kerja (Praktek)","5"=>"PORTOFOLIO","6"=>"PROYEK","0"=>"Remedial");
		 }
		                                                   
															

															?><option value="">== Pilih Penilaian ==</option><?php
															    foreach($data as $i=>$row){
																	?><option value="<?php echo $i; ?>"> <?php echo ($row); ?></option><?php 
																}
																
																
	}
			
	
	public function sessionhabis()
		{  
			
			 $this->load->view('page/page_session');
	

		}

	public function satkerKanwil(){

		$data = $this->db->get("provinsi")->result();
		 foreach($data as $row){
			 $this->db->set("kategori",1);
			 $this->db->set("provinsi_id",$row->kode);
			 $this->db->set("kode",$row->kode);
			 $this->db->set("nama","KANWIL ".$row->nama);
			 $this->db->insert("satker");
		 }
	}

	public function satkerKota(){

		$data = $this->db->get("kota")->result();
		 foreach($data as $row){
			 $this->db->set("kategori",2);
			 $this->db->set("provinsi_id",substr($row->kode,0,2));
			 $this->db->set("kota_id",$row->kode);
			 $this->db->set("kode",$row->kode);
			 $this->db->set("nama","KANKEMENAG ".$row->nama);
			 $this->db->insert("satker");
		 }
	}

	public function satkermadrasah(){

		$data = $this->db->query("SELECT * from madrasah where jenjang IN(3,4)")->result();
		
		
		 foreach($data as $row){
			if($row->status==1){
				$jenjang = array("3"=>"MTsN","4"=>"MAN");
			}else if($row->status==2){
				$jenjang = array("3"=>"MTsS","4"=>"MAS");
			}
			 $provinsi = $this->db->get_where("provinsi",array("id"=>$row->provinsi_id))->row();
			 $kota = $this->db->get_where("kota",array("id"=>$row->kota_id))->row();

			 $this->db->set("kategori",3);
			 $this->db->set("provinsi_id",$provinsi->kode);
			 $this->db->set("kota_id",$kota->kode);
			 
			 $this->db->set("kode",$row->nsm);
			 $this->db->set("nama",$jenjang[$row->jenjang]." ".$row->nama);
			 $this->db->insert("satker");
		 }
	}
}
