
</style>
 <?php 
   $uri = $this->uri->segment(1);
   $uri2 = $this->uri->segment(2);
   $uri4 = ($this->uri->segment(4));
  
   
     
  ?>

 <div class="sidebar-wrapper" style="color:black">
        <ul class="nav">
          <li class="nav-item <?php echo ($uri=="admin") ? "active" :""; ?> ">
            <a class="nav-link menuajax" style="color:black" href="<?php echo site_url("admin"); ?>">
              <i class="fa fa-dashboard"></i>
              <p>Dashboard  </p>
            </a>
          </li>

       <?php 
        if($_SESSION['status']=="admin"){
        ?>
		
		   <li class="nav-item  ">
                        <a href="javascript:void(0);" style="color:black" class="menu-toggle nav-link">
                            <i class="fa fa-desktop"></i>
                            <span>Referensi Data    </span> 
							 
                        </a>
						
							<ul class="ml-menu" style="display:none">
							 
                <li>
                                <a href="<?php echo site_url("datakategori"); ?>" style="color:black" class="nav-link menuajax" title="Data Jurusan"><i class="fa fa-angle-right"></i>Data Jurusan   </a>
                </li>

                <li>
                                <a href="<?php echo site_url("jadwal"); ?>" style="color:black" class="nav-link menuajax" title="Jadwal Ujian "><i class="fa fa-angle-right"></i>Jadwal Ujian   </a>
                </li>
                <li>
                                <a href="<?php echo site_url("sesi"); ?>" style="color:black" class="nav-link menuajax" title="Master Sesi"><i class="fa fa-angle-right"></i>Sesi   </a>
                </li>
              
              </ul>
							 
							 
                            
         </li>

         <?php 
        }
        ?>
   
        

          <li class="nav-item <?php echo ($uri=="datasiswa" && $uri2=="") ? "active":""; ?> ">
            <a class="nav-link menuajax" style="color:black" href="<?php echo site_url("datasiswa"); ?>">
            <i class="fa fa-users" aria-hidden="true"></i>
              <p>Data Peserta Tes </p>
            </a>
          </li>                   
        
        <?php 
        if($_SESSION['status']=="admin"){
        ?>
          <li class="nav-item <?php echo ($uri=="banksoal") ? "active":""; ?> ">
                        <a href="javascript:void(0);" style="color:black" class="menu-toggle nav-link">
                            <i class="fa fa-database"></i>
                            <span>Bank Soal Ujian     </span> 
                            
                        </a>
						
							<ul class="ml-menu" style="display:none">
							   <li>
                 <a href="<?php echo site_url("banksoalsimulasi/buatsoal"); ?>" style="color:black" class="nav-link menuajax" title="Simulasi"><i class="fa fa-angle-right"></i> Soal Simulasi   </a>
                 </li>
							 
                 <li>
                 <a href="<?php echo site_url("banksoal/buatsoal"); ?>" style="color:black" class="nav-link menuajax" title="Simulasi"><i class="fa fa-angle-right"></i> Soal Ujian   </a>
                 </li>
							  
               
							  

								
							  </ul>
                            
         </li>

         <?php 
        }
        ?>
   


         <li class="nav-item <?php echo ($uri=="pelaksanaan" && $uri2=="statistik") ? "active":""; ?>">
            <a class="nav-link menuajax" style="color:black" href="<?php echo site_url("pelaksanaan/statistik"); ?>">
              <i class="fa fa-laptop"></i>
              <p>Statistik Pelaksanaan   </p>
            </a>
          </li>
        
		   
          
          <li class="nav-item <?php echo ($uri=="datasiswa" && $uri2=="hasil") ? "active":""; ?> ">
                        <a href="javascript:void(0);" style="color:black" class="menu-toggle nav-link">
                            <i class="fa fa-retweet"></i>
                            <span>  Simulasi  CBT      </span> 
                            
                        </a>
						
							<ul class="ml-menu" style="display:none">
               <?php 
                 $ujian = $this->db->query("select * from tm_kategori")->result();

                  foreach($ujian as $rj){
                  ?>
                  <li>
                 <a href="<?php echo site_url("datasiswa/ujian?id=0&kategori=".$rj->id.""); ?>" style="color:black" class="nav-link menuajax" title="Pengawasan Simulasi "><i class="fa fa-angle-right"></i> <?php echo $rj->nama; ?> </a>
                 </li>
                 <?php 

                  }
                ?>
							  
							
								
							  </ul>
                            
         </li>




                       <li class="nav-item <?php echo ($uri=="datasiswa" && $uri2=="hasil") ? "active":""; ?> ">
                        <a href="javascript:void(0);" style="color:black" class="menu-toggle nav-link">
                            <i class="fa fa-edit"></i>
                            <span> Pelaksanaan CBT   </span> 
                            
                        </a>
						

						
                <ul class="ml-menu" style="display:none">
               <?php 
                 $ujian = $this->db->query("select * from tm_kategori")->result();

                  foreach($ujian as $rj){
                  ?>
                  <li>
                 <a href="<?php echo site_url("datasiswa/ujian?id=1&kategori=".$rj->id.""); ?>" style="color:black" class="nav-link menuajax" title="Pengawasan Ujian "><i class="fa fa-angle-right"></i> <?php echo $rj->nama; ?> </a>
                 </li>
                 <?php 

                  }
                ?>
							  
							
								
							  </ul>
                            
         </li>

         <li class="nav-item <?php echo ($uri=="datasiswa" && $uri2=="hasilujian") ? "active":""; ?> ">
                        <a href="javascript:void(0);" style="color:black" class="menu-toggle nav-link">
                            <i class="fa fa-file"></i>
                            <span> Hasil  Seleksi   </span> 
                            
                        </a>
						

						
                <ul class="ml-menu" style="display:none">
               <?php 
                 $ujian = $this->db->query("select * from tm_kategori")->result();

                  foreach($ujian as $rj){
                  ?>
                  <li>
                 <a href="<?php echo site_url("datasiswa/hasilujian?kategori=".$rj->id.""); ?>" style="color:black" class="nav-link menuajax" title="Hasil Tes Seleksi  "><i class="fa fa-angle-right"></i> <?php echo $rj->nama; ?> </a>
                 </li>
                 <?php 

                  }
                ?>
							  
							
								
							  </ul>
                            
         </li>


      
        
          <?php 
        if($_SESSION['status']=="admin"){
        ?>
<!-- 
<li class="nav-item <?php echo ($uri=="datasiswa" && $uri2=="rekap") ? "active":""; ?> ">
                        <a href="javascript:void(0);" style="color:black" class="menu-toggle nav-link">
                            <i class="fa fa-graduation-cap"></i>
                            <span> Kelulusan Fasilitator   </span> 
                            
                        </a>
						

						
                <ul class="ml-menu" style="display:none">
               <?php 
                 $ujian = $this->db->query("select * from tm_kategori")->result();

                  foreach($ujian as $rj){
                  ?>
                   <li>
                 <a href="<?php echo site_url("datasiswa/kelulusan?kategori=".$rj->id.""); ?>" style="color:black" class="nav-link menuajax" title="Hasil Tes Seleksi  "><i class="fa fa-angle-right"></i> <?php echo $rj->nama; ?> </a>
                 </li>
                 <?php 

                  }
                ?>
							  
							
								
							  </ul>
                            
         </li>
-->

        

            <li class="nav-item <?php echo ($uri2=="pengaturan") ? "active":""; ?> ">
              <a class="nav-link menuajax" style="color:black" href="<?php echo site_url("pelaksanaan/pengaturan"); ?>">
              <i class="fa fa-check-square-o" aria-hidden="true"></i>

              <p>Aktivasi Peserta  </p>
            </a>
           </li>  
          
         <?php 
        }
        ?>
		   
		  
             <li class="nav-item <?php echo ($uri=="schoolmadrasah" && $uri2=="aktifitas") ? "active":""; ?> ">
              <a class="nav-link menuajax" style="color:black" href="<?php echo site_url("schoolmadrasah/aktifitas"); ?>">
              <i class="fa fa-clock-o"></i>
              <p>Monitoring Aktifitas </p>
            </a>
           </li> 

         


           
		  
          <li class="nav-item ">
            <a class="nav-link" style="color:black" href="javascript:void(0)" id="keluar">
              <i class="fa fa-sign-out"></i>
              <p>Logout </p>
            </a>
          </li>
         
          
         
        </ul>
      </div>
	  
