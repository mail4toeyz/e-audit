<link href="<?php echo base_url(); ?>__statics/js/gal/css/lightgallery.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>__statics/js/gal/js/lightgallery-all.min.js"></script>
<style>
.badge {
  display:inline-block;
  padding:.25em .4em;
  font-size:75%;
  font-weight:700;
  line-height:1;
  text-align:center;
  white-space:nowrap;
  vertical-align:baseline;
  border-radius:.25rem
}

.badge:empty{display:none} //Empty Badge

.btn .badge{position:relative;top:-1px} //Badges with Button

.badge-pill{padding-right:.6em;padding-left:.6em;border-radius:10rem} //pills

.badge-primary{color:#fff;background-color:#007bff}
.badge-primary[href]:focus,.badge-primary[href]:hover{color:#fff;text-decoration:none;background-color:#0062cc}
.badge-secondary{color:#fff;background-color:#6c757d}
.badge-secondary[href]:focus,.badge-secondary[href]:hover{color:#fff;text-decoration:none;background-color:#545b62}
.badge-success{color:#fff;background-color:#28a745}
.badge-success[href]:focus,.badge-success[href]:hover{color:#fff;text-decoration:none;background-color:#1e7e34}
.badge-info{color:#fff;background-color:#17a2b8}.badge-info[href]:focus,
.badge-info[href]:hover{color:#fff;text-decoration:none;background-color:#117a8b}
.badge-warning{color:#212529;background-color:#ffc107}.badge-warning[href]:focus,
.badge-warning[href]:hover{color:#212529;text-decoration:none;background-color:#d39e00}
.badge-danger{color:#fff;background-color:#dc3545}
.badge-danger[href]:focus,.badge-danger[href]:hover{color:#fff;text-decoration:none;background-color:#bd2130}
.badge-light{color:#212529;background-color:#f8f9fa}.badge-light[href]:focus,
.badge-light[href]:hover{color:#212529;text-decoration:none;background-color:#dae0e5}
.badge-dark{color:#fff;background-color:#343a40}
.badge-dark[href]:focus,.badge-dark[href]:hover{color:#fff;text-decoration:none;background-color:#1d2124}


</style>
<div class="col-md-12">
		
				       <div class="card">
						<div class="card-header card-header-success">
						  
						  <p class="card-category">Dibawah ini adalah Peserta yang harus Anda lakukan wawancara  </p>
						</div>
					  
						 <div class="card-body">
						
						 		
					   <div class="row" >
						 
					        
					   <div class="col-md-2">
								
								<select class="form-control onchange"    id="provinsi" urlnya="<?php echo site_url('publik/kota'); ?>" target="kota">
													 <option value=""> Filter Provinsi </option>
													 <?php 
												  $kelas = $this->db->get("provinsi")->result();
													 foreach($kelas as $i=>$r){
														 
														?><option value="<?php echo $r->id; ?>" > <?php echo ($r->nama); ?> <?php echo ($r->status==1) ? " (*)":""; ?></option><?php  
														 
													 }
												?>
								</select>
				</div>
			
				<div class="col-md-2">
					
					<select class="form-control kota"    id="kota" >
										 <option value=""> Filter Kabkota </option>
										 <?php 
												  $kelas = $this->db->get("kota")->result();
													 foreach($kelas as $i=>$r){
														 
														?><option value="<?php echo $r->id; ?>" > <?php echo ($r->nama); ?></option><?php  
														 
													 }
												?>
										
					</select>
			  </div>

              <div class="col-md-2">
								
								<select class="form-control"    id="unsur" >
													 <option value=""> Unsur Peserta </option>
													 <?php 
															  
																	if($kategori==1){
																	$dposisi = array("1"=>"Penulis Modul PKB Guru  ","2"=>"Tim pengembang PKB Guru tingkat Nasional","3"=>"Widyaiswara Pusdiklat dan Balai diklat Keagamaan ","4"=>"Tim Review Modul PKB Guru Madrasah ","5"=>"Instruktur Nasional PKB Guru Madrasah hasil piloting
																	program PKB Guru. ","6"=>"Dosen/Praktisi pendidikan");
																	}else{
																		
																		$dposisi = array("1"=>"Guru  ","2"=>"Kepala RA","3"=>"Pengawas Madrasah ","4"=>"Dosen ","5"=>"Praktisi Pendidikan ","6"=>"Widyaiswara","7"=>"Kepala Madrasah");
																	}

																 foreach($dposisi as $i=>$r){
																	 
																	?><option value="<?php echo $i; ?>" > <?php echo ($r); ?></option><?php  
																	 
																 }
															?>
													
								</select>
			        	  </div>
						
						  <div class="col-md-2">
								<input type="hidden" id="kategori" value="<?php echo $kategori; ?>">
								<select class="form-control"    id="literasi" >
													 <option value=""> Literasi  </option>
													 <?php 
															  if($kategori==2){
																$literasi = $this->db->query("select * from literasi where jenjang IN(1,2,3,0)")->result();

																
															  }else if($kategori==3){

																$literasi = $this->db->query("select * from literasi where jenjang IN(5,6,7,0)")->result();
															  }else if($kategori==1){

                                                                $literasi = $this->db->query("select * from literasi where jenjang IN(9,10,11)")->result();
                                                              }
															  foreach($literasi as $r){
																  
																 ?><option value="<?php echo $r->id; ?>" > <?php echo $this->Reff->get_kondisi(array("id"=>$r->jenjang),"jenjang","nama"); ?> -  <?php echo ($r->nama); ?></option><?php  
																  
															  }
															?>
													
								</select>
			        	  </div>

						  	
						  <div class="col-md-2">
						  <select  id="keputusan" class="form-control">
						       <option value=""> Status Seleksi </option> 
							   
							     
							   <option value="1" > Lulus </option>  
							   <option value="2">Tidak Lulus </option>  
							    </select>
			        	  </div>
						  <div class="col-md-2">
						  <select  id="urutkan" class="form-control">
						       <option value=""> Urutkan </option> 
							   
							     
							   <option value="nilai" selected> Nilai</option>  
							   <option value="Provinsi "> Provinsi </option>  
							   <option value="Prioritas "> Prioritas </option>  

							    </select>
			        	  </div>

						        		
								<div class="col-md-4">
								
									<div class="input-group">
									<input type="text" class="form-control" id="keyword"  placeholder="Cari  Nomor Tes atau Nama disini..">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" id="cariatuh" type="button">CARI</button>
									</div>
									</div>
								</div>
					   </div>
					   
					   <br>


							<div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
                                        <tr>
                                            
                                            
                                            <th rowspan="2">NO</th>                                            
                                            <th rowspan="2">NO PESERTA</th>                                            
                                            <th rowspan="2">NAMA PESERTA</th>                                            
                                            <th rowspan="2">JK</th>                                            
                                            <th rowspan="2">NO HP</th>                                            
                                            <th rowspan="2">EMAIL</th>                                            
                                            <th rowspan="2">STATUS</th>                                            
                                            <th rowspan="2">UNSUR </th>                                            
                                            <th rowspan="2">LBP</th>                                            
                                            <th rowspan="2">LITERASI</th>                                            
                                            <th rowspan="2">PROVINSI </th>                                            
                                            <th rowspan="2">KABUPATEN/KOTA </th>                                            
										
                                            
                                            <th colspan="4"><center> NILAI </center></th>
											
											<th rowspan="2">PENYESUAIAN  </th>
										
                                            
                                            
                                        </tr>

										<tr>
										 

										
										  <th> Ped </th>
										  <th> Prof </th>										 
										  <th> Skor CBT </th>
										  <th> Wawancara </th>
										  

                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                         </div>
                         </div>
                         </div>
                 
 </div> 
      <div id="defaultModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	     Lembar Jawaban 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body" id="loadform">
        <p>Loading...</p>
      </div>
      
    </div>

  </div>
</div>


<div id="pelaksanaanmodal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body" id="loadbody">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
	
<script type="text/javascript">


$(document).off("click",".camera").on("click",".camera",function(){
	            
				var tmujian_id = $(this).attr("tmujian_id");
			 
				  $.post("<?php echo site_url('datasiswa/camera'); ?>",{tmujian_id:tmujian_id},function(data){
	   
				   $("#loadbody").html(data);
  
				  })
				   
				 
			   });
			   
			   $(document).off("click",".camerates").on("click",".camerates",function(){
				  
				var tmsiswa_id = $(this).attr("tmsiswa_id");
			 
				  $.post("<?php echo site_url('datasiswa/camerates'); ?>",{tmsiswa_id:tmsiswa_id},function(data){
	   
				   $("#loadbody").html(data);
  
				  })
				   
				 
			   });
			   $(document).off("click",".bantuan").on("click",".bantuan",function(){
				  
				var tmsiswa_id = $(this).attr("tmsiswa_id");
			 
				  $.post("<?php echo site_url('datasiswa/bantuan'); ?>",{tmsiswa_id:tmsiswa_id},function(data){
	   
				   $("#loadbody").html(data);
  
				  })
				   
				 
			   });

			   $(document).off("click",".ljk").on("click",".ljk",function(){
				  
				  var tmujian_id = $(this).attr("tmujian_id");
			   
					$.post("<?php echo site_url('datasiswa/ljk'); ?>",{tmujian_id:tmujian_id},function(data){
		 
					 $("#loadform").html(data);
	
					})
					 
				   
				 });

				 $(document).off("click",".ljkwawancara").on("click",".ljkwawancara",function(){
				  
				  var tmujian_id = $(this).attr("tmujian_id");
			   
					$.post("<?php echo site_url('datasiswa/ljkwawancara'); ?>",{tmujian_id:tmujian_id},function(data){
		 
					 $("#loadform").html(data);
	
					})
					 
				   
				 });
			   
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[5,10,25, 50,100,200,300,500,1000, 800000000], [5,10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11]
                            },
							text:'Cetak Excel',
						
							},
							
							
							
							
						
							{
							extend: 'colvis',
							
							text:' Pengaturan Kolom ',
							
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("datasiswa/grid_kelulusan"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
							data.keyword 		= $("#keyword").val();
						
							data.kategori 		= $("#kategori").val();
							data.kategori_ujian = $("#kategori_ujian").val();
							data.provinsi = $("#provinsi").val();
							data.kota = $("#kota").val();
							data.unsur = $("#unsur").val();
							data.status = $("#status").val();
							data.literasi = $("#literasi").val();
							data.keputusan = $("#keputusan").val();
							data.urutkan = $("#urutkan").val();
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
				
				$(document).on("click","#cariatuh",function(){
	  
					dataTable.ajax.reload(null,false);	
					
				});
				$(document).on("input","#keyword",function(){
	  
					dataTable.ajax.reload(null,false);	
					
				});

				$(document).on("change","#tmmadrasah_id,#kategori,#provinsi,#kota,#literasi,#status,#keputusan,#urutkan",function(){
						
						dataTable.ajax.reload(null,false);	
						
					});

					$(document).off("click","#generate").on("click","#generate",function(){
	  
	                
					  alertify.confirm("Generate Nomor Tes akan dilakukan, apakah Anda yakin ?  ",function(){


						  $.post("<?php echo site_url("datasiswa/generate_no"); ?>",function(data){

							dataTable.ajax.reload(null,false);	
							alertify.alert(data);



						  })
					  });
  });



  $(document).off("change",".literasi").on("change",".literasi",function(){
      
	   var tmsiswa_id = $(this).attr("tmsiswa_id");
	   var literasi  = $(this).val();
	                
      
          $.post("<?php echo site_url("datasiswa/ubah_literasi"); ?>",{tmsiswa_id:tmsiswa_id,literasi:literasi},function(data){

            dataTable.ajax.reload(null,false);	
            alertify.success("Berhasil Sob");



          });
      
});

$(document).off("change",".kelulusan").on("change",".kelulusan",function(){
      
	  var tmsiswa_id = $(this).attr("tmsiswa_id");
	  var kelulusan  = $(this).val();
				   
	 
		 $.post("<?php echo site_url("datasiswa/ubah_kelulusan"); ?>",{tmsiswa_id:tmsiswa_id,kelulusan:kelulusan},function(data){

		   dataTable.ajax.reload(null,false);	
		   alertify.success("Berhasil Sob");



		 });
	 
});

    

</script>
				