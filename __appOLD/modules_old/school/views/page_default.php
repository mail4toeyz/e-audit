
	<div class="row">	  

<div class="col-lg-4 col-md-6 col-sm-6">
  <div class="card card-stats">
    <div class="card-header card-header-warning card-header-icon">
      <div class="card-icon">
           <i class="fa fa-line-chart"></i>
      </div>
      <p class="card-category">Jumlah Madrasah</p>
      <h3 class="card-title"> <?php echo $this->m->jml("tm_madrasah"); ?>
        <small>Madrasah</small>
      </h3>
    </div>
    <div class="card-footer">
      <div class="stats">
        
        <a href="<?php echo site_url("schooleksekutif"); ?>"> <i class="fa fa-angle-right"></i> Lihat Semua data</a>
      </div>
    </div>
  </div>
</div>
<div class="col-lg-4 col-md-6 col-sm-6">
  <div class="card card-stats">
    <div class="card-header card-header-danger card-header-icon">
      <div class="card-icon">
           <i class="fa fa-line-chart"></i>
      </div>
      <p class="card-category"> Jumlah Siswa Kelas IV</p>
      <h3 class="card-title"> <?php echo $this->m->jml("tm_siswa",6); ?>
        <small>Orang</small>
      </h3>
    </div>
    <div class="card-footer">
      <div class="stats">
        
        <a href="<?php echo site_url("schoolguru"); ?>"> <i class="fa fa-angle-right"></i> Lihat Semua data</a>
      </div>
    </div>
  </div>
</div>
<div class="col-lg-4 col-md-6 col-sm-6">
  <div class="card card-stats">
    <div class="card-header card-header-info card-header-icon">
      <div class="card-icon">
           <i class="fa fa-line-chart"></i>
      </div>
      <p class="card-category"> Jumlah Siswa Kelas V</p>
      <h3 class="card-title"> <?php echo $this->m->jml("tm_siswa",7); ?>
        <small>Orang</small>
      </h3>
    </div>
    <div class="card-footer">
      <div class="stats">
        
        <a href="<?php echo site_url("schoolsiswa"); ?>"> <i class="fa fa-angle-right"></i> Lihat Semua data</a>
      </div>
    </div>
  </div>
</div>


</div>


<div class="col-md-12">
		
        <div class="card">
         <div class="card-header card-header-success">
           <h4 class="card-title">Informasi Sesi pada AKSI 2020  </h4>
           <p class="card-category">Dibawah ini adalah informasi sesi pada AKSI 2020  </p>
         </div>
       
          <div class="card-body">
          
        <div class="row" >
          <div class="table-responsive">
             <table class="table table-hover table-bordered table-striped">
               <thead>
                  <tr>
                     <th> SESI </th>
                     <th> HARI  </th>
                     <th> PUKUL   </th>
                     <th> KUOTA   </th>
                     <th> TERISI    </th>
                  </tr>

                  </thead>

                  <tbody>
               <?php 
                 $sesi = $this->db->get("tm_sesi")->result();
                   foreach($sesi as $rs){
                        $terisi = $this->db->query("select count(id) as jml from tm_siswa where sesi='".$rs->sesi."'")->row();

                   ?>
                  <tr>
                     <td> Sesi <?php echo $rs->sesi; ?> </td>
                     <td> <?php echo $rs->hari; ?>  </td>
                     <td> <?php echo $rs->pukul; ?>   </td>
                     <td> <?php echo $rs->maksimal; ?>   </td>
                     <td> <?php echo $terisi->jml; ?>    </td>
                  </tr>

                  <?php 
                   }
                   ?>

                  </tbody>

             </table>
          </div>


        </div>

        </div>
    </div></div>
