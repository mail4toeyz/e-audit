<?php

class M_guru extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
    }

    
	public function grid($paging){
      
		$key = $_REQUEST['search']['value'];
		$jenjang  = $this->input->get_post("jenjang");

		$this->db->select("*");
        $this->db->from('tm_madrasah');
       
	
	    if(!empty($key)){  $this->db->where("UPPER(nama) LIKE '%".strtoupper($key)."%'");    }
	    if(!empty($jenjang)){  $this->db->where("jenjang",$jenjang);    }
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("jenjang","ASC");
					 $this->db->order_by("id","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
	
	public function insert(){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
	    
							
				
		$this->db->set("password",enkrip($_POST['password']));
		
        
		$this->db->insert("tm_madrasah",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
	public function update($id){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
		
		
		$this->db->where("id",$id);
		$this->db->update("tm_madrasah",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
}
