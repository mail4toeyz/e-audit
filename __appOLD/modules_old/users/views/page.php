<div class="col-md-12">
		
				       <div class="card">
						<div class="card-header card-header-success">
						  <h4 class="card-title">Data Pengguna Aplikasi </h4>
						  <p class="card-category">Anda dapat memasukan users aplikasi dihalaman ini  </p>
						</div>
					  
						 <div class="card-body">
						 
						 <button type="button" target="#loadform" url="<?php echo site_url("users/form"); ?>"  data-toggle="modal" data-target="#defaultModal" data-color="cyan" class="btn btn-success btn-sm addmodal"><span class="fa fa-plus"></span> TAMBAH DATA </button>
						 <button type="button"  id="generatew" class="btn btn-success btn-sm "><span class="fa fa-retweet"></span> GENERATE  </button>
							  
							  
							  
							   <div class="row" >
						 
					        





						        		
								<div class="col-md-5">
								
									<div class="input-group">
									<input type="text" class="form-control" id="keyword"  placeholder="Cari  Nama disini..">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="button">CARI</button>
									</div>
									</div>
								</div>
					   </div>
					   
					   <br>


							<div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
                                        <tr>
                                            <th width="2px">NO</th>
                                            <th>NAMA </th>
                                            <th>USERNAME</th>
                                            <th>PASSWORD </th>                                          
                                            
                                            <th>DIWAWANCARAI  </th>
                                            <th>SUDAH  </th>
                                            <th>BELUM  </th>
                                            
										  							
                                          	<th width="5%">AKSI </th>
                                        </tr>
                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                         </div>
                         </div>
                         </div>
                 
 </div> 
      <div id="defaultModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	     FORM 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body" id="loadform">
        <p>Loading...</p>
      </div>
      
    </div>

  </div>
</div>
	
<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							
							text:'Cetak Excel',
						
							},
							
							
							
							
						
							{
							extend: 'colvis',
							
							text:' Pengaturan Kolom ',
							
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("users/grid"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
							data.keyword = $("#keyword").val();
						
							data.kategori = $("#kategori").val();
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
				
				
	
				$(document).on("input","#keyword",function(){
	  
					dataTable.ajax.reload(null,false);	
					
				});


				$(document).off("click","#generatew").on("click","#generatew",function(){
	  
					alertify.confirm("Apakah Anda yakin melakukan generate wawancara ? ",function(){
                      loading();
						$.post("<?php echo site_url('users/generate_wawancara'); ?>",function(data){


											alertify.alert(data);
											dataTable.ajax.reload(null,false);	

											jQuery.unblockUI({ });

						});

					})
					
					
				});


  

</script>
				