<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 if(!$this->session->userdata("status")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_siswa','m');
		  $this->load->helper('exportpdf_helper');  
		
	  }
	  
   function _template($data)
	{
		
	      $this->load->view('admin/page_header',$data);	
		
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Data Users ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		  $status   =  array("0"=>"Administrator dan Pengawas Ujian","1"=>"Pewawancara");
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			
               $pewawancara = $this->db->query("select count(id) as jml from tm_siswa where pewawancara='".$val['id']."' and nilai IS NOT NULL")->row();
               $sudah       = $this->db->query("select count(id) as jml from tm_siswa where pewawancara='".$val['id']."' and id IN(select tmsiswa_id from h_ujian_w)")->row();
               $belum       = $pewawancara->jml - $sudah->jml;

				$no = $i++;
				$records["data"][] = array(
					$no,
					
					$val['nama']." - ".$val['jabatan'],
					$val['username'],
					$val['password'],
					
					
				
					
					$pewawancara->jml."  Peserta",
					$sudah->jml."  Peserta",
					$belum."  Peserta",
				
					
				    ' 
					
					<button type="button" class="btn btn-success btn-sm ubahmodal" datanya="'.$val['id'].'" urlnya="'.site_url("users/form").'" target="#loadform" data-toggle="modal" data-target="#defaultModal">
                                    <i class="fa fa-pencil"></i>
                      </button>
					  <button type="button"  class="btn btn-success btn-sm hapus"  datanya="'.$val['id'].'" urlnya="'.site_url("users/hapus").'">
                                    <i class="fa fa-trash"></i>
					  </button>
					 
					  '
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}

	public function form(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("admin",array("id"=>$id));
			   
		   }
		$this->load->view("form",$data);
		
	}
	
	
	
	public function save(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan username lain.');
			    $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
    
		 
			
				$config = array(
				    
				   
				    array('field' => 'f[nama]', 'label' => 'Nama Guru  ', 'rules' => 'trim|required'),
				    array('field' => 'f[username]', 'label' => 'Username  ', 'rules' => 'trim|required|is_unique[admin.username]'),
				  
				   
			
				   
				  
				   
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
				
				
				 
				
							 if(empty($id)){
								
								 
								   $this->m->insert();
								   echo "Data Berhasil disimpan";	
								 
								
								
							 }else{
								$this->m->update($id); 
								echo "Data Berhasil disimpan";	
								 
							 }
							 
						
							
						
		
		
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}
	

	
	
	public function hapus(){
		
		$id = $this->input->get_post("id",true);
		
		$this->db->delete("admin",array("id"=>$id));
		echo "sukses";
	}
	// sESI 

   

	public function generate_wawancara(){
		
		if($_SESSION['status']=="kanwil"){

			$peserta_jml = $this->db->query("select count(id) as jml from tm_siswa where pri IS NULL and posisi=2 and provinsi='".$_SESSION['aksi_id']."' and id IN(select tmsiswa_id from h_ujian)")->row();
			$data = $this->db->query("select * from admin where status=1 and provinsi='".$_SESSION['aksi_id']."' and kankemenag=0")->result();


			$this->db->query("update tm_siswa set pewawancara=0 where pri IS NULL and posisi=2 and provinsi='".$_SESSION['aksi_id']."'  and id IN(select tmsiswa_id from h_ujian)");


		}else if($_SESSION['status']=="kankemenag"){

			$peserta_jml = $this->db->query("select count(id) as jml from tm_siswa where pri IS NULL and posisi=3 and kota='".$_SESSION['aksi_id']."' and id IN(select tmsiswa_id from h_ujian)")->row();
			$data = $this->db->query("select * from admin where status=1 and kankemenag='".$_SESSION['aksi_id']."'")->result();

			$this->db->query("update tm_siswa set pewawancara=0 where pri IS NULL and posisi=3 and kota='".$_SESSION['aksi_id']."'  and id IN(select tmsiswa_id from h_ujian)");

		}else if($_SESSION['status']=="admin"){

			$peserta_jml = $this->db->query("select count(id) as jml from tm_siswa where pri IS NULL and posisi=1  and id IN(select tmsiswa_id from h_ujian)")->row();
			$data = $this->db->query("select * from admin where status=1 and provinsi=0 and kankemenag=0")->result();

			$this->db->query("update tm_siswa set pewawancara=0 where pri IS NULL and posisi=1  and id IN(select tmsiswa_id from h_ujian)");
		}
		  
		$no=0;	
		    foreach($data as $rd){

				 $limit = ceil($peserta_jml->jml/count($data)); 

				$peserta = $this->db->query("select * from tm_siswa where pri IS NULL and  pewawancara=0 AND posisi=1  and id IN(select tmsiswa_id from h_ujian) and nilai IS NOT NULL limit ".$limit."")->result();
				if($_SESSION['status']=="kanwil"){

					$peserta = $this->db->query("select * from tm_siswa where pri IS NULL and  pewawancara=0 AND posisi=2 and provinsi='".$_SESSION['aksi_id']."' and nilai IS NOT NULL and id IN(select tmsiswa_id from h_ujian) limit ".$limit."")->result();

				}else if($_SESSION['status']=="kankemenag"){
				   $peserta = $this->db->query("select * from tm_siswa where pri IS NULL and  pewawancara=0 AND posisi=3 and kota='".$_SESSION['aksi_id']."' and nilai IS NOT NULL  and id IN(select tmsiswa_id from h_ujian) limit ".$limit."")->result();
				}
				
				 foreach($peserta as $row){

					$this->db->set("pewawancara",$rd->id);
					$this->db->where("id",$row->id);
					$this->db->update("tm_siswa");

					$no++;
				 }
				
			}

			echo count($peserta)." Peserta Berhasil digenerate";

	}


	public function jadwal()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Data Users ";
	     if(!empty($ajax)){
					    
			 $this->load->view('jadwal',$data);
		
		 }else{
			 
		     $data['konten'] = "jadwal";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid_j(){
		
		  $iTotalRecords = $this->m->grid_j(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid_j(true)->result_array();
		  $status   =  array("0"=>"Administrator dan Pengawas Ujian","1"=>"Pewawancara");
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			
               

				$no = $i++;
				$records["data"][] = array(
					$no,
					
					$val['hari'],
					$val['keterangan'],
					
				
					
				    ' 
					
					<button type="button" class="btn btn-success btn-sm ubahmodal" datanya="'.$val['id'].'" urlnya="'.site_url("users/form_jadwal").'" target="#loadform" data-toggle="modal" data-target="#defaultModal">
                                    <i class="fa fa-pencil"></i>
                      </button>
					  <button type="button"  class="btn btn-success btn-sm hapus" datanya="'.$val['id'].'" urlnya="'.site_url("users/hapus_jadwal").'">
                                    <i class="fa fa-trash"></i>
					  </button>
					 
					  '
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}

	public function form_jadwal(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("jadwal",array("id"=>$id));
			   
		   }
		$this->load->view("jadwal_form",$data);
		
	}
	
	
	
	public function save_jadwal(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan username lain.');
			    $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
    
		 
			
				$config = array(
				    
				   
				    array('field' => 'f[hari]', 'label' => 'Nama   ', 'rules' => 'trim|required'),
				
				  
				   
			
				   
				  
				   
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
				
				
				 
				
							 if(empty($id)){
								
								if($_SESSION['status']=="admin"){
		
								}else if($_SESSION['status']=="kanwil"){
						
									$this->db->set("provinsi",$_SESSION['aksi_id']); 
								
								}else if($_SESSION['status']=="kankemenag"){
						
									$this->db->set("kota",$_SESSION['aksi_id']); 
									
								}

								   $this->db->insert("jadwal",$f);
								   echo "Data Berhasil disimpan";	
								 
								
								
							 }else{

								if($_SESSION['status']=="admin"){
		
								}else if($_SESSION['status']=="kanwil"){
						
									$this->db->set("provinsi",$_SESSION['aksi_id']); 
								
								}else if($_SESSION['status']=="kankemenag"){
						
									$this->db->set("kota",$_SESSION['aksi_id']); 
									
								}


								$this->db->where("id",$id); 
								$this->db->update("jadwal",$f); 
								echo "Data Berhasil disimpan";	
								 
							 }
							 
						
							
						
		
		
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}
	

	
	
	public function hapus_jadwal(){
		
		$id = $this->input->get_post("id",true);
		
		$this->db->delete("jadwal",array("id"=>$id));
		echo "sukses";
	}
	 
}
