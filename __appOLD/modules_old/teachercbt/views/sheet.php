
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        CBT 
        <small> <?php echo $this->Reff->owner(); ?>  </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> CBT</a></li>
        <li class="active">Lembar Soal Ujian </li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content"  style="min-height:700px">

      <div class="row">

         <div class="col-md-2">
              <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    
                    
                    <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-12 border-right">
                           
                        <center> 
                         <img class="img-circle img-responsive" style="width:70px" src="<?php echo base_url(); ?>__statics/img/not.png" alt="<?php echo $data->nama; ?>">
                        </center>   
                        <div class="description-block">
                            <h5 class="description-header"><?php echo $data->nama; ?></h5>
                            <h5 class="description-header"><?php echo $data->no_test; ?></h5>
                        
                        </div>
                        <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                    
                        <!-- /.col -->
                        
                    </div>
                    <!-- /.row -->

                    <div class="row">
                    <div class="col-sm-12">
                    
                    <button class="btn btn-sm btn-danger btn-block bantuan" data-toggle="modal" data-target="#myModal"> <i class="fa fa-question-circle"></i> Bantuan </button> 
                           
                    
                    </div>

                    </div>
                    </div>
                </div>


                <div class="box box-warning">
                            <div class="box-header with-border">
                               <h3 class="box-title">Pengawasan Ujian  </h3>

                            </div>
                        
                            <div class="box-body">
                            <canvas id="canvas" width="210px" height="170px"></canvas> 
                                <video autoplay="true" id="player" style="width: 100%;height:auto">
                                Browsermu tidak mendukung, upgrade yaa !
                                </video> 
                                <br>
                            
                                
                            
                                <button class="btn btn-danger" id="capture-btn" style="display:none"><i class="fa fa-camera"></i> Ambil Gambar  </button>
                            
                            </div>
                </div>

         </div>

         <div class="col-md-7">
                    <?php echo form_open('', array('id'=>'ujian'), array('id'=> $id_tes)); ?>
                    <?php  
                    $jmsoal = $this->db->query("select count(id) as jmsoal from tr_soal where tmujian_id='".$ujian->id."'")->row(); 
                    
                    ?>

                <div class="box box-warning" >
                    <div class="box-header with-border">
                    <h3 class="box-title">Lembar Ujian  <?php echo ($ujian->nama); ?>  </h3>

                    <div class="box-tools pull-right">
                    <span class="badge ">Soal No. <span id="soalke"></span>  Dari <span id="darisoal"><?php echo $jmsoal->jmsoal; ?> Soal </span> </span>
                        <button class="btn btn-box-tool" onclick="increaseFontSizeBy100px()" type="button"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" onclick="increaseFontSizeBy1px()" type="button"><i class="fa fa-plus"></i></button>

                    </div>
                   
                    </div>
                
                    <div class="box-body" id="lembarsoal_dendi">
                       <?php echo $html; ?>

                    </div>

                    <div class="box-footer text-center">
                            <a class="action back btn btn-info" rel="0"  onclick="return back();"><i class="glyphicon glyphicon-chevron-left"></i> Sebelumnya</a>
                            
                            <a class="ragu_ragu btn btn-warning" rel="1" onclick="return tidak_jawab();" >Ragu-ragu</a>
                            
                            <a class="action next btn btn-info" rel="2"  onclick="return next();"><i class="glyphicon glyphicon-chevron-right"></i> Selanjutnya </a>
                            <a class="selesai action submit btn btn-danger"  onclick="return simpan_akhir();"><i class="glyphicon glyphicon-stop"></i> Selesai</a>
                            <input type="hidden" name="jml_soal" id="jml_soal" value="<?=$no; ?>">
                    </div>


                </div>
         </div>



	 <div class="col-md-3">


    <div class="box box-warning">
                    <div class="box-header with-border">
                    <h3 class="box-title"> NAVIGASI SOAL  </h3>
                     </div>
                
                    <div class="box-body" >
                         <div class="text-center" id="tampil_jawaban"></div> <br>
                        <span class="col-md-2 col-sm-2 col-xs-2" style="border:1px solid black;height:20px;width:20px;margin-left:40px;background-color:#e1dedd"></span> &nbsp; <b style="font-size:11px">Belum dijawab<b> <br>
                                
                        <span class="col-md-2 col-sm-2 col-xs-2" style="border:1px solid black;height:20px;width:20px;margin-left:40px;background-color:#f39c12"></span> &nbsp; <b style="font-size:11px">Ragu-Ragu <b> <br>
                        <span class="col-md-2 col-sm-2 col-xs-2" style="border:1px solid black;height:20px;width:20px;margin-left:40px;background-color:#1dc672"></span> &nbsp; <b style="font-size:11px">Sudah Dijawab <b> <br>
                       
                    <br>
                    <center> <a href="<?php echo site_url("banksoal/buatsoal"); ?>" class="btn btn-danger btn-sm " ><i class="glyphicon glyphicon-stop"></i>
			    Kembali   </center>
			 </a> 
                    </div>
    </div>

    

  

    
      
    
       

      
        
		
		
		
		
		
		
    </div>
</section>
</div>

<?php 





?>


<script type="text/javascript">

function increaseFontSizeBy100px() {
    txt = document.getElementById('lembarsoal_dendi');
    style = window.getComputedStyle(txt, null).getPropertyValue('font-size');
    currentSize = parseFloat(style);
    txt.style.fontSize = (currentSize - 1) + 'px';
}
function increaseFontSizeBy1px() {
    txt = document.getElementById('lembarsoal_dendi');
    style = window.getComputedStyle(txt, null).getPropertyValue('font-size');
    currentSize = parseFloat(style);
    txt.style.fontSize = (currentSize + 1) + 'px';
}



    var base_url        = "<?php echo base_url(); ?>";
    var id_tes          = "<?php echo $id_tes; ?>";
    var widget          = $(".step");
    var total_widget    = widget.length;
	
	



</script>

<script type="text/javascript">

$(document).ready(function () {
    var t = $('.sisawaktu');
    if (t.length) {
        sisawaktu(t.data('time'));
    }

    buka(1);
    simpan_sementara();

    widget = $(".step");
    btnnext = $(".next");
    btnback = $(".back");
    btnsubmit = $(".submit");

    $(".step, .back, .selesai").hide();
    $("#widget_1").show();
});

function getFormData($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};
    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });
    return indexed_array;
}

function buka(id_widget) {
    $(".next").attr('rel', (id_widget + 1));
    $(".back").attr('rel', (id_widget - 1));
    $(".ragu_ragu").attr('rel', (id_widget));
    cek_status_ragu(id_widget);
    cek_terakhir(id_widget);

    $("#soalke").html(id_widget);

    $(".step").hide();
    $("#widget_" + id_widget).show();

    simpan();
}

function next() {
    var berikutnya = $(".next").attr('rel');
    berikutnya = parseInt(berikutnya);
    berikutnya = berikutnya > total_widget ? total_widget : berikutnya;

    $("#soalke").html(berikutnya);

    $(".next").attr('rel', (berikutnya + 1));
    $(".back").attr('rel', (berikutnya - 1));
    $(".ragu_ragu").attr('rel', (berikutnya));
    cek_status_ragu(berikutnya);
    cek_terakhir(berikutnya);

    var sudah_akhir = berikutnya == total_widget ? 1 : 0;

    $(".step").hide();
    $("#widget_" + berikutnya).show();

    if (sudah_akhir == 1) {
        $(".back").show();
        $(".next").hide();
    } else if (sudah_akhir == 0) {
        $(".next").show();
        $(".back").show();
    }

    simpan();
}

function back() {
    var back = $(".back").attr('rel');
    back = parseInt(back);
    back = back < 1 ? 1 : back;

    $("#soalke").html(back);

    $(".back").attr('rel', (back - 1));
    $(".next").attr('rel', (back + 1));
    $(".ragu_ragu").attr('rel', (back));
    cek_status_ragu(back);
    cek_terakhir(back);

    $(".step").hide();
    $("#widget_" + back).show();

    var sudah_awal = back == 1 ? 1 : 0;

    $(".step").hide();
    $("#widget_" + back).show();

    if (sudah_awal == 1) {
        $(".back").hide();
        $(".next").show();
    } else if (sudah_awal == 0) {
        $(".next").show();
        $(".back").show();
    }

    simpan();
}

function tidak_jawab() {
    var id_step = $(".ragu_ragu").attr('rel');
    var status_ragu = $("#rg_" + id_step).val();

    if (status_ragu == "N") {
        $("#rg_" + id_step).val('Y');
        $("#btn_soal_" + id_step).removeClass('btn-success');
        $("#btn_soal_" + id_step).addClass('btn-warning');

    } else {
        $("#rg_" + id_step).val('N');
        $("#btn_soal_" + id_step).removeClass('btn-warning');
        $("#btn_soal_" + id_step).addClass('btn-success');
    }

    cek_status_ragu(id_step);

    simpan();
}

function cek_status_ragu(id_soal) {
    var status_ragu = $("#rg_" + id_soal).val();

    if (status_ragu == "N") {
        $(".ragu_ragu").html('Ragu');
    } else {
        $(".ragu_ragu").html('Tidak Ragu');
    }
}

function cek_terakhir(id_soal) {
    var jml_soal = $("#jml_soal").val();
    jml_soal = (parseInt(jml_soal) - 1);

    if (jml_soal === id_soal) {
        $('.next').hide();
        $(".selesai, .back").show();
    } else {
        $('.next').show();
        $(".selesai, .back").hide();
    }
}

function simpan_sementara() {
    var f_asal = $("#ujian");
    var form = getFormData(f_asal);
   
    var jml_soal = form.jml_soal;
    jml_soal = parseInt(jml_soal);
   
    var hasil_jawaban = "";
    var nomor=0;
    for (var i = 1; i < jml_soal; i++) {
		nomor++;
		  if(nomor <10){
			  nomor = "0"+nomor;
		  }
        var idx = 'jawaban_' + i;
        var idx2 = 'rg_' + i;
        var jawab = form[idx];
        var ragu = form[idx2];
      
          if(i % 5==1){
			  
			  hasil_jawaban +='<br>';
		  }
        if (jawab != undefined) {
            if (ragu == "Y") {
                if (jawab == "-") {
                    hasil_jawaban += '<a id="btn_soal_' + (i) + '" class="btn btn-default btn_soal btn-sm" onclick="return buka(' + (i) + ');">' + (nomor) + ". " + jawab + "</a>";
                } else {
                    hasil_jawaban += '<a id="btn_soal_' + (i) + '" class="btn btn-warning btn_soal btn-sm" onclick="return buka(' + (i) + ');">' + (nomor) + ". " + jawab + "</a>";
                }
            } else {
                if (jawab == "-") {
                    hasil_jawaban += '<a id="btn_soal_' + (i) + '" class="btn btn-default btn_soal btn-sm" onclick="return buka(' + (i) + ');">' + (nomor) + ". " + jawab + "</a>";
                } else {
                    hasil_jawaban += '<a id="btn_soal_' + (i) + '" class="btn btn-success btn_soal btn-sm" onclick="return buka(' + (i) + ');">' + (nomor) + ". " + jawab + "</a>";
                }
            }
        } else {
			
			
            hasil_jawaban += '<a id="btn_soal_' + (i) + '" class="btn btn-default btn_soal btn-sm" onclick="return buka(' + (i) + ');">' + (nomor) + ". -</a>";
        }
      
    }
    $("#tampil_jawaban").html('<div id="yes"></div>' + hasil_jawaban);
  
}

 $(document).off("click",".ayolah").on("click",".ayolah",function(){
    
     var nomor = $(this).attr("nomor");

       $("#btn_soal_"+nomor).addClass('btn-success').removeClass('btn-default');

 });


function simpan() {
    update_waktu();

    var form = $("#ujian");

    $.ajax({
        type: "POST",
        url: base_url + "cbt/simpan_satu",
        data: form.serialize(),
        dataType: 'json',
        success: function (data) {
            // $('.ajax-loading').show();
            console.log(data);
        }
    });
}


function selesai() {
    simpan();
    
    $.ajax({
        type: "POST",
        url: base_url + "cbt/simpan_akhir",
        data: { id: id_tes },
        beforeSend: function () {
            simpan();
            // $('.ajax-loading').show();    
        },
        success: function (r) {
            if (r.status) {
            window.location.href = base_url + 'cbt';
            }else{

                Swal.fire({
				  title: 'Gagal menyimpan, Periksa kembali lembar jawaban Anda  !',
				  html  : 'Soal nomor : <br> '+r.data+' <br>  belum terisi atau tersimpan',
				  type : 'error',
                  confirmButtonText: 'Refresh Browser dan Lanjutkan pengerjaan',
				}).then((result) => {
			        if (result.value) {
				  
				      location.reload();
				   
				
			        }
			   })

               

                
            }
        }
    });
}

function waktuHabis() {
	  simpan(); 
	         
			
               Swal.fire(
				  'Terimakasih !',
				  'Waktu Anda sudah habis',
				  'warning'
				);
				
				   selesai();
				   
				   
			
				  
   
    
}

function simpan_akhir() {
    clearInterval(waktu_update);
    simpan();   
			Swal.fire({
			  title: 'Apakah Anda yakin menyelesaikan ujian ini ?',
			  text: "Jika Anda mengakhiri ujian ini, hasil jawaban Anda tidak dapat dirubah, mohon periksa kembali hasil jawaban Anda sebelum mengakhiri ujian ini !",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Ya, Akhiri Ujian ini ',
			  cancelButtonText: 'Tidak, sepertinya saya akan periksa kembali '
			}).then((result) => {
			  if (result.value) {
				  
				   Swal.fire(
				  'Terimakasih !',
				  'Anda sudah mengakhiri ujian ini.',
				  'success'
				);
				
				
				   selesai();
				   
				   
				
			  }
			})

	
    
}


function sisawaktu(t) {

    var timeleft = $("#waktu_selesai").val();

        var downloadTimer = setInterval(function(){
        if(timeleft <= 0){
            clearInterval(downloadTimer);
            
            Swal.fire(
				  'Waktu Anda sudah habis !',
				  'Ujian ini sudah berakhir, Terimakasih ',
				  'success'
				);
				
				
				   selesai();


        } else {

           
			var sisawaktu = new Date(timeleft * 1000).toISOString().substr(11, 8);	

            $("#waktu_selesai").val(timeleft);
            $(".sisawaktu").html(sisawaktu);
        }
        timeleft -= 1;
        }, 1000);
}


function update_waktu() {
    
    var waktu = $("#waktu_selesai").val();

    $.ajax({
        type: "POST",
        url: base_url + "cbt/update_waktu",
        data: { id: id_tes,waktu:waktu},
      
        success: function (r) {
          
              
        }
    });


}


var waktu_update = setInterval(function() {
    update_waktu();
    $("#capture-btn").trigger("click");
}, 300000);

//clearInterval(waktu_update)


</script>



<script type="text/javascript">
  const videoPlayer = document.querySelector("#player");
const canvasElement = document.querySelector("#canvas");
const captureButton = document.querySelector("#capture-btn");
const imagePicker = document.querySelector("#image-picker");
//const imagePickerArea = document.querySelector("#pick-image");
const newImages = document.querySelector("#newImages");

// Image dimensions
const width = 320;
const height = 240;
let zIndex = 1;

const createImage = (src, alt, title, width, height, className) => {
  let newImg = document.createElement("img");

  if (src !== null) newImg.setAttribute("src", src);
  if (alt !== null) newImg.setAttribute("alt", alt);
  if (title !== null) newImg.setAttribute("title", title);
  if (width !== null) newImg.setAttribute("width", width);
  if (height !== null) newImg.setAttribute("height", height);
  if (className !== null) newImg.setAttribute("class", className);

  return newImg;
};

const startMedia = () => {
  if (!("mediaDevices" in navigator)) {
    navigator.mediaDevices = {};
  }

  if (!("getUserMedia" in navigator.mediaDevices)) {
    navigator.mediaDevices.getUserMedia = constraints => {
      const getUserMedia =
        navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

      if (!getUserMedia) {
        return Promise.reject(new Error("getUserMedia is not supported"));
      } else {
        return new Promise((resolve, reject) =>
          getUserMedia.call(navigator, constraints, resolve, reject)
        );
      }
    };
  }

  navigator.mediaDevices
    .getUserMedia({ video: true })
    .then(stream => {
      videoPlayer.srcObject = stream;
      videoPlayer.style.display = "block";
    })
    .catch(err => {
     
    });
};


captureButton.addEventListener("click", event => {
    $("#canvas").show("");
                                      canvasElement.style.display = "block";
                                        const context = canvasElement.getContext("2d");

                                        context.drawImage(videoPlayer, 0, 0, canvas.width, canvas.height);

                                        videoPlayer.srcObject.getVideoTracks().forEach(track => {
                                        
                                        });

                                        
                                        let picture = canvasElement.toDataURL();
                                        var ngepost = JSON.stringify({ data: picture });
                                            $.post("<?php echo site_url('cbt/capturefoto/'.$id_tes.''); ?>",ngepost,function(data){
                                            
                                           
                                            
                                        });

                                       


});

window.addEventListener("load", event => startMedia());



</script>