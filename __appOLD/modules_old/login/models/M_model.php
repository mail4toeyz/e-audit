<?php

class M_model extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		
    }

	
	
	public function login(){
		  
		date_default_timezone_set("Asia/Jakarta");
		  $username   = xss(trim($this->input->get_post("username")));
		  $password   = xss(trim($this->input->get_post("password")));
		
		  $username   = $this->security->xss_clean(($this->db->escape_str($username)));
		  $password   = $this->db->escape_str($this->security->xss_clean($password));
		
		  
		  $kanwil     = $this->Reff->query("select * from provinsi where username='".$username."' and password='".($password)."'")->row();
		  $kankemenag = $this->Reff->query("select * from kota where username='".$username."' and password='".($password)."'")->row();
		  
		  $admin      = $this->Reff->query("select * from admin where username='".$username."' and password='".($password)."'")->row(); 
		  
		      
		
			 
		 if(!is_null($kanwil)){


				$session = array(
					'aksi_id'        => $kanwil->id,
					'nama_aksi'        => $kanwil->nama,
					'status'        => "kanwil",
					'is_login'        => true,
					'date_login'   => date("Y-m-d H:i:s"));
					
					$this->session->sess_expiration = 0;
					$this->session->set_userdata($session);
					
					
					
					$this->Reff->log($kanwil->nama. " login kedalam aplikasi pada ".formattimestamp(date("Y-m-d H:i:s"))."","1",$kanwil->id,$kanwil->id);
					return "admin";

			}else if(!is_null($kankemenag)){


					$session = array(
						'aksi_id'        => $kankemenag->id,
						'nama_aksi'        => $kankemenag->nama,
						'status'        => "kankemenag",
						'is_login'        => true,
						'date_login'   => date("Y-m-d H:i:s"));
						
						$this->session->sess_expiration = 0;
						$this->session->set_userdata($session);
						
						
						
						$this->Reff->log($kankemenag->nama. " login kedalam aplikasi pada ".formattimestamp(date("Y-m-d H:i:s"))."","1",$kankemenag->id,$kankemenag->id);
						return "admin";

			}else if(!is_null($admin)){
							if($admin->status==0){
				 
															   $session = array(
															   'aksi_id'        => $admin->id,
															   'nama_aksi'        => $admin->nama,
															   'status'        => "admin",
															   'is_login'        => true,
															   'date_login'   => date("Y-m-d H:i:s"));
															   
															   $this->session->sess_expiration = 0;
															   $this->session->set_userdata($session);
															   
															   
															   
															   $this->Reff->log($admin->nama. " login kedalam aplikasi pada ".formattimestamp(date("Y-m-d H:i:s"))."","1",$admin->id,$admin->id);
															   return "admin";


							 }else if($admin->status==1){
							   
							   $status ="pusat";
							   if($admin->provinsi !=0){

								   $status="kanwil";
							   }else if($admin->kankemenag !=0){

								   $status="kankemenag";
							   }

							   return "gagal";
														/*	   $session = array(
																   'users_id'        => $admin->id,
																   'nama_aksi'        => $admin->nama,
																   'status'        => "Pewawancara",
																   'role'        => $status,
																   'is_login'        => true,
																   'date_login'   => date("Y-m-d H:i:s"));
																   
																   $this->session->sess_expiration = 0;
																   $this->session->set_userdata($session);
																   
																   
																   
																   $this->Reff->log($admin->nama. " login kedalam aplikasi pada ".formattimestamp(date("Y-m-d H:i:s"))."","1",$admin->id,$admin->id);
																   return "wawancara"; */
									   
									   
									   
						   }

				   
				 
			 
			  }else{
				  
				  return "gagal";
				  
			  }
		
	}


	public function login_peserta(){
		  
		date_default_timezone_set("Asia/Jakarta");
		  $username   = xss(trim($this->input->get_post("username")));
		  $password   = xss(trim($this->input->get_post("password")));
		
		  $username   = $this->security->xss_clean(($this->db->escape_str($username)));
		  $password   = $this->db->escape_str($this->security->xss_clean($password));
		
		   $siswa      = $this->Reff->query("select id,nama,sesi,login from tm_siswa where no_test='".$username."' and tgl_lahir='".($password)."'")->row();
		  
		  
		  
		     if(!is_null($siswa)){
				  
				  $cek = $this->Reff->get_kondisi(array("sesi"=>$siswa->sesi),"tm_sesi","open");

						 if($cek==0){
							return "sesibuka";
							false;
						 }

						 if($siswa->login==1){
						//	return "sudahlogin";
						//	false;
						 }
				                                                $session = array(
																'siswaID'        => $siswa->id,
																'nama_siswa'        => $siswa->nama,																
																'status'        => "siswa",
																'timestamp'        => time(),
																'is_login'        => true,																
																'date_login'   => date("Y-m-d H:i:s"));
																
																$this->session->sess_expiration = 0;
																$this->session->set_userdata($session);
																

																$this->db->set("login",1);
																$this->db->where("id",$siswa->id);
																$this->db->update("tm_siswa");  

																$this->Reff->log($siswa->nama. " login kedalam aplikasi pada ".formattimestamp(date("Y-m-d H:i:s"))."","3",$siswa->id,$siswa->id);
															
																
																return "cbt";
			 
					
				  
			  }else{
				  
				  return "gagal";
				  
			  }
		
	}
	
	
}
