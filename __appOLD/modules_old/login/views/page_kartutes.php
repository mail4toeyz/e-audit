<!DOCTYPE html>
<html>
<head>
	<title>Selamat Datang di Computer Based Test </title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/login/css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/a81368914c.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/alert/alert.css">
	<script src="<?php echo base_url(); ?>__statics/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>__statics/js/alert/alert.js"></script>
	<link rel="shortcut icon" href="<?php echo base_url(); ?>__statics/img/logo.png">
	<meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
	<img class="wave" src="<?php echo base_url(); ?>__statics/login/img/wave.png">
	<div class="container">
	
		<div class="img">
		 
			<img src="<?php echo base_url(); ?>__statics/login/img/bg.svg">
		</div>
		<div class="login-content">
		
		<form  action="<?php echo site_url("login/do_kartutes"); ?>" method="post">
				<img src="<?php echo base_url(); ?>__statics/img/logomadrasah.png">
				<h4 class="title"><h3>Cetak Kartu Tes Anda </h3>   </h4>
           		<div class="input-div one">
           		   <div class="i">
           		   		<i class="fas fa-user"></i>
           		   </div>
           		   <div class="div">
           		   		<h5>Masukkan NISN Anda  </h5>
           		   		<input type="text" class="input" name="username" required>
           		   </div>
           		</div>
           		
            	<button type="submit" value="submit" class="btn"> <i class="fa fa-file"></i> Download Kartu Tes  </button> 
			
            	
												
													 <br>
												 <img src="<?php echo base_url(); ?>__statics/img/loading.gif" id="loadingm" style="display:none">
            	
            </form>
        </div>
		
    </div>
	<script type="text/javascript" src="<?php echo base_url(); ?>__statics/login/js/main.js"></script>
	
	

<script type="text/javascript">

var base_url="<?php echo base_url(); ?>";
   




function sukses(param){
	   let timerInterval;
	   Swal.fire({
		 type: 'success',
		 title: 'Proses Authentication Berhasil ',
		 showConfirmButton: false,
		  html: 'Dalam <strong></strong> detik<br>Anda akan dialihkan kedalam aplikasi',

		 timer: 1000,
		 onBeforeOpen: () => {
		   Swal.showLoading();
		   timerInterval = setInterval(() => {
			 Swal.getContent().querySelector('strong')
			   .textContent = Swal.getTimerLeft()
		   }, 100)
		 },
		 onClose: () => {
		   clearInterval(timerInterval)
		 }
	   }).then((result) => {
		 if (
		   /* Read more about handling dismissals below */
		   result.dismiss === Swal.DismissReason.timer
		   
		 ) {
		
		  location.href = base_url+param;
		 }
	   })
}

function gagal(param){
	   let timerInterval;
	   Swal.fire({
		 type: 'warning',
		 title: 'Proses Authentication Gagal ',
		 showConfirmButton: true,
		  html: param

	   })
}


   $(document).on('submit', 'form#loginmadrasah', function (event, messages) {
	event.preventDefault();
	  var form   = $(this);
	  var urlnya = $(this).attr("url");
   
	 
	
			 $("#loadingm").show();
			  $.ajax({
				   type: "POST",
				   url: urlnya,
				   data: form.serialize(),
				   success: function (response, status, xhr) {
				if(response=="gagal"){
					alert("NIK tidak ditemukan");

				}else{


				}
					   
					   $("#loadingm").hide();
					   
				   }
			   });
			 
	   return false;
   });
   
   
   
</script>


</body>
</html>
