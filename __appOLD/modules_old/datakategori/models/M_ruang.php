<?php

class M_ruang extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
    }

    
	public function grid($paging){
      
	   
	   
	    $keyword = $_POST['keyword'];
	 
		
		
		$this->db->select("*");
        $this->db->from('tm_kategori');
        
        
	    if(!empty($keyword)){  $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'");    }
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 
					 $this->db->order_by("id","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	


	public function insert(){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
		$this->db->set("tmmadrasah_id",$_SESSION['aksi_id']);
		$this->db->insert("tm_kategori",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
	public function update($id){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
		  
		
		$this->db->where("id",$id);
		$this->db->update("tm_kategori",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
	
}
