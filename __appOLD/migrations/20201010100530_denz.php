<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Denz extends CI_Migration {

        public function up()
        {
		   // Ruang Kelas
		   
		 $ajaran = array(
                         'ajaran' => array(
                                                 'type' => 'char',
                                                 'constraint' => 4
                                          )
                        
                        );
			
			if (!$this->db->field_exists('ajaran', 'tr_ruangkelas'))
               {
	
                 $this->dbforge->add_column('tr_ruangkelas', $ajaran);
				 
				   $this->db->query("update tr_ruangkelas set ajaran='2019' where ajaran < '2020'");
			   }else{
				   
				   $this->db->query("update tr_ruangkelas set ajaran='2019' where ajaran < '2020'");
			   }
			   
			   
			   
			  $alumni = array(
                         'id' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 11,
                                                 'unsigned' => TRUE,
                                                 'auto_increment' => TRUE
                                          ),
                        'tmsiswa_id' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 11
                                          ),
										  
						'nisn' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => 50,
												  'null' => TRUE
                                          ),
					    'nama' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => 300
                                          ),	

						'tempat' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => 100,
												  'null' => TRUE
                                          ),

						'gender' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => 100,
												  'null' => TRUE
                                          ),										  

						'tgl_lahir' => array(
                                                 'type' => 'date',
												  'null' => TRUE
                                          ),

						'foto' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => 100,
												 'null' => TRUE
                                          ),

                        'folder' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => 100,
												  'null' => TRUE
                                          ),


                        'ajaran' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => 10
                                          ),										  
                        
                        
                        );
						
	   
	   if (!$this->db->table_exists("tr_alumni") )
           {
				   $this->dbforge->add_key('id', TRUE);
				   $this->dbforge->add_field($alumni);
				   $this->dbforge->create_table('tr_alumni');
		   }
		   
		   
		 
		 $vicon = array(
                         'id' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 11,
                                                 'unsigned' => TRUE,
                                                 'auto_increment' => TRUE
                                          ),
										  
						'jenis' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 11
                                          ),
										  
					    'status' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 11
                                        ),
                        'trkelas_id' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 11
                                          ),
										  
						'kode' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => 50,
												  'null' => TRUE
                                          ),
					    'nama' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => 300
                                          ),	

						'partisipan' => array(
                                                 'type' => 'text',
												  'null' => TRUE
                                          ),
										  
						 'partisipan_mulai' => array(
                                                  'type' => 'text',
												  'null' => TRUE
                           ),

						'partisipan_selesai' => array(
                                                  'type' => 'text',
												  'null' => TRUE
                           ),									  

						'tgl_mulai' => array(
                                                 'type' => 'DATETIME',
												  'null' => TRUE
                                          ),

						'tgl_selesai' => array(
                                                 'type' => 'DATETIME',
                                                 
												 'null' => TRUE
                                          ),
									  
                        
                        
                        );
						
	   
	   if (!$this->db->table_exists("tr_vicon") )
           {
				   $this->dbforge->add_key('id', TRUE);
				   $this->dbforge->add_field($vicon);
				   $this->dbforge->create_table('tr_vicon');
		   }
		   
		   
			
		   
        }
		
		
		

       
}

?>