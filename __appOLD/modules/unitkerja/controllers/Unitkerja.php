<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unitkerja extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 
		  isLogin();
		  $this->load->model('M_referensi','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('admin/page_header',$data);	
	}
		
	public function pegawai($param)
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['data']	  = $this->db->get_where("unit_kerja",array("id"=>base64_decode($param)))->row();
		 $data['title']   = $data['data']->nama;
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			    
			   
				$no = $i++;
				$records["data"][] = array(
					$no,
			
              	
				
					$val['nama'],
					$val['nip'],
					$val['golongan'],
					$val['jabatan'],
					$this->Reff->get_kondisi(array("id"=>$val['jabatan_id']),"jabatan","nama"),
					$this->Reff->get_kondisi(array("id"=>$val['unitkerja_id']),"unit_kerja","nama"),
				
					
					' 
					<div class="btn-group" role="group">
					<button type="button" class="btn btn-outline-primary ubahmodal" datanya="'.$val['id'].'" urlnya="'.site_url("unitkerja/form").'" data-bs-toggle="modal" data-bs-target="#basicModal" target="#loadform">
                                    <i class="fa fa-pencil"></i>
                      </button>
					  <button type="button"  class="btn btn-outline-danger btn-sm hapus" datanya="'.$val['id'].'" urlnya="'.site_url("unitkerja/hapus").'">
                                    <i class="fa fa-trash"></i>
					  </button>
					  </div>
					 
					  '
				  
				    

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	

	public function form_excel(){		
	
		$data = array();	
		$this->load->view("form_excel",$data);
		
	}
	
	public function import_excel()
		{
		$this->load->library('PHPExcel/IOFactory');
       
	
	  
	    $sheetup   = 0;
	    $rowup     = 2;
		
		$unitkerja_id = $this->input->get_post("unitkerja_id");
		
		$fileName = time().".xlsx";
         
        $config['upload_path'] = './__statics/excel/tmp/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = '*';
        $config['max_size'] = 100000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') ){
			$jupload=0;
              echo $this->upload->display_errors();

			  
		}else{
             
        $media = $this->upload->data('file');
		
        $inputFileName = './__statics/excel/tmp/'.$fileName;
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet($sheetup);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
        
             
			 $jupload=0;
            for ($row = $rowup; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
				
       
			 	
			 	$nip        =      (trim($rowData[0][1]));
			 	$nama         =      (trim($rowData[0][2]));
			 	$golongan     			 =      (trim($rowData[0][3]));
			    $jabatan                 =      (trim($rowData[0][4])); 
			    $jabatan_penugasan       =      (trim($rowData[0][5])); 
			    $unitkerja              =      (trim($rowData[0][6])); 
			   
			 
			
				 if(!empty($nip)){
					 
			
					$jupload++;
                 $data = array(
                    "nip"=> $nip ,                 
                    "nama"=> $nama ,                 
                    "golongan"=> $golongan ,                 
                    "jabatan"=> $jabatan ,                 
                    "jabatan_penugasan"=> $jabatan_penugasan ,                 
                    "unitkerja"=> $unitkerja         
                           
				  );
				  
				  
				   	  
				
                   $this->db->set("unitkerja_id",$unitkerja_id);
                   $this->db->insert("pegawai",$data);
				
				   
				   
				
            
		   }
			}
			
			
		
			echo "<br><br><center>Data yg di upload : ". $jupload ." Data <br> <a href='".site_url("schoolsiswa")."' > Kembali ke Aplikasi </a>";
        
        }
   }

	public function generateJabatan(){

		$data = $this->db->get("pegawai")->result();
		  foreach($data as $row){
			  $jabatan_id = $this->db->get_where("jabatan",array("nama"=>$row->jabatan_penugasan))->row();

			  $this->db->set("jabatan_id",$jabatan_id->id);
			  $this->db->where("id",$row->id);
			  $this->db->update("pegawai");
		  }
	}
	


	public function form(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("pegawai",array("id"=>$id));
			   
		   }
		$this->load->view("form",$data);
		
	}
	
	
	
	public function save(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
			    $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
    
		 
			
				$config = array(
				    
				   
				    array('field' => 'f[nip]', 'label' => 'Nomor Induk Pegawai  ', 'rules' => 'trim|required'),
				    array('field' => 'f[nama]', 'label' => 'Nama   ', 'rules' => 'trim|required'),
				    array('field' => 'f[golongan]', 'label' => 'Golongan   ', 'rules' => 'trim|required'),
				    array('field' => 'f[jabatan]', 'label' => 'Jabatan   ', 'rules' => 'trim|required'),
				    array('field' => 'f[jabatan_id]', 'label' => 'Jabatan Penugasan   ', 'rules' => 'trim|required'),
				    array('field' => 'f[unitkerja_id]', 'label' => 'Unit Kerja    ', 'rules' => 'trim|required'),
				  
				   
			
				   
				  
				   
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
				
				
				 
				
							 if(empty($id)){
								
								 
								   $this->db->insert("pegawai",$f);
								   echo "Data Berhasil disimpan";	
								 
								
								
							 }else{
								$this->db->where("id",$id);
								$this->db->update("pegawai",$f);
								echo "Data Berhasil disimpan";	
								 
							 }
							 
						
							
						
		
		
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}

	public function hapus(){
		
		$id = $this->input->get_post("id",true);
		
		$this->db->delete("pegawai",array("id"=>$id));
		echo "sukses";
	}
	

	 
}
