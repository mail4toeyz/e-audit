<link href="<?php echo base_url(); ?>__statics/js/gal/css/lightgallery.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>__statics/js/gal/js/lightgallery-all.min.js"></script>
<?php 
$pengaturan_id  = $this->Reff->set();
?>
<div class="col-md-12">
              <div class="card">
              
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                  <h6 class="text-white text-capitalize ps-3">Pengawasan Peserta CBT <?php echo $this->Reff->get_kondisi(array("id"=>$tmjadwal_id),"tm_jadwal","hari"); ?> <br>Anda dapat melakukan pengawasan ujian secara virtual melalui aplikasi, Anda dapat melihat foto-foto pergerakan peserta ketika sedang mengerjakan ujian </h6>
                  
                </div>
                </div>


                <div class="card-body">
                <div class="row">
                <div class="col-lg-12 col-md-12">
                <div class="card">
                <div class="card-header card-header-tabs card-header-danger">


                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                            
                              <ul class="nav nav-tabs" data-tabs="tabs">
                                  <?php 
                                  
                                  $hari = $this->Reff->get_kondisi(array("id"=>$tmjadwal_id,"pengaturan_id"=>$pengaturan_id ),"tm_jadwal","hari");
                                  $mapel = $this->db->query("select * from tm_jadwal where hari='".$this->db->escape_str($hari)."' and pengaturan_id='{$pengaturan_id}' ")->result();
                                  $no=0;
                                  foreach($mapel as $rm){
                                    $no++;
                                    $active="";
                                      if($no==1){
                                        $active="active";
        
                                      }
                                    ?>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo $active; ?>" href="#mapel<?php echo $rm->id; ?>" data-toggle="tab">
                                      <?php echo $rm->matapelajaran; ?>
                                      <div class="ripple-container"></div>
                                    </a>
                                  </li>

                                  <?php 
                                  }
                                  ?>

                              </ul>
                            </div>
                        </div>
                    </div>

                <div class="tab-content">
                <?php 
                                  $hari = $this->Reff->get_kondisi(array("id"=>$tmjadwal_id),"tm_jadwal","hari");
                                  $mapel = $this->db->query("select * from tm_jadwal where hari='".$this->db->escape_str($hari)."' ")->result();
                                  $no=0;
                                  foreach($mapel as $rm){
                                    $no++;
                                    $active="";
                                      if($no==1){
                                        $active="active";
        
                                      }
                                    ?>
                                <div class="tab-pane <?php echo $active; ?>" id="mapel<?php echo $rm->id; ?>">
                                
                                 
                                                                <div class="card">
                                                                  <div class="card-header card-header-tabs card-header-primary">
                                                                    <div class="nav-tabs-navigation">
                                                                      <div class="nav-tabs-wrapper">
                                                                      
                                                                        <ul class="nav nav-tabs" data-tabs="tabs">

                                                              <?php 
                                                                
                                                               
                                                                            $no=0;
                                                                            $sesi = $this->db->query("select max(sesi) as sesi from tm_sesi where pengaturan_id='{$pengaturan_id}'")->row();

                                                                          for($a=1;$a <= $sesi->sesi; $a++){
                                                                              $no++;
                                                                              $active="";
                                                                                if($no==1){
                                                                                  $active="active";

                                                                                }
                                                                          ?>
                                                                            <li class="nav-item">
                                                                              <a class="nav-link <?php echo $active; ?>" href="#literasi-<?php echo $a; ?><?php echo $rm->id; ?>" data-toggle="tab">
                                                                                Sesi  <?php echo $a; ?> 
                                                                                <div class="ripple-container"></div>
                                                                              </a>
                                                                            </li>
                                                                          <?php 
                                                                            }
                                                                          ?>
                                                                      
                                                                          


                                                                        </ul>
                                                                      </div>
                                                                    </div>
                                                                  </div>





                                                                  <div class="card-body">
                                                                    <div class="tab-content">
                                                            
                                                            <?php 
                                                            $no=0;
                                                            $offset =20;
                                                              $batas=0;
                                                              $sesi = $this->db->query("select max(sesi) as sesi from tm_sesi where pengaturan_id='{$pengaturan_id}'")->row();

                                                                          for($a=1;$a <= $sesi->sesi; $a++){
                                                                $no++;
                                                                $hasiljsnya  = $offset;
                                                                              $active="";
                                                                                if($no==1){
                                                                                  $active="active";

                                                                                }

                                                                                        $checkedl ="";
                                                                                          if($row->aktivasi==1){
                                                                                            $checkedl ="checked";

                                                                                          }
                                                                          ?>
                                                                      <div class="tab-pane <?php echo $active; ?>" id="literasi-<?php echo $a; ?><?php echo $rm->id; ?>">
                                                                      

                                                                                                            <div class="table-responsive">
                                                                                                                        <table class="table table-bordered table-striped  " id="literasitabel<?php echo $a; ?><?php echo $rm->id; ?>" width="99%">
                                                                                                                            <thead class="bg-blue">
                                                                                                                                <tr>
                                                                                                                                    <th width="2px">NO</th>
                                                                                                                                    
                                                                                                                                    
                                                                                                                                    <th>NO TES </th>
                                                                                                                                    <th>TOKEN  </th>
                                                                                                                                    <th>PESERTA </th>
                                                                                                                                    <th>JURUSAN </th>
                                                                                                                                    <th>SESI </th>
                                                                                                                                    <th>NILAI </th>
                                                                                                                                  
                                                                                                                                  
                                                                                                                                    <th><?php echo $rm->matapelajaran; ?> </th>                                            
                                                                                                                                    
                                                                                                                                    
                                                                                                                                    
                                                                                                                                </tr>
                                                                                                                            </thead>
                                                                                                                          
                                                                                                                            <tbody>

                                                                                                                            <?php 


                                                                                                                        $siswa = $this->db->query("select * from tm_siswa where kategori IN(".$rm->tmjurusan_id.") and sesi='".$a."'  order by kategori asc,nama asc ")->result();
                                                                                                                                $nomor=1;
                                                                                                                              foreach($siswa as $rs){
                                                                                                                                $checked ="";
                                                                                                                              
                                                                                                                                
                                                                                                                              

                                                                                                                                $ujian = $this->db->query("select id,status,nilai from h_ujian  where kategori='".$rm->id."' and tmsiswa_id='".$rs->id."' and foto !=''")->row();
                                                                                                                                  $simulasi="-";
                                                                                                                                  $nilai   ="-";
                                                                                                                                if(!is_null($ujian)){
                                                                                                                                  
                                                                                                                                    $nilai    = $ujian->nilai; 
                                                                                                                                    $simulasi ="<a href='#' class='btn btn-success btn-sm camera' tmujian_id='".$ujian->id."' data-toggle='modal' data-target='#pelaksanaanmodal'><span class='fa fa-camera'></span> </a>";
                                                                                                                                }
                                                                                                                                
                                                                                                                            
                                                                                                                            

                                                                                                                              $bantuan = $this->db->query("select id from bantuan where tmsiswa_id='".$rs->id."' AND jawaban =''")->row();
                                                                                                                              $pertanyaan="";
                                                                                                                                if(!is_null($bantuan)){
                                                                                                                                  $pertanyaan ="<a href='#' class='btn btn-warning btn-sm bantuan' tmsiswa_id='".$rs->id."' data-toggle='modal' data-target='#pelaksanaanmodal'><span class='fa fa-reply'></span> </a>";
                                                                                                                            

                                                                                                                                }


                                                                                                                                ?>
                                                                                                                                  <tr>
                                                                                                                                    <td width="2px"><?php echo $nomor++; ?></td>
                                                                                                                                    
                                                                                                                                    
                                                                                                                                    <td><?php echo $rs->no_test; ?> </td>
                                                                                                                                    <td><?php echo $rs->token; ?> </td>
                                                                                                                                    <td><?php echo $rs->nama; ?> </td>
                                                                                                                                    <td><?php echo $this->Reff->get_kondisi(array("id"=>$rs->kategori),"tm_kategori","nama"); ?></td>
                                                                                                                                    <td><?php echo $rs->sesi; ?> </td>
                                                                                                                                    <td><?php echo number_format($nilai,2); ?>  </td>
                                                                                                                                    <td><?php echo $simulasi; ?> <?php echo $pertanyaan; ?> </td>
                                                                                                                                  
                                                                                                                                    
                                                                                                                                    
                                                                                                                                </tr>
                                                                                                                                <?php 



                                                                                                                              }



                                                                                                                              ?>
                                                                                                                                
                                                                                                                                
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                            </div>

                                                                                                            <script type="text/javascript">
                                                                                                            
                                                                                                    var dataTable = $('#literasitabel<?php echo $a; ?><?php echo $rm->id; ?>').DataTable( {
                                                                                                    
                                                                                                          "lengthMenu": [[20, 50,100,200,300,500,1000, 800000000], [20, 50,100,200,300,500,1000,"All"]],
                                                                                                          "dom": 'Blfrtip',
                                                                                                          "sPaginationType": "full_numbers",
                                                                                                          "buttons": [
                                                                                                          
                                                                                                                          
                                                                                                                {
                                                                                                              extend: 'excelHtml5',
                                                                                                              
                                                                                                              text:'Cetak Excel',
                                                                                                            
                                                                                                              },
                                                                                                              
                                                                                                              
                                                                                                              
                                                                                                              
                                                                                                            
                                                                                                              {
                                                                                                              extend: 'colvis',
                                                                                                              
                                                                                                              text:' Pengaturan Kolom ',
                                                                                                              
                                                                                                            }
                                                                                                          ]

                                                                                                    });


                                                                                                            </script>	

                                                                                                  <?php 
                                                                                                  
                                                                                                  $batas = $batas + $hasiljsnya; 
                                                                                                                  }
                                                                                                          ?>
                                                                                                  
                                                                                                  
                                                                                                  
                                                                                                  




                                                                                                        </div>
                                                                                                      </div>
                                                                                                  
                                                                                                  
                                                                                            </div>
                                
                                </div>
                    <?php 
                                  }
                                  ?>
                </div>
              </div>
              </div>
              </div>
            

                      
					
					
					
					
					
					
					
					
					
					
					
					
					
					
            
          
                    </div>
                </div>
              </div>
            </div>
        
  <div id="pelaksanaanmodal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body" id="loadbody">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">

$(document).off("click",".camera").on("click",".camera",function(){
	            
              var tmujian_id = $(this).attr("tmujian_id");
           
                $.post("<?php echo site_url('datasiswa/camera'); ?>",{tmujian_id:tmujian_id},function(data){
     
                 $("#loadbody").html(data);

                })
                 
               
             });
             
             $(document).off("click",".camerates").on("click",".camerates",function(){
	            
              var tmsiswa_id = $(this).attr("tmsiswa_id");
           
                $.post("<?php echo site_url('datasiswa/camerates'); ?>",{tmsiswa_id:tmsiswa_id},function(data){
     
                 $("#loadbody").html(data);

                })
                 
               
             });
             $(document).off("click",".bantuan").on("click",".bantuan",function(){
	            
              var tmsiswa_id = $(this).attr("tmsiswa_id");
           
                $.post("<?php echo site_url('datasiswa/bantuan'); ?>",{tmsiswa_id:tmsiswa_id},function(data){
     
                 $("#loadbody").html(data);

                })
                 
               
             });

$(document).off("click",".resetlogin").on("click",".resetlogin",function(){
	            
              var tmsiswa_id = $(this).attr("tmsiswa_id");
           
                $.post("<?php echo site_url('datasiswa/resetlogin'); ?>",{tmsiswa_id:tmsiswa_id},function(){
     
                  alertify.success("Login berhasil direset");
                  location.reload();
                })
                 
               
             });
    
 $(document).off("click",".aktivasi_literasi").on("click",".aktivasi_literasi",function(){
	            
              var literasi = $(this).attr("literasi");
              
              if($(this).is(":checked")){
                           var status =1;
                           var keterangan ="<b>Mengaktifkan</b>";
                      }else{
     
                          var status =0;
                          var keterangan ="<b>Menonaktifkan</b>";
               }

              alertify.confirm("Apakah Bu Yuna yakin "+keterangan+" seluruh peserta pada Literasi ini ?",function(){

             
     
                $.post("<?php echo site_url('pelaksanaan/aktivasi_literasi'); ?>",{literasi:literasi,status:status},function(){
     
                  alertify.success("Berhasil dilakukan perubahan aktivasi");
                  location.reload();
                })


              })
                 
               
   });


  </script>