<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 
		  isLogin();
		  $this->load->model('M_referensi','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('admin/page_header',$data);	
	}
		
	public function data($param)
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['data']	  = $this->db->get_where("groups",array("id"=>base64_decode($param)))->row();
		 $data['title']   = $data['data']->nama;
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			    $pegawai = $this->db->get_where("pegawai",array("id"=>$val['pegawai_id']))->row();
			   
				$no = $i++;
				$records["data"][] = array(
					$no,
			
              	
					$this->Reff->get_kondisi(array("id"=>$pegawai->unitkerja_id),"unit_kerja","nama"),
					$val['nama'],
					$this->Reff->get_kondisi(array("id"=>$val['group_id']),"groups","nama"),
					$val['username'],
					
					' 
					<div class="btn-group" role="group">
					<button type="button" class="btn btn-outline-primary ubah" datanya="'.$val['id'].'" urlnya="'.site_url("users/form").'" >
                                    <i class="fa fa-pencil"></i>
                      </button>
					  <button type="button"  class="btn btn-outline-danger btn-sm hapus" datanya="'.$val['id'].'" urlnya="'.site_url("users/hapus").'">
                                    <i class="fa fa-trash"></i>
					  </button>
					  </div>
					 
					  '
				  
				    

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	

	

	public function form(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("users",array("id"=>$id));
			   
		   }
		$this->load->view("form",$data);
		
	}
	
	
	
	public function save(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
			    $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
    
		 
			
				$config = array(
				    
				   
				    array('field' => 'f[nama]', 'label' => 'Nama Users ', 'rules' => 'trim|required'),
				    array('field' => 'f[username]', 'label' => 'Username   ', 'rules' => 'trim|required'),
				    array('field' => 'f[group_id]', 'label' => 'Groups   ', 'rules' => 'trim|required'),
				  	   
			
				   
				  
				   
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
			    $password  = $this->input->get_post("password",true);
				
				
				 
				
							 if(empty($id)){
								
								 
								   $this->db->set("password",sha1(md5($password)));
								   $this->db->insert("users",$f);
								   echo "Data Berhasil disimpan";	
								 
								
								
							 }else{
								 if(!empty($password)){
								$this->db->set("password",sha1(md5($password)));
								 }
								$this->db->where("id",$id);
								$this->db->update("users",$f);
								echo "Data Berhasil disimpan";	
								 
							 }
							 
						
							
						
		
		
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}

	public function hapus(){
		
		$id = $this->input->get_post("id",true);
		
		$this->db->delete("users",array("id"=>$id));
		echo "sukses";
	}
	

	 
}
