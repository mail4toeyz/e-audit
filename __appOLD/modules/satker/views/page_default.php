<main id="main" class="main">

<div class="pagetitle">
  <h1>Dashboard</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.html">Home</a></li>
      <li class="breadcrumb-item active">Dashboard</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section dashboard">
  <div class="row">

    <!-- Left side columns -->
    <div class="col-lg-8">
      <div class="row">

        <!-- Sales Card -->
        <div class="col-xxl-4 col-md-6">
          <div class="card info-card sales-card">

            <div class="filter">
              <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
              <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                <li class="dropdown-header text-start">
                  <h6>Filter</h6>
                </li>

                <li><a class="dropdown-item" href="#">Today</a></li>
                <li><a class="dropdown-item" href="#">This Month</a></li>
                <li><a class="dropdown-item" href="#">This Year</a></li>
              </ul>
            </div>

            <div class="card-body">
              <h5 class="card-title">Auditor  <span></span></h5>

              <div class="d-flex align-items-center">
                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                  <i class="bi bi-people"></i>
                </div>
                <div class="ps-3">
                  <h6>

                   <?php 
                      $kegiatan    = $this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
                      $anggota    = json_decode($kegiatan->anggota,true);
                      echo  count($anggota);
                    ?>
                  </h6>
                
                </div>
              </div>
            </div>

          </div>
        </div><!-- End Sales Card -->

        <!-- Revenue Card -->
        <div class="col-xxl-4 col-md-6">
          <div class="card info-card revenue-card">

            <div class="filter">
              <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
              <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                <li class="dropdown-header text-start">
                  <h6>Filter</h6>
                </li>

                <li><a class="dropdown-item" href="#">Today</a></li>
                <li><a class="dropdown-item" href="#">This Month</a></li>
                <li><a class="dropdown-item" href="#">This Year</a></li>
              </ul>
            </div>

            <div class="card-body">
              <h5 class="card-title">Permintaan Dokumen</span></h5>

              <div class="d-flex align-items-center">
                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                  <i class="bi bi-file"></i>
                </div>
                <div class="ps-3">
                  <h6>

                   <?php 
                  echo $this->db->get_where("tr_persyaratan",array("kegiatan_id"=>$_SESSION['kegiatan_id']))->num_rows();
                   ?>
                  </h6>
                
                </div>
              </div>
            </div>

          </div>
        </div><!-- End Revenue Card -->

        <!-- Customers Card -->
        <div class="col-xxl-4 col-xl-12">

          <div class="card info-card customers-card">

            <div class="filter">
              <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
              <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                <li class="dropdown-header text-start">
                  <h6>Filter</h6>
                </li>

                <li><a class="dropdown-item" href="#">Today</a></li>
                <li><a class="dropdown-item" href="#">This Month</a></li>
                <li><a class="dropdown-item" href="#">This Year</a></li>
              </ul>
            </div>

            <div class="card-body">
              <h5 class="card-title">Video Conference</span></h5>

              <div class="d-flex align-items-center">
                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                  <i class="bi bi-people"></i>
                </div>
                <div class="ps-3">
                  <h6>

                  <?php 
                  echo $this->db->get_where("tr_vicon",array("kegiatan_id"=>$_SESSION['kegiatan_id']))->num_rows();
                   ?>

                  </h6>
                  
                </div>
              </div>

            </div>
          </div>

        </div><!-- End Customers Card -->

        <!-- Reports -->
        <div class="col-12">
          <div class="card">

            <div class="filter">
              <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
              <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                <li class="dropdown-header text-start">
                  <h6>Filter</h6>
                </li>

                <li><a class="dropdown-item" href="#">Today</a></li>
                <li><a class="dropdown-item" href="#">This Month</a></li>
                <li><a class="dropdown-item" href="#">This Year</a></li>
              </ul>
            </div>

            <div class="card-body">
            <h5 class="card-title">Data Kegiatan </h5>

            <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
              <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Kegiatan  </button>
             
            </li>
              <li class="nav-item" role="presentation">
                  <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Petugas  </button>
                </li>
              
          
          </ul>
          <div class="tab-content pt-2" id="myTabContent">
            <?php 
              $kegiatan    = $this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
              if(!is_null($kegiatan)){

             ?>
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
               <table class="table">
                   <tr>
                       <td> Jenis Kegiatan </td>
                       <td> <?php echo  $this->Reff->get_kondisi(array("id"=>$kegiatan->jenis),"tm_jenis","nama"); ?> </td>

                   </tr>

                   <tr>
                       <td> Judul Kegiatan </td>
                       <td> <?php echo  $kegiatan->judul; ?> </td>

                   </tr>

                   <tr>
                       <td> Provinsi </td>
                       <td> <?php echo  $this->Reff->get_kondisi(array("id"=>$kegiatan->provinsi_id),"provinsi","nama"); ?> </td>

                   </tr>

                   <tr>
                       <td> Kabupaten/Kota </td>
                       <td> <?php echo  $this->Reff->get_kondisi(array("id"=>$kegiatan->kota_id),"kota","nama"); ?> </td>

                   </tr>
                   <tr>
                       <td> Satuan Kerja  </td>
                       <td> <?php echo  $this->Reff->get_kondisi(array("id"=>$kegiatan->satker_id),"satker","nama"); ?> </td>

                   </tr>
                   <tr>
                       <td> Tanggal Mulai Kegiatan  </td>
                       <td> <?php echo  $this->Reff->formattanggalstring($kegiatan->tgl_mulai); ?> </td>

                   </tr>
                   <tr>
                       <td> Tanggal Selesai Kegiatan  </td>
                       <td> <?php echo  $this->Reff->formattanggalstring($kegiatan->tgl_selesai); ?> </td>

                   </tr>
               </table>
            </div>



                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                <table class="table">
                   <thead>
                      <tr>
                       <th> Nama </th>
                       <th> NIP </th>
                       <th> Jabatan </th>
                       <th> Jumlah Hari </th>
                     </tr>

                   </thead>
                   <tbody>
                     <tr>
                       <td> <?php echo $this->Reff->get_kondisi(array("id"=>$kegiatan->penanggung_jawab),"sekertariat","nama"); ?></td>
                       <td> <?php echo $this->Reff->get_kondisi(array("id"=>$kegiatan->penanggung_jawab),"sekertariat","nip"); ?> </td>
                       <td> Penanggung Jawab </td>
                       <td>
                        <?php echo $this->Reff->get_kondisi(array("kegiatan_id"=>$kegiatan->id,"jabatan"=>1,"pejabat_id"=>$kegiatan->penanggung_jawab),"tr_kegiatanJabatan","waktu");
                        ?> Hari 
                         
                       
                    
                    </td>
                     </tr>

                     <tr>
                     <td> <?php echo $this->Reff->get_kondisi(array("id"=>$kegiatan->pengendali_mutu),"sekertariat","nama"); ?></td>
                       <td> <?php echo $this->Reff->get_kondisi(array("id"=>$kegiatan->pengendali_mutu),"sekertariat","nip"); ?> </td>
                       <td> Pengendali Mutu </td>
                       <td>

                       <?php echo $this->Reff->get_kondisi(array("kegiatan_id"=>$kegiatan->id,"jabatan"=>2,"pejabat_id"=>$kegiatan->pengendali_mutu),"tr_kegiatanJabatan","waktu");
                        ?> Hari 
                       </td>
                     </tr>

                     <tr>
                     <td> <?php echo $this->Reff->get_kondisi(array("id"=>$kegiatan->pengendali_teknis),"sekertariat","nama"); ?></td>
                       <td> <?php echo $this->Reff->get_kondisi(array("id"=>$kegiatan->pengendali_teknis),"sekertariat","nip"); ?> </td>
                       <td> Pengendali Teknis </td>
                       <td>

                       <?php echo $this->Reff->get_kondisi(array("kegiatan_id"=>$kegiatan->id,"jabatan"=>3,"pejabat_id"=>$kegiatan->pengendali_teknis),"tr_kegiatanJabatan","waktu");
                        ?> Hari 

                       </td>
                     </tr>

                     <tr>
                     <td> <?php echo $this->Reff->get_kondisi(array("id"=>$kegiatan->ketua),"auditor","nama"); ?></td>
                       <td> <?php echo $this->Reff->get_kondisi(array("id"=>$kegiatan->ketua),"auditor","nip"); ?> </td>
                       <td> Ketua TIM </td>
                       <td>

                         
                       <?php echo $this->Reff->get_kondisi(array("kegiatan_id"=>$kegiatan->id,"jabatan"=>4,"pejabat_id"=>$kegiatan->ketua),"tr_kegiatanJabatan","waktu");
                        ?> Hari 

                       </td>
                     </tr>
                     <?php 
                       $anggota = json_decode($kegiatan->anggota,true);
                        foreach($anggota as $r){
                          ?>
                          <tr>
                            <td><?php echo $this->Reff->get_kondisi(array("id"=>$r),"auditor","nama"); ?> </td>
                            <td><?php echo $this->Reff->get_kondisi(array("id"=>$r),"auditor","nip"); ?></td>
                            <td> Anggota </td>
                            <td>


                            <?php echo $this->Reff->get_kondisi(array("kegiatan_id"=>$kegiatan->id,"jabatan"=>5,"pejabat_id"=>$r),"tr_kegiatanJabatan","waktu");
                        ?> Hari 


                            </td>
                          </tr>
                     <?php 


                        }
                        ?>
                     
                   </tbody>
                 </table>
            </div>

            <?php 
              }else{

                ?> Tidak ada kegiatan yang dipilih <?php 
              }

              ?>
            </div>
           
            

            </div>

          </div>
        </div><!-- End Reports -->

  
      </div>
    </div><!-- End Left side columns -->

    <!-- Right side columns -->
    <div class="col-lg-4">

      <!-- Recent Activity -->
      
      <!-- Budget Report -->
      <div class="card">
        <div class="filter">
          <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
          <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
            <li class="dropdown-header text-start">
              <h6>Filter</h6>
            </li>

            <li><a class="dropdown-item" href="#">Today</a></li>
            <li><a class="dropdown-item" href="#">This Month</a></li>
            <li><a class="dropdown-item" href="#">This Year</a></li>
          </ul>
        </div>

        <div class="card-body pb-0">
          <h5 class="card-title">Audit Report <span></span></h5>

          <div id="budgetChart" style="min-height: 400px;" class="echart"></div>

          <script>
            document.addEventListener("DOMContentLoaded", () => {
              var budgetChart = echarts.init(document.querySelector("#budgetChart")).setOption({
                legend: {
                  data: ['Allocated Budget', 'Actual Spending']
                },
                radar: {
                  // shape: 'circle',
                  indicator: [{
                      name: 'Audit',
                      max: 6500
                    },
                    {
                      name: 'Review',
                      max: 16000
                    },
                    {
                      name: 'Pelaksanaan',
                      max: 30000
                    },
                    {
                      name: 'Pengawasan',
                      max: 38000
                    },
                    {
                      name: 'Pembiayaan',
                      max: 52000
                    },
                    {
                      name: 'Notulasi',
                      max: 25000
                    }
                  ]
                },
                series: [{
                  name: 'Budget vs spending',
                  type: 'radar',
                  data: [{
                      value: [4200, 3000, 20000, 35000, 50000, 18000],
                      name: 'Allocated Budget'
                    },
                    {
                      value: [5000, 14000, 28000, 26000, 42000, 21000],
                      name: 'Actual Spending'
                    }
                  ]
                }]
              });
            });
          </script>

        </div>
      </div><!-- End Budget Report -->

    </div><!-- End Right side columns -->

  </div>
</section>

</main>