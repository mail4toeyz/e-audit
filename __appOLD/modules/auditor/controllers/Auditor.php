<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auditor extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		  if(!$this->session->userdata("is_login")){
			    
				echo $this->Reff->sessionhabis();
				exit();
			
		  }
		  $this->load->model('M_dashboard','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('auditor/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Dashboard  ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page_default',$data);
		
		 }else{
			 
			
		     $data['konten'] = "page_default";
			 
			 $this->_template($data);
		 }
	

	}
	
	 
	 public function grid(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		  $role = array("1"=>"Administrator Pusat","2"=>"Madrasah","3"=>"Peserta");
		  
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			    
			   
				$no = $i++;
				$records["data"][] = array(
					$no,
			
              	
					
					$role[$val['status']],
					$val['keterangan'],
					$this->Reff->timeAgo($val['tanggal']),
					
		
				   
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	
	public function setKegiatan(){
       $kegiatan_id = $_POST['kegiatan_id'];
       $auditor    = $this->db->get_where("auditor",array("id"=>$_SESSION['idAuditor']))->row();
		$session = array(
			'idAuditor'        => $auditor->id,
			'kegiatan_id'        => $kegiatan_id,
			'nama'        => $auditor->nama,
			'group_id'        => "Auditor",
			'is_login'        => true,
			'date_login'   => date("Y-m-d H:i:s"));
			
			$this->session->sess_expiration = 0;
			$this->session->set_userdata($session);

			


	}

	public function laporan()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['kegiatan'] =$this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
		 $data['title']    = $data['kegiatan']->judul;
	     if(!empty($ajax)){
					    
			 $this->load->view('page_laporan',$data);
		
		 }else{
			 
			
		     $data['konten'] = "page_laporan";
			 
			 $this->_template($data);
		 }
	

	}
	
 
	
	public function logout(){
		     $this->session->sess_destroy();
			 
		
			echo "yes";
		 
	 }

	 public function generate_skor(){


		$h_ujian = $this->db->query("SELECT * FROM `h_ujian` where tmujian_id=25")->result();

		 foreach($h_ujian as $row){

		  $list_jawaban = $row->list_jawaban;
		  
		  $pc_jawaban = explode(",", $list_jawaban);
		  
		  $jumlah_benar 	= 0;
		  $jumlah_salah 	= 0;
		  $jumlah_ragu  	= 0;
		  $nilai_bobot 	= 0;
		  $total_bobot	= 0;
		  $jumlah_soal	= sizeof($pc_jawaban);

		  $arab_benar            = 0;
		  $inggris_benar          = 0;
		  $islam_benar      = 0;
		  
		  $arab_salah            = 0;
		  $inggris_salah          = 0;
		  $islam_salah      = 0;
		 

		  $arab            = 0;
		  $inggris          = 0;
		  $islam           = 0;
		  


  
		  foreach ($pc_jawaban as $jwb) {

			  $pc_dt 		= explode(":", $jwb);
			  $id_soal 	= $pc_dt[0];
			  $jawaban 	= $pc_dt[1];
			  $ragu 		= $pc_dt[2];
  
			  $cek_jwb 	     = $this->m->getSoalById($id_soal);
			  $cek_kategori 	 = $cek_jwb->paket; 
			 
			  
			  if($cek_jwb->jenis==1){
  
				  if($cek_jwb->jawaban==$jawaban){
					  $jumlah_benar++;
					  $nilai_bobot = $nilai_bobot + $cek_jwb->bobot;
					  $total_bobot = $total_bobot + $cek_jwb->bobot;

					  if($cek_kategori==3){

							$arab = $arab + $cek_jwb->bobot;
							$arab_benar++;
						  
					  }else if($cek_kategori==2){

							$inggris = $inggris + $cek_jwb->bobot;
							$inggris_benar++;

					  }else if($cek_kategori==1){

							$islam = $islam + $cek_jwb->bobot;
							$islam_benar++;
					  }


				  }else{
					  $jumlah_salah++;

					   if($cek_kategori==3){

						  $arab_salah++;
						  
						}else if($cek_kategori==2){

							$inggris_salah++;
						}else if($cek_kategori==1){

							$islam_salah++;
						}


				  } 
  
			  }
			  
		  }
         
		  echo $this->Reff->get_kondisi(array("id"=>$row->tmsiswa_id),"tm_siswa","nama")."-".$islam_benar."-".$inggris."-".$islam."<br>"; 

		$cekData = $this->db->get_where("tr_skor",array("tmsiswa_id"=>$row->tmsiswa_id))->num_rows();
		  if($cekData==0){

			$nilai_bobot 	= ($islam+$inggris+$arab);

			$this->db->set("tmsiswa_id",$row->tmsiswa_id);			
			$this->db->set("islam_benar",$islam_benar);
			$this->db->set("islam_salah",$islam_salah);
			$this->db->set("islam",$islam);
			$this->db->set("inggris_benar",$inggris_benar);
			$this->db->set("inggris_salah",$inggris_salah);
			$this->db->set("inggris",$inggris);
			$this->db->set("arab_benar",$arab_benar);
			$this->db->set("arab_salah",$arab_salah);
			$this->db->set("arab",$arab);
			$this->db->set("skor_mapel",$nilai_bobot);
			$this->db->insert("tr_skor");
			

		  }else{

			$nilai_bobot 	= ($islam+$inggris+$arab);

			$this->db->where("tmsiswa_id",$row->tmsiswa_id);			
			$this->db->set("islam_benar",$islam_benar);
			$this->db->set("islam_salah",$islam_salah);
			$this->db->set("islam",$islam);
			$this->db->set("inggris_benar",$inggris_benar);
			$this->db->set("inggris_salah",$inggris_salah);
			$this->db->set("inggris",$inggris);
			$this->db->set("arab_benar",$arab_benar);
			$this->db->set("arab_salah",$arab_salah);
			$this->db->set("arab",$arab);
			$this->db->set("skor_mapel",$nilai_bobot);
			$this->db->update("tr_skor");


		  }


		  //$nilai 			= ($jumlah_benar / ($jumlah_soal))  * 100;
		  //$nilai_bobot 	= ($total_bobot / ($jumlah_soal))  * 100;

		//   $andragogi  = number_format($andragogi/10,1);
		//   $skor = $edm + $erkam +  $bos + $andragogi;
  
		//   $d_update = [
		// 	  'edm_benar'		=> $edm_benar,
		// 	  'erkam_benar'		=> $erkam_benar,
		// 	  'bos_benar'		=> $bos_benar,
		// 	  'andragogi_benar'		=> $andragogi_benar,
		// 	  'edm_salah'		=> $edm_salah,
		// 	  'erkam_salah'		=> $erkam_salah,
		// 	  'bos_salah'		=> $bos_salah,
		// 	  'andragogi_salah'		=> $andragogi_salah,
		// 	  'edm'		=> $edm,
		// 	  'erkam'		=> $erkam,
		// 	  'bos'		=> $bos,
		// 	  'andragogi'		=> $andragogi,
		// 	  'skor'		=> $skor,
		// 	  'nilai'		=> $skor,
		// 	  'nilai_bobot'		=> $skor
			  
			  
		//   ];
		  
		//   $this->db->update('h_ujian', $d_update,array("id"=>$row->id));

		}

	  echo "sukses";
  


	 }

	 public function generate_skor_kompetensi(){


		$h_ujian = $this->db->query("SELECT * FROM `h_ujian` where tmujian_id=26")->result();

		 foreach($h_ujian as $row){

		  $list_jawaban = $row->list_jawaban;
		  
		  $pc_jawaban = explode(",", $list_jawaban);
		  
		  $jumlah_benar 	= 0;
		  $jumlah_salah 	= 0;
		  $jumlah_ragu  	= 0;
		  $nilai_bobot 	= 0;
		  $total_bobot	= 0;
		  $jumlah_soal	= sizeof($pc_jawaban);

		  $kompetensi_benar            = 0;
		 
		 
		  $kompetensi_salah            = 0;
		 

		  $kompetensi            = 0;
		 


  
		  foreach ($pc_jawaban as $jwb) {

			  $pc_dt 		= explode(":", $jwb);
			  $id_soal 	= $pc_dt[0];
			  $jawaban 	= $pc_dt[1];
			  $ragu 		= $pc_dt[2];
  
			  $cek_jwb 	     = $this->m->getSoalById($id_soal);
			  $cek_kategori 	 = $cek_jwb->paket; 
			 
			  
			  if($cek_jwb->jenis==1){
  
				  if($cek_jwb->jawaban==$jawaban){
					  $jumlah_benar++;
					  $nilai_bobot = $nilai_bobot + $cek_jwb->bobot;
					  $total_bobot = $total_bobot + $cek_jwb->bobot;

					  if($cek_kategori==4){

							$kompetensi = $kompetensi + $cek_jwb->bobot;
							$kompetensi_benar++;
						  
					  }


				  }else{
					  $jumlah_salah++;

					   if($cek_kategori==4){

						  $kompetensi_salah++;
						  
						}


				  } 
  
			  }
			  
		  }
         
		 // echo $this->Reff->get_kondisi(array("id"=>$row->tmsiswa_id),"tm_siswa","nama")."-".$islam_benar."-".$inggris."-".$islam."<br>"; 


			

			$this->db->where("tmsiswa_id",$row->tmsiswa_id);			
			$this->db->set("kompetensi_benar",$kompetensi_benar);
			$this->db->set("kompetensi_salah",$kompetensi_salah);
			$this->db->set("kompetensi",$kompetensi);
		
			$this->db->update("tr_skor");


		  


		  //$nilai 			= ($jumlah_benar / ($jumlah_soal))  * 100;
		  //$nilai_bobot 	= ($total_bobot / ($jumlah_soal))  * 100;

		//   $andragogi  = number_format($andragogi/10,1);
		//   $skor = $edm + $erkam +  $bos + $andragogi;
  
		//   $d_update = [
		// 	  'edm_benar'		=> $edm_benar,
		// 	  'erkam_benar'		=> $erkam_benar,
		// 	  'bos_benar'		=> $bos_benar,
		// 	  'andragogi_benar'		=> $andragogi_benar,
		// 	  'edm_salah'		=> $edm_salah,
		// 	  'erkam_salah'		=> $erkam_salah,
		// 	  'bos_salah'		=> $bos_salah,
		// 	  'andragogi_salah'		=> $andragogi_salah,
		// 	  'edm'		=> $edm,
		// 	  'erkam'		=> $erkam,
		// 	  'bos'		=> $bos,
		// 	  'andragogi'		=> $andragogi,
		// 	  'skor'		=> $skor,
		// 	  'nilai'		=> $skor,
		// 	  'nilai_bobot'		=> $skor
			  
			  
		//   ];
		  
		//   $this->db->update('h_ujian', $d_update,array("id"=>$row->id));

		}

	  echo "sukses";
  


	 }


	 public function generate_skor_moderasi(){


		$h_ujian = $this->db->query("SELECT * FROM `h_ujian` where tmujian_id=26")->result();

		 foreach($h_ujian as $row){

		  $list_jawaban = $row->list_jawaban;
		  
		  $pc_jawaban = explode(",", $list_jawaban);
		  
		  $jumlah_benar 	= 0;
		  $jumlah_salah 	= 0;
		  $jumlah_ragu  	= 0;
		  $nilai_bobot 	= 0;
		  $total_bobot	= 0;
		  $jumlah_soal	= sizeof($pc_jawaban);

		  $moderasi1            = 0;
		  $moderasi2            = 0;
		  $moderasi3            = 0;
		  $moderasi4            = 0;
		  $moderasi5            = 0;
		 
		 
		 


  
		  foreach ($pc_jawaban as $jwb) {

			  $pc_dt 		= explode(":", $jwb);
			  $id_soal 	= $pc_dt[0];
			  $jawaban 	= $pc_dt[1];
			  $ragu 		= $pc_dt[2];
  
			  $cek_jwb 	     = $this->m->getSoalById($id_soal);
			  $cek_kategori 	 = $cek_jwb->paket; 
			  if($cek_kategori==5){
			 

					if($jawaban=="A"){
						$moderasi1++;
					}else if($jawaban=="B"){
						$moderasi2++;
					}else if($jawaban=="C"){
						$moderasi3++;
						
					}else if($jawaban=="D"){
						$moderasi4++;
					}else if($jawaban=="E"){
						$moderasi5++;
					}
					
					
		
					$arr_nilai = array("A"=>"1","B"=>"2","C"=>"3","D"=>"4","E"=>"5");
					$nilai_bobot = $nilai_bobot + $arr_nilai[$jawaban];

			}
			//  $total_bobot = $total_bobot + $cek_jwb->bobot;
  
			  
			  
		  }
         
		 // echo $this->Reff->get_kondisi(array("id"=>$row->tmsiswa_id),"tm_siswa","nama")."-".$islam_benar."-".$inggris."-".$islam."<br>"; 


		   $cekData    = $this->db->get_where("tr_skor",array("tmsiswa_id"=>$row->tmsiswa_id))->row();
		   $skor_kamad = $cekData->kompetensi+$nilai_bobot;
			$this->db->where("tmsiswa_id",$row->tmsiswa_id);			
			$this->db->set("moderasi1",$moderasi1);
			$this->db->set("moderasi2",$moderasi2);
			$this->db->set("moderasi3",$moderasi3);
			$this->db->set("moderasi4",$moderasi4);
			$this->db->set("moderasi5",$moderasi5);
			$this->db->set("moderasi",$nilai_bobot);
			$this->db->set("skor_kamad",$skor_kamad);
		
			$this->db->update("tr_skor");


		  


		  //$nilai 			= ($jumlah_benar / ($jumlah_soal))  * 100;
		  //$nilai_bobot 	= ($total_bobot / ($jumlah_soal))  * 100;

		//   $andragogi  = number_format($andragogi/10,1);
		//   $skor = $edm + $erkam +  $bos + $andragogi;
  
		//   $d_update = [
		// 	  'edm_benar'		=> $edm_benar,
		// 	  'erkam_benar'		=> $erkam_benar,
		// 	  'bos_benar'		=> $bos_benar,
		// 	  'andragogi_benar'		=> $andragogi_benar,
		// 	  'edm_salah'		=> $edm_salah,
		// 	  'erkam_salah'		=> $erkam_salah,
		// 	  'bos_salah'		=> $bos_salah,
		// 	  'andragogi_salah'		=> $andragogi_salah,
		// 	  'edm'		=> $edm,
		// 	  'erkam'		=> $erkam,
		// 	  'bos'		=> $bos,
		// 	  'andragogi'		=> $andragogi,
		// 	  'skor'		=> $skor,
		// 	  'nilai'		=> $skor,
		// 	  'nilai_bobot'		=> $skor
			  
			  
		//   ];
		  
		//   $this->db->update('h_ujian', $d_update,array("id"=>$row->id));

		}

	  echo "sukses";
  


	 }


	public function generate_nilai(){

		$cekData    = $this->db->get("tr_skor")->result();
		  foreach($cekData as $r){
			  $nilai = $r->skor_mapel+$r->skor_kamad;
			  $this->db->set("nilai",$nilai);
			  $this->db->where("id",$r->id);
			  $this->db->update("tr_skor");

			  $this->db->set("nilai",$nilai);
			  $this->db->where("id",$r->tmsiswa_id);
			  $this->db->update("tm_siswa");


		  }
	}

	public function generate_peringkat(){

		$kelas = $this->db->query("SELECT * from madrasah_peminatan where id_admin IN(select madrasah_peminatan from tm_siswa)")->result();
		foreach($kelas as $i=>$r){

			 $siswa = $this->db->query("SELECT * from tm_siswa where madrasah_peminatan='{$r->id_admin}' order by nilai DESC")->result();
			 $peringkat=0;
			   foreach($siswa as $rs){
				   $peringkat++;
				   $this->db->set("peringkat",$peringkat);
				   $this->db->where("id",$rs->id);
				   $this->db->update("tm_siswa");


			   }


		}


	}


}
