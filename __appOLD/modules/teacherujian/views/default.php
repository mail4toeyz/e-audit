

<div class="col-lg-9 col-md-9 ">
							<div class="tab-content" id="nav-tabContent">
							
						
								
								<!-- Pertanyaan -->
								<div class="tab-pane fade show  active" id="pertanyaan" role="tabpanel" aria-labelledby="nav-acc-tab">
				
										<?php $this->load->view("teacherujian/pertanyaan"); ?>
							    </div>
								
								
								<div class="tab-pane fade show " id="importsoal" role="tabpanel" aria-labelledby="nav-acc-tab">
				


										<div class="acc-setting">
										<h3 id="titleujian">Import Soal </h3>
										<form method="post" action="<?php echo site_url("teacherujian/import_excel"); ?>" enctype="multipart/form-data">
										<input type="hidden" value="<?php echo $tmujian_id ?>" name="tmujian_id">
										<div class="alert alert-info">
										Import Soal melalui template yang sudah disediakan dibawah ini :
										
										<a class="btn btn-info btn-sm" href="<?php echo base_url(); ?>__statics/soal.xlsx"> Download Template </a> 
										
										
										</div>
										

										<div class="cp-field">
												<h5> Pilih File  </h5>
												<div class="cpp-fiel">
													 <input type="file" name="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required />
												</div>
											</div>
											
									  
									  	<div class="cp-field">
												
												<div class="cpp-fiel">
													<center> <button type="submit" class="btn btn-info btn-sm"> Import Soal </button> </center>
												</div>
											</div>
											
											
											
											
											</form>
											
											<br>
											<br>
											<br>
											<br>
											
											<hr>
										</div>
										
										
											
							    </div>
									
									
							


									<div class="tab-pane fade show " id="importsoalpdf" role="tabpanel" aria-labelledby="nav-acc-tab">
				


										<div class="acc-setting">
										<h3 id="titleujian">Import Soal PDF </h3>
										<form method="post" action="<?php echo site_url("teacherujian/import_pdf"); ?>" enctype="multipart/form-data">
										<input type="hidden" value="<?php echo $tmujian_id ?>" name="tmujian_id">
										
										
										<div class="alert alert-info">
										Anda dapat mengimport soal berbentuk pdf, jika ada soal yang mengandung gambar, silahkan dapat dilengkapi pada menu buat soal ujian
										
									
										</div>
										<div class="cp-field">
												<h5> Delimeter  </h5>
												<div class="cpp-fiel">
												<input type="text" class="form-control" placeholder="Kalimat terakhir sebelum soal" required  name="delimeter">
												</div>
											</div>
										<div class="cp-field">
												<h5> Jumlah Soal  </h5>
												<div class="cpp-fiel">
												<input type="number" class="form-control" placeholder="Jumlah Soal" required  name="jml_soal">
												</div>
											</div>
											
										
										  <div class="cp-field">
												<h5> Pilih File  </h5>
												<div class="cpp-fiel">
													 <input type="file" name="file" accept="application/pdf" required />
												</div>
											</div>
											
									  
									  	<div class="cp-field">
												
												<div class="cpp-fiel">
													<center> <button type="submit" class="btn btn-info btn-sm"> Import Soal </button> </center>
												</div>
											</div>
											
											
											
											
											</form>
											
											<br>
											<br>
											<br>
											<br>
											
											<hr>
										</div>
										
										
											
							    </div>

								<div class="tab-pane fade show " id="banksoal" role="tabpanel" aria-labelledby="nav-acc-tab">
                                <?php $this->load->view("banksoal"); ?>

								</div>
											
   

									
									
									
	  </div>
</div>


<script type="text/javascript">
   $(document).off("click",".simpansemuaujian").on("click",".simpansemuaujian",function(){
	  
	   var tmujian_id      = $(this).attr("ujian-id");
	   var trkelas_id      = "4";
	   var jenis_tes      = $("#jenis_tes").val();
	   var kd             = "";
	   
	   var nama_tes       = $("#nama_tes").val();
	   var mulai       = $("#mulai").val();
	   alertify.confirm("Apakah Anda yakin menyimpan CBT ini ? ",function(){
	    if(nama_tes=="" || jenis_tes ==""){
			
			           alertify.alert("Proses penyimapanan gagal,Silahakan isi semua kolom pada Jenis Tes ");
					  return false;
		}
	   
	   
	    if(jenis_tes==1){
			   var kd       = $("#kd").val();
			      if(kd==""){
					  
					  alertify.alert("Proses penyimpanan gagal,Silahkan pilih kompetensi Dasar ");
					  return false;
				  }
		   
		      $("#statussoal").html("<i class='fas fa-cog fa-spin'></i>    Sedang menyimpan..");
			  $.post("<?php echo site_url("teacherujian/simpan_ujian_semua"); ?>",{tmujian_id:tmujian_id,trkelas_id:trkelas_id,nama_tes:nama_tes,jenis_tes:jenis_tes,kd:kd,mulai:mulai},function(){
				  
				  alertify.alert("Data berhasil disimpan");
				  location.href= base_url+"banksoal/buatsoal";
			  })
		}else{
			 $("#statussoal").html("<i class='fas fa-cog fa-spin'></i>    Sedang menyimpan..");
			  $.post("<?php echo site_url("teacherujian/simpan_ujian_semua"); ?>",{tmujian_id:tmujian_id,trkelas_id:trkelas_id,nama_tes:nama_tes,jenis_tes:jenis_tes,kd:kd,mulai:mulai},function(){
				  
				location.href= base_url+"banksoal/buatsoal";
			  })
			
		}
	   })
	  
  });
  
  
  
  $(document).off("input",".ketikujian").on("input",".ketikujian",function(){
	  
	   var tmujian_id = $(this).attr("ujian-id");
	   var kolom      = $(this).attr("kolom");
	   var nilai      = $(this).val();
	   $("#statussoal").html("<i class='fas fa-cog fa-spin'></i>    Sedang menyimpan..");
	      $.post("<?php echo site_url("teacherujian/simpan_ujian"); ?>",{tmujian_id:tmujian_id,kolom:kolom,nilai:nilai},function(){
			  
			  
			  
			  $("#statussoal").html("Tesimpan");
		  })
	  
	  
	  
  });
  
  
   $(document).off("change",".changesoal").on("change",".changesoal",function(){
	  
	   var tmujian_id = $(this).attr("ujian-id");
	   var kolom      = $(this).attr("kolom");
	   var nilai      = $(this).val();
	  $("#statussoal").html("<i class='fas fa-cog fa-spin'></i>    Sedang menyimpan..");
	      $.post("<?php echo site_url("teacherujian/simpan_ujian"); ?>",{tmujian_id:tmujian_id,kolom:kolom,nilai:nilai},function(){
			  
			  
			  
			  $("#statussoal").html("Tesimpan");
		  })
	  
	  
	  
   });
   
    $(document).off("change",".jenistes").on("change",".jenistes",function(){
	  
	  
	   var nilai      = $(this).val();
	  if(nilai==1){
		  
		  $("#kompetensidasar").show();
	  }else{
		  
		   $("#kompetensidasar").hide();
	  }
	  
	  
	  
   });
   
    $(document).off("click","#jenisujian").on("click","#jenisujian",function(){
	  var nilai = $(this).is(":checked");
	 
	   var tmujian_id = $(this).attr("ujian-id");
	   var kolom      = "jenis";
	    if(nilai){
			
			nilai ="acak";
			
		}else{
			
			nilai ="urut";
		}
		
	   $("#statussoal").html("<i class='fas fa-cog fa-spin'></i>    Sedang menyimpan..");
	      $.post("<?php echo site_url("teacherujian/simpan_ujian"); ?>",{tmujian_id:tmujian_id,kolom:kolom,nilai:nilai},function(){
			  
			  
			  
			  $("#statussoal").html("Tesimpan");
		  })
	  
	  
	  
   });
   
    $(document).off("click","#publishujian").on("click","#publishujian",function(){
	  var nilai = $(this).is(":checked");
	 
	   var tmujian_id = $(this).attr("ujian-id");
	   var kolom      = "publish";
	    if(nilai){
			
			nilai =1;
			
		}else{
			
			nilai =0;
		}
		
	   $("#statussoal").html("<i class='fas fa-cog fa-spin'></i>    Sedang menyimpan..");
	      $.post("<?php echo site_url("teacherujian/simpan_ujian"); ?>",{tmujian_id:tmujian_id,kolom:kolom,nilai:nilai},function(){
			  
			  
			  
			  $("#statussoal").html("Tesimpan");
		  })
	  
	  
		  
  		 });

 		$(document).off("click",".soalgambar").on("click",".soalgambar",function(){
			     var trsoal_id = $(this).attr("trsoal_id");
				 
				 
				 $("#filesoal"+trsoal_id).click();
											   
		 });
		 $(document).off("change",".filesoal").on("change",".filesoal",function(){
			     var trsoal_id = $(this).attr("trsoal_id");
				readURL(this,trsoal_id);
				
											   
		 });
		 
	
		
		
		 

			
			
			$(document).off("click",".hapus_tingal_gambar").on("click",".hapus_tingal_gambar",function(){
				         var trsoal_id = $(this).attr("trsoal_id");
						 $("#filesoal"+trsoal_id).val("");
						 $("#div_tingal_gambar"+trsoal_id).hide();
													   
			 });
			 
	
  
	$(document).off("input",".ketiksoal").on("input",".ketiksoal",function(){
	  
	   var trsoal_id  = $(this).attr("trsoal_id");
	   var kolom      = $(this).attr("kolom");
	   var nilai      = $(this).val();
	
	   
	   
	   $("#statussoal").html("<i class='fas fa-cog fa-spin'></i>    Sedang menyimpan..");
	      $.post("<?php echo site_url("teacherujian/simpan_soal"); ?>",{trsoal_id:trsoal_id,kolom:kolom,nilai:nilai},function(){
			  
			  
			  
			  $("#statussoal").html("Tesimpan");
		  })
	  
	  
	  
  });
  
  $(document).off("change",".changejawaban").on("change",".changejawaban",function(){
	  
	   var trsoal_id  = $(this).attr("trsoal_id");
	   var kolom      = $(this).attr("kolom");
	   var nilai      = $(this).val();
	   $("#statussoal").html("<i class='fas fa-cog fa-spin'></i>    Sedang menyimpan..");
	      $.post("<?php echo site_url("teacherujian/simpan_soal"); ?>",{trsoal_id:trsoal_id,kolom:kolom,nilai:nilai},function(){
			  
			  
			  
			  $("#statussoal").html("Tesimpan");
		  })
	  
	  
	  
  });
  
  $(document).off("click",".tambahsoal").on("click",".tambahsoal",function(){
	  
	   var tmujian_id  = $(this).attr("tmujian_id");
	    
	   $("#statussoal").html("<i class='fas fa-cog fa-spin'></i>    Sedang menyimpan..");
	   loading();
	   
	 
	      $.post("<?php echo site_url("teacherujian/tambah_soal"); ?>",{tmujian_id:tmujian_id},function(data){
			  var datasplit =  data.split("[splitdendiganteng]");
			  var id_soal   =   datasplit[0];
			  var urutan    =   datasplit[1];
			
			     $(".detailpertanyaan").removeClass("active");
			 
			  $(".navigasisoal").append('<li class="active" id="navigasidendi'+id_soal+'"><a data-toggle="tab" class="navigasidendi" id_soal="'+id_soal+'" href="#detailpertanyaan'+id_soal+'"><span class="fa-stack" style="color:blue;border:1px solid blue"><span class="fa fa-square-o fa-stack-2x"></span><strong class="fa-stack-1x">'+urutan+'</strong></span></a></li>');
			  
			 
			  
			  
			  $("#statussoal").html("Tesimpan");
			  $("#pertanyaan").append(datasplit[2]);
			  
			  jQuery.unblockUI({ });
		  })
	  
	  
	  
  });
  
  $(document).off("click",".navigasidendi").on("click",".navigasidendi",function(){
	   var id_soal  = $(this).attr("id_soal");
	    $(".detailpertanyaan").removeClass("active");
	    $("#detailpertanyaan"+id_soal).addClass("active");
	  
	  
  });
    $(document).off("click",".hapus_soal").on("click",".hapus_soal",function(){
	  
	   var trsoal_id  = $(this).attr("trsoal_id");
	  
	   $("#statussoal").html("<i class='fas fa-cog fa-spin'></i>    Sedang menghapus..");
	      $.post("<?php echo site_url("teacherujian/hapus_soal"); ?>",{trsoal_id:trsoal_id},function(data){
			  
			  
			  
			  $("#statussoal").html("Tesimpan");
			  $("#detailpertanyaan"+trsoal_id).remove();
			  $("#navigasidendi"+trsoal_id).remove();
			  $("#detailpertanyaan"+(trsoal_id-1)).addClass("active");
			 
		  })
	  
	  
	  
  });
  
  




</script>