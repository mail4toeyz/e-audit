<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

      <li class="nav-item">
        <a class="nav-link " href="<?php echo site_url('admin'); ?>">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->


      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#master-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-menu-button-wide"></i><span>Data Master</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="master-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
        <li>
           
           <a href="<?php echo site_url("sbmtransport/data"); ?>" title="SBM Transport" class="menuajax">
                   <i class="bi bi-circle"></i><span>SBM Transport </span>
                 </a>
         </li>
         <li>
          
          <a href="<?php echo site_url("sbmhotel/data"); ?>" title="SBM Hotel" class="menuajax">
                  <i class="bi bi-circle"></i><span>SBM Hotel </span>
                </a>
        </li>
        <li>
          
          <a href="<?php echo site_url("sbmuh/data"); ?>" title="SBM Uang Harian" class="menuajax">
                  <i class="bi bi-circle"></i><span>SBM Uang Harian </span>
                </a>
        </li>

          <li>
            <a href="#">
              <i class="bi bi-circle"></i><span>Jenis Penugasan</span>
            </a>
          </li>
          <li>
            <a href="#">
              <i class="bi bi-circle"></i><span>Sub Jenis Penugasan</span>
            </a>
          </li>
         
         
        </ul>
      </li><!-- End Components Nav -->


      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#components-nav" data-bs-toggle="collapse" href="#">
        <i class="bi bi-people-fill"></i></i><span>Data Pegawai</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="components-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <?php 
             $unit_kerja = $this->db->get("unit_kerja")->result();
              foreach($unit_kerja as $row){
                ?>
                <li>
                  <a href="<?php echo site_url("unitkerja/pegawai/".base64_encode($row->id).""); ?>" title="<?php echo $row->nama; ?> " class="menuajax">
                    <i class="bi bi-circle"></i><span> <?php echo $row->nama; ?> </span>
                  </a>
                </li>
                <?php

              }
          ?>
          
          
          
         
          
        </ul>
      </li><!-- End Components Nav -->


      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#satker-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-bank"></i><span>Satuan Kerja </span>  <i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="satker-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <?php 
             $satker_kategori = $this->db->get("satker_kategori")->result();
              foreach($satker_kategori as $row){
                ?>
                <li>
                  <a href="<?php echo site_url("satuankerja/data/".base64_encode($row->id).""); ?>" title="<?php echo $row->nama; ?> " class="menuajax">
                    <i class="bi bi-circle"></i><span> <?php echo $row->nama; ?> </span>
                  </a>
                </li>
                <?php

              }
          ?>
          
          
          
         
          
        </ul>
      </li><!-- End Components Nav -->


      
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#users-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-file-person-fill"></i><span>Pengguna Aplikasi </span>  <i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="users-nav" class="nav-content collapse show" data-bs-parent="#sidebar-nav">
          <?php 
             $groups = $this->db->query("SELECT * from groups order by urutan ASC")->result();
              foreach($groups as $row){
                ?>
                <li>
                  <a href="<?php echo site_url("users/data/".base64_encode($row->id).""); ?>" title="<?php echo $row->nama; ?> " class="menuajax">
                    <i class="bi bi-circle"></i><span> <?php echo $row->nama; ?> </span>
                  </a>
                </li>
                <?php

              }
          ?>
          
          
          
         
          
        </ul>
      </li><!-- End Components Nav -->

      <li class="nav-heading">Pengaturan </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#">
          <i class="bi bi-dash-circle"></i>
          <span>Profile Anda</span>
        </a>
      </li><!-- End Error 404 Page Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" id="keluar">
          <i class="bi bi-file-earmark"></i>
          <span>Logout</span>
        </a>
      </li><!-- End Blank Page Nav -->
    </ul>

  </aside><!-- End Sidebar-->