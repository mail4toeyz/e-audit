
<div class="pagetitle">
  <h1><?php echo $title; ?></h1>
  <nav>
	<ol class="breadcrumb">
	  <li class="breadcrumb-item"><a href="index.html">e-Audit</a></li>
	
	  <li class="breadcrumb-item active"><?php echo $title; ?></li>
	</ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
	<div class="col-lg-12">

            <div class="card">
                    <div class="card-body">
                  
                    <!-- Bordered Tabs Justified -->
                    <ul class="nav nav-tabs nav-tabs-bordered d-flex" id="borderedTabJustified" role="tablist">
                        <li class="nav-item flex-fill" role="presentation">
                        <button class="nav-link w-100 active" id="home-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-home" type="button" role="tab" aria-controls="home" aria-selected="true">Data Usulan Kegiatan</button>
                        </li>
                        <li class="nav-item flex-fill" role="presentation">
                        <button class="nav-link w-100" id="profile-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Usulan Tim Kegiatan </button>
                        </li>
                        <li class="nav-item flex-fill" role="presentation">
                        <button class="nav-link w-100" id="contact-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Surat Tugas </button>
                        </li>
                    </ul>
                    <div class="tab-content pt-2" id="borderedTabJustifiedContent">
                        <div class="tab-pane fade show active" id="bordered-justified-home" role="tabpanel" aria-labelledby="home-tab">
                          
                        <div class="row p-2 ">
                <div class="col-md-6">
                    <table class="table">

                    <tr>
                        <td> Jenis Kegiatan </td>
                        <td>:</td>
                        <td>  <?php echo $this->Reff->get_kondisi(array("id"=>$data->jenis),"jenis_sub","nama"); ?> </td>
                      </tr>

                       <tr>
                        <td> Nama Kegiatan </td>
                        <td>:</td>
                        <td>  <?php echo $data->judul; ?> </td>
                      </tr>

                      <tr>
                        <td> Wilayah </td>
                        <td>:</td>
                        <td> <?php echo $this->Reff->get_kondisi(array("id"=>$data->provinsi_id),"provinsi","nama"); ?>  - <?php echo $this->Reff->get_kondisi(array("id"=>$data->kota_id),"kota","nama"); ?></td>
                      </tr>

                     
                      
                    </table>

                </div>

                <div class="col-md-6">
                    <table class="table">
                       <tr>
                        <td> Periode </td>
                        <td>:</td>
                        <td> <?php echo $this->Reff->formattanggalstring($data->tgl_mulai); ?>  - <?php echo $this->Reff->formattanggalstring($data->tgl_selesai); ?></td>
                      </tr>
                      <tr>
                        <td> Satuan Kerja </td>
                        <td>:</td>
                        <td>  
                            <ol start="1">
                              <?php 
                                $satker_id = explode(",",$data->satker_id);
                                  foreach($satker_id as $satker){

                                    ?><li><?php echo $this->Reff->get_kondisi(array("id"=>$satker),"satker","nama"); ?> - <?php echo $this->Reff->get_kondisi(array("id"=>$satker),"satker","kode"); ?></li><?php 
                                  }
                                  ?>
                              
                            </ol>

                          
                        </td>
                      </tr>
                     
                      
                    </table>

                </div>
</div>
                        </div>
                        <div class="tab-pane fade" id="bordered-justified-profile" role="tabpanel" aria-labelledby="profile-tab">
                          
                        <table class="table table-hover table-striped table-bordered">
                   <thead>
                      <tr>
                       <th rowspan="2"> # </th>
                       <th rowspan="2"> Nama </th>
                       
                       <th rowspan="2"> Gol </th>
                       <th rowspan="2"> Jabatan </th>
                       <th rowspan="2"> HP </th>
                       <th colspan="4"> Perhitungan SBM </th>
                       <th rowspan="2"> Anggaran </th>
                     </tr>

                     <tr>
                       <th>BST</th>
                       <th>Transport</th>
                       <th>Hotel</th>
                       <th>UH</th>
                     </tr>

                   </thead>
                   <tbody>

                   <?php 
                       $pegawaiJabatan = $this->db->query("SELECT * from tr_kegiatanJabatan where kegiatan_id='".$data->id."'")->result();
                       $no=1;
                       $total=0;
                       $bst = 512000;
                         foreach($pegawaiJabatan as $r){
                           $dataPegawai = $this->db->get_where("pegawai",array("id"=>$r->pejabat_id))->row();
                           $total = $total + $r->anggaran;
                      ?>
                     <tr>
                       <td> <?php echo $no++; ?></td>
                       <td> <?php echo $dataPegawai->nama; ?></td>
                       
                       <td> <?php echo $dataPegawai->golongan; ?></td>
                        <td> <?php echo $this->Reff->get_kondisi(array("id"=>$r->jabatan),"jabatan","nama"); ?></td>
                       <td>

                       <?php echo $r->waktu; ?>
                    
                    </td>

                    <td>
                    <?php echo $this->Reff->formatuang2($r->bst); ?>
                    </td>

                    <td>
                    
                      <?php echo $this->Reff->formatuang2($r->transport); ?>
                      
                    </td>

                    <td>
                    <?php 
                        echo  $this->Reff->formatuang2($r->hotel);
                    ?>
                    </td>

                    <td>
                    <?php 
                        echo  $this->Reff->formatuang2($r->uh);
                    ?>
                    </td>

                    <td>
                       <?php echo $this->Reff->formatuang2($r->anggaran); ?>
                    </td>


                     </tr>

                   
                     <?php 


                        }
                        ?>

                        <tr style="font-weight:bold">
                          <td colspan="9"> Total Anggaran </th>
                          <td> <?php echo $this->Reff->formatuang2($total); ?> </th>
                        </tr>
                     
                   </tbody>
                 </table>

                        </div>
                        <div class="tab-pane fade" id="bordered-justified-contact" role="tabpanel" aria-labelledby="contact-tab">
                           
                      

                       
                        <form class="row g-3" action="javascript:void(0)" method="post" id="simpangambar" name="simpangambarmodal" url="<?php echo site_url("bagumum/save"); ?>">
						             <input type="hidden" class="form-control"  name='id' value="<?php echo (isset($data)) ? $data->id :""; ?>" >
                            <div class="col-12">
                            <label for="inputNanme4" class="form-label">No Surat  </label>
                            <input class="form-control" id="catatan" name="f[no_surat]" value="<?php echo $data->no_surat; ?>">
                            </div>
                            <div class="col-12">
                            <label for="inputEmail4" class="form-label">Menimbang </label>
                            <textarea class="form-control" id="catatan" name="menimbang" placeholder="Gunakan tanda (;) untuk garis baru"><?php echo $data->menimbang; ?></textarea>
                            </div>
                            <div class="col-12">
                            <label for="inputEmail4" class="form-label">Dasar </label>
                            <textarea class="form-control" id="catatan" name="dasar" name="menimbang" placeholder="Gunakan tanda (;) untuk garis baru"><?php echo $data->dasar; ?></textarea>
                            </div>

                            <div class="col-12">
                            <label for="inputEmail4" class="form-label">Waktu </label>
                            <textarea class="form-control" id="catatan" name="waktu"><?php echo $data->waktu; ?></textarea>
                            </div>

                            <div class="col-12">
                            <label for="inputNanme4" class="form-label">Tanggal Surat  </label>
                            <input class="form-control" id="tgl_surat" name="f[tgl_surat]" value="<?php echo $data->tgl_surat; ?>">
                            <script>
																$(document).ready(function () {
																	startdate = new Date() - 18;
																	$('#tgl_surat').datepicker({
																	
																			 changeMonth: true,
																								changeYear: true,
																								autoclose: true,
																								dateFormat: 'yy-mm-dd',
																								yearRange: "2022:2024",
																							
																	});
																});
																
															
															</script>
                            </div>

                            
                            <div class="text-center">
                            <button type="submit" class="btn btn-outline-primary">Simpan </button>
                            <button type="button" class="btn btn-outline-primary" id ="cancel"> <i class="bi bi-box-arrow-in-down-right"></i>  Kembali </button>
                          
                            </div>
                        </form><!-- Vertical Form -->



                        </div>

                       
                    </div><!-- End Bordered Tabs Justified -->

                    </div>
                </div>

	 
     </div>
 </div>
</section>
