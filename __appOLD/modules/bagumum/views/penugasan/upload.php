
<div class="pagetitle">
  <h1><?php echo $title; ?></h1>
  <nav>
	<ol class="breadcrumb">
	  <li class="breadcrumb-item"><a href="index.html">e-Audit</a></li>
	
	  <li class="breadcrumb-item active"><?php echo $title; ?></li>
	</ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
	<div class="col-lg-12">

            <div class="card">
                    <div class="card-body">
                    <br>
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <i class="bi bi-exclamation-triangle me-1"></i>
                Upload Surat Tugas yang sudah ditandatangan pada laman dibawah ini
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>

                    <input id="file" name="file" type="file" multiple accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps" />
                    <br>
                   <center> <button type="button" class="btn btn-outline-primary" id ="cancel"> <i class="bi bi-box-arrow-in-down-right"></i>  Kembali </button></center>

                    </div>
                </div>

	 
     </div>
 </div>
</section>


<script type="text/javascript">
  
 

  $(document).ready(function() {

$("#file").fileinput({
          uploadUrl: "<?php echo site_url("satker/berkas_upload"); ?>",
          overwriteInitial: false,
          
          browseClass: "btn btn-success",
          browseLabel: "Surat Tugas",
          browseIcon: "<i class=\"fa fa-file-signature\"></i> Ambil File ",
          removeClass: "btn btn-danger",
          removeLabel: "Delete",
          removeIcon: "<i class=\"fa fa-trash\"></i>  Hapus File",
          uploadClass: "btn btn-info",
          uploadLabel: "Upload",
          uploadIcon: "<i class=\"fa fa-upload\"></i>  Upload Semua File",
          initialPreviewAsData: true,
              
                          


                              
                            });





                              });		
                              

                            </script>