<script type="text/javascript">
      
$(function () {
    // Set up the chart
    var chart = new Highcharts.Chart({
		chart: {
        type: 'column',
		
		renderTo: 'container_grafik',
		 options3d: {
                enabled: true,
                alpha: 5,
                beta: 5,
                depth: 50,
                viewDistance: 25
            },
    },
    title: {
        text: 'e-Audit'
    },
    subtitle: {
        text: 'Jabatan Pegawai Inspektorat<br> '
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '10px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Peserta'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Jumlah : <b>{point.y} Peserta</b>'
    },
    series: [{
        name: 'Population',
        data: [
		
		       
         <?php 
         $htmls ="";
         foreach ($grid as $key){
         $varnya = $key['INDEXES'];
         $jumlah = $key['Jumlah'];
         

           $htmls .=",
            
            ['$varnya',$jumlah]
            ";

       
         
         }

         echo substr($htmls,1);
        ?>
           
        ],
        dataLabels: {
			rotation: 0,
            enabled: true,
			 
                crop: false,
                overflow: 'none',
             
            color: '#000',
            align: 'center',
            format: '{point.y}', // one decimal
            y: -12, // 10 pixels down from the top
            style: {
                fontSize: '10px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
		
	  });	
		
      

      var chart = new Highcharts.Chart({
        chart: {
			renderTo: 'container_Pie',
            type: 'pie'
        },
		
        title: {
            text: 'Persentase Jabatan'
        },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                fontSize: '12px',
                fontFamily: 'Verdana, sans-serif'
            }
            }
        }
    },
        series: [{
            type: 'pie',
            name: '<?php echo $statistik; ?>',
            data: [
               <?php echo $json_pie_chart; ?>
               
                
                
                
            ]
        }]
    });








        });
    </script>

<main id="main" class="main">

<div class="pagetitle">
  <h1>Dashboard <?php echo $this->Reff->get_kondisi(array("id"=>$_SESSION['group_id']),"groups","nama"); ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.html">Home</a></li>
      <li class="breadcrumb-item active">Dashboard</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section dashboard">
  <div class="row">

    <!-- Left side columns -->
    <div class="col-lg-8">
      <div class="row">
      <div class="col-xxl-4 col-xl-12">

        <div class="card info-card customers-card">
          <div class="card-body">
            <h5 class="card-title">Surat Tugas </span></h5>

            <div class="d-flex align-items-center">
              <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
              <i class="bi bi-bag-dash-fill"></i>
              </div>
              <div class="ps-3">
                <h6><?php 
                  $jml = $this->db->query("SELECT count(id) as jml from kegiatan where status !=0")->row();  echo $jml->jml; ?></h6>
                
              </div>
            </div>

          </div>
        </div>

        </div>

        <div class="col-xxl-4 col-xl-12">

        <div class="card info-card customers-card">
          <div class="card-body">
            <h5 class="card-title">Perlu Tindakan </span></h5>

            <div class="d-flex align-items-center">
              <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
              <i class="bi bi-binoculars-fill"></i>
              </div>
              <div class="ps-3">
                <h6><?php 
                  $jml = $this->db->query("SELECT count(id) as jml from kegiatan where status !=0 and approval=0")->row();  echo $jml->jml; ?></h6>
                
              </div>
            </div>

          </div>
        </div>

        </div>

        <div class="col-xxl-4 col-xl-12">

        <div class="card info-card customers-card">
          <div class="card-body">
            <h5 class="card-title">Dterbitkan  </span></h5>

            <div class="d-flex align-items-center">
              <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
              <i class="bi bi-check-circle me-"></i>
              </div>
              <div class="ps-3">
                <h6><?php 
                  $jml = $this->db->query("SELECT count(id) as jml from kegiatan where status !=0 and approval=1")->row();  echo $jml->jml; ?></h6>
                
              </div>
            </div>

          </div>
        </div>

        </div>

   



      </div>
      <div class="row">

        

        <!-- Customers Card -->
        <?php 
          $unit_kerja = $this->db->get("unit_kerja")->result();
            foreach($unit_kerja as $row){
               $jml = $this->db->query("SELECT count(id) as jml from pegawai where unitkerja_id='{$row->id}'")->row();
              ?>

              <div class="col-xxl-4 col-xl-12">

              <div class="card info-card customers-card">
                <div class="card-body">
                  <h5 class="card-title"><?php echo $row->nama; ?></span></h5>

                  <div class="d-flex align-items-center">
                    <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                      <i class="bi bi-people"></i>
                    </div>
                    <div class="ps-3">
                      <h6><?php echo $jml->jml; ?></h6>
                      
                    </div>
                  </div>

                </div>
              </div>

              </div>
              <?php


            }
          ?>
        

        <!-- Reports -->
        <div class="col-12">
          <div class="card">

            <div class="filter">
              <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
              <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                <li class="dropdown-header text-start">
                  <h6>Filter</h6>
                </li>

                <li><a class="dropdown-item" href="#">Today</a></li>
                <li><a class="dropdown-item" href="#">This Month</a></li>
                <li><a class="dropdown-item" href="#">This Year</a></li>
              </ul>
            </div>

            <div class="card-body">
            <h5 class="card-title">Data Kegiatan </h5>

            <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
              <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Grafik   </button>
             
            </li>
              <li class="nav-item" role="presentation">
                  <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Data   </button>
                </li>
              
          
          </ul>
          <div class="tab-content pt-2" id="myTabContent">
          
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div id="container_grafik"  class="grafikdiv"></div>
            </div>



                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                        </div>

           
            </div>
           
            

            </div>

          </div>
        </div><!-- End Reports -->

  
      </div>
    </div><!-- End Left side columns -->

    <!-- Right side columns -->
    <div class="col-lg-4">

      <!-- Recent Activity -->
      
      <!-- Budget Report -->

      <div class="card">
        <div class="filter">
          <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
          <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
            <li class="dropdown-header text-start">
              <h6>Filter</h6>
            </li>

            <li><a class="dropdown-item" href="#">Today</a></li>
            <li><a class="dropdown-item" href="#">This Month</a></li>
            <li><a class="dropdown-item" href="#">This Year</a></li>
          </ul>
        </div>

        <div class="card-body pb-0">
          <h5 class="card-title">Persentase Jabatan  <span></span></h5>

          <div id="container_Pie"   class="grafikdiv"></div>

         

        </div>
      </div>


      <div class="card">
        <div class="filter">
          <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
          <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
            <li class="dropdown-header text-start">
              <h6>Filter</h6>
            </li>

            <li><a class="dropdown-item" href="#">Today</a></li>
            <li><a class="dropdown-item" href="#">This Month</a></li>
            <li><a class="dropdown-item" href="#">This Year</a></li>
          </ul>
        </div>

        <div class="card-body pb-0">
          <h5 class="card-title">Audit Report <span></span></h5>

          <div id="budgetChart" style="min-height: 400px;" class="echart"></div>

          <script>
            document.addEventListener("DOMContentLoaded", () => {
              var budgetChart = echarts.init(document.querySelector("#budgetChart")).setOption({
                legend: {
                  data: ['Allocated Budget', 'Actual Spending']
                },
                radar: {
                  // shape: 'circle',
                  indicator: [{
                      name: 'Audit',
                      max: 6500
                    },
                    {
                      name: 'Review',
                      max: 16000
                    },
                    {
                      name: 'Pelaksanaan',
                      max: 30000
                    },
                    {
                      name: 'Pengawasan',
                      max: 38000
                    },
                    {
                      name: 'Pembiayaan',
                      max: 52000
                    },
                    {
                      name: 'Notulasi',
                      max: 25000
                    }
                  ]
                },
                series: [{
                  name: 'Budget vs spending',
                  type: 'radar',
                  data: [{
                      value: [4200, 3000, 20000, 35000, 50000, 18000],
                      name: 'Allocated Budget'
                    },
                    {
                      value: [5000, 14000, 28000, 26000, 42000, 21000],
                      name: 'Actual Spending'
                    }
                  ]
                }]
              });
            });
          </script>

        </div>
      </div><!-- End Budget Report -->

    </div><!-- End Right side columns -->

  </div>
</section>

</main>