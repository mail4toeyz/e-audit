<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">
    <li class="nav-heading"> Akun <?php echo $this->Reff->get_kondisi(array("id"=>$_SESSION['group_id']),"groups","nama"); ?>  </li>
      <li class="nav-item">
        <a class="nav-link " href="<?php echo site_url('irjen'); ?>">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->

      <li class="nav-heading"> Penomoran  </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo site_url("bagumum/penugasan"); ?>">
          <i class="bi bi-plus-circle"></i>
          <span> Penomoran Surat Tugas </span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#">
        <i class="bi bi-menu-button-wide"></i>
          <span>Penomoran LHA  </span>
        </a>
      </li>

      

      <li class="nav-heading">Pengaturan </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#">
          <i class="bi bi-dash-circle"></i>
          <span>Profile Anda</span>
        </a>
      </li><!-- End Error 404 Page Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo site_url("login"); ?>">
          <i class="bi bi-file-earmark"></i>
          <span>Logout</span>
        </a>
      </li><!-- End Blank Page Nav -->

    </ul>

  </aside><!-- End Sidebar-->