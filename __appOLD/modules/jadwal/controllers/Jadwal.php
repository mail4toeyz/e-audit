<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		  if(!$this->session->userdata("is_login")){
			    
				echo $this->Reff->sessionhabis();
				exit();
			
		  }
		  $this->load->model('M_jadwal','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('dashboard/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Data Jadwal Kegiatan ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
			
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}

	public function grid(){
		
		
		$iTotalRecords = $this->m->grid(false)->num_rows();
		
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);
		
		$records = array();
		$records["data"] = array(); 

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;
		
		$datagrid = $this->m->grid(true)->result_array();
		
		
		 $i= ($iDisplayStart +1);
		 foreach($datagrid as $val) {
			  
			$anggota ="<ol>";
			$dataAnggota = json_decode($val['anggota'],true);
			  foreach($dataAnggota as $r){
				$anggota .="<li>".$this->Reff->get_kondisi(array("id"=>$r),"pegawai","nama")."</li>";

			  }

			 $anggota .="</ol>";

			 $satkerData ="<ol>";
			 $satker =  explode(",",$val['satker_id']);
			   foreach($satker as $r){
				 $satkerData .="<li>".$this->Reff->get_kondisi(array("id"=>$r),"satker","nama")."</li>";
 
			   }
 
			  $satkerData .="</ol>";

			 


			  if($val['approval']==0){
				  $approval ='<span class="badge bg-danger"><i class="bi bi-exclamation-octagon me-1"></i> Belum disetujui</span>';
			  }

			  if($val['approval']==1){
				$approval ='<span class="badge bg-success"><i class="bi bi-check-circle me-"></i>  Disetujui</span>';
			}

			if($val['status']==0){
				$approval ='<span class="badge bg-danger"><i class="bi bi-exclamation-octagon me-1"></i> Draft </span>';
			}

			  $no = $i++;
			  $records["data"][] = array(
				  $no,
		  
				
				  $approval,
				  $this->Reff->get_kondisi(array("id"=>$val['jenis']),"tm_jenis","nama"),
				  
				  $val['judul'],
				  $this->Reff->get_kondisi(array("id"=>$val['provinsi_id']),"provinsi","nama"),
				  $this->Reff->get_kondisi(array("id"=>$val['kota_id']),"kota","nama"),
				  $satkerData,
				  $this->Reff->formattanggalstring($val['tgl_mulai'])." - ". $this->Reff->formattanggalstring($val['tgl_selesai']),
				  
				  $this->Reff->get_kondisi(array("id"=>$val['penanggung_jawab']),"pegawai","nip")."-".$this->Reff->get_kondisi(array("id"=>$val['penanggung_jawab']),"pegawai","nama"),
				  $this->Reff->get_kondisi(array("id"=>$val['pengendali_mutu']),"pegawai","nip")."-".$this->Reff->get_kondisi(array("id"=>$val['pengendali_mutu']),"pegawai","nama"),
				  $this->Reff->get_kondisi(array("id"=>$val['pengendali_teknis']),"pegawai","nip")."-".$this->Reff->get_kondisi(array("id"=>$val['pengendali_teknis']),"pegawai","nama"),
				
				  $this->Reff->get_kondisi(array("id"=>$val['ketua']),"pegawai","nip")."-".$this->Reff->get_kondisi(array("id"=>$val['ketua']),"pegawai","nama"),
				  $anggota,
				  ' 
					<div class="btn-group" role="group">
					<a class="btn btn-outline-primary btn-sm " datanya="'.$val['id'].'" href="'.site_url("jadwal/create?kegiatan=".$val['id']."").'" >
                                    <i class="fa fa-pencil"></i>
                      </a>
					  <button type="button"  class="btn btn-outline-danger btn-sm hapus" datanya="'.$val['id'].'" urlnya="'.site_url("jadwal/hapus").'">
                                    <i class="fa fa-trash"></i>
					  </button>
					  </div>
					 
					  '
				
				  
	  
				 
				 

				);
			}
	  
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		
		echo json_encode($records);
  }

	public function create()
	{  
	    
		  
		  
	    		
         $ajax                = $this->input->get_post("ajax",true);	
         $kegiatan            = $this->input->get_post("kegiatan",true);	
		
		 $data['title']   = "Buat Jadwal Kegiatan ";
		 if(!empty($kegiatan)){
			$data['data'] = $this->db->get_where("kegiatan",array("id"=>$kegiatan))->row();
		 }
	     if(!empty($ajax)){
					    
			 $this->load->view('page_create',$data);
		
		 }else{
			 
			
		     $data['konten'] = "page_create";
			 
			 $this->_template($data);
		 }
	

	}
	
	public function getSatker(){
		$provinsi_id  = $this->input->get_post("provinsi_id");
		$kota_id      = $this->input->get_post("kota_id");
		$provinsiKode = $this->Reff->get_kondisi(array("id"=>$provinsi_id),"provinsi","kode");
		$kotaKode 	  = $this->Reff->get_kondisi(array("id"=>$kota_id),"kota","kode");
		$where =" AND 1=1";
		  if(!empty($kotaKode)){
			  $where .=" AND kota_id='{$kotaKode}' ";
		  }
		$satker_kategori = $this->db->query("SELECT * from satker_kategori where id IN(SELECT kategori from satker where provinsi_id='{$provinsiKode}' $where )")->result();
		foreach($satker_kategori as $satker){
			?>

			  <optgroup  label="<?php echo $satker->nama; ?>">  
			  <?php       
				  $satkerData = $this->db->query("SELECT * from satker where kategori='{$satker->id}' and provinsi_id='{$provinsiKode}' $where order by nama ASC")->result();
				   foreach($satkerData as $row){   
				?>                   
				  <option value="<?php echo $row->id; ?>"><?php echo $row->kode; ?> - <?php echo $row->nama; ?></option>  
				  <?php 
				   }
				   ?>                           
			  </optgroup>   
			  
			  <?php 
		}
 
	}

	public function stepJabatan(){

		$this->form_validation->set_message('required', '{field} Wajib diisi. ');
		$this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
		$this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
		$this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
   			$config = array(
				   array('field' => 'f[jenis]', 'label' => 'Jenis Kegiatan  ', 'rules' => 'trim|required'),
				   array('field' => 'f[judul]', 'label' => 'Judul   ', 'rules' => 'trim|required'),
				   array('field' => 'f[provinsi_id]', 'label' => 'Provinsi   ', 'rules' => 'trim|required'),
				   array('field' => 'f[tgl_mulai]', 'label' => 'Tanggal Mulai   ', 'rules' => 'trim|required'),
				   array('field' => 'f[tgl_selesai]', 'label' => 'Tanggal Selesai   ', 'rules' => 'trim|required'),
				   array('field' => 'satuankerja[]', 'label' => 'Satuan Kerja    ', 'rules' => 'trim|required'),
				  );
			   
			   $this->form_validation->set_rules($config);	
	   
	   if ($this->form_validation->run() == true) {

		   $f 			= $this->input->post("f");
		   $satuankerja = implode(",",$this->input->post("satuankerja"));
		   

	    	$this->db->where("jenis",$f['jenis']);
	    	$this->db->where("judul",$f['judul']);
	    	$this->db->where("provinsi_id",$f['provinsi_id']);
	    	$this->db->where("kota_id",$f['kota_id']);			
			$this->db->where("satker_id",$satuankerja);
			$cek = $this->db->get("kegiatan")->row();
			if(is_null($cek)){

				
					$this->db->set("satker_id",$satuankerja);
					$this->db->insert("kegiatan",$f);
					$data['kegiatan_id'] = $this->db->insert_id();
					$data['data'] = $this->db->get_where("kegiatan",array("id"=>$data['kegiatan_id']))->row();
					$this->load->view("form_jabatan",$data);
			}else{

			    	$this->db->where("id",$cek->id);
			    	$this->db->set("satker_id",$satuankerja);
					$this->db->update("kegiatan",$f);
				    $data['kegiatan_id'] = $cek->id;
					$data['data'] = $this->db->get_where("kegiatan",array("id"=>$data['kegiatan_id']))->row();
					$this->load->view("form_jabatan",$data);
			}

	   }else{

		   echo  '<div class="alert alert-warning">'.validation_errors().'</div>';
	   }
	}

	public function stepPeriode(){
		$kegiatan_id        = $this->input->get_post('kegiatan_id');
		$penanggung_jawab  = $this->input->get_post('penanggung_jawab');
		$pengendali_mutu   = $this->input->get_post('pengendali_mutu');
		$pengendali_teknis  = $this->input->get_post('pengendali_teknis');
		$ketua_tim  = $this->input->get_post('ketua_tim');
		$auditor            = json_encode($this->input->get_post('auditor'),true);


	
		$this->db->where("id",$kegiatan_id);
		$this->db->set("penanggung_jawab",$penanggung_jawab);
		$this->db->set("pengendali_mutu",$pengendali_mutu);
		$this->db->set("pengendali_teknis",$pengendali_teknis);
		$this->db->set("ketua",$ketua_tim);
		$this->db->set("anggota",$auditor);
	    $this->db->update("kegiatan");
      
	   $this->db->query("DELETE  FROM tr_kegiatanJabatan where kegiatan_id='{$kegiatan_id}'");
	     if(!empty($penanggung_jawab)){

			$this->db->set("kegiatan_id",$kegiatan_id);
			$this->db->set("jabatan",1);
			$this->db->set("pejabat_id",$penanggung_jawab);
			$this->db->set("waktu",0);
			$this->db->insert("tr_kegiatanJabatan");

		 }

		 if(!empty($pengendali_mutu)){

			$this->db->set("kegiatan_id",$kegiatan_id);
			$this->db->set("jabatan",3);
			$this->db->set("pejabat_id",$pengendali_mutu);
			$this->db->set("waktu",0);
			$this->db->insert("tr_kegiatanJabatan");

		 }

		 if(!empty($pengendali_teknis)){

			$this->db->set("kegiatan_id",$kegiatan_id);
			$this->db->set("jabatan",4);
			$this->db->set("pejabat_id",$pengendali_teknis);
			$this->db->set("waktu",0);
			$this->db->insert("tr_kegiatanJabatan");

		 }

		 if(!empty($ketua_tim)){

			$this->db->set("kegiatan_id",$kegiatan_id);
			$this->db->set("jabatan",2);
			$this->db->set("pejabat_id",$ketua_tim);
			$this->db->set("waktu",0);
			$this->db->insert("tr_kegiatanJabatan");

		 }

		$auditor  = $this->input->get_post('auditor');
		 if(count($auditor) >0){

			 foreach($auditor as $ra){
				$this->db->set("kegiatan_id",$kegiatan_id);
				$this->db->set("jabatan",5);
				$this->db->set("pejabat_id",$ra);
				$this->db->set("waktu",0);
				$this->db->insert("tr_kegiatanJabatan");

			 }

		 }




		$data["kegiatan_id"] =  $kegiatan_id;
		$data['kegiatan'] = $this->db->query("SELECT * from kegiatan where id='{$kegiatan_id}'")->row();
		$this->load->view("form_periode",$data);

	}

	public function savePeriode(){

		$kegiatan_id        = $this->input->get_post('kegiatan_id');
		$id                 = $this->input->get_post('id');
		$hari               = $this->input->get_post('hari');
		$bst               = $this->input->get_post('bst');
		$transport               = $this->input->get_post('transport');
		$hotel               = $this->input->get_post('hotel');
		$uh               = $this->input->get_post('uh');

		$totalHotel = $hari * $hotel;
		$totaluh = $hari * $uh;
		$totalAnggaran = $bst + $transport + $totalHotel + $totaluh;

		if($hari ==0){
			$totalAnggaran = 0;
		}

		$this->db->where("id",$id);
		$this->db->set("waktu",$hari);
		$this->db->set("anggaran",$totalAnggaran);
		$this->db->set("bst",$bst);
		$this->db->set("transport",$transport);
		$this->db->set("hotel",$hotel);
		$this->db->set("uh",$uh);
		$this->db->update("tr_kegiatanJabatan");
		$data["kegiatan_id"] =  $kegiatan_id;
		$data['kegiatan'] = $this->db->query("SELECT * from kegiatan where id='{$kegiatan_id}'")->row();
		$this->load->view("load_sbm",$data);
		
		

		  

		
	}

	public function saveAll(){


		$kegiatan_id        = $this->input->get_post('kegiatan_id');
		$tgl_mulai          = $this->input->get_post('tgl_mulai');
		$tgl_selesai   = $this->input->get_post('tgl_selesai');
	
	
		$this->db->where("id",$kegiatan_id);
		$this->db->set("status",1);	
	    $this->db->update("kegiatan");

	}
	 
	 
	
	
	public function hapus(){
		
		$id = $this->input->get_post("id",true);
		
		$this->db->delete("kegiatan",array("id"=>$id));
		$this->db->delete("tr_kegiatanJabatan",array("kegiatan_id"=>$id));
		echo "sukses";
	}


}
