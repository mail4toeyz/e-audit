<main id="main" class="main">

    <div class="pagetitle">
      <h1><?php echo $title; ?></h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#"><?php echo app(); ?></a></li>
          
          <li class="breadcrumb-item active"><?php echo $title; ?></li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title"><?php echo $title; ?></h5>


              <div id="smartwizard">

                <ul class="nav">
                    <li class="nav-item">
                    <a class="nav-link" href="#step-1">
                        <strong>Langkah 1</strong> <br>  Jenis Kegiatan 
                    </a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#step-2">
                        <strong>Langkah  2</strong> <br> Memilih Jabatan
                    </a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#step-3">
                        <strong>Langkah  3</strong> <br> Periode Kegiatan 
                    </a>
                    </li>
                    
                </ul>
                <form method="post" action="javascript:void(0)" id="kegiatanForm" url="<?php echo site_url("berkas/kirim"); ?>" class="row g-3">
                <div class="tab-content">

                    <div id="step-1" class="tab-pane" role="tabpanel" aria-labelledby="step-1">
                     
                        <?php   $this->load->view("form_jenis"); ?>
                     
                    </div>
                    <div id="step-2" class="tab-pane" role="tabpanel" aria-labelledby="step-2">
                      <div class="alert alert-danger"> Silahkan lengkapi terlebih dahulu jenis kegiatan  </div>
                    </div>
                    <div id="step-3" class="tab-pane" role="tabpanel" aria-labelledby="step-3">
                    
                    </div>
                    
                </div>

                </form>

                </div> 



            </div>
          </div>

          

        </div>

      </div>
    </section>

  </main>

  <script type="text/javascript">
        $(document).ready(function(){

            $("#smartwizard").on("stepContent", function(e, anchorObject, stepIndex, stepDirection) {


                if(stepIndex==1){
                  var form = document.getElementById('kegiatanForm');
                  var formData = new FormData(form);
                       
                        var url ="<?php echo site_url("jadwal/stepJabatan"); ?>";
                        var tab_id = "step-2";

                      
                          $.ajax({
                              type: "POST",
                              url: url,
                              
                        data: formData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                        contentType: false,       // The content type used when sending data to the server.
                        cache: false,             // To unable request pages to be cached
                        processData:false,    
                        success: function (response, status, xhr) {
                          $('#smartwizard').smartWizard("loader", "hide");
                          $("#"+tab_id).html(response);
                          
                        
                              }
                          });



                        $('#smartwizard').smartWizard("loader", "show");
                            // $.post(url,{jenis:jenis,judul:judul,provinsi:provinsi,kota:kota,satker:satker},function(data){

                            //     $("#"+tab_id).html(data);
                            //     $('#smartwizard').smartWizard("loader", "hide");
                            // });
              
              
              
              
                          }


                    
                    if(stepIndex==2){
                     
                   
                      var kegiatan_id = $("#kegiatan_id").val();
                      var penanggung_jawab = $("#penanggung_jawab").val();
                      var pengendali_mutu = $("#pengendali_mutu").val();
                      var pengendali_teknis = $("#pengendali_teknis").val();
                      var ketua_tim = $("#ketua_tim").val();
                     
                      var auditor=[]; 
                      $('select[name="auditor[]"] option:selected').each(function() {
                        if($(this).val() !=""){
                           auditor.push($(this).val());
                        }
                      });
                        var url ="<?php echo site_url("jadwal/stepPeriode"); ?>";
                        var tab_id = "step-3";
                        $('#smartwizard').smartWizard("loader", "show");
                            $.post(url,{auditor:auditor,penanggung_jawab:penanggung_jawab,pengendali_mutu:pengendali_mutu,pengendali_teknis:pengendali_teknis,kegiatan_id:kegiatan_id,ketua_tim:ketua_tim},function(data){

                                $("#"+tab_id).html(data);
                                $('#smartwizard').smartWizard("loader", "hide");
                            });
                    }
                    

            });
                    

            $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
                $("#prev-btn").removeClass('disabled');
                $("#next-btn").removeClass('disabled');
                if(stepPosition === 'first') {
                    $("#prev-btn").addClass('disabled');
                } else if(stepPosition === 'last') {
                    $("#next-btn").addClass('disabled');
                } else {
                    $("#prev-btn").removeClass('disabled');
                    $("#next-btn").removeClass('disabled');
                }
            });

            // Smart Wizard
            $('#smartwizard').smartWizard({
                selected: 0,
                theme: 'arrows', // default, arrows, dots, progress
                // darkMode: true,
                transition: {
                    animation: 'slide-horizontal', // Effect on navigation, none/fade/slide-horizontal/slide-vertical/slide-swing
                },
                toolbarSettings: {
                    toolbarPosition: 'bottom'
                },
                lang: {  // Language variables
                next: 'Selanjutnya >',
                previous: '< Sebelumnya'
            }
            });

            // External Button Events
            $("#reset-btn").on("click", function() {
                // Reset wizard
                $('#smartwizard').smartWizard("reset");
                return true;
            });

            $("#prev-btn").on("click", function() {
                // Navigate previous
                $('#smartwizard').smartWizard("prev");
                return true;
            });

            $("#next-btn").on("click", function() {
                // Navigate next
                $('#smartwizard').smartWizard("next");
                return true;
            });


            // Demo Button Events
            $("#got_to_step").on("change", function() {
                // Go to step
                var step_index = $(this).val() - 1;
                $('#smartwizard').smartWizard("goToStep", step_index);
                return true;
            });


            $("#dark_mode").on("click", function() {
                // Change dark mode
                var options = {
                  darkMode: $(this).prop("checked")
                };

                $('#smartwizard').smartWizard("setOptions", options);
                return true;
            });

            $("#is_justified").on("click", function() {
                // Change Justify
                var options = {
                  justified: $(this).prop("checked")
                };

                $('#smartwizard').smartWizard("setOptions", options);
                return true;
            });

            $("#animation").on("change", function() {
                // Change theme
                var options = {
                  transition: {
                      animation: $(this).val()
                  },
                };
                $('#smartwizard').smartWizard("setOptions", options);
                return true;
            });

            $("#theme_selector").on("change", function() {
                // Change theme
                var options = {
                  theme: $(this).val()
                };
                $('#smartwizard').smartWizard("setOptions", options);
                return true;
            });
           

        });

        $(document).ready(function() {
              $('.js-example-basic-single').select2({ width: '100%' });
            //   $( '.js-example-basic-single' ).select2( {
            //    theme: 'bootstrap-3'
            //  } );
             });

      $(document).off("change","#jenis").on("change","#jenis",function(){
      var jenis  = $(this).val();
      var option = $('option:selected', this).attr('nama');
  
      
      if(jenis =="") { return false; }
      $("#judul").html("Judul "+option);

      });


      $(document).off("change","#provinsi").on("change","#provinsi",function(){
      var provinsi_id  = $(this).val();
          $.post("<?php echo site_url("jadwal/getSatker"); ?>",{provinsi_id:provinsi_id},function(data){

            $("#satker").html(data);
          })
      

      });

      $(document).off("change","#kota").on("change","#kota",function(){
        var provinsi_id  = $("#provinsi").val();
        var kota_id  = $(this).val();
          $.post("<?php echo site_url("jadwal/getSatker"); ?>",{provinsi_id:provinsi_id,kota_id:kota_id},function(data){

            $("#satker").html(data);
          })
      

      });
 

            
    </script>