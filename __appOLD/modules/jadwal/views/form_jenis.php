
         <div class="row p-2 ">
                <div class="col-md-12">
                  <label for="inputName5" class="form-label"> Jenis Kegiatan</label>
                  <select class="form-control js-example-basic-single" name="f[jenis]" id="jenis">
                              <option value="">- Pilih Jenis Kegiatan  -</option>
                              <?php 
                             

                             $jenis = $this->db->query("SELECT * from jenis")->result();
                            foreach($jenis as $jenisData){
                              ?>

                                <optgroup  label="<?php echo $jenisData->nama; ?>">  
                                <?php       
                                  $subJenis = $this->db->query("SELECT * from jenis_sub where jenis_id='{$jenisData->id}' order by nama ASC")->result();
                                  foreach($subJenis as $row){   
                                ?>                   
                                  <option value="<?php echo $row->id; ?>" <?php if(isset($data)){  echo ($data->jenis==$row->id) ? "selected" : ""; }  ?>><?php echo $jenisData->nama; ?> - <?php echo $row->nama; ?></option>  
                                  <?php 
                                  }
                                  ?>                           
                                </optgroup>   
                                
                                <?php 
                            }

                             ?>


                         </select>
                </div>
            </div>
           
            <div class="row p-2 ">
                <div class="col-md-12">
                  <label for="inputName5" class="form-label" id="judul">Judul</label>
                  <input type="text" class="form-control" name="f[judul]" id="judulKegiatan" placeholder="ex: Audit Kinerja Madrasah " value="<?php echo isset($data) ? $data->judul :"" ;  ?>">
                </div>
            </div>
            <div class="row p-2 ">
                <div class="col-md-6">
                  <label for="inputEmail5" class="form-label">Provinsi</label>
                 
					<select class="form-control onchange js-example-basic-single" name="f[provinsi_id]"  target="kota" urlnya="<?php echo site_url('publik/kota'); ?>" id="provinsi" >
						<option value="">- Pilih Provinsi -</option>
						<?php 
							$provinsi = $this->db->get("provinsi")->result();
							foreach($provinsi as $row){

								?> <option value="<?php echo $row->id; ?>" <?php if(isset($data)){  echo ($data->provinsi_id==$row->id) ? "selected" : ""; }  ?>><?php echo $row->nama; ?></option> <?php 
							}
						?>


					</select>
				
                </div>
                <div class="col-md-6">
                  <label for="inputPassword5" class="form-label">Kabupaten/Kota</label>
                  <select class="form-control kota js-example-basic-single" name="f[kota_id]" id="kota">
						<option value="">- Pilih Kabupaten/Kota -</option>
						<?php 
							$provinsi = $this->db->get_where("kota",array("provinsi_id"=>$data->provinsi_id))->result();
							foreach($provinsi as $row){

								?> <option value="<?php echo $row->id; ?>" <?php if(isset($data)){  echo ($data->kota_id==$row->id) ? "selected" : ""; }  ?>><?php echo $row->nama; ?></option> <?php 
							}
						?>


					</select>
                </div>
            </div>
                <div class="row p-2 ">
                <div class="col-md-12">
                  <label for="inputPassword5" class="form-label">Satuan Kerja </label>
                    <select class="form-control  js-example-basic-single" name="satuankerja[]" id="satker" multiple="multiple">
                            <option value="">- Pilih Satuan Kerja  -</option>

                            <?php 
                              if(isset($data)){

                                $where =" AND 1=1";
                               
                                $provinsiKode = $this->Reff->get_kondisi(array("id"=>$data->provinsi_id),"provinsi","kode");
	                            	$kotaKode 	  = $this->Reff->get_kondisi(array("id"=>$data->kota_id),"kota","kode");
                                  if(!empty($kotaKode)){
                                    $where .=" AND kota_id='{$kotaKode}' ";
                                  }
                                $satker_kategori = $this->db->query("SELECT * from satker_kategori where id IN(SELECT kategori from satker where provinsi_id='{$provinsiKode}' $where )")->result();
                                foreach($satker_kategori as $satker){
                                  ?>

                                    <optgroup  label="<?php echo $satker->nama; ?>">  
                                    <?php       
                                      $satkerDb =  explode(",",$data->satker_id);
                                      $satkerData = $this->db->query("SELECT * from satker where kategori='{$satker->id}' and provinsi_id='{$provinsiKode}' $where order by nama ASC")->result();
                                      foreach($satkerData as $row){  
                                        $sel = "";
                                        if(in_array($row->id,$satkerDb)){
                                          $sel ="selected";
                                        }
                                        

                                    ?>                   
                                      <option value="<?php echo $row->id; ?>" <?php echo $sel; ?>><?php echo $row->kode; ?> - <?php echo $row->nama; ?></option>  
                                      <?php 
                                      }
                                      ?>                           
                                    </optgroup>   
                                    
                                    <?php 
                                }


                              }
                             ?> 
                           

                        </select>
                </div>
            </div>

            <div class="row p-2 ">
                <div class="col-md-6">
                  <label for="inputName5" class="form-label" id="judul">Tanggal Mulai Kegiatan </label>
                  <input type="text" class="form-control" name="f[tgl_mulai]" id="tgl_mulai" value="<?php echo isset($data) ? $data->tgl_mulai :"" ;  ?>">
                  <script>
																$(document).ready(function () {
																	startdate = new Date() - 18;
																	$('#tgl_mulai').datepicker({
																	
																			 changeMonth: true,
																								changeYear: true,
																								autoclose: true,
																								dateFormat: 'yy-mm-dd',
																								yearRange: "2022:2024",
																							
																	});
																});
																
															
															</script>
                </div>

                <div class="col-md-6">
                  <label for="inputName5" class="form-label" id="judul">Tanggal Berakhir Kegiatan </label>
                  <input type="text" class="form-control" name="f[tgl_selesai]" id="tgl_selesai" value="<?php echo isset($data) ? $data->tgl_selesai :"" ;  ?>">
                  <script>
																$(document).ready(function () {
																	startdate = new Date() - 18;
																	$('#tgl_selesai').datepicker({
																	
																			 changeMonth: true,
																								changeYear: true,
																								autoclose: true,
																								dateFormat: 'yy-mm-dd',
																								yearRange: "2022:2024",
																							
																	});
																});
																
															
															</script>
                </div>
            </div>

