<input type="hidden" id="kegiatan_id" value="<?php echo $kegiatan_id; ?>">
<div class="row p-2 ">
                <div class="col-md-6">
                    <table class="table">
                       <tr>
                        <td> Nama Kegiatan :</td>
                        <td>  <?php echo $kegiatan->judul; ?> </td>
                      </tr>

                      <tr>
                        <td> Wilayah :</td>
                        <td> <?php echo $this->Reff->get_kondisi(array("id"=>$kegiatan->provinsi_id),"provinsi","nama"); ?> <?php echo $this->Reff->get_kondisi(array("id"=>$kegiatan->kota_id),"kota","nama"); ?></td>
                      </tr>

                     
                      
                    </table>

                </div>

                <div class="col-md-6">
                    <table class="table">
                       <tr>
                        <td> Periode  :</td>
                        <td> <?php echo $this->Reff->formattanggalstring($kegiatan->tgl_mulai); ?>  - <?php echo $this->Reff->formattanggalstring($kegiatan->tgl_selesai); ?></td>
                      </tr>
                      <tr>
                        <td> Satuan Kerja  :</td>
                        <td>  
                            <ol start="1">
                              <?php 
                                $satker_id = explode(",",$kegiatan->satker_id);
                                  foreach($satker_id as $satker){

                                    ?><li><?php echo $this->Reff->get_kondisi(array("id"=>$satker),"satker","nama"); ?></li><?php 
                                  }
                                  ?>
                              
                            </ol>

                          
                        </td>
                      </tr>
                     
                      
                    </table>

                </div>
</div>
      
           
            
            <div class="row p-2 ">
                <div class="col-md-12">

                 <div class="table-responsive" id="load_sbm">
                 <table class="table table-hover table-striped table-bordered">
                   <thead>
                      <tr>
                       <th rowspan="2"> # </th>
                       <th rowspan="2"> Nama </th>
                       
                       <th rowspan="2"> Gol </th>
                       <th rowspan="2"> Jabatan </th>
                       <th rowspan="2"> HP </th>
                       <th colspan="4"> Perhitungan SBM </th>
                       <th rowspan="2"> Anggaran </th>
                     </tr>

                     <tr>
                       <th>BST</th>
                       <th>Transport</th>
                       <th>Hotel</th>
                       <th>UH</th>
                     </tr>

                   </thead>
                   <tbody>

                     <?php 
                       $pegawaiJabatan = $this->db->query("SELECT * from tr_kegiatanJabatan where kegiatan_id='".$kegiatan_id."'")->result();
                       $no=1;
                       $total=0;
                       $bst = 512000;
                         foreach($pegawaiJabatan as $r){
                           $dataPegawai = $this->db->get_where("pegawai",array("id"=>$r->pejabat_id))->row();
                           $total = $total + $r->anggaran;
                      ?>
                     <tr>
                       <td> <?php echo $no++; ?></td>
                       <td> <?php echo $dataPegawai->nama; ?></td>
                       
                       <td> <?php echo $dataPegawai->golongan; ?></td>
                        <td> <?php echo $this->Reff->get_kondisi(array("id"=>$r->jabatan),"jabatan","nama"); ?></td>
                       <td>


                         <input type="hidden" id="hari<?php echo $r->id; ?>" value="<?php echo $r->waktu; ?>">
                       <select class="hari form-control" data_id="<?php echo $r->id; ?>">
                          <option value="0">- 0 -</option>
                          <?php 
                            for($a=1;$a <=100;$a++){

                              ?><option value="<?php echo $a; ?>" <?php echo ($a==$r->waktu) ? "selected":""; ?>><?php echo $a; ?>  </option><?php 
                            }
                          ?>
                          
                      
                      </select>
                    
                    </td>

                    <td>
                      <input type="text" class="form-control keychange" value="<?php echo ($r->bst !=0) ? $this->Reff->formatuang2($r->bst) :   $this->Reff->formatuang2($bst); ?>" id="bst<?php echo $r->id; ?>" onkeyup="return FormatCurrency(this)" data_id="<?php echo $r->id; ?>">
                    </td>

                    <td>
                      <?php 
                        $transport = $this->db->get_where("sbm_pesawat",array("provinsi_tujuan"=>$kegiatan->provinsi_id))->row();
                      ?>
                      <input type="text" class="form-control keychange" value="<?php echo ($r->transport !=0) ? $this->Reff->formatuang2($r->transport) :   $this->Reff->formatuang2($transport->ekonomi); ?>" id="transport<?php echo $r->id; ?>" onkeypress="return FormatCurrency(this)" data_id="<?php echo $r->id; ?>">
                    </td>

                    <td>
                    <?php 
                        $golongan = explode("/",$dataPegawai->golongan);
                        $hotel = $this->db->get_where("sbm_hotel",array("provinsi_id"=>$kegiatan->provinsi_id,"golongan"=>$golongan[0]))->row();

                      ?>
                      <input type="text" class="form-control keychange" value="<?php echo ($r->hotel !=0) ? $this->Reff->formatuang2($r->hotel) :   $this->Reff->formatuang2($hotel->harga); ?>" id="hotel<?php echo $r->id; ?>" onkeypress="return FormatCurrency(this)" data_id="<?php echo $r->id; ?>">
                    </td>

                    <td>
                      <?php 
                        $uh = $this->db->get_where("sbm_uh",array("provinsi_id"=>$kegiatan->provinsi_id))->row();
                      ?>
                      <input type="text" class="form-control keychange" value="<?php echo ($r->uh !=0) ? $this->Reff->formatuang2($r->uh) :   $this->Reff->formatuang2($uh->harga); ?>" id="uh<?php echo $r->id; ?>" onkeypress="return FormatCurrency(this)" data_id="<?php echo $r->id; ?>">
                    </td>

                    <td>
                       <?php echo $this->Reff->formatuang2($r->anggaran); ?>
                    </td>


                     </tr>

                   
                     <?php 


                        }
                        ?>

                        <tr style="font-weight:bold">
                          <td colspan="9"> Total Anggaran </th>
                          <td> <?php echo $this->Reff->formatuang2($total); ?> </th>
                        </tr>
                     
                   </tbody>
                 </table>
                </div>
                 
				
                </div>
               
            </div>

            <center>
              <button type="button" id="saveAll" class="btn btn-outline-primary "> <i class="bi bi-box-arrow-in-up-right"></i>  Kirim Usulan Tim Penugasan </button>
            </center>
          

      <script>
            $(document).off("change",".hari").on("change",".hari",function(){
            
            var kegiatan_id  = $("#kegiatan_id").val();
            var id     = $(this).attr("data_id");         
            var hari   = $(this).val();
            var bst    = HapusTitik($("#bst"+id).val());
            var transport    = HapusTitik($("#transport"+id).val());
            var hotel    = HapusTitik($("#hotel"+id).val());
            var uh    = HapusTitik($("#uh"+id).val());
            
          loading();
          $.post("<?php echo site_url("jadwal/savePeriode"); ?>",{id:id,hari:hari,kegiatan_id:kegiatan_id,bst:bst,transport:transport,hotel:hotel,uh:uh},function(data){

            $("#load_sbm").html(data);
            jQuery.unblockUI({ });
          })
      

      });

      $(document).off("change",".keychange").on("change",".keychange",function(e){
           
      //  if (e.key === 'Enter' || e.keyCode === 13) {
            var kegiatan_id  = $("#kegiatan_id").val();
            var id     = $(this).attr("data_id");        
         
            var hari    = HapusTitik($("#hari"+id).val());
            var bst    = HapusTitik($("#bst"+id).val());
            var transport    = HapusTitik($("#transport"+id).val());
            var hotel    = HapusTitik($("#hotel"+id).val());
            var uh    = HapusTitik($("#uh"+id).val());
            
          loading();
          $.post("<?php echo site_url("jadwal/savePeriode"); ?>",{id:id,hari:hari,kegiatan_id:kegiatan_id,bst:bst,transport:transport,hotel:hotel,uh:uh},function(data){

            $("#load_sbm").html(data);
            jQuery.unblockUI({ });
          })


      // }
      

      });


      $(document).off("click","#saveAll").on("click","#saveAll",function(){
            var kegiatan_id  = $("#kegiatan_id").val();
          
            
      
          $.post("<?php echo site_url("jadwal/saveAll"); ?>",{kegiatan_id:kegiatan_id},function(data){

              location.href="<?php echo site_url('jadwal');?>";
          })
      

      });
 
      </script>

