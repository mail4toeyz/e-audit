<table class="table table-hover table-striped table-bordered">
                   <thead>
                      <tr>
                       <th rowspan="2"> # </th>
                       <th rowspan="2"> Nama </th>
                       
                       <th rowspan="2"> Gol </th>
                       <th rowspan="2"> Jabatan </th>
                       <th rowspan="2"> HP </th>
                       <th colspan="4"> Perhitungan SBM </th>
                       <th rowspan="2"> Anggaran </th>
                     </tr>

                     <tr>
                       <th>BST</th>
                       <th>Transport</th>
                       <th>Hotel</th>
                       <th>UH</th>
                     </tr>

                   </thead>
                   <tbody>

                   <?php 
                       $pegawaiJabatan = $this->db->query("SELECT * from tr_kegiatanJabatan where kegiatan_id='".$kegiatan_id."'")->result();
                       $no=1;
                       $total=0;
                       $bst = 512000;
                         foreach($pegawaiJabatan as $r){
                           $dataPegawai = $this->db->get_where("pegawai",array("id"=>$r->pejabat_id))->row();
                           $total = $total + $r->anggaran;
                      ?>
                     <tr>
                       <td> <?php echo $no++; ?></td>
                       <td> <?php echo $dataPegawai->nama; ?></td>
                       
                       <td> <?php echo $dataPegawai->golongan; ?></td>
                        <td> <?php echo $this->Reff->get_kondisi(array("id"=>$r->jabatan),"jabatan","nama"); ?></td>
                       <td>


                         <input type="hidden" id="hari<?php echo $r->id; ?>" value="<?php echo $r->waktu; ?>">
                       <select class="hari form-control" data_id="<?php echo $r->id; ?>">
                          <option value="0">- 0 -</option>
                          <?php 
                            for($a=1;$a <=100;$a++){

                              ?><option value="<?php echo $a; ?>" <?php echo ($a==$r->waktu) ? "selected":""; ?>><?php echo $a; ?>  </option><?php 
                            }
                          ?>
                          
                      
                      </select>
                    
                    </td>

                    <td>
                      <input type="text" class="form-control keychange" value="<?php echo ($r->bst !=0) ? $this->Reff->formatuang2($r->bst) :   $this->Reff->formatuang2($bst); ?>" id="bst<?php echo $r->id; ?>" onkeyup="return FormatCurrency(this)" data_id="<?php echo $r->id; ?>">
                    </td>

                    <td>
                      <?php 
                        $transport = $this->db->get_where("sbm_pesawat",array("provinsi_tujuan"=>$kegiatan->provinsi_id))->row();
                      ?>
                      <input type="text" class="form-control keychange" value="<?php echo ($r->transport !=0) ? $this->Reff->formatuang2($r->transport) :   $this->Reff->formatuang2($transport->ekonomi); ?>" id="transport<?php echo $r->id; ?>" onkeypress="return FormatCurrency(this)" data_id="<?php echo $r->id; ?>">
                    </td>

                    <td>
                    <?php 
                        $golongan = explode("/",$dataPegawai->golongan);
                        $hotel = $this->db->get_where("sbm_hotel",array("provinsi_id"=>$kegiatan->provinsi_id,"golongan"=>$golongan[0]))->row();

                      ?>
                      <input type="text" class="form-control keychange" value="<?php echo ($r->hotel !=0) ? $this->Reff->formatuang2($r->hotel) :   $this->Reff->formatuang2($hotel->harga); ?>" id="hotel<?php echo $r->id; ?>" onkeypress="return FormatCurrency(this)" data_id="<?php echo $r->id; ?>">
                    </td>

                    <td>
                      <?php 
                        $uh = $this->db->get_where("sbm_uh",array("provinsi_id"=>$kegiatan->provinsi_id))->row();
                      ?>
                      <input type="text" class="form-control keychange" value="<?php echo ($r->uh !=0) ? $this->Reff->formatuang2($r->uh) :   $this->Reff->formatuang2($uh->harga); ?>" id="uh<?php echo $r->id; ?>" onkeypress="return FormatCurrency(this)" data_id="<?php echo $r->id; ?>">
                    </td>

                    <td>
                       <?php echo $this->Reff->formatuang2($r->anggaran); ?>
                    </td>


                     </tr>

                   
                     <?php 


                        }
                        ?>

                        <tr style="font-weight:bold">
                          <td colspan="9"> Total Anggaran </th>
                          <td> <?php echo $this->Reff->formatuang2($total); ?> </th>
                        </tr>
                     
                   </tbody>
                 </table>