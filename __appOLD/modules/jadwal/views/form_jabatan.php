
        <input type="hidden" id="kegiatan_id" value="<?php echo $kegiatan_id; ?>">
      
        <div class="row p-2 ">
                
                <div class="col-md-4">
                  <label for="inputName5" class="form-label"> Penanggung Jawab</label>
                  <select class="form-control js-example-basic-single" name="penanggung_jawab" id="penanggung_jawab">
                              <option value="">- Pilih Penanggung Jawab  -</option>
                            
                              <?php 
                                $dataGroup = $this->db->query("SELECT * from unit_kerja where id IN(select unitkerja_id from pegawai where jabatan_id=1)")->result();
                                  foreach($dataGroup as $dg){
                              ?>

                              <optgroup  label="<?php echo $dg->nama; ?>">  
                             <?php       
                                    $dataPegawai = $this->db->query("SELECT * from pegawai where jabatan_id=1  and unitkerja_id='{$dg->id}' order by nama ASC")->result();
                                        foreach($dataPegawai as $row){   
                                    ?>                   
                                    <option value="<?php echo $row->id; ?>" <?php if(isset($data)){  echo ($data->penanggung_jawab==$row->id) ? "selected" : ""; }  ?>><?php echo $row->nip; ?> - <?php echo $row->nama; ?></option>  
                                    <?php 
                                        }
                                        ?>    
                              </optgroup> 
                              <?php 
                                  }
                                ?>
                                                      
                              


                         </select>
                </div>


                <div class="col-md-4">
                  <label for="inputName5" class="form-label"> Pengendali Mutu</label>
                  <select class="form-control js-example-basic-single" name="pengendali_mutu" id="pengendali_mutu">
                              <option value="">- Pilih Pengendali Mutu -</option>
                              <?php       
                                    $dataPegawai = $this->db->query("SELECT * from pegawai where jabatan_id=3  order by nama ASC")->result();
                                        foreach($dataPegawai as $row){   
                                    ?>                   
                                    <option value="<?php echo $row->id; ?>" <?php if(isset($data)){  echo ($data->pengendali_mutu==$row->id) ? "selected" : ""; }  ?>><?php echo $row->nip; ?> - <?php echo $row->nama; ?></option>  
                                    <?php 
                                        }
                                        ?>     
                                                      
                              


                         </select>
                </div>

                <div class="col-md-4">
                  <label for="inputName5" class="form-label"> Pengendali Teknis</label>
                  <select class="form-control js-example-basic-single" name="pengendali_teknis" id="pengendali_teknis">
                              <option value="">- Pilih Pengendali Teknis -</option>
                            <?php 
                                $dataGroup = $this->db->query("SELECT * from unit_kerja where id IN(select unitkerja_id from pegawai where jabatan_id=4)")->result();
                                  foreach($dataGroup as $dg){
                              ?>

                              <optgroup  label="<?php echo $dg->nama; ?>">  
                             <?php       
                                    $dataPegawai = $this->db->query("SELECT * from pegawai where jabatan_id=4  and unitkerja_id='{$dg->id}' order by nama ASC")->result();
                                        foreach($dataPegawai as $row){   
                                    ?>                   
                                    <option value="<?php echo $row->id; ?>" <?php if(isset($data)){  echo ($data->pengendali_teknis==$row->id) ? "selected" : ""; }  ?>><?php echo $row->nip; ?> - <?php echo $row->nama; ?></option>  
                                    <?php 
                                        }
                                        ?>    
                              </optgroup> 
                              <?php 
                                  }
                                ?> 
                                                                  
                              


                         </select>
                </div>


        </div>
           
            <div class="row p-2 ">
                <div class="col-md-12">
                  <label for="inputName5" class="form-label" >Ketua Tim </label>
                   <select class="form-control js-example-basic-single" name="ketua_tim" id="ketua_tim">
                              <option value="">- Pilih Ketua Tim -</option>
                              <?php 
                                $dataGroup = $this->db->query("SELECT * from unit_kerja where id IN(select unitkerja_id from pegawai where jabatan_id=2)")->result();
                                  foreach($dataGroup as $dg){
                              ?>

                              <optgroup  label="<?php echo $dg->nama; ?>">  
                             <?php       
                                    $dataPegawai = $this->db->query("SELECT * from pegawai where jabatan_id=2  and unitkerja_id='{$dg->id}' order by nama ASC")->result();
                                        foreach($dataPegawai as $row){   
                                    ?>                   
                                    <option value="<?php echo $row->id; ?>" <?php if(isset($data)){  echo ($data->ketua==$row->id) ? "selected" : ""; }  ?>><?php echo $row->nip; ?> - <?php echo $row->nama; ?></option>  
                                    <?php 
                                        }
                                        ?>    
                              </optgroup> 
                              <?php 
                                  }
                                ?>              
                              


                         </select>
                </div>
            </div>


            <div class="row p-2 ">
                <div class="col-md-12">
                   <div class="form-group">
										<label class="col-sm-3 control-label no-padding-top" for="duallist"> Anggota </label>

										<div class="col-sm-12">
											<select multiple="multiple" size="10" name="auditor[]" id="duallist">
												  <option value="" selected disabled> Dibawah ini adalah auditor yang terpilih sebagai anggota  </option>
												
												<?php 
                                               
											     $auditor = $this->db->query("select * from pegawai where jabatan_id=5 order by unitkerja_id asc")->result();

											      foreach($auditor as $r){
                                                      
                                                        $sel ="";
                                                          if(isset($data)){
                                                            $anggota = json_decode($data->anggota,true);
                                                           
                                                              if(in_array($r->id,$anggota)){
                                                                  $sel ="selected";
                                                              }
                                                           
                                                              
                                                          }

													  ?>
												   <option value="<?php echo $r->id; ?>" <?php echo $sel; ?>> <?php echo $r->nip; ?> -  <?php echo $r->nama; ?> - <?php echo  $this->Reff->get_kondisi(array("id"=>$r->unitkerja_id),"unit_kerja","nama"); ?></option>
												<?php 
												  } 
												  ?>
												
											</select>

											<div class="hr hr-16 hr-dotted"></div>
										</div>
									</div>

				
                </div>
               


                
            </div>


             


<script>
      $(document).ready(function() {
              $('.js-example-basic-single').select2({ width: '100%' });
                var demo1 = $('select[name="auditor[]"]').bootstrapDualListbox({infoTextFiltered: '<span class="label label-purple label-lg">Terpilih</span>'});
				var container1 = demo1.bootstrapDualListbox('getContainer');
				container1.find('.btn').addClass('btn-white btn-info btn-bold');
             });
</script>

