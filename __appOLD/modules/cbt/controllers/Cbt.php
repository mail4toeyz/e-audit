<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cbt extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		  if(!$this->session->userdata("siswaID")){
			    
				redirect(site_url('login/peserta'));
			
		  }
		  $this->load->helper('my');
		  $this->load->model('M_cbt');
		$this->Reff->zona_waktu();
	  }

	
	  public function index()
	{  
	    
		  
		  
	    		
      
		 $data['data']	  = $this->Reff->get_where("tm_siswa",array("id"=>$_SESSION['siswaID']));
		
		 $data['title']   = "Asesment Kompetensi Siswa";
		 
		 $this->load->view("cbt/header",$data);
		 $this->load->view('cbt/token');
		 $this->load->view("cbt/footer",$data);
	    
	

	}


	public function ujian()
	{
		
		$id 		= base64_decode($this->input->get('parameter', true));
		$kategori   = base64_decode($this->input->get('kategori', true));
		
		$ujian 		= $this->M_cbt->getUjianById($id);
		$soal 		= $this->M_cbt->getSoal($id);
		   
		
		$ujian 		= $this->M_cbt->getUjianById($id);
		$soal 		= $this->M_cbt->getSoal($id);
		
		

		$siswa     = $this->db->query("select * from  tm_siswa where id='".$_SESSION['siswaID']."'")->row();
		
		

		$h_ujian 	= $this->M_cbt->HslUjian($id,$siswa->id);
	
		$cek_sudah_ikut = $h_ujian->num_rows(); 

		if ($cek_sudah_ikut < 1) {
			$soal_urut_ok 	= array();
			$i = 0;
			foreach ($soal as $s) {
				$soal_per = new stdClass();
				$soal_per->id_soal 		= $s->id_soal;
				$soal_per->soal 		= $s->soal;
								
				$soal_per->opsi_a 		= $s->opsi_a;
				$soal_per->opsi_b 		= $s->opsi_b;
				$soal_per->opsi_c 		= $s->opsi_c;
				$soal_per->opsi_d 		= $s->opsi_d;
				$soal_per->opsi_e 		= $s->opsi_e;
				$soal_per->jawaban 		= $s->jawaban;
				$soal_urut_ok[$i] 		= $soal_per;
				$i++;
			}
			$soal_urut_ok 	= $soal_urut_ok;
			$list_id_soal	= "";
			$list_jw_soal 	= "";
			if (!empty($soal)) {
				foreach ($soal as $d) {
					$list_id_soal .= $d->id_soal.",";
					$list_jw_soal .= $d->id_soal.":::,";
				}
			}
			$list_id_soal 	= substr($list_id_soal, 0, -1);
			$list_jw_soal 	= substr($list_jw_soal, 0, -1);
			$waktu_selesai 	= $ujian->waktu * 60;
			$time_mulai		= date('Y-m-d H:i:s');

			//print_r($list_jw_soal); exit();

			$device = $this->Reff->mobile();


			$input = [
				'tmujian_id' 		=> $id,				
				'kategori' 		=> $kategori,				
				'tmsiswa_id'	=> $siswa->id,
				'list_soal'		=> $list_id_soal,
				'list_jawaban' 	=> $list_jw_soal,
				'jml_benar'		=> 0,
				'nilai'			=> 0,
				'nilai_bobot'	=> 0,
				'tgl_mulai'		=> $time_mulai,
				'waktu'      	=> $waktu_selesai,
				'device'	    => $device,
				'status'		=> 'Y'
			];
			
			
			$this->db->insert('h_ujian', $input);

			redirect('cbt/ujian?parameter='.base64_encode($id).'&kategori='.base64_encode($kategori), 'location', 301);
		}
		
		$q_soal = $h_ujian->row();
		
		$urut_soal 		= explode(",", $q_soal->list_jawaban);
		$soal_urut_ok	= array();
		for ($i = 0; $i < sizeof($urut_soal); $i++) {
			$pc_urut_soal	= explode(":",$urut_soal[$i]);
			$pc_urut_soal1 	= empty($pc_urut_soal[1]) ? "''" : "'{$pc_urut_soal[1]}'";
			$pc_urut_soal1  = str_replace(array("<br>","'",":","-","","(",")",",",".","-"),"",$pc_urut_soal1);
			//$ambil_soal 	= $this->M_cbt->ambilSoal($pc_urut_soal1, $pc_urut_soal[0]);
			//$soal_urut_ok[] = $ambil_soal; 
		}

		$detail_tes = $q_soal;
		$soal_urut_ok = $soal_urut_ok;

		$pc_list_jawaban = explode(",", $detail_tes->list_jawaban);
		$arr_jawab = array();
		
		foreach ($pc_list_jawaban as $v) {
			$pc_v 	= explode(":", $v);
			$idx 	= $pc_v[0];
			$val 	= $pc_v[1];
			$rg 	= $pc_v[2];

			$arr_jawab[$idx] = array("j"=>$val,"r"=>$rg);
		}








		$arr_opsi = array("a","b","c","d","e");
		$html = '';
			
		$no = 1;
		//print_r($soal_urut_ok); exit();
		if (!empty($soal)) {
			foreach ($soal as $s) {
				//$path = $s->folder."/";
				$jwbn       = isset($h_jawaban) ? $h_jawaban :"";
				$vrg = $arr_jawab[$s->id_soal]["r"] == "" ? "N" : $arr_jawab[$s->id_soal]["r"];
				$jwbn = $arr_jawab[$s->id_soal]["j"] == "" ? "" : $arr_jawab[$s->id_soal]["j"];
				$ket  = $arr_jawab[$s->id_soal]["r"] == "" ? "" : $arr_jawab[$s->id_soal]["r"];
				
				$html .= '<input type="hidden" name="id_soal_'.$no.'" value="'.$s->id_soal.'">';
				$html .= '<input type="hidden" name="jenis_'.$no.'" value="'.$s->jenis.'">';
				$html .= '<input type="hidden" name="jwbn'.$no.'" value="'.$jwbn.'">';
				$html .= '<input type="hidden" name="rg_'.$no.'" id="rg_'.$no.'" value="'.$vrg.'">';
				$html .= '<div class="step" id="widget_'.$no.'">';

				$html .= '<div class="papers"><div class="text-center"></div>'.$s->soal.'</div>';
				
				if($s->jenis==1){
					$html .= '<hr><div class="funkyradio">';
						for ($j = 0; $j < 5; $j++) {
							$opsi 			= "opsi_".$arr_opsi[$j];
							$file 			= "file_".$arr_opsi[$j];
							
							$pilihan_opsi 	= !empty($s->$opsi) ? trim($s->$opsi) : "";
							if($pilihan_opsi !=""){
								$checked 		= "";
							
							 if($jwbn==strtoupper($arr_opsi[$j])){
								$checked = "checked";
							

							 }
							$html .= '<div class="funkyradio-success" onclick="return simpan_sementara();">
								<input type="radio" id="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'"   name="jawaban_'.$no.'"  nomor="'.$no.'" value="'.strtoupper($arr_opsi[$j]).'" '.$checked.'> <label for="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'"><div class="huruf_opsi">'.$arr_opsi[$j].'</div> <p>'.$pilihan_opsi.'</p></label></div>';
							}
						}
						
					}else	if($s->jenis==9){
							$html .= '<hr><div class="funkyradio">';
								for ($j = 0; $j < 5; $j++) {
									$opsi 			= "opsi_".$arr_opsi[$j];
									$file 			= "file_".$arr_opsi[$j];
									
									$pilihan_opsi 	= !empty($s->$opsi) ? $s->$opsi : "";
									if($pilihan_opsi !=""){
										$checked 		= "";
									
									 if($jwbn==strtoupper($arr_opsi[$j])){
										$checked = "checked";
									
		
									 }
									$html .= '<div class="funkyradio-success" onclick="return simpan_sementara();">
										<input type="radio" id="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'"   name="jawaban_'.$no.'" nomor="'.$no.'" value="'.strtoupper($arr_opsi[$j]).'" '.$checked.'> <label for="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'"><div class="huruf_opsi">'.$arr_opsi[$j].'</div> <p>'.$pilihan_opsi.'</p></label></div>';
									}
								}
								
						
				}else if($s->jenis==2){
					$html .= '<hr><div class="funkyradio">';
					for ($j = 0; $j < 2; $j++) {
							$opsi 			= "opsi_".$arr_opsi[$j];
							$file 			= "file_".$arr_opsi[$j];
							$checked 		= "";
							$pilihan_opsi 	= !empty($s->$opsi) ? $s->$opsi : "";
							if($jwbn==$arr_opsi[$j]){
								$checked 		= "checked";

							 }
							

					  $html .= '<div class="funkyradio-success" >
								<input type="radio"  class="ayolah" nomor="'.$no.'" id="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'" name="jawaban_'.$no.'" value="'.strtoupper($arr_opsi[$j]).'" '.$checked.'> <label for="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'"><div class="huruf_opsi">'.$arr_opsi[$j].'</div> <p>'.$pilihan_opsi.'</p></label></div>';
					}
					
					
				}else if($s->jenis==4){

					if($jwbn !=""){
						
						$jawaban44 = explode("#",$jwbn);

					}

					
					$html .= '<hr><div class="row">';
					$opsi_a   = explode("[split]",$s->opsi_a);
					  
					  
					$opsi_b   = explode("[split]",$s->opsi_b);
					$opsi_c   = explode("[split]",$s->opsi_c);
					$opsi_d   = explode("[split]",$s->opsi_d);
					$opsi_e   = explode("[split]",$s->opsi_e);
					

					$opsi_a_per = isset($opsi_a[0]) ? $opsi_a[0]:"-";
					$opsi_b_per = isset($opsi_b[0]) ? $opsi_b[0]:"-";
					$opsi_c_per = isset($opsi_c[0]) ? $opsi_c[0]:"-";
					$opsi_d_per = isset($opsi_d[0]) ? $opsi_d[0]:"-";
					$opsi_e_per = isset($opsi_e[0]) ? $opsi_e[0]:"-";

					$opsi_a_j = isset($opsi_a[1]) ? $opsi_a[1]:"-";
					$opsi_b_j = isset($opsi_b[1]) ? $opsi_b[1]:"-";
					$opsi_c_j = isset($opsi_c[1]) ? $opsi_c[1]:"-";
					$opsi_d_j = isset($opsi_d[1]) ? $opsi_d[1]:"-";
					$opsi_e_j = isset($opsi_e[1]) ? $opsi_e[1]:"-";


					
					$pertanyaan    = array($opsi_a_per,$opsi_b_per,$opsi_c_per,$opsi_d_per,$opsi_e_per);
					
					$listjawaban  = array($opsi_a_j,$opsi_b_j,$opsi_c_j,$opsi_d_j,$opsi_e_j);
						
				   
					  $selek = '<select class="form-control ayolah" nomor="'.$no.'"  name="jawaban_'.$no.'[]" >';
					  $selek .= "<option value=''>- Pilih Jawaban Anda - </option>";
					  
						foreach($listjawaban as  $ij=>$jwb){
							$selekted44="";
							if($jwbn !=""){
								
								

								   if (in_array($ij, $jawaban44)){
							   
									  $selekted44="selected"; 
 
 
								     }
								}
							   
							
							
							//echo $checked; exit();
	

							$selek .= "<option value='".$ij."' ".$selekted44.">".$jwb."</option>";
						}

						$selek .= "</select>";

					  $html .= '<hr>  <div class="table-responsive"> <table class="table-hover table  ">';
				  
						$noempat=1;
						  foreach($pertanyaan as $ig=>$per){

							

							  
  
							  if(!empty($per)){
					  
								$html .= '<tr>';
								$html .= '<td align="left" width="3px">'.$noempat++.'. </td>';
								$html .= '<td align="left">'.$per.'</td>';
								$html .= '<td align="left">'.$selek.'</td>';
							  
							   
								$html .= '<tr>';
							  }
  
  
  
						  }
						  $html .= '</table>
						  
						  <textarea class="form-control" name="keterangan_'.$no.'" placeholder="Masukan alasan Anda ">'.$ket.'</textarea>
						 
						  </div>';
					
					
					
					
					
					
				}else if($s->jenis==3){
					$html .= '<hr><div class="rows">';
					$html .= '<div class="form-group"><textarea class="form-control ayolah" nomor="'.$no.'" name="jawaban_'.$no.'"  placeholder="Masukkan Jawaban Anda disini " >'.$jwbn.'</textarea></div>';
					
					
					
			   }else if($s->jenis==5){
					$html .= '<hr><div class="rows">';
					$html .= '<div class="form-group"><textarea class="form-control ayolah" nomor="'.$no.'" name="jawaban_'.$no.'"  placeholder="Masukkan Jawaban Anda disini " >'.$jwbn.'</textarea></div>';
					
					
					
				}else if($s->jenis==6){
					$arnumber = array("a"=>"1","b"=>"2","c"=>"3","d"=>"4","e"=>"5");
					if($jwbn !=""){
						
						$jawaban = explode("#",$jwbn);

					}

					$html .= '<hr><div class="funkyradio">';
					for ($j = 0; $j < 5; $j++) {
						$opsi 			= "opsi_".$arr_opsi[$j];
						$file 			= "file_".$arr_opsi[$j];
						$checked 		= "";

						if($jwbn !=""){
										
							if (in_array(strtoupper($arr_opsi[$j]), $jawaban)){
							   
								   $checked="checked";


							   }
						   
						   
						   
						   
				   }



						$pilihan_opsi 	= !empty($s->$opsi) ? $s->$opsi : "";
						if($pilihan_opsi !=""){
						$html .= '<div class="funkyradio-success">
							<input type="checkbox" id="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'" class="ayolah" nomor="'.$no.'"  name="jawaban_'.$no.'[]" value="'.strtoupper($arr_opsi[$j]).'" '.$checked.' > <label for="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'"><div class="huruf_opsi">'.$arnumber[$arr_opsi[$j]].'</div> <p>'.$pilihan_opsi.'</p></label></div>';
						}
					}
					
					



							
				}else if($s->jenis==7){
                    //echo 
					if($jwbn !=""){
						
						$jawaban = explode("#",$jwbn);

					}

					$html .= '<hr><div class="row">';
					$html .= '<hr>  <div class="table-responsive"> <table class="table-hover table  ">';
					$html .= '<tr>';
					$html .= '<td width="60%"></td>';
					$html .= ' <td align="center">'.$s->subjek1.'</td>';
					$html .= ' <td align="center">'.$s->subjek2.'</td>';
					$html .= '</tr>';
					$pilihanganda = array("A"=>"opsi_a","B"=>"opsi_b","C"=>"opsi_c","D"=>"opsi_d","E"=>"opsi_e");
						$jawabanganda = array("A"=>"file_a","B"=>"file_b","C"=>"file_c","D"=>"file_d","E"=>"file_e");
						$nt =0;
						foreach($pilihanganda as $ig=>$pg){
							$jj = $jawabanganda[$ig];

							
							
							$penanda = $nt;
							$subjek1 = (!empty($s->subjek1)) ? $s->subjek1.$penanda : "s1".$penanda; 
							$subjek2 = (!empty($s->subjek2))? $s->subjek2.$penanda : "s2".$penanda; 
							
							if(!empty($s->$pg)){
								$checked1="";
								$checked2="";
								
								if($jwbn !=""){
										
									     if (in_array($subjek1, $jawaban)){
											
												$checked1="checked";


											}
										
										if (in_array($subjek2, $jawaban)){
											
											$checked2="checked";


										}
									
										
										
								}
					          
					          $html .= '<tr>';
					          $html .= '<td>'.$s->$pg.'</td>';
					          $html .= '<td align="center"><input type="checkbox"  class="ayolah" nomor="'.$no.'" name="jawaban_'.$no.'[]" '.$checked1.' value="'.$subjek1.'" ></td>';
					          $html .= '<td align="center"><input type="checkbox"   class="ayolah" nomor="'.$no.'" name="jawaban_'.$no.'[]" '.$checked2.' value="'.$subjek2.'" ></td>';
					         
							  $html .= '<tr>';
							  $nt++;
							  

							}
							
							

						}
						$html .= '</table></div>';
						
					}else if($s->jenis==8){
							
					   $html .= '<hr><div class="row">';
						$html .= ' <div class="table-responsive"> <table class="table-hover table  ">';

						if($jwbn !=""){
						
							$jwbn8 = explode("#",$jwbn);
	
						}
					
						 
						  $pertanyaan   = explode("<br />",$s->jawaban_essay);
					  
							foreach($pertanyaan as $ig=>$per){

								
	
								if(!empty($per)){
									$jw ="";
									 if(isset($jwbn8[$ig])){
										$jw = (!empty($jwbn8[$ig])) ? $jwbn8[$ig] :"";

									 }
									
						
								  $html .= '<tr>';
								 
								  $html .= '<td align="left">'.$per.'</td>';
								  $html .= '<td align="left"><input type="text" class="form-control ayolah"  nomor="'.$no.'" name="jawaban_'.$no.'[]" value="'.$jw.'" placeholder="Ketik Jawaban Anda disini " ></td>';
								
								 
								  $html .= '<tr>';
								}
	
	
	
							}
							$html .= '</table></div>';
					}
				
				
				
				$html .= '</div></div>';
				$no++;
			}
		}

		$id_tes = ($detail_tes->id);

		$data = [
			
			'judul'		=> 'Ujian',
			'subjudul'	=> 'Lembar Ujian',
			'soal'		=> $detail_tes,
			'no' 		=> $no,
			'html' 		=> $html,
			'id_tes'	=> $id_tes,
			'waktu'  	=> $detail_tes->waktu
		];
		 $data['data']	      = $siswa;
		 $data['ujian']	      = $this->Reff->get_where("tm_ujian",array("id"=>$id));
		 $data['title']	      = "UJIAN - ".$data['ujian']->nama;
		 
		 $this->load->view("cbt/header",$data);
		 $this->load->view('cbt/sheet');
		 $this->load->view("cbt/footer",$data);
		 
	}
	

	public function cektoken()
	{
		$id    = $this->input->post('id_ujian', true);
		$token = $this->input->post('token', true);
		$cek   = $this->M_cbt->getUjianById($id);
		
		$data['status'] = $token === $cek->token ? TRUE : FALSE;
		$this->output_json($data);
	}
	
	public function simpan_satu()
	{
	
		$id_tes = $this->input->post('id', true);
		$id_tes = ($id_tes);
		
		$input 	= $this->input->post(null, true);
		$list_jawaban 	= "";
		for ($i = 1; $i < $input['jml_soal']; $i++) {
			$_tjawab 	= "jawaban_".$i;
			$_tidsoal 	= "id_soal_".$i;
			$_ragu 		= "rg_".$i;
			$jawaban_ 	= empty($input[$_tjawab]) ? "" : $input[$_tjawab];
			$list_jawaban	.= "".$input[$_tidsoal].":".$jawaban_.":".$input[$_ragu].",";
		}
		$list_jawaban	= substr($list_jawaban, 0, -1);
		$d_simpan = [
			'list_jawaban' => $list_jawaban
		];
	
		$this->db->update('h_ujian', $d_simpan,array("id"=>$id_tes));
		$this->output_json(['status'=>true]);
	
	}

	public function simpan_akhir()
	{
		// Decrypt Id
		$this->Reff->zona_waktu();
		
	
		$id_tes = $this->input->post('id', true);
		$id_tes = ($id_tes);
		$list_jawaban = $this->M_cbt->getJawaban($id_tes);
		
		// Pecah Jawaban
		$pc_jawaban = explode(",", $list_jawaban);
		
		$jumlah_benar 	= 0;
		$jumlah_salah 	= 0;
		$jumlah_ragu  	= 0;
		$nilai_bobot 	= 0;
		$total_bobot	= 0;
		$jumlah_soal	= sizeof($pc_jawaban);

		foreach ($pc_jawaban as $jwb) {
			$pc_dt 		= explode(":", $jwb);
			$id_soal 	= $pc_dt[0];
			$jawaban 	= $pc_dt[1];
			$ragu 		= $pc_dt[2];

			$cek_jwb 	 = $this->M_cbt->getSoalById($id_soal);
			
			if($cek_jwb->jenis==1){

				if($cek_jwb->jawaban==$jawaban){
					$jumlah_benar++;
					$nilai_bobot = $nilai_bobot + $cek_jwb->bobot;
					$total_bobot = $total_bobot + $cek_jwb->bobot;
				}else{
					$jumlah_salah++;
				} 

			}else if($cek_jwb->jenis==9){
				$arr_nilai = array("A"=>"1","B"=>"2","C"=>"3","D"=>"4","E"=>"5");
				$nilai_bobot = $nilai_bobot + $arr_nilai[$jawaban];
				$total_bobot = $total_bobot + $cek_jwb->bobot;
				
			}
			
		}

		$nilai 			= ($jumlah_benar / ($jumlah_soal))  * 100;
		$nilai_bobot 	= ($total_bobot / ($jumlah_soal))  * 100;

		$d_update = [
			'jml_benar'		=> $jumlah_benar,
			'tgl_selesai'	=> date("Y-m-d H:i:s"),
			'nilai'			=> $nilai,
			'nilai_bobot'	=> number_format(floor($nilai_bobot), 0),
			'status'		=> 'N'
		];
        
		$this->db->update('h_ujian', $d_update,array("id"=>$id_tes));



		
		$this->output_json(['status'=>TRUE, 'data'=>"sukses", 'id'=>$id_tes]);
	}
	
	
	 public function output_json($data, $encode = true)
	{
        if($encode) $data = json_encode($data);
        $this->output->set_content_type('application/json')->set_output($data);
	}
	
	
	public function save_essay(){
		
		$id_soal = $_POST['id_soal'];
		$nilai   = trim($_POST['nilai']);
		$datasoal = $this->db->get_where("tr_soal",array("id"=>$id_soal))->row();
		 $cek  = $this->db->get_where("tr_essay",array("id_soal"=>$id_soal,"tmsiswa_id"=>$_SESSION['tmsiswa_id']))->num_rows();
		   if($cek ==0){
			   
			   $this->db->set("jwb",$nilai);
			   $this->db->set("id_soal",$id_soal);
			   $this->db->set("tmsiswa_id",$_SESSION['tmsiswa_id']);
			   $this->db->insert("tr_essay");
		   }else{
			   
			   $this->db->set("jwb",$nilai);
			   $this->db->where("id_soal",$id_soal);
			   $this->db->where("tmsiswa_id",$_SESSION['tmsiswa_id']);
			   $this->db->update("tr_essay");
		   }
		   
		if($datasoal->jenis==3){
			
		 
         if(trim($datasoal->jawaban_essay)==$nilai){
			 echo "A";
			 
		 }else{
			echo "E"; 
			 
		 }
		 
		 
		}
		
		 
		
	}
	 
	 // Foto 

	public function ambilfoto(){


		$folder = $this->Reff->path();
		$destinationFolder = $_SERVER['DOCUMENT_ROOT'] . $folder; 
		$maxFileSize = 2 * 1024 * 1024;		
		
		$postdata = file_get_contents("php://input");
		
		if (!isset($postdata) || empty($postdata))
			exit(json_encode(["success" => false, "reason" => "Not a post data"]));
		
	
		$request = json_decode($postdata);
		
	
		if (trim($request->data) === "")
			exit(json_encode(["success" => false, "reason" => "Not a post data"]));
		
		
		$file = $request->data;
		
		
		$ext = '.png';
		
		$img = str_replace('data:image/png;base64,', '', $file);
		$img = str_replace('data:image/jpeg;base64,', '', $img);
		$img = str_replace(' ', '+', $img);
		
		
		$img = base64_decode($img);
		
		
		$filename = time(). $ext;
		
		
		$destinationPath = "$destinationFolder$filename";
		
		
		
		$success = file_put_contents($destinationPath, $img);
		
		if (!$success){
			exit(json_encode(['success' => false, 'reason' => 'the server failed in creating the image']));
		}else{ 
			$this->db->set("foto_awal",$filename);
			$this->db->where("id",$_SESSION['siswaID']);
			$this->db->update("tm_siswa");

		    exit(json_encode(['success' => true, 'path' => "berhasil"]));
		}


	}


   public function update_waktu(){
		  
		$id 	= $this->input->get_post("id");
		$waktu  = $this->input->get_post("waktu");

		$this->db->set("waktu",$waktu);
		$this->db->where("id",$id);
		$this->db->update("h_ujian");

		$data = $this->db->query("SELECT id,locked from tm_siswa where id='".$_SESSION['siswaID']."'")->row();
		  if($data->locked==1){
			echo "keluar";
		  }else{
			echo "sukses";
		  }
		


   }

   public function capturefoto($id_tes){


	$folder = $this->Reff->path();
	$destinationFolder = $_SERVER['DOCUMENT_ROOT'] . $folder; 
	$maxFileSize = 2 * 1024 * 1024;		
	
	$postdata = file_get_contents("php://input");
	
	if (!isset($postdata) || empty($postdata))
		exit(json_encode(["success" => false, "reason" => "Not a post data"]));
	

	$request = json_decode($postdata);
	

	if (trim($request->data) === "")
		exit(json_encode(["success" => false, "reason" => "Not a post data"]));
	
	
	$file = $request->data;
	
	$ext = '.png';
	
	$img = str_replace('data:image/png;base64,', '', $file);
	$img = str_replace('data:image/jpeg;base64,', '', $img);
	$img = str_replace(' ', '+', $img);
	
	
	$img = base64_decode($img);
	
	
	$filename = $_SESSION['siswaID'].time(). $ext;
	
	
	$destinationPath = "$destinationFolder$filename";
	
	
	
	$success = file_put_contents($destinationPath, $img);
	
	if (!$success){
		exit(json_encode(['success' => false, 'reason' => 'the server failed in creating the image']));
	}else{ 

		

		$foto_detail = $this->Reff->get_kondisi(array("id"=>$id_tes),"h_ujian","foto");
		$foto_detail = $foto_detail.",".$filename;
		$this->db->set("foto",$foto_detail);
		$this->db->where("id",$id_tes);
		$this->db->update("h_ujian");

		exit(json_encode(['success' => true, 'path' => "berhasil"]));
	}


}

  public function keluar(){

	  
	    $this->db->set("login",2);
		$this->db->where("id",$_SESSION['siswaID']);
		$this->db->update("tm_siswa");

		$this->session->sess_destroy();
		redirect(base_url()."login/peserta");

	
  }

  public function ulangi_foto(){

	  
	$this->db->set("foto_awal",'');
	$this->db->where("id",$_SESSION['siswaID']);
	$this->db->update("tm_siswa");

	
	redirect(base_url()."cbt");


}

public function bantuan(){

	  
	 $this->load->view("bantuan");


}

public function bantuan_save(){


	$this->db->set("tgl_pertanyaan",date("Y-m-d H:i:s"));
	$this->db->set("pertanyaan",$_POST['pertanyaan']);
	$this->db->set("tmsiswa_id",$_SESSION['siswaID']);
	$this->db->insert("bantuan");
	echo "sukses";
}
	

public function teguran(){

	$cek = $this->db->query("select * from teguran where status=1 and tmsiswa_id='{$_SESSION['siswaID']}' limit 1")->row();

	if(!is_null($cek)){

		echo $cek->teguran;
	}else{

		echo "";
	}


}

public function update_teguran(){

	$this->db->query("update teguran set status=0 where status=1 and tmsiswa_id='{$_SESSION['siswaID']}'");

	echo "sukses";
}
	 
}
