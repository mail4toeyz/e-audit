<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * The MIT License
 *
 * Copyright 2014 uchilaka.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


/** 
 * This script requires the following files from the google_api_php_client library
 * src/Google_Client.php
 * src/contrib/Google_DriveService.php
 * 
 * **/
define( 'BACKUP_FOLDER', 'eAuditItjen' );
define( 'SHARE_WITH_GOOGLE_EMAIL', 'elearning@madrasah.kemenag.go.id' );

require_once APPPATH .  'third_party' . DIRECTORY_SEPARATOR . 'google' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

class GoogleDrive2Exception extends Exception {
    public function __construct($message, $code, $previous) {
        parent::__construct($message, $code, $previous);
        die("GoogleDrive2 Library Exception:: " . $message);
    }
}
class Google_Drive {
	
	protected $scope = array('https://www.googleapis.com/auth/drive',
	'https://www.googleapis.com/auth/userinfo.profile'
	);
	private $_service;
	private $_youtube;
	private $_client;
	private $status_api;
	private $_oauth;
	public function __construct() {
		$client = new Google_Client();
		$client->setApplicationName('Elearning');
		$client->setScopes($this->scope);
		$client->setAuthConfig(__DIR__ . DIRECTORY_SEPARATOR .'credentials.json');
		$client->setAccessType('offline');
		$client->setPrompt('select_account consent');
		$tokenPath = __DIR__ . DIRECTORY_SEPARATOR .'token.json';
		if (file_exists($tokenPath)) {
			$accessToken = json_decode(file_get_contents($tokenPath), true);
			if(!empty($accessToken)){
				$client->setAccessToken($accessToken);
				$this->status_api = true;
			}else $this->status_api = false;
		}
		if ($client->isAccessTokenExpired()) {
			// Refresh the token if possible, else fetch a new one.
			if ($client->getRefreshToken()) {
				$client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
				$this->status_api = true;
				if (!file_exists(dirname($tokenPath))) {
					mkdir(dirname($tokenPath), 0700, true);
				}
				file_put_contents($tokenPath, json_encode($client->getAccessToken()));
			} else {
				$this->status_api = false;
			}
		}	
		$this->_client = $client;		
		$this->_service = new Google_Service_Drive($client);
		$this->_youtube = new Google_Service_YouTube($client);
		$this->_oauth =new Google_Service_Oauth2($client);
	}
	
	public function __get( $name ) {
		return $this->_service->$name;
	}
	public function getOuthData() {
		try{
			return $this->_oauth->userinfo->get();
		} catch (Exception $e) {
			return null;
		}
	}
	public function hapusToken() {
		$tokenPath = __DIR__ . DIRECTORY_SEPARATOR .'token.json';
		if (file_exists($tokenPath)) {
			unlink($tokenPath);
		}
		//file_put_contents($tokenPath, null);
	}
	public function createAuthUrl() {
		if($this->status_api) return "";
		else return $this->_client->createAuthUrl();
	}
	public function fetchAccessTokenWithAuthCode($authCode) {
		$tokenPath = __DIR__ . DIRECTORY_SEPARATOR .'token.json';
		if(!$this->status_api){
				$accessToken = $this->_client->fetchAccessTokenWithAuthCode($authCode);
				$this->_client->setAccessToken($accessToken);
				if (!file_exists(dirname($tokenPath))) {
					mkdir(dirname($tokenPath), 0700, true);
				}
				file_put_contents($tokenPath, json_encode($this->_client->getAccessToken()));
		}
	}
	public function statusApi(){
		return $this->status_api;
	}
	public function uploadYtb( $name, $description, $videoPath) {
		$snippet = new Google_Service_YouTube_VideoSnippet();
		$snippet->setTitle($name);
		$snippet->setDescription($description);
		$snippet->setTags(array("Elearning", $description));
		$snippet->setCategoryId("22");
		$status = new Google_Service_YouTube_VideoStatus();
		$status->privacyStatus = "unlisted";
		$video = new Google_Service_YouTube_Video();
		$video->setSnippet($snippet);
		$video->setStatus($status);
		$chunkSizeBytes = 1 * 1024 * 1024;
		$this->_client->setDefer(true);
		$insertRequest = $this->_youtube->videos->insert("status,snippet", $video);
		$media = new Google_Http_MediaFileUpload(
			$this->_client,
			$insertRequest,
			'video/*',
			null,
			true,
			$chunkSizeBytes
		);
		$media->setFileSize(filesize($videoPath));
		$status = false;
		$handle = fopen($videoPath, "rb");
		while (!$status && !feof($handle)) {
		  $chunk = fread($handle, $chunkSizeBytes);
		  $status = $media->nextChunk($chunk);
		}
		fclose($handle);
		$this->_client->setDefer(false); 
		return $status['id'];
	}
	
	public function createFile( $name, $mime, $description, $path, $parentId = null ) {
		$file = new Google_Service_Drive_DriveFile();
		$file->setName( $name );
		$file->setDescription( $description );
		$file->setMimeType( $mime );
		
		if( $parentId ) {
			$file->setParents( array( $parentId ) );
		}
		$chunkSizeBytes = 1 * 1024 * 1024;
		$this->_client->setDefer(true);
		$insertRequest = $this->_service->files->create($file); 
		$media = new Google_Http_MediaFileUpload(
			$this->_client,
			$insertRequest,
			'text/plain',
			null,
			true,
			$chunkSizeBytes
		);
		$media->setFileSize(filesize($path));
		$status = false;
		$handle = fopen($path, "rb");
		while (!$status && !feof($handle)) {
		  $chunk = fread($handle, $chunkSizeBytes);
		  $status = $media->nextChunk($chunk);
		}
		fclose($handle);
		$this->_client->setDefer(false); 
		return $status['id'];
	}
	public function updateFile($fileId, $name, $mime, $description, $path, $parentId = null ) {
		$file = new Google_Service_Drive_DriveFile();
		$file->setName( $name );
		$file->setDescription( $description );
		$file->setMimeType( $mime );		
		$chunkSizeBytes = 1 * 1024 * 1024;
		$this->_client->setDefer(true);
		$insertRequest = $this->_service->files->update($fileId,$file);
		$media = new Google_Http_MediaFileUpload(
			$this->_client,
			$insertRequest,
			'text/plain',
			null,
			true,
			$chunkSizeBytes
		);
		$media->setFileSize(filesize($path));
		$status = false;
		$handle = fopen($path, "rb");
		while (!$status && !feof($handle)) {
		  $chunk = fread($handle, $chunkSizeBytes);
		  $status = $media->nextChunk($chunk);
		}
		fclose($handle);
		$this->_client->setDefer(false); 
		return $status['id'];
	}
	

	public function prepareFolder($folder, $parent = NULL) {
	  $parent_id = $parent ? $this->prepareFolder($parent) : FALSE;
	  $params = array(
		'q' => "mimeType='application/vnd.google-apps.folder' and name = '" . $folder . "' and trashed = false
		",
	  );

	  if ($parent_id) {
		$params['q'] = $params['q'] . " and '" . $parent_id . "' in parents";
	  }

	  $directories = $this->_service->files->listFiles($params)->getFiles();
	  if (!empty($directories)) {
		return $directories[0]->getId();
	  }
	  else {
		// Create the folder and return the id.
		$file = new Google_Service_Drive_DriveFile();
		$file->setTitle($folder);
		$file->setDescription("Reese Creative Backup directory.");
		$file->setMimeType("application/vnd.google-apps.folder");
		if ($parent_id) {
		  $file->setParents(array($parent_id));
		}
		return $this->_service->files->create($file, array('fields' => "id"))->id;
	  }
	}	

	public function compressImage($source, $destination,$mime, $quality) { 
		//$imgInfo = getimagesize($source); 
		//$mime = $imgInfo['mime']; 
		try{
			switch($mime){ 
				case 'image/jpeg': 
					$image = imagecreatefromjpeg($source); 
					imagejpeg($image, $destination, $quality);
					break; 
				case 'image/png': 
					$image = imagecreatefrompng($source); 
					imagepng($image, $destination, round($quality/100));
					break; 
				case 'image/gif': 
					$image = imagecreatefromgif($source); 
					imagegif($image, $destination);
					break; 
				default: 
					$image = imagecreatefromjpeg($source); 
				   imagejpeg($image, $destination, $quality);
			} 
			return $destination; 
		} catch (Exception $e) {
			return $source;
		}
	}
	

	public function createFileFromPath( $path, $description, $parentId = null ) {
		$mimeType ="";
		try{
			
			
			$idx = explode( '.', $description );
			$count_explode = count($idx);
			$idx = strtolower($idx[$count_explode-1]);

			$mimet = array( 
				'txt' => 'text/plain',
				'htm' => 'text/html',
				'html' => 'text/html',
				'php' => 'text/html',
				'css' => 'text/css',
				'js' => 'application/javascript',
				'json' => 'application/json',
				'xml' => 'application/xml',
				'swf' => 'application/x-shockwave-flash',
				'flv' => 'video/x-flv',

				// images
				'png' => 'image/png',
				'jpe' => 'image/jpeg',
				'jpeg' => 'image/jpeg',
				'jpg' => 'image/jpeg',
				'gif' => 'image/gif',
				'bmp' => 'image/bmp',
				'ico' => 'image/vnd.microsoft.icon',
				'tiff' => 'image/tiff',
				'tif' => 'image/tiff',
				'svg' => 'image/svg+xml',
				'svgz' => 'image/svg+xml',
				
				 'PNG' => 'image/png',
				'JPE' => 'image/jpeg',
				'JPEG' => 'image/jpeg',
				'JPG' => 'image/jpeg',
				'GIF' => 'image/gif',
				'BMP' => 'image/bmp',
				'ICO' => 'image/vnd.microsoft.icon',
				'TIFF' => 'image/tiff',
				'TIFF' => 'image/tiff',
				'SVG' => 'image/svg+xml',
				'svgz' => 'image/svg+xml',

				// archives
				'zip' => 'application/zip',
				'rar' => 'application/x-rar-compressed',
				'exe' => 'application/x-msdownload',
				'msi' => 'application/x-msdownload',
				'cab' => 'application/vnd.ms-cab-compressed',

				// audio/video
				'mp3' => 'audio/mpeg',
				'mp3' => 'audio/mpeg',
				'qt' => 'video/quicktime',
				'mov' => 'video/quicktime',
				'mp4' => 'video/mp4',

				// adobe
				'pdf' => 'application/pdf',
				'PDF' => 'application/pdf',
				'psd' => 'image/vnd.adobe.photoshop',
				'ai' => 'application/postscript',
				'eps' => 'application/postscript',
				'ps' => 'application/postscript',

				// ms office
				'doc' => 'application/msword',
				'DOC' => 'application/msword',
				'rtf' => 'application/rtf',
				'xls' => 'application/vnd.ms-excel',
				'ppt' => 'application/vnd.ms-powerpoint',
				'docx' => 'application/msword',
				'DOCX' => 'application/msword',
				'xlsx' => 'application/vnd.ms-excel',
				'XLSX' => 'application/vnd.ms-excel',
				'pptx' => 'application/vnd.ms-powerpoint',


				// open office
				'odt' => 'application/vnd.oasis.opendocument.text',
				'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
			);

			if (isset( $mimet[$idx] )) {
			 $mimeType = $mimet[$idx];
			} else {
			 $mimeType = 'application/octet-stream';
			}
			 
			 
		} catch (Exception $e) {
			$fi = new finfo( FILEINFO_MIME );
			$mimeType0 = explode( ';', $fi->buffer(file_get_contents($path)));
			$mimeType = $mimeType0[0];
		}
		$imageTypes = array('jpg','png','jpeg','gif'); 
		$fileType = pathinfo($description, PATHINFO_EXTENSION);
		if(in_array($fileType, $imageTypes)){
			$imageTemp = $path;
			$imageUploadPath = "/var/www/html/tmp/";
			if(!file_exists($imageUploadPath)){
				mkdir($imageUploadPath);
			}
			$imageUploadPath = $imageUploadPath.$description;
			$compressedImage = $this->compressImage($imageTemp, $imageUploadPath,$mimeType, 75);
			if($compressedImage){				
				$path = $compressedImage;
			}
		}		 
		$fileName = preg_replace('/.*\//', '', $description );
		$chekfile = $this->listFilesFolders($fileName,$parentId,"files");
		if(count($chekfile) > 0){
			$fileID = $chekfile[0]["id"];
			return $this->updateFile($fileID, $fileName, $mimeType, $description, $path, $parentId );			
		}else{
			return $this->createFile( $fileName, $mimeType, $description, $path, $parentId );			
		}
	}
	
	
	public function createSubFolder( $name, $parent_id ) {
		$file = new Google_Service_Drive_DriveFile();
		$file->setName($name);
		$file->setDescription($name);
		$file->setMimeType("application/vnd.google-apps.folder");
		if ($parent_id) {;
		  $file->setParents(array($parent_id));
		}
		$createdFile =  $this->_service->files->create($file, array('fields' => "id"));
		return $createdFile['id'];

	}
	public function docreateFolder( $name, $mime, $description, $content, $parentId = null ) {
		$file = new Google_Service_Drive_DriveFile();
		$file->setName( $name );
		$file->setDescription( $description );
		$file->setMimeType( $mime );
		
		if( $parentId ) {
			$file->setParents( array( $parentId ) );
		}
		
		$createdFile = $this->_service->files->create($file, array(
				'data' => $content,
				'mimeType' => $mime,
		));
		
		return $createdFile['id'];
	}
	public function createFolder( $name, Google_ParentReference $fileParent = null ) {
		return $this->docreateFolder( $name, 'application/vnd.google-apps.folder', null, $fileParent);
	}
	public function readFileGdrive( $fileId) {
		return $this->_service->files->get($fileId);;
	}
	
	public function setPermissions( $fileId, $value, $role = 'writer', $type = 'user' ) {
		$perm = new Google_Service_Drive_Permission();
		$perm->setValue( $value );
		$perm->setType( $type );
		$perm->setRole( $role );
		
		$this->_service->permissions->insert($fileId, $perm);
	}
	public function setPublic( $fileId) {
		$perm1 = new Google_Service_Drive_Permission();
		//$perm1->setValue( "me" );
		$perm1->setType( "anyone" );
		$perm1->setRole( "reader" );
		
		$this->_service->permissions->create($fileId, $perm1);
	}
    public function listFilesFolders($search, $parentId, $type = 'all')
    {
        $query = '';
        // Checking if search is empty the use 'contains' condition if search is empty (to get all files or folders).
        // Otherwise use '='  condition
        $condition = $search!=''?'=':'contains';
        
        // Search all files and folders otherwise search in root or  any folder
		if($parentId == 'root') $query .="'root' in parents";
        else $query .= $parentId!='all'?"'".$parentId."' in parents":"";
        
        // Check if want to search files or folders or both
        switch ($type) {
            case "files":
                $query .= $query!=''?' and ':'';
                $query .= "mimeType != 'application/vnd.google-apps.folder' 
                            and name ".$condition." '".$search."'";
                break;

            case "folders":
                $query .= $query!=''?' and ':'';
                $query .= "mimeType = 'application/vnd.google-apps.folder' and name contains '".$search."'";
                break;
            default:
                $query .= "";
                break;
        }

        // Make sure that not list trashed files
        $query .= $query!=''?' and trashed = false':'trashed = false';
        $optParams = array('q' => $query,'pageSize' => 1000);

        // Returns the list of files and folders as object
        $results = $this->_service->files->listFiles($optParams)->getFiles();
        $result = array();
        // Return false if nothing is found
		if(is_array($results)){
       

        // Converting array to object
        
        foreach ($results as $file) {
            $result[] = array("id"=>$file->getId(),"name"=>$file->getName());
        }
        return $result;
	}
    }
	
	public function getFileFolderIdByName( $name,$fileParent ) {
		$files = $this->_service->children->listChildren($fileParent);
		//printf( json_encode($files));
		foreach( $files['items'] as $item ) {
			$datafile = $this->_service->files->get($item['id']);
			//printf($datafile['title']);
			if( $datafile['title'] == $name ) {
				return $item['id'];
			}
		}
		
		return false;
	}
	public function getFileIdByName( $name ) {
		$chekfolder = $this->listFilesFolders($name,'root',"folders");
		//echo json_encode($chekfolder);
		if(count($chekfolder) == 0) {
			return false;
		}else{
			if($chekfolder[0]["name"] == $name){
				//echo $chekfolder[0]["name"]."->".$chekfolder[0]["id"]."<br>";
				return $chekfolder[0]["id"];
			}
			return false;
		}		
		return false;
	}
	public function createMultiFolder($path,$parent_id) {
		$listfolder = explode(DIRECTORY_SEPARATOR,$path);
		if(count($listfolder) > 0){
			foreach($listfolder as $folder){
				$chekfolder = $this->listFilesFolders($folder,$parent_id,"folders");
				if(count($chekfolder) == 0) {
					$parent_id = $this->createSubFolder( $folder, $parent_id);
				}else{
					if($chekfolder[0]["name"] == $folder){
						$parent_id = $chekfolder[0]["id"];
					}else{
						$parent_id = $this->createSubFolder( $folder, $parent_id);
					}
				}
			}
			return $parent_id;	
		}else return null;
	}
	public function downloadFile($file) {
	  $downloadUrl = $file['selfLink'];
	  if ($downloadUrl) {
		$httpRequest = new Google_HttpRequest($downloadUrl, 'GET', null, null);
		//$httpRequest = $this->_service->get($request);
		if ($httpRequest->getResponseHttpCode() == 200) {
		  return $httpRequest->getResponseBody();
		} else {
		  // An error occurred.
		  return null;
		}
	  } else {
		// The file doesn't have any content stored on Drive.
		return null;
	  }
	}
	function deleteFile($fileId) {
	  try {
		$this->_service->files->delete($fileId);
		return true;
	  } catch (Exception $e) {
		 return false;
		//print "An error occurred: " . $e->getMessage();
	  }
	}

}

