<?php
$documentroot = $_SERVER["DOCUMENT_ROOT"]."/e-audit/";
?>
<style>

#bodinya { margin-right: 40px;
  margin-left: 60px; 
  font-family: Bookman Old Style;
font-size: 15px;
  }
</style>
<table width="100%"  style="margin-top:0px">
			  
			    <tr>
				  <td width="105px"> <img src="<?php echo $documentroot.'/__statics/img/kemenag.jpg';?>" style="width:100px"></td>
				    <td align="center"   valign="top">
				       <span style="font-size:19px;font-weight:bold;font-family:Bookman Old Style">KEMENTERIAN AGAMA REPUBLIK INDONESIA</span><br/>
					   <span style="font-size:17px;font-weight:bold;font-family:Bookman Old Style">INSPEKTORAT JENDERAL </span><br/>

					   <span style="font-size:12px;font-family:Bookman Old Style"> Jl. RS. Fatmawati Raya No.33A Cipete Jakarta 12420</span>
					   <span style="font-size:12px;font-family:Bookman Old Style">  Telepon : (021) 75916038-7697-853-08334 &nbsp; Faksimili : (021) 75916038-7697-853 </span>
					   <span style="font-size:12px;font-family:Bookman Old Style">   Website : www.itjen.kemenag.go.id </span>
					  </span>
				  </td>
				   <td width="20px">&nbsp;</td>
				</tr>
				
			 </table>
			 
<hr style="height: 2px;">
<div id="bodinya">
<center>SURAT TUGAS <br> Nomor : <?php echo $data->no_surat;  ?></center>
</br>&nbsp;</br>
<table>
<tr>
<td valign="top">Menimbang </td>
<td valign="top">:</td>
<td valign="top">
    <ol style="list-style-type: lower-latin;margin-top: 0px; text-align: justify;">
    <?php 
        $menimbang = explode("<br />",$data->menimbang);
          foreach($menimbang as $m){
              if(!empty($m)){

            ?><li><?php echo $m; ?></li><?php 
          }
        }
         

        ?>
    </ol>
        
    </td>
</tr>

<tr>
<td valign="top">Dasar </td>
<td valign="top">:</td>
<td valign="top">
    <ol style="list-style-type: decimal-leading-zero;margin-top: 0px; text-align: justify;">
    <?php 
        $dasar = explode("<br />",$data->dasar);
          foreach($dasar as $m){
              if(!empty($m)){

            ?><li><?php echo $m; ?></li><?php 
          }
        }
         

        ?>
    </ol>
        
    </td>
</tr>

<tr>
<td valign="top">Kepada  </td>
<td valign="top">:</td>
<td valign="top">
<center>Memberi Tugas </center>
      <table width="100%" style="font-size:14px;font-family: Bookman Old Style;">
          <tr>
                <td>No.</td>
                <td>Nama/NIP</td>
                <td>Jabatan</td>
                <td>Waktu Tugas</td>
         </tr>

         <?php 
                       $pegawaiJabatan = $this->db->query("SELECT * from tr_kegiatanJabatan where kegiatan_id='".$data->id."'")->result();
                       $no=1;
                      
                         foreach($pegawaiJabatan as $r){
                           $dataPegawai = $this->db->get_where("pegawai_simpeg",array("id"=>$r->pejabat_id))->row();
                           $total = $total + $r->anggaran;
                           if ($dataPegawai->NIP_BARU != ''){
                      ?>
                     <tr>
                       <td> <?php echo $no++; ?></td>
                       <td> <?php echo $dataPegawai->NAMA; ?> <br> <?php echo $dataPegawai->NIP_BARU; ?></td>                       
                        <td> <?php echo $this->Reff->get_kondisi(array("id"=>$r->jabatan),"jabatan","nama"); ?></td>
                       <td> <?php echo $r->waktu; ?> hari </td>
                           </tr>

                   
                     <?php 
                           }

                        }
                        ?>
      </table>
        
    </td>
</tr>


<tr>
<td valign="top">Untuk </td>
<td valign="top">:</td>
<td valign="top" style="text-align: justify;">
      Melaksanakan <?php echo $data->judul; ?>
        
    </td>
</tr>

<tr>
<td valign="top">Waktu  </td>
<td valign="top">:</td>
<td valign="top" style="text-align: justify;">
      Melaksanakan <?php echo $data->waktu; ?>
    </td>
</tr>

</table>

<br>

<table style="width:100%" class="mceItemTable">
     
        <tr>
          <td style="width:50%;">&nbsp;</td>
          <td>Jakarta, <?php echo $this->Reff->formattanggalstring($data->tgl_surat); ?> </td>
        </tr>
        <tr>
          <td style="width:50%;">&nbsp;</td>
          <td>Inspektur Jenderal,</td>
        </tr>
        <tr>
          <td style="width:50%;">&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
          <td style="width:50%;">&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
          <td style="width:50%;">&nbsp;</td>
          <td>Faisal</td>
        </tr>
       
     
    </table>




</div>