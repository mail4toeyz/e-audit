


<main id="main" class="main">
<div id="showform"></div>
<div class="pagetitle">
  <h1><?php echo $title; ?></h1>
  <nav>
	<ol class="breadcrumb">
	  <li class="breadcrumb-item"><a href="index.html">e-Audit</a></li>
	
	  <li class="breadcrumb-item active"><?php echo $title; ?></li>
	</ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
	<div class="col-lg-12">

	  <div class="card">
		<div class="card-body">
			<br>
		<div class="alert alert-warning alert-dismissible fade show" role="alert">
                <i class="bi bi-exclamation-triangle me-1"></i>
                Dibawah ini adalah data Kegiatan yang diusulkan oleh Tata Usaha Wilayah dan sudah disetujui oleh Inspektorat Jenderal dan Sekertariat dan Perencanaan, silahkan terbitkan Surat Tugas
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>

		 <h5 class="card-title">
				
			
			 			<div class="row " >
						 <div class="col-md-6">
										 <select class="form-control" required  id="jabatan_id">
																 <option value="">- Tampilkan Status -</option>
																 <?php 
															   $status = array("1"=>"Belum di Setujui","2"=>"Disetujui");
															     foreach($status as $r){
																	 
																	?><option value="<?php echo $r; ?>"> <?php echo ($r); ?></option><?php  
																	 
																 }
															?>
											</select>

						</div>
						 <div class="col-md-6">
						 


						 <div class="input-group">
							
							<input class="form-control border-end-0 border rounded-pill" type="search"   id="keyword"  placeholder="Cari  disini..">
							<span class="input-group-append">
								<button class="btn btn-outline-secondary bg-white border-bottom-0 border rounded-pill ms-n5" type="button">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
									
						</div>
								
					   </div>
					 
					   
		 </h5>
		 

		  <!-- Default Table -->
		  <div class="table-responsive">
		  <table class="table table-bordered table-striped  table-hover " id="datatableTable">
			<thead>
			  <tr>
				<th scope="col" rowspan="2">#</th>
				<th scope="col" rowspan="2">Aksi</th>
				<th scope="col" rowspan="2">Jenis</th>				
				<th scope="col" rowspan="2">Unit</th>				
				<th scope="col" rowspan="2">Judul</th>
				<th scope="col" rowspan="2">Provinsi </th>
				<th scope="col" rowspan="2">Kabupaten/Kota</th>
				<th scope="col" rowspan="2">Satuan Kerja</th>
				<th scope="col" rowspan="2"> Periode</th>
				<th colspan="5"> Jabatan Kegiatan </th>
				
			  </tr>
			  <tr>
				<th scope="col">Penanggung Jawab</th>
				<th scope="col">Pengendali Mutu</th>
				<th scope="col">Pengendali Teknis</th>
				<th scope="col">Ketua Tim</th>
				<th scope="col">Anggota </th>
				
			  </tr>
			</thead>
			<tbody>
			  
			</tbody>
		  </table>
		</div>
		 
		</div>
	  </div>


	</div>

  </div>
</section>

</main>
    
<div class="modal fade" id="basicModal" tabindex="-1">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title"></h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body" id="loadform">
					<p>Loading...</p>
                    </div>
                    <div class="modal-footer">
                     
                    </div>
                  </div>
                </div>
</div>

	
<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					
					
					"ajax":{
						url :"<?php echo site_url("bagumum/gridPenugasan"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						data.trkelas_id = $("#trkelas_id").val();
						data.keyword = $("#keyword").val();
						data.ajaran = $("#ajaran").val();
						
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
				
				$(document).on("input","#keyword",function(){
	  
				  dataTable.ajax.reload(null,false);	
				  
			  });
			  $(document).on("change","#trkelas_id,#ajaran",function(){
	  
				  dataTable.ajax.reload(null,false);	
				  
			  });
			  $(document).on("click",".btnMdl",function(){
				$('#basicModal').modal('show');

				id = $(this).attr('id').split("_");

					$.ajax({
						url: "<?php echo site_url("jadwal/getstatus"); ?>",
						type: "post",
						data: {id:id[1]} ,
						success: function (response) {
							$('#loadform').html(response); 
						},
						error: function(jqXHR, textStatus, errorThrown) {
						console.log(textStatus, errorThrown);
						}
					});
			})

			$(document).on("click", "#btnClose", function() {
				$('#basicModal').modal('hide');
				location.reload();
			})
	


</script>
				