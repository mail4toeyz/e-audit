<?php

class M_dashboard extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }


    public function grid($paging)
    {


        $this->db->select("*");
        $this->db->from('kegiatan_status');
        $this->db->where('status_approval', 1);
        $this->db->where_in('groups_id', array('6'));

        if ($paging == true) {
            $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
            $this->db->order_by("id", "DESC");
        }



        return $this->db->get();
    }
}
