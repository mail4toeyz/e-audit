<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Keuangan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata("is_login")) {

			echo $this->Reff->sessionhabis();
			exit();
		}
		$this->load->model('M_dashboard', 'm');
	}

	function _template($data)
	{
		$this->load->view('keuangan/page_header', $data);
	}

	public function index()
	{


		$ajax            = $this->input->get_post("ajax", true);

		$data['title']   = "Dashboard Keuangan  ";

		$data['categorie_xAxis'] = "";
		$data['json_anggota']    = "";
		$pie   					 = "";
		$data['title']           = "Persentase Jumlah Permadrasah ";
		$jenis = $this->db->get("jabatan")->result();

		foreach ($jenis as $row) {

			$jml = $this->db->query("SELECT count(id) as jml from pegawai where jabatan_id='{$row->id}'")->row();
			$pm = $jml->jml;
			$data['json_anggota'] .= "," . $pm;
			$data["categorie_xAxis"] .= ",'" . $row->nama . "";
			$tempo = array("INDEXES" => $row->nama, "Jumlah" => $pm);

			$pie          .= ",['" . $row->nama . "'," . (($pm / count($jenis)) * 100) . "]";


			$grid[] = $tempo;
		}


		$data['statistik'] = " Pendaftar SNPDB  ";
		$data['header']    = $data['statistik'];
		$data['categorie_xAxis'] = "Jumlah Pendaftar ";

		$data['json_pie_chart']  =  substr($pie, 1);
		$data['json_anggota']    = substr($data['json_anggota'], 1);

		$data['grid'] = $grid;
		if (!empty($ajax)) {

			$this->load->view('page_default', $data);
		} else {


			$data['konten'] = "page_default";

			$this->_template($data);
		}
	}



	public function penugasan()
	{




		$ajax            = $this->input->get_post("ajax", true);

		$data['title']   = "Data Usulan Penugasan ";
		if (!empty($ajax)) {

			$this->load->view('penugasan/page', $data);
		} else {


			$data['konten'] = "penugasan/page";

			$this->_template($data);
		}
	}

	public function gridPenugasan()
	{


		$iTotalRecords = $this->m->grid(false)->num_rows();

		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);

		$records = array();
		$records["data"] = array();

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;

		$datagrid = $this->m->grid(true)->result_array();


		$i = ($iDisplayStart + 1);
		foreach ($datagrid as $val) {

			$anggota = "<ol>";
			$dataAnggota = json_decode($val['anggota'], true);
			foreach ($dataAnggota as $r) {
				$anggota .= "<li>" . $this->Reff->get_kondisi(array("id" => $r), "pegawai_simpeg", "nama_lengkap") . "</li>";
			}

			$anggota .= "</ol>";

			$satkerData = "<ol>";
			$satker =  explode(",", $val['satker_id']);
			foreach ($satker as $r) {
				$satkerData .= "<li>" . $this->Reff->get_kondisi(array("id" => $r), "satker", "nama") . "</li>";
			}

			$satkerData .= "</ol>";




			if ($val['approval_perencanaan'] == 0) {
				$approval = '<button type="button" class="btn btn-danger ubah" datanya="' . $val['id'] . '" urlnya="' . site_url("keuangan/approval") . '" ><i class="bi bi-exclamation-octagon me-1"></i> Perlu Tindakan </button> ';
			}

			if ($val['approval_perencanaan'] == 1) {
				$approval = '<button type="button" class="btn btn-primary ubah" datanya="' . $val['id'] . '" urlnya="' . site_url("keuangan/approval") . '" ><i class="bi bi-check-circle me-"></i> Disetujui </button> ';
			}

			if ($val['approval_perencanaan'] == 2) {
				$approval = '<button type="button" class="btn btn-warning ubah" datanya="' . $val['id'] . '" urlnya="' . site_url("keuangan/approval") . '" ><i class="bi bi-exclamation-octagon me-1"></i> Ditolak </button> ';
			}

			$btnRealisasi = '<button type="button" class="btn btn-warning ubah" datanya="' . $val['id'] . '" urlnya="' . site_url("keuangan/realisasi") . '" ><i class="bi bi-folder"></i> Realisasi </button> ';


			$no = $i++;
			$records["data"][] = array(
				$no,


				$approval . " " .
					$btnRealisasi,
				$this->Reff->get_kondisi(array("id" => $val['jenis']), "tm_jenis", "nama"),
				$this->Reff->get_kondisi(array("id" => $val['unit_kerja_id']), "unit_kerja", "nama"),
				$val['judul'],
				$this->Reff->get_kondisi(array("id" => $val['provinsi_id']), "provinsi", "nama"),
				$this->Reff->get_kondisi(array("id" => $val['kota_id']), "kota", "nama"),
				$satkerData,
				$this->Reff->formattanggalstring($val['tgl_mulai']) . " - " . $this->Reff->formattanggalstring($val['tgl_selesai']),

				$this->Reff->get_kondisi(array("id" => $val['penanggung_jawab']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['penanggung_jawab']), "pegawai_simpeg", "nama_lengkap"),
				$this->Reff->get_kondisi(array("id" => $val['pengendali_mutu']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['pengendali_mutu']), "pegawai_simpeg", "nama_lengkap"),
				$this->Reff->get_kondisi(array("id" => $val['pengendali_teknis']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['pengendali_teknis']), "pegawai_simpeg", "nama_lengkap"),

				$this->Reff->get_kondisi(array("id" => $val['ketua']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['ketua']), "pegawai_simpeg", "nama_lengkap"),
				$anggota







			);
		}

		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;

		echo json_encode($records);
	}


	public function approval()
	{

		$id = $this->input->get_post("id");
		$data = array();
		if (!empty($id)) {

			$data['data']  = $this->Reff->get_where("kegiatan", array("id" => $id));
			$data['title']  =  "Approval Detail " . $data['data']->judul;
		}
		$this->load->view("penugasan/detail", $data);
	}

	public function realisasi()
	{

		$id = $this->input->get_post("id");
		$data = array();
		if (!empty($id)) {

			$data['data']  = $this->Reff->get_where("kegiatan", array("id" => $id));
			$data['title']  =  "Approval Detail " . $data['data']->judul;
		}
		$this->load->view("penugasan/realisasi", $data);
	}

	public function save()
	{

		$this->form_validation->set_message('required', '{field} Wajib diisi. ');
		$this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
		$this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
		$this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');



		$config = array(
			array('field' => 'f[approval_perencanaan]', 'label' => 'Status Approval ', 'rules' => 'trim|required'),
		);

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == true) {

			$id = $this->input->get_post("id", true);
			$f  = xssArray($this->input->get_post("f", true));

			$this->db->where("id", $id);
			$this->db->update("kegiatan", $f);
			echo "Data Berhasil disimpan";
		} else {


			header('Content-Type: application/json');
			echo json_encode(array('error' => true, 'message' => validation_errors()));
		}
	}

	public function savePeriode()
	{

		$kegiatan_id        = $this->input->get_post('kegiatan_id');
		$id                 = $this->input->get_post('id');
		$hari               = $this->input->get_post('hari');
		$bst               = $this->input->get_post('bst');
		$transport               = $this->input->get_post('transport');
		$hotel               = $this->input->get_post('hotel');
		$uh               = $this->input->get_post('uh');
		$rep               = $this->input->get_post('rep');

		$totalHotel = ($hari-1) * $hotel;
		$totaluh = $hari * $uh;
		$totalAnggaran = $bst + $transport + $totalHotel + $totaluh + $rep;

		// echo $totalAnggaran;
		// die();

		if ($hari == 0) {
			$totalAnggaran = 0;
		}

		$this->db->where("id", $id);
		$this->db->set("waktu", $hari);
		$this->db->set("anggaran", $totalAnggaran);
		$this->db->set("bst", $bst);
		$this->db->set("transport", $transport);
		$this->db->set("hotel", $hotel);
		$this->db->set("uh", $uh);
		$this->db->set("representatif", $rep);
		$this->db->update("tr_kegiatanJabatan");
		// echo $this->db->last_query();
		// die();

		$data["kegiatan_id"] =  $kegiatan_id;
		$data['kegiatan'] = $this->db->query("SELECT * from kegiatan where id='{$kegiatan_id}'")->row();
		$this->load->view("penugasan/load_sbm", $data);
	}

	public function saveRealisasi(){
		$kegiatan_id        = $this->input->get_post('kegiatan_id');
		$id                 = $this->input->get_post('id');
		$nilai               = $this->input->get_post('nilai');

		$this->db->where("kegiatan_id", $kegiatan_id);
		$this->db->where("id", $id);
		$this->db->set("uang_muka", $nilai);
		$this->db->update("tr_kegiatanJabatan");
		// echo $this->db->last_query();

		echo $this->Reff->formatuang2($nilai);

	}
}
