<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

      <li class="nav-item">
        <a class="nav-link " href="<?php echo site_url('satker'); ?>">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo site_url('satker/dataumum'); ?>">
           <i class="bi bi-pencil-square"></i>
          <span>Data Umum </span>
        </a>
      </li>

  <?php 
   $kegiatan		   = $this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
   
   if(!is_null($kegiatan)){
    ?>
      <li class="nav-heading"> Kegiatan</li>

      <li class="nav-item">
       <a class="nav-link collapsed" data-bs-target="#forms-nav" data-bs-toggle="collapse" href="#">
         <i class="bi bi-journal-text"></i><span>Proses Audit  </span><i class="bi bi-chevron-down ms-auto"></i>
       </a>
       <ul id="forms-nav" class="nav-content collapse  show" data-bs-parent="#sidebar-nav">
         <?php 
         $satker = $this->db->query("SELECT id,kode,nama from satker where id IN(".$_SESSION['satker_id'].")")->result();
         foreach($satker as $rsat){
         ?>
         <li>
           <a href="<?php echo site_url("kkaauditi/data?kode=".base64_encode($rsat->kode)); ?>">
             <i class="bi bi-circle"></i><span> Upload Dokumen KPI  </span>
           </a>
         </li>
         <?php 
         }
         $jmlNotisi 		  = $this->db->query("SELECT count(id) as jml from tr_notisi WHERE satker_id='{$_SESSION['satker_id']}' AND kegiatan_id='{$_SESSION['kegiatan_id']}' AND approval_dalnis=1")->row();
         ?>
         <li>
           <a href="<?php echo site_url("kkaauditi/dataNotisiSatker?kode=".base64_encode($rsat->kode)); ?>">
             <i class="bi bi-circle"></i><span> Notisi  <span class="badge bg-danger"><?php echo $jmlNotisi->jml; ?></span> </span>
           </a>
         </li>

         
       
       </ul>
     </li>

      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#tables-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-layout-text-window-reverse"></i><span>Permintaan Dokumen </span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="tables-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">

          <?php 
           // $dokumen    = $this->db->get_where("tr_persyaratan",array("kegiatan_id"=>$_SESSION['kegiatan_id']))->result();
            $dokumen    =  $this->db->query("SELECT * from tr_persyaratan where kegiatan_id='{$_SESSION['kegiatan_id']}' AND FIND_IN_SET(".$_SESSION['satker_id'].",satker_id)")->result();
              foreach($dokumen as $r){

            ?>
            <li>
            <a href="<?php echo site_url("satker/berkas/".base64_encode($r->id).""); ?>" class="menuajax" title="<?php echo $r->nama; ?>">
              <i class="bi bi-circle"></i><span><?php echo $r->nama; ?></span>
            </a>
          </li>
          
          <?php 
              }
              ?>

          
         
        </ul>
      </li><!-- End Tables Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo site_url("viconsatker"); ?>" class="menuajax" title="Video Conference">
        <i class="bi bi-menu-button-wide"></i>
          <span> Video Conference</span>
        </a>
      </li>

      <li class="nav-heading">Laporan </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo site_url("satker/laporan"); ?>" class="menuajax" title="Hasil Audit">
        <i class="fa fa-file"></i>
          <span> Hasil Audit</span>
        </a>
      </li>

      <?php 
   }
   ?>

      <li class="nav-heading">Pengaturan </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#">
          <i class="bi bi-dash-circle"></i>
          <span>Profile Anda</span>
        </a>
      </li><!-- End Error 404 Page Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo site_url("login"); ?>">
          <i class="bi bi-file-earmark"></i>
          <span>Logout</span>
        </a>
      </li><!-- End Blank Page Nav -->

    </ul>

  </aside><!-- End Sidebar-->