
<main id="main" class="main">

<div class="pagetitle">
  <h1><?php echo $title; ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.html">Permintaan Dokumen </a></li>
      <li class="breadcrumb-item active"><?php echo $title; ?></li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section dashboard">
  <div class="row">
  <div class="col-lg-12">

    <div class="card">
            <div class="card-body">
              <br>
             
           
              <input id="file" name="file" type="file" multiple accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps" />

            </div>
      </div>
      </div>
     </div>
</section>

</main>

<script type="text/javascript">
  
 

  $(document).ready(function() {

$("#file").fileinput({
          uploadUrl: "<?php echo site_url("satker/berkas_upload"); ?>",
          overwriteInitial: false,
          
          browseClass: "btn btn-success",
          browseLabel: "<?php echo $persyaratan->nama; ?>",
          browseIcon: "<i class=\"fa fa-file-signature\"></i> Ambil File ",
          removeClass: "btn btn-danger",
          removeLabel: "Delete",
          removeIcon: "<i class=\"fa fa-trash\"></i>  Hapus File",
          uploadClass: "btn btn-info",
          uploadLabel: "Upload",
          uploadIcon: "<i class=\"fa fa-upload\"></i>  Upload Semua File",
          initialPreviewAsData: true,
                  uploadExtraData: function() {
                      return {
                        
                          persyaratan_id: "<?php echo $persyaratan->id; ?>",
                      };
                  },
                  initialPreview: [
                      <?php 
                          
                          $eksis = $this->db->query("select id,file from tr_permintaan_dokumen where kegiatan_id='".$_SESSION['kegiatan_id']."' and trpersyaratan_id='".$persyaratan->id."' and satker_id='".$_SESSION['satker_id']."' ")->result();
                              $lihat="";
                              if(!is_null($eksis)){
                                  foreach($eksis as $r){
                                      
                                      $lihat .= ",'https://drive.google.com/file/d/".$r->file."/preview'";
                                  }
                                  
                                  
                              }
                          
                          echo substr($lihat,1);
                          
                          
                          ?>
                          
                          
                      ],
                              initialPreviewConfig: [

                            <?php 
                           $eksis = $this->db->query("select id,file from tr_permintaan_dokumen where kegiatan_id='".$_SESSION['kegiatan_id']."' and trpersyaratan_id='".$persyaratan->id."' and satker_id='".$_SESSION['satker_id']."' ")->result();
                           $lihat="";
                              if(!is_null($eksis)){
                              
                                foreach($eksis as $r){
                                
                                  
                                  $lihat .= ',{caption: "", size:200, width: "100%", url: "'.site_url("satker/hapus_berkas").'", key: "'.$r->id.'"}';
                                }
                                
                                
                              }
                            
                            echo substr($lihat,1);


                            ?>


                                
                              ]
                 
                          


                              
                            });





                              });		
                              

                            </script>
