<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masterkegiatan extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 
		  isLogin();
		  $this->load->model('M_referensi','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('admin/page_header',$data);	
	}
		
	public function jenis()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Jenis Pengawasan";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid_jenis(){
		
		  $iTotalRecords = $this->m->grid_jenis(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid_jenis(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			    
			   
				$no = $i++;
				$records["data"][] = array(
					$no,
			
              	
					$val['nama'],
					
					
					' 
					<div class="btn-group" role="group">
					<button type="button" class="btn btn-outline-primary ubahmodal" datanya="'.$val['id'].'" urlnya="'.site_url("masterkegiatan/form_jenis").'" data-bs-toggle="modal" data-bs-target="#basicModal" target="#loadform">
                                    <i class="fa fa-pencil"></i>
                      </button>
					  <button type="button"  class="btn btn-outline-danger btn-sm hapus" datanya="'.$val['id'].'" urlnya="'.site_url("masterkegiatan/hapus_jenis").'">
                                    <i class="fa fa-trash"></i>
					  </button>
					  </div>
					 
					  '
				  
				    

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	

	

	public function form_jenis(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("jenis",array("id"=>$id));
			   
		   }
		$this->load->view("form",$data);
		
	}
	
	
	
	public function save_jenis(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
			    $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
    
		 
			
				$config = array(
				    
				   
				  
				    array('field' => 'f[nama]', 'label' => 'Nama    ', 'rules' => 'trim|required'),
				  	   
			
				   
				  
				   
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
				
				
				 
				
							 if(empty($id)){
								
								 
								   $this->db->insert("jenis",$f);
								   echo "Data Berhasil disimpan";	
								 
								
								
							 }else{
								$this->db->where("id",$id);
								$this->db->update("jenis",$f);
								echo "Data Berhasil disimpan";	
								 
							 }
							 
						
							
						
		
		
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}

	public function hapus_jenis(){
		
		$id = $this->input->get_post("id",true);
		
		$this->db->delete("jenis",array("id"=>$id));
		echo "sukses";
	}


	// SubJenis Pengawasan 

	public function subjenis()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Subjenis Pengawasan";
	     if(!empty($ajax)){
					    
			 $this->load->view('subjenis/page',$data);
		
		 }else{
			 
		     $data['konten'] = "subjenis/page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid_subjenis(){
		
		  $iTotalRecords = $this->m->grid_subjenis(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid_subjenis(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			    
			   
				$no = $i++;
				$records["data"][] = array(
					$no,
			
              	
				
					$this->Reff->get_kondisi(array("id"=>$val['jenis_id']),"jenis","nama"),
					
					$val['nama'],
					
					
					' 
					<div class="btn-group" role="group">
					<button type="button" class="btn btn-outline-primary ubahmodal" datanya="'.$val['id'].'" urlnya="'.site_url("masterkegiatan/form_subjenis").'" data-bs-toggle="modal" data-bs-target="#basicModal" target="#loadform">
                                    <i class="fa fa-pencil"></i>
                      </button>
					  <button type="button"  class="btn btn-outline-danger btn-sm hapus" datanya="'.$val['id'].'" urlnya="'.site_url("masterkegiatan/hapus_subjenis").'">
                                    <i class="fa fa-trash"></i>
					  </button>
					  </div>
					 
					  '
				  
				    

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	

	

	public function form_subjenis(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("jenis_sub",array("id"=>$id));
			   
		   }
		$this->load->view("subjenis/form",$data);
		
	}
	
	
	
	public function save_subjenis(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
			    $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
    
		 
			
				$config = array(
				    
				   
				    array('field' => 'f[jenis_id]', 'label' => 'Jenis Pengawasan ', 'rules' => 'trim|required'),
				    array('field' => 'f[nama]', 'label' => 'Sub Jenis Pengawasan  ', 'rules' => 'trim|required'),
				  	   
			
				   
				  
				   
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
				
				
				 
				
							 if(empty($id)){
								
								 
								   $this->db->insert("jenis_sub",$f);
								   echo "Data Berhasil disimpan";	
								 
								
								
							 }else{
								$this->db->where("id",$id);
								$this->db->update("jenis_sub",$f);
								echo "Data Berhasil disimpan";	
								 
							 }
							 
						
							
						
		
		
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}

	public function hapus_subjenis(){
		
		$id = $this->input->get_post("id",true);
		
		$this->db->delete("jenis_sub",array("id"=>$id));
		echo "sukses";
	}

	// Sektor 

	public function sektor()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Master Sektor";
	     if(!empty($ajax)){
					    
			 $this->load->view('sektor/page',$data);
		
		 }else{
			 
		     $data['konten'] = "sektor/page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid_sektor(){
		
		  $iTotalRecords = $this->m->grid_sektor(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid_sektor(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			    
			   
				$no = $i++;
				$records["data"][] = array(
					$no,
			
              	
					$val['nama'],
					
					
					' 
					<div class="btn-group" role="group">
					<button type="button" class="btn btn-outline-primary ubahmodal" datanya="'.$val['id'].'" urlnya="'.site_url("masterkegiatan/form_sektor").'" data-bs-toggle="modal" data-bs-target="#basicModal" target="#loadform">
                                    <i class="fa fa-pencil"></i>
                      </button>
					  <button type="button"  class="btn btn-outline-danger btn-sm hapus" datanya="'.$val['id'].'" urlnya="'.site_url("masterkegiatan/hapus_sektor").'">
                                    <i class="fa fa-trash"></i>
					  </button>
					  </div>
					 
					  '
				  
				    

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	

	

	public function form_sektor(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("sektor",array("id"=>$id));
			   
		   }
		$this->load->view("sektor/form",$data);
		
	}
	
	
	
	public function save_sektor(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
			    $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
    
		 
			
				$config = array(
				    
				   
				  
				    array('field' => 'f[nama]', 'label' => 'Nama    ', 'rules' => 'trim|required'),
				  	   
			
				   
				  
				   
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
				
				
				 
				
							 if(empty($id)){
								
								 
								   $this->db->insert("sektor",$f);
								   echo "Data Berhasil disimpan";	
								 
								
								
							 }else{
								$this->db->where("id",$id);
								$this->db->update("sektor",$f);
								echo "Data Berhasil disimpan";	
								 
							 }
							 
						
							
						
		
		
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}

	public function hapus_sektor(){
		
		$id = $this->input->get_post("id",true);
		
		$this->db->delete("sektor",array("id"=>$id));
		echo "sukses";
	}

	// Topik 

	public function topik()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Master Topik";
	     if(!empty($ajax)){
					    
			 $this->load->view('topik/page',$data);
		
		 }else{
			 
		     $data['konten'] = "topik/page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid_topik(){
		
		  $iTotalRecords = $this->m->grid_topik(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid_topik(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			    
			   
				$no = $i++;
				$records["data"][] = array(
					$no,
			
					$this->Reff->get_kondisi(array("id"=>$val['sektor_id']),"sektor","nama"),
					$val['nama'],
					
					
					' 
					<div class="btn-group" role="group">
					<button type="button" class="btn btn-outline-primary ubahmodal" datanya="'.$val['id'].'" urlnya="'.site_url("masterkegiatan/form_topik").'" data-bs-toggle="modal" data-bs-target="#basicModal" target="#loadform">
                                    <i class="fa fa-pencil"></i>
                      </button>
					  <button type="button"  class="btn btn-outline-danger btn-sm hapus" datanya="'.$val['id'].'" urlnya="'.site_url("masterkegiatan/hapus_topik").'">
                                    <i class="fa fa-trash"></i>
					  </button>
					  </div>
					 
					  '
				  
				    

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	

	

	public function form_topik(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("topik",array("id"=>$id));
			   
		   }
		$this->load->view("topik/form",$data);
		
	}
	
	
	
	public function save_topik(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
			    $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
    
		 
			
				$config = array(
				    
				   
				  
				    array('field' => 'f[sektor_id]', 'label' => 'Sektor    ', 'rules' => 'trim|required'),
				    array('field' => 'f[nama]', 'label' => 'Topik    ', 'rules' => 'trim|required'),
				  	   
			
				   
				  
				   
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
				
				
				 
				
							 if(empty($id)){
								
								 
								   $this->db->insert("topik",$f);
								   echo "Data Berhasil disimpan";	
								 
								
								
							 }else{
								$this->db->where("id",$id);
								$this->db->update("topik",$f);
								echo "Data Berhasil disimpan";	
								 
							 }
							 
						
							
						
		
		
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}

	public function hapus_topik(){
		
		$id = $this->input->get_post("id",true);
		
		$this->db->delete("topik",array("id"=>$id));
		echo "sukses";
	}
	

	 
}
