<?php

class M_referensi extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
    }

    
	public function grid_jenis($paging){
      
		
	    $keyword      = $this->input->post("keyword",true);
	  
	 
	 
		$this->db->select("*");
        $this->db->from('jenis');
	
        
	  
	    if(!empty($keyword)){  $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'");    }
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 
					 $this->db->order_by("id","ASC");
					
		
			 }
      
		
		
		return $this->db->get();
		
	}

	public function grid_subjenis($paging){
      
		
	    $keyword      = $this->input->post("keyword",true);
	  
	 
	 
		$this->db->select("*");
        $this->db->from('jenis_sub');
	
        
	  
	    if(!empty($keyword)){  $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'");    }
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 
					 $this->db->order_by("jenis_id","ASC");
					 $this->db->order_by("nama","ASC");
					
		
			 }
      
		
		
		return $this->db->get();
		
	}

	public function grid_sektor($paging){
      
		
	    $keyword      = $this->input->post("keyword",true);
	  
	 
	 
		$this->db->select("*");
        $this->db->from('sektor');
	
        
	  
	    if(!empty($keyword)){  $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'");    }
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 
					 $this->db->order_by("id","ASC");
					
		
			 }
      
		
		
		return $this->db->get();
		
	}


	public function grid_topik($paging){
      
		
	    $keyword      = $this->input->post("keyword",true);
	  
	 
	 
		$this->db->select("*");
        $this->db->from('topik');
	
        
	  
	    if(!empty($keyword)){  $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'");    }
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 
					 $this->db->order_by("sektor_id","ASC");
					 $this->db->order_by("nama","ASC");
					
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
}
