
<script type="text/javascript" src="<?php echo base_url(); ?>__statics/ckeditor/ckeditor.js" ></script> 
             <div class="alert alert-info alert-dismissible fade show" role="alert">
                <i class="bi bi-info-circle me-1"></i>
                <?php echo $kka->nama; ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              <br>
            <form class="row g-3" method="post" action="<?php echo site_url('notisi/saveNotisiData'); ?>">
                <div class="col-12">
                    <input type="hidden" name="satker_id" value="<?php echo $satker_id; ?>">
                    <input type="hidden" name="kka_id" value="<?php echo $kka->id; ?>">
                    <table class="table table-bordered">
                        <tr>
                            <td width="20%"> Kode Temuan </td>
                            
                            <td> 
                                <?php 
                                $temuan = $this->db->get_where("tm_temuan",array("id"=>$notisi->kode_temuan))->row();
                                echo "<b>".$temuan->sub_kel."-".$this->Reff->get_kondisi(array("sub_kel" =>$temuan->sub_kel,"kel"=>1), "tm_temuan", "deskripsi")."</b>";
                                echo "<br>"; 
                                echo $temuan->jenis."-".$temuan->deskripsi;
                                ?>
                                
                            </td>
                        </tr>

                        <tr>
                            <td width="20%"> Kode Sebab </td>
                            
                            <td> 
                                <?php 
                                $temuan = $this->db->get_where("tm_temuan",array("id"=>$notisi->kode_sebab))->row();
                                echo "<b>".$temuan->sub_kel."-".$this->Reff->get_kondisi(array("sub_kel" =>$temuan->sub_kel,"kel"=>2), "tm_temuan", "deskripsi")."</b>";
                                echo "<br>"; 
                                echo $temuan->jenis."-".$temuan->deskripsi;
                                ?>
                                
                            </td>
                        </tr>

                        <tr>
                            <td width="20%"> Kode Rekomendasi </td>
                            
                            <td> 
                                <?php 
                                $temuan = $this->db->get_where("tm_temuan",array("id"=>$notisi->kode_rekomendasi))->row();
                                echo "<b>".$temuan->sub_kel."-".$this->Reff->get_kondisi(array("sub_kel" =>$temuan->sub_kel,"kel"=>3), "tm_temuan", "deskripsi")."</b>";
                                echo "<br>"; 
                                echo $temuan->jenis."-".$temuan->deskripsi;
                                ?>
                                
                            </td>
                        </tr>
                        <tr>
                            <td width="20%"> Kondisi  </td>
                            
                            <td> 
                                <?php 
                                echo $notisi->kondisi;
                                ?>
                                
                            </td>
                        </tr>
                        <tr>
                            <td width="20%"> Kriteria  </td>
                            
                            <td> 
                                <?php 
                                echo $notisi->kriteria;
                                ?>
                                
                            </td>
                        </tr>
                        <tr>
                            <td width="20%"> Akibat  </td>
                            
                            <td> 
                                <?php 
                                echo $notisi->akibat;
                                ?>
                                
                            </td>
                        </tr>
                        <tr>
                            <td width="20%"> Sebab  </td>
                            
                            <td> 
                                <?php 
                                echo $notisi->sebab;
                                ?>
                                
                            </td>
                        </tr>
                        <tr>
                            <td width="20%"> Rekomendasi  </td>
                            
                            <td> 
                                <?php 
                                echo $notisi->rekomendasi;
                                ?>
                                
                            </td>
                        </tr>
                        <tr>
                            <td width="20%"> Review Ketua   </td>
                            
                            <td> 
                                <?php 
                                $hasil = array("1"=>"Disetujui","2"=>"Ditolak");
                                echo '<span class="badge bg-success"><i class="bi bi-check-circle me-1"></i> '.$hasil[$notisi->approval_ketua].'</span>';
                                echo "<hr>";
                                echo $notisi->catatan_ketua;
                                ?>
                                
                            </td>
                        </tr>

                        <tr>
                            <td width="20%"> Review Dalnis   </td>
                            
                            <td> 
                                <?php 
                                $hasil = array("1"=>"Disetujui","2"=>"Ditolak");
                                echo '<span class="badge bg-success"><i class="bi bi-check-circle me-1"></i> '.$hasil[$notisi->approval_dalnis].'</span>';
                            
                                echo "<hr>";
                                echo $notisi->catatan_dalnis;
                                ?>
                                
                            </td>
                        </tr>
                    </table>
                    <?php 
                    if($_SESSION['group_id']=="ketua"){
                      
                        ?>
                    <div class="col-12">
                    <label for="inputNanme4" class="form-label">Hasil Review  </label>
                        <select class="form-control" name="f[approval_ketua]"  required>
                            <option value="">- Pilih Hasil -</option>
                            <?php
                        
                            $hasil = array("1"=>"Setujui","2"=>"Tolak");
                            foreach ($hasil as $i=>$h) {
                            ?>
                            <option value="<?php echo $i; ?>" <?php if(isset($notisi)){ echo ($notisi->approval_ketua==$i) ? "selected":""; }  ?>><?php echo $h; ?> </option>
                            <?php
                            }
                            ?>
                            </select>
                    </div>

                    <div class="col-12">
                    <label for="inputNanme4" class="form-label">Catatan   </label>
                    <textarea class="form-control" name="f[catatan_ketua]"  rows="3" id="catatan" required><?php  echo ($notisi->catatan_ketua) ? $notisi->catatan_ketua:"";   ?></textarea>
                    </div>
                <?php 
                    }
                    ?>

                <?php 
                    if($_SESSION['group_id']=="dalnis"){

                        ?>
                    <div class="col-12">
                    <label for="inputNanme4" class="form-label">Hasil Review  </label>
                        <select class="form-control" name="f[approval_dalnis]"  required>
                            <option value="">- Pilih Hasil -</option>
                            <?php
                        
                            $hasil = array("1"=>"Setujui","2"=>"Tolak");
                            foreach ($hasil as $i=>$h) {
                            ?>
                            <option value="<?php echo $i; ?>" <?php if(isset($notisi)){ echo ($notisi->approval_dalnis==$i) ? "selected":""; }  ?>><?php echo $h; ?> </option>
                            <?php
                            }
                            ?>
                            </select>
                    </div>

                    <div class="col-12">
                    <label for="inputNanme4" class="form-label">Catatan   </label>
                    <textarea class="form-control" name="f[catatan_dalnis]"  id="catatan" rows="3" required><?php  echo ($notisi->catatan_dalnis) ? $notisi->catatan_dalnis:"";   ?></textarea>
                    </div>
                <?php 
                    }
                    ?>
                  
                    <br>

               
                <div class="text-center">
                  <button type="submit" class="btn btn-success"><i class="bi bi-check-circle"></i> Simpan Hasil Review</button>
                
                  
                </div>
              </form>


              <script type="text/javascript">
																
																
                                var catatan = CKEDITOR.replace( 'catatan', {
   
                                     
                                     
                                       width:"100%",
                                       height:100,
                                       removeButtons: 'NewPage,Save,ExportPdf,Preview,Print,Smiley,About,ShowBlocks,BGColor,Anchor,Language,SelectAll,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Find,Styles,Maximize,Format,Font,Iframe,PageBreak,SpecialChar',
                                        allowedContent :true
                               } );
                               
                               
                                                             
                                                     
                               
                                </script>




              