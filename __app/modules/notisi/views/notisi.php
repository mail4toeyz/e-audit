
<script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/tinymce/tinymce.min.js"></script>
<style>
  .form-label{
    font-weight:bold;
  }
  .tox-promotion-link{
    display:none;
  }
</style>
             <div class="alert alert-info alert-dismissible fade show" role="alert">
                <i class="bi bi-info-circle me-1"></i>
                <?php echo $kka->nama; ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              <br>
            <form class="row g-3" method="post" url="<?php echo site_url('notisi/saveNotisi'); ?>" id="simpannorespon">
                <div class="col-12">
                    <input type="hidden" name="satker_id" value="<?php echo $satker_id; ?>">
                    <input type="hidden" name="kka_id" value="<?php echo $kka->id; ?>">

                  <label for="inputNanme4" class="form-label">Kode Temuan </label>
                     <select class="form-control temuanSelect" name="f[kode_temuan]" id="select2Temuan" required >
                        <option value="">- Pilih Temuan -</option>
                        <?php
                       
                        $unit_kerja = $this->db->query("SELECT * from tm_temuan where kel='1' AND jenis IS NULL")->result();
                        foreach ($unit_kerja as $unit) {
                        ?>

                            <optgroup label="<?php echo $unit->sub_kel; ?> - <?php echo $unit->deskripsi; ?>">
                            <?php
                            $satkerData = $this->db->query("SELECT * from tm_temuan where sub_kel='{$unit->sub_kel}' AND jenis IS NOT NULL AND  kel='1'")->result();
                            foreach ($satkerData as $row) {
                            ?>
                                <option value="<?php echo $row->id; ?>" <?php if(isset($notisi)){ echo ($notisi->kode_temuan==$row->id) ? "selected":""; }  ?>><?php echo $row->jenis; ?> - <?php echo $row->deskripsi; ?></option>
                            <?php
                            }
                            ?>
                            </optgroup>

                        <?php
                        }
                        ?>
                        </select>
                </div>
                <div class="col-12">
                  <label for="inputPassword4" class="form-label">Nominal Temuan(Rp)  (Bila Ada)  </label>
                  <input type="text" class="form-control nominal_temuan" name="nominal_temuan" value="<?php  echo ($notisi->nominal_temuan) ? $this->Reff->formatuang2($notisi->nominal_temuan) :""; ?>" onkeypress="return FormatCurrency(this)">
                  
                </div>

                <div class="col-12">
                  <label for="inputNanme4" class="form-label">Sebab  </label>
                     <select class="form-control temuanSelect" name="f[kode_sebab]"  required>
                        <option value="">- Pilih Sebab -</option>
                        <?php
                       
                        $unit_kerja = $this->db->query("SELECT * from tm_temuan where kel='2' AND jenis IS NULL")->result();
                        foreach ($unit_kerja as $unit) {
                        ?>

                            <optgroup label="<?php echo $unit->sub_kel; ?> - <?php echo $unit->deskripsi; ?>">
                            <?php
                            $satkerData = $this->db->query("SELECT * from tm_temuan where sub_kel='{$unit->sub_kel}' AND jenis IS NOT NULL AND  kel='2'")->result();
                            foreach ($satkerData as $row) {
                            ?>
                                <option value="<?php echo $row->id; ?>" <?php if(isset($notisi)){ echo ($notisi->kode_sebab==$row->id) ? "selected":""; }  ?>><?php echo $row->jenis; ?> - <?php echo $row->deskripsi; ?></option>
                            <?php
                            }
                            ?>
                            </optgroup>

                        <?php
                        }
                        ?>
                        </select>
                </div>

                <div class="col-12">
                  <label for="inputNanme4" class="form-label">Rekomendasi   </label>
                     <select class="form-control temuanSelect" name="f[kode_rekomendasi]"  required>
                        <option value="">- Pilih Rekomendasi -</option>
                        <?php
                       
                        $unit_kerja = $this->db->query("SELECT * from tm_temuan where kel='3' AND jenis IS NULL")->result();
                        foreach ($unit_kerja as $unit) {
                        ?>

                            <optgroup label="<?php echo $unit->sub_kel; ?> - <?php echo $unit->deskripsi; ?>">
                            <?php
                            $satkerData = $this->db->query("SELECT * from tm_temuan where sub_kel='{$unit->sub_kel}' AND jenis IS NOT NULL AND  kel='3'")->result();
                            foreach ($satkerData as $row) {
                            ?>
                                <option value="<?php echo $row->id; ?>" <?php if(isset($notisi)){ echo ($notisi->kode_rekomendasi==$row->id) ? "selected":""; }  ?>><?php echo $row->jenis; ?> - <?php echo $row->deskripsi; ?></option>
                            <?php
                            }
                            ?>
                            </optgroup>

                        <?php
                        }
                        ?>
                        </select>
                </div>

                <div class="col-12">
                  <label for="inputPassword4" class="form-label">Penyelesaian ditempat (Rp) (Bila Ada) </label>
                   
                    <div class="row">
                      <div class="col-6">
                      <input type="text" class="form-control nominal_rekomendasi" name="nominal_rekomendasi" value="<?php  echo ($notisi->nominal_rekomendasi) ? $this->Reff->formatuang2($notisi->nominal_rekomendasi) :""; ?>" onkeypress="return FormatCurrency(this)">
                      </div>
                      <div class="col-6">
                        Saldo : <b id="saldoSisa"></b>
                      <input type="hidden" class="form-control saldo" name="saldo"  value="<?php  echo ($notisi->saldo) ? $this->Reff->formatuang2($notisi->saldo) :""; ?>" onkeypress="return FormatCurrency(this)">
                      </div>
                    </div>
                </div>


                <div class="col-12">
                  <label for="inputPassword4" class="form-label">Kondisi</label>
                  <textarea class="form-control tinymce-editor" name="kondisi" id="kondisi" rows="3" > <?php  echo ($notisi->kondisi) ? $notisi->kondisi:"";   ?></textarea>
                  
                </div>
                <div class="col-12">
                  <label for="inputPassword4" class="form-label">Kriteria</label>
                  <textarea class="form-control tinymce-editor" name="kriteria" id="kriteria" rows="3" ><?php  echo ($notisi->kriteria) ? $notisi->kriteria:"";   ?></textarea>
                  
                </div>
                
                <div class="col-12">
                  <label for="inputPassword4" class="form-label">Akibat</label>
                  <textarea class="form-control tinymce-editor" name="akibat" id="akibat" rows="3" ><?php  echo ($notisi->akibat) ? $notisi->akibat:"";   ?></textarea>
                  

                </div>
                <div class="col-12">
                  <label for="inputPassword4" class="form-label">Sebab</label>
                  <textarea class="form-control tinymce-editor" name="sebab" id="sebab" rows="3" ><?php  echo ($notisi->sebab) ? $notisi->sebab:"";   ?></textarea>
                  
                </div>
                <div class="col-12">
                  <label for="inputPassword4" class="form-label">Rekomendasi </label>
                  <textarea class="form-control tinymce-editor" name="rekomendasi" id="rekomendasi" rows="3" ><?php  echo ($notisi->rekomendasi) ? $notisi->rekomendasi:"";   ?></textarea>
                 
                </div>
               
               
                <div class="text-center">
                  <button type="submit" class="btn btn-primary">Simpan Notisi</button>
                
                  <button type="reset" class="btn btn-secondary">Reset</button>
                </div>
              </form>


 <script type="text/javascript">
            //   $(document).ready(function() {
            //   $('.js-example-basic-single').select2({ width: '100%',  dropdownParent: $("#largeModalNotisi");  });
           
            //  });

  $(document).off("input",".nominal_rekomendasi").on("input",".nominal_rekomendasi",function(){
	  
      var nominal_temuan = HapusTitik($(".nominal_temuan").val());
      var nominal_rekomendasi = HapusTitik($(".nominal_rekomendasi").val());
      var saldo = nominal_temuan - nominal_rekomendasi;

      $(".saldo").val(saldo);
      $("#saldoSisa").html(TambahTitikManual(saldo));
      
    
  })	


	$(".temuanSelect").select2({
				dropdownParent: $("#largeModalNotisi"),
                width: '100%'
	});

  const useDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;
  const isSmallScreen = window.matchMedia('(max-width: 1023.5px)').matches;

  tinymce.init({
    selector: 'textarea.tinymce-editor',
    plugins: 'preview importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap pagebreak nonbreaking anchor insertdatetime advlist lists wordcount help charmap quickbars emoticons',
    editimage_cors_hosts: ['picsum.photos'],
    menubar: 'file edit view insert format tools table help',
    toolbar: 'undo redo | bold italic underline strikethrough | fontfamily fontsize blocks | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
    toolbar_sticky: true,
    branding: false,
    promotion: false,
    toolbar_sticky_offset: isSmallScreen ? 102 : 108,
    autosave_ask_before_unload: true,
    autosave_interval: '30s',
    autosave_prefix: '{path}{query}-{id}-',
    autosave_restore_when_empty: false,
    autosave_retention: '2m',
    image_advtab: true,
    link_list: [{
        title: 'My page 1',
        value: 'https://www.tiny.cloud'
      },
      {
        title: 'My page 2',
        value: 'http://www.moxiecode.com'
      }
    ],
    image_list: [{
        title: 'My page 1',
        value: 'https://www.tiny.cloud'
      },
      {
        title: 'My page 2',
        value: 'http://www.moxiecode.com'
      }
    ],
    image_class_list: [{
        title: 'None',
        value: ''
      },
      {
        title: 'Some class',
        value: 'class-name'
      }
    ],
    importcss_append: true,
    file_picker_callback: (callback, value, meta) => {
      /* Provide file and text for the link dialog */
      if (meta.filetype === 'file') {
        callback('https://www.google.com/logos/google.jpg', {
          text: 'My text'
        });
      }

      /* Provide image and alt text for the image dialog */
      if (meta.filetype === 'image') {
        callback('https://www.google.com/logos/google.jpg', {
          alt: 'My alt text'
        });
      }

      /* Provide alternative source and posted for the media dialog */
      if (meta.filetype === 'media') {
        callback('movie.mp4', {
          source2: 'alt.ogg',
          poster: 'https://www.google.com/logos/google.jpg'
        });
      }
    },
    templates: [{
        title: 'New Table',
        description: 'creates a new table',
        content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>'
      },
      {
        title: 'Starting my story',
        description: 'A cure for writers block',
        content: 'Once upon a time...'
      },
      {
        title: 'New list with dates',
        description: 'New List with dates',
        content: '<div class="mceTmpl"><span class="cdate">cdate</span><br><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>'
      }
    ],
    template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
    template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
    height: 250,
    image_caption: true,
    quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
    noneditable_class: 'mceNonEditable',
    toolbar_mode: 'sliding',
    contextmenu: 'link image table',
    skin: useDarkMode ? 'oxide-dark' : 'oxide',
    content_css: useDarkMode ? 'dark' : 'default',
    content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:16px }'
  });
    </script>


              