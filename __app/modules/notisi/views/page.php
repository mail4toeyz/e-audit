<?php 
     if($_SESSION['group_id'] =="ketua"){

        ?>
<style>
 
        
    select,textarea,input{
    pointer-events: none;
	background-color: transparent;
   
        }
        .required{

          color:red;
        }
       
		
 </style>

 <?php 
	 }
	 $statusMadrasah = substr($satker->kode,3,1);
	 $satker_id   = $satker->id;
	 $kegiatan_id = $_SESSION['kegiatan_id'];
	 $penyusun    = $_SESSION['idAuditor'];
?>

<link href="<?php echo base_url(); ?>__statics/js/upload/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
 <script src="<?php echo base_url(); ?>__statics/js/upload/js/fileinput.js" type="text/javascript"></script>
 <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>     
<div id="showform"></div>

<main id="main" class="main">

<div class="pagetitle">
  <h1><?php echo $title; ?></h1>
  <nav>
	<ol class="breadcrumb">
	  <li class="breadcrumb-item"><a href="#">e-Audit</a></li>
	
	  <li class="breadcrumb-item active"><?php echo $title; ?></li>
	</ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
	<div class="col-lg-12">

	  <div class="card">
		<div class="card-body">
		<form action="javascript:void(0)" method="post" id="simpanpka" name="simpanpka" url="<?php echo site_url("kka/save"); ?>">
		<input type="hidden" name="satker_id" value="<?php echo $satker->id; ?>">
		  <h5 class="card-title"> 
			<div style="float:right">
		   
			</div>
			<br>
			<br>
			<hr>
			 <table class="table  table-bordered table-striped">
				 <tr>
					 <td> Nama Auditi </td>
					 <td>   
					 <button type="button" class="btn btn-outline-primary detailLembaga" nsm="<?php echo $satker->kode; ?>" data-bs-toggle="modal" data-bs-target="#fullscreenModal">
					 <?php echo $satker->nama; ?> 
             		 </button></td>
				 </tr>
				 <tr>
					 <td> Tahun Anggaran </td>
					 <td> <?php echo $kegiatan->anggaran; ?> </td>
				 </tr>

			</table>
		  </h5>

		  <ul class="nav nav-tabs d-flex" id="myTabjustified" role="tablist">
			  <?php 
			  $kategoriPKA = $this->db->get_where("tm_kategori_pka",array("subjenis"=>$kegiatan->jenis))->result();
			     foreach($kategoriPKA as $rkat){
					$active = ($rkat->id==1) ? "active" :"";
					$jmlUraian 		  = $this->db->query("SELECT count(id) as jml from tm_kka where kategori_id='{$rkat->id}' AND jenis !=0 AND status_lembaga='{$statusMadrasah}' AND id IN(SELECT uraian_id FROM tr_kka WHERE   kategori_pka='{$rkat->id}'  AND satker_id='{$satker_id}' AND kegiatan_id='{$kegiatan_id}' AND nilai < 1 ) ")->row();
			  ?>
                <li class="nav-item flex-fill" role="presentation">
                  <button class="nav-link w-100 <?php echo $active; ?>" id="home-tab" data-bs-toggle="tab" data-bs-target="#home-justified<?php echo $rkat->id; ?>" type="button" role="tab" aria-controls="home" aria-selected="true"><?php echo $rkat->slug; ?> <span class="badge bg-danger"><?php echo $jmlUraian->jml; ?></span></button>
                </li>
			 <?php 
				 }
				?>
               
              </ul>
              <div class="tab-content pt-2" id="myTabjustifiedContent">
			  <?php 
			    $kategoriPKA = $this->db->get_where("tm_kategori_pka",array("subjenis"=>$kegiatan->jenis))->result();
			     foreach($kategoriPKA as $rkat){
					$active = ($rkat->id==1) ? "active" :"";
					
					
					$this->db->where("kategori_pka",$rkat->id);
					$this->db->where("satker_id",$satker_id);
					$this->db->where("kegiatan_id",$kegiatan_id);
					$this->db->where("penyusun",$penyusun);
					
					$datakategoriPKA  = $this->db->get("tr_kka")->row();
					$dataInduk 		  = $this->db->query("SELECT SUM(nilai) as nilai, SUM(bobot) as bobot, sum(capaian) as capaian from tr_kka where uraian_id IN(SELECT id from tm_kka where kategori_id='".$rkat->id."' and jenis !=0 AND status_lembaga='{$statusMadrasah}') AND satker_id='{$satker_id}' AND kegiatan_id='{$kegiatan_id}'")->row();
					$dataIndex 		  = $this->db->query("SELECT SUM(bobot) as bobot FROM tm_kka where kategori_id='".$rkat->id."' and jenis !=0 AND status_lembaga='{$statusMadrasah}'")->row();



					
					
			  ?>
                <div class="tab-pane fade show <?php echo $active; ?>" id="home-justified<?php echo $rkat->id; ?>" role="tabpanel" aria-labelledby="home-tab">
					<center><h5 style="font-weight:bold">Notisi Audit <br> Capaian Kinerja <?php echo $rkat->slug; ?></h5></center>
					
						

						<div class="col-md-4">
						<table class="table  table-bordered table-striped" width="100%">
							<thead>
								<tr>
									<th colspan="2"> <center> Kondisi</center> </th>
									<th rowspan="2"> Nilai Capaian </th>
				 				</tr>
								 <tr>
									<th> Realisasi Nilai </th>
									<th> Bobot(%)  </th>
				 				</tr>

				 			</thead>
							 <tbody>

							 <tr>
									<td> <?php echo number_format($dataInduk->nilai,3,",","."); ?> </td>
									<td> <?php echo number_format($dataIndex->bobot,2,",","."); ?> </td>
									<td> <?php echo number_format($dataInduk->capaian,3,",","."); ?> </td>
									
				 				</tr>
								

				 			</tbody>
				 		</table>
				 		</div>
						<div class="table-responsive">
						

					  	 <table class="table  table-bordered ">
							<thead>
								<tr>
									<th>No</th>
									<th>Key Performance Indicator (KPI) </th>
									
									<th>Capaian</th>
									<th>Dokumen</th>
									<th width="30%">Status</th>
									
									<th>Aksi </th>
									
									
									<!-- <th rowspan="2">Aksi</th> -->
				 				</tr>
								
				 			</thead>
							<tbody>
								 <?php 
								 $uraian 		  = $this->db->query("SELECT * from tm_kka where kategori_id='{$rkat->id}' AND jenis !=0 AND status_lembaga='{$statusMadrasah}' AND id IN(SELECT uraian_id FROM tr_kka WHERE   kategori_pka='{$rkat->id}'  AND satker_id='{$satker_id}' AND kegiatan_id='{$kegiatan_id}' AND nilai < 1 ) ")->result();
								  $no=1;
								  if(count($uraian) > 0){
								    foreach($uraian as $urow){

										$this->db->where("uraian_id",$urow->id);
										$this->db->where("kategori_pka",$rkat->id);
										$this->db->where("satker_id",$satker_id);
										$this->db->where("kegiatan_id",$kegiatan_id);
										
										$dataUraianPKA  = $this->db->get("tr_kka")->row();

										$style="";
										$readonly="";
										

										$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
										$this->db->where("satker_id",$satker_id);
										$this->db->where("kka_id",$urow->id);
									    $kegiatanPersyaratan = $this->db->get("tr_kegiatan_persyaratan")->row();
										if(!is_null($kegiatanPersyaratan)){
											$status ='<button class="btn btn-primary uploadDokumen" data-id="'.$urow->id.'" satker_id="'.$satker_id.'" type="button" data-bs-toggle="modal" data-bs-target="#largeModal"><i class="fa fa-file"></i> Buka  ('.count($kegiatanPersyaratan).')</button>';
										}else{
											$status ='<span class="badge bg-danger"><i class="bi bi-exclamation-octagon me-1"></i></span>';
										}
										$this->db->where("satker_id",$satker_id);
										$this->db->where("kka_id",$urow->id);
										$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
										$this->db->where("kode_temuan !=0");
										$notisi = $this->db->get("tr_notisi")->row();
										$dataNotisi = "";
										$statusNotisi = '<span class="badge border-success border-1 text-danger"> Belum</span>';
										$statusKetua  = '<span class="badge border-success border-1 text-danger"> Belum</span>';
										$statusDalnis  	  = '<span class="badge border-success border-1 text-danger"> Belum</span>';
										$datatanggapan  = '<span class="badge border-success border-1 text-danger"> - </span>';
											if(!is_null($notisi)){
												
												$dataNotisi = '<span class="badge border-danger border-1 text-danger">Kode Temuan : '.$notisi->kode_temuan.' </span>';
												$dataNotisi .= '<br><span class="badge border-primary border-1 text-danger">Kode Rekomendasi : '.$notisi->kode_rekomendasi.' </span> ';
											
												$dataNotisi .= '<br><span class="badge border-secondary border-1 text-secondary">'. $notisi->keterangan."</span>";
												$statusNotisi  = '<span class="badge border-success border-1 text-success"> Lengkap</span>';

												  if($notisi->approval_ketua==1){
													$statusKetua  = '<span class="badge border-success border-1 text-success"> Disetujui</span>';
												  }else if($notisi->approval_ketua==2){
													$statusKetua  = '<span class="badge border-warning border-1 text-warning"> Ditolak</span>';
												  }

												  if($notisi->approval_dalnis==1){
													$statusDalnis  = '<span class="badge border-success border-1 text-success"> Disetujui</span>';
												  }else if($notisi->approval_dalnis==2){
													$statusDalnis  = '<span class="badge border-warning border-1 text-warning"> Ditolak</span>';
												  }

												 	$this->db->where("satker_id",$satker_id);
													$this->db->where("notisi_id",$notisi->id);
													$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
													
													$tanggapan = $this->db->get("tr_tanggapan")->row();
												
															if(!is_null($tanggapan)){

																if($tanggapan->hasil==1){
																	$datatanggapan = ' <span class="badge bg-success"><i class="bi bi-check-circle me-1"></i> Menerima & Komitmen </span>';
																}else if($tanggapan->hasil==2){
																	$datatanggapan = '  <span class="badge bg-danger"><i class="bi bi-exclamation-octagon me-1"></i> Menyanggah </span>';

																}



															}
											}

													

											


											?>
											<tr>
											   <td><?php echo $no++; ?></td>
											   <td><input type="hidden" name="uraian<?php echo $rkat->id; ?><?php echo $urow->id; ?>" class="form-control" value="<?php echo  isset($dataUraianPKA->uraian) ? $dataUraianPKA->uraian : $urow->nama; ?>"><?php echo  isset($dataUraianPKA->uraian) ? $dataUraianPKA->uraian : $urow->nama; ?></td>
												<td><?php echo  isset($dataUraianPKA->capaian) ? number_format($dataUraianPKA->capaian,3,",",".") : ""; ?></td>
											   <td><?php echo $status; ?></td>
											  
											   <td>
												<table class="table table-bordered">
													<tr>
														<td> Pengisian Notisi </td>
														<td> <a href="#" class="notisiDetail" data-bs-toggle="modal" data-bs-target="#largeModalNotisi" data-id="<?php echo $urow->id; ?>"> <?php echo $statusNotisi; ?>  </a></td>
													</tr>
													<tr>
														<td> Review Ketua Tim </td>
														<td> <a href="#" class="notisiDetail" data-bs-toggle="modal" data-bs-target="#largeModalNotisi" data-id="<?php echo $urow->id; ?>"><?php echo $statusKetua; ?> </a></td>
													</tr>

													<tr>
														<td> Review Dalnis </td>
														<td><a href="#" class="notisiDetail" data-bs-toggle="modal" data-bs-target="#largeModalNotisi" data-id="<?php echo $urow->id; ?>"> <?php echo $statusDalnis; ?> </a></td>
													</tr>
													<?php 
													 if(!is_null($tanggapan)){
													?>
													<tr>
														<td> Tanggapan Auditi </td>
														<td><a href="#" class="tanggapanAuditi" data-bs-toggle="modal" data-bs-target="#largeModalNotisi" data-id="<?php echo $tanggapan->id; ?>"> <?php echo $datatanggapan; ?> </a></td>
													</tr>
															<?php 
															if($tanggapan->ap_anggota_hasil !=0){
																$hasil = array("1"=>"Diterima","2"=>"Ditolak");
																?>
																<tr>
																<td> Tanggapan (Review Anggota) </td>
																<td><a href="#" class="tanggapanAuditi" data-bs-toggle="modal" data-bs-target="#largeModalNotisi" data-id="<?php echo $tanggapan->id; ?>"> <?php echo '<span class="badge bg-info"> '.$hasil[$tanggapan->ap_anggota_hasil].'</span>'; ?> </a></td>
															</tr>
																
																<?php
															}
															?>

														  <?php 
															if($tanggapan->ap_ketua_hasil !=0){
																$hasil = array("1"=>"Diterima","2"=>"Ditolak");
																?>
																<tr>
																<td> Tanggapan (Review Ketua) </td>
																<td><a href="#" class="tanggapanAuditi" data-bs-toggle="modal" data-bs-target="#largeModalNotisi" data-id="<?php echo $tanggapan->id; ?>"> <?php echo '<span class="badge bg-info"> '.$hasil[$tanggapan->ap_ketua_hasil].'</span>'; ?> </a></td>
															</tr>
																
																<?php
															}
															?>

															<?php 
															if($tanggapan->ap_dalnis_hasil !=0){
																$hasil = array("1"=>"Diterima","2"=>"Ditolak");
																?>
																<tr>
																<td> Tanggapan (Review Dalnis) </td>
																<td><a href="#" class="tanggapanAuditi" data-bs-toggle="modal" data-bs-target="#largeModalNotisi" data-id="<?php echo $tanggapan->id; ?>"> <?php echo '<span class="badge bg-info"> '.$hasil[$tanggapan->ap_dalnis_hasil].'</span>'; ?> </a></td>
															</tr>
																
																<?php
															}
															?>

													<?php 
													 }
													 ?>
													
												</table>


											  </td>
										
											   <td> 
												<?php 
												if($_SESSION['group_id'] !="ketua"){
												?>	
											   <button type="button" class="btn btn-danger rounded-pill notisi" type="button" data-bs-toggle="modal" data-bs-target="#largeModalNotisi" data-id="<?php echo $urow->id; ?>" > Notisi </button>
											   <?php 
												}
												?>
											   </td>
   
											   
										   </tr>
   
   
										   <?php 
											


										}

									}else{

										?>
										<tr>
											 <td colspan="6"><center> Kinerja Terpenuhi atau belum ada notisi pada aspek  <u><?php echo $rkat->slug; ?></u></center></td>
										</tr>

										<?php 
									}

										


									
								 ?>

				 			</tbody>
				   		 </table>

				 		</div>
						
					
                 
                </div>
				<?php 
				 }
				
				 ?>
               
              </div>
		 

		  <!-- Default Table -->
		</form>
		 
		</div>
	  </div>


	</div>

  </div>
</section>

</main>

<div class="modal fade" id="largeModal" tabindex="-1">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Detail Dokumen</h5>
                      
                    </div>
                    <div class="modal-body" id="load-body">
				 		Mohon Tunggu ..
                     
                    </div>
					<div class="modal-footer">
						<button type="button"  class="btn btn-default" data-bs-dismiss="modal">Tutup</button>
					</div>
                   
                  </div>
                </div>
 </div>

 <div class="modal fade" id="largeModalNotisi">
                <div class="modal-dialog modal-xl">
                  <div class="modal-content">
                  
                    <div class="modal-body" id="load-bodyNotisi">
					Mohon Tunggu ..
                    </div>
					<div class="modal-footer">
						<button type="button"  class="btn btn-default" data-bs-dismiss="modal">Tutup</button>
					</div>
                   
                  </div>
                </div>
 </div>
	


<script type="text/javascript">

$(document).on("click",".tanggapanAuditi",function(){
				var id 		  = $(this).data("id");
				var satker_id = "<?php echo $satker->id; ?>";

				$.post("<?php echo site_url('notisi/tanggapan_form'); ?>",{id:id,satker_id:satker_id},function(data){

					$("#load-bodyNotisi").html(data);

				})


});


	 $(document).on("click",".uploadDokumen",function(){
				var id 		  = $(this).data("id");
				var satker_id = "<?php echo $satker->id; ?>";

				$.post("<?php echo site_url('kka/berkas'); ?>",{id:id,satker_id:satker_id},function(data){

					$("#load-body").html(data);

				})


			  });

			  $(document).on("click",".notisi",function(){
				var id 		  = $(this).data("id");
				var satker_id = "<?php echo $satker->id; ?>";

				$.post("<?php echo site_url('notisi/notisi_form'); ?>",{id:id,satker_id:satker_id},function(data){

					$("#load-bodyNotisi").html(data);

				})


			  })

			  $(document).on("click",".notisiDetail",function(){
				var id 		  = $(this).data("id");
				var satker_id = "<?php echo $satker->id; ?>";

				$.post("<?php echo site_url('notisi/notisi_detail'); ?>",{id:id,satker_id:satker_id},function(data){

					$("#load-bodyNotisi").html(data);

				})


			  })


  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					
					
					"ajax":{
						url :"<?php echo site_url("dokumen/grid"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						data.trkelas_id = $("#trkelas_id").val();
						data.keyword = $("#keyword").val();
						data.ajaran = $("#ajaran").val();
						
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
				
				$(document).on("input","#keyword",function(){
	  
				  dataTable.ajax.reload(null,false);	
				  
			  });
			  $(document).on("change","#trkelas_id,#ajaran",function(){
	  
				  dataTable.ajax.reload(null,false);	
				  
			  });
	
			  $(document).on("change","#dikerjakanolehRencana",function(){
				var nilai = $(this).val();
			 	 $(".dikerjakanolehRencana").val(nilai);

			  });

			  $(document).on("change","#dikerjakanolehRealisasi",function(){
				var nilai = $(this).val();
			 	 $(".dikerjakanolehRealisasi").val(nilai);

			  });
			  $(document).on("change","#waktuRencana",function(){
				var nilai = $(this).val();
			 	 $(".waktuRencana").val(nilai);

			  });
			  $(document).on("change","#waktuRealisasi",function(){
				var nilai = $(this).val();
			 	 $(".waktuRealisasi").val(nilai);

			  });

			  $(document).on("input",".skor",function(){
				var nilai 	 = $(this).val();
				var id   	 = $(this).data("id");
				var bobot    = $("#bobot"+id).val();

				if(!isNaN(nilai)){

					if(nilai <= 1){
						var capaian = nilai * bobot;
						
				  		$("#capaian"+id).val(capaian);

					}else{
						alertify.warning("Tidak melebihi angka 1");	
					}
				   

				}else{
					alertify.warning("Masukkan berupa angka, jika ada komma, masukkan dengan tanda titik. Contoh : 0.750");

				}
			 	 

			  });

			  $(document).on('submit', 'form#simpanpka', function (event, messages) {
				event.preventDefault()
				var form   = $(this);
				var urlnya = $(this).attr("url");
				loading();
					$.ajax({
						type: "POST",
						url: urlnya,
						data: form.serialize(),
						success: function (response, status, xhr) {
							var ct = xhr.getResponseHeader("content-type") || "";
							if (ct == "application/json") {
						
							
							
								toastr.error(response.message, "Gagal  , perhatikan !  ", {
										"timeOut": "0",
										"extendedTImeout": "0",
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-top-right",
										"onclick": null,
										"showDuration": "10000",
										"hideDuration": "10000",
										"timeOut": "50000",
										"extendedTimeOut": "10000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut"
									});
								
							} else {
								
								toastr.success("Data Berhasil disimpan", "Sukses !", {
										"timeOut": "0",
										"extendedTImeout": "0",
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-top-right",
										"onclick": null,
										"showDuration": "10000",
										"hideDuration": "10000",
										"timeOut": "50000",
										"extendedTimeOut": "10000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut"
									});
									location.reload();
								
							
							
							
							}
							
							jQuery.unblockUI({ });
						}
					});

					return false;
				});

				


</script>

<div class="modal fade" id="fullscreenModal" tabindex="-1">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Detail Profile Auditi </h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body" id="loadBodyAuditi">
					 Mohon Tunggu ...
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                      
                    </div>
                  </div>
                </div>
</div>

<script>

	$(document).off("click",".detailLembaga").on("click",".detailLembaga",function(){

		var nsm = $(this).attr("nsm");
		 $.post("<?php echo site_url('web/detail_auditi'); ?>",{nsm:nsm},function(data){

			$("#loadBodyAuditi").html(data);

		 })

	});
  
</script>
				
				