
<script type="text/javascript" src="<?php echo base_url(); ?>__statics/ckeditor/ckeditor.js" ></script> 
            
            <form class="row g-3" method="post" action="<?php echo site_url('notisi/saveNotisiDataTanggapan'); ?>">
                <div class="col-12">
                    
                    <input type="hidden" name="id" value="<?php echo $tanggapan->id; ?>">
                    <input type="hidden" name="satker_id" value="<?php echo $satker_id; ?>">
                    <table class="table table-bordered">
                       
                         <tr>
                            <td width="20%"> Tanggapan Auditi  </td>
                            
                            <td> 
                                <?php 
                                if($tanggapan->hasil==1){
                                   echo  $datatanggapan = ' <span class="badge bg-success"><i class="bi bi-check-circle me-1"></i> Menerima dan Komitmen</span>';
                                 }else if($tanggapan->hasil==2){
                                    echo $datatanggapan = '  <span class="badge bg-danger"><i class="bi bi-exclamation-octagon me-1"></i> Menyanggah </span>';
                                 }
                                ?>
                                
                            </td>
                        </tr>


                        <tr>
                            <td width="20%"> Keterangan  </td>
                            
                            <td> 
                                <?php 
                                echo $tanggapan->keterangan;
                                ?>
                                
                            </td>
                        </tr>
                        <?php 
                        if($tanggapan->file !=""){
                            ?>
                            <tr>
                            <td width="20%"> Keterangan  </td>
                            
                            <td> 
                                <a href="https://drive.google.com/file/d/<?php echo $tanggapan->file; ?>/preview" target="_blank" class="btn btn-outline-danger" ><i class="bi bi-folder"></i>  Lihat Dokumen </a>
                                
                            </td>
                        </tr>
                        <?php 
                           
                           }
                        ?>
                       
                        
                        <?php
                         if($tanggapan->ap_anggota_hasil !=0){
                        ?>
                        <tr>
                            <td width="20%"> Review Anggota   </td>
                            
                            <td> 
                                <?php 
                                $hasil = array("1"=>"Diterima","2"=>"Ditolak");
                                echo '<span class="badge bg-info"><i class="bi bi-check-circle me-1"></i> '.$hasil[$tanggapan->ap_anggota_hasil].'</span>';
                                echo "<hr>";
                                echo $tanggapan->ap_anggota_ctt;
                                echo "<hr>";
                                echo $this->Reff->formattimestamp($tanggapan->ap_anggota_tgl);
                                ?>
                                
                            </td>
                        </tr>
                        <?php 
                         }
                         ?>
                        <?php
                         if($tanggapan->ap_ketua_hasil !=0){
                        ?>
                        <tr>
                            <td width="20%"> Review Ketua    </td>
                            
                            <td> 
                                <?php 
                                $hasil = array("1"=>"Diterima","2"=>"Ditolak");
                                echo '<span class="badge bg-info"><i class="bi bi-check-circle me-1"></i> '.$hasil[$tanggapan->ap_ketua_hasil].'</span>';
                                echo "<hr>";
                                echo $tanggapan->ap_ketua_ctt;
                                echo "<hr>";
                                echo $this->Reff->formattimestamp($tanggapan->ap_ketua_tgl);
                                ?>
                                
                            </td>
                        </tr>

                        <?php 
                         }
                         ?>
                        <?php
                         if($tanggapan->ap_dalnis_hasil !=0){
                        ?>
                        <tr>
                            <td width="20%"> Review Dalnis   </td>
                            
                            <td> 
                                <?php 
                                $hasil = array("1"=>"Diterima","2"=>"Ditolak");
                                echo '<span class="badge bg-info"><i class="bi bi-check-circle me-1"></i> '.$hasil[$tanggapan->ap_dalnis_hasil].'</span>';
                                echo "<hr>";
                                echo $tanggapan->ap_dalnis_ctt;
                                echo "<hr>";
                                echo $this->Reff->formattimestamp($tanggapan->ap_dalnis_tgl);
                                ?>
                                
                            </td>
                        </tr>
                        <?php 
                         }
                         ?>

                    </table>

                    <?php 
                    if($_SESSION['group_id']=="anggota"){
                      
                        ?>
                    <div class="col-12">
                    <label for="inputNanme4" class="form-label">Hasil Review  </label>
                    <input type="hidden" name="f[ap_anggota_tgl]" value="<?php echo ($tanggapan->ap_anggota_tgl != NULL) ? $tanggapan->ap_anggota_tgl : date("Y-m-d H:i:s") ; ?>">
                        <select class="form-control" name="f[ap_anggota_hasil]"  required>
                            <option value="">- Pilih Hasil -</option>
                            <?php
                        
                            $hasil = array("1"=>"Terima","2"=>"Tolak");
                            foreach ($hasil as $i=>$h) {
                            ?>
                            <option value="<?php echo $i; ?>" <?php if(isset($tanggapan)){ echo ($tanggapan->ap_anggota_hasil==$i) ? "selected":""; }  ?>><?php echo $h; ?> </option>
                            <?php
                            }
                            ?>
                            </select>
                    </div>
                    <br>

                    <div class="col-12">
                    <label for="inputNanme4" class="form-label">Catatan   </label>
                    <textarea class="form-control" name="f[ap_anggota_ctt]"  rows="3" id="catatan" required><?php  echo ($tanggapan->ap_anggota_ctt) ? $tanggapan->ap_anggota_ctt:"";   ?></textarea>
                    </div>
                <?php 
                    }
                    ?>

                <?php 
                    if($_SESSION['group_id']=="ketua"){
                      
                        ?>
                    <div class="col-12">
                    <label for="inputNanme4" class="form-label">Hasil Review  </label>
                    <input type="hidden" name="f[ap_ketua_tgl]" value="<?php echo ($tanggapan->ap_ketua_tgl != NULL) ? $tanggapan->ap_ketua_tgl : date("Y-m-d H:i:s") ; ?>">
                        <select class="form-control" name="f[ap_ketua_hasil]"  required>
                            <option value="">- Pilih Hasil -</option>
                            <?php
                        
                            $hasil = array("1"=>"Terima","2"=>"Tolak");
                            foreach ($hasil as $i=>$h) {
                            ?>
                            <option value="<?php echo $i; ?>" <?php if(isset($tanggapan)){ echo ($tanggapan->ap_ketua_hasil==$i) ? "selected":""; }  ?>><?php echo $h; ?> </option>
                            <?php
                            }
                            ?>
                            </select>
                    </div>
                    <br>

                    <div class="col-12">
                    <label for="inputNanme4" class="form-label">Catatan   </label>
                    <textarea class="form-control" name="f[ap_ketua_ctt]"  rows="3" id="catatan" required><?php  echo ($tanggapan->ap_ketua_ctt) ? $tanggapan->ap_ketua_ctt:"";   ?></textarea>
                    </div>
                <?php 
                    }
                    ?>

                <?php 
                    if($_SESSION['group_id']=="dalnis"){
                      
                        ?>
                    <div class="col-12">
                    <label for="inputNanme4" class="form-label">Hasil Review  </label>
                    <input type="hidden" name="f[ap_dalnis_tgl]" value="<?php echo ($tanggapan->ap_dalnis_tgl != NULL) ? $tanggapan->ap_dalnis_tgl : date("Y-m-d H:i:s") ; ?>">
                        <select class="form-control" name="f[ap_dalnis_hasil]"  required>
                            <option value="">- Pilih Hasil -</option>
                            <?php
                        
                            $hasil = array("1"=>"Terima","2"=>"Tolak");
                            foreach ($hasil as $i=>$h) {
                            ?>
                            <option value="<?php echo $i; ?>" <?php if(isset($tanggapan)){ echo ($tanggapan->ap_dalnis_hasil==$i) ? "selected":""; }  ?>><?php echo $h; ?> </option>
                            <?php
                            }
                            ?>
                            </select>
                    </div>
                    <br>

                    <div class="col-12">
                    <label for="inputNanme4" class="form-label">Catatan   </label>
                    <textarea class="form-control" name="f[ap_dalnis_ctt]"  rows="3" id="catatan" required><?php  echo ($tanggapan->ap_dalnis_ctt) ? $tanggapan->ap_dalnis_ctt:"";   ?></textarea>
                    </div>
                <?php 
                    }
                    ?>

                  
                    <br>

               
                <div class="text-center">
                  <button type="submit" class="btn btn-success"><i class="bi bi-check-circle"></i> Simpan Hasil Review Tanggapan </button>
                
                  
                </div>
              </form>


              <script type="text/javascript">
																
																
                                var catatan = CKEDITOR.replace( 'catatan', {
   
                                     
                                     
                                       width:"100%",
                                       height:100,
                                       removeButtons: 'NewPage,Save,ExportPdf,Preview,Print,Smiley,About,ShowBlocks,BGColor,Anchor,Language,SelectAll,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Find,Styles,Maximize,Format,Font,Iframe,PageBreak,SpecialChar',
                                        allowedContent :true
                               } );
                </script>




              