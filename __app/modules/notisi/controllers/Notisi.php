<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notisi extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		if(!$this->session->userdata("idAuditor")){
			    
			echo $this->Reff->sessionhabis();
			exit();
		
	  }
		  $this->load->model('M_ruang','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('auditor/page_header',$data);	
	}
		
	public function data()
	{  
	   
	    		
         $ajax            = $this->input->get_post("ajax",true);	
         $kode            = $this->input->get_post("kode",true);	
		 $data['satker']  = $this->db->get_where("satker",array("kode"=>$kode))->row();
		 $data['kegiatan']  = $this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
		 $data['title']   = "Notisi Audit  | ".$data['satker']->nama;
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}

	public function dataApproval()
	{  
	   
	    		
         $ajax            = $this->input->get_post("ajax",true);	
         $kode            = $this->input->get_post("kode",true);	
		 $data['satker']  = $this->db->get_where("satker",array("kode"=>$kode))->row();
		 $data['kegiatan']  = $this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
		 $data['title']   = "Notisi Audit  | ".$data['satker']->nama;
	     if(!empty($ajax)){
					    
			 $this->load->view('page_approval',$data);
		
		 }else{
			 
		     $data['konten'] = "page_approval";
			 
			 $this->_template($data);
		 }
	

	}

	public function dataApprovalDalnis(){

		$ajax            = $this->input->get_post("ajax",true);	
		$kode            = $this->input->get_post("kode",true);	
		$data['satker']  = $this->db->get_where("satker",array("kode"=>$kode))->row();
		$data['kegiatan']  = $this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
		$data['title']   = "Notisi Audit  | ".$data['satker']->nama;
		if(!empty($ajax)){
					   
			$this->load->view('page_approval_dalnis',$data);
	   
		}else{
			
			$data['konten'] = "page_approval_dalnis";
			
			$this->_template($data);
		}
	}


	public function save(){

		$satker_id   = $this->input->get_post("satker_id");
		$kegiatan_id = $_SESSION['kegiatan_id'];
		$penyusun    = $_SESSION['idAuditor'];

		$this->db->where("satker_id",$satker_id);
		$this->db->where("kegiatan_id",$kegiatan_id);
		$this->db->where("penyusun",$penyusun);
		
		$this->db->delete("tr_kka");
		
		$kategoriPKA = $this->db->get("tm_kategori_pka")->result();
		foreach($kategoriPKA as $rkat){
			$uraian = $this->db->query("SELECT * from tm_kka where kategori_id='{$rkat->id}' and jenis !=0")->result();
			$no=1;

			$kategori_pka    = $rkat->id;
		
			foreach($uraian as $urow){

				 $uraian_id			    = $urow->id;
				 $uraian 				=$this->input->get_post("uraian".$kategori_pka.$uraian_id);
				 $nilai 			    =$this->input->get_post("nilai".$kategori_pka.$uraian_id);
				 $capaian      	        =$this->input->get_post("capaian".$kategori_pka.$uraian_id);
				


				 $this->db->set("satker_id",$satker_id);
				 $this->db->set("kegiatan_id",$kegiatan_id);
				 $this->db->set("penyusun",$penyusun);
				 $this->db->set("kategori_pka",$kategori_pka);
				 
				 $this->db->set("uraian_id",$uraian_id);
				 $this->db->set("uraian",$uraian);
				 $this->db->set("nilai",$nilai);
				 $this->db->set("bobot",$urow->bobot);
				 $this->db->set("capaian",$capaian);
				 
				 $this->db->insert("tr_kka");

				    

			}


		}

	}

	public function berkas(){

		$data['satker_id'] 	 = $this->input->get_post("satker_id");
		$data['persyaratan'] = $this->db->get_where("tm_kka",array("id"=>$_POST['id']))->row();
		$this->load->view('berkas',$data);
	}

	public function notisi_form(){

		$data['satker_id'] 	 = $this->input->get_post("satker_id");
		$data['kka'] 	     = $this->db->get_where("tm_kka",array("id"=>$_POST['id']))->row();

										$this->db->where("satker_id",$data['satker_id']);
										$this->db->where("kka_id",$_POST['id']);
										$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
										$data['notisi'] = $this->db->get("tr_notisi")->row();
		$this->load->view('notisi',$data);
	}

	public function tanggapan_form(){

		$data['satker_id'] 	 = $this->input->get_post("satker_id");
		

										$this->db->where("satker_id",$data['satker_id']);
										$this->db->where("id",$_POST['id']);
										$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
										$data['tanggapan'] = $this->db->get("tr_tanggapan")->row();

		$this->load->view('tanggapan',$data);


	}

	public function notisi_data(){

		$data['satker_id'] 	 = $this->input->get_post("satker_id");
		$data['kka'] 	     = $this->db->get_where("tm_kka",array("id"=>$_POST['id']))->row();

										$this->db->where("satker_id",$data['satker_id']);
										$this->db->where("kka_id",$_POST['id']);
										$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
										$data['notisi'] = $this->db->get("tr_notisi")->row();
		$this->load->view('notisi_data',$data);
	}

	


	public function notisi_detail(){

		$data['satker_id'] 	 = $this->input->get_post("satker_id");
		$data['kka'] 	     = $this->db->get_where("tm_kka",array("id"=>$_POST['id']))->row();

										$this->db->where("satker_id",$data['satker_id']);
										$this->db->where("kka_id",$_POST['id']);
										$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
										$data['notisi'] = $this->db->get("tr_notisi")->row();
		$this->load->view('notisi_detail',$data);
	}

	public function getRekomendasi(){

		$id = $this->input->get_post("id",TRUE);
		$rekomendasi = $this->Reff->get_kondisi(array("jenis" => $id), "tm_temuan", "rekomendasi");
		$kota = $this->db->query("select id,jenis,nama from tm_rekomendasi where id IN(".$rekomendasi.") ")->result();
		                                                     ?><option value="">- Pilih Rekomendasi-</option><?php
															    foreach($kota as $row){
																?><option value="<?php echo $row->id; ?>"><?php echo ($row->jenis); ?> - <?php echo ($row->nama); ?></option><?php 
															}
	}
	
	public function saveNotisi(){

		$f = $this->input->get_post("f");
		$satker_id = $this->input->get_post("satker_id");
		$kka_id    = $this->input->get_post("kka_id");
		$nominal_temuan    = $this->input->get_post("nominal_temuan");
		$nominal_rekomendasi    = $this->input->get_post("nominal_rekomendasi");
		$saldo    = $this->input->get_post("saldo");
		$kondisi   = $_POST['kondisi'];
		$kriteria   = $_POST['kriteria'];
		$akibat   = $_POST['akibat'];
		$sebab   = $_POST['sebab'];
		$rekomendasi   = $_POST['rekomendasi'];

		$this->db->where("satker_id",$satker_id);
		$this->db->where("kka_id",$kka_id);
		$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
		$cek = $this->db->get("tr_notisi")->row();


		
		if(is_null($cek)){
			$this->db->set("nominal_temuan",$nominal_temuan);
			$this->db->set("nominal_rekomendasi",$nominal_rekomendasi);
			$this->db->set("saldo",$saldo);
			$this->db->set("kondisi",$kondisi);
			$this->db->set("kriteria",$kriteria);
			$this->db->set("akibat",$akibat);
			$this->db->set("sebab",$sebab);
			$this->db->set("rekomendasi",$rekomendasi);
			$this->db->set("satker_id",$satker_id);
			$this->db->set("kka_id",$kka_id);
			$this->db->set("kegiatan_id",$_SESSION['kegiatan_id']);
			$this->db->set("approval_ketua",0);
			$this->db->set("approval_dalnis",0);
			$this->db->insert("tr_notisi",$f);
		}else{
			$this->db->set("nominal_temuan",$nominal_temuan);
			$this->db->set("nominal_rekomendasi",$nominal_rekomendasi);
			$this->db->set("saldo",$saldo);
			$this->db->set("kondisi",$kondisi);
			$this->db->set("kriteria",$kriteria);
			$this->db->set("akibat",$akibat);
			$this->db->set("sebab",$sebab);
			$this->db->set("rekomendasi",$rekomendasi);
			$this->db->where("satker_id",$satker_id);
			$this->db->where("kka_id",$kka_id);
			$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
			$this->db->set("approval_ketua",0);
			$this->db->set("approval_dalnis",0);
			
			$this->db->update("tr_notisi",$f);
		}
		
		$nsm = $this->Reff->get_kondisi(array("id" => $satker_id), "satker", "kode");
		redirect(site_url("notisi/data?kode=".$nsm));
	}

	public function saveNotisiData(){

		$f = (($this->input->get_post("f",true)));
		$satker_id = $this->input->get_post("satker_id");
		$kka_id    = $this->input->get_post("kka_id");
		

			$kegiatan		   = $this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
			 if($kegiatan->pengendali_teknis==0){
				
				$this->db->set("approval_dalnis",$f['approval_ketua']);
			 }

			$this->db->where("satker_id",$satker_id);
			$this->db->where("kka_id",$kka_id);
			$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
			
			$this->db->update("tr_notisi",$f);

			
		
		
		$nsm = $this->Reff->get_kondisi(array("id" => $satker_id), "satker", "kode");
		redirect(site_url("notisi/dataApproval?kode=".$nsm));
	}

	public function saveNotisiDataTanggapan(){

			$f = (($this->input->get_post("f",true)));
			
			$id    		  = $this->input->get_post("id");
			$satker_id    = $this->input->get_post("satker_id");
			

				$kegiatan		   = $this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
				if($_SESSION['group_id']=="ketua"){
					if($kegiatan->pengendali_teknis==0){
						
						$this->db->set("ap_dalnis_hasil",$f['ap_ketua_hasil']);
					}
				}

			
				$this->db->where("id",$id);
				$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
				$this->db->update("tr_tanggapan",$f);
			
			$nsm = $this->Reff->get_kondisi(array("id" => $satker_id), "satker", "kode");

			if($_SESSION['group_id']=="anggota"){

				redirect(site_url("notisi/data?kode=".$nsm));
			}else{

				redirect(site_url("notisi/dataApproval?kode=".$nsm));
			}

	}
	
	 
}
