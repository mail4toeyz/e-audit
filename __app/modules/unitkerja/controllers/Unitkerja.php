<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Unitkerja extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		isLogin();
		$this->load->model('M_referensi', 'm');
	}

	function _template($data)
	{
		$this->load->view('admin/page_header', $data);
	}

	public function pegawai($param)
	{
		$ajax            = $this->input->get_post("ajax", true);
		$data['data']	  = $this->db->get_where("unit_kerja", array("kode" => base64_decode($param)))->row();
		$data['title']   = $data['data']->nama;
		if (!empty($ajax)) {

			$this->load->view('page', $data);
		} else {

			$data['konten'] = "page";

			$this->_template($data);
		}
	}

	public function grid()
	{

		$iTotalRecords = $this->m->grid(false)->num_rows();

		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);

		$records = array();
		$records["data"] = array();

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;

		$datagrid = $this->m->grid(true)->result_array();

		$i = ($iDisplayStart + 1);
		foreach ($datagrid as $val) {


			$no = $i++;
			$records["data"][] = array(
				$no,



				$val['nama'],
				$val['nip'],
				$val['golongan'],
				$val['jabatan'],
				$this->Reff->get_kondisi(array("id" => $val['jabatan_id']), "jabatan", "nama"),
				$this->Reff->get_kondisi(array("id" => $val['unitkerja_id']), "unit_kerja", "nama"),
				$val['EMAIL'],
				$val['PASSWORD'],


				' 
					<div class="btn-group" role="group">
					<button type="button" class="btn btn-outline-primary ubahmodal" datanya="' . $val['id'] . '" urlnya="' . site_url("unitkerja/form") . '" data-bs-toggle="modal" data-bs-target="#basicModal" target="#loadform">
                                    <i class="fa fa-pencil"></i>
                      </button>
					  <button type="button"  class="btn btn-outline-danger btn-sm hapus" datanya="' . $val['id'] . '" urlnya="' . site_url("unitkerja/hapus") . '">
                                    <i class="fa fa-trash"></i>
					  </button>
					  </div>
					 
					  '



			);
		}

		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;

		echo json_encode($records);
	}

	public function gridSimpeg()
	{

		$iTotalRecords = $this->m->gridSimpeg(false)->num_rows();

		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);

		$records = array();
		$records["data"] = array();

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;

		$datagrid = $this->m->gridSimpeg(true)->result_array();

		$i = ($iDisplayStart + 1);
		foreach ($datagrid as $val) {


			$no = $i++;
			$records["data"][] = array(
				$no,



				$val['NAMA_LENGKAP'],
				$val['NIP_BARU'],
				$val['GOL_RUANG'],
				$val['TAMPIL_JABATAN'],
				$val['KODE_SATKER_3'],
				$val['SATKER_3'],
				$val['EMAIL'],
				$val['PASSWORD']
			);
		}

		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;

		echo json_encode($records);
	}



	public function form_excel()
	{

		$data = array();
		$this->load->view("form_excel", $data);
	}

	public function import_excel()
	{
		$this->load->library('PHPExcel/IOFactory');



		$sheetup   = 0;
		$rowup     = 2;

		$unitkerja_id = $this->input->get_post("unitkerja_id");

		$fileName = time() . ".xlsx";

		$config['upload_path'] = './__statics/excel/tmp/'; //buat folder dengan nama assets di root folder
		$config['file_name'] = $fileName;
		$config['allowed_types'] = '*';
		$config['max_size'] = 100000;

		$this->load->library('upload');
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('file')) {
			$jupload = 0;
			echo $this->upload->display_errors();
		} else {

			$media = $this->upload->data('file');

			$inputFileName = './__statics/excel/tmp/' . $fileName;

			try {
				$inputFileType = IOFactory::identify($inputFileName);
				$objReader = IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
			}

			$sheet = $objPHPExcel->getSheet($sheetup);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();


			$jupload = 0;
			for ($row = $rowup; $row <= $highestRow; $row++) {                  //  Read a row of data into an array                 
				$rowData = $sheet->rangeToArray(
					'A' . $row . ':' . $highestColumn . $row,
					NULL,
					TRUE,
					FALSE
				);



				$nip        =      (trim($rowData[0][1]));
				$nama         =      (trim($rowData[0][2]));
				$golongan     			 =      (trim($rowData[0][3]));
				$jabatan                 =      (trim($rowData[0][4]));
				$jabatan_penugasan       =      (trim($rowData[0][5]));
				$unitkerja              =      (trim($rowData[0][6]));



				if (!empty($nip)) {


					$jupload++;
					$data = array(
						"nip" => $nip,
						"nama" => $nama,
						"golongan" => $golongan,
						"jabatan" => $jabatan,
						"jabatan_penugasan" => $jabatan_penugasan,
						"unitkerja" => $unitkerja

					);




					$this->db->set("unitkerja_id", $unitkerja_id);
					$this->db->insert("pegawai", $data);
				}
			}



			echo "<br><br><center>Data yg di upload : " . $jupload . " Data <br> <a href='" . site_url("schoolsiswa") . "' > Kembali ke Aplikasi </a>";
		}
	}

	public function generateJabatan()
	{

		$data = $this->db->get("pegawai")->result();
		foreach ($data as $row) {
			$jabatan_id = $this->db->get_where("jabatan", array("nama" => $row->jabatan_penugasan))->row();

			$this->db->set("jabatan_id", $jabatan_id->id);
			$this->db->where("id", $row->id);
			$this->db->update("pegawai");
		}
	}



	public function form()
	{

		$id = $this->input->get_post("id");
		$data = array();
		if (!empty($id)) {

			$data['data']  = $this->Reff->get_where("pegawai", array("id" => $id));
		}
		$this->load->view("form", $data);
	}



	public function save()
	{

		$this->form_validation->set_message('required', '{field} Wajib diisi. ');
		$this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
		$this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
		$this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');



		$config = array(


			array('field' => 'f[nip]', 'label' => 'Nomor Induk Pegawai  ', 'rules' => 'trim|required'),
			array('field' => 'f[nama]', 'label' => 'Nama   ', 'rules' => 'trim|required'),
			array('field' => 'f[golongan]', 'label' => 'Golongan   ', 'rules' => 'trim|required'),
			array('field' => 'f[jabatan]', 'label' => 'Jabatan   ', 'rules' => 'trim|required'),
			array('field' => 'f[jabatan_id]', 'label' => 'Jabatan Penugasan   ', 'rules' => 'trim|required'),
			array('field' => 'f[unitkerja_id]', 'label' => 'Unit Kerja    ', 'rules' => 'trim|required'),
		);

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == true) {

			$id = $this->input->get_post("id", true);
			$f  = xssArray($this->input->get_post("f", true));




			if (empty($id)) {


				$this->db->insert("pegawai", $f);
				echo "Data Berhasil disimpan";
			} else {
				$this->db->where("id", $id);
				$this->db->update("pegawai", $f);
				echo "Data Berhasil disimpan";
			}
		} else {


			header('Content-Type: application/json');
			echo json_encode(array('error' => true, 'message' => validation_errors()));
		}
	}

	public function hapus()
	{

		$id = $this->input->get_post("id", true);

		$this->db->delete("pegawai", array("id" => $id));
		echo "sukses";
	}

	public function getDataPegawai()
	{
		$token = $this->Oauth();
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://api.kemenag.go.id/v1/pegawai',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('satker' => '01100000000000'),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer ' . $token . '',
				'Cookie: cookiesession1=678B28B5DEFGIJKLMNOPQRSTVWXYF409; ropeg_apisimpeg=3qheld7sirm34b0o5n739tlp27l47j34'
			),
		));

		$response = json_decode(curl_exec($curl));

		// print_r($response); exit();

		$nip_baru ="";
		foreach ($response->data as $item) {
			$data = array(
				"NIP" => $item->NIP,
				"NIP_BARU" => $item->NIP_BARU,
				"NAMA" => $item->NAMA,
				"NAMA_LENGKAP" => $item->NAMA_LENGKAP,
				"AGAMA" => $item->AGAMA,
				"TEMPAT_LAHIR" => $item->TEMPAT_LAHIR,
				"TANGGAL_LAHIR" => $item->TANGGAL_LAHIR,
				"JENIS_KELAMIN" => $item->JENIS_KELAMIN,
				"PENDIDIKAN" => $item->PENDIDIKAN,
				"KODE_LEVEL_JABATAN" => $item->KODE_LEVEL_JABATAN,
				"LEVEL_JABATAN" => $item->LEVEL_JABATAN,
				"PANGKAT" => $item->PANGKAT,
				"GOL_RUANG" => $item->GOL_RUANG,
				"TMT_CPNS" => $item->TMT_CPNS,
				"TMT_PANGKAT" => $item->TMT_PANGKAT,
				"MASAKERJA_TAHUN" => $item->MASAKERJA_TAHUN,
				"MASAKERJA_BULAN" => $item->MASAKERJA_BULAN,
				"TIPE_JABATAN" => $item->TIPE_JABATAN,
				"KODE_JABATAN" => $item->KODE_JABATAN,
				"TAMPIL_JABATAN" => $item->TAMPIL_JABATAN,
				"TMT_JABATAN" => $item->TMT_JABATAN,
				"KODE_SATKER_1" => $item->KODE_SATKER_1,
				"SATKER_1" => $item->SATKER_1,
				"KODE_SATKER_2" => $item->KODE_SATKER_2,
				"SATKER_2" => $item->SATKER_2,
				"KODE_SATKER_3" => $item->KODE_SATKER_3,
				"SATKER_3" => $item->SATKER_3,
				"KODE_SATKER_4" => $item->KODE_SATKER_4,
				"SATKER_4" => $item->SATKER_4,
				"KODE_SATKER_5" => $item->KODE_SATKER_5,
				"SATKER_5" => $item->SATKER_5,
				"SATUAN_KERJA" => $item->SATUAN_KERJA,
				"STATUS_KAWIN" => $item->STATUS_KAWIN,
				"ALAMAT_1" => $item->ALAMAT_1,
				"ALAMAT_2" => $item->ALAMAT_2,
				"TELEPON" => $item->TELEPON,
				"KAB_KOTA" => $item->KAB_KOTA,
				"PROVINSI" => $item->PROVINSI,
				"KODE_POS" => $item->KODE_POS,
				"KODE_LOKASI" => $item->KODE_LOKASI,
				"KODE_PANGKAT" => $item->KODE_PANGKAT,
				"NO_HP" => $item->NO_HP,
				"EMAIL" => $item->EMAIL,
				"TMT_PANGKAT_YAD" => $item->TMT_PANGKAT_YAD,
				"TMT_KGB_YAD" => $item->TMT_KGB_YAD,
				"TMT_PENSIUN" => $item->TMT_PENSIUN,
				"KODE_KUA" => $item->KODE_KUA,
				"NSM" => $item->NSM,
				"NPSN" => $item->NPSN,
				"STATUS_PEGAWAI" => $item->STATUS_PEGAWAI
			);

			$nip_baru .=",".$item->NIP_BARU;

			$cek = $this->db->query("SELECT * from pegawai_simpeg where NIP_BARU='" . $item->NIP_BARU . "'")->result();

			if (!is_null($cek)) {
				$this->db->where("NIP_BARU", $item->NIP_BARU);
				$this->db->update("pegawai_simpeg", $data);
				echo "Data dengan NIP " . $item->NIP_BARU . " Berhasil diperbaharui <br/>";
			} else {
				$this->db->insert("pegawai_simpeg", $data);
				echo "Data dengan NIP " . $item->NIP_BARU . " Berhasil Ditabahkan <br/>";
			}
		}

		echo "Data Pegawai Berhasil Diperbaharui <br/>";

		 $nip_baru = substr($nip_baru,1); 

		$pegawai = $this->db->query("UPDATE pegawai_simpeg set status=1 where NIP_BARU NOT IN(".$nip_baru.")");
  
	}

	public function Oauth()
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://api.kemenag.go.id/v1/auth/login',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('email' => 'sipintar@kemenag.go.id', 'password' => 'S1p1nt4r'),
			CURLOPT_HTTPHEADER => array(
				'Cookie: cookiesession1=678B28B5DEFGIJKLMNOPQRSTVWXYF409; ropeg_apisimpeg=v8br5866f3tj93eab7osj8dt2rehiiom'
			),
		));

		$response = json_decode(curl_exec($curl));
		curl_close($curl);
		return $response->token;
	}
}
