<main id="main" class="main">

<div class="pagetitle">
  <h1>Laporan Perjalanan Dinas</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.html">Home</a></li>
      <li class="breadcrumb-item active">Laporan Perjalanan Dinas</li>
    </ol>
  </nav>
</div>

<section class="section dashboard">
  <div class="row">
  <!-- Reports -->
  <div class="col-12">
          <div class="card">

            <div class="filter">
              <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
              <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                <li class="dropdown-header text-start">
                  <h6>Filter</h6>
                </li>

                <li><a class="dropdown-item" href="#">Today</a></li>
                <li><a class="dropdown-item" href="#">This Month</a></li>
                <li><a class="dropdown-item" href="#">This Year</a></li>
              </ul>
            </div>

            <div class="card-body">
            <h5 class="card-title">Data Kegiatan </h5>

            <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
              <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Rincian Biaya </button>
            </li>
<!--             
              <li class="nav-item" role="presentation">
                  <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Petugas  </button>
                </li> -->
              
          
          </ul>
          <div class="tab-content pt-2" id="myTabContent">
            <?php 
              
              if(!is_null($kegiatan)){
                // print_r($auditor);
             ?>

            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
               <table class="table">
                    <tr>
                       <td> NIP </td>
                       <td> <?php echo  $auditor->NIP_BARU; ?> </td>

                   </tr>
                    <tr>
                       <td> Auditor </td>
                       <td> <?php echo  $auditor->NAMA_LENGKAP; ?> </td>

                   </tr>
                   <tr>
                       <td> Jenis Kegiatan </td>
                       <td> <?php echo  $this->Reff->get_kondisi(array("id"=>$kegiatan->jenis),"tm_jenis","nama"); ?> </td>

                   </tr>

                   <tr>
                       <td> Judul Kegiatan </td>
                       <td> <?php echo  $kegiatan->judul; ?> </td>

                   </tr>

                   <tr>
                       <td> Provinsi </td>
                       <td> <?php echo  $this->Reff->get_kondisi(array("id"=>$kegiatan->provinsi_id),"provinsi","nama"); ?> </td>

                   </tr>

                   <tr>
                       <td> Kabupaten/Kota </td>
                       <td> <?php echo  $this->Reff->get_kondisi(array("id"=>$kegiatan->kota_id),"kota","nama"); ?> </td>

                   </tr>
                   <tr>
                       <td> Auditi  </td>
                       <td>  
                         <ol>
                         <?php 
                         $satker = $this->db->query("SELECT id,kode,nama from satker where id IN(".$kegiatan->satker_id.")")->result();
                           foreach($satker as $rsat){
                            ?><li> <?php echo $rsat->kode; ?> - <?php echo $rsat->nama; ?> <?php 
                           }
                       
                          ?>
                          </ol>
                          </td>

                   </tr>
                   <tr>
                       <td> Tanggal Mulai Kegiatan  </td>
                       <td> <?php echo  $this->Reff->formattanggalstring($kegiatan->tgl_mulai); ?> </td>

                   </tr>
                   <tr>
                       <td> Tanggal Selesai Kegiatan  </td>
                       <td> <?php echo  $this->Reff->formattanggalstring($kegiatan->tgl_selesai); ?> </td>

                   </tr>
                   <tr>
                       <td> Anggaran  </td>
                       <td> <?php echo 'Rp. '.$this->Reff->formatuang2($detail->anggaran); ?> </td>
                   </tr>
                   <tr>
                       <td> Transfer Uang Muka  </td>
                       <td><?php echo (is_null($detail->uang_muka))? 'Rp. '.$this->Reff->formatuang2($detail->anggaran) : 'Rp. '.$this->Reff->formatuang2($detail->uang_muka); ?></td>
                   </tr>
               </table>



            </div>
            <br/>

            <?php
                $bst = $this->Reff->formatuang2($this->Reff->get_kondisi(array("pejabat_id"=>$detail->pejabat_id, "kegiatan_id"=>$kegiatan->id, "komponen"=>'bst'),"tr_verifikasi","nilai"));
                $uh = $this->Reff->formatuang2($this->Reff->get_kondisi(array("pejabat_id"=>$detail->pejabat_id, "kegiatan_id"=>$kegiatan->id, "komponen"=>'uh'),"tr_verifikasi","nilai"));
                $transport = $this->Reff->formatuang2($this->Reff->get_kondisi(array("pejabat_id"=>$detail->pejabat_id, "kegiatan_id"=>$kegiatan->id, "komponen"=>'transport'),"tr_verifikasi","nilai"));
                $hotel = $this->Reff->formatuang2($this->Reff->get_kondisi(array("pejabat_id"=>$detail->pejabat_id, "kegiatan_id"=>$kegiatan->id, "komponen"=>'hotel'),"tr_verifikasi","nilai"));
                $representatif = $this->Reff->formatuang2($this->Reff->get_kondisi(array("pejabat_id"=>$detail->pejabat_id, "kegiatan_id"=>$kegiatan->id, "komponen"=>'representatif'),"tr_verifikasi","nilai"));
                $lain = $this->Reff->formatuang2($this->Reff->get_kondisi(array("pejabat_id"=>$detail->pejabat_id, "kegiatan_id"=>$kegiatan->id, "komponen"=>'lain'),"tr_verifikasi","nilai"));
		            $ttl = $this->db->query("SELECT sum(nilai) as total from tr_verifikasi where kegiatan_id='{$kegiatan->id}' and pejabat_id='{$detail->pejabat_id}'")->row();

                $kbst = $this->Reff->get_kondisi(array("pejabat_id"=>$detail->pejabat_id, "kegiatan_id"=>$kegiatan->id, "komponen"=>'bst'),"tr_verifikasi","keterangan");
                $kuh = $this->Reff->get_kondisi(array("pejabat_id"=>$detail->pejabat_id, "kegiatan_id"=>$kegiatan->id, "komponen"=>'uh'),"tr_verifikasi","keterangan");
                $ktransport = $this->Reff->get_kondisi(array("pejabat_id"=>$detail->pejabat_id, "kegiatan_id"=>$kegiatan->id, "komponen"=>'transport'),"tr_verifikasi","keterangan");
                $khotel = $this->Reff->get_kondisi(array("pejabat_id"=>$detail->pejabat_id, "kegiatan_id"=>$kegiatan->id, "komponen"=>'hotel'),"tr_verifikasi","keterangan");
                $krepresentatif = $this->Reff->get_kondisi(array("pejabat_id"=>$detail->pejabat_id, "kegiatan_id"=>$kegiatan->id, "komponen"=>'representatif'),"tr_verifikasi","keterangan");
                $klain = $this->Reff->get_kondisi(array("pejabat_id"=>$detail->pejabat_id, "kegiatan_id"=>$kegiatan->id, "komponen"=>'lain'),"tr_verifikasi","keterangan");
            ?>

                <table class="table">
                    <thead>
                      <tr>
                       <th> Komponen </th>
                       <th> Pagu</th>
                       <th> Uang Muka </th>
                       <th> Pengeluaran Ril </th>
                       <th> Keterangan </th>
                       <th> Bukti </th>
                     </tr>

                   </thead>
                    <tbody>
                        <tr>
                            <td>BST</td>
                            <td><?php echo 'Rp. '.$this->Reff->formatuang2($detail->bst); ?></td>
                            <td><?php echo 'Rp. '.$this->Reff->formatuang2($detail->bst); ?></td>
                            <td width="200px"><input class="form-control keychange" type="text" id="bst" value="<?php echo $bst; ?>" data_id="<?php echo $detail->pejabat_id; ?>" data_keg_id="<?php echo $kegiatan->id; ?>"/></td>
                            <td><textarea class="from-control tchange" id="bst_keterangan"  rows="2" cols="50" data_id="<?php echo $detail->pejabat_id; ?>" data_keg_id="<?php echo $kegiatan->id; ?>"><?php echo $kbst; ?></textarea></td>
                            <td><button class="btn btn-danger uploadDokumen" data-id="bst" type="button" data-bs-toggle="modal" data-bs-target="#largeModal"><i class="fa fa-upload"></i> </button></td>
                        </tr>    
                        <tr>    
                            <td>Uang Harian</td>
                            <td><?php echo 'Rp. '.$this->Reff->formatuang2($detail->uh); ?></td>
                            <td><?php echo 'Rp. '.$this->Reff->formatuang2($detail->uh * $detail->waktu * 0.75); ?></td>
                            <td width="200px"><input class="form-control keychange" type="text" id="uh" value="<?php echo $uh; ?>" data_id="<?php echo $detail->pejabat_id; ?>" data_keg_id="<?php echo $kegiatan->id; ?>"/></td>
                            <td><textarea class="from-control tchange" id="uh_keterangan"  rows="2" cols="50" data_id="<?php echo $detail->pejabat_id; ?>" data_keg_id="<?php echo $kegiatan->id; ?>"><?php echo $kuh; ?></textarea></td>
                            <td><button class="btn btn-danger uploadDokumen" data-id="uh" type="button" data-bs-toggle="modal" data-bs-target="#largeModal"><i class="fa fa-upload"></i> </button></td>
                        </tr>    
                        <tr>  
                            <td>Transport (PP)</td>
                            <td><?php echo 'Rp. '.$this->Reff->formatuang2($detail->transport); ?></td>
                            <td><?php echo 'Rp. '.$this->Reff->formatuang2($detail->transport); ?></td>
                            <td width="200px"><input class="form-control keychange" type="text" id="transport" value="<?php echo $transport; ?>" data_id="<?php echo $detail->pejabat_id; ?>" data_keg_id="<?php echo $kegiatan->id; ?>"/></td>
                            <td><textarea class="from-control tchange" id="transport_keterangan" rows="2" cols="50" data_id="<?php echo $detail->pejabat_id; ?>" data_keg_id="<?php echo $kegiatan->id; ?>"><?php echo $ktransport; ?></textarea></td>
                            <td><button class="btn btn-danger uploadDokumen" data-id="transport" type="button" data-bs-toggle="modal" data-bs-target="#largeModal"><i class="fa fa-upload"></i> </button></td>
                        </tr>    
                        <tr>  
                            <td>Hotel</td>
                            <td><?php echo 'Rp. '.$this->Reff->formatuang2($detail->hotel); ?></td>
                            <td><?php echo 'Rp. '.$this->Reff->formatuang2($detail->hotel * ($detail->waktu-1)); ?></td>
                            <td width="200px"><input class="form-control keychange" type="text" id="hotel" value="<?php echo $hotel; ?>" data_id="<?php echo $detail->pejabat_id; ?>" data_keg_id="<?php echo $kegiatan->id; ?>"/></td>
                            <td><textarea class="from-control tchange" id="hotel_keterangan" rows="2" cols="50" data_id="<?php echo $detail->pejabat_id; ?>" data_keg_id="<?php echo $kegiatan->id; ?>"><?php echo $khotel; ?></textarea></td>
                            <td><button class="btn btn-danger uploadDokumen" data-id="hotel" type="button" data-bs-toggle="modal" data-bs-target="#largeModal"><i class="fa fa-upload"></i> </button></td>
                        </tr>    
                        <tr>  
                            <td>Representatif</td>
                            <td><?php echo 'Rp. '.$this->Reff->formatuang2($detail->representatif); ?></td>
                            <td><?php echo 'Rp. '.$this->Reff->formatuang2($detail->representatif); ?></td>
                            <td width="200px"><input class="form-control keychange" type="text" id="representatif" value="<?php echo $representatif; ?>" data_id="<?php echo $detail->pejabat_id; ?>" data_keg_id="<?php echo $kegiatan->id; ?>"/></td>
                            <td><textarea class="from-control tchange" id="representatif_keterangan" rows="2" cols="50" data_id="<?php echo $detail->pejabat_id; ?>" data_keg_id="<?php echo $kegiatan->id; ?>"><?php echo $krepresentatif; ?></textarea></td>
                            <td><button class="btn btn-danger uploadDokumen" data-id="representatif" type="button" data-bs-toggle="modal" data-bs-target="#largeModal"><i class="fa fa-upload"></i> </button></td>
                        </tr>
                        <tr> 
                            <td>Lain-Lain</td>
                            <td></td>
                            <td></td>
                            <td width="200px"><input class="form-control keychange" type="text" id="lain" value="<?php echo $lain; ?>" data_id="<?php echo $detail->pejabat_id; ?>" data_keg_id="<?php echo $kegiatan->id; ?>"/></td>
                            <td><textarea class="from-control tchange" id="lain_keterangan" rows="2" cols="50" data_id="<?php echo $detail->pejabat_id; ?>" data_keg_id="<?php echo $kegiatan->id; ?>"><?php echo $klain; ?></textarea></td>
                            <td><button class="btn btn-danger uploadDokumen" data-id="lain" type="button" data-bs-toggle="modal" data-bs-target="#largeModal"><i class="fa fa-upload"></i> </button></td>
                        </tr>  
                        <tr> 
                            <td></td>
                            <td></td>
                            <td></td>
                            <td width="200px"><input class="from-control" readonly type="text" id="ttl" value="<?php echo $this->Reff->formatuang2($ttl->total);?>"/></td>
                            <td></td>
                            <td></td>
                        </tr>  
                    </tbody>
                 </table>
                 <div class="alert alert-info"><input type="button" id="submit_spj" class="btn btn-primary" value="Submit Laporan"></button><div>
            </div>

            <?php 
              }else{

                ?> Tidak ada kegiatan yang dipilih <?php 
              }

              ?>
            </div>
           
            

            </div>

          </div>
        </div><!-- End Reports -->
</div>
</div>
</main>

<div class="modal fade" id="largeModal" tabindex="-1">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"></h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body" id="loadform">
	        <p>Loading...</p>
        </div>
        <div class="modal-footer">
         
        </div>
      </div>
    </div>
</div>

<script>
  $(document).off("change", ".keychange").on("change", ".keychange", function(e) {

    var kegiatan_id = $(this).attr('data_keg_id');
    var id = $(this).attr("data_id");
    var field = $(this).attr("id");
    var nilai = HapusTitik($(this).val());

    loading();
    $.post("<?php echo site_url("auditor/saveVerifikasi"); ?>", {
      kegiatan_id: kegiatan_id,
      id: id,
      nilai: nilai,
      field: field,
    }, function(data) {
        // alert ('Data Berhasil Disimpan');
        data = data.split('|');
        $('#'+field).val(data[0]);
        $('#ttl').val(data[1]);
    //   $("#load_sbm").html(data);
      jQuery.unblockUI({});
    })

  });

  $(document).off("change", ".tchange").on("change", ".tchange", function(e) {

    var kegiatan_id = $(this).attr('data_keg_id');
    var id = $(this).attr("data_id");
    var field = $(this).attr("id");
    var keterangan = $(this).val();


    // alert (kegiatan_id +' '+id+ '-' +field+ '-' + keterangan);
    loading();
    $.post("<?php echo site_url("auditor/saveVerifikasiKet"); ?>", {
    kegiatan_id: kegiatan_id,
    id: id,
    keterangan: keterangan,
    field: field,
    }, function(data) {
        // alert ('Data Berhasil Disimpan');
        $('#'+field).val(data);
    //   $("#load_sbm").html(data);
      jQuery.unblockUI({});
    })

  });

  $(document).on("click",".uploadDokumen",function(){
	var komponen = $(this).attr("data-id");

	$.post("<?php echo site_url('auditor/openUpload'); ?>",
            {
                kegiatan:<?php echo $kegiatan->id; ?>,
                pejabat:<?php echo $detail->pejabat_id; ?>,
                komponen:komponen,
            },
            function(data){
                // alert(data);
		        $("#loadform").html(data);

	})
 });

  $(document).on("click","#submit_spj",function() {
    loading();
    $.post("<?php echo site_url('auditor/submitSpj'); ?>",
            {
                kegiatan:<?php echo $kegiatan->id; ?>,
                pejabat:<?php echo $detail->pejabat_id; ?>,
                total:$('#ttl').val(),
            },
            function(data){
            alert("Laporan Berhasil Disubmit");
		        //$("#loadform").html(data);
            jQuery.unblockUI({});

	  })
  });



</script>