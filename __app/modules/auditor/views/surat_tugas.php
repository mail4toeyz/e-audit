
<?php 
   $kegiatan		   = $this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
  
?>
<main id="main" class="main">

<div class="pagetitle">
  <h1>Surat Tugas <?php echo $kegiatan->judul; ?> </h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active">Surat Tugas </li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section dashboard">
  <div class="row">
  <div class="card">
		<div class="card-body">
        <iframe src="https://drive.google.com/file/d/<?php echo $kegiatan->file_st; ?>/preview" class="holds-the-iframe"  width="100%" height="700px" allow="autoplay"></iframe>
        </div>
  </div>
  </div>
</section>

</main>
