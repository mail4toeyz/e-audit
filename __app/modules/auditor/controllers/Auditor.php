<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auditor extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		  if(!$this->session->userdata("is_login")){
			    
				echo $this->Reff->sessionhabis();
				exit();
			
		  }
		  $this->load->model('M_dashboard','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('auditor/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax              = $this->input->get_post("ajax",true);	
		 $data['title']   	= "Dashboard | ".$_SESSION['nama'];
		 $data['kegiatan']  = $this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
	     if(!empty($ajax)){
					    
			 $this->load->view('page_default',$data);
		
		 }else{
			 
			
		     $data['konten'] = "page_default";
			 
			 $this->_template($data);
		 }
	

	}

	public function surattugas(){
		$ajax              = $this->input->get_post("ajax",true);	
		$data['title']   	= "Surat Tugas | ".$_SESSION['nama'];
		$data['kegiatan']  = $this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
		if(!empty($ajax)){
					   
			$this->load->view('surat_tugas',$data);
	   
		}else{
			
		   
			$data['konten'] = "surat_tugas";
			
			$this->_template($data);
		}
   

	}

	
	
	 
	 public function grid(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		  $role = array("1"=>"Administrator Pusat","2"=>"Madrasah","3"=>"Peserta");
		  
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			    
			   
				$no = $i++;
				$records["data"][] = array(
					$no,
			
              	
					
					$role[$val['status']],
					$val['keterangan'],
					$this->Reff->timeAgo($val['tanggal']),
					
		
				   
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	
	public function setKegiatan(){
         $kegiatan_id      = $_POST['kegiatan_id'];
      	 $auditor   	   = $this->db->get_where("pegawai_simpeg",array("id"=>$_SESSION['idAuditor']))->row();
		 $kegiatan		   = $this->db->get_where("kegiatan",array("id"=>$kegiatan_id))->row();
		 $anggota = explode(",",$kegiatan->anggota);

		
		 if($kegiatan->ketua==$_SESSION['idAuditor']){
			$group_id ="ketua";
		 }else if(in_array($_SESSION['idAuditor'], $anggota)){
			$group_id ="anggota";
		 }else  if($kegiatan->pengendali_teknis==$_SESSION['idAuditor']){
			$group_id ="dalnis";
		 }
		 
		 $session = array(
			'idAuditor'        => $auditor->id,
			'kegiatan_id'        => $kegiatan_id,
			'nama'        => $auditor->NAMA_LENGKAP,
			'group_id'        => $group_id,
			'is_login'        => true,
			'date_login'   => date("Y-m-d H:i:s"));
			
			print_r($session);
			$this->session->sess_expiration = 0;
			$this->session->set_userdata($session);

			


	}

	public function laporan()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['kegiatan'] =$this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
		 $data['title']    = $data['kegiatan']->judul;
	     if(!empty($ajax)){
					    
			 $this->load->view('page_laporan',$data);
		
		 }else{
			 
			
		     $data['konten'] = "page_laporan";
			 
			 $this->_template($data);
		 }
	

	}
	
 
	
	public function logout(){
		     $this->session->sess_destroy();
			 
		
			echo "yes";
		 
	 }

	 public function generate_skor(){


		$h_ujian = $this->db->query("SELECT * FROM `h_ujian` where tmujian_id=25")->result();

		 foreach($h_ujian as $row){

		  $list_jawaban = $row->list_jawaban;
		  
		  $pc_jawaban = explode(",", $list_jawaban);
		  
		  $jumlah_benar 	= 0;
		  $jumlah_salah 	= 0;
		  $jumlah_ragu  	= 0;
		  $nilai_bobot 	= 0;
		  $total_bobot	= 0;
		  $jumlah_soal	= sizeof($pc_jawaban);

		  $arab_benar            = 0;
		  $inggris_benar          = 0;
		  $islam_benar      = 0;
		  
		  $arab_salah            = 0;
		  $inggris_salah          = 0;
		  $islam_salah      = 0;
		 

		  $arab            = 0;
		  $inggris          = 0;
		  $islam           = 0;
		  


  
		  foreach ($pc_jawaban as $jwb) {

			  $pc_dt 		= explode(":", $jwb);
			  $id_soal 	= $pc_dt[0];
			  $jawaban 	= $pc_dt[1];
			  $ragu 		= $pc_dt[2];
  
			  $cek_jwb 	     = $this->m->getSoalById($id_soal);
			  $cek_kategori 	 = $cek_jwb->paket; 
			 
			  
			  if($cek_jwb->jenis==1){
  
				  if($cek_jwb->jawaban==$jawaban){
					  $jumlah_benar++;
					  $nilai_bobot = $nilai_bobot + $cek_jwb->bobot;
					  $total_bobot = $total_bobot + $cek_jwb->bobot;

					  if($cek_kategori==3){

							$arab = $arab + $cek_jwb->bobot;
							$arab_benar++;
						  
					  }else if($cek_kategori==2){

							$inggris = $inggris + $cek_jwb->bobot;
							$inggris_benar++;

					  }else if($cek_kategori==1){

							$islam = $islam + $cek_jwb->bobot;
							$islam_benar++;
					  }


				  }else{
					  $jumlah_salah++;

					   if($cek_kategori==3){

						  $arab_salah++;
						  
						}else if($cek_kategori==2){

							$inggris_salah++;
						}else if($cek_kategori==1){

							$islam_salah++;
						}


				  } 
  
			  }
			  
		  }
         
		  echo $this->Reff->get_kondisi(array("id"=>$row->tmsiswa_id),"tm_siswa","nama")."-".$islam_benar."-".$inggris."-".$islam."<br>"; 

		$cekData = $this->db->get_where("tr_skor",array("tmsiswa_id"=>$row->tmsiswa_id))->num_rows();
		  if($cekData==0){

			$nilai_bobot 	= ($islam+$inggris+$arab);

			$this->db->set("tmsiswa_id",$row->tmsiswa_id);			
			$this->db->set("islam_benar",$islam_benar);
			$this->db->set("islam_salah",$islam_salah);
			$this->db->set("islam",$islam);
			$this->db->set("inggris_benar",$inggris_benar);
			$this->db->set("inggris_salah",$inggris_salah);
			$this->db->set("inggris",$inggris);
			$this->db->set("arab_benar",$arab_benar);
			$this->db->set("arab_salah",$arab_salah);
			$this->db->set("arab",$arab);
			$this->db->set("skor_mapel",$nilai_bobot);
			$this->db->insert("tr_skor");
			

		  }else{

			$nilai_bobot 	= ($islam+$inggris+$arab);

			$this->db->where("tmsiswa_id",$row->tmsiswa_id);			
			$this->db->set("islam_benar",$islam_benar);
			$this->db->set("islam_salah",$islam_salah);
			$this->db->set("islam",$islam);
			$this->db->set("inggris_benar",$inggris_benar);
			$this->db->set("inggris_salah",$inggris_salah);
			$this->db->set("inggris",$inggris);
			$this->db->set("arab_benar",$arab_benar);
			$this->db->set("arab_salah",$arab_salah);
			$this->db->set("arab",$arab);
			$this->db->set("skor_mapel",$nilai_bobot);
			$this->db->update("tr_skor");


		  }


		  //$nilai 			= ($jumlah_benar / ($jumlah_soal))  * 100;
		  //$nilai_bobot 	= ($total_bobot / ($jumlah_soal))  * 100;

		//   $andragogi  = number_format($andragogi/10,1);
		//   $skor = $edm + $erkam +  $bos + $andragogi;
  
		//   $d_update = [
		// 	  'edm_benar'		=> $edm_benar,
		// 	  'erkam_benar'		=> $erkam_benar,
		// 	  'bos_benar'		=> $bos_benar,
		// 	  'andragogi_benar'		=> $andragogi_benar,
		// 	  'edm_salah'		=> $edm_salah,
		// 	  'erkam_salah'		=> $erkam_salah,
		// 	  'bos_salah'		=> $bos_salah,
		// 	  'andragogi_salah'		=> $andragogi_salah,
		// 	  'edm'		=> $edm,
		// 	  'erkam'		=> $erkam,
		// 	  'bos'		=> $bos,
		// 	  'andragogi'		=> $andragogi,
		// 	  'skor'		=> $skor,
		// 	  'nilai'		=> $skor,
		// 	  'nilai_bobot'		=> $skor
			  
			  
		//   ];
		  
		//   $this->db->update('h_ujian', $d_update,array("id"=>$row->id));

		}

	  echo "sukses";
  


	 }

	 public function generate_skor_kompetensi(){


		$h_ujian = $this->db->query("SELECT * FROM `h_ujian` where tmujian_id=26")->result();

		 foreach($h_ujian as $row){

		  $list_jawaban = $row->list_jawaban;
		  
		  $pc_jawaban = explode(",", $list_jawaban);
		  
		  $jumlah_benar 	= 0;
		  $jumlah_salah 	= 0;
		  $jumlah_ragu  	= 0;
		  $nilai_bobot 	= 0;
		  $total_bobot	= 0;
		  $jumlah_soal	= sizeof($pc_jawaban);

		  $kompetensi_benar            = 0;
		 
		 
		  $kompetensi_salah            = 0;
		 

		  $kompetensi            = 0;
		 


  
		  foreach ($pc_jawaban as $jwb) {

			  $pc_dt 		= explode(":", $jwb);
			  $id_soal 	= $pc_dt[0];
			  $jawaban 	= $pc_dt[1];
			  $ragu 		= $pc_dt[2];
  
			  $cek_jwb 	     = $this->m->getSoalById($id_soal);
			  $cek_kategori 	 = $cek_jwb->paket; 
			 
			  
			  if($cek_jwb->jenis==1){
  
				  if($cek_jwb->jawaban==$jawaban){
					  $jumlah_benar++;
					  $nilai_bobot = $nilai_bobot + $cek_jwb->bobot;
					  $total_bobot = $total_bobot + $cek_jwb->bobot;

					  if($cek_kategori==4){

							$kompetensi = $kompetensi + $cek_jwb->bobot;
							$kompetensi_benar++;
						  
					  }


				  }else{
					  $jumlah_salah++;

					   if($cek_kategori==4){

						  $kompetensi_salah++;
						  
						}


				  } 
  
			  }
			  
		  }
         
		 // echo $this->Reff->get_kondisi(array("id"=>$row->tmsiswa_id),"tm_siswa","nama")."-".$islam_benar."-".$inggris."-".$islam."<br>"; 


			

			$this->db->where("tmsiswa_id",$row->tmsiswa_id);			
			$this->db->set("kompetensi_benar",$kompetensi_benar);
			$this->db->set("kompetensi_salah",$kompetensi_salah);
			$this->db->set("kompetensi",$kompetensi);
		
			$this->db->update("tr_skor");


		  


		  //$nilai 			= ($jumlah_benar / ($jumlah_soal))  * 100;
		  //$nilai_bobot 	= ($total_bobot / ($jumlah_soal))  * 100;

		//   $andragogi  = number_format($andragogi/10,1);
		//   $skor = $edm + $erkam +  $bos + $andragogi;
  
		//   $d_update = [
		// 	  'edm_benar'		=> $edm_benar,
		// 	  'erkam_benar'		=> $erkam_benar,
		// 	  'bos_benar'		=> $bos_benar,
		// 	  'andragogi_benar'		=> $andragogi_benar,
		// 	  'edm_salah'		=> $edm_salah,
		// 	  'erkam_salah'		=> $erkam_salah,
		// 	  'bos_salah'		=> $bos_salah,
		// 	  'andragogi_salah'		=> $andragogi_salah,
		// 	  'edm'		=> $edm,
		// 	  'erkam'		=> $erkam,
		// 	  'bos'		=> $bos,
		// 	  'andragogi'		=> $andragogi,
		// 	  'skor'		=> $skor,
		// 	  'nilai'		=> $skor,
		// 	  'nilai_bobot'		=> $skor
			  
			  
		//   ];
		  
		//   $this->db->update('h_ujian', $d_update,array("id"=>$row->id));

		}

	  echo "sukses";
  


	 }


	 public function generate_skor_moderasi(){


		$h_ujian = $this->db->query("SELECT * FROM `h_ujian` where tmujian_id=26")->result();

		 foreach($h_ujian as $row){

		  $list_jawaban = $row->list_jawaban;
		  
		  $pc_jawaban = explode(",", $list_jawaban);
		  
		  $jumlah_benar 	= 0;
		  $jumlah_salah 	= 0;
		  $jumlah_ragu  	= 0;
		  $nilai_bobot 	= 0;
		  $total_bobot	= 0;
		  $jumlah_soal	= sizeof($pc_jawaban);

		  $moderasi1            = 0;
		  $moderasi2            = 0;
		  $moderasi3            = 0;
		  $moderasi4            = 0;
		  $moderasi5            = 0;
		 
		 
		 


  
		  foreach ($pc_jawaban as $jwb) {

			  $pc_dt 		= explode(":", $jwb);
			  $id_soal 	= $pc_dt[0];
			  $jawaban 	= $pc_dt[1];
			  $ragu 		= $pc_dt[2];
  
			  $cek_jwb 	     = $this->m->getSoalById($id_soal);
			  $cek_kategori 	 = $cek_jwb->paket; 
			  if($cek_kategori==5){
			 

					if($jawaban=="A"){
						$moderasi1++;
					}else if($jawaban=="B"){
						$moderasi2++;
					}else if($jawaban=="C"){
						$moderasi3++;
						
					}else if($jawaban=="D"){
						$moderasi4++;
					}else if($jawaban=="E"){
						$moderasi5++;
					}
					
					
		
					$arr_nilai = array("A"=>"1","B"=>"2","C"=>"3","D"=>"4","E"=>"5");
					$nilai_bobot = $nilai_bobot + $arr_nilai[$jawaban];

			}
			//  $total_bobot = $total_bobot + $cek_jwb->bobot;
  
			  
			  
		  }
         
		 // echo $this->Reff->get_kondisi(array("id"=>$row->tmsiswa_id),"tm_siswa","nama")."-".$islam_benar."-".$inggris."-".$islam."<br>"; 


		   $cekData    = $this->db->get_where("tr_skor",array("tmsiswa_id"=>$row->tmsiswa_id))->row();
		   $skor_kamad = $cekData->kompetensi+$nilai_bobot;
			$this->db->where("tmsiswa_id",$row->tmsiswa_id);			
			$this->db->set("moderasi1",$moderasi1);
			$this->db->set("moderasi2",$moderasi2);
			$this->db->set("moderasi3",$moderasi3);
			$this->db->set("moderasi4",$moderasi4);
			$this->db->set("moderasi5",$moderasi5);
			$this->db->set("moderasi",$nilai_bobot);
			$this->db->set("skor_kamad",$skor_kamad);
		
			$this->db->update("tr_skor");


		  


		  //$nilai 			= ($jumlah_benar / ($jumlah_soal))  * 100;
		  //$nilai_bobot 	= ($total_bobot / ($jumlah_soal))  * 100;

		//   $andragogi  = number_format($andragogi/10,1);
		//   $skor = $edm + $erkam +  $bos + $andragogi;
  
		//   $d_update = [
		// 	  'edm_benar'		=> $edm_benar,
		// 	  'erkam_benar'		=> $erkam_benar,
		// 	  'bos_benar'		=> $bos_benar,
		// 	  'andragogi_benar'		=> $andragogi_benar,
		// 	  'edm_salah'		=> $edm_salah,
		// 	  'erkam_salah'		=> $erkam_salah,
		// 	  'bos_salah'		=> $bos_salah,
		// 	  'andragogi_salah'		=> $andragogi_salah,
		// 	  'edm'		=> $edm,
		// 	  'erkam'		=> $erkam,
		// 	  'bos'		=> $bos,
		// 	  'andragogi'		=> $andragogi,
		// 	  'skor'		=> $skor,
		// 	  'nilai'		=> $skor,
		// 	  'nilai_bobot'		=> $skor
			  
			  
		//   ];
		  
		//   $this->db->update('h_ujian', $d_update,array("id"=>$row->id));

		}

	  echo "sukses";
  


	 }


	public function generate_nilai(){

		$cekData    = $this->db->get("tr_skor")->result();
		  foreach($cekData as $r){
			  $nilai = $r->skor_mapel+$r->skor_kamad;
			  $this->db->set("nilai",$nilai);
			  $this->db->where("id",$r->id);
			  $this->db->update("tr_skor");

			  $this->db->set("nilai",$nilai);
			  $this->db->where("id",$r->tmsiswa_id);
			  $this->db->update("tm_siswa");


		  }
	}

	public function generate_peringkat(){

		$kelas = $this->db->query("SELECT * from madrasah_peminatan where id_admin IN(select madrasah_peminatan from tm_siswa)")->result();
		foreach($kelas as $i=>$r){

			 $siswa = $this->db->query("SELECT * from tm_siswa where madrasah_peminatan='{$r->id_admin}' order by nilai DESC")->result();
			 $peringkat=0;
			   foreach($siswa as $rs){
				   $peringkat++;
				   $this->db->set("peringkat",$peringkat);
				   $this->db->where("id",$rs->id);
				   $this->db->update("tm_siswa");


			   }


		}


	}

	public function spj()
	{  	
         $ajax              = $this->input->get_post("ajax",true);	
		 $data['title']   	= "Dashboard | ".$_SESSION['nama'];
		 $data['kegiatan']  = $this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
	     if(!empty($ajax)){
			 $this->load->view('page_spj',$data);
		 }else{
		     $data['konten'] = "page_spj";
			 
			 $this->_template($data);
		 }
	}

	public function isi_spj()
	{  	

         $ajax              = $this->input->get_post("ajax",true);	
		 $data['title']   	= "Dashboard | ".$_SESSION['nama'];
		 $id = base64_decode($this->input->get_post("id"));
		 $data['auditor'] = $this->db->get_where("pegawai_simpeg",array("id"=>$id))->row();
		 $data['kegiatan']  = $this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
		 $data['detail']  = $this->db->get_where("tr_kegiatanJabatan",array("kegiatan_id"=>$_SESSION['kegiatan_id'], "pejabat_id"=>$id))->row();

	     if(!empty($ajax)){
			 $this->load->view('isi_spj',$data);
		 }else{
		     $data['konten'] = "isi_spj";
			 
			 $this->_template($data);
		 }
	}

	public function saveVerifikasi(){
		$kegiatan_id        = $this->input->get_post('kegiatan_id');
		$pejabat_id                 = $this->input->get_post('id');
		$nilai               = $this->input->get_post('nilai');
		$field               = $this->input->get_post('field');

		$cek= $this->db->get_where("tr_verifikasi",array("kegiatan_id"=>$kegiatan_id, "pejabat_id"=>$pejabat_id, "komponen"=>$field))->row();

		if (is_null($cek)){
			$this->db->set("komponen",$field);
			$this->db->set("nilai",$nilai);
			$this->db->set("kegiatan_id",$kegiatan_id);
			$this->db->set("pejabat_id",$pejabat_id);
			$this->db->insert("tr_verifikasi");
		}else{
			$this->db->set("nilai",$nilai);
			$this->db->where("komponen",$field);
			$this->db->where("kegiatan_id",$kegiatan_id);
			$this->db->where("pejabat_id",$pejabat_id);
			$this->db->update("tr_verifikasi");
		}

		$ttl = $this->db->query("SELECT sum(nilai) as total from tr_verifikasi where kegiatan_id='{$kegiatan_id}' and pejabat_id='{$pejabat_id}'")->row();
		echo $this->Reff->formatuang2($nilai).'|'.$this->Reff->formatuang2($ttl->total);

	}

	public function saveVerifikasiKet(){
		$kegiatan_id        = $this->input->get_post('kegiatan_id');
		$pejabat_id                 = $this->input->get_post('id');
		$keterangan               = $this->input->get_post('keterangan');
		$field               = explode("_", $this->input->get_post('field'));

		$cek= $this->db->get_where("tr_verifikasi",array("kegiatan_id"=>$kegiatan_id, "pejabat_id"=>$pejabat_id, "komponen"=>$field[0]))->row();

		if (is_null($cek)){
			$this->db->set("komponen",$field[0]);
			$this->db->set("keterangan",$nilai);
			$this->db->set("kegiatan_id",$kegiatan_id);
			$this->db->set("pejabat_id",$pejabat_id);
			$this->db->insert("tr_verifikasi");
		}else{
			$this->db->set("keterangan",$keterangan);
			$this->db->where("komponen",$field[0]);
			$this->db->where("kegiatan_id",$kegiatan_id);
			$this->db->where("pejabat_id",$pejabat_id);
			$this->db->update("tr_verifikasi");
		}
		echo $keterangan;

	}

	public function openUpload(){
		$data['kegiatan_id'] = $this->input->get_post('kegiatan');
		$data['pejabat_id']  = $this->input->get_post('pejabat');
		$data['komponen']  = $this->input->get_post('komponen');
		
		// print_r($data);
		$this->load->view('berkas',$data);
		
	}

	public function berkas_upload(){
		$kegiatan_id = $this->input->get_post("kegiatan_id");
		$pejabat_id = $this->input->get_post("pejabat_id");
		$komponen = $this->input->get_post("komponen");


		$kegiatan     = $this->db->get_where("tr_kegiatanJabatan",array("kegiatan_id"=>$kegiatan_id, "pejabat_id"=>$pejabat_id))->row();
		
		$service 	= new Google_Drive();
		$foldernama = $pejabat_id;
		$foldernama = str_replace("/",DIRECTORY_SEPARATOR,$foldernama);
		$folderId   = $kegiatan->folder;

		$temp       = explode(".", $_FILES["file"]["name"]);
		$namafile   = $kegiatan_id."-".$pejabat_id.str_replace(array(" ","'"),"_",$_FILES["file"]["name"]).time().'.' . end($temp);

		if(!$folderId){
			$folderId = $service->getFileIdByName( BACKUP_FOLDER );
			if( !$folderId ) {
				$folderId = $service->createFolder( BACKUP_FOLDER );					
			}
			$folderId = $service->createMultiFolder($foldernama,$folderId);
			$this->db->set("folder",$folderId);
			
			$this->db->where("kegiatan_id",$kegiatan_id);
			$this->db->where("pejabat_id",$pejabat_id);
			$this->db->update("tr_kegiatanJabatan");
		}

		$fileId = $service->createFileFromPath( $_FILES["file"]['tmp_name'], $namafile, $folderId );
		$service->setPublic($fileId);

		$this->db->set("kegiatan_id",$kegiatan_id);
		$this->db->set("pejabat_id",$pejabat_id);
		$this->db->set("komponen",$komponen);
		$this->db->set("file",$fileId);
		$this->db->set("tgl_upload",date("Y-m-d H:i:s"));
		$this->db->insert("tr_verifikasi_berkas");
		header('Content-Type: application/json');
		echo json_encode(array('success' => true, 'message' => "Upload Sukses"));
	}

	public function hapus_berkas(){

							
		$this->db->where("id",$_POST['key']);
		
		$this->db->delete("tr_verifikasi_berkas");
		header('Content-Type: application/json');
		echo json_encode(array('success' => true, 'message' => "Hapus Sukses"));
	}

	public function submitSpj(){
		$kegiatan_id = $this->input->get_post("kegiatan");
		$pejabat_id = $this->input->get_post("pejabat");
		$total = $this->input->get_post("total");

		$this->db->set('total_verif', $total);
		$this->db->set('status_verif', 1);
		$this->db->where("kegiatan_id",$kegiatan_id);
		$this->db->where("pejabat_id",$pejabat_id);
		$this->db->update("tr_kegiatanJabatan");

		echo json_encode(array('success' => true, 'message' => "Sukses"));
	}
}
