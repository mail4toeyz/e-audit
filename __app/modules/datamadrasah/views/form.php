<style>
.btn-file {
  position: relative;
  overflow: hidden;
}
.btn-file input[type=file] {
  position: absolute;
  top: 0;
  right: 0;
  min-width: 100%;
  min-height: 100%;
  font-size: 100px;
  text-align: right;
  filter: alpha(opacity=0);
  opacity: 0;
  outline: none;
  background: white;
  cursor: inherit;
  display: block;
}
</style>
						
						 <form action="javascript:void(0)" method="post" id="simpangambarmodal" name="simpangambarmodal" url="<?php echo site_url("datamadrasah/save"); ?>">
						<input type="hidden" class="form-control"  name='id' value="<?php echo (isset($data)) ? $data->id :""; ?>" data-toggle="tooltip" title="Isi dengan Nama  Organisasi">
                                
							
								

								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                            <input type="text" class="form-control" onkeypress="javascript:return isNumber(event)" id="nsm" name="f[nsm]" value="<?php echo (isset($data)) ? $data->nsm :""; ?>" placeholder="NSM or NPSN" >
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                            <input type="text" class="form-control" id="nama" name="f[nama]" value="<?php echo (isset($data)) ? $data->nama :""; ?>" placeholder="Nama Madrasah" >
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
									<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                            <input type="password" class="form-control" id="password" name="password" value="<?php echo (isset($data)) ? dekrip($data->password) :""; ?>" placeholder="Buat Password" >
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										                        <select class="form-control" name="f[provinsi_id]">

                                            <?php 
                                               $prov = $this->db->get("provinsi")->result();
                                                 foreach($prov as $rp){
                                                    ?><option value="<?php echo $rp->id; ?>" <?php if(isset($data)){ echo ($rp->id==$data->provinsi_id) ? "selected":""; } ?>><?php echo $rp->nama; ?></option> <?php 


                                                 }
                                                 ?>


                                            </select>
                                            
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								
								
								  <br>
								 
								 <div class=" " id="aksimodal">
						       <center>
								   <button type="submit" class="btn btn-success btn-sm ">
										<i class="fa fa-save"></i>
										<span>Simpan Data  </span>
                                    </button>
								    <button type="reset" class="btn btn-success  btn-sm">
									
										<span>Reset   </span>
                                    </button>
									<button type="button" class="btn btn-success btn-sm " data-dismiss="modal">
										
										<span>Tutup   </span>
                                    </button>
									
								   </center>
								
								 </div>
								 
								  <div class="row clearfix" id="loadingmodal" style="display:none">
								  <center><div class="preloader pl-size-md"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div> <br> Data sedang disimpan, Mohon Tunggu ...</center>
								 
								 </div>
							  
					</form>	    
	<script>
	$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
	
	
	
    });
	
	function readURL(input) {
	  if (input.files && input.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function(e) {
		  $('#blah').attr('src', e.target.result);
		}
		
		reader.readAsDataURL(input.files[0]);
	  }
	}

	$("#imgInp").change(function() {
	  readURL(this);
	});

	</script>