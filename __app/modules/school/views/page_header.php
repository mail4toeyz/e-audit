
<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
  <meta charset="utf-8" />
  <link rel="shortcut icon" href="<?php echo base_url(); ?>__statics/img/logo.png">
  <meta charset="UTF-8">
    
    <meta property="og:site_name" content="<?php echo site_url(); ?>">
	<meta property="og:title" content="E-Learning Madrasah - Halaman Login " />
	<meta property="og:description" content="E-learning Madrasah adalah program yang diluncurkan oleh Direktorat KSKK Madrasah Kementerian Agama Republik Indonesia untuk menunjang proses pembelajaran di Madrasah kapanpun dan dimanapun, Kita pasti bisa , Kita hebat, Karena Kita Madrasah Hebat Bermartabat" />
	<meta property="og:image" itemprop="image" content="<?php echo base_url(); ?>__statics/img/logo.png">
	<meta property="og:type" content="website" />
	<meta property="og:updated_time" content="<?php echo time(); ?>" />
  <title>
    <?php echo $title; ?>
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
 
  <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/school/fa/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="<?php echo base_url(); ?>__statics/school/css/material-dashboard.min1036.css?v=2.1.1" rel="stylesheet" />
 
  <link href="<?php echo base_url(); ?>__statics/school/demo/demo.css" rel="stylesheet" />
   <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/js/datetimepicker/jquery.datetimepicker.css">

	
	
	<link href="<?php echo base_url(); ?>__statics/js/datepicker/datepicker.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/bootstrap-toastr/toastr.min.css"/>
	<link href="<?php echo base_url(); ?>__statics/js/alertify/css/alertify.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>__statics/js/datatable/button.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>__statics/js/datatable/datatables.min.css" rel="stylesheet" type="text/css"/>
	
	<script src="<?php echo base_url(); ?>__statics/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>__statics/js/proses.js"></script>
	
	<script src="<?php echo base_url(); ?>__statics/js/datatable/datatables.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>__statics/js/datatable/button.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>__statics/js/datatable/jszip.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>__statics/js/datatable/pdf.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>__statics/js/datatable/font.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>__statics/js/datatable/html5.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>__statics/js/datatable/colvis.js" type="text/javascript"></script>
		

  <style>
 .form-controllllll{
	 color:black;
	 
 }
.table {
  width: 100%;
  max-width: 100%;
  margin-bottom: 1rem;
  background-color: transparent;
}

.table th,
.table td {
  padding: 0.75rem;
  vertical-align: top;
  border-top: 1px solid rgba(0, 0, 0, 0.06);
}

.table thead th {
  vertical-align: bottom;
  font-size:12px;
  color:black;
  border-bottom: 2px solid rgba(0, 0, 0, 0.06);
}

.table tbody+tbody {
  border-top: 2px solid rgba(0, 0, 0, 0.06);
}

.table .table {
  background-color: #fafafa;
}

.table-sm th,
.table-sm td {
  padding: 0.3rem;
}

.table-bordered {
  border: 1px solid rgba(0, 0, 0, 0.06);
}

.table-bordered th,
.table-bordered td {
  border: 1px solid rgba(0, 0, 0, 0.06);
}

.table-bordered thead th,
.table-bordered thead td {
  border-bottom-width: 2px;
}

.table-striped tbody tr:nth-of-type(odd) {
  background-color: rgba(0, 0, 0, 0.05);
}

.table-hover tbody tr:hover {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-primary,
.table-primary>th,
.table-primary>td {
  background-color: #c1e2fc;
}

.table-hover .table-primary:hover {
  background-color: #a9d7fb;
}

.table-hover .table-primary:hover>td,
.table-hover .table-primary:hover>th {
  background-color: #a9d7fb;
}

.table-dark,
.table-dark>th,
.table-dark>td {
  background-color: #c6c8ca;
}

.table-hover .table-dark:hover {
  background-color: #b9bbbe;
}

.table-hover .table-dark:hover>td,
.table-hover .table-dark:hover>th {
  background-color: #b9bbbe;
}

.table-active,
.table-active>th,
.table-active>td {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-hover .table-active:hover {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-hover .table-active:hover>td,
.table-hover .table-active:hover>th {
  background-color: rgba(0, 0, 0, 0.075);
}

.table .thead-dark th {
  color: #fafafa;
  background-color: #212529;
  border-color: #32383e;
}

.table .thead-light th {
  color: #495057;
  background-color: #e9ecef;
  border-color: rgba(0, 0, 0, 0.06);
}

.table-dark {
  color: #fafafa;
  background-color: #212529;
}

.table-dark th,
.table-dark td,
.table-dark thead th {
  border-color: #32383e;
}

.table-dark.table-bordered {
  border: 0;
}

.table-dark.table-striped tbody tr:nth-of-type(odd) {
  background-color: rgba(0, 0, 0, 0.05);
}

.table-dark.table-hover tbody tr:hover {
  background-color: rgba(255, 255, 255, 0.075);
}

.dt-button span{
	
	color:black;
}
  
  
  
.modal-open {
  overflow: hidden;
}

.modal {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1050;
  display: none;
  overflow: hidden;
  outline: 0;
}

.modal-open .modal {
  overflow-x: hidden;
  overflow-y: auto;
}

.modal-dialog {
  position: relative;
  width: auto;
  margin: 0.5rem;
  pointer-events: none;
}

.modal.fade .modal-dialog {
  transition: transform 0.3s ease-out;
  transform: translate(0, -25%);
}

.modal.show .modal-dialog {
  transform: translate(0, 0);
}

.modal-dialog-centered {
  display: flex;
  align-items: center;
  min-height: calc(100% - (0.5rem * 2));
}

.modal-content {
  position: relative;
  display: flex;
  flex-direction: column;
  width: 100%;
  pointer-events: auto;
  background-color: #ffffff;
  background-clip: padding-box;
  border: 1px solid rgba(0, 0, 0, 0.2);
  border-radius: 0.3rem;
  box-shadow: 0 0.25rem 0.5rem rgba(0, 0, 0, 0.5);
  outline: 0;
}

.modal-backdrop {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1040;
  background-color: #000000;
}

.modal-backdrop.fade {
  opacity: 0;
}

.modal-backdrop.show {
  opacity: 0.26;
}

.modal-header {
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  padding: 1rem;
  border-bottom: 1px solid #e9ecef;
  border-top-left-radius: 0.3rem;
  border-top-right-radius: 0.3rem;
}

.modal-header .close {
  padding: 1rem;
  margin: -1rem -1rem -1rem auto;
}

.modal-title {
  margin-bottom: 0;
  line-height: 1.5;
}

.modal-body {
  position: relative;
  flex: 1 1 auto;
  padding: 1rem;
}

.modal-footer {
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding: 1rem;
  border-top: 1px solid #e9ecef;
}

.modal-footer> :not(:first-child) {
  margin-left: .25rem;
}

.modal-footer> :not(:last-child) {
  margin-right: .25rem;
}

.modal-scrollbar-measure {
  position: absolute;
  top: -9999px;
  width: 50px;
  height: 50px;
  overflow: scroll;
}

@media (min-width: 576px) {
  .modal-dialog {
    max-width: 500px;
    margin: 1.75rem auto;
  }

  .modal-dialog-centered {
    min-height: calc(100% - (1.75rem * 2));
  }

  .modal-content {
    box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.5);
  }

  .modal-sm {
    max-width: 300px;
  }
}

@media (min-width: 992px) {
  .modal-lg {
    max-width: 800px;
  }
}

.form-group{
	
	color:black;
	
}
  
       .ml-menu {
        list-style: none;
        
        padding-left: 10px; }
         .ml-menu span {
          font-weight: normal;
          font-size: 14px;
          margin: 3px 0 1px 6px; }
         .ml-menu li a {
          padding-left: 55px;
          padding-top: 7px;
          padding-bottom: 7px; }
         .ml-menu li.active a.toggled:not(.menu-toggle) {
          font-weight: 600;
          margin-left: 5px; }
           .ml-menu li.active a.toggled:not(.menu-toggle):before {
            content: '\E315';
            font-family: 'Material Icons';
            position: relative;
            font-size: 21px;
            height: 20px;
            top: -5px;
            right: 0px; }
         .ml-menu li .ml-menu li a {
          padding-left: 80px; }
         .ml-menu li .ml-menu .ml-menu li a {
          padding-left: 95px; }
		  
		  .ml-menu2 {
        list-style: none;
		
        
        padding-left: 10; }
         .ml-menu2 span {
          font-weight: normal;
          font-size: 14px;
          margin: 3px 0 1px 6px; }
         .ml-menu2 li a {
          padding-left: 55px;
          padding-top: 7px;
          padding-bottom: 7px; }
         .ml-menu2 li.active a.toggled:not(.menu-toggle) {
          font-weight: 600;
          margin-left: 5px; }
           .ml-menu2 li.active a.toggled:not(.menu-toggle):before {
            content: '\E315';
            font-family: 'Material Icons';
            position: relative;
            font-size: 21px;
            height: 20px;
            top: -5px;
            right: 0px; }
         .ml-menu2 li .ml-menu li a {
          padding-left: 80px; }
         .ml-menu2 li .ml-menu .ml-menu li a {
          padding-left: 95px; }
		  
.dark-edition {
  background-color: #1a2035;
}

.dark-edition .btn,
.dark-edition .btn.btn-default {
  color: #fff;
  background-color: #9095a2;
  border-color: #9095a2;
  box-shadow: 0 2px 2px 0 rgba(144, 149, 162, 0.14), 0 3px 1px -2px rgba(144, 149, 162, 0.2), 0 1px 5px 0 rgba(144, 149, 162, 0.12);
}

.dark-edition .btn:hover,
.dark-edition .btn.btn-default:hover {
  color: #fff;
  background-color: #888d9b;
  border-color: #707685;
}

.dark-edition .btn:focus,
.dark-edition .btn.focus,
.dark-edition .btn:hover,
.dark-edition .btn.btn-default:focus,
.dark-edition .btn.btn-default.focus,
.dark-edition .btn.btn-default:hover {
  color: #fff;
  background-color: #888d9b;
  border-color: #707685;
}

.dark-edition .btn:active,
.dark-edition .btn.active,
.open>.dark-edition .btn.dropdown-toggle,
.show>.dark-edition .btn.dropdown-toggle,
.dark-edition .btn.btn-default:active,
.dark-edition .btn.btn-default.active,
.open>.dark-edition .btn.btn-default.dropdown-toggle,
.show>.dark-edition .btn.btn-default.dropdown-toggle {
  color: #fff;
  background-color: #888d9b;
  border-color: #707685;
  box-shadow: 0 2px 2px 0 rgba(144, 149, 162, 0.14), 0 3px 1px -2px rgba(144, 149, 162, 0.2), 0 1px 5px 0 rgba(144, 149, 162, 0.12);
}

.dark-edition .btn:active:hover,
.dark-edition .btn:active:focus,
.dark-edition .btn:active.focus,
.dark-edition .btn.active:hover,
.dark-edition .btn.active:focus,
.dark-edition .btn.active.focus,
.open>.dark-edition .btn.dropdown-toggle:hover,
.open>.dark-edition .btn.dropdown-toggle:focus,
.open>.dark-edition .btn.dropdown-toggle.focus,
.show>.dark-edition .btn.dropdown-toggle:hover,
.show>.dark-edition .btn.dropdown-toggle:focus,
.show>.dark-edition .btn.dropdown-toggle.focus,
.dark-edition .btn.btn-default:active:hover,
.dark-edition .btn.btn-default:active:focus,
.dark-edition .btn.btn-default:active.focus,
.dark-edition .btn.btn-default.active:hover,
.dark-edition .btn.btn-default.active:focus,
.dark-edition .btn.btn-default.active.focus,
.open>.dark-edition .btn.btn-default.dropdown-toggle:hover,
.open>.dark-edition .btn.btn-default.dropdown-toggle:focus,
.open>.dark-edition .btn.btn-default.dropdown-toggle.focus,
.show>.dark-edition .btn.btn-default.dropdown-toggle:hover,
.show>.dark-edition .btn.btn-default.dropdown-toggle:focus,
.show>.dark-edition .btn.btn-default.dropdown-toggle.focus {
  color: #fff;
  background-color: #888d9b;
  border-color: #515661;
}

.open>.dark-edition .btn.dropdown-toggle.bmd-btn-icon,
.open>.dark-edition .btn.btn-default.dropdown-toggle.bmd-btn-icon {
  color: inherit;
  background-color: #9095a2;
}

.open>.dark-edition .btn.dropdown-toggle.bmd-btn-icon:hover,
.open>.dark-edition .btn.btn-default.dropdown-toggle.bmd-btn-icon:hover {
  background-color: #888d9b;
}

.dark-edition .btn.disabled:focus,
.dark-edition .btn.disabled.focus,
.dark-edition .btn:disabled:focus,
.dark-edition .btn:disabled.focus,
.dark-edition .btn.btn-default.disabled:focus,
.dark-edition .btn.btn-default.disabled.focus,
.dark-edition .btn.btn-default:disabled:focus,
.dark-edition .btn.btn-default:disabled.focus {
  background-color: #9095a2;
  border-color: #9095a2;
}

.dark-edition .btn.disabled:hover,
.dark-edition .btn:disabled:hover,
.dark-edition .btn.btn-default.disabled:hover,
.dark-edition .btn.btn-default:disabled:hover {
  background-color: #9095a2;
  border-color: #9095a2;
}

.dark-edition .btn:focus,
.dark-edition .btn:active,
.dark-edition .btn:hover,
.dark-edition .btn.btn-default:focus,
.dark-edition .btn.btn-default:active,
.dark-edition .btn.btn-default:hover {
  box-shadow: 0 14px 26px -12px rgba(144, 149, 162, 0.42), 0 4px 23px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(144, 149, 162, 0.2);
}

.dark-edition .btn.btn-link,
.dark-edition .btn.btn-default.btn-link {
  background-color: transparent;
  color: #9095a2;
  box-shadow: none;
}

.dark-edition .btn.btn-link:hover,
.dark-edition .btn.btn-link:focus,
.dark-edition .btn.btn-link:active,
.dark-edition .btn.btn-default.btn-link:hover,
.dark-edition .btn.btn-default.btn-link:focus,
.dark-edition .btn.btn-default.btn-link:active {
  background-color: transparent;
  color: #9095a2;
}

.dark-edition .btn.btn-primary {
  color: #fff;
  background-color: #913f9e;
  border-color: #913f9e;
  box-shadow: 0 2px 2px 0 rgba(145, 63, 158, 0.14), 0 3px 1px -2px rgba(145, 63, 158, 0.2), 0 1px 5px 0 rgba(145, 63, 158, 0.12);
}

.dark-edition .btn.btn-primary:hover {
  color: #fff;
  background-color: #9b43a9;
  border-color: #692e72;
}

.dark-edition .btn.btn-primary:focus,
.dark-edition .btn.btn-primary.focus,
.dark-edition .btn.btn-primary:hover {
  color: #fff;
  background-color: #9b43a9;
  border-color: #692e72;
}

.dark-edition .btn.btn-primary:active,
.dark-edition .btn.btn-primary.active,
.open>.dark-edition .btn.btn-primary.dropdown-toggle,
.show>.dark-edition .btn.btn-primary.dropdown-toggle {
  color: #fff;
  background-color: #9b43a9;
  border-color: #692e72;
  box-shadow: 0 2px 2px 0 rgba(145, 63, 158, 0.14), 0 3px 1px -2px rgba(145, 63, 158, 0.2), 0 1px 5px 0 rgba(145, 63, 158, 0.12);
}

.dark-edition .btn.btn-primary:active:hover,
.dark-edition .btn.btn-primary:active:focus,
.dark-edition .btn.btn-primary:active.focus,
.dark-edition .btn.btn-primary.active:hover,
.dark-edition .btn.btn-primary.active:focus,
.dark-edition .btn.btn-primary.active.focus,
.open>.dark-edition .btn.btn-primary.dropdown-toggle:hover,
.open>.dark-edition .btn.btn-primary.dropdown-toggle:focus,
.open>.dark-edition .btn.btn-primary.dropdown-toggle.focus,
.show>.dark-edition .btn.btn-primary.dropdown-toggle:hover,
.show>.dark-edition .btn.btn-primary.dropdown-toggle:focus,
.show>.dark-edition .btn.btn-primary.dropdown-toggle.focus {
  color: #fff;
  background-color: #9b43a9;
  border-color: #3d1b43;
}

.open>.dark-edition .btn.btn-primary.dropdown-toggle.bmd-btn-icon {
  color: inherit;
  background-color: #913f9e;
}

.open>.dark-edition .btn.btn-primary.dropdown-toggle.bmd-btn-icon:hover {
  background-color: #9b43a9;
}

.dark-edition .btn.btn-primary.disabled:focus,
.dark-edition .btn.btn-primary.disabled.focus,
.dark-edition .btn.btn-primary:disabled:focus,
.dark-edition .btn.btn-primary:disabled.focus {
  background-color: #913f9e;
  border-color: #913f9e;
}

.dark-edition .btn.btn-primary.disabled:hover,
.dark-edition .btn.btn-primary:disabled:hover {
  background-color: #913f9e;
  border-color: #913f9e;
}

.dark-edition .btn.btn-primary:focus,
.dark-edition .btn.btn-primary:active,
.dark-edition .btn.btn-primary:hover {
  box-shadow: 0 14px 26px -12px rgba(145, 63, 158, 0.42), 0 4px 23px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(145, 63, 158, 0.2);
}

.dark-edition .btn.btn-primary.btn-link {
  background-color: transparent;
  color: #913f9e;
  box-shadow: none;
}

.dark-edition .btn.btn-primary.btn-link:hover,
.dark-edition .btn.btn-primary.btn-link:focus,
.dark-edition .btn.btn-primary.btn-link:active {
  background-color: transparent;
  color: #913f9e;
}

.dark-edition .btn.btn-warning {
  color: #fff;
  background-color: #f5700c;
  border-color: #f5700c;
  box-shadow: 0 2px 2px 0 rgba(245, 112, 12, 0.14), 0 3px 1px -2px rgba(245, 112, 12, 0.2), 0 1px 5px 0 rgba(245, 112, 12, 0.12);
}

.dark-edition .btn.btn-warning:hover {
  color: #fff;
  background-color: #e8690a;
  border-color: #bc5508;
}

.dark-edition .btn.btn-warning:focus,
.dark-edition .btn.btn-warning.focus,
.dark-edition .btn.btn-warning:hover {
  color: #fff;
  background-color: #e8690a;
  border-color: #bc5508;
}

.dark-edition .btn.btn-warning:active,
.dark-edition .btn.btn-warning.active,
.open>.dark-edition .btn.btn-warning.dropdown-toggle,
.show>.dark-edition .btn.btn-warning.dropdown-toggle {
  color: #fff;
  background-color: #e8690a;
  border-color: #bc5508;
  box-shadow: 0 2px 2px 0 rgba(245, 112, 12, 0.14), 0 3px 1px -2px rgba(245, 112, 12, 0.2), 0 1px 5px 0 rgba(245, 112, 12, 0.12);
}

.dark-edition .btn.btn-warning:active:hover,
.dark-edition .btn.btn-warning:active:focus,
.dark-edition .btn.btn-warning:active.focus,
.dark-edition .btn.btn-warning.active:hover,
.dark-edition .btn.btn-warning.active:focus,
.dark-edition .btn.btn-warning.active.focus,
.open>.dark-edition .btn.btn-warning.dropdown-toggle:hover,
.open>.dark-edition .btn.btn-warning.dropdown-toggle:focus,
.open>.dark-edition .btn.btn-warning.dropdown-toggle.focus,
.show>.dark-edition .btn.btn-warning.dropdown-toggle:hover,
.show>.dark-edition .btn.btn-warning.dropdown-toggle:focus,
.show>.dark-edition .btn.btn-warning.dropdown-toggle.focus {
  color: #fff;
  background-color: #e8690a;
  border-color: #7c3805;
}

.open>.dark-edition .btn.btn-warning.dropdown-toggle.bmd-btn-icon {
  color: inherit;
  background-color: #f5700c;
}

.open>.dark-edition .btn.btn-warning.dropdown-toggle.bmd-btn-icon:hover {
  background-color: #e8690a;
}

.dark-edition .btn.btn-warning.disabled:focus,
.dark-edition .btn.btn-warning.disabled.focus,
.dark-edition .btn.btn-warning:disabled:focus,
.dark-edition .btn.btn-warning:disabled.focus {
  background-color: #f5700c;
  border-color: #f5700c;
}

.dark-edition .btn.btn-warning.disabled:hover,
.dark-edition .btn.btn-warning:disabled:hover {
  background-color: #f5700c;
  border-color: #f5700c;
}

.dark-edition .btn.btn-warning:focus,
.dark-edition .btn.btn-warning:active,
.dark-edition .btn.btn-warning:hover {
  box-shadow: 0 14px 26px -12px rgba(245, 112, 12, 0.42), 0 4px 23px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(245, 112, 12, 0.2);
}

.dark-edition .btn.btn-warning.btn-link {
  background-color: transparent;
  color: #f5700c;
  box-shadow: none;
}

.dark-edition .btn.btn-warning.btn-link:hover,
.dark-edition .btn.btn-warning.btn-link:focus,
.dark-edition .btn.btn-warning.btn-link:active {
  background-color: transparent;
  color: #f5700c;
}

.dark-edition .btn.btn-success {
  color: #fff;
  background-color: #288c6c;
  border-color: #288c6c;
  box-shadow: 0 2px 2px 0 rgba(40, 140, 108, 0.14), 0 3px 1px -2px rgba(40, 140, 108, 0.2), 0 1px 5px 0 rgba(40, 140, 108, 0.12);
}

.dark-edition .btn.btn-success:hover {
  color: #fff;
  background-color: #2b9875;
  border-color: #1a5c47;
}

.dark-edition .btn.btn-success:focus,
.dark-edition .btn.btn-success.focus,
.dark-edition .btn.btn-success:hover {
  color: #fff;
  background-color: #2b9875;
  border-color: #1a5c47;
}

.dark-edition .btn.btn-success:active,
.dark-edition .btn.btn-success.active,
.open>.dark-edition .btn.btn-success.dropdown-toggle,
.show>.dark-edition .btn.btn-success.dropdown-toggle {
  color: #fff;
  background-color: #2b9875;
  border-color: #1a5c47;
  box-shadow: 0 2px 2px 0 rgba(40, 140, 108, 0.14), 0 3px 1px -2px rgba(40, 140, 108, 0.2), 0 1px 5px 0 rgba(40, 140, 108, 0.12);
}

.dark-edition .btn.btn-success:active:hover,
.dark-edition .btn.btn-success:active:focus,
.dark-edition .btn.btn-success:active.focus,
.dark-edition .btn.btn-success.active:hover,
.dark-edition .btn.btn-success.active:focus,
.dark-edition .btn.btn-success.active.focus,
.open>.dark-edition .btn.btn-success.dropdown-toggle:hover,
.open>.dark-edition .btn.btn-success.dropdown-toggle:focus,
.open>.dark-edition .btn.btn-success.dropdown-toggle.focus,
.show>.dark-edition .btn.btn-success.dropdown-toggle:hover,
.show>.dark-edition .btn.btn-success.dropdown-toggle:focus,
.show>.dark-edition .btn.btn-success.dropdown-toggle.focus {
  color: #fff;
  background-color: #2b9875;
  border-color: #0c2920;
}

.open>.dark-edition .btn.btn-success.dropdown-toggle.bmd-btn-icon {
  color: inherit;
  background-color: #288c6c;
}

.open>.dark-edition .btn.btn-success.dropdown-toggle.bmd-btn-icon:hover {
  background-color: #2b9875;
}

.dark-edition .btn.btn-success.disabled:focus,
.dark-edition .btn.btn-success.disabled.focus,
.dark-edition .btn.btn-success:disabled:focus,
.dark-edition .btn.btn-success:disabled.focus {
  background-color: #288c6c;
  border-color: #288c6c;
}

.dark-edition .btn.btn-success.disabled:hover,
.dark-edition .btn.btn-success:disabled:hover {
  background-color: #288c6c;
  border-color: #288c6c;
}

.dark-edition .btn.btn-success:focus,
.dark-edition .btn.btn-success:active,
.dark-edition .btn.btn-success:hover {
  box-shadow: 0 14px 26px -12px rgba(40, 140, 108, 0.42), 0 4px 23px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(40, 140, 108, 0.2);
}

.dark-edition .btn.btn-success.btn-link {
  background-color: transparent;
  color: #288c6c;
  box-shadow: none;
}

.dark-edition .btn.btn-success.btn-link:hover,
.dark-edition .btn.btn-success.btn-link:focus,
.dark-edition .btn.btn-success.btn-link:active {
  background-color: transparent;
  color: #288c6c;
}

.dark-edition .btn.btn-danger {
  color: #fff;
  background-color: #d22824;
  border-color: #f44336;
  box-shadow: 0 2px 2px 0 rgba(210, 40, 36, 0.14), 0 3px 1px -2px rgba(210, 40, 36, 0.2), 0 1px 5px 0 rgba(210, 40, 36, 0.12);
}

.dark-edition .btn.btn-danger:hover {
  color: #fff;
  background-color: #db2f2b;
  border-color: #e11b0c;
}

.dark-edition .btn.btn-danger:focus,
.dark-edition .btn.btn-danger.focus,
.dark-edition .btn.btn-danger:hover {
  color: #fff;
  background-color: #db2f2b;
  border-color: #e11b0c;
}

.dark-edition .btn.btn-danger:active,
.dark-edition .btn.btn-danger.active,
.open>.dark-edition .btn.btn-danger.dropdown-toggle,
.show>.dark-edition .btn.btn-danger.dropdown-toggle {
  color: #fff;
  background-color: #db2f2b;
  border-color: #e11b0c;
  box-shadow: 0 2px 2px 0 rgba(210, 40, 36, 0.14), 0 3px 1px -2px rgba(210, 40, 36, 0.2), 0 1px 5px 0 rgba(210, 40, 36, 0.12);
}

.dark-edition .btn.btn-danger:active:hover,
.dark-edition .btn.btn-danger:active:focus,
.dark-edition .btn.btn-danger:active.focus,
.dark-edition .btn.btn-danger.active:hover,
.dark-edition .btn.btn-danger.active:focus,
.dark-edition .btn.btn-danger.active.focus,
.open>.dark-edition .btn.btn-danger.dropdown-toggle:hover,
.open>.dark-edition .btn.btn-danger.dropdown-toggle:focus,
.open>.dark-edition .btn.btn-danger.dropdown-toggle.focus,
.show>.dark-edition .btn.btn-danger.dropdown-toggle:hover,
.show>.dark-edition .btn.btn-danger.dropdown-toggle:focus,
.show>.dark-edition .btn.btn-danger.dropdown-toggle.focus {
  color: #fff;
  background-color: #db2f2b;
  border-color: #a21309;
}

.open>.dark-edition .btn.btn-danger.dropdown-toggle.bmd-btn-icon {
  color: inherit;
  background-color: #d22824;
}

.open>.dark-edition .btn.btn-danger.dropdown-toggle.bmd-btn-icon:hover {
  background-color: #db2f2b;
}

.dark-edition .btn.btn-danger.disabled:focus,
.dark-edition .btn.btn-danger.disabled.focus,
.dark-edition .btn.btn-danger:disabled:focus,
.dark-edition .btn.btn-danger:disabled.focus {
  background-color: #d22824;
  border-color: #f44336;
}

.dark-edition .btn.btn-danger.disabled:hover,
.dark-edition .btn.btn-danger:disabled:hover {
  background-color: #d22824;
  border-color: #f44336;
}

.dark-edition .btn.btn-danger:focus,
.dark-edition .btn.btn-danger:active,
.dark-edition .btn.btn-danger:hover {
  box-shadow: 0 14px 26px -12px rgba(210, 40, 36, 0.42), 0 4px 23px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(210, 40, 36, 0.2);
}

.dark-edition .btn.btn-danger.btn-link {
  background-color: transparent;
  color: #d22824;
  box-shadow: none;
}

.dark-edition .btn.btn-danger.btn-link:hover,
.dark-edition .btn.btn-danger.btn-link:focus,
.dark-edition .btn.btn-danger.btn-link:active {
  background-color: transparent;
  color: #d22824;
}

.dark-edition .btn.btn-info {
  color: #fff;
  background-color: #029eb1;
  border-color: #029eb1;
  box-shadow: 0 2px 2px 0 rgba(2, 158, 177, 0.14), 0 3px 1px -2px rgba(2, 158, 177, 0.2), 0 1px 5px 0 rgba(2, 158, 177, 0.12);
}

.dark-edition .btn.btn-info:hover {
  color: #fff;
  background-color: #0290a2;
  border-color: #016874;
}

.dark-edition .btn.btn-info:focus,
.dark-edition .btn.btn-info.focus,
.dark-edition .btn.btn-info:hover {
  color: #fff;
  background-color: #0290a2;
  border-color: #016874;
}

.dark-edition .btn.btn-info:active,
.dark-edition .btn.btn-info.active,
.open>.dark-edition .btn.btn-info.dropdown-toggle,
.show>.dark-edition .btn.btn-info.dropdown-toggle {
  color: #fff;
  background-color: #0290a2;
  border-color: #016874;
  box-shadow: 0 2px 2px 0 rgba(2, 158, 177, 0.14), 0 3px 1px -2px rgba(2, 158, 177, 0.2), 0 1px 5px 0 rgba(2, 158, 177, 0.12);
}

.dark-edition .btn.btn-info:active:hover,
.dark-edition .btn.btn-info:active:focus,
.dark-edition .btn.btn-info:active.focus,
.dark-edition .btn.btn-info.active:hover,
.dark-edition .btn.btn-info.active:focus,
.dark-edition .btn.btn-info.active.focus,
.open>.dark-edition .btn.btn-info.dropdown-toggle:hover,
.open>.dark-edition .btn.btn-info.dropdown-toggle:focus,
.open>.dark-edition .btn.btn-info.dropdown-toggle.focus,
.show>.dark-edition .btn.btn-info.dropdown-toggle:hover,
.show>.dark-edition .btn.btn-info.dropdown-toggle:focus,
.show>.dark-edition .btn.btn-info.dropdown-toggle.focus {
  color: #fff;
  background-color: #0290a2;
  border-color: #012d33;
}

.open>.dark-edition .btn.btn-info.dropdown-toggle.bmd-btn-icon {
  color: inherit;
  background-color: #029eb1;
}

.open>.dark-edition .btn.btn-info.dropdown-toggle.bmd-btn-icon:hover {
  background-color: #0290a2;
}

.dark-edition .btn.btn-info.disabled:focus,
.dark-edition .btn.btn-info.disabled.focus,
.dark-edition .btn.btn-info:disabled:focus,
.dark-edition .btn.btn-info:disabled.focus {
  background-color: #029eb1;
  border-color: #029eb1;
}

.dark-edition .btn.btn-info.disabled:hover,
.dark-edition .btn.btn-info:disabled:hover {
  background-color: #029eb1;
  border-color: #029eb1;
}

.dark-edition .btn.btn-info:focus,
.dark-edition .btn.btn-info:active,
.dark-edition .btn.btn-info:hover {
  box-shadow: 0 14px 26px -12px rgba(2, 158, 177, 0.42), 0 4px 23px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(2, 158, 177, 0.2);
}

.dark-edition .btn.btn-info.btn-link {
  background-color: transparent;
  color: #029eb1;
  box-shadow: none;
}

.dark-edition .btn.btn-info.btn-link:hover,
.dark-edition .btn.btn-info.btn-link:focus,
.dark-edition .btn.btn-info.btn-link:active {
  background-color: transparent;
  color: #029eb1;
}

.dark-edition .btn.btn-white {
  color: #9095a2;
  background-color: #fff;
  border-color: #fff;
  box-shadow: 0 2px 2px 0 rgba(255, 255, 255, 0.14), 0 3px 1px -2px rgba(255, 255, 255, 0.2), 0 1px 5px 0 rgba(255, 255, 255, 0.12);
}

.dark-edition .btn.btn-white:hover {
  color: #9095a2;
  background-color: #f7f7f7;
  border-color: #e0e0e0;
}

.dark-edition .btn.btn-white:focus,
.dark-edition .btn.btn-white.focus,
.dark-edition .btn.btn-white:hover {
  color: #9095a2;
  background-color: #f7f7f7;
  border-color: #e0e0e0;
}

.dark-edition .btn.btn-white:active,
.dark-edition .btn.btn-white.active,
.open>.dark-edition .btn.btn-white.dropdown-toggle,
.show>.dark-edition .btn.btn-white.dropdown-toggle {
  color: #9095a2;
  background-color: #f7f7f7;
  border-color: #e0e0e0;
  box-shadow: 0 2px 2px 0 rgba(255, 255, 255, 0.14), 0 3px 1px -2px rgba(255, 255, 255, 0.2), 0 1px 5px 0 rgba(255, 255, 255, 0.12);
}

.dark-edition .btn.btn-white:active:hover,
.dark-edition .btn.btn-white:active:focus,
.dark-edition .btn.btn-white:active.focus,
.dark-edition .btn.btn-white.active:hover,
.dark-edition .btn.btn-white.active:focus,
.dark-edition .btn.btn-white.active.focus,
.open>.dark-edition .btn.btn-white.dropdown-toggle:hover,
.open>.dark-edition .btn.btn-white.dropdown-toggle:focus,
.open>.dark-edition .btn.btn-white.dropdown-toggle.focus,
.show>.dark-edition .btn.btn-white.dropdown-toggle:hover,
.show>.dark-edition .btn.btn-white.dropdown-toggle:focus,
.show>.dark-edition .btn.btn-white.dropdown-toggle.focus {
  color: #9095a2;
  background-color: #f7f7f7;
  border-color: #bfbfbf;
}

.open>.dark-edition .btn.btn-white.dropdown-toggle.bmd-btn-icon {
  color: inherit;
  background-color: #fff;
}

.open>.dark-edition .btn.btn-white.dropdown-toggle.bmd-btn-icon:hover {
  background-color: #f7f7f7;
}

.dark-edition .btn.btn-white.disabled:focus,
.dark-edition .btn.btn-white.disabled.focus,
.dark-edition .btn.btn-white:disabled:focus,
.dark-edition .btn.btn-white:disabled.focus {
  background-color: #fff;
  border-color: #fff;
}

.dark-edition .btn.btn-white.disabled:hover,
.dark-edition .btn.btn-white:disabled:hover {
  background-color: #fff;
  border-color: #fff;
}

.dark-edition .btn.btn-white:focus,
.dark-edition .btn.btn-white:active,
.dark-edition .btn.btn-white:hover {
  box-shadow: 0 14px 26px -12px rgba(255, 255, 255, 0.42), 0 4px 23px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(255, 255, 255, 0.2);
}

.dark-edition .btn.btn-white.btn-link {
  background-color: transparent;
  color: #fff;
  box-shadow: none;
}

.dark-edition .btn.btn-white.btn-link:hover,
.dark-edition .btn.btn-white.btn-link:focus,
.dark-edition .btn.btn-white.btn-link:active {
  background-color: transparent;
  color: #fff;
}

.dark-edition .btn.btn-facebook {
  color: #ffffff;
  background-color: #3b5998;
  border-color: #3b5998;
  box-shadow: 0 2px 2px 0 rgba(59, 89, 152, 0.14), 0 3px 1px -2px rgba(59, 89, 152, 0.2), 0 1px 5px 0 rgba(59, 89, 152, 0.12);
}

.dark-edition .btn.btn-facebook:hover {
  color: #ffffff;
  background-color: #37538d;
  border-color: #2a3f6c;
}

.dark-edition .btn.btn-facebook:focus,
.dark-edition .btn.btn-facebook.focus,
.dark-edition .btn.btn-facebook:hover {
  color: #ffffff;
  background-color: #37538d;
  border-color: #2a3f6c;
}

.dark-edition .btn.btn-facebook:active,
.dark-edition .btn.btn-facebook.active,
.open>.dark-edition .btn.btn-facebook.dropdown-toggle,
.show>.dark-edition .btn.btn-facebook.dropdown-toggle {
  color: #ffffff;
  background-color: #37538d;
  border-color: #2a3f6c;
  box-shadow: 0 2px 2px 0 rgba(59, 89, 152, 0.14), 0 3px 1px -2px rgba(59, 89, 152, 0.2), 0 1px 5px 0 rgba(59, 89, 152, 0.12);
}

.dark-edition .btn.btn-facebook:active:hover,
.dark-edition .btn.btn-facebook:active:focus,
.dark-edition .btn.btn-facebook:active.focus,
.dark-edition .btn.btn-facebook.active:hover,
.dark-edition .btn.btn-facebook.active:focus,
.dark-edition .btn.btn-facebook.active.focus,
.open>.dark-edition .btn.btn-facebook.dropdown-toggle:hover,
.open>.dark-edition .btn.btn-facebook.dropdown-toggle:focus,
.open>.dark-edition .btn.btn-facebook.dropdown-toggle.focus,
.show>.dark-edition .btn.btn-facebook.dropdown-toggle:hover,
.show>.dark-edition .btn.btn-facebook.dropdown-toggle:focus,
.show>.dark-edition .btn.btn-facebook.dropdown-toggle.focus {
  color: #ffffff;
  background-color: #37538d;
  border-color: #17233c;
}

.open>.dark-edition .btn.btn-facebook.dropdown-toggle.bmd-btn-icon {
  color: inherit;
  background-color: #3b5998;
}

.open>.dark-edition .btn.btn-facebook.dropdown-toggle.bmd-btn-icon:hover {
  background-color: #37538d;
}

.dark-edition .btn.btn-facebook.disabled:focus,
.dark-edition .btn.btn-facebook.disabled.focus,
.dark-edition .btn.btn-facebook:disabled:focus,
.dark-edition .btn.btn-facebook:disabled.focus {
  background-color: #3b5998;
  border-color: #3b5998;
}

.dark-edition .btn.btn-facebook.disabled:hover,
.dark-edition .btn.btn-facebook:disabled:hover {
  background-color: #3b5998;
  border-color: #3b5998;
}

.dark-edition .btn.btn-facebook:focus,
.dark-edition .btn.btn-facebook:active,
.dark-edition .btn.btn-facebook:hover {
  box-shadow: 0 14px 26px -12px rgba(59, 89, 152, 0.42), 0 4px 23px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(59, 89, 152, 0.2);
}

.dark-edition .btn.btn-facebook.btn-link {
  background-color: transparent;
  color: #3b5998;
  box-shadow: none;
}

.dark-edition .btn.btn-facebook.btn-link:hover,
.dark-edition .btn.btn-facebook.btn-link:focus,
.dark-edition .btn.btn-facebook.btn-link:active {
  background-color: transparent;
  color: #3b5998;
}

.dark-edition .btn.btn-twitter {
  color: #ffffff;
  background-color: #55acee;
  border-color: #55acee;
  box-shadow: 0 2px 2px 0 rgba(85, 172, 238, 0.14), 0 3px 1px -2px rgba(85, 172, 238, 0.2), 0 1px 5px 0 rgba(85, 172, 238, 0.12);
}

.dark-edition .btn.btn-twitter:hover {
  color: #ffffff;
  background-color: #47a5ed;
  border-color: #1d91e8;
}

.dark-edition .btn.btn-twitter:focus,
.dark-edition .btn.btn-twitter.focus,
.dark-edition .btn.btn-twitter:hover {
  color: #ffffff;
  background-color: #47a5ed;
  border-color: #1d91e8;
}

.dark-edition .btn.btn-twitter:active,
.dark-edition .btn.btn-twitter.active,
.open>.dark-edition .btn.btn-twitter.dropdown-toggle,
.show>.dark-edition .btn.btn-twitter.dropdown-toggle {
  color: #ffffff;
  background-color: #47a5ed;
  border-color: #1d91e8;
  box-shadow: 0 2px 2px 0 rgba(85, 172, 238, 0.14), 0 3px 1px -2px rgba(85, 172, 238, 0.2), 0 1px 5px 0 rgba(85, 172, 238, 0.12);
}

.dark-edition .btn.btn-twitter:active:hover,
.dark-edition .btn.btn-twitter:active:focus,
.dark-edition .btn.btn-twitter:active.focus,
.dark-edition .btn.btn-twitter.active:hover,
.dark-edition .btn.btn-twitter.active:focus,
.dark-edition .btn.btn-twitter.active.focus,
.open>.dark-edition .btn.btn-twitter.dropdown-toggle:hover,
.open>.dark-edition .btn.btn-twitter.dropdown-toggle:focus,
.open>.dark-edition .btn.btn-twitter.dropdown-toggle.focus,
.show>.dark-edition .btn.btn-twitter.dropdown-toggle:hover,
.show>.dark-edition .btn.btn-twitter.dropdown-toggle:focus,
.show>.dark-edition .btn.btn-twitter.dropdown-toggle.focus {
  color: #ffffff;
  background-color: #47a5ed;
  border-color: #126db2;
}

.open>.dark-edition .btn.btn-twitter.dropdown-toggle.bmd-btn-icon {
  color: inherit;
  background-color: #55acee;
}

.open>.dark-edition .btn.btn-twitter.dropdown-toggle.bmd-btn-icon:hover {
  background-color: #47a5ed;
}

.dark-edition .btn.btn-twitter.disabled:focus,
.dark-edition .btn.btn-twitter.disabled.focus,
.dark-edition .btn.btn-twitter:disabled:focus,
.dark-edition .btn.btn-twitter:disabled.focus {
  background-color: #55acee;
  border-color: #55acee;
}

.dark-edition .btn.btn-twitter.disabled:hover,
.dark-edition .btn.btn-twitter:disabled:hover {
  background-color: #55acee;
  border-color: #55acee;
}

.dark-edition .btn.btn-twitter:focus,
.dark-edition .btn.btn-twitter:active,
.dark-edition .btn.btn-twitter:hover {
  box-shadow: 0 14px 26px -12px rgba(85, 172, 238, 0.42), 0 4px 23px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(85, 172, 238, 0.2);
}

.dark-edition .btn.btn-twitter.btn-link {
  background-color: transparent;
  color: #55acee;
  box-shadow: none;
}

.dark-edition .btn.btn-twitter.btn-link:hover,
.dark-edition .btn.btn-twitter.btn-link:focus,
.dark-edition .btn.btn-twitter.btn-link:active {
  background-color: transparent;
  color: #55acee;
}

.dark-edition .sidebar[data-color="purple"] li.active>a {
  background: linear-gradient(60deg, #7b1fa2, #913f9e);
  box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(156, 39, 176, 0.4);
}

.dark-edition .sidebar[data-color="azure"] li.active>a {
  background: linear-gradient(60deg, #029eb1, #25b1c3);
  box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(0, 188, 212, 0.4);
}

.dark-edition .sidebar[data-color="green"] li.active>a {
  background: linear-gradient(60deg, #288c6c, #4ea752);
  box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(76, 175, 80, 0.4);
}

.dark-edition .sidebar[data-color="orange"] li.active>a {
  background: linear-gradient(60deg, #f5700c, #ff9800);
  box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(255, 152, 0, 0.4);
}

.dark-edition .sidebar[data-color="danger"] li.active>a {
  background: linear-gradient(60deg, #d22824, #da3a36);
  box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(244, 67, 54, 0.4);
}

.dark-edition .sidebar a {
  -webkit-transition: all 150ms ease-in, color, 0ms;
  -moz-transition: all 150ms ease-in, color, 0ms;
  -o-transition: all 150ms ease-in, color, 0ms;
  -ms-transition: all 150ms ease-in, color, 0ms;
  transition: all 150ms ease-in, color, 0ms;
}

.dark-edition .sidebar .sidebar-background:after,
.dark-edition .sidebar {
  background: #1f283e;
  opacity: .94;
}

.dark-edition .sidebar[data-background-color="black"] .nav li:not(.active) a,
.dark-edition .sidebar[data-background-color="black"] .nav li:not(.active) .dropdown-menu a {
  color: #a9afbbd1;
}

.dark-edition .sidebar[data-background-color="black"] .nav li:not(.active) i {
  color: #a9afbbd1;
}

.dark-edition .sidebar[data-background-color="black"] .nav li:not(.active):not(.active):hover>a,
.dark-edition .sidebar[data-background-color="black"] .nav li:not(.active) .dropdown-menu a:hover,
.dark-edition .sidebar[data-background-color="black"] .nav li:not(.active) .dropdown-menu a:focus,
.dark-edition .sidebar[data-background-color="black"] .nav li:not(.active).active>[data-toggle="collapse"] {
  background-color: rgba(200, 200, 200, 0.05);
  color: rgba(255, 255, 255, 0.8);
}

.dark-edition .sidebar .logo .simple-text {
  color: #a9afbbd1;
  font-weight: 300;
}

.dark-edition .sidebar .logo:after {
  background-color: rgba(180, 180, 180, 0.1);
}

.dark-edition .navbar:not([class*="bg-"]) .navbar-brand,
.dark-edition .navbar:not([class*="bg-"]) .collapse .navbar-nav .nav-item .nav-link {
  color: #a9afbbd1;
}

.dark-edition .navbar[class*="bg-"] .form-controllllll::-moz-placeholder {
  color: #000;
}

.dark-edition .navbar[class*="bg-"] .form-controllllll:-ms-input-placeholder {
  color: #000;
}

.dark-edition .navbar[class*="bg-"] .form-controllllll::-webkit-input-placeholder {
  color: #000;
}

.dark-edition .navbar .notification {
  background: linear-gradient(60deg, #7b1fa2, #913f9e);
  border: 0;
  height: 18px;
  min-width: 18px;
  right: 12px;
}

.dark-edition .navbar .navbar-toggler .icon-bar {
  background: #a9afbbd1 !important;
}

.dark-edition .navbar.bg-primary {
  background-color: #913f9e !important;
}

.dark-edition .navbar.bg-info {
  background-color: #029eb1 !important;
}

.dark-edition .navbar.bg-warning {
  background-color: #f5700c !important;
}

.dark-edition .navbar.bg-danger {
  background-color: #d22824 !important;
}

.dark-edition .navbar.bg-success {
  background-color: #288c6c !important;
}

.dark-edition .footer {
  border-top: 0;
}

.dark-edition .footer ul li a,
.dark-edition .footer .copyright {
  color: #a9afbbd1;
}

.dark-edition .footer .copyright a {
  color: #fff;
}

.dark-edition .footer .copyright i {
  animation: heartbeat 1s infinite;
  -webkit-background-clip: text;
  background-clip: text;
  background-image: linear-gradient(60deg, #7b1fa2, #913f9e);
  color: transparent;
}

.dark-edition .card {
  background: #202940;
}

.dark-edition .card .card-header.card-header-text .card-title,
.dark-edition .card .card-header.card-header-icon .card-title {
  color: #606477;
}

.dark-edition .card .card-header.card-header-tabs .nav-tabs-title {
  color: rgba(255, 255, 255, 0.8);
}

.dark-edition .card .card-body {
  color: #8b92a9;
}

.dark-edition .card .card-body .card-category+.card-title {
  color: #fff;
}

.dark-edition .card .card-body .card-category,
.dark-edition .card .card-body .card-title {
  color: #8b92a9;
}

.dark-edition .card .card-body .card-description {
  color: #606477;
}

.dark-edition .card .card-body+.card-footer .stats,
.dark-edition .card .card-footer .stats {
  color: #a9afbbd1;
}

.dark-edition .card .card-footer .stats a {
  -webkit-background-clip: text;
  background-clip: text;
  background-image: linear-gradient(60deg, #7b1fa2, #913f9e);
  color: transparent;
}

.dark-edition .card .card-header-warning .card-icon,
.dark-edition .card .card-header-warning .card-text,
.dark-edition .card .card-header-warning:not(.card-header-icon):not(.card-header-text),
.dark-edition .card.bg-warning,
.dark-edition .card.card-rotate.bg-warning .front,
.dark-edition .card.card-rotate.bg-warning .back {
  background: linear-gradient(60deg, #f5700c, #ff9800);
}

.dark-edition .card .card-header-success .card-icon,
.dark-edition .card .card-header-success .card-text,
.dark-edition .card .card-header-success:not(.card-header-icon):not(.card-header-text),
.dark-edition .card.bg-success,
.dark-edition .card.card-rotate.bg-success .front,
.dark-edition .card.card-rotate.bg-success .back {
  background: linear-gradient(60deg, #288c6c, #4ea752);
}

.dark-edition .card .card-header-danger .card-icon,
.dark-edition .card .card-header-danger .card-text,
.dark-edition .card .card-header-danger:not(.card-header-icon):not(.card-header-text),
.dark-edition .card.bg-danger,
.dark-edition .card.card-rotate.bg-danger .front,
.dark-edition .card.card-rotate.bg-danger .back {
  background: linear-gradient(60deg, #d22824, #da3a36);
}

.dark-edition .card .card-header-info .card-icon,
.dark-edition .card .card-header-info .card-text,
.dark-edition .card .card-header-info:not(.card-header-icon):not(.card-header-text),
.dark-edition .card.bg-info,
.dark-edition .card.card-rotate.bg-info .front,
.dark-edition .card.card-rotate.bg-info .back {
  background: linear-gradient(60deg, #029eb1, #25b1c3);
}

.dark-edition .card .card-header-primary .card-icon,
.dark-edition .card .card-header-primary .card-text,
.dark-edition .card .card-header-primary:not(.card-header-icon):not(.card-header-text),
.dark-edition .card.bg-primary,
.dark-edition .card.card-rotate.bg-primary .front,
.dark-edition .card.card-rotate.bg-primary .back {
  background: linear-gradient(60deg, #7b1fa2, #913f9e);
}

.dark-edition .primary-link {
  background-image: linear-gradient(60deg, #7b1fa2, #913f9e) !important;
}

.dark-edition .info-link {
  background-image: linear-gradient(60deg, #029eb1, #25b1c3) !important;
}

.dark-edition .warning-link {
  background-image: linear-gradient(60deg, #f5700c, #ff9800) !important;
}

.dark-edition .success-link {
  background-image: linear-gradient(60deg, #288c6c, #4ea752) !important;
}

.dark-edition .danger-link {
  background-image: linear-gradient(60deg, #d22824, #da3a36) !important;
}

.dark-edition .card-stats .card-header .card-category:not([class*="text-"]) {
  color: #8b92a9;
}

.dark-edition .card-stats .card-header+.card-footer {
  border-top: 1px solid rgba(180, 180, 180, 0.1);
}

.dark-edition .card-plain {
  background: transparent;
}

.dark-edition .card-chart .card-body .card-category {
  color: #606477;
}

.dark-edition .card-chart .card-body .card-category span {
  -webkit-background-clip: text;
  background-clip: text;
  background-image: linear-gradient(60deg, #288c6c, #4ea752);
  color: transparent;
}

.dark-edition .card-chart .card-footer {
  border-top: 1px solid rgba(180, 180, 180, 0.1);
}

.dark-edition .table>thead>tr>th,
.dark-edition .table>tbody>tr>th,
.dark-edition .table>tfoot>tr>th,
.dark-edition .table>thead>tr>td,
.dark-edition .table>tbody>tr>td,
.dark-edition .table>tfoot>tr>td {
  color: #8b92a9;
  border-color: rgba(180, 180, 180, 0.1);
}

.dark-edition .table-hover tbody tr:hover {
  background-color: rgba(0, 0, 0, 0.075);
}

.dark-edition .form-check .form-check-sign .check {
  border-color: rgba(96, 100, 119, 0.5);
}

.dark-edition .form-check .form-check-sign:before {
  left: -1px;
  top: -1px;
}

.dark-edition .form-check .form-check-input[disabled]~.form-check-sign .check {
  border-color: rgba(96, 100, 119, 0.5);
}

.dark-edition .form-check .form-check-input:checked+.form-check-sign .check {
  border-color: #40475a;
  background: linear-gradient(60deg, #fff, #ca9dd2);
}

.dark-edition .form-check .form-check-input:checked+.form-check-sign .check:before {
  color: #202940;
}

.dark-edition .form-check.form-check-radio .form-check-input~.circle {
  border-color: rgba(96, 100, 119, 0.5);
}

.dark-edition .form-check.form-check-radio .form-check-label .circle .check {
  background-color: #fff;
}

.dark-edition .nav-tabs .nav-item .nav-link {
  position: relative;
}

.dark-edition .nav-tabs .nav-item .nav-link.active {
  background-color: rgba(255, 255, 255, 0.1);
}

.dark-edition .popover,
.dark-edition .tooltip-inner {
  background: #202940;
  color: #8b92a9;
}

.dark-edition .tooltip.bs-tooltip-top .arrow::before,
.dark-edition .tooltip.bs-tooltip-auto[x-placement^="top"] .arrow::before,
.dark-edition .tooltip.bs-tooltip-auto[x-placement^="top"] .arrow::before {
  border-top-color: #202940;
}

.dark-edition .tooltip.bs-tooltip-bottom .arrow::before,
.dark-edition .tooltip.bs-tooltip-auto[x-placement^="bottom"] .arrow::before,
.dark-edition .tooltip.bs-tooltip-auto[x-placement^="bottom"] .arrow::before {
  border-bottom-color: #202940;
}

.dark-edition .tooltip.bs-tooltip-right .arrow::before,
.dark-edition .tooltip.bs-tooltip-auto[x-placement^="right"] .arrow::before,
.dark-edition .tooltip.bs-tooltip-auto[x-placement^="right"] .arrow::before {
  border-right-color: #202940;
}

.dark-edition .tooltip.bs-tooltip-left .arrow::before,
.dark-edition .tooltip.bs-tooltip-auto[x-placement^="left"] .arrow::before,
.dark-edition .tooltip.bs-tooltip-auto[x-placement^="left"] .arrow::before {
  border-left-color: #202940;
}

.dark-edition .form-controllllll {
  color: #000;
}

.dark-edition .form-controllllll::-moz-placeholder {
  color: #000;
}

.dark-edition .form-controllllll:-ms-input-placeholder {
  color: #000;
}

.dark-edition .form-controllllll::-webkit-input-placeholder {
  color: #000;
}

.dark-edition .bmd-form-group:not(.has-success):not(.has-danger) [class^='bmd-label'].bmd-label-floating,
.dark-edition .bmd-form-group:not(.has-success):not(.has-danger) [class*=' bmd-label'].bmd-label-floating {
  color: #000;
}

.dark-edition .form-group label {
  color: #a9afbbd1;
}

.dark-edition [class^='bmd-label'],
.dark-edition [class*=' bmd-label'] {
  color: #999999;
}

.dark-edition .form-controllllll,
.is-focused .dark-edition .form-controllllll {
  background-image: linear-gradient(to top, #9c27b0 2px, rgba(156, 39, 176, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px);
}

.dark-edition .form-controllllll:invalid {
  background-image: linear-gradient(to top, #f44336 2px, rgba(244, 67, 54, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px);
}

.dark-edition .form-controllllll:read-only {
  background-image: linear-gradient(to top, #d2d2d2 1px, rgba(210, 210, 210, 0) 1px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px);
}

fieldset[disabled][disabled] .dark-edition .form-controllllll,
.dark-edition .form-controllllll.disabled,
.dark-edition .form-controllllll:disabled,
.dark-edition .form-controllllll[disabled] {
  background-image: linear-gradient(to right, rgba(180, 180, 180, 0.1) 0%, rgba(180, 180, 180, 0.1) 30%, transparent 30%, transparent 100%);
  background-repeat: repeat-x;
  background-size: 3px 1px;
}

.dark-edition .form-controllllll.form-controllllll-success,
.is-focused .dark-edition .form-controllllll.form-controllllll-success {
  background-image: linear-gradient(to top, #9c27b0 2px, rgba(156, 39, 176, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px), "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2MTIgNzkyIj48cGF0aCBmaWxsPSIjNWNiODVjIiBkPSJNMjMzLjggNjEwYy0xMy4zIDAtMjYtNi0zNC0xNi44TDkwLjUgNDQ4LjhDNzYuMyA0MzAgODAgNDAzLjMgOTguOCAzODljMTguOC0xNC4yIDQ1LjUtMTAuNCA1OS44IDguNGw3MiA5NUw0NTEuMyAyNDJjMTIuNS0yMCAzOC44LTI2LjIgNTguOC0xMy43IDIwIDEyLjQgMjYgMzguNyAxMy43IDU4LjhMMjcwIDU5MGMtNy40IDEyLTIwLjIgMTkuNC0zNC4zIDIwaC0yeiIvPjwvc3ZnPg==";
}

.dark-edition .form-controllllll.form-controllllll-warning,
.is-focused .dark-edition .form-controllllll.form-controllllll-warning {
  background-image: linear-gradient(to top, #9c27b0 2px, rgba(156, 39, 176, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px), "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2MTIgNzkyIj48cGF0aCBmaWxsPSIjZjBhZDRlIiBkPSJNNjAzIDY0MC4ybC0yNzguNS01MDljLTMuOC02LjYtMTAuOC0xMC42LTE4LjUtMTAuNnMtMTQuNyA0LTE4LjUgMTAuNkw5IDY0MC4yYy0zLjcgNi41LTMuNiAxNC40LjIgMjAuOCAzLjggNi41IDEwLjggMTAuNCAxOC4zIDEwLjRoNTU3YzcuNiAwIDE0LjYtNCAxOC40LTEwLjQgMy41LTYuNCAzLjYtMTQuNCAwLTIwLjh6bS0yNjYuNC0zMGgtNjEuMlY1NDloNjEuMnY2MS4yem0wLTEwN2gtNjEuMlYzMDRoNjEuMnYxOTl6Ii8+PC9zdmc+";
}

.dark-edition .form-controllllll.form-controllllll-danger,
.is-focused .dark-edition .form-controllllll.form-controllllll-danger {
  background-image: linear-gradient(to top, #9c27b0 2px, rgba(156, 39, 176, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px), "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2MTIgNzkyIj48cGF0aCBmaWxsPSIjZDk1MzRmIiBkPSJNNDQ3IDU0NC40Yy0xNC40IDE0LjQtMzcuNiAxNC40LTUyIDBsLTg5LTkyLjctODkgOTIuN2MtMTQuNSAxNC40LTM3LjcgMTQuNC01MiAwLTE0LjQtMTQuNC0xNC40LTM3LjYgMC01Mmw5Mi40LTk2LjMtOTIuNC05Ni4zYy0xNC40LTE0LjQtMTQuNC0zNy42IDAtNTJzMzcuNi0xNC4zIDUyIDBsODkgOTIuOCA4OS4yLTkyLjdjMTQuNC0xNC40IDM3LjYtMTQuNCA1MiAwIDE0LjMgMTQuNCAxNC4zIDM3LjYgMCA1MkwzNTQuNiAzOTZsOTIuNCA5Ni40YzE0LjQgMTQuNCAxNC40IDM3LjYgMCA1MnoiLz48L3N2Zz4=";
}

.dark-edition .is-focused .valid-feedback {
  display: none;
  width: 100%;
  margin-top: 0.25rem;
  font-size: 80%;
  color: #999999;
}

.dark-edition .is-focused .valid-tooltip {
  position: absolute;
  top: 100%;
  z-index: 5;
  display: none;
  max-width: 100%;
  padding: .5rem;
  margin-top: .1rem;
  font-size: .875rem;
  line-height: 1;
  color: #fff;
  background-color: rgba(153, 153, 153, 0.8);
  border-radius: .2rem;
}

.was-validated .dark-edition .is-focused .form-controllllll:valid,
.dark-edition .is-focused .form-controllllll.is-valid,
.was-validated .dark-edition .is-focused .custom-select:valid,
.dark-edition .is-focused .custom-select.is-valid {
  border-color: #999999;
}

.was-validated .dark-edition .is-focused .form-controllllll:valid:focus,
.dark-edition .is-focused .form-controllllll.is-valid:focus,
.was-validated .dark-edition .is-focused .custom-select:valid:focus,
.dark-edition .is-focused .custom-select.is-valid:focus {
  border-color: #999999;
  box-shadow: 0 0 0 0.2rem rgba(153, 153, 153, 0.25);
}

.was-validated .dark-edition .is-focused .form-controllllll:valid~.valid-feedback,
.was-validated .dark-edition .is-focused .form-controllllll:valid~.valid-tooltip,
.dark-edition .is-focused .form-controllllll.is-valid~.valid-feedback,
.dark-edition .is-focused .form-controllllll.is-valid~.valid-tooltip,
.was-validated .dark-edition .is-focused .custom-select:valid~.valid-feedback,
.was-validated .dark-edition .is-focused .custom-select:valid~.valid-tooltip,
.dark-edition .is-focused .custom-select.is-valid~.valid-feedback,
.dark-edition .is-focused .custom-select.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .is-focused .form-check-input:valid~.form-check-label,
.dark-edition .is-focused .form-check-input.is-valid~.form-check-label {
  color: #999999;
}

.was-validated .dark-edition .is-focused .form-check-input:valid~.valid-feedback,
.was-validated .dark-edition .is-focused .form-check-input:valid~.valid-tooltip,
.dark-edition .is-focused .form-check-input.is-valid~.valid-feedback,
.dark-edition .is-focused .form-check-input.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .is-focused .custom-control-input:valid~.custom-control-label,
.dark-edition .is-focused .custom-control-input.is-valid~.custom-control-label {
  color: #999999;
}

.was-validated .dark-edition .is-focused .custom-control-input:valid~.custom-control-label::before,
.dark-edition .is-focused .custom-control-input.is-valid~.custom-control-label::before {
  background-color: #d9d9d9;
}

.was-validated .dark-edition .is-focused .custom-control-input:valid~.valid-feedback,
.was-validated .dark-edition .is-focused .custom-control-input:valid~.valid-tooltip,
.dark-edition .is-focused .custom-control-input.is-valid~.valid-feedback,
.dark-edition .is-focused .custom-control-input.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .is-focused .custom-control-input:valid:checked~.custom-control-label::before,
.dark-edition .is-focused .custom-control-input.is-valid:checked~.custom-control-label::before {
  background-color: #b3b3b3;
}

.was-validated .dark-edition .is-focused .custom-control-input:valid:focus~.custom-control-label::before,
.dark-edition .is-focused .custom-control-input.is-valid:focus~.custom-control-label::before {
  box-shadow: 0 0 0 1px #fafafa, 0 0 0 0.2rem rgba(153, 153, 153, 0.25);
}

.was-validated .dark-edition .is-focused .custom-file-input:valid~.custom-file-label,
.dark-edition .is-focused .custom-file-input.is-valid~.custom-file-label {
  border-color: #999999;
}

.was-validated .dark-edition .is-focused .custom-file-input:valid~.custom-file-label::before,
.dark-edition .is-focused .custom-file-input.is-valid~.custom-file-label::before {
  border-color: inherit;
}

.was-validated .dark-edition .is-focused .custom-file-input:valid~.valid-feedback,
.was-validated .dark-edition .is-focused .custom-file-input:valid~.valid-tooltip,
.dark-edition .is-focused .custom-file-input.is-valid~.valid-feedback,
.dark-edition .is-focused .custom-file-input.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .is-focused .custom-file-input:valid:focus~.custom-file-label,
.dark-edition .is-focused .custom-file-input.is-valid:focus~.custom-file-label {
  box-shadow: 0 0 0 0.2rem rgba(153, 153, 153, 0.25);
}

.dark-edition .is-focused [class^='bmd-label'],
.dark-edition .is-focused [class*=' bmd-label'] {
  color: #9c27b0;
}

.dark-edition .is-focused .bmd-label-placeholder {
  color: #999999;
}

.dark-edition .is-focused .form-controllllll {
  border-color: rgba(180, 180, 180, 0.1);
}

.dark-edition .is-focused .bmd-help {
  color: #555555;
}

.dark-edition .has-success [class^='bmd-label'],
.dark-edition .has-success [class*=' bmd-label'] {
  color: #4caf50;
}

.dark-edition .has-success .form-controllllll,
.is-focused .dark-edition .has-success .form-controllllll {
  background-image: linear-gradient(to top, #4caf50 2px, rgba(76, 175, 80, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px);
}

.dark-edition .has-success .form-controllllll:invalid {
  background-image: linear-gradient(to top, #f44336 2px, rgba(244, 67, 54, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px);
}

.dark-edition .has-success .form-controllllll:read-only {
  background-image: linear-gradient(to top, #d2d2d2 1px, rgba(210, 210, 210, 0) 1px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px);
}

fieldset[disabled][disabled] .dark-edition .has-success .form-controllllll,
.dark-edition .has-success .form-controllllll.disabled,
.dark-edition .has-success .form-controllllll:disabled,
.dark-edition .has-success .form-controllllll[disabled] {
  background-image: linear-gradient(to right, rgba(180, 180, 180, 0.1) 0%, rgba(180, 180, 180, 0.1) 30%, transparent 30%, transparent 100%);
  background-repeat: repeat-x;
  background-size: 3px 1px;
}

.dark-edition .has-success .form-controllllll.form-controllllll-success,
.is-focused .dark-edition .has-success .form-controllllll.form-controllllll-success {
  background-image: linear-gradient(to top, #4caf50 2px, rgba(76, 175, 80, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px), "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2MTIgNzkyIj48cGF0aCBmaWxsPSIjNWNiODVjIiBkPSJNMjMzLjggNjEwYy0xMy4zIDAtMjYtNi0zNC0xNi44TDkwLjUgNDQ4LjhDNzYuMyA0MzAgODAgNDAzLjMgOTguOCAzODljMTguOC0xNC4yIDQ1LjUtMTAuNCA1OS44IDguNGw3MiA5NUw0NTEuMyAyNDJjMTIuNS0yMCAzOC44LTI2LjIgNTguOC0xMy43IDIwIDEyLjQgMjYgMzguNyAxMy43IDU4LjhMMjcwIDU5MGMtNy40IDEyLTIwLjIgMTkuNC0zNC4zIDIwaC0yeiIvPjwvc3ZnPg==";
}

.dark-edition .has-success .form-controllllll.form-controllllll-warning,
.is-focused .dark-edition .has-success .form-controllllll.form-controllllll-warning {
  background-image: linear-gradient(to top, #4caf50 2px, rgba(76, 175, 80, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px), "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2MTIgNzkyIj48cGF0aCBmaWxsPSIjZjBhZDRlIiBkPSJNNjAzIDY0MC4ybC0yNzguNS01MDljLTMuOC02LjYtMTAuOC0xMC42LTE4LjUtMTAuNnMtMTQuNyA0LTE4LjUgMTAuNkw5IDY0MC4yYy0zLjcgNi41LTMuNiAxNC40LjIgMjAuOCAzLjggNi41IDEwLjggMTAuNCAxOC4zIDEwLjRoNTU3YzcuNiAwIDE0LjYtNCAxOC40LTEwLjQgMy41LTYuNCAzLjYtMTQuNCAwLTIwLjh6bS0yNjYuNC0zMGgtNjEuMlY1NDloNjEuMnY2MS4yem0wLTEwN2gtNjEuMlYzMDRoNjEuMnYxOTl6Ii8+PC9zdmc+";
}

.dark-edition .has-success .form-controllllll.form-controllllll-danger,
.is-focused .dark-edition .has-success .form-controllllll.form-controllllll-danger {
  background-image: linear-gradient(to top, #4caf50 2px, rgba(76, 175, 80, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px), "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2MTIgNzkyIj48cGF0aCBmaWxsPSIjZDk1MzRmIiBkPSJNNDQ3IDU0NC40Yy0xNC40IDE0LjQtMzcuNiAxNC40LTUyIDBsLTg5LTkyLjctODkgOTIuN2MtMTQuNSAxNC40LTM3LjcgMTQuNC01MiAwLTE0LjQtMTQuNC0xNC40LTM3LjYgMC01Mmw5Mi40LTk2LjMtOTIuNC05Ni4zYy0xNC40LTE0LjQtMTQuNC0zNy42IDAtNTJzMzcuNi0xNC4zIDUyIDBsODkgOTIuOCA4OS4yLTkyLjdjMTQuNC0xNC40IDM3LjYtMTQuNCA1MiAwIDE0LjMgMTQuNCAxNC4zIDM3LjYgMCA1MkwzNTQuNiAzOTZsOTIuNCA5Ni40YzE0LjQgMTQuNCAxNC40IDM3LjYgMCA1MnoiLz48L3N2Zz4=";
}

.dark-edition .has-success .is-focused .valid-feedback {
  display: none;
  width: 100%;
  margin-top: 0.25rem;
  font-size: 80%;
  color: #4caf50;
}

.dark-edition .has-success .is-focused .valid-tooltip {
  position: absolute;
  top: 100%;
  z-index: 5;
  display: none;
  max-width: 100%;
  padding: .5rem;
  margin-top: .1rem;
  font-size: .875rem;
  line-height: 1;
  color: #fff;
  background-color: rgba(76, 175, 80, 0.8);
  border-radius: .2rem;
}

.was-validated .dark-edition .has-success .is-focused .form-controllllll:valid,
.dark-edition .has-success .is-focused .form-controllllll.is-valid,
.was-validated .dark-edition .has-success .is-focused .custom-select:valid,
.dark-edition .has-success .is-focused .custom-select.is-valid {
  border-color: #4caf50;
}

.was-validated .dark-edition .has-success .is-focused .form-controllllll:valid:focus,
.dark-edition .has-success .is-focused .form-controllllll.is-valid:focus,
.was-validated .dark-edition .has-success .is-focused .custom-select:valid:focus,
.dark-edition .has-success .is-focused .custom-select.is-valid:focus {
  border-color: #4caf50;
  box-shadow: 0 0 0 0.2rem rgba(76, 175, 80, 0.25);
}

.was-validated .dark-edition .has-success .is-focused .form-controllllll:valid~.valid-feedback,
.was-validated .dark-edition .has-success .is-focused .form-controllllll:valid~.valid-tooltip,
.dark-edition .has-success .is-focused .form-controllllll.is-valid~.valid-feedback,
.dark-edition .has-success .is-focused .form-controllllll.is-valid~.valid-tooltip,
.was-validated .dark-edition .has-success .is-focused .custom-select:valid~.valid-feedback,
.was-validated .dark-edition .has-success .is-focused .custom-select:valid~.valid-tooltip,
.dark-edition .has-success .is-focused .custom-select.is-valid~.valid-feedback,
.dark-edition .has-success .is-focused .custom-select.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-success .is-focused .form-check-input:valid~.form-check-label,
.dark-edition .has-success .is-focused .form-check-input.is-valid~.form-check-label {
  color: #4caf50;
}

.was-validated .dark-edition .has-success .is-focused .form-check-input:valid~.valid-feedback,
.was-validated .dark-edition .has-success .is-focused .form-check-input:valid~.valid-tooltip,
.dark-edition .has-success .is-focused .form-check-input.is-valid~.valid-feedback,
.dark-edition .has-success .is-focused .form-check-input.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-success .is-focused .custom-control-input:valid~.custom-control-label,
.dark-edition .has-success .is-focused .custom-control-input.is-valid~.custom-control-label {
  color: #4caf50;
}

.was-validated .dark-edition .has-success .is-focused .custom-control-input:valid~.custom-control-label::before,
.dark-edition .has-success .is-focused .custom-control-input.is-valid~.custom-control-label::before {
  background-color: #a3d7a5;
}

.was-validated .dark-edition .has-success .is-focused .custom-control-input:valid~.valid-feedback,
.was-validated .dark-edition .has-success .is-focused .custom-control-input:valid~.valid-tooltip,
.dark-edition .has-success .is-focused .custom-control-input.is-valid~.valid-feedback,
.dark-edition .has-success .is-focused .custom-control-input.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-success .is-focused .custom-control-input:valid:checked~.custom-control-label::before,
.dark-edition .has-success .is-focused .custom-control-input.is-valid:checked~.custom-control-label::before {
  background-color: #6ec071;
}

.was-validated .dark-edition .has-success .is-focused .custom-control-input:valid:focus~.custom-control-label::before,
.dark-edition .has-success .is-focused .custom-control-input.is-valid:focus~.custom-control-label::before {
  box-shadow: 0 0 0 1px #fafafa, 0 0 0 0.2rem rgba(76, 175, 80, 0.25);
}

.was-validated .dark-edition .has-success .is-focused .custom-file-input:valid~.custom-file-label,
.dark-edition .has-success .is-focused .custom-file-input.is-valid~.custom-file-label {
  border-color: #4caf50;
}

.was-validated .dark-edition .has-success .is-focused .custom-file-input:valid~.custom-file-label::before,
.dark-edition .has-success .is-focused .custom-file-input.is-valid~.custom-file-label::before {
  border-color: inherit;
}

.was-validated .dark-edition .has-success .is-focused .custom-file-input:valid~.valid-feedback,
.was-validated .dark-edition .has-success .is-focused .custom-file-input:valid~.valid-tooltip,
.dark-edition .has-success .is-focused .custom-file-input.is-valid~.valid-feedback,
.dark-edition .has-success .is-focused .custom-file-input.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-success .is-focused .custom-file-input:valid:focus~.custom-file-label,
.dark-edition .has-success .is-focused .custom-file-input.is-valid:focus~.custom-file-label {
  box-shadow: 0 0 0 0.2rem rgba(76, 175, 80, 0.25);
}

.dark-edition .has-success .is-focused [class^='bmd-label'],
.dark-edition .has-success .is-focused [class*=' bmd-label'] {
  color: #4caf50;
}

.dark-edition .has-success .is-focused .bmd-label-placeholder {
  color: #4caf50;
}

.dark-edition .has-success .is-focused .form-controllllll {
  border-color: #4caf50;
}

.dark-edition .has-success .is-focused .bmd-help {
  color: #555555;
}

.dark-edition .has-info [class^='bmd-label'],
.dark-edition .has-info [class*=' bmd-label'] {
  color: #00bcd4;
}

.dark-edition .has-info .form-controllllll,
.is-focused .dark-edition .has-info .form-controllllll {
  background-image: linear-gradient(to top, #00bcd4 2px, rgba(0, 188, 212, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px);
}

.dark-edition .has-info .form-controllllll:invalid {
  background-image: linear-gradient(to top, #f44336 2px, rgba(244, 67, 54, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px);
}

.dark-edition .has-info .form-controllllll:read-only {
  background-image: linear-gradient(to top, #d2d2d2 1px, rgba(210, 210, 210, 0) 1px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px);
}

fieldset[disabled][disabled] .dark-edition .has-info .form-controllllll,
.dark-edition .has-info .form-controllllll.disabled,
.dark-edition .has-info .form-controllllll:disabled,
.dark-edition .has-info .form-controllllll[disabled] {
  background-image: linear-gradient(to right, rgba(180, 180, 180, 0.1) 0%, rgba(180, 180, 180, 0.1) 30%, transparent 30%, transparent 100%);
  background-repeat: repeat-x;
  background-size: 3px 1px;
}

.dark-edition .has-info .form-controllllll.form-controllllll-success,
.is-focused .dark-edition .has-info .form-controllllll.form-controllllll-success {
  background-image: linear-gradient(to top, #00bcd4 2px, rgba(0, 188, 212, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px), "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2MTIgNzkyIj48cGF0aCBmaWxsPSIjNWNiODVjIiBkPSJNMjMzLjggNjEwYy0xMy4zIDAtMjYtNi0zNC0xNi44TDkwLjUgNDQ4LjhDNzYuMyA0MzAgODAgNDAzLjMgOTguOCAzODljMTguOC0xNC4yIDQ1LjUtMTAuNCA1OS44IDguNGw3MiA5NUw0NTEuMyAyNDJjMTIuNS0yMCAzOC44LTI2LjIgNTguOC0xMy43IDIwIDEyLjQgMjYgMzguNyAxMy43IDU4LjhMMjcwIDU5MGMtNy40IDEyLTIwLjIgMTkuNC0zNC4zIDIwaC0yeiIvPjwvc3ZnPg==";
}

.dark-edition .has-info .form-controllllll.form-controllllll-warning,
.is-focused .dark-edition .has-info .form-controllllll.form-controllllll-warning {
  background-image: linear-gradient(to top, #00bcd4 2px, rgba(0, 188, 212, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px), "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2MTIgNzkyIj48cGF0aCBmaWxsPSIjZjBhZDRlIiBkPSJNNjAzIDY0MC4ybC0yNzguNS01MDljLTMuOC02LjYtMTAuOC0xMC42LTE4LjUtMTAuNnMtMTQuNyA0LTE4LjUgMTAuNkw5IDY0MC4yYy0zLjcgNi41LTMuNiAxNC40LjIgMjAuOCAzLjggNi41IDEwLjggMTAuNCAxOC4zIDEwLjRoNTU3YzcuNiAwIDE0LjYtNCAxOC40LTEwLjQgMy41LTYuNCAzLjYtMTQuNCAwLTIwLjh6bS0yNjYuNC0zMGgtNjEuMlY1NDloNjEuMnY2MS4yem0wLTEwN2gtNjEuMlYzMDRoNjEuMnYxOTl6Ii8+PC9zdmc+";
}

.dark-edition .has-info .form-controllllll.form-controllllll-danger,
.is-focused .dark-edition .has-info .form-controllllll.form-controllllll-danger {
  background-image: linear-gradient(to top, #00bcd4 2px, rgba(0, 188, 212, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px), "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2MTIgNzkyIj48cGF0aCBmaWxsPSIjZDk1MzRmIiBkPSJNNDQ3IDU0NC40Yy0xNC40IDE0LjQtMzcuNiAxNC40LTUyIDBsLTg5LTkyLjctODkgOTIuN2MtMTQuNSAxNC40LTM3LjcgMTQuNC01MiAwLTE0LjQtMTQuNC0xNC40LTM3LjYgMC01Mmw5Mi40LTk2LjMtOTIuNC05Ni4zYy0xNC40LTE0LjQtMTQuNC0zNy42IDAtNTJzMzcuNi0xNC4zIDUyIDBsODkgOTIuOCA4OS4yLTkyLjdjMTQuNC0xNC40IDM3LjYtMTQuNCA1MiAwIDE0LjMgMTQuNCAxNC4zIDM3LjYgMCA1MkwzNTQuNiAzOTZsOTIuNCA5Ni40YzE0LjQgMTQuNCAxNC40IDM3LjYgMCA1MnoiLz48L3N2Zz4=";
}

.dark-edition .has-info .is-focused .valid-feedback {
  display: none;
  width: 100%;
  margin-top: 0.25rem;
  font-size: 80%;
  color: #00bcd4;
}

.dark-edition .has-info .is-focused .valid-tooltip {
  position: absolute;
  top: 100%;
  z-index: 5;
  display: none;
  max-width: 100%;
  padding: .5rem;
  margin-top: .1rem;
  font-size: .875rem;
  line-height: 1;
  color: #fff;
  background-color: rgba(0, 188, 212, 0.8);
  border-radius: .2rem;
}

.was-validated .dark-edition .has-info .is-focused .form-controllllll:valid,
.dark-edition .has-info .is-focused .form-controllllll.is-valid,
.was-validated .dark-edition .has-info .is-focused .custom-select:valid,
.dark-edition .has-info .is-focused .custom-select.is-valid {
  border-color: #00bcd4;
}

.was-validated .dark-edition .has-info .is-focused .form-controllllll:valid:focus,
.dark-edition .has-info .is-focused .form-controllllll.is-valid:focus,
.was-validated .dark-edition .has-info .is-focused .custom-select:valid:focus,
.dark-edition .has-info .is-focused .custom-select.is-valid:focus {
  border-color: #00bcd4;
  box-shadow: 0 0 0 0.2rem rgba(0, 188, 212, 0.25);
}

.was-validated .dark-edition .has-info .is-focused .form-controllllll:valid~.valid-feedback,
.was-validated .dark-edition .has-info .is-focused .form-controllllll:valid~.valid-tooltip,
.dark-edition .has-info .is-focused .form-controllllll.is-valid~.valid-feedback,
.dark-edition .has-info .is-focused .form-controllllll.is-valid~.valid-tooltip,
.was-validated .dark-edition .has-info .is-focused .custom-select:valid~.valid-feedback,
.was-validated .dark-edition .has-info .is-focused .custom-select:valid~.valid-tooltip,
.dark-edition .has-info .is-focused .custom-select.is-valid~.valid-feedback,
.dark-edition .has-info .is-focused .custom-select.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-info .is-focused .form-check-input:valid~.form-check-label,
.dark-edition .has-info .is-focused .form-check-input.is-valid~.form-check-label {
  color: #00bcd4;
}

.was-validated .dark-edition .has-info .is-focused .form-check-input:valid~.valid-feedback,
.was-validated .dark-edition .has-info .is-focused .form-check-input:valid~.valid-tooltip,
.dark-edition .has-info .is-focused .form-check-input.is-valid~.valid-feedback,
.dark-edition .has-info .is-focused .form-check-input.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-info .is-focused .custom-control-input:valid~.custom-control-label,
.dark-edition .has-info .is-focused .custom-control-input.is-valid~.custom-control-label {
  color: #00bcd4;
}

.was-validated .dark-edition .has-info .is-focused .custom-control-input:valid~.custom-control-label::before,
.dark-edition .has-info .is-focused .custom-control-input.is-valid~.custom-control-label::before {
  background-color: #55ecff;
}

.was-validated .dark-edition .has-info .is-focused .custom-control-input:valid~.valid-feedback,
.was-validated .dark-edition .has-info .is-focused .custom-control-input:valid~.valid-tooltip,
.dark-edition .has-info .is-focused .custom-control-input.is-valid~.valid-feedback,
.dark-edition .has-info .is-focused .custom-control-input.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-info .is-focused .custom-control-input:valid:checked~.custom-control-label::before,
.dark-edition .has-info .is-focused .custom-control-input.is-valid:checked~.custom-control-label::before {
  background-color: #08e3ff;
}

.was-validated .dark-edition .has-info .is-focused .custom-control-input:valid:focus~.custom-control-label::before,
.dark-edition .has-info .is-focused .custom-control-input.is-valid:focus~.custom-control-label::before {
  box-shadow: 0 0 0 1px #fafafa, 0 0 0 0.2rem rgba(0, 188, 212, 0.25);
}

.was-validated .dark-edition .has-info .is-focused .custom-file-input:valid~.custom-file-label,
.dark-edition .has-info .is-focused .custom-file-input.is-valid~.custom-file-label {
  border-color: #00bcd4;
}

.was-validated .dark-edition .has-info .is-focused .custom-file-input:valid~.custom-file-label::before,
.dark-edition .has-info .is-focused .custom-file-input.is-valid~.custom-file-label::before {
  border-color: inherit;
}

.was-validated .dark-edition .has-info .is-focused .custom-file-input:valid~.valid-feedback,
.was-validated .dark-edition .has-info .is-focused .custom-file-input:valid~.valid-tooltip,
.dark-edition .has-info .is-focused .custom-file-input.is-valid~.valid-feedback,
.dark-edition .has-info .is-focused .custom-file-input.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-info .is-focused .custom-file-input:valid:focus~.custom-file-label,
.dark-edition .has-info .is-focused .custom-file-input.is-valid:focus~.custom-file-label {
  box-shadow: 0 0 0 0.2rem rgba(0, 188, 212, 0.25);
}

.dark-edition .has-info .is-focused [class^='bmd-label'],
.dark-edition .has-info .is-focused [class*=' bmd-label'] {
  color: #00bcd4;
}

.dark-edition .has-info .is-focused .bmd-label-placeholder {
  color: #00bcd4;
}

.dark-edition .has-info .is-focused .form-controllllll {
  border-color: #00bcd4;
}

.dark-edition .has-info .is-focused .bmd-help {
  color: #555555;
}

.dark-edition .has-white [class^='bmd-label'],
.dark-edition .has-white [class*=' bmd-label'] {
  color: #fff;
}

.dark-edition .has-white .form-controllllll,
.is-focused .dark-edition .has-white .form-controllllll {
  background-image: linear-gradient(to top, #fff 2px, rgba(255, 255, 255, 0) 2px), linear-gradient(to top, #FFFFFF 1px, rgba(255, 255, 255, 0) 1px);
}

.dark-edition .has-white .form-controllllll:invalid {
  background-image: linear-gradient(to top, #f44336 2px, rgba(244, 67, 54, 0) 2px), linear-gradient(to top, #FFFFFF 1px, rgba(255, 255, 255, 0) 1px);
}

.dark-edition .has-white .form-controllllll:read-only {
  background-image: linear-gradient(to top, #d2d2d2 1px, rgba(210, 210, 210, 0) 1px), linear-gradient(to top, #FFFFFF 1px, rgba(255, 255, 255, 0) 1px);
}

fieldset[disabled][disabled] .dark-edition .has-white .form-controllllll,
.dark-edition .has-white .form-controllllll.disabled,
.dark-edition .has-white .form-controllllll:disabled,
.dark-edition .has-white .form-controllllll[disabled] {
  background-image: linear-gradient(to right, #FFFFFF 0%, #FFFFFF 30%, transparent 30%, transparent 100%);
  background-repeat: repeat-x;
  background-size: 3px 1px;
}

.dark-edition .has-white .form-controllllll.form-controllllll-success,
.is-focused .dark-edition .has-white .form-controllllll.form-controllllll-success {
  background-image: linear-gradient(to top, #fff 2px, rgba(255, 255, 255, 0) 2px), linear-gradient(to top, #FFFFFF 1px, rgba(255, 255, 255, 0) 1px), "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2MTIgNzkyIj48cGF0aCBmaWxsPSIjNWNiODVjIiBkPSJNMjMzLjggNjEwYy0xMy4zIDAtMjYtNi0zNC0xNi44TDkwLjUgNDQ4LjhDNzYuMyA0MzAgODAgNDAzLjMgOTguOCAzODljMTguOC0xNC4yIDQ1LjUtMTAuNCA1OS44IDguNGw3MiA5NUw0NTEuMyAyNDJjMTIuNS0yMCAzOC44LTI2LjIgNTguOC0xMy43IDIwIDEyLjQgMjYgMzguNyAxMy43IDU4LjhMMjcwIDU5MGMtNy40IDEyLTIwLjIgMTkuNC0zNC4zIDIwaC0yeiIvPjwvc3ZnPg==";
}

.dark-edition .has-white .form-controllllll.form-controllllll-warning,
.is-focused .dark-edition .has-white .form-controllllll.form-controllllll-warning {
  background-image: linear-gradient(to top, #fff 2px, rgba(255, 255, 255, 0) 2px), linear-gradient(to top, #FFFFFF 1px, rgba(255, 255, 255, 0) 1px), "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2MTIgNzkyIj48cGF0aCBmaWxsPSIjZjBhZDRlIiBkPSJNNjAzIDY0MC4ybC0yNzguNS01MDljLTMuOC02LjYtMTAuOC0xMC42LTE4LjUtMTAuNnMtMTQuNyA0LTE4LjUgMTAuNkw5IDY0MC4yYy0zLjcgNi41LTMuNiAxNC40LjIgMjAuOCAzLjggNi41IDEwLjggMTAuNCAxOC4zIDEwLjRoNTU3YzcuNiAwIDE0LjYtNCAxOC40LTEwLjQgMy41LTYuNCAzLjYtMTQuNCAwLTIwLjh6bS0yNjYuNC0zMGgtNjEuMlY1NDloNjEuMnY2MS4yem0wLTEwN2gtNjEuMlYzMDRoNjEuMnYxOTl6Ii8+PC9zdmc+";
}

.dark-edition .has-white .form-controllllll.form-controllllll-danger,
.is-focused .dark-edition .has-white .form-controllllll.form-controllllll-danger {
  background-image: linear-gradient(to top, #fff 2px, rgba(255, 255, 255, 0) 2px), linear-gradient(to top, #FFFFFF 1px, rgba(255, 255, 255, 0) 1px), "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2MTIgNzkyIj48cGF0aCBmaWxsPSIjZDk1MzRmIiBkPSJNNDQ3IDU0NC40Yy0xNC40IDE0LjQtMzcuNiAxNC40LTUyIDBsLTg5LTkyLjctODkgOTIuN2MtMTQuNSAxNC40LTM3LjcgMTQuNC01MiAwLTE0LjQtMTQuNC0xNC40LTM3LjYgMC01Mmw5Mi40LTk2LjMtOTIuNC05Ni4zYy0xNC40LTE0LjQtMTQuNC0zNy42IDAtNTJzMzcuNi0xNC4zIDUyIDBsODkgOTIuOCA4OS4yLTkyLjdjMTQuNC0xNC40IDM3LjYtMTQuNCA1MiAwIDE0LjMgMTQuNCAxNC4zIDM3LjYgMCA1MkwzNTQuNiAzOTZsOTIuNCA5Ni40YzE0LjQgMTQuNCAxNC40IDM3LjYgMCA1MnoiLz48L3N2Zz4=";
}

.dark-edition .has-white .is-focused .valid-feedback {
  display: none;
  width: 100%;
  margin-top: 0.25rem;
  font-size: 80%;
  color: #fff;
}

.dark-edition .has-white .is-focused .valid-tooltip {
  position: absolute;
  top: 100%;
  z-index: 5;
  display: none;
  max-width: 100%;
  padding: .5rem;
  margin-top: .1rem;
  font-size: .875rem;
  line-height: 1;
  color: #fff;
  background-color: rgba(255, 255, 255, 0.8);
  border-radius: .2rem;
}

.was-validated .dark-edition .has-white .is-focused .form-controllllll:valid,
.dark-edition .has-white .is-focused .form-controllllll.is-valid,
.was-validated .dark-edition .has-white .is-focused .custom-select:valid,
.dark-edition .has-white .is-focused .custom-select.is-valid {
  border-color: #fff;
}

.was-validated .dark-edition .has-white .is-focused .form-controllllll:valid:focus,
.dark-edition .has-white .is-focused .form-controllllll.is-valid:focus,
.was-validated .dark-edition .has-white .is-focused .custom-select:valid:focus,
.dark-edition .has-white .is-focused .custom-select.is-valid:focus {
  border-color: #fff;
  box-shadow: 0 0 0 0.2rem rgba(255, 255, 255, 0.25);
}

.was-validated .dark-edition .has-white .is-focused .form-controllllll:valid~.valid-feedback,
.was-validated .dark-edition .has-white .is-focused .form-controllllll:valid~.valid-tooltip,
.dark-edition .has-white .is-focused .form-controllllll.is-valid~.valid-feedback,
.dark-edition .has-white .is-focused .form-controllllll.is-valid~.valid-tooltip,
.was-validated .dark-edition .has-white .is-focused .custom-select:valid~.valid-feedback,
.was-validated .dark-edition .has-white .is-focused .custom-select:valid~.valid-tooltip,
.dark-edition .has-white .is-focused .custom-select.is-valid~.valid-feedback,
.dark-edition .has-white .is-focused .custom-select.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-white .is-focused .form-check-input:valid~.form-check-label,
.dark-edition .has-white .is-focused .form-check-input.is-valid~.form-check-label {
  color: #fff;
}

.was-validated .dark-edition .has-white .is-focused .form-check-input:valid~.valid-feedback,
.was-validated .dark-edition .has-white .is-focused .form-check-input:valid~.valid-tooltip,
.dark-edition .has-white .is-focused .form-check-input.is-valid~.valid-feedback,
.dark-edition .has-white .is-focused .form-check-input.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-white .is-focused .custom-control-input:valid~.custom-control-label,
.dark-edition .has-white .is-focused .custom-control-input.is-valid~.custom-control-label {
  color: #fff;
}

.was-validated .dark-edition .has-white .is-focused .custom-control-input:valid~.custom-control-label::before,
.dark-edition .has-white .is-focused .custom-control-input.is-valid~.custom-control-label::before {
  background-color: white;
}

.was-validated .dark-edition .has-white .is-focused .custom-control-input:valid~.valid-feedback,
.was-validated .dark-edition .has-white .is-focused .custom-control-input:valid~.valid-tooltip,
.dark-edition .has-white .is-focused .custom-control-input.is-valid~.valid-feedback,
.dark-edition .has-white .is-focused .custom-control-input.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-white .is-focused .custom-control-input:valid:checked~.custom-control-label::before,
.dark-edition .has-white .is-focused .custom-control-input.is-valid:checked~.custom-control-label::before {
  background-color: white;
}

.was-validated .dark-edition .has-white .is-focused .custom-control-input:valid:focus~.custom-control-label::before,
.dark-edition .has-white .is-focused .custom-control-input.is-valid:focus~.custom-control-label::before {
  box-shadow: 0 0 0 1px #fafafa, 0 0 0 0.2rem rgba(255, 255, 255, 0.25);
}

.was-validated .dark-edition .has-white .is-focused .custom-file-input:valid~.custom-file-label,
.dark-edition .has-white .is-focused .custom-file-input.is-valid~.custom-file-label {
  border-color: #fff;
}

.was-validated .dark-edition .has-white .is-focused .custom-file-input:valid~.custom-file-label::before,
.dark-edition .has-white .is-focused .custom-file-input.is-valid~.custom-file-label::before {
  border-color: inherit;
}

.was-validated .dark-edition .has-white .is-focused .custom-file-input:valid~.valid-feedback,
.was-validated .dark-edition .has-white .is-focused .custom-file-input:valid~.valid-tooltip,
.dark-edition .has-white .is-focused .custom-file-input.is-valid~.valid-feedback,
.dark-edition .has-white .is-focused .custom-file-input.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-white .is-focused .custom-file-input:valid:focus~.custom-file-label,
.dark-edition .has-white .is-focused .custom-file-input.is-valid:focus~.custom-file-label {
  box-shadow: 0 0 0 0.2rem rgba(255, 255, 255, 0.25);
}

.dark-edition .has-white .is-focused [class^='bmd-label'],
.dark-edition .has-white .is-focused [class*=' bmd-label'] {
  color: #fff;
}

.dark-edition .has-white .is-focused .bmd-label-placeholder {
  color: #fff;
}

.dark-edition .has-white .is-focused .form-controllllll {
  border-color: #fff;
}

.dark-edition .has-white .is-focused .bmd-help {
  color: #555555;
}

.dark-edition .has-white .form-controllllll:focus {
  color: #fff;
}

.dark-edition .has-warning [class^='bmd-label'],
.dark-edition .has-warning [class*=' bmd-label'] {
  color: #ff9800;
}

.dark-edition .has-warning .form-controllllll,
.is-focused .dark-edition .has-warning .form-controllllll {
  background-image: linear-gradient(to top, #ff9800 2px, rgba(255, 152, 0, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px);
}

.dark-edition .has-warning .form-controllllll:invalid {
  background-image: linear-gradient(to top, #f44336 2px, rgba(244, 67, 54, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px);
}

.dark-edition .has-warning .form-controllllll:read-only {
  background-image: linear-gradient(to top, #d2d2d2 1px, rgba(210, 210, 210, 0) 1px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px);
}

fieldset[disabled][disabled] .dark-edition .has-warning .form-controllllll,
.dark-edition .has-warning .form-controllllll.disabled,
.dark-edition .has-warning .form-controllllll:disabled,
.dark-edition .has-warning .form-controllllll[disabled] {
  background-image: linear-gradient(to right, rgba(180, 180, 180, 0.1) 0%, rgba(180, 180, 180, 0.1) 30%, transparent 30%, transparent 100%);
  background-repeat: repeat-x;
  background-size: 3px 1px;
}

.dark-edition .has-warning .form-controllllll.form-controllllll-success,
.is-focused .dark-edition .has-warning .form-controllllll.form-controllllll-success {
  background-image: linear-gradient(to top, #ff9800 2px, rgba(255, 152, 0, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px), "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2MTIgNzkyIj48cGF0aCBmaWxsPSIjNWNiODVjIiBkPSJNMjMzLjggNjEwYy0xMy4zIDAtMjYtNi0zNC0xNi44TDkwLjUgNDQ4LjhDNzYuMyA0MzAgODAgNDAzLjMgOTguOCAzODljMTguOC0xNC4yIDQ1LjUtMTAuNCA1OS44IDguNGw3MiA5NUw0NTEuMyAyNDJjMTIuNS0yMCAzOC44LTI2LjIgNTguOC0xMy43IDIwIDEyLjQgMjYgMzguNyAxMy43IDU4LjhMMjcwIDU5MGMtNy40IDEyLTIwLjIgMTkuNC0zNC4zIDIwaC0yeiIvPjwvc3ZnPg==";
}

.dark-edition .has-warning .form-controllllll.form-controllllll-warning,
.is-focused .dark-edition .has-warning .form-controllllll.form-controllllll-warning {
  background-image: linear-gradient(to top, #ff9800 2px, rgba(255, 152, 0, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px), "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2MTIgNzkyIj48cGF0aCBmaWxsPSIjZjBhZDRlIiBkPSJNNjAzIDY0MC4ybC0yNzguNS01MDljLTMuOC02LjYtMTAuOC0xMC42LTE4LjUtMTAuNnMtMTQuNyA0LTE4LjUgMTAuNkw5IDY0MC4yYy0zLjcgNi41LTMuNiAxNC40LjIgMjAuOCAzLjggNi41IDEwLjggMTAuNCAxOC4zIDEwLjRoNTU3YzcuNiAwIDE0LjYtNCAxOC40LTEwLjQgMy41LTYuNCAzLjYtMTQuNCAwLTIwLjh6bS0yNjYuNC0zMGgtNjEuMlY1NDloNjEuMnY2MS4yem0wLTEwN2gtNjEuMlYzMDRoNjEuMnYxOTl6Ii8+PC9zdmc+";
}

.dark-edition .has-warning .form-controllllll.form-controllllll-danger,
.is-focused .dark-edition .has-warning .form-controllllll.form-controllllll-danger {
  background-image: linear-gradient(to top, #ff9800 2px, rgba(255, 152, 0, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px), "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2MTIgNzkyIj48cGF0aCBmaWxsPSIjZDk1MzRmIiBkPSJNNDQ3IDU0NC40Yy0xNC40IDE0LjQtMzcuNiAxNC40LTUyIDBsLTg5LTkyLjctODkgOTIuN2MtMTQuNSAxNC40LTM3LjcgMTQuNC01MiAwLTE0LjQtMTQuNC0xNC40LTM3LjYgMC01Mmw5Mi40LTk2LjMtOTIuNC05Ni4zYy0xNC40LTE0LjQtMTQuNC0zNy42IDAtNTJzMzcuNi0xNC4zIDUyIDBsODkgOTIuOCA4OS4yLTkyLjdjMTQuNC0xNC40IDM3LjYtMTQuNCA1MiAwIDE0LjMgMTQuNCAxNC4zIDM3LjYgMCA1MkwzNTQuNiAzOTZsOTIuNCA5Ni40YzE0LjQgMTQuNCAxNC40IDM3LjYgMCA1MnoiLz48L3N2Zz4=";
}

.dark-edition .has-warning .is-focused .valid-feedback {
  display: none;
  width: 100%;
  margin-top: 0.25rem;
  font-size: 80%;
  color: #ff9800;
}

.dark-edition .has-warning .is-focused .valid-tooltip {
  position: absolute;
  top: 100%;
  z-index: 5;
  display: none;
  max-width: 100%;
  padding: .5rem;
  margin-top: .1rem;
  font-size: .875rem;
  line-height: 1;
  color: #fff;
  background-color: rgba(255, 152, 0, 0.8);
  border-radius: .2rem;
}

.was-validated .dark-edition .has-warning .is-focused .form-controllllll:valid,
.dark-edition .has-warning .is-focused .form-controllllll.is-valid,
.was-validated .dark-edition .has-warning .is-focused .custom-select:valid,
.dark-edition .has-warning .is-focused .custom-select.is-valid {
  border-color: #ff9800;
}

.was-validated .dark-edition .has-warning .is-focused .form-controllllll:valid:focus,
.dark-edition .has-warning .is-focused .form-controllllll.is-valid:focus,
.was-validated .dark-edition .has-warning .is-focused .custom-select:valid:focus,
.dark-edition .has-warning .is-focused .custom-select.is-valid:focus {
  border-color: #ff9800;
  box-shadow: 0 0 0 0.2rem rgba(255, 152, 0, 0.25);
}

.was-validated .dark-edition .has-warning .is-focused .form-controllllll:valid~.valid-feedback,
.was-validated .dark-edition .has-warning .is-focused .form-controllllll:valid~.valid-tooltip,
.dark-edition .has-warning .is-focused .form-controllllll.is-valid~.valid-feedback,
.dark-edition .has-warning .is-focused .form-controllllll.is-valid~.valid-tooltip,
.was-validated .dark-edition .has-warning .is-focused .custom-select:valid~.valid-feedback,
.was-validated .dark-edition .has-warning .is-focused .custom-select:valid~.valid-tooltip,
.dark-edition .has-warning .is-focused .custom-select.is-valid~.valid-feedback,
.dark-edition .has-warning .is-focused .custom-select.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-warning .is-focused .form-check-input:valid~.form-check-label,
.dark-edition .has-warning .is-focused .form-check-input.is-valid~.form-check-label {
  color: #ff9800;
}

.was-validated .dark-edition .has-warning .is-focused .form-check-input:valid~.valid-feedback,
.was-validated .dark-edition .has-warning .is-focused .form-check-input:valid~.valid-tooltip,
.dark-edition .has-warning .is-focused .form-check-input.is-valid~.valid-feedback,
.dark-edition .has-warning .is-focused .form-check-input.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-warning .is-focused .custom-control-input:valid~.custom-control-label,
.dark-edition .has-warning .is-focused .custom-control-input.is-valid~.custom-control-label {
  color: #ff9800;
}

.was-validated .dark-edition .has-warning .is-focused .custom-control-input:valid~.custom-control-label::before,
.dark-edition .has-warning .is-focused .custom-control-input.is-valid~.custom-control-label::before {
  background-color: #ffcc80;
}

.was-validated .dark-edition .has-warning .is-focused .custom-control-input:valid~.valid-feedback,
.was-validated .dark-edition .has-warning .is-focused .custom-control-input:valid~.valid-tooltip,
.dark-edition .has-warning .is-focused .custom-control-input.is-valid~.valid-feedback,
.dark-edition .has-warning .is-focused .custom-control-input.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-warning .is-focused .custom-control-input:valid:checked~.custom-control-label::before,
.dark-edition .has-warning .is-focused .custom-control-input.is-valid:checked~.custom-control-label::before {
  background-color: #ffad33;
}

.was-validated .dark-edition .has-warning .is-focused .custom-control-input:valid:focus~.custom-control-label::before,
.dark-edition .has-warning .is-focused .custom-control-input.is-valid:focus~.custom-control-label::before {
  box-shadow: 0 0 0 1px #fafafa, 0 0 0 0.2rem rgba(255, 152, 0, 0.25);
}

.was-validated .dark-edition .has-warning .is-focused .custom-file-input:valid~.custom-file-label,
.dark-edition .has-warning .is-focused .custom-file-input.is-valid~.custom-file-label {
  border-color: #ff9800;
}

.was-validated .dark-edition .has-warning .is-focused .custom-file-input:valid~.custom-file-label::before,
.dark-edition .has-warning .is-focused .custom-file-input.is-valid~.custom-file-label::before {
  border-color: inherit;
}

.was-validated .dark-edition .has-warning .is-focused .custom-file-input:valid~.valid-feedback,
.was-validated .dark-edition .has-warning .is-focused .custom-file-input:valid~.valid-tooltip,
.dark-edition .has-warning .is-focused .custom-file-input.is-valid~.valid-feedback,
.dark-edition .has-warning .is-focused .custom-file-input.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-warning .is-focused .custom-file-input:valid:focus~.custom-file-label,
.dark-edition .has-warning .is-focused .custom-file-input.is-valid:focus~.custom-file-label {
  box-shadow: 0 0 0 0.2rem rgba(255, 152, 0, 0.25);
}

.dark-edition .has-warning .is-focused [class^='bmd-label'],
.dark-edition .has-warning .is-focused [class*=' bmd-label'] {
  color: #ff9800;
}

.dark-edition .has-warning .is-focused .bmd-label-placeholder {
  color: #ff9800;
}

.dark-edition .has-warning .is-focused .form-controllllll {
  border-color: #ff9800;
}

.dark-edition .has-warning .is-focused .bmd-help {
  color: #555555;
}

.dark-edition .has-danger [class^='bmd-label'],
.dark-edition .has-danger [class*=' bmd-label'],
.dark-edition .has-error [class^='bmd-label'],
.dark-edition .has-error [class*=' bmd-label'] {
  color: #f44336;
}

.dark-edition .has-danger .form-controllllll,
.is-focused .dark-edition .has-danger .form-controllllll,
.dark-edition .has-error .form-controllllll,
.is-focused .dark-edition .has-error .form-controllllll {
  background-image: linear-gradient(to top, #f44336 2px, rgba(244, 67, 54, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px);
}

.dark-edition .has-danger .form-controllllll:invalid,
.dark-edition .has-error .form-controllllll:invalid {
  background-image: linear-gradient(to top, #f44336 2px, rgba(244, 67, 54, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px);
}

.dark-edition .has-danger .form-controllllll:read-only,
.dark-edition .has-error .form-controllllll:read-only {
  background-image: linear-gradient(to top, #d2d2d2 1px, rgba(210, 210, 210, 0) 1px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px);
}

fieldset[disabled][disabled] .dark-edition .has-danger .form-controllllll,
.dark-edition .has-danger .form-controllllll.disabled,
.dark-edition .has-danger .form-controllllll:disabled,
.dark-edition .has-danger .form-controllllll[disabled],
fieldset[disabled][disabled] .dark-edition .has-error .form-controllllll,
.dark-edition .has-error .form-controllllll.disabled,
.dark-edition .has-error .form-controllllll:disabled,
.dark-edition .has-error .form-controllllll[disabled] {
  background-image: linear-gradient(to right, rgba(180, 180, 180, 0.1) 0%, rgba(180, 180, 180, 0.1) 30%, transparent 30%, transparent 100%);
  background-repeat: repeat-x;
  background-size: 3px 1px;
}

.dark-edition .has-danger .form-controllllll.form-controllllll-success,
.is-focused .dark-edition .has-danger .form-controllllll.form-controllllll-success,
.dark-edition .has-error .form-controllllll.form-controllllll-success,
.is-focused .dark-edition .has-error .form-controllllll.form-controllllll-success {
  background-image: linear-gradient(to top, #f44336 2px, rgba(244, 67, 54, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px), "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2MTIgNzkyIj48cGF0aCBmaWxsPSIjNWNiODVjIiBkPSJNMjMzLjggNjEwYy0xMy4zIDAtMjYtNi0zNC0xNi44TDkwLjUgNDQ4LjhDNzYuMyA0MzAgODAgNDAzLjMgOTguOCAzODljMTguOC0xNC4yIDQ1LjUtMTAuNCA1OS44IDguNGw3MiA5NUw0NTEuMyAyNDJjMTIuNS0yMCAzOC44LTI2LjIgNTguOC0xMy43IDIwIDEyLjQgMjYgMzguNyAxMy43IDU4LjhMMjcwIDU5MGMtNy40IDEyLTIwLjIgMTkuNC0zNC4zIDIwaC0yeiIvPjwvc3ZnPg==";
}

.dark-edition .has-danger .form-controllllll.form-controllllll-warning,
.is-focused .dark-edition .has-danger .form-controllllll.form-controllllll-warning,
.dark-edition .has-error .form-controllllll.form-controllllll-warning,
.is-focused .dark-edition .has-error .form-controllllll.form-controllllll-warning {
  background-image: linear-gradient(to top, #f44336 2px, rgba(244, 67, 54, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px), "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2MTIgNzkyIj48cGF0aCBmaWxsPSIjZjBhZDRlIiBkPSJNNjAzIDY0MC4ybC0yNzguNS01MDljLTMuOC02LjYtMTAuOC0xMC42LTE4LjUtMTAuNnMtMTQuNyA0LTE4LjUgMTAuNkw5IDY0MC4yYy0zLjcgNi41LTMuNiAxNC40LjIgMjAuOCAzLjggNi41IDEwLjggMTAuNCAxOC4zIDEwLjRoNTU3YzcuNiAwIDE0LjYtNCAxOC40LTEwLjQgMy41LTYuNCAzLjYtMTQuNCAwLTIwLjh6bS0yNjYuNC0zMGgtNjEuMlY1NDloNjEuMnY2MS4yem0wLTEwN2gtNjEuMlYzMDRoNjEuMnYxOTl6Ii8+PC9zdmc+";
}

.dark-edition .has-danger .form-controllllll.form-controllllll-danger,
.is-focused .dark-edition .has-danger .form-controllllll.form-controllllll-danger,
.dark-edition .has-error .form-controllllll.form-controllllll-danger,
.is-focused .dark-edition .has-error .form-controllllll.form-controllllll-danger {
  background-image: linear-gradient(to top, #f44336 2px, rgba(244, 67, 54, 0) 2px), linear-gradient(to top, rgba(180, 180, 180, 0.1) 1px, rgba(180, 180, 180, 0) 1px), "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2MTIgNzkyIj48cGF0aCBmaWxsPSIjZDk1MzRmIiBkPSJNNDQ3IDU0NC40Yy0xNC40IDE0LjQtMzcuNiAxNC40LTUyIDBsLTg5LTkyLjctODkgOTIuN2MtMTQuNSAxNC40LTM3LjcgMTQuNC01MiAwLTE0LjQtMTQuNC0xNC40LTM3LjYgMC01Mmw5Mi40LTk2LjMtOTIuNC05Ni4zYy0xNC40LTE0LjQtMTQuNC0zNy42IDAtNTJzMzcuNi0xNC4zIDUyIDBsODkgOTIuOCA4OS4yLTkyLjdjMTQuNC0xNC40IDM3LjYtMTQuNCA1MiAwIDE0LjMgMTQuNCAxNC4zIDM3LjYgMCA1MkwzNTQuNiAzOTZsOTIuNCA5Ni40YzE0LjQgMTQuNCAxNC40IDM3LjYgMCA1MnoiLz48L3N2Zz4=";
}

.dark-edition .has-danger .is-focused .valid-feedback,
.dark-edition .has-error .is-focused .valid-feedback {
  display: none;
  width: 100%;
  margin-top: 0.25rem;
  font-size: 80%;
  color: #f44336;
}

.dark-edition .has-danger .is-focused .valid-tooltip,
.dark-edition .has-error .is-focused .valid-tooltip {
  position: absolute;
  top: 100%;
  z-index: 5;
  display: none;
  max-width: 100%;
  padding: .5rem;
  margin-top: .1rem;
  font-size: .875rem;
  line-height: 1;
  color: #fff;
  background-color: rgba(244, 67, 54, 0.8);
  border-radius: .2rem;
}

.was-validated .dark-edition .has-danger .is-focused .form-controllllll:valid,
.dark-edition .has-danger .is-focused .form-controllllll.is-valid,
.was-validated .dark-edition .has-danger .is-focused .custom-select:valid,
.dark-edition .has-danger .is-focused .custom-select.is-valid,
.was-validated .dark-edition .has-error .is-focused .form-controllllll:valid,
.dark-edition .has-error .is-focused .form-controllllll.is-valid,
.was-validated .dark-edition .has-error .is-focused .custom-select:valid,
.dark-edition .has-error .is-focused .custom-select.is-valid {
  border-color: #f44336;
}

.was-validated .dark-edition .has-danger .is-focused .form-controllllll:valid:focus,
.dark-edition .has-danger .is-focused .form-controllllll.is-valid:focus,
.was-validated .dark-edition .has-danger .is-focused .custom-select:valid:focus,
.dark-edition .has-danger .is-focused .custom-select.is-valid:focus,
.was-validated .dark-edition .has-error .is-focused .form-controllllll:valid:focus,
.dark-edition .has-error .is-focused .form-controllllll.is-valid:focus,
.was-validated .dark-edition .has-error .is-focused .custom-select:valid:focus,
.dark-edition .has-error .is-focused .custom-select.is-valid:focus {
  border-color: #f44336;
  box-shadow: 0 0 0 0.2rem rgba(244, 67, 54, 0.25);
}

.was-validated .dark-edition .has-danger .is-focused .form-controllllll:valid~.valid-feedback,
.was-validated .dark-edition .has-danger .is-focused .form-controllllll:valid~.valid-tooltip,
.dark-edition .has-danger .is-focused .form-controllllll.is-valid~.valid-feedback,
.dark-edition .has-danger .is-focused .form-controllllll.is-valid~.valid-tooltip,
.was-validated .dark-edition .has-danger .is-focused .custom-select:valid~.valid-feedback,
.was-validated .dark-edition .has-danger .is-focused .custom-select:valid~.valid-tooltip,
.dark-edition .has-danger .is-focused .custom-select.is-valid~.valid-feedback,
.dark-edition .has-danger .is-focused .custom-select.is-valid~.valid-tooltip,
.was-validated .dark-edition .has-error .is-focused .form-controllllll:valid~.valid-feedback,
.was-validated .dark-edition .has-error .is-focused .form-controllllll:valid~.valid-tooltip,
.dark-edition .has-error .is-focused .form-controllllll.is-valid~.valid-feedback,
.dark-edition .has-error .is-focused .form-controllllll.is-valid~.valid-tooltip,
.was-validated .dark-edition .has-error .is-focused .custom-select:valid~.valid-feedback,
.was-validated .dark-edition .has-error .is-focused .custom-select:valid~.valid-tooltip,
.dark-edition .has-error .is-focused .custom-select.is-valid~.valid-feedback,
.dark-edition .has-error .is-focused .custom-select.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-danger .is-focused .form-check-input:valid~.form-check-label,
.dark-edition .has-danger .is-focused .form-check-input.is-valid~.form-check-label,
.was-validated .dark-edition .has-error .is-focused .form-check-input:valid~.form-check-label,
.dark-edition .has-error .is-focused .form-check-input.is-valid~.form-check-label {
  color: #f44336;
}

.was-validated .dark-edition .has-danger .is-focused .form-check-input:valid~.valid-feedback,
.was-validated .dark-edition .has-danger .is-focused .form-check-input:valid~.valid-tooltip,
.dark-edition .has-danger .is-focused .form-check-input.is-valid~.valid-feedback,
.dark-edition .has-danger .is-focused .form-check-input.is-valid~.valid-tooltip,
.was-validated .dark-edition .has-error .is-focused .form-check-input:valid~.valid-feedback,
.was-validated .dark-edition .has-error .is-focused .form-check-input:valid~.valid-tooltip,
.dark-edition .has-error .is-focused .form-check-input.is-valid~.valid-feedback,
.dark-edition .has-error .is-focused .form-check-input.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-danger .is-focused .custom-control-input:valid~.custom-control-label,
.dark-edition .has-danger .is-focused .custom-control-input.is-valid~.custom-control-label,
.was-validated .dark-edition .has-error .is-focused .custom-control-input:valid~.custom-control-label,
.dark-edition .has-error .is-focused .custom-control-input.is-valid~.custom-control-label {
  color: #f44336;
}

.was-validated .dark-edition .has-danger .is-focused .custom-control-input:valid~.custom-control-label::before,
.dark-edition .has-danger .is-focused .custom-control-input.is-valid~.custom-control-label::before,
.was-validated .dark-edition .has-error .is-focused .custom-control-input:valid~.custom-control-label::before,
.dark-edition .has-error .is-focused .custom-control-input.is-valid~.custom-control-label::before {
  background-color: #fbb4af;
}

.was-validated .dark-edition .has-danger .is-focused .custom-control-input:valid~.valid-feedback,
.was-validated .dark-edition .has-danger .is-focused .custom-control-input:valid~.valid-tooltip,
.dark-edition .has-danger .is-focused .custom-control-input.is-valid~.valid-feedback,
.dark-edition .has-danger .is-focused .custom-control-input.is-valid~.valid-tooltip,
.was-validated .dark-edition .has-error .is-focused .custom-control-input:valid~.valid-feedback,
.was-validated .dark-edition .has-error .is-focused .custom-control-input:valid~.valid-tooltip,
.dark-edition .has-error .is-focused .custom-control-input.is-valid~.valid-feedback,
.dark-edition .has-error .is-focused .custom-control-input.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-danger .is-focused .custom-control-input:valid:checked~.custom-control-label::before,
.dark-edition .has-danger .is-focused .custom-control-input.is-valid:checked~.custom-control-label::before,
.was-validated .dark-edition .has-error .is-focused .custom-control-input:valid:checked~.custom-control-label::before,
.dark-edition .has-error .is-focused .custom-control-input.is-valid:checked~.custom-control-label::before {
  background-color: #f77066;
}

.was-validated .dark-edition .has-danger .is-focused .custom-control-input:valid:focus~.custom-control-label::before,
.dark-edition .has-danger .is-focused .custom-control-input.is-valid:focus~.custom-control-label::before,
.was-validated .dark-edition .has-error .is-focused .custom-control-input:valid:focus~.custom-control-label::before,
.dark-edition .has-error .is-focused .custom-control-input.is-valid:focus~.custom-control-label::before {
  box-shadow: 0 0 0 1px #fafafa, 0 0 0 0.2rem rgba(244, 67, 54, 0.25);
}

.was-validated .dark-edition .has-danger .is-focused .custom-file-input:valid~.custom-file-label,
.dark-edition .has-danger .is-focused .custom-file-input.is-valid~.custom-file-label,
.was-validated .dark-edition .has-error .is-focused .custom-file-input:valid~.custom-file-label,
.dark-edition .has-error .is-focused .custom-file-input.is-valid~.custom-file-label {
  border-color: #f44336;
}

.was-validated .dark-edition .has-danger .is-focused .custom-file-input:valid~.custom-file-label::before,
.dark-edition .has-danger .is-focused .custom-file-input.is-valid~.custom-file-label::before,
.was-validated .dark-edition .has-error .is-focused .custom-file-input:valid~.custom-file-label::before,
.dark-edition .has-error .is-focused .custom-file-input.is-valid~.custom-file-label::before {
  border-color: inherit;
}

.was-validated .dark-edition .has-danger .is-focused .custom-file-input:valid~.valid-feedback,
.was-validated .dark-edition .has-danger .is-focused .custom-file-input:valid~.valid-tooltip,
.dark-edition .has-danger .is-focused .custom-file-input.is-valid~.valid-feedback,
.dark-edition .has-danger .is-focused .custom-file-input.is-valid~.valid-tooltip,
.was-validated .dark-edition .has-error .is-focused .custom-file-input:valid~.valid-feedback,
.was-validated .dark-edition .has-error .is-focused .custom-file-input:valid~.valid-tooltip,
.dark-edition .has-error .is-focused .custom-file-input.is-valid~.valid-feedback,
.dark-edition .has-error .is-focused .custom-file-input.is-valid~.valid-tooltip {
  display: block;
}

.was-validated .dark-edition .has-danger .is-focused .custom-file-input:valid:focus~.custom-file-label,
.dark-edition .has-danger .is-focused .custom-file-input.is-valid:focus~.custom-file-label,
.was-validated .dark-edition .has-error .is-focused .custom-file-input:valid:focus~.custom-file-label,
.dark-edition .has-error .is-focused .custom-file-input.is-valid:focus~.custom-file-label {
  box-shadow: 0 0 0 0.2rem rgba(244, 67, 54, 0.25);
}

.dark-edition .has-danger .is-focused [class^='bmd-label'],
.dark-edition .has-danger .is-focused [class*=' bmd-label'],
.dark-edition .has-error .is-focused [class^='bmd-label'],
.dark-edition .has-error .is-focused [class*=' bmd-label'] {
  color: #f44336;
}

.dark-edition .has-danger .is-focused .bmd-label-placeholder,
.dark-edition .has-error .is-focused .bmd-label-placeholder {
  color: #f44336;
}

.dark-edition .has-danger .is-focused .form-controllllll,
.dark-edition .has-error .is-focused .form-controllllll {
  border-color: #f44336;
}

.dark-edition .has-danger .is-focused .bmd-help,
.dark-edition .has-error .is-focused .bmd-help {
  color: #555555;
}

.dark-edition .fixed-plugin {
  background: rgba(47, 57, 84, 0.6);
}

.dark-edition .fixed-plugin .fa-cog {
  color: #8b92a9;
}

.dark-edition .fixed-plugin .dropdown .dropdown-menu {
  background-color: #2f3954;
}

.dark-edition .fixed-plugin .dropdown .dropdown-menu:after {
  border-left-color: #2f3954;
}

.dark-edition .fixed-plugin .dropdown .dropdown-menu li.adjustments-line {
  border-bottom-color: rgba(180, 180, 180, 0.1);
}

.dark-edition .fixed-plugin .dropdown .dropdown-menu li>a.img-holder {
  border-color: #303c58;
}

.dark-edition .fixed-plugin .dropdown .dropdown-menu li>a.img-holder:hover,
.dark-edition .fixed-plugin .dropdown .dropdown-menu li.active>a.img-holder {
  border-color: #596d9c;
}

.dark-edition .fixed-plugin li.header-title {
  color: #8b92a9;
}

.dark-edition .fixed-plugin .badge {
  border: 2px solid #303c58;
}

.dark-edition .fixed-plugin .badge.badge-purple {
  background-color: #913f9e;
}

.dark-edition .fixed-plugin .badge.badge-azure {
  background-color: #029eb1;
}

.dark-edition .fixed-plugin .badge.badge-green {
  background-color: #288c6c;
}

.dark-edition .fixed-plugin .badge.badge-warning {
  background-color: #f5700c;
}

.dark-edition .fixed-plugin .badge.badge-danger {
  background-color: #d22824;
}

.dark-edition .fixed-plugin .badge.active,
.dark-edition .fixed-plugin .badge:hover {
  border-color: #596d9c;
}

.dark-edition .dropdown:not(.show-dropdown) .dropdown-menu .dropdown-item,
.dark-edition .dropdown:not(.show-dropdown) .dropdown-menu li>a {
  color: #606477;
  -webkit-transition: all 150ms linear, color, box-shadow 0ms;
  -moz-transition: all 150ms linear, color, box-shadow 0ms;
  -o-transition: all 150ms linear, color, box-shadow 0ms;
  -ms-transition: all 150ms linear, color, box-shadow 0ms;
  transition: all 150ms linear, color, box-shadow 0ms;
}

.dark-edition .dropdown:not(.show-dropdown) .dropdown-menu .dropdown-item:hover,
.dark-edition .dropdown:not(.show-dropdown) .dropdown-menu .dropdown-item:focus,
.dark-edition .dropdown:not(.show-dropdown) .dropdown-menu .dropdown-item:active,
.dark-edition .dropdown:not(.show-dropdown) .dropdown-menu li>a:hover,
.dark-edition .dropdown:not(.show-dropdown) .dropdown-menu li>a:focus,
.dark-edition .dropdown:not(.show-dropdown) .dropdown-menu li>a:active {
  box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(156, 39, 176, 0.4);
  background: linear-gradient(60deg, #7b1fa2, #913f9e);
  color: #fff;
}

.dark-edition .text-primary {
  color: #a84ab7 !important;
}

.dark-edition .text-muted,
.dark-edition .bmd-help {
  color: #9095a2 !important;
}

.dark-edition .text-info {
  color: #029eb1 !important;
}

.dark-edition .text-success {
  color: #288c6c !important;
}

.dark-edition .text-warning {
  color: #f5700c !important;
}

.dark-edition .text-danger {
  color: #d22824 !important;
}

.dark-edition .tim-typo {
  color: #606477;
}

.dark-edition .tim-typo .tim-note,
.dark-edition .places-buttons .card-title {
  color: #8b92a9;
}

.dark-edition .places-buttons .card-title .category {
  color: #606477;
}

.dark-edition .list-group-item {
  color: #8b92a9;
}

.dark-edition a {
  color: #fff;
}

.dark-edition.offline-doc .brand .description {
  color: #8b92a9;
}

.dark-edition.offline-doc .navbar.navbar-transparent .navbar-brand {
  color: #a9afbbd1;
}

.dark-edition.offline-doc .page-header:after {
  background-color: rgba(26, 32, 53, 0.7);
}

.dark-edition .alert.alert-info {
  background: linear-gradient(60deg, #029eb1, #25b1c3);
  background-color: unset;
}

.dark-edition .alert.alert-warning {
  background: linear-gradient(60deg, #f5700c, #ff9800);
  background-color: unset;
}

.dark-edition .alert.alert-success {
  background: linear-gradient(60deg, #288c6c, #4ea752);
  background-color: unset;
}

.dark-edition .alert.alert-danger {
  background: linear-gradient(60deg, #d22824, #da3a36);
  background-color: unset;
}

.dark-edition .alert.alert-primary {
  background: linear-gradient(60deg, #7b1fa2, #913f9e);
  background-color: unset;
}

.dark-edition .alert .close {
  outline: 0;
  text-shadow: none;
  bottom: 0;
  top: 0 !important;
  margin-top: 0 !important;
}

.dark-edition .alert .close:hover,
.dark-edition .alert .close:focus {
  color: #fff;
  opacity: 1;
}

@keyframes heartbeat {
  0% {
    transform: scale(0.75);
  }

  20% {
    transform: scale(1);
  }

  40% {
    transform: scale(0.75);
  }

  60% {
    transform: scale(1);
  }

  80% {
    transform: scale(0.75);
  }

  100% {
    transform: scale(0.75);
  }
}


		  </style>
</head>

<body >
  
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="<?php echo base_url(); ?>__statics/school/img/sidebar-3.jpg">
   
      <div class="logo">
        <a href="<?php echo site_url("school"); ?>" class="simple-text logo-normal">
        <img src="<?php echo base_url(); ?>__statics/img/logocolor.png" style="width:250px">
        
        </a>
      </div>
        <?php $this->load->view("school/page_menu"); ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php $this->load->view("school/navbar"); ?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
		
		  
          <div id="kontendefault">
		     
			  <?php $this->load->view($konten); ?>
		                
          </div>
        </div>
      </div>
      <footer class="footer">
        <div class="container-fluid">
         
          <div class="copyright float-right">
            &copy; <?php echo date("Y"); ?> Copyright by
           
            <a href="https://elearning.kemenag.go.id" target="_blank"></a> Kementerian Agama Republik Indonesia
          </div>
        </div>
      </footer>
    </div>
  </div>
  
  <!--   Core JS Files   -->

  <script src="<?php echo base_url(); ?>__statics/school/js/core/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/school/js/core/bootstrap-material-design.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/school/js/plugins/perfect-scrollbar.jquery.min.js"></script>
 
  <script src="<?php echo base_url(); ?>__statics/school/js/material-dashboard.min1036.js?v=2.1.1" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="<?php echo base_url(); ?>__statics/school/demo/demo.js"></script>
  <script src="<?php echo base_url(); ?>__statics/js/alertify/alertify.js"></script>
<script src="<?php echo base_url(); ?>__statics/js/jquery.blockui.min.js"></script>
<script src="<?php echo base_url(); ?>__statics/js/datepicker/datepicker.js" ></script>
<script src="<?php echo base_url(); ?>__statics/js/bootstrap-toastr/toastr.min.js"></script>
<script src="<?php echo base_url(); ?>__statics/js/datetimepicker/jquery.datetimepicker.full.js"></script>
  <script>
    $(document).ready(function() {
      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>
  <!-- Sharrre libray -->
  
   <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();

    });
	
	 $('.menu-toggle').on('click', function (e) {
            var $this = $(this);
			
            var $content = $this.next();

            if ($($this.parents('ul')[0]).hasClass('list')) {
                var $not = $(e.target).hasClass('menu-toggle') ? e.target : $(e.target).parents('.menu-toggle');

                $.each($('.menu-toggle.toggled').not($not).next(), function (i, val) {
                    if ($(val).is(':visible')) {
                        $(val).prev().toggleClass('toggled');
                        $(val).slideUp();
                    }
                });
            }

            $this.toggleClass('toggled');
            $content.slideToggle(320);
        });
		
		$(document).on("click","#keluar",function(){
	  
				  var base_url = "<?php echo base_url(); ?>";
				  
				  alertify.confirm("Anda akan keluar dari aplikasi, apakah anda yakin?",function(){
					  
						   $.post("<?php echo site_url("school/logout"); ?>",function(data){
							   
							   location.href= base_url;
							   
						   });
					});
				  
				  
				  
			  })
			  
		$(document).off("click",".menuajax").on("click",".menuajax",function (event, messages) {
	           event.preventDefault()
			   var url = $(this).attr("href");
			   var title = $(this).attr("title");
			  
			 
				   
				   
			    $("li").siblings().removeClass('active');
			    $(this).parent().addClass('active');
			    
				
			  	  
			   $("#kontendefault").html('....');
		      loading();
			  $.post(url,{ajax:"yes"},function(data){
				  
				
				  history.replaceState(title, title, url);
				  $('title').html(title);
				
				  $("#kontendefault").html(data);
				 
				 
				    jQuery.unblockUI({ });
				 
			  })
		  })
  </script>
</body>

</html>
