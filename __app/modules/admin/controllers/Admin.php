<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		  isLogin();
		  $this->load->model('M_admin','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('admin/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Dashboard Administrator  ";

		 $data['categorie_xAxis'] = "";
								$data['json_anggota']    = "";
								$pie   					 = "";
								$data['title']           = "Persentase Jumlah Permadrasah ";
								$jenis = $this->db->get("jabatan")->result();
									
									 foreach($jenis as $row){									   
									   
										$jml = $this->db->query("SELECT count(id) as jml from pegawai where jabatan_id='{$row->id}'")->row();
										$pm = $jml->jml;
										$data['json_anggota'] .=",".$pm;
										$data["categorie_xAxis"] .=",'".$row->nama."";
										$tempo = array("INDEXES"=>$row->nama,"Jumlah"=>$pm);
										
										$pie          .=",['".$row->nama."',".(($pm/count($jenis))*100)."]";
										
										
									   $grid[] = $tempo;
									   
									 }
								
								
									 $data['statistik'] = " Pendaftar SNPDB  ";
									 $data['header']    = $data['statistik'];
									 $data['categorie_xAxis'] = "Jumlah Pendaftar ";

									$data['json_pie_chart']  =  substr($pie,1);
									$data['json_anggota']    = substr($data['json_anggota'], 1);
									
									$data['grid'] = $grid;
								


	     if(!empty($ajax)){
					    
			 $this->load->view('page_default',$data);
		
		 }else{
			 
			
		     $data['konten'] = "page_default";
			 
			 $this->_template($data);
		 }
	

	}
	

	public function kegiatan()
	{




		$ajax            = $this->input->get_post("ajax", true);

		$data['title']   = "Data Jadwal Kegiatan ";
		if (!empty($ajax)) {

			$this->load->view('page', $data);
		} else {


			$data['konten'] = "page";

			$this->_template($data);
		}
	}

	 
	public function grid()
	{


		$iTotalRecords = $this->m->grid(false)->num_rows();

		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);

		$records = array();
		$records["data"] = array();

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;

		$datagrid = $this->m->grid(true)->result_array();


		$i = ($iDisplayStart + 1);
		foreach ($datagrid as $val) {

			$anggota = "<ol>";
			$dataAnggota = explode(",",$val['anggota']);
			foreach ($dataAnggota as $r) {
				$anggota .= "<li>" . $this->Reff->get_kondisi(array("id" => $r), "pegawai_simpeg", "nama") . "</li>";
			}

			$anggota .= "</ol>";

			$satkerData = "<ol>";
			$satker =  explode(",", $val['satker_id']);
			foreach ($satker as $r) {
				$satkerData .= "<li>" . $this->Reff->get_kondisi(array("id" => $r), "satker", "nama") . "</li>";
			}

			$satkerData .= "</ol>";

			$approval ="Ditolak";
			if ($val['status'] == 1) {
					$approval = '<span class="badge bg-info"><i class="bi bi-exclamation-octagon me-1"></i> Belum disetujui Itjen </span>';
			}

			$itjen  = $this->db->query("SELECT count(id) as jml from kegiatan where status=1 AND id='{$val['id']}' and id IN(select kegiatan_id from tr_status where groups_id=14)")->row();
			if ($itjen->jml >0) {

				$approval = '<span class="badge bg-success"><i class="bi bi-check-circle me-"></i>  Disetujui Itjen, Menunggu disetujui Sekretaris</span>';
			}

			$sekretaris  = $this->db->query("SELECT count(id) as jml from kegiatan where status=1 AND id='{$val['id']}' and id IN(select kegiatan_id from tr_status where groups_id=16)")->row();
			if ($sekretaris->jml >0) {

				$approval = '<span class="badge bg-success"><i class="bi bi-check-circle me-"></i>  Disetujui Sekretaris, Menunggu Anggaran tersedia diperencanaan</span>';
			}

			$perencanaan  = $this->db->query("SELECT count(id) as jml from kegiatan where status=1 AND id='{$val['id']}' and id IN(select kegiatan_id from tr_status where groups_id=6)")->row();
			if ($perencanaan->jml >0) {

				$approval = '<span class="badge bg-success"><i class="bi bi-check-circle me-"></i>  Disetujui Perencanaan, Menunggu Penerbitan surat tugas oleh Bagian Umum</span>';
			}

			$BagUmum  = $this->db->query("SELECT count(id) as jml from kegiatan where status=1 and no_surat IS NOT NULL AND approval=1 and  id='{$val['id']}'")->row();
			if ($perencanaan->jml >0) {

				$approval = '<span class="badge bg-success"><i class="bi bi-check-circle me-"></i>  Diterbitkan Surat Tugas oleh Bagian Umum</span>';
			}
			




			$no = $i++;
			$records["data"][] = array(
				$no,


				$approval,
				$this->Reff->get_kondisi(array("id" => $val['jenis']), "jenis_sub", "nama"),

				$val['judul'],
				$this->Reff->get_kondisi(array("id" => $val['provinsi_id']), "provinsi", "nama"),
				$this->Reff->get_kondisi(array("id" => $val['kota_id']), "kota", "nama"),
				$satkerData,
				$this->Reff->formattanggalstring($val['tgl_mulai']) . " - " . $this->Reff->formattanggalstring($val['tgl_selesai']),

				$this->Reff->get_kondisi(array("id" => $val['penanggung_jawab']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['penanggung_jawab']), "pegawai_simpeg", "nama"),
				$this->Reff->get_kondisi(array("id" => $val['pengendali_mutu']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['pengendali_mutu']), "pegawai_simpeg", "nama"),
				$this->Reff->get_kondisi(array("id" => $val['pengendali_teknis']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['pengendali_teknis']), "pegawai_simpeg", "nama"),

				$this->Reff->get_kondisi(array("id" => $val['ketua']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['ketua']), "pegawai_simpeg", "nama"),
				$anggota






			);
		}

		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;

		echo json_encode($records);
	}
	
 
	
	public function logout(){
		     $this->session->sess_destroy();
			 
		
			echo "yes";
		 
	 }

	 // pengaturan 
	 public function pengaturan()
	{




		$ajax            = $this->input->get_post("ajax", true);

		$data['title']   = "Pengaturan GoogleDrive ";
		if (!empty($ajax)) {

			$this->load->view('page_google', $data);
		} else {


			$data['konten'] = "page_google";

			$this->_template($data);
		}
	}
	public function authapisave()
	{
		$service = new Google_Drive();
		$aksestoken = $this->input->get_post("userauth");
		$service->fetchAccessTokenWithAuthCode($aksestoken);
		redirect("/schoolmadrasah/settingDrive");
	}
	public function hapusGdrive()
	{
		$service = new Google_Drive();
		$service->hapusToken();
		$this->db->truncate("tm_gdrive");
		redirect("/schoolmadrasah/settingDrive");
	}

	

}
