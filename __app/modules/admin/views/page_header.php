<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Dashboard - Administrator</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo base_url(); ?>__statics/img/logo.png" rel="icon">

  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/fa/css/all.min.css">
  <!-- Vendor CSS Files -->
  
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/simple-datatables/style.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo base_url(); ?>__statics/admin/assets/css/style.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/bootstrap-toastr/toastr.min.css"/>

  <script src="<?php echo base_url(); ?>__statics/js/jquery.min.js"></script>
  <script> var base_url="<?php echo base_url(); ?>"; </script>
  <script src="<?php echo base_url(); ?>__statics/js/proses.js"></script>

   <link href="<?php echo base_url(); ?>__statics/js/datatable/button.css" rel="stylesheet" type="text/css"/>
   <link href="<?php echo base_url(); ?>__statics/js/datatable/datatables.min.css" rel="stylesheet" type="text/css"/>
   <script src="<?php echo base_url(); ?>__statics/js/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>__statics/js/datatable/button.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>__statics/js/datatable/buttons.print.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>__statics/js/datatable/jszip.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>__statics/js/datatable/pdf.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>__statics/js/datatable/font.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>__statics/js/datatable/html5.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>__statics/js/datatable/colvis.js" type="text/javascript"></script>  <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/js/datetimepicker/jquery.datetimepicker.css">
	  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/alert/alert.css">
		<script src="<?php echo base_url(); ?>__statics/js/alert/alert.js"></script>
		<link href="<?php echo base_url(); ?>__statics/js/alertify/css/alertify.css" rel="stylesheet">
		<script src="<?php echo base_url(); ?>__statics/js/alertify/alertify.js"></script>

		<script src="<?php echo base_url(); ?>__statics/js/grafik/grafiksatu.js"></script>
		<script src="<?php echo base_url(); ?>__statics/js/grafik/download.js"></script>
		<script src="<?php echo base_url(); ?>__statics/js/grafik/3d.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>

   
  

</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top d-flex align-items-center">

    <div class="d-flex align-items-center justify-content-between">
      <a href="<?php echo base_url(); ?>" class="logo d-flex align-items-center">
        <img src="<?php echo base_url(); ?>__statics/img/logo.png" alt="">
        <span class="d-none d-lg-block" style="font-size:14px">Sistem Informasi Pengawasan Internal Terintegrasi</span>
      </a>
      <i class="bi bi-list toggle-sidebar-btn"></i>
    </div><!-- End Logo -->

    <?php $this->load->view("admin/navbar"); ?>

  </header><!-- End Header -->

  <!-- ======= Sidebar ======= -->
   <?php $this->load->view("admin/page_menu"); ?>

   <div id="kontendefault"> <?php $this->load->view(isset($konten) ? $konten : "page_default"); ?></div>

  <!--
  <footer id="footer" class="footer">
    <div class="copyright">
      &copy; Copyright <strong><span><?php echo date("Y"); ?> - Kementerian Agama RI </span></strong>. All Rights Reserved
    </div>
  
  </footer> -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <script src="<?php echo base_url(); ?>__statics/js/jquery.blockui.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/js/bootstrap-toastr/toastr.min.js"></script>

  <!-- Vendor JS Files -->
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/apexcharts/apexcharts.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/chart.js/chart.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/echarts/echarts.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/quill/quill.min.js"></script>
   <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/tinymce/tinymce.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="<?php echo base_url(); ?>__statics/admin/assets/js/main.js"></script>

  <script>
   
		
		$(document).on("click","#keluar",function(){
	  
				  var base_url = "<?php echo base_url(); ?>";
				  
				  alertify.confirm("Anda akan keluar dari aplikasi, apakah anda yakin?",function(){
					  
						   $.post("<?php echo site_url("school/logout"); ?>",function(data){
							   
							   location.href= base_url;
							   
						   });
					});
				  
				  
				  
			  });
			  
		$(document).off("click",".menuajax").on("click",".menuajax",function (event, messages) {
	           event.preventDefault();
			   var url = $(this).attr("href");
			   var title = $(this).attr("title");
			  
			 
				   
				   
			    $("li").siblings().removeClass('active');
			    $(this).parent().addClass('active');
			    
				
			  	  
			   $("#kontendefault").html('....');
		      loading();
			  $.post(url,{ajax:"yes"},function(data){
				  
				
				  history.replaceState(title, title, url);
				  $('title').html(title);
				
				  $("#kontendefault").html(data);
				 
				 
				    jQuery.unblockUI({ });
				 
			  })
		  })
  </script>

</body>

</html>