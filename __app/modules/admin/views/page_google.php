

<main id="main" class="main">

<div class="pagetitle">
  <h1>Pengaturan Akun Google Drive</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.html">Home</a></li>
      <li class="breadcrumb-item active">Pengaturan Akun Google Drive</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section dashboard">
  <div class="row">

    <!-- Left side columns -->
    <div class="col-lg-12">
      <div class="row">

      <?php            
				
$service = new Google_Drive();   
if(!$service->statusApi()){
	$authlink = $service->createAuthUrl();
	$buttonlink =  "<a href=\"" . $authlink . "\" target=\"_googleAuthLogin\" class=\"btn btn-danger \">\n   <i class=\"fa fa-ban\"></i>\n Get Token GDrive </a>";
}else{
	$buttonlink = "";
}  


echo "  <div class=\"row\">";
  if($buttonlink==""){
	  $dataakun = $service->getOuthData();
	  if(empty($dataakun)){
		  $dataakun['picture'] = "https://www.gstatic.com/images/branding/product/1x/drive_48dp.png";
		  $dataakun['name'] = "Google Akun";
	  }
	echo "<div class=\"col-md-12\">
	  <div class=\"card\">
	  <div class=\"card-header card-header-success\">
	  <h4 class=\"card-title\">Pengaturan Upload Goggle Drive</h4>
	  <p class=\"card-category\"> Form ini digunakan untuk mengaktifkan upload gdrive  </p>
	  </div>	                  
	  <div class=\"card-body\">
	  <div class=\"col-md-12\">
	  <div class=\"row\"> 
	  <div class=\"col-md-3\">
	  <div  style=\"max-width: 130px;max-height: 130px;margin: 0px auto 0;border-radius: 50%; 
    overflow: hidden;padding: 0;box-shadow: 0 16px 38px -12px rgba(0,0,0,.56), 0 4px 25px 0 rgba(0,0,0,.12), 0 8px 10px -5px rgba(0,0,0,.2);\">
                  <a ><img  style=\"width: 100%;height: auto;\" src=\"".$dataakun['picture']."\" >
                  </a>
               </div>
				  <br><h4 style=\"text-align: center;\">".$dataakun['name']."</h4>
                 </div>";	  
				 
	  echo "<div class=\"col-md-9\"><p class=\"card-category\"> Upload Google Drive sudah aktif  </p>";
	  echo "<p class=\"card-category\">  Semua dokumen akan terintegrasi dengan akun google </b> </p>";
	  echo "<a href=\"" . site_url("admin/hapusGdrive") . "\" class=\"btn btn-danger \" onclick=\"return confirm('Jika Anda mengganti akun, Anda akan kehilangan akses terhadap akun gsuite unlimited default dari pusat, apakah Anda yakin ?') \" >\n   <i class=\"fa fa-trash\"></i>\n Nonaktifkan Penyimpanan Google Drive</a>";   
	 
	echo"               </div>\r\n</div>\r\n</div>\r\n  </div>\r\n              </div>\r\n            </div></div>\r\n         \r\n         \r\n  "; 
	  
  }else{
  echo "<div class=\"col-md-12\">
	  <div class=\"card\">
	  <div class=\"card-header card-header-success\">
	  <h4 class=\"card-title\">Pengaturan Upload GDrive</h4>
	  <p class=\"card-category\"> Form ini digunakan untuk mengaktifkan upload gdrive  </p>
	  </div>
	  <div class=\"card-body\"> ";
	  echo "<p > Untuk mendapatkan token, silahkan klik  <b> Get Token GDrive </b>. Kemudian pilih akun gmail yang akan di integrasikan. Setelah muncul kode token, Isikan ke form berikut dan Klik Simpan </p>";
echo "  <form  method=\"post\" action=\"";
echo site_url("admin/authapisave");
echo "\">\r\n                    <div class=\"row\">\r\n                      <div class=\"col-md-12\">\r\n                        <div class=\"form-group\">\r\n 
<input type=\"text\" class=\"form-control\" name=\"userauth\" placeholder=\"Masukkan Token Gdive\"  title=\"Masukkan NSM \" data-placement=\"right\">
\r\n                    \r\n                    \r\n                    \r\n                        </div>\r\n                      </div>\r\n                     \r\n                      \r\n                    </div>\r\n                      \r\n            \r\n                  \r\n".$buttonlink."                    <button type=\"submit\" class=\"btn btn-success pull-right\">Simpan Token</button>\r\n                    <div class=\"clearfix\"></div>\r\n                  </form>\r\n ";
echo"               </div>\r\n              </div>\r\n            </div></div>\r\n         \r\n         \r\n  "; 
  }

					?>




      </div>
     
    </div>

   
  </div>
</section>

</main>