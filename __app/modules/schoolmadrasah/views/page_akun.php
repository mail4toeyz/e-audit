  <div class="col-md-8">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Pengaturan Password Akun Anda </h4>
                  <p class="card-category">Perbaharui Password Akun Anda secara berkala</p>
                </div>
                <div class="card-body">
                    <form id="simpantanparespon" method="post" url="<?php echo site_url("schoolmadrasah/saveakun"); ?>">
				  
				  
                     <div class="row">
                       <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Password Baru</label>
                          <input type="password" class="form-control" name="password" >
                        </div>
                      </div>
                    </div>
					 <div class="row">
                       <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Konfirmasi Password</label>
                          <input type="password" class="form-control" name="passwordc" >
                        </div>
                      </div>
                    </div>
                    
				
                  
                    <button type="submit" class="btn btn-success pull-right">Perbaharui Password Akun Anda</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card card-profile">
                <div class="card-avatar">
                  <a href="#pablo">
                    <img class="img" src="<?php echo base_url(); ?>__statics/img/elearning.png" />
                  </a>
                </div>
                <div class="card-body">
                 
                  <h4 class="card-title">Pengaturan Password Akun Anda</h4>
                  <p class="card-description">
                     Update akun password Anda secara berkala demi keamanan ketika Anda login
                  </p>
                 
                </div>
              </div>
            </div>