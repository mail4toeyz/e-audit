<?php

class M_jadwal extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}



	// public function grid($paging)
	// {


	// 	$this->db->select("*");
	// 	$this->db->from('kegiatan k');
	// 	$this->db->join('tr_status ts', 'ts.kegiatan_id = k.id', 'left');


	// 	if ($paging == true) {
	// 		$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
	// 		$this->db->order_by("k.id", "ASC");
	// 	}



	// 	return $this->db->get();
	// }

	public function grid($paging)
	{
		$user = $this->db->get_where("users", array("id" => $_SESSION['idAdmin']))->row();
		$pegawai = $this->db->get_where("pegawai_simpeg", array("id" => $user->pegawai_id))->row();
		$unitkerja = $this->Reff->get_kondisi(array("kode" => $pegawai->KODE_SATKER_3), "unit_kerja", "id");

		$query = 'select * from (
			select * from kegiatan k
			left join ( 
			SELECT kegiatan_id, groups_id, status as status_approval, notes, tanggal_approve
			FROM tr_status AS a
			WHERE tanggal_approve = (
				SELECT MAX(tanggal_approve)
				FROM tr_status AS b
				WHERE a.kegiatan_id = b.kegiatan_id
			)
			) s
			on s.kegiatan_id = k.id
			) x where unit_kerja_id = "'.$unitkerja.'"';

		if ($paging == true) {
			$query .= 'ORDER BY id ASC 
			LIMIT ' . $_REQUEST['length'] . ' OFFSET ' . $_REQUEST['start'];
		}
		return $this->db->query($query);
	}
}
