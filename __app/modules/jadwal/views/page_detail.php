<style>
    .timeline {
    border-left: 3px solid #727cf5;
    border-bottom-right-radius: 4px;
    border-top-right-radius: 4px;
    background: rgba(114, 124, 245, 0.09);
    margin: 0 auto;
    letter-spacing: 0.2px;
    position: relative;
    line-height: 1.4em;
    font-size: 1.03em;
    padding: 50px;
    list-style: none;
    text-align: left;
    max-width: 50%;
}

@media (max-width: 767px) {
    .timeline {
        max-width: 98%;
        padding: 25px;
    }
}

.timeline h1 {
    font-weight: 300;
    font-size: 1.4em;
}

.timeline h2,
.timeline h3 {
    font-weight: 600;
    font-size: 1rem;
    margin-bottom: 10px;
}

.timeline .event {
    border-bottom: 1px dashed #e8ebf1;
    padding-bottom: 25px;
    margin-bottom: 25px;
    position: relative;
}

@media (max-width: 767px) {
    .timeline .event {
        padding-top: 30px;
    }
}

.timeline .event:last-of-type {
    padding-bottom: 0;
    margin-bottom: 0;
    border: none;
}

.timeline .event:before,
.timeline .event:after {
    position: absolute;
    display: block;
    top: 0;
}

.timeline .event:before {
    left: -207px;
    content: attr(data-date);
    text-align: right;
    font-weight: 100;
    font-size: 0.9em;
    min-width: 120px;
}

@media (max-width: 767px) {
    .timeline .event:before {
        left: 0px;
        text-align: left;
    }
}

.timeline .event:after {
    -webkit-box-shadow: 0 0 0 3px #727cf5;
    box-shadow: 0 0 0 3px #727cf5;
    left: -55.8px;
    background: #fff;
    border-radius: 50%;
    height: 9px;
    width: 9px;
    content: "";
    top: 5px;
}

@media (max-width: 767px) {
    .timeline .event:after {
        left: -31.8px;
    }
}

.rtl .timeline {
    border-left: 0;
    text-align: right;
    border-bottom-right-radius: 0;
    border-top-right-radius: 0;
    border-bottom-left-radius: 4px;
    border-top-left-radius: 4px;
    border-right: 3px solid #727cf5;
}

.rtl .timeline .event::before {
    left: 0;
    right: -170px;
}

.rtl .timeline .event::after {
    left: 0;
    right: -55.8px;
}
</style>
<main id="main" class="main">

  <div class="pagetitle">
    <h1><?php echo $title; ?></h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><?php echo app(); ?></a></li>

        <li class="breadcrumb-item active"><?php echo $title; ?></li>
      </ol>
    </nav>
  </div><!-- End Page Title -->
    <section class="section">
        <div class="row">
            <div class="col-lg-12">

            <div class="card">
                    <div class="card-body">

                    <!-- Bordered Tabs Justified -->
                    <!-- <ul class="nav nav-tabs nav-tabs-bordered d-flex" id="borderedTabJustified" role="tablist">
                        <li class="nav-item flex-fill" role="presentation">
                        <button class="nav-link w-100 active" id="home-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-home" type="button" role="tab" aria-controls="home" aria-selected="true">Data Usulan Kegiatan</button>
                        </li>
                        <li class="nav-item flex-fill" role="presentation">
                        <button class="nav-link w-100" id="profile-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Usulan Tim Kegiatan </button>
                        </li>
                        <li class="nav-item flex-fill" role="presentation">
                        <button class="nav-link w-100" id="contact-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Timeline</button>
                        </li>
                    </ul>
                    <div class="tab-content pt-2" id="borderedTabJustifiedContent">
                        <div class="tab-pane fade show active" id="bordered-justified-home" role="tabpanel" aria-labelledby="home-tab"> -->

                        <div class="row p-2 ">
                            <div class="col-md-6">
                            <table class="table">

                                <tr>
                                <td> Jenis Kegiatan </td>
                                <td>:</td>
                                <td> <?php echo $this->Reff->get_kondisi(array("id" => $data->jenis), "jenis_sub", "nama"); ?> </td>
                                </tr>

                                <tr>
                                <td> Nama Kegiatan </td>
                                <td>:</td>
                                <td> <?php echo $data->judul; ?> </td>
                                </tr>

                                <tr>
                                <td> Wilayah </td>
                                <td>:</td>
                                <td> <?php echo $this->Reff->get_kondisi(array("id" => $data->provinsi_id), "provinsi", "nama"); ?> - <?php echo $this->Reff->get_kondisi(array("id" => $data->kota_id), "kota", "nama"); ?></td>
                                </tr>
                            </table>

                            </div>

                            <div class="col-md-6">
                            <table class="table">
                                <tr>
                                <td> Periode </td>
                                <td>:</td>
                                <td> <?php echo $this->Reff->formattanggalstring($data->tgl_mulai); ?> - <?php echo $this->Reff->formattanggalstring($data->tgl_selesai); ?></td>
                                </tr>
                                <tr>
                                <td> Satuan Kerja </td>
                                <td>:</td>
                                <td>
                                    <ol start="1">
                                    <?php
                                    $satker_id = explode(",", $data->satker_id);
                                    foreach ($satker_id as $satker) {

                                    ?><li><?php echo $this->Reff->get_kondisi(array("id" => $satker), "satker", "nama"); ?> - <?php echo $this->Reff->get_kondisi(array("id" => $satker), "satker", "kode"); ?></li><?php
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                    ?>

                                    </ol>


                                </td>
                                </tr>


                            </table>

                            </div>
                        </div>
                        </div>
                        <!-- <div class="tab-pane fade" id="bordered-justified-profile" role="tabpanel" aria-labelledby="profile-tab">
 -->
                        <div class="table-responsive" id="load_sbm">
                            <table class="table table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                <th rowspan="2"> # </th>
                                <th rowspan="2"> Nama </th>
                                <th rowspan="2"> Gol </th>
                                <th rowspan="2"> Jabatan </th>
                                <th rowspan="2"> HP </th>
                                <th colspan="5"> Perhitungan SBM </th>
                                <th rowspan="2"> Anggaran </th>
                                </tr>

                                <tr>
                                <th>BST</th>
                                <th>Transport</th>
                                <th>Hotel</th>
                                <th>UH</th>
                                <th>Representatif</th>
                                </tr>

                            </thead>
                            <tbody>

                                <?php
                                $pegawaiJabatan = $this->db->query("SELECT * from tr_kegiatanJabatan where kegiatan_id='" . $data->id . "'")->result();
                                $no = 1;
                                $total = 0;
                                foreach ($pegawaiJabatan as $r) {
                                $dataPegawai = $this->db->get_where("pegawai_simpeg", array("id" => $r->pejabat_id))->row();
                                $total = $total + $r->anggaran;
                                ?>
                                <tr>
                                    <td> <?php echo $no++; ?></td>
                                    <td> <?php echo $dataPegawai->NAMA_LENGKAP; ?></td>

                                    <td> <?php echo $dataPegawai->GOL_RUANG; ?></td>
                                    <td> <?php echo $dataPegawai->SATKER_3; //echo $this->Reff->get_kondisi(array("id" => $r->jabatan), "jabatan", "nama"); 
                                        ?></td>
                                    <td>
                                        <?php echo $r->waktu; ?>
                                    </td>
                                    <td>
                                        <?php echo $this->Reff->formatuang2($r->bst); ?>
                                    </td>
                                    <td>
                                        <?php echo $this->Reff->formatuang2($r->transport); ?>
                                    </td>
                                    <td>
                                        <?php echo $this->Reff->formatuang2($r->hotel); ?>
                                    </td>
                                    <td>
                                        <?php echo $this->Reff->formatuang2($r->uh); ?>
                                    </td>
                                    <td>
                                        <?php echo $this->Reff->formatuang2($r->representatif); ?>
                                    </td>
                                    <td>
                                        <?php echo $this->Reff->formatuang2($r->anggaran); ?>
                                    </td>
                                </tr>

                                <?php
                                }
                                ?>

                                <tr style="font-weight:bold">
                                <td colspan="10"> Total Anggaran </th>
                                <td> <?php echo $this->Reff->formatuang2($total); ?> </th>
                                </tr>

                            </tbody>
                            </table>
                        </div>




                        <!-- </div>
                        <div class="tab-pane fade" id="bordered-justified-contact" role="tabpanel" aria-labelledby="contact-tab"> -->
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div id="content">
                                                <h3>Timeline</h3>
                                                <ul class="timeline">
                                                <?php
                                                $tl = $this->db->query("SELECT ts.*, g.nama from tr_status ts
                                                join groups g 
                                                on g.id = ts.groups_id
                                                where kegiatan_id='{$data->id}'")->result();
                                                // $a=array("#41516C","#FBCA3E","#E24A68","#1B5F8C","#4CADAD");

                                                    foreach ($tl as $r) {
                                                        if ($row->status == 0){
                                                            $status = '<span class="badge bg-info"><i class="bi bi-exclamation-octagon me-1"></i> Dibuat </span>';
                                                        } else if ($row->status == 1){
                                                            $status = '<span class="badge bg-success"><i class="bi bi-exclamation-octagon me-1"></i> Disetujui </span>';
                                                        }else{
                                                            $status = '<span class="badge bg-danger"><i class="bi bi-exclamation-octagon me-1"></i> Ditolak </span>';
                                                        }

                                                        echo '<li class="event" data-date="'.$r->tanggal_approve.'">
                                                        '.$status.'
                                                        <h3><b>'.$r->nama.'</b></h3>
                                                        <p>'.$r->notes.'</p>
                                                    </li>';
                                                    }
                                                ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- </div> -->


                    <!-- </div> -->
                    <!-- End Bordered Tabs Justified -->

                    </div>
                </div>

            </div>
        </div>
    </section>
</main>