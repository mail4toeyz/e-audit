<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Perencanaan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata("is_login")) {

			echo $this->Reff->sessionhabis();
			exit();
		}
		$this->load->model('M_dashboard', 'm');
	}

	function _template($data)
	{
		$this->load->view('perencanaan/page_header', $data);
	}

	public function index()
	{


		$ajax            = $this->input->get_post("ajax", true);

		$data['title']   = "Dashboard Perencanaan  ";

		$data['categorie_xAxis'] = "";
		$data['json_anggota']    = "";
		$pie   					 = "";
		$data['title']           = "Persentase Jumlah Permadrasah ";
		$jenis = $this->db->get("jabatan")->result();

		foreach ($jenis as $row) {

			$jml = $this->db->query("SELECT count(id) as jml from pegawai where jabatan_id='{$row->id}'")->row();
			$pm = $jml->jml;
			$data['json_anggota'] .= "," . $pm;
			$data["categorie_xAxis"] .= ",'" . $row->nama . "";
			$tempo = array("INDEXES" => $row->nama, "Jumlah" => $pm);

			$pie          .= ",['" . $row->nama . "'," . (($pm / count($jenis)) * 100) . "]";


			$grid[] = $tempo;
		}


		$data['statistik'] = " Pendaftar SNPDB  ";
		$data['header']    = $data['statistik'];
		$data['categorie_xAxis'] = "Jumlah Pendaftar ";

		$data['json_pie_chart']  =  substr($pie, 1);
		$data['json_anggota']    = substr($data['json_anggota'], 1);

		$data['grid'] = $grid;
		if (!empty($ajax)) {

			$this->load->view('page_default', $data);
		} else {


			$data['konten'] = "page_default";

			$this->_template($data);
		}
	}



	public function penugasan()
	{




		$ajax            = $this->input->get_post("ajax", true);

		$data['title']   = "Data Usulan Penugasan ";
		if (!empty($ajax)) {

			$this->load->view('penugasan/page', $data);
		} else {


			$data['konten'] = "penugasan/page";

			$this->_template($data);
		}
	}

	public function gridPenugasan()
	{


		$iTotalRecords = $this->m->grid(false)->num_rows();

		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);

		$records = array();
		$records["data"] = array();

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;

		$datagrid = $this->m->grid(true)->result_array();


		$i = ($iDisplayStart + 1);
		foreach ($datagrid as $val) {

			$anggota = "<ol>";
			$dataAnggota = explode(",",$val['anggota']);
			foreach ($dataAnggota as $r) {
				$anggota .= "<li>" . $this->Reff->get_kondisi(array("id" => $r), "pegawai_simpeg", "nama") . "</li>";
			}

			$anggota .= "</ol>";

			$satkerData = "<ol>";
			$satker =  explode(",", $val['satker_id']);
			foreach ($satker as $r) {
				$satkerData .= "<li>" . $this->Reff->get_kondisi(array("id" => $r), "satker", "nama") . "</li>";
			}

			$satkerData .= "</ol>";




			if ($val['status_approval'] == 1 && $val['groups_id'] == 16) {
				$approval = '<button type="button" class="btn btn-info ubah" datanya="' . $val['id'] . '" urlnya="' . site_url("perencanaan/approval") . '" ><i class="bi bi-exclamation-octagon me-1"></i> Perlu Tindakan </button> ';
			}

			if ($val['status_approval'] == 1 && $val['groups_id'] == 6) {
				$approval = '<button type="button" class="btn btn-primary ubah" datanya="' . $val['id'] . '" urlnya="' . site_url("perencanaan/approval") . '" ><i class="bi bi-check-circle me-"></i> Disetujui </button> ';
			}

			if ($val['status_approval'] == 2 && $val['groups_id'] == 6) {
				$approval = '<button type="button" class="btn btn-warning ubah" datanya="' . $val['id'] . '" urlnya="' . site_url("perencanaan/approval") . '" ><i class="bi bi-exclamation-octagon me-1"></i> Ditolak </button> ';
			}



			$no = $i++;
			$records["data"][] = array(
				$no,
				$approval,
				'<button type="button" class="btn btnMdl" id="btn_' . $val['id'] . '"><i class="bi bi-clipboard-check bi--sm"></i></button>'.
				$this->Reff->get_kondisi(array("id" => $val['jenis']), "jenis_sub", "nama"),
				$this->Reff->get_kondisi(array("id" => $val['unit_kerja_id']), "unit_kerja", "nama"),
				$val['judul'],
				$this->Reff->get_kondisi(array("id" => $val['provinsi_id']), "provinsi", "nama"),
				$this->Reff->get_kondisi(array("id" => $val['kota_id']), "kota", "nama"),
				$satkerData,
				$this->Reff->formattanggalstring($val['tgl_mulai']) . " - " . $this->Reff->formattanggalstring($val['tgl_selesai']),

				$this->Reff->get_kondisi(array("id" => $val['penanggung_jawab']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['penanggung_jawab']), "pegawai_simpeg", "nama"),
				$this->Reff->get_kondisi(array("id" => $val['pengendali_mutu']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['pengendali_mutu']), "pegawai_simpeg", "nama"),
				$this->Reff->get_kondisi(array("id" => $val['pengendali_teknis']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['pengendali_teknis']), "pegawai_simpeg", "nama"),

				$this->Reff->get_kondisi(array("id" => $val['ketua']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['ketua']), "pegawai_simpeg", "nama"),
				$anggota







			);
		}

		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;

		echo json_encode($records);
	}


	public function approval()
	{

		$id = $this->input->get_post("id");
		$data = array();
		if (!empty($id)) {

			$data['data']  = $this->Reff->get_where("kegiatan_status", array("kegiatan_id" => $id));
			$data['title']  =  "Approval Detail " . $data['data']->judul;
		}
		$this->load->view("penugasan/detail", $data);
	}

	public function save()
	{

		$this->form_validation->set_message('required', '{field} Wajib diisi. ');
		$this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
		$this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
		$this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');

		$config = array(
			array('field' => 'f[approval_perencanaan]', 'label' => 'Status Approval ', 'rules' => 'trim|required'),
		);

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == true) {

			$id = $this->input->get_post("id", true);
			$f  = xssArray($this->input->get_post("f", true));

			// $this->db->where("id", $id);
			// $this->db->update("kegiatan", $f);

			$id = $this->input->get_post("id", true);
			$approval = $this->input->get_post("f[approval_perencanaan]", true);
			$note = $this->input->get_post("f[approval_catatan]", true);
			$mak = $this->input->get_post("f[no_mak]", true);

			$this->db->set("status", $approval);
			$this->db->set("notes", $note);
			$this->db->set("kegiatan_id", $id);
			$this->db->set("groups_id", $_SESSION['group_id']);
			$this->db->insert("tr_status");

			$this->db->where("id", $id);
			$this->db->set("no_mak", $mak);
			$this->db->update("kegiatan");

			echo "Data Berhasil disimpan";
		} else {
			header('Content-Type: application/json');
			echo json_encode(array('error' => true, 'message' => validation_errors()));
		}
	}
}
