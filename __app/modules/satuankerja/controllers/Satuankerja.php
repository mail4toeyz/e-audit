<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Satuankerja extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 
		  isLogin();
		  $this->load->model('M_referensi','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('admin/page_header',$data);	
	}
		
	public function data($param)
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['data']	  = $this->db->get_where("satker_kategori",array("id"=>base64_decode($param)))->row();
		 $data['title']   = $data['data']->nama;
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			    
			   
				$no = $i++;
				$records["data"][] = array(
					$no,
			
              	
				
					$val['kode'],
					$val['password'],
					$val['nama'],
					
					' 
					<div class="btn-group" role="group">
					<button type="button" class="btn btn-outline-primary ubahmodal" datanya="'.$val['id'].'" urlnya="'.site_url("satuankerja/form").'" data-bs-toggle="modal" data-bs-target="#basicModal" target="#loadform">
                                    <i class="fa fa-pencil"></i>
                      </button>
					  <button type="button"  class="btn btn-outline-danger btn-sm hapus" datanya="'.$val['id'].'" urlnya="'.site_url("satuankerja/hapus").'">
                                    <i class="fa fa-trash"></i>
					  </button>
					  </div>
					 
					  '
				  
				    

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	

	

	public function form(){
		
		$id 	  = $this->input->get_post("id");
		$kategori = $this->input->get_post("kategori");
		$data = array();
		$data['kategori'] = $kategori;
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("satker",array("id"=>$id));
			   
		   }
		$this->load->view("form",$data);
		
	}
	
	
	
	public function save(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
			    $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
    
		 
			
				$config = array(
				    
				   
				    array('field' => 'f[kode]', 'label' => 'Kode Satker ', 'rules' => 'trim|required'),
				    array('field' => 'f[nama]', 'label' => 'Nama   ', 'rules' => 'trim|required'),
				    array('field' => 'f[password]', 'label' => 'Buat Password   ', 'rules' => 'trim|required'),
				    array('field' => 'f[kategori]', 'label' => 'Kategori   ', 'rules' => 'trim|required'),
				  	   
			
				   
				  
				   
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
				
				
				 
				
							 if(empty($id)){
								
								 
								   $this->db->insert("satker",$f);
								   echo "Data Berhasil disimpan";	
								 
								
								
							 }else{
								$this->db->where("id",$id);
								$this->db->update("satker",$f);
								echo "Data Berhasil disimpan";	
								 
							 }
							 
						
							
						
		
		
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}

	public function hapus(){
		
		$id = $this->input->get_post("id",true);
		
		$this->db->delete("satker",array("id"=>$id));
		echo "sukses";
	}
	

	 
}
