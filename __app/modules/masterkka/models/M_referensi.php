<?php

class M_referensi extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
    }

    
	public function grid($paging){
      
		
	    $keyword      = $this->input->post("keyword",true);
		$subjenis      = $this->input->post("subjenis",true);
		$kategori_id      = $this->input->post("kategori_id",true);
	 
		$this->db->select("*");
        $this->db->from('tm_kka');
	
		if(!empty($kategori_id)){  $this->db->where("kategori_id",$kategori_id);    }
        
		if(!empty($subjenis)){  $this->db->where("subjenis",$subjenis);    }
	    if(!empty($keyword)){  $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'");    }
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 
					 $this->db->order_by("kategori_id","ASC");
					 $this->db->order_by("status_lembaga","ASC");
					 $this->db->order_by("id","ASC");
					
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
}
