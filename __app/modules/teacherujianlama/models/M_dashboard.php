<?php

class M_dashboard extends CI_Model {
 
    public function __construct() {
        parent::__construct();
    }

    
	public function cek($table,$kolom,$id){
		
		 
		 $cek = $this->db->query("select $kolom from $table where $kolom !='' and $id='".$_SESSION['tmyayasan_id']."'")->num_rows();
		 if($cek==0){
			 return "0";
			 
		 }else{
			 return "100";
		 }
		
	}
	public function cek_selesai(){
		
		 
		 $cek = $this->db->query("select id from tm_madrasah where tmyayasan_id='".$_SESSION['tmyayasan_id']."' and status !='0'")->num_rows();
		 if($cek==0){
			 return "0";
			 
		 }else{
			 return "100";
		 }
		
	}
	
	public function cek_berkas(){
		
		 
		 $cek1 = $this->db->query("select count(id) as jml from tm_persyaratan")->row();
		 $cek2 = $this->db->query("select count(id) as jml from tr_persyaratan where tmyayasan_id='".$_SESSION['tmyayasan_id']."'")->row();
		 
		 
		return round(($cek2->jml *100)/$cek1->jml);
		
	}
	
	
	public function s_proposal(){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
	
		$this->db->where("id",$_SESSION['tmmadrasah_id']);
		$this->db->update("dmadrasahs",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return $tmmadrasah_id;
				   
				} else {
						
					$this->db->trans_commit();
					 return $tmmadrasah_id;
					
				}
		
		
		
	}
	
	public function s_dataumum(){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
		$form2  			  = $this->security->xss_clean($this->input->get_post("u"));
		$trorganisasi_id  			  = $this->security->xss_clean($this->input->get_post("trorganisasi_id"));
	
		$this->db->where("id",$_SESSION['tmmadrasah_id']);
		$this->db->update("dmadrasahs",$form);
		
		
		             if(empty($trorganisasi_id)){
                       
					    $this->db->set("tmmadrasah_id",$_SESSION['tmmadrasah_id']);
						$this->db->insert("tr_organisasi",$form2);

					 }else{
                        $this->db->where("tmmadrasah_id",$_SESSION['tmmadrasah_id']);
						$this->db->update("tr_organisasi",$form2);

					 }					 
           				  
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return true;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
	
	public function m_pengurus($paging){
 
		$this->db->select("*");
        $this->db->from('tr_pengurus');
        $this->db->where('tmmadrasah_id',$_SESSION['tmmadrasah_id']);
	
		$kolom    = array("0"=>"id","1"=>"nama","2"=>"jabatan","3"=>"no_telepon","4"=>"id");
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by($kolom[$_REQUEST['order'][0]['column']],"ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	public function i_pengurus(){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("p"));
	
		
		$this->db->set("tmmadrasah_id",$_SESSION['tmmadrasah_id']);
		
		$this->db->insert("tr_pengurus",$form);
		                                                
																
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return true;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
	public function u_pengurus($id){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("p"));
	
		
		
	
		$this->db->where("id",$id);
		$this->db->update("tr_pengurus",$form);
		                                                
																
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return true;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
	
	public function m_ptk($paging){
 
		$this->db->select("*");
        $this->db->from('tr_ptk');
        $this->db->where('tmmadrasah_id',$_SESSION['tmmadrasah_id']);
	
		$kolom    = array("0"=>"kategori","1"=>"nama","2"=>"tgl_lahir","3"=>"kategori","4"=>"id");
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by($kolom[$_REQUEST['order'][0]['column']],"ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	public function i_ptk(){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("p"));
	
		
		$this->db->set("tmmadrasah_id",$_SESSION['tmmadrasah_id']);
		
		$this->db->insert("tr_ptk",$form);
		                                                
																
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return true;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
	public function u_ptk($id){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("p"));
	
		
		
	
		$this->db->where("id",$id);
		$this->db->update("tr_ptk",$form);
		                                                
																
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return true;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
	
	public function yesp($link){
		
		$d = $this->Acuan_model->get_where("dmadrasahs",array("id"=>$_SESSION['tmmadrasah_id']));
		  if($link > 7){
			   if($link ==9 or $link == 11){
				   
				   if(empty($d->nik_pendiri)){

							return "2";
				   }else{

                            return "1";
				   }				   
				 
				 }else if($link ==8){
					 
					 
					  if(empty($d->latarbelakang)){

							return "2";
				   }else{

                            return "1";
				   }		
					 
				   
			   }else if($link ==10){
				    $peng = $this->Acuan_model->get_where("tr_pengurus",array("tmmadrasah_id"=>$_SESSION['tmmadrasah_id']));
				    if(count($peng) ==0){

							return "2";
				   }else{

                            return "1";
				   }
				   
			   }
			  
			  
			  
			  
		  }else{
			  
			    $persy = $this->Acuan_model->get_where("tr_persyaratan",array("tmmadrasah_id"=>$_SESSION['tmmadrasah_id'],"tmpersyaratan_id"=>$link));
				 
				 
			       if(count($persy) ==0){

							return "2";
				   }else{

                            return "1";
				   }
			  
		  }
		
	}
	
		public function cek_persyaratan($persyaratan){
		
		 $cek = $this->db->get_where("tr_persyaratan",array("tmmadrasah_id"=>$_SESSION['tmmadrasah_id'],"tmpersyaratan_id"=>$persyaratan))->row();
		   
		     if(count($cek)==0){
				 
				 return "0";
			 }else{
				 return "1";
			 }
		
		
	}
	
	public function cek_formulir(){
		
		 $organisasi = $this->db->query("select id from tr_organisasi where tmmadrasah_id='".$_SESSION['tmmadrasah_id']."'")->num_rows();
		 $pengurus   = $this->db->query("select id from tr_pengurus where tmmadrasah_id='".$_SESSION['tmmadrasah_id']."'")->num_rows();
		 $ptk      = $this->db->query("select id from tr_ptk where tmmadrasah_id='".$_SESSION['tmmadrasah_id']."'")->num_rows();

		   
		     if($organisasi==0 or  $ptk==0){
				 
				 return "0";
			 }else{
				 return "1";
			 }
		
		
	}
	
	public function grid_bank($paging){
        
	    $tmguru_id  = $_POST['tmguru_id'];
	    $ajaran     = $_POST['ajaran'];
	    $semester   = $_POST['semester'];
	    $tmmapel_id = $_POST['tmmapel_id'];
		
		$this->db->select("*");
        $this->db->from('tm_ujian');
       
         if(!empty($semester)){  $this->db->where("semester",$semester);    }
         if(!empty($ajaran)){  $this->db->where("ajaran",$ajaran);    }
         if(!empty($tmguru_id)){  $this->db->where("tmguru_id",$tmguru_id);    }
         if(!empty($tmmapel_id)){  
		 
		 $this->db->where("trkelas_id in(select id from tr_kelas where tmmapel_id='".$tmmapel_id."')"); 


		 }
         
         
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("ajaran","ASC");
					 $this->db->order_by("semester","ASC");
					 $this->db->order_by("created","DESC");
					 
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
}
