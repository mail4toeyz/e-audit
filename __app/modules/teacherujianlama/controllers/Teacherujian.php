<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teacherujian extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		  if(!$this->session->userdata("aksi_id")){
			    
				echo $this->Reff->sessionhabis();
				exit();
			
		  }
		  $this->load->model('M_dashboard',"m");
		date_default_timezone_set("Asia/Jakarta");
	  }
	  
   function _template($data)
	{
	  $this->load->view('admin/page_header',$data);	
	}
	
	
		

    public function create(){
		
		
		
		
		 $tmujian_id                = $this->input->get_post("id",true);
         
		 
		 $cek = $this->db->query("select id from tr_soal where tmujian_id='".$tmujian_id."'")->num_rows();
		   if($cek==0){
			 
				 
				 $id_soal    = $this->Reff->get_max_id("id","tr_soal")+1;
				 $this->db->set("id",$id_soal);
				 $this->db->set("tmujian_id",$tmujian_id);				
				 $this->db->set("urutan",1);
				
				 $this->db->insert("tr_soal");
				
				 
				 $data['tmujian_id']  = $tmujian_id;
				 $data['ujian'] = $this->db->get_where("tm_ujian",array("id"=>$tmujian_id))->row();
				 $data['soal']  = $this->db->get_where("tr_soal",array("id"=>$id_soal))->row();
				 $data['dendiganteng']  = "Yes";

	          }else{
				$data['tmujian_id']  = $tmujian_id;
				$data['ujian'] = $this->db->get_where("tm_ujian",array("id"=>$tmujian_id))->row();
			
				$data['soal']  = $this->db->get_where("tr_soal",array("tmujian_id"=>$tmujian_id))->result();
				 
				$data['edit']  = "yes";
				$data['editganteng']  = "yes";

			  }
			     $this->load->view('teacherujian/index',$data);
				
		
		 
		
	}
	
	public function simpan_ujian(){
		   $nilai = $_POST['nilai'];
		    if($_POST['kolom']=="tgl_mulai" or $_POST['kolom']=="terlambat"){
				
				$nilai  = formattanggaldb2($_POST['nilai']);;
				
			}
		         $this->db->set($_POST['kolom'],$nilai);
		         $this->db->where("id",$_POST['tmujian_id']);
				 $this->db->update("tm_ujian");
				 echo "sukses";
		
	}
	
	public function hapusujian(){
		         $id = $_POST['dataid'];
		         $this->db->where("id",$id);
				 $this->db->delete("tm_ujian");
				 
				 $this->db->where("tmujian_id",$id);
				 $this->db->delete("tr_soal");
				 
				 $this->db->where("tmujian_id",$id);
				 $this->db->delete("h_ujian");
				 
				  $this->db->where("tmujian_id",$id);
				 $this->db->delete("tr_tugas");
				 
				 
				  echo "sukses";
		
	}
	
	public function simpan_soal(){
		
		         
		         $this->db->set($_POST['kolom'],nl2br($_POST['nilai']));
		         $this->db->where("id",$_POST['trsoal_id']);
				 $this->db->update("tr_soal");
				 echo "sukses";
		
	}
	
	
	
	
	 public function tambah_soal(){
		
		
		         $tmujian_id                = $this->input->get_post("tmujian_id",true);
                 $id_soal    = $this->Reff->get_max_id("id","tr_soal")+1;
				
                 $urutan     = $this->db->query("select max(urutan) as urutan from tr_soal where tmujian_id='".$tmujian_id."'")->row();
			
				
				 $this->db->set("id",$id_soal);
				 $this->db->set("tmujian_id",$tmujian_id);
				
				 $this->db->set("urutan",($urutan->urutan+1));
				
				 $this->db->insert("tr_soal");
				
				 $data['soal']  = $this->db->get_where("tr_soal",array("id"=>$id_soal))->row();
				 $data['tmujian_id']  = $tmujian_id;
				 echo $data['soal']->id;
				 echo "[splitdendiganteng]";
				 echo $data['soal']->urutan;
				 echo "[splitdendiganteng]";
			     $this->load->view('teacherujian/pertanyaan',$data);
				
	
	}
	

	
	public function hapus_soal(){
		         $this->db->where("id",$_POST['trsoal_id']);
				
				 $this->db->delete("tr_soal");
				 echo "sukses";
		
	}
	
	// Edit ujian
	 public function edit($kode=null,$nama=null,$id=null,$id_ujian=null){
		
		
		 $ajax                = $this->input->get_post("ajax",true);
         $id_ujian            = base64_decode($id_ujian);		 
         $data['trkelas_id']    = base64_decode($id);		 
       
		 $data['data']	      = $this->Reff->get_where("tm_guru",array("id"=>$_SESSION['tmguru_id']));
		 $data['madrasah']	  = $this->Reff->get_where("tm_madrasah",array("id"=>$data['data']->tmmadrasah_id));
		 $data['title']   = "Ujian  ";
		 
		        
		 
		 
	     if($ajax=="yes"){
			 
			    
				
				 
				 $data['tmujian_id']  = $id_ujian;
				 $data['ujian'] = $this->db->get_where("tm_ujian",array("id"=>$id_ujian))->row();
				 $data['soal']  = $this->db->get_where("tr_soal",array("tmujian_id"=>$id_ujian))->result();
				       if(count($data['soal'])==0){
						     $tmmapel_id    = $this->Reff->get_kondisi(array("id"=>$data['ujian']->trkelas_id),"tr_kelas","tmmapel_id");
							 
							 
						     $id_soal    = $this->Reff->get_max_id2("id","tr_soal");
							 $this->db->set("id",$id_soal);
							 $this->db->set("tmujian_id",$id_ujian);
							 $this->db->set("tmmapel_id",$tmmapel_id);
							 $this->db->set("tmguru_id",$_SESSION['tmguru_id']);
							 $this->db->set("urutan",1);
							
							 $this->db->insert("tr_soal");
				 
				 
						   
						   
					   }
			     $data['soal']  = $this->db->get_where("tr_soal",array("tmujian_id"=>$id_ujian))->result();
				 
				 $data['edit']  = "yes";
				 $data['editganteng']  = "yes";
				
			     $this->load->view('teacherujian/index',$data);
				
		
		 }else{
			 
			 $kelas = $this->db->get_where("tr_kelas",array("id"=>$data['trkelas_id']))->row();
			 
			 redirect(site_url("teacherkelas/ujian/".base64_encode($kelas->kode)."/".base64_encode($kelas->nama)."/".base64_encode($kelas->id).""));
		 
		 }
		
	}
	
	
	
	public function loadsoal(){
		
		
		 $ajax                      = $this->input->get_post("ajax",true);
		 $tmujian_id                = $this->input->get_post("tmujian_id",true);
          
         	 
         $data['tmujian_id']        = ($tmujian_id);		 
       
		 $data['data']	      = $this->Reff->get_where("tm_guru",array("id"=>$_SESSION['tmguru_id']));
		 $data['madrasah']	  = $this->Reff->get_where("tm_madrasah",array("id"=>$data['data']->tmmadrasah_id));
		 $data['title']   = "Ujian  ";
		 
		 $this->load->view("teacherujian/page_ujian",$data);
		 
		
		
	}
	
	public function loadsoalujian($tmujian_id=null,$nama=null,$id=null,$id_ujian=null){
		
		 $ajax                = $this->input->get_post("ajax",true);
         $id_ujian        = base64_decode($id_ujian);		 
         $data['trkelas_id']        = base64_decode($id);		 
         $tmujian_id        = base64_decode($tmujian_id);		 
       
		 $data['data']	      = $this->Reff->get_where("tm_guru",array("id"=>$_SESSION['tmguru_id']));
		 $data['madrasah']	  = $this->Reff->get_where("tm_madrasah",array("id"=>$data['data']->tmmadrasah_id));
		 $data['title']   = "Ujian  ";
		 
		 $this->db->query("delete from tr_soal where soal IS NULL and tmujian_id='".$tmujian_id."'");
		 $this->db->query("delete from tr_soal where  tmujian_id='".$tmujian_id."'");
		 $this->db->query("INSERT INTO tr_soal (tmguru_id, soal,opsi_a,opsi_b,opsi_c,opsi_d,opsi_e,jawaban,bobot,tmujian_id,jenis,urutan,jawaban_essay)
					SELECT tmguru_id, soal,opsi_a,opsi_b,opsi_c,opsi_d,opsi_e,jawaban,bobot,".$tmujian_id.",jenis,urutan,jawaban_essay
					FROM tr_soal
					WHERE tmujian_id='".$id_ujian."'");
					
				 $data['tmujian_id']  = $tmujian_id;
				 $data['ujian'] = $this->db->get_where("tm_ujian",array("id"=>$tmujian_id))->row();
				 $data['soal']  = $this->db->get_where("tr_soal",array("tmujian_id"=>$tmujian_id))->result();
				 $data['edit']  = "yes";
				
				
				$data['konten'] = "teacherujian/index";
			 
			 $this->_template($data);
		
	}
	
	public function jenissoal(){
		
		$trsoal_id                    = $this->input->get_post("trsoal_id",true);
		$data['jenis']                = $this->input->get_post("jenis",true);
		
		 $this->db->set("jenis",$data['jenis']);
		 $this->db->set("opsi_a",NULL);
		 $this->db->set("opsi_b",NULL);
		 $this->db->set("opsi_c",NULL);
		 $this->db->set("opsi_d",NULL);
		 $this->db->set("opsi_e",NULL);
		
		 $this->db->where("id",$trsoal_id);
		 $this->db->update("tr_soal");
		 
		$data['soal']  = $this->db->get_where("tr_soal",array("id"=>$trsoal_id))->row();
		$this->load->view('teacherujian/opsi_soal',$data);
		
	}
	
	public function simpan_soal_bs(){
		$trsoal_id                    = $this->input->get_post("trsoal_id",true);
		$kolom                        = $this->input->get_post("kolom",true);
		$isi                          = $this->input->get_post("isi",true);
		$jawaban                      = $this->input->get_post("jawaban",true);
		
			$this->db->set("opsi_a","Benar");
			$this->db->set("opsi_b","Salah");
			$this->db->set("jawaban",$jawaban);
			$this->db->where("id",$trsoal_id);
			$this->db->update("tr_soal");
			echo "sukses";
		
	}
	
	public function simpan_soal_jodoh(){
		$trsoal_id                    = $this->input->get_post("trsoal_id",true);
		$kolom                        = $this->input->get_post("kolom",true);
		$kolom1                       = $this->input->get_post("kolom1",true);
		$kolom2                       = $this->input->get_post("kolom2",true);
		$gabung                       = $kolom1."[split]".$kolom2;
		
			$this->db->set($kolom,$gabung);
			
			$this->db->where("id",$trsoal_id);
			$this->db->update("tr_soal");
			echo "sukses";
		
	}


  public function simpan_ujian_semua(){
	  
	  $tmujian_id                    = $this->input->get_post("tmujian_id",true);
	  $trkelas_id                    = $this->input->get_post("trkelas_id",true);
	  $nama_tes                      = $this->input->get_post("nama_tes",true);
	  $jenis_tes                     = $this->input->get_post("jenis_tes",true);
	  $kode                          = $this->input->get_post("kd",true);
	  $mulai                         = $this->input->get_post("mulai",true);
	  $terlambat                     = $this->input->get_post("terlambat",true);
	  
	  		
		$namakelas = $this->Reff->get_kondisi(array("id"=>$trkelas_id),"tr_kelas","nama");
						
	    $this->Reff->notifikasi($_SESSION['nama_guru']." membuat ujian CBT untuk kelas  ".$namakelas,"kelasnotifsiswa",0,0,$trkelas_id);
			   
			   
			   
	  
	    $semester = $this->Reff->semester();
		$ajaran   = $this->Reff->ajaran();
		
	    $jumlahph   = $this->db->query("select count(id) as ph,max(pertemuan) as pertemuan from tr_tugas where trkelas_id='".$trkelas_id."' and kd='".$kode."' and ki='3' and semester='".$semester."' and ajaran='".$ajaran."' ")->row();
	
	    $ph = ($jumlahph->ph);
	    $pertemuan = ($jumlahph->pertemuan+1);
		
		$cek = $this->db->query("select tmujian_id from tr_tugas where tmujian_id='".$tmujian_id."'")->num_rows();
		$ujiandata = $this->db->get_where("tm_ujian",array("id"=>$tmujian_id))->row();
		
		  if($jenis_tes==1){
		  if($cek==0){
			  
	          $ph = ($jumlahph->ph+1);
			  
			  $this->db->set("trkelas_id",$trkelas_id);
			  $this->db->set("pertemuan",0);
			  $this->db->set("nama",1);
			  $this->db->set("ph",$ph);
			  $this->db->set("pertemuan",$pertemuan);
			  $this->db->set("keterangan","CBT ".$nama_tes);
			 
			  $this->db->set("tanggal",$mulai);
			  $this->db->set("tgl_mulai",$ujiandata->tgl_mulai);
			  $this->db->set("tgl_selesai",$ujiandata->terlambat);
			  $this->db->set("ki",3);
			  $this->db->set("kd",$kode);
			  $this->db->set("ajaran",$ajaran);
			  $this->db->set("semester",$semester);
			  $this->db->set("tmujian_id",$tmujian_id);
			  $this->db->insert("tr_tugas");
			 
			   
			   
			   
			   
		  }else{
			  
			  $this->db->set("trkelas_id",$trkelas_id);
			  $this->db->set("pertemuan",0);
			   $this->db->set("nama",1);
			  $this->db->set("ph",$ph);
			  $this->db->set("pertemuan",$pertemuan);
			  $this->db->set("keterangan","CBT ".$nama_tes);
			  $this->db->set("tanggal",$mulai);
			  $this->db->set("tgl_mulai",$ujiandata->tgl_mulai);
			  $this->db->set("tgl_selesai",$ujiandata->terlambat);
			  $this->db->set("ki",3);
			  $this->db->set("kd",$kode);
			  $this->db->set("ajaran",$ajaran);
			  $this->db->set("semester",$semester);
			  $this->db->where("tmujian_id",$tmujian_id);
			  $this->db->update("tr_tugas");
		  }
		
		  }
		
		
		$this->db->query("delete from tr_soal where tmguru_id='".$_SESSION['tmguru_id']."' and jenis IS NULL and soal IS NULL or (jenis='' and soal='')");

			   
		echo "sukses";
		
	  
  }
  
  
  
  public function import_excel()
		{
				$this->load->library('PHPExcel/IOFactory');
       // error_reporting(0);
	
	 
	    $sheetup   = 0;
	    $rowup     = 1;
		
		
		$fileName = time().".xlsx";
         
        $config['upload_path'] = './__statics/excel/tmp/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size'] = 100000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') ){
			$jupload=0;
              echo $this->upload->display_errors();

			  
		}else{
             
        $media = $this->upload->data('file');
		
        $inputFileName = './__statics/excel/tmp/'.$fileName;
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet($sheetup);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
			
			
				 $tmujian_id                = $this->input->get_post("tmujian_id",true);
				 
				 $this->db->query("delete from tr_soal where tmujian_id='".$tmujian_id."'");
               
             
			 $jupload=0;
            for ($row = $rowup; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
				
       
			 	
			    $urutan        =      (trim($rowData[0][0])); 
			    $soal          =      (trim($rowData[0][1])); 
			 	$a         =      (trim($rowData[0][2]));
			 	$b       =      (trim($rowData[0][3]));
			    $c       =      (trim($rowData[0][4])); 
			    $d       =      (trim($rowData[0][5]));	 
			 	
			 
			 	$e         =      (trim($rowData[0][6]));
			 	//$jawaban         =      (trim($rowData[0][7]));
				
				
				
				if($soal !=""){
				 $this->db->set("jenis",$_POST['jenis']);
				 $this->db->set("soal",$soal);
				 $this->db->set("opsi_a",$a);
				 $this->db->set("opsi_b",$b);
				 $this->db->set("opsi_c",$c);
				 $this->db->set("opsi_d",$d);
				 $this->db->set("opsi_e",$e);
				/*  $this->db->set("jawaban",$jawaban);
				 
				 $this->db->set("tmujian_id",$tmujian_id);
			
				 $this->db->set("urutan",($urutan)); */
				
				 $this->db->insert("tm_wawancara");
				}
				
				
			}
			
			
		
			 
			 redirect(site_url("banksoal/buatsoal"));
        
        }
   }
	
	
	public function grid_bank(){
		
		  $iTotalRecords = $this->m->grid_bank(false)->num_rows();
		  
		  $trkelas_id        = $this->input->get_post("trkelas_id",true);
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid_bank(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		    
		   foreach($datagrid as $val) {
			    $kelas = $this->db->get_where("tr_kelas",array("id"=>$val['trkelas_id']))->row();
				
			    
			     $jumlahsoal = $this->db->query("select count(id) as jml from tr_soal where tmujian_id='".$val['id']."' ")->row();
			   
				$no = $i++;
				$records["data"][] = array(
					$no,
						
					
					$this->Reff->get_kondisi(array("id"=>$val['tmguru_id']),"tm_guru","nama"),
					kelasshow($kelas->kelas)."<br>".$this->Reff->get_kondisi(array("id"=>$kelas->rombel),"tr_ruangkelas","nama"),
					
					$val['ajaran']."/".($val['ajaran']+1)."<br>".semester($val['semester']),
					$val['nama'],
					$this->Reff->get_kondisi(array("id"=>$kelas->tmmapel_id),"tm_mapel","nama")."<br><span class='badge badge-danger'>".$jumlahsoal->jml." Soal </span>",
					'<a href="javascript:void(0)" class="lihatsoal btn btn-danger btn-sm" dataid="'.$val['id'].'"  data-toggle="modal" data-target="#MyModalAgenda"> <i class="fa fa-file"></i> Ambil Soal </a> '

				  );
			  
			  
		   }
		   
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	
	public function lihatsoal(){
		
		
		$data['id'] = $this->input->get_post("id");
		$data['tmujian_id'] = $this->input->get_post("tmujian_id");
		
		$this->load->view("lihatsoal",$data);
	}
	
	
	public function ambilgaes(){
		
		$id_soal    = $this->input->get_post("id_soal");
		$tmujian_id = $this->input->get_post("tmujian_id");
		
		$cek = $this->db->query("select * from tm_ujian where id='".$tmujian_id."'")->row();
		
		  if(empty($cek->nama)){
			  echo "Penarikan Soal ini Gagal Karena Anda belum melengkapi Pengaturan ujian, Silahkan lengkapi dulu pengaturan ujiannya ya..";
			  
		  }else{
			  
			  $soal = $this->db->get_where("tr_soal",array("id"=>$id_soal))->row();
			  
			  
			   $tmmapel_id    = $this->Reff->get_kondisi(array("id"=>$cek->trkelas_id),"tr_kelas","tmmapel_id");
			   
			     $this->db->set("tmujian_id",$tmujian_id);
				 $this->db->set("tmguru_id",$_SESSION['tmguru_id']);
				 $this->db->set("jenis",$soal->jenis);
				 $this->db->set("soal",$soal->soal);
				 $this->db->set("opsi_a",$soal->opsi_a);
				 $this->db->set("opsi_b",$soal->opsi_b);
				 $this->db->set("opsi_c",$soal->opsi_c);
				 $this->db->set("opsi_d",$soal->opsi_d);
				 $this->db->set("opsi_e",$soal->opsi_e);
				 $this->db->set("jawaban",$soal->jawaban);
				 $this->db->set("bobot",$soal->bobot);
				 $this->db->set("urutan",$soal->urutan);
				 $this->db->set("jawaban_essay",$soal->jawaban_essay);
				 $this->db->set("tmmapel_id",$tmmapel_id);
				 
				 $this->db->insert("tr_soal");
				 
				 
			   echo "Penarikan Soal ini berhasil dilakukan, silahkan simpan dan tutup ujian dan cek pada tombol simulasi atau pensil";
			  
		  }
		
	}
	 
 // Import PDF

	public function import_pdf(){
		include APPPATH.'/libraries/importpdf/PdfToText.phpclass';
		

		$tmujian_id = $this->input->get_post("tmujian_id",true);
		$jml_soal   = $this->input->get_post("jml_soal",true);

		if($tmujian_id==""){

			return "salah woy ";
		}


		$fileName = time().".pdf";
         
        $config['upload_path'] = './tmp/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = '*';
        $config['max_size'] = 10000000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') ){
			$jupload=0;
			  echo $this->upload->display_errors();
			  
		}else{
					$file		=  'tmp/'.$fileName ;
					$pdf		=  new PdfToText ( "$file", PdfToText::PDFOPT_DECODE_IMAGE_DATA ) ;
					$image_count 	=  count ( $pdf -> Images ) ;
					$outputdata = nl2br( $pdf -> Text ) ;

					$this->db->where("tmujian_id",$tmujian_id);					
					$this->db->delete("tr_soal");



					$no=1;
					for($a=1;$a <=$jml_soal; $a++){
						 $no++;
						 if($a==$jml_soal){
							//echo "dendi".$a; exit(); 
							 $parsing =  $this->get_string_between(strtolower($outputdata), $a.'.','e.'); 
						 }else{
						 $parsing =  $this->get_string_between($outputdata, $a.'.', $no.'.'); 
						 }

						   $soal    = $a.".".$parsing; 
						   $soal_pecah  =  $this->get_string_between($soal, $a.'.','A.');  

						 

						if(!empty($parsing)){
							       $id_soal = $this->Reff->get_max_id2("id","tr_soal");
									


									$jawaban = array("1"=>"a.","2"=>"b.","3"=>"c.","4"=>"d.","5"=>"e.");
									$soal    = $this->array_explode($jawaban,strtolower($soal));
										
									//echo $soal[1];	 exit();
											$this->db->set("tmujian_id",$tmujian_id);
											$this->db->set("id",$id_soal);
											$this->db->set("jenis",1);
											$this->db->set("soal",$soal[0]);
											$this->db->set("opsi_a",$soal[1]);
											$this->db->set("opsi_b",$soal[2]);
											$this->db->set("opsi_c",$soal[3]);
											$this->db->set("opsi_d",$soal[4]);
											$this->db->set("opsi_e",$soal[5]);
											
										
											$this->db->set("urutan",$a);
											$this->db->insert("tr_soal");
												

											
										
						} 

							
					}
					

					if  ( $image_count )
					{
						for  ( $i = 0 ; $i  <  $image_count ; $i ++ )
						{
							
							$img		=  $pdf -> Images [$i] ;			
							$imgindex 	=  sprintf ( "%02d", $i + 1 ) ;
							$output_image	=  "__statics/gudangsoal/images/".time().".jpg" ;
							
							
							$textcolor	=  imagecolorallocate ( $img -> ImageResource, 0, 0, 255 ) ;
							
							
							imagestring ( $img -> ImageResource, 5, 0, 0, "Hello world #$imgindex", $textcolor ) ;
							
							
							$img -> SaveAs ( $output_image ) ;
							
							$this->output ( "Generated image file \"$output_image\"" ) ;
							}
						}
					else
						echo "No image was found in sample file \"$file.pdf\"" ;


						redirect(base_url()."banksoal/buatsoal");

		}

			
		}

		public function array_explode($delimiters, $string){
			if(!is_array(($delimiters)) && !is_array($string)){
				//if neither the delimiter nor the string are arrays
				return explode($delimiters,$string);
			} else if(!is_array($delimiters) && is_array($string)) {
				//if the delimiter is not an array but the string is
				foreach($string as $item){
					foreach(explode($delimiters, $item) as $sub_item){
						$items[] = $sub_item;
					}
				}
				return $items;
			} else if(is_array($delimiters) && !is_array($string)) {
				//if the delimiter is an array but the string is not
				$string_array[] = $string;
				foreach($delimiters as $delimiter){
					$string_array = $this->array_explode($delimiter, $string_array);
				}
				return $string_array;
			}
		}

		
		
	function  output ( $message )
	   {
		if  ( php_sapi_name ( )  ==  'cli' )
			echo ( $message ) ;
		else
			echo ( nl2br ( $message ) ) ;
		}
		
	function get_string_between($string, $start, $end){
			$string = ' ' . $string;
			$ini = strpos($string, $start);
			if ($ini == 0) return   '';
			$ini += strlen($start);
			$len = strpos($string, $end, $ini) - $ini;
			return  substr($string, $ini, $len);
	}

}