
  <div class="tab-content acc-setting">
  
  
  
 <?php 
          $no=0;
		 foreach($soal as $soal){
			 $no++;
			 $active="";
			 if($no==1){
				 $active="active";
			 }
           ?>
	
	
		   
 <div class="tab-pane in <?php echo $active; ?> detailpertanyaan" id="detailpertanyaan<?php echo $soal->id; ?>">
						
					
						 
												<h3 id="titleujian<?php echo $soal->id; ?>">
												 	
												 
												 
												<span class="badge badge-sm bg-gradient-success" style="font-size:20px">PERTANYAAN KE - <?php echo $soal->urutan; ?>  </span> </h3>
												
												<div class="notbarpertanyaan">
													Nomor Soal 
												   <div class="input-group input-group-outline my-3">
														   <input type="text" placeholder="Nomor  Soal" class="form-control  ketiksoal" onkeypress="return formatnilai(this);" trsoal_id="<?php echo $soal->id; ?>" kolom="urutan"   value="<?php echo $soal->urutan; ?>">  
														</div>
												</div>
												
												<div class="notbarpertanyaan">
													Jenis Soal 
													<div class="input-group input-group-outline my-3">
														  <select class="form-control jenissoal" trsoal_id="<?php echo $soal->id; ?>">
														  <option value=""> - Pilih Jenis Soal - </option>
														    <option value="1" <?php echo ($soal->jenis==1) ? "selected":""; ?>> Pilihan Ganda (Multiple Choice) </option>
															<option value="9" <?php echo ($soal->jenis==9) ? "selected":""; ?>> Pilihan Ganda (Scoring) </option>
															<option value="2" <?php echo ($soal->jenis==2) ? "selected":""; ?>> Salah Benar (True/False) </option>
															<option value="3" <?php echo ($soal->jenis==3) ? "selected":""; ?>> Short Answer </option>
															<option value="4" <?php echo ($soal->jenis==4) ? "selected":""; ?> > Menjodohkan (Matching) </option>
															<option value="5" <?php echo ($soal->jenis==5) ? "selected":""; ?>> Essay  </option>
														    <option value="6" <?php echo ($soal->jenis==6) ? "selected":""; ?>> Pilihan  Multiple Answer </option> 
														    <option value="7" <?php echo ($soal->jenis==7) ? "selected":""; ?>> Checked Box </option> 
															<option value="8" <?php echo ($soal->jenis==8) ? "selected":""; ?>> Tebakan  </option>
														    
														  </select>
														</div> 
														
														
														Pertanyaan
														<div class="input-group input-group-outline my-3">
														 
															 <textarea id="editor<?php echo $soal->id; ?>"   trsoal_id="<?php echo $soal->id; ?>" kolom="soal"><?php echo $soal->soal; ?></textarea>
															 <script type="text/javascript">
															 
																
																 var editor = CKEDITOR.replace( 'editor<?php echo $soal->id; ?>', {
																	 
															
																

																			  
																			  uploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json&show=1',
																			  filebrowserBrowseUrl: base_url+'__statics/ckdrive/ckfinder.html',
																			  filebrowserImageBrowseUrl: base_url+'__statics/ckdrive/ckfinder.html?type=Images&show=1',
																			  filebrowserUploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Files&show=1',
																			  filebrowserImageUploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Images&show=1',
																			  height: 300,
																			  enterMode : CKEDITOR.ENTER_BR,
                                                                              shiftEnterMode: CKEDITOR.ENTER_P,
																			  removeDialogTabs: 'image:advanced;link:advanced',
																			  allowedContent :true
																} );
																

																editor.on('instanceReady', function(evt) {
																		this.dataProcessor.htmlFilter.addRules({
																			elements: {
																				img: function(element) {
																					if (!element.hasClass('img-responsive')) {
																						element.addClass('img-responsive');
																					}
																				}
																			}
																		});
																	});

																	
															CKEDITOR.plugins.addExternal( 'ckeditor_wiris', '<?php echo base_url(); ?>__statics/js/matematika/plugin.js');													
																editor.on('change',function(){
																	
																	cek_opsi("<?php echo $soal->id; ?>");
																	
																});		
																 </script>
														    
														     
														</div>
														<?php 
														if($soal->urutan==1){
														?>
														<div style="float:right">
														
														
															    </div>
														<?php
														  }else{
															 ?>
															  <div style="float:right">
															  <button class="btn btn-sm btn-danger hapus_soal" trsoal_id="<?php echo $soal->id; ?>"><i class="la la-trash-o"></i> Hapus Soal  </button>
															  </div>

															 <?php 
														  }							
														  ?>
															 
													</div><!--notbar end-->
													
													<div id="opsisoal<?php echo $soal->id; ?>"> 
													
													   
													     <?php 
														 $dataaa['jenis'] = $soal->jenis;
														 $dataaa['soal']  = $soal;
														 $this->load->view("teacherujian/opsi_soal",$dataaa);

														 ?>
													   
													   
													
										           </div>
												   
												   
			</div>
	<?php 
		 }
	?>
	 
  </div>
	