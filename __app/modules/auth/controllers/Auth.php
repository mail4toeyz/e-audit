<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

	const SSO_URL     = 'https://sso.kemenag.go.id/auth';
	const SSO_SIGNIN  = self::SSO_URL . '/signin';
	const SSO_SIGNOUT = self::SSO_URL . '/signout';
	const SSO_VERIFY  = self::SSO_URL . '/verify';
	const APP_ID      = '0dba499bb4ebee0cb02ac21bf79c2d8e';

	public function __construct()
	{
		parent::__construct();
		// isLogin();
		// $this->load->model('M_admin', 'm');
	}

	function index()
	{
		// echo "auth";
		// print_r($this->session->userdata);
		redirect(self::SSO_SIGNIN . '?appid=' . self::APP_ID);

		// if (!$this->session->userdata('nip')) {
		// 	
		// } else {
		// 	redirect('user/absen');
		// }
	}

	public function callback()
	{
		$token = $this->input->get('token') ?? '';

		echo $token;
		die();
		
		if ($token) {
			$verify_url = self::SSO_VERIFY;
			$ch = curl_init($verify_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept: application/json', 'Authorization: Bearer ' . $token]);

			$response = curl_exec($ch);

			if (curl_errno($ch)) {
				echo "CURL ERROR: " . curl_error($ch);
				curl_close($ch);
				exit();
			}
			curl_close($ch);
			$ret = json_decode($response, true);

			if ($ret['status'] == 200) {
				$pegawai = $ret['pegawai'];
				$session_data = array(
					'nip_lama'  => $pegawai['NIP_LAMA'],
					'nip'       => $pegawai['NIP'],
					'jabatan'   => $pegawai['KETERANGAN_JABATAN'],
					'nama'      => $pegawai['NAMA'],
					'photo'     => $pegawai['PHOTO'],
					'sso_role'  => $pegawai['SSO_ROLE'],
					'satker1'   => $pegawai['KODE_SATUAN_KERJA'],
					'satker2'   => $pegawai['KODE_SATKER_2'],
					'satker3'   => $pegawai['KODE_SATKER_3'],
					'satker4'   => $pegawai['KODE_SATKER_4'],
					'satker5'   => $pegawai['KODE_SATKER_5'],
					'type'      => 'user',
				);
				$this->session->set_userdata($session_data);

				print_r($ret)
				die();
				//jika valid
				//redirect('wellcomepage');
			} else {
				print_r($ret)
				die();
				//jika tidak valid
				//redirect('auth');
			}
		} else {
			die('Something Wrong');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect($this->SSO_SIGNOUT);
	}

	public function is_loggedin($value = '')
	{
		if ($this->session->userdata('nip')) {
			return true;
		}

		return false;
	}
}
