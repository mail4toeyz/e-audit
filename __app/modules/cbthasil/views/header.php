<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $title; ?></title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>__statics/img/logo.png">
	<link rel="stylesheet" href="<?php echo base_url(); ?>__statics/cbt/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link href="<?php echo base_url(); ?>__statics/frontend/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>__statics/cbt/dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>__statics/cbt/dist/css/skins/skin-blue.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>__statics/cbt/dist/css/mystyle.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>__statics/cbt/bower_components/pace/pace-theme-flash.css">
	<link href="<?php echo base_url(); ?>__statics/js/alertify/css/alertify.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>__statics/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>__statics/js/jqueryui.js"></script>
     <script src="<?php echo base_url(); ?>__statics/js/alertify/alertify.js"></script>
	<script src="<?php echo base_url(); ?>__statics/cbt/bower_components/sweetalert2/sweetalert2.all.min.js"></script>
	<style>
	.navbarcbt{
-webkit-box-shadow: 9px 8px 7px 0px rgba(0,0,0,0.37);
-moz-box-shadow: 9px 8px 7px 0px rgba(0,0,0,0.37);
box-shadow: 9px 8px 7px 0px rgba(0,0,0,0.37);
		
	}
	
	
.papers, .papers:before, .papers:after  {
  background-color: #fff;
  border: 1px solid #ccc;
  box-shadow: inset 0 0 30px rgba(0,0,0,0.1), 1px 1px 3px rgba(0,0,0,0.2);
}

.papers  {
  position: relative;
  width: 100%;
  padding: 2em;
 
}

.papers:before, .papers:after  {
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  -webkit-transform: rotateZ(2.5deg);
  -o-transform: rotate(2.5deg);
  transform: rotateZ(2.5deg);
  z-index: -1;
}

.papers:after  {
  -webkit-transform: rotateZ(-2.5deg);
  -o-transform: rotate(-2.5deg);
  transform: rotateZ(-2.5deg);
}

.papers h2  {
  font-size: 1.8em;
  font-weight: normal;
  text-align: center;
  padding: 0.2em 0;
  margin: 0;
  border-top: 1px solid #ddd;
  border-bottom: 2px solid #ddd;
}

.papers p  {
  text-align: left;
  margin: 1,5em 0;
}

	</style>
	<script type="text/javascript">
		var base_url = '<?php echo base_url(); ?>';
	</script>
</head>

<body class="hold-transition skin-blue layout-top-nav" id="cbtdendi">
	<div class="wrapperss">

		<header class="main-header">
            <?php $this->load->view("cbthasil/menu"); ?>
		</header>
		<!-- Full Width Column -->
    <br>
   
		<div class="content-wrapper">
			<div class="container2">
				<!-- Content Header (Page header) -->
				
       <!-- <script defer src="<?php echo base_url(); ?>__statics/ai/face-api.min.js"></script>

<script defer src="<?php echo base_url(); ?>__statics/ai/script.js"></script> -->
<style>
    

    canvas {
      position: absolute;
    }
  </style>
<style>
/* Add some margin to the page and set a default font and colour */


/* Give headings their own font */

h1, h2, h3, h4 {
  font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
}

/* Main content area */

.contentdragda {
 
  text-align: center;
  -moz-user-select: none;
  -webkit-user-select: none;
  user-select: none;
}

/* Header/footer boxes */



.wideBox h1 {
  font-weight: bold;
  margin: 20px;
  color: #666;
  font-size: 1.5em;
}

/* Slots for final card positions */


/* The initial pile of unsorted cards */

.cardPile {
  margin: 0 auto;
  background: #ffd;
}

.cardSlots, .cardPile {
  width: 100%;
  height: 100%;
  padding: 20px;
  border: none;
  
}

/* Individual cards and slots */

.cardSlots div, .cardPile div {
  float: left;
  width:100%;
    height: 100%;
  padding: 10px;
  padding-top: 40px;
  padding-bottom: 0;
  border: none;
  -moz-border-radius: 10px;
  -webkit-border-radius: 10px;
  border-radius: 10px;
  margin: 0 0 0 10px;
  background: #fff;
}

.cardSlots div:first-child, .cardPile div:first-child {
  margin-left: 0;
}

.cardSlots div.hovered {
  background: #aaa;
}

.cardSlots div {
  border-style: dashed;
}

.cardPile div {
  background: #6ea8d1;
  color:white;
 
  font-size: 15px;
  border:1px;
}

.cardPile div.ui-draggable-dragging {
  -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
  -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
  box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
}

/* Individually coloured cards */

#card1.correct { background: red; }
#card2.correct { background: brown; }
#card3.correct { background: orange; }
#card4.correct { background: yellow; }
#card5.correct { background: green; }
#card6.correct { background: cyan; }
#card7.correct { background: blue; }
#card8.correct { background: indigo; }
#card9.correct { background: purple; }
#card10.correct { background: violet; }




</style>