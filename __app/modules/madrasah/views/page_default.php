<div class="row">	  
  <div class="col-md-12">
   <div class="alert alert-danger">Selamat Datang <u><?php echo $_SESSION['nama_aksi']; ?></u> Dalam Aplikasi Computer Based Test (CBT) Ujian Madrasah Tahun 2020/2021  </div>

  </div>
</div>

	<div class="row">	  
 
            
          <div class="col-lg-3 col-md-3 col-sm-3">
              <div class="card card-stats">
                <div class="card-header card-header-danger card-header-icon">
                  <div class="card-icon">
                       <i class="fa fa-bank"></i>
                  </div>
                  <p class="card-category"> Jumlah Madrasah </p>
                  <h3 class="card-title"> <?php echo $this->m->jml("tm_madrasah"); ?>
                    <small>Lembaga</small>
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    
                    <a href="<?php echo site_url("datamadrasah"); ?>"> <i class="fa fa-angle-right"></i> Lihat Semua data</a>
                  </div>
                </div>
              </div>
            </div>
          <?php 
            foreach($this->db->get("tm_kategori")->result() as $row){
            ?>
            <div class="col-lg-3 col-md-3 col-sm-3">
              <div class="card card-stats">
                <div class="card-header card-header-danger card-header-icon">
                  <div class="card-icon">
                       <i class="fa fa-bank"></i>
                  </div>
                  <p class="card-category"> <?php echo $row->nama; ?> </p>
                  <h3 class="card-title"><?php $jml = $this->db->query("select count(id) as jml from tm_madrasah where jenjang='".$row->id."' ")->row(); echo $jml->jml; ?> 
                    <small>Lembaga</small>
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    
                    <a href="<?php echo site_url("datamadrasah"); ?>"> <i class="fa fa-angle-right"></i> Lihat Semua data</a>
                  </div>
                </div>
              </div>
            </div>
          
          
    <?php 
            }
          ?>
     </div>


     <div class="row">	  

            
          <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                       <i class="fa fa-graduation-cap"></i>
                  </div>
                  <p class="card-category"> Jumlah Siswa Peserta CBT Di Madrasah Anda </p>
                  <h3 class="card-title"> <?php $jml = $this->db->query("select count(id) as jml from tm_siswa where tmmadrasah_id='".$_SESSION['madrasah_id']."' ")->row(); echo $jml->jml; ?>
                    <small>Orang</small>
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    
                    <a href="<?php echo site_url("datasiswa"); ?>"> <i class="fa fa-angle-right"></i> Lihat Semua data</a>
                  </div>
                </div>
              </div>
            </div>
         
     </div>
          <div class="row">
              <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-danger">
                  <h4 class="card-title">Laporan Login </h4>
                 
                </div>
                <div class="card-body table-responsive">
				
					<div class="table-responsive">
                                <table class="table table-bordered table-striped  " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
                                        <tr>
                                            <th width="2px">NO</th>
                                            
                                            <th>ROLE  </th>
                                            <th>KETERANGAN </th>
                                            <th>WAKTU </th>
                                            
                                            
                                        </tr>
                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
							
							
                  
                </div>
              </div>
            </div>
            </div>
			
			
				
<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							exportOptions: {
							  columns: [ 0,1,2,3]
							},
							text:'<font color:"black">Cetak Excel</font>',
							
							}
					],
					
					"ajax":{
						url :"<?php echo site_url("madrasah/grid"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
				
			


				
	


</script>