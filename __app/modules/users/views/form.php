<br>
<br>
<form class="row g-3" action="javascript:void(0)" method="post" id="simpangambar" name="simpangambarmodal" url="<?php echo site_url("users/save"); ?>">
	<input type="hidden" class="form-control" name='id' value="<?php echo (isset($data)) ? $data->id : ""; ?>">
	<input type="hidden" class="form-control" name='f[pegawai_id]' id="pegawai_id" value="<?php echo (isset($data)) ? $data->pegawai_id : ""; ?>">



	<div class="col-md-12">


		<select class="form-control  jabatanPilihan" id="pegawai">
			<option value="">- Cari Pegawai -</option>
			<?php
			$unit_kerja = $this->db->get("unit_kerja")->result();
			foreach ($unit_kerja as $unit) {
			?>

				<optgroup label="<?php echo $unit->nama; ?>">
					<?php
					$satkerData = $this->db->query("SELECT * from pegawai_simpeg where KODE_SATKER_3='{$unit->kode}'  order by NAMA_LENGKAP  ASC")->result();
					foreach ($satkerData as $row) {
					?>
						<option value="<?php echo $row->id; ?>" nip="<?php echo $row->NIP_BARU; ?>" nama="<?php echo $row->NAMA_LENGKAP; ?>" id="<?php echo $row->id; ?>"><?php echo $row->NIP_BARU; ?> - <?php echo $row->NAMA_LENGKAP; ?> <?php //echo $this->Reff->get_kondisi(array("id" => $row->jabatan_id), "jabatan", "nama"); 
																																																											?></option>
					<?php
					}
					?>
				</optgroup>

			<?php
			}
			?>


		</select>


	</div>
	<div class="col-md-6">
		<label for="inputEmail5" class="form-label">Nama </label>
		<input type="text" class="form-control" id="nama" name="f[nama]" value="<?php echo (isset($data)) ? $data->nama : ""; ?>">
	</div>
	<div class="col-md-6">
		<label for="inputPassword5" class="form-label">Username</label>
		<input type="text" class="form-control" id="username" name="f[username]" value="<?php echo (isset($data)) ? $data->username : ""; ?>">
	</div>

	<div class="col-md-12">
		<label for="inputPassword5" class="form-label">Buat Password </label>
		<input type="password" class="form-control" id="password" name="password" value="">
	</div>


	<div class="col-md-12">
		<label for="inputPassword5" class="form-label">Group User </label>

		<select class="form-control" name="f[group_id]">
			<option value=""> Pilih Group User </option>
			<?php
			$jabatan = $this->db->get("groups")->result();
			foreach ($jabatan as $r) {

			?><option value="<?php echo $r->id; ?>" <?php if (isset($data)) {
														echo ($data->group_id == $r->id) ? "selected" : "";
													} ?>> <?php echo ($r->nama); ?></option><?php

																						}
																							?>
		</select>

	</div>



	<br>

	<div class=" " id="aksimodal">
		<center>
			<button type="submit" class="btn btn-outline-primary ">
				<i class="fa fa-save"></i>
				<span>Simpan Data </span>
			</button>

			<button type="button" class="btn btn-outline-primary" id="cancel">

				<span>Close </span>
			</button>

		</center>

	</div>

	<div class="row clearfix" id="loadingmodal" style="display:none">
		<center>
			<div class="preloader pl-size-md">
				<div class="spinner-layer pl-red-grey">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div> <br> Data sedang disimpan, Mohon Tunggu ...
		</center>

	</div>

</form>
<script>
	$(document).ready(function() {
		$('.jabatanPilihan').select2({
      width: '100%',
      cache: false
    });

	})
</script>