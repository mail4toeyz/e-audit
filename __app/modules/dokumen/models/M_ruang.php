<?php

class M_ruang extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
    }

    
	public function grid($paging){
      
	   
	   
	    $keyword = $this->input->post('keyword');
	   
		
		
		$this->db->select("*");
        $this->db->from('tr_persyaratan');
        $this->db->where('kegiatan_id',$_SESSION['kegiatan_id']);
      
          
	    if(!empty($keyword)){  $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'");    }
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					
					 $this->db->order_by("nama","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
	
	public function insert(){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
	    
		$this->db->set("tmmadrasah_id",$_SESSION['tmmadrasah_id']);

		$this->db->set("i_entry",$_SESSION['nama_madrasah']);
		
		$this->db->set("d_entry",date("Y-m-d H:i:s"));

		$this->db->insert("tr_ruangkelas",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
	public function update($id){
		
		$form  = $this->security->xss_clean($this->input->get_post("f"));
	   
		$this->db->set("i_update",$_SESSION['nama_madrasah']);
		$this->db->set("d_update",date("Y-m-d H:i:s"));
		
		$this->db->where("id",$id);
		$this->db->update("tr_ruangkelas",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
}
