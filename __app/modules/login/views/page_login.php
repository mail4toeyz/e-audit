<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description"
        content="Dashboard Kegiatan adalah Platform yang akan memudahkan proses manajemen dan pengumpulan biodata Peserta kegiatan">
    
    <meta name="author" content="Dendi Nuraziz">
    <link rel="icon" href="https://appmadrasah.kemenag.go.id/pmu/__statics/images/logo.png" type="image/x-icon">
    <link rel="shortcut icon" href="https://appmadrasah.kemenag.go.id/pmu/__statics/images/logo.png" type="image/x-icon">
    <title>Sistem Informasi Pengawasan Internal Terintegrasi | Inspektorat Jenderal Kementerian Agama</title>

		
		<link rel="stylesheet" type="text/css" href="https://appmadrasah.kemenag.go.id/dmakmi/__statics/js/alert/alert.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/fa/css/all.min.css">  <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="https://appmadrasah.kemenag.go.id/dmakmi/assets/css/vendors/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="https://appmadrasah.kemenag.go.id/dmakmi/assets/css/vendors/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="https://appmadrasah.kemenag.go.id/dmakmi/assets/css/vendors/feather-icon.css">

    <link rel="stylesheet" type="text/css" href="https://appmadrasah.kemenag.go.id/dmakmi/assets/css/vendors/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="https://appmadrasah.kemenag.go.id/dmakmi/assets/css/style.css">
    <link id="color" rel="stylesheet" href="https://appmadrasah.kemenag.go.id/dmakmi/assets/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="https://appmadrasah.kemenag.go.id/dmakmi/assets/css/responsive.css">
    <script src="https://appmadrasah.kemenag.go.id/dmakmi/__statics/js/jquery-2.1.4.min.js"></script>
    <script src="https://appmadrasah.kemenag.go.id/dmakmi/__statics/js/proses.js"></script>
    <script src="https://appmadrasah.kemenag.go.id/dmakmi/__statics/js/jquery.blockui.min.js"></script>
    <script src="https://appmadrasah.kemenag.go.id/dmakmi/__statics/js/alert/alert.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
  	<script src="https://appmadrasah.kemenag.go.id/dmakmi/__statics/js/bootstrap.min.js"></script>
    <script type="text/javascript" crossorigin="use-credentials" src="https://api-emis.kemenag.go.id/v1/accounts/sso/signal"></script>
</head>

<body>
    <!-- login page start-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-5"><img class="bg-img-cover bg-center"
                    src="https://appmadrasah.kemenag.go.id/pmu/assets/images/Account-amico.png" alt="looginpage"></div>
            <div class="col-xl-7 p-0">
                <div class="login-card">
                    <div>
                        <div><a class="logo text-start" href="index.html">
                        <img class="img-fluid for-light"
                                    src="<?php echo base_url(); ?>__statics/img/logo.png" alt="looginpage"
                                    style="width:150px">
                          <img class="img-fluid for-light"
                                    src="<?php echo base_url(); ?>__statics/img/sipintar.png" alt="looginpage"
                                    style="width:150px">
                                  </a></div>
                        <div class="login-main">
						<form id="loginmadrasah" class="theme-form" id="loginmadrasah" action="javascript:void(0)" url="<?php echo site_url("login/do_login"); ?>" method="post">
                           
                                <h4>Sistem Informasi Pengawasan Internal Terintegrasi  <br>  Inspektorat Jenderal Kementerian Agama </h4>
                                <p>Masukkan Username dan Password Anda  </p>
								<div class="form-group">
                                    <label class="col-form-label">Username  </label>
									<input type="text" name="username" id="username"  class="form-control"  placeholder="Masukkan Username Anda" />
                                </div>

                               

                                <div class="form-group">
                                    <label class="col-form-label">Password </label>
									<input type="password" name="password"  class="form-control" id="password" placeholder="Masukkan Password Anda" />
                                </div>
                              
                                <div class="form-group mb-0">

                                    <button class="btn btn-success btn-block w-100" type="submit"><i class="fa fa-sign-in"></i> Masuk </button>
                                    <br>
                                    <br>
                                    <a href="https://emis.kemenag.go.id/login?continue=https://appmadrasah.kemenag.go.id/e-audit" class="btn btn-danger btn-block w-100"><i class="fa fa-lock"></i> Login SSO EMIS</a>
                                
                                </div>
                                <h6 class="text-muted mt-6 or"> </h6>

                               
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- latest jquery-->
      
        <!-- Bootstrap js-->
        <script src="https://appmadrasah.kemenag.go.id/pmu/assets/js/bootstrap/bootstrap.bundle.min.js"></script>
        <!-- feather icon js-->
        <script src="https://appmadrasah.kemenag.go.id/pmu/assets/js/icons/feather-icon/feather.min.js"></script>
        <script src="https://appmadrasah.kemenag.go.id/pmu/assets/js/icons/feather-icon/feather-icon.js"></script>
        <!-- scrollbar js-->
        <!-- Sidebar jquery-->
        <script src="https://appmadrasah.kemenag.go.id/pmu/assets/js/config.js"></script>
        <!-- Plugins JS start-->
        <!-- Plugins JS Ends-->
        <!-- Theme js-->
        <script src="https://appmadrasah.kemenag.go.id/pmu/assets/js/script.js"></script>
        <!-- login js-->
        <!-- Plugin used-->
    </div>
</body>
<script type="text/javascript">

		

			
		
</script>
<script type="text/javascript">
var base_url="<?php echo base_url(); ?>";


function sukses(param){
	   let timerInterval;
	   Swal.fire({
		 type: 'success',
		 title: 'Proses Authentication Berhasil ',
		 showConfirmButton: false,
		  html: 'Dalam <strong></strong> detik<br>Anda akan dialihkan kedalam aplikasi',

		 timer: 1000,
		 onBeforeOpen: () => {
		   Swal.showLoading();
		   timerInterval = setInterval(() => {
			 Swal.getContent().querySelector('strong')
			   .textContent = Swal.getTimerLeft()
		   }, 100)
		 },
		 onClose: () => {
		   clearInterval(timerInterval)
		 }
	   }).then((result) => {
		 if (
		   /* Read more about handling dismissals below */
		   result.dismiss === Swal.DismissReason.timer
		   
		 ) {
		
		  location.href = base_url+param;
		 }
	   })
}

function gagal(param){
	   let timerInterval;
	   Swal.fire({
		 type: 'warning',
		 title: 'Proses Authentication Gagal ',
		 showConfirmButton: true,
		  html: param

	   })
}


   $(document).on('submit', 'form#loginmadrasah', function (event, messages) {
	event.preventDefault();
	  var form   = $(this);
	  var urlnya = $(this).attr("url");
   
	 
	
			 $("#loading").show();
			 $("#action").hide();
			  $.ajax({
				   type: "POST",
				   url: urlnya,
				   data: form.serialize(),
				   success: function (response, status, xhr) {
					var ct = xhr.getResponseHeader("content-type") || "";
					   if (ct == "application/json") {
					   
						gagal(response.message);
						$("#loading").hide();
						$("#action").show();
						   
						   
					   } else {
						 
						  
						  sukses(response);
						  
						   
						   
							
						   
						 
					   }
					   
					  
					   
				   }
			   });
			 
	   return false;
   });
   
   
   
</script>
</html>