<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vicon extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		if(!$this->session->userdata("is_login")){
			    
			echo $this->Reff->sessionhabis();
			exit();
		
	  }
		  $this->load->model('M_ruang','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('auditor/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['title']   = "Video Conference ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			  
			
			$satkerData = "<ol>";
			$satker =  explode(",", $val['satker_id']);
			foreach ($satker as $r) {
				$satkerData .= "<li>" . $this->Reff->get_kondisi(array("id" => $r), "satker", "nama") . "</li>";
			}

			$satkerData .= "</ol>";

			$partisipan = "<ol>";
			$partisipanData =  explode(",", $val['partisipan']);
			foreach ($partisipanData as $r) {
				$partisipan .= "<li>" . $r. "</li>";
			}

			$partisipan .= "</ol>";
		   
			$no = $i++;
			$records["data"][] = array(
				$no,
				'
				<div class="btn-group" role="group">
				 <button type="button" class="btn btn-primary btn-sm  ubahmodal" datanya="'.$val['id'].'" urlnya="'.site_url("vicon/form").'" target="#loadform" data-bs-toggle="modal" data-bs-target="#basicModal">
								<i class="fa fa-pencil"></i>
				  </button>
				  <button type="button" class="btn btn-danger btn-sm  hapus" datanya="'.$val['id'].'" urlnya="'.site_url("vicon/hapus").'">
								<i class="fa fa-trash"></i>
				  </button>
				  </div>
				  ',
				$satkerData,
				$val['nama'],
				$val['kode'],
				$partisipan,
				formattimestamp($val['tgl_mulai']),
				(!empty($val['tgl_selesai'])) ? formattimestamp($val['tgl_selesai']):"-",
				
				'<a href="'.site_url("vicon/video_join/".base64_encode($val['id'])."/".base64_encode($val['kegiatan_id'])."").'" class=" btn btn-danger btn-sm" > <i class="fa fa-camera"></i> Join  </a>'
			

			  );
		  
		  
	   }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	public function form(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("tr_vicon",array("id"=>$id));
			   
		   }
		$this->load->view("form",$data);
		
	}
	
	
	
	
	public function save(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NIP lain.');
         $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
         $this->form_validation->set_message('numeric', '%s: Harus terdiri dari Angka');
				 
		 
			
				$config = array(
				    
				       array('field' => 'f[tgl_mulai]', 'label' => 'Tanggal Pertemuan    ', 'rules' => 'trim|required'),
					
				    array('field' => 'f[nama]', 'label' => 'Nama Pertemuan ', 'rules' => 'trim|required'),
				    array('field' => 'satuankerja[]', 'label' => 'Nama Pertemuan ', 'rules' => 'trim|required'),
					
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
			    $satker_id = implode(",",$this->input->get_post("satuankerja",true)); 
				$partisipan = implode(",",$this->input->get_post("partisipan",true));
				$kode  = $this->Reff->get_id(10);
				             if(empty($id)){
								 $this->db->set("kode",$kode);
								 $this->db->set("satker_id",$satker_id);
								 $this->db->set("partisipan",$partisipan);
								 $this->db->set("kegiatan_id",$_SESSION['kegiatan_id']);
								 $this->db->insert("tr_vicon",$f);
							 }else{
								$this->db->where("id",$id); 
								$this->db->set("satker_id",$satker_id);
								$this->db->set("partisipan",$partisipan);
								$this->db->update("tr_vicon"); 
								 
							 }
							 
						
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}
	
	
	public function hapus(){
		
		$id = $this->input->get_post("id",true);
		
		$this->db->delete("tr_vicon",array("id"=>$id));
		echo "sukses";
	}
	
	
	public function video_join($id,$trkelas_id){
	   
	   
		$ajax                  = $this->input->get_post("ajax",true);
		$data['kegiatan_id']   = base64_decode($trkelas_id);		 
		$data['id']           = base64_decode($id);		 
	
		$data['title']   = " Video Conference  ";
		
	   
		
		   
			$data['konten'] = "video_join";
		
			
			$this->_template($data);
		
	   
	   
   }
	
	 
}
