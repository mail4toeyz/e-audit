<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<div id="showform"></div>

<main id="main" class="main">

<div class="pagetitle">
  <h1><?php echo $title; ?></h1>
  <nav>
	<ol class="breadcrumb">
	  <li class="breadcrumb-item"><a href="index.html">e-Audit</a></li>
	
	  <li class="breadcrumb-item active"><?php echo $title; ?></li>
	</ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
	<div class="col-lg-12">

	  <div class="card">
		<div class="card-body">
		  <h5 class="card-title">
		 
		  <button type="button" target="#loadform" url="<?php echo site_url("vicon/form"); ?>"  data-bs-toggle="modal" data-bs-target="#basicModal"  class="btn btn-primary addmodal btn-sm"><span class="bi bi-plus-square-fill"></span> Tambah Jadwal Video Conference  </button>

		  
			 			<div class="row float-end" >
						
						 <div class="col-md-12">
						 <div class="input-group">
							<input class="form-control border-end-0 border rounded-pill" type="search"  id="example-search-input" id="keyword"  placeholder="Cari  Nama disini..">
							<span class="input-group-append">
								<button class="btn btn-outline-secondary bg-white border-bottom-0 border rounded-pill ms-n5" type="button">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
									
						</div>
								
					   </div>
					   
		 </h5>
		 

		  <!-- Default Table -->
		  <div class="table-responsive">
		  <table class="table table-bordered table-striped  " id="datatableTable">
			<thead>
			  <tr>
				<th scope="col">#</th>
								<th> AKSI </th>
															    <th> AUDITI</th>
															    <th> NAMA MEETING</th>
															    <th> KODE </th>										    
															    <th> PARTISIPAN  </th>
															    
															    <th> TGL MEETING </th>
																<th> SELESAI </th>
																
																<th> JOIN E-MEET </th>
				
				
			  </tr>
			</thead>
			<tbody>
			  
			</tbody>
		  </table>
		</div>
		 
		</div>
	  </div>


	</div>

  </div>
</section>

</main>
    
<div class="modal fade" id="basicModal" tabindex="-1">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title"></h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body" id="loadform">
					<p>Loading...</p>
                    </div>
                    <div class="modal-footer">
                     
                    </div>
                  </div>
                </div>
</div>

	
<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					
					
					"ajax":{
						url :"<?php echo site_url("vicon/grid"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						data.trkelas_id = $("#trkelas_id").val();
						data.keyword = $("#keyword").val();
						data.ajaran = $("#ajaran").val();
						
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
				
				$(document).on("input","#keyword",function(){
	  
				  dataTable.ajax.reload(null,false);	
				  
			  });
			  $(document).on("change","#trkelas_id,#ajaran",function(){
	  
				  dataTable.ajax.reload(null,false);	
				  
			  });
	


</script>
				