
<?php

$kelas    = $this->db->get_where("kegiatan",array("id"=>$kegiatan_id))->row();
$vc    = $this->db->get_where("tr_vicon",array("id"=>$id))->row();
$mulai = strtotime($vc->tgl_mulai);
$tgl_selesai = strtotime($vc->tgl_selesai);
$now = time();
?>

<style>
         .watermark{
			 
			 display:none;
			 
		 }
            .toolbox {
                position: absolute;
                bottom: 0px;
                border:1px red  solid;
                width: 100%;
                height:50px;
            }
 </style>

<script src="https://meet.jit.si/external_api.js"></script>
	

<main id="main" class="main">

<div class="pagetitle">
  <h1><?php echo $title; ?></h1>
  <nav>
	<ol class="breadcrumb">
	  <li class="breadcrumb-item"><a href="index.html">e-Audit</a></li>
	
	  <li class="breadcrumb-item active"><?php echo $title; ?></li>
	</ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
	<div class="col-lg-12">

	  <div class="card">
		<div class="card-body">
		  <h5 class="card-title"> <i class="fa fa-camera"></i> Video Conference - <?php echo $vc->nama; ?> </h5>
		  <div id='jitsi-meet-conf-container'></div>
		</div>
	 </div>
	 </div>
	 </div>
	 </section>
	 </main>
		    
<?php 
		$namaguru = str_replace("'","",$_SESSION['nama']);
		// if($now >= $mulai) {
		// 	if($tgl_selesai ==""){
											
		?>
<script type="text/javascript">
 StartMeeting();
var apiObj = null;
var base_url = "<?php echo base_url(); ?>";
function StartMeeting(){
	
	

    const domain = 'meet.jit.si';

    var  id   = "<?php echo $vc->id; ?>";
    
    const options = {
        roomName: "<?php echo $vc->kode; ?>",
        width: '100%',
        height: 600,
        parentNode: document.querySelector('#jitsi-meet-conf-container'),
        userInfo: {
            displayName: '<?php echo $namaguru; ?>'
        },
         configOverwrite:{
            
            startWithAudioMuted: false,
            enableWelcomePage: true,           
            disableRemoteMute: true
            
        },
		
		
          interfaceConfigOverwrite: {
             filmStripOnly: false,
             SHOW_JITSI_WATERMARK: false,
             SHOW_WATERMARK_FOR_GUESTS: false,
			},
        onload: function () {
           
        }
    };
    apiObj = new JitsiMeetExternalAPI(domain, options);

    apiObj.addEventListeners({
        readyToClose: function () {
            
        }
    });

    apiObj.executeCommand('subject', '<?php echo $vc->nama; ?>');
	apiObj.executeCommand('avatarUrl', '<?php echo base_url(); ?>__statics/img/logo.png');
}

function HangupCall(){
    apiObj.executeCommand('hangup');
}

</script>

<?php 
	// }
	// 	}
?>
