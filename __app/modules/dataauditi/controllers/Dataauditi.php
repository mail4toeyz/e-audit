<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dataauditi extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		if(!$this->session->userdata("is_login")){
			    
			echo $this->Reff->sessionhabis();
			exit();
		
	  }
		  $this->load->model('M_model','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('auditor/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['title']   = "Data Auditi ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			    
			$madrasah = $this->db->get_where("madrasah",array("nsm"=>$val['kode']))->row();
			   
				$no = $i++;
				$records["data"][] = array(
					$no,
			
              	
					$val['kode'],
					$val['nama'],
					$madrasah->nsm,
					$madrasah->password
					
					
					
				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	
	 
}
