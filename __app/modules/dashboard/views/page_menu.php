<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

      <li class="nav-item">
        <a class="nav-link " href="<?php echo site_url('dashboard'); ?>">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->

      <li class="nav-heading">Jadwal Kegiatan</li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo site_url("jadwal/create"); ?>">
          <i class="bi bi-plus-circle"></i>
          <span>Buat Penugasan </span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo site_url("jadwal"); ?>">
        <i class="bi bi-menu-button-wide"></i>
          <span>Data Penugasan </span>
        </a>
      </li>

      

     
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo site_url("login"); ?>">
          <i class="bi bi-file-earmark"></i>
          <span>Logout</span>
        </a>
      </li><!-- End Blank Page Nav -->

    </ul>

  </aside><!-- End Sidebar-->