
<div class="col-md-12">
		
        <div class="card">
         <div class="card-header card-header-success">
           <h4 class="card-title">Lembar Jawaban Wawancara  <?php 
          
            $jml_soal  = 20;
             
           
           ?></h4>
           <p class="card-category"> Dibawah ini adalah lembar jawaban Peserta </p>
         </div>
       
          <div class="card-body">
         
        
<?php 

?>

             <div class="table-responsive">
                 <table class="table table-bordered table-striped table-hover " id="datatableTable_hasil" width="99%">
                     <thead class="bg-blue">
                         <tr>
                             <th width="2px" rowspan="2">NO</th>
                             <th rowspan="2">NO TEST </th>                                            
                             <th rowspan="2">PESERTA  </th>    
                             <th rowspan="2">KATEGORI  </th>    
                             
                             <th colspan="<?php echo $jml_soal; ?>">NOMOR SOAL  </th>    
                            
                           
                             
                         
                         </tr>

                         <tr>
                         <?php 
                              
                              $no=1;
                                for($a=1;$a<=$jml_soal;$a++){
                                    ?>  <th> <?php echo  $no++ ?> </th> <?php 


                                }
                            ?>
                        </tr>
                     </thead>
                    
                     <tbody>
                   <?php 
                     $datasiswa = $this->db->query("select * from tm_siswa order by kategori asc,nama asc")->result();
                     $no=1;
                     
                      foreach($datasiswa as $ds){
                         
                        ?>
                        <tr> 
                          <td><?php echo $no++; ?></td>
                          <td><?php echo $ds->no_test; ?></td>
                          <td><?php echo $ds->nama; ?></td>
                          <td><?php echo $this->Reff->get_kondisi(array("id"=>$ds->kategori),"tm_kategori","nama"); ?></td>
                       
                          <?php 
                              
                              $arjaw =0;
                           

                                $soal = $this->db->query("select id from tm_wawancara ")->result();

                                $jawaban = $this->Reff->get_kondisi(array("tmsiswa_id"=>$ds->id),"h_ujian_w","list_jawaban");
                                $pc_jawaban = explode(",", $jawaban);


                             
                             
                                foreach($soal as $rs){
                                    $pc_dt 		= explode(":", $pc_jawaban[$arjaw]);

                                    ?>  <td> <?php echo $pc_dt[1]; ?> </td> <?php 

                                    $arjaw++;
                                }
                            ?>
                          
                        
                        </tr>
                    <?php 
                      }
                      ?>
                         
                     </tbody>
                 </table>
          </div>
          </div>
          </div>
  
</div> 
<div id="defaultModal" class="modal fade" tabindex="-1" role="dialog">
<div class="modal-dialog modal-lg">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
FORM SISWA
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body" id="loadform">
<p>Loading...</p>
</div>

</div>

</div>
</div>

<script type="text/javascript">
var dataTable = $('#datatableTable_hasil').DataTable( {
         
      "dom": 'Blfrtip',
      "sPaginationType": "full_numbers",
     "buttons": [
     
              
             {
             extend: 'excelHtml5',
             
             text:'Cetak Excel',
         
             },
             
             
             
             
         
             {
             extend: 'colvis',
             
             text:' Pengaturan Kolom ',
             
         }
     ]
 } );
 
 
 


</script>
 