<script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/tinymce/tinymce.min.js"></script>

<main id="main" class="main">

<div class="pagetitle">
  <h1><?php echo $title; ?></h1>
  <nav>
	<ol class="breadcrumb">
	  <li class="breadcrumb-item"><a href="#">e-Audit</a></li>
	
	  <li class="breadcrumb-item active"><?php echo $title; ?></li>
	</ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
  <div class="card">
		<div class="card-body">
					<form action="javascript:void(0)" method="post" id="simpannorespon" name="simpannorespon" url="<?php echo site_url("lha/saveRevisi"); ?>">
						<input type="hidden" class="form-control"  name='id' value="<?php echo (isset($data)) ? $data->id :""; ?>" >
						<input type="hidden" class="form-control"  name='lhajenissub_id' value="<?php echo $key; ?>" >

								<div class="col-lg-12">
									<textarea class="form-control tinymce-editor" name="desk"  rows="3" required><?php  echo $data->desk; ?> </textarea>
								</div>
							<br>

							<div class="text-center">
							<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan </button>
							<a href="<?php echo site_url("lha?key=".base64_encode($jenis_sub->id)); ?>" class="btn btn-secondary"><i class="fa fa-history"></i> Kembali</a>
							</div>
					</form>

 		 </div>
 	 </div>
  </div>
</section>

</main>

	


