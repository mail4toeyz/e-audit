

<?php 
 $key = base64_decode($this->input->get_post("key"));
?>
<main id="main" class="main">

<div class="pagetitle">
  <h1><?php echo $title; ?></h1>
  <nav>
	<ol class="breadcrumb">
	  <li class="breadcrumb-item"><a href="#">e-Audit</a></li>
	
	  <li class="breadcrumb-item active"><?php echo $title; ?></li>
	</ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
	<div class="col-lg-12">

	    					<?php 
							  $jenis = $this->db->order_by("urutan","ASC")->get_where("lha_jenis",array("subjenis"=>$kegiatan->jenis))->result();
							    foreach($jenis as $je){
							?>
									<div class="card">
											<div class="card-body">
											<h5 class="card-title" style="font-weight:bold"><?php echo $je->nama; ?></h5>

											<!-- Default Accordion -->
											<div class="accordion" id="accordionExample">
												
											<?php 
												$jenis = $this->db->order_by("tipe","ASC")->get_where("lha_jenissub",array("jenis_lha"=>$je->id))->result();
													foreach($jenis as $jes){
												?>
											    <div class="accordion-item">
												<h2 class="accordion-header" id="headingOne">
													<button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne<?php echo $jes->id; ?>" aria-expanded="true" aria-controls="collapseOne">
														<?php echo $jes->nama; ?>
													</button>
												</h2>
												<div id="collapseOne<?php echo $jes->id; ?>" class="accordion-collapse collapse <?php echo ($jes->id==$key) ? "show":""; ?> " aria-labelledby="headingOne" data-bs-parent="#accordionExample">
													<div class="accordion-body">
														<?php 
														 
														 $lha = $this->db->get_where("lha",array("lhajenissub_id"=>$jes->id,"kegiatan_id"=>$_SESSION['kegiatan_id']))->row();

														   if($jes->id==1){

															    $template ='
																<ol start="1">
																	<li>Undang-Undang Nomor 20 Tahun 2003 Tentang Sistem Pendidikan Nasional;</li>
																	<li>Peraturan Pemerintah Nomor 60 Tahun 2008 tentang Sistem Pengendalian Intern Pemerintah;</li>
																	<li>Keputusan Presiden Nomor 103 Tahun 2001 tentang Kedudukan, Tugas, Fungsi, Kewenangan, Susunan Organisasi dan Tata Kerja LPND yang telah beberapa kali diubah, terakhir dengan Peraturan Presiden Nomor 64 Tahun 2005;</li>
																	<li>Peraturan Pemerintah Nomor 19 Tahun 2017 tentang Perubahan Atas Peraturan Pemerintah Nomor 74 Tahun 2008 tentang Guru;</li>
																	<li>Peraturan Menteri Keuangan Nomor 197 Tahun 2017 tentang Rencana Penarikan Dana, Rencana Penerimaan Dana, dan Perencanaan Kas;</li>
																	<li>Peraturan Menteri Agama Nomor 42 Tahun 2016 tentang Organisasi dan Tata Kerja Kementerian Agama (Berita Negara Republik Indonesia Tahun 2016 Nomor 1495) sebagaimana telah diubah dengan Peraturan Menteri Agama Nomor 72 Tahun 2022 tentang Organisasi dan Tata Kerja Kementerian Agama;</li>
																	<li>Peraturan Menteri Agama Nomor 41 Tahun 2016 tentang Pengawasan Internal pada Kementerian Agama;</li>
																	<li>Keputusan Direktur Jenderal Pendidikan Islam Nomor 6065 Tahun 2021 tentang Petunjuk Teknis Bantuan Operasional Pendidikan dan Bantuan Operasiona Sekolah Pada Madrasah Tahun Anggaran 2022;</li>
																	<li>Surat Tugas Inspektur Jenderal Kementerian Agama Nomor '.$kegiatan->no_surat.' Tanggal '.$this->Reff->formattanggalstring($kegiatan->tgl_surat).'</li>
																	</ol>';
																	 echo is_null($lha) ? $template : $lha->desk; 
															   ?>
															    

																
																<textarea style="display:none" id="nama" jenis="<?php echo $jes->id; ?>"><?php echo is_null($lha) ? $template : $lha->desk; ?></textarea>
																 <?php 
																   if(is_null($lha)){
																	 $this->db->set("kegiatan_id",$_SESSION['kegiatan_id']);
																	 $this->db->set("lhajenissub_id",$jes->id);
																	 $this->db->set("desk",$template);
																	 $this->db->insert("lha");

																   }
																   ?>

																


															  <?php 

														   }else if($jes->id==2){
															$template ='<p>Audit kinerja BOS tahun anggaran '.$kegiatan->periode.' bertujuan untuk menilai keberhasilan pelaksanaan program BOS dan memberikan rekomendasi perbaikan jika dijumpai kelemahan dalam kaitannya dengan efektivitas dan efisiensi dari pelaksanaan program tersebut. Keberhasilan pelaksanaan program dijabarkan dalam ”Lima Tepat”, yaitu: </p>
															   <ol start="1">
																	<li>Tepat Sasaran, yang meliputi kriteria madrasah penerima BOS dan kriteria siswa yang mendapatkan bantuan dana BOS.</li>
																	<li>Tepat Jumlah, berupa ketepatan jumlah dana BOS diterima oleh madrasah sesuai dengan SK alokasi dan jumlah riil siswa pada masing-masing madrasah.</li>
																	<li>Tepat Waktu, yang meliputi ketepatan waktu dana BOS diterima di rekening penampung, di rekening madrasah serta ketepatan waktu saat dana BOS dibutuhkan oleh madrasah.</li>
																	<li>Tepat Guna, meliputi ketepatan penggunaan dana BOS sesuai dengan Buku Panduan BOS, prosedur pencairan dan penggunaan sesuai buku panduan BOS, serta penggunaan dana BOS secara transparan dan dapat dipertanggungjawabkan.</li>
																	<li>Tepat Prosedur, meliputi ketepatan persyaratan administrasi madrasah penerima BOS, serta ketepatan administrasi pertanggungjawaban penggunaan dana BOS sesuai dengan buku panduan BOS.</li>
																
																</ol>';
																 echo is_null($lha) ? $template : $lha->desk; 

															?>
															
																<textarea style="display:none" id="nama" jenis="<?php echo $jes->id; ?>"><?php echo is_null($lha) ? $template : $lha->desk; ?></textarea>
																 <?php 
																   if(is_null($lha)){
																	 $this->db->set("kegiatan_id",$_SESSION['kegiatan_id']);
																	 $this->db->set("lhajenissub_id",$jes->id);
																	 $this->db->set("desk",$template);
																	 $this->db->insert("lha");

																   }
																   ?>

															<?php 
											                 }else if($jes->id==3){
																$jmlMadrasahNegeri = $this->db->query("SELECT count(id) as jml from satker where SUBSTRING(kode, 4, 1)='1' and id IN(".$kegiatan->satker_id.")")->row();
																$jmlMadrasahSwasta = $this->db->query("SELECT count(id) as jml from satker where SUBSTRING(kode, 4, 1)='2' and id IN(".$kegiatan->satker_id.")")->row();
																$template ='<ol start="1"><li>Sasaran Audit <br> Audit kinerja BOS dilaksanakan pada '.$jmlMadrasahNegeri->jml.' madrasah negeri dan '.$jmlMadrasahSwasta->jml.' madrasah swasta di lingkungan Kantor Kementerian Agama '.ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$kegiatan->kota_id),"kota","nama"))).' Provinsi '.ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$kegiatan->provinsi_id),"provinsi","nama"))).'. </li><li>Ruang Lingkup Audit <br> Audit kinerja program BOS Tahun Anggaran 2022 merupakan audit kinerja yang dilaksanakan untuk menilai tingkat keberhasilan pelaksanaan program dimulai dari tahap perencanaan, pelaksanaan (penyaluran dan penggunaan dana), evaluasi sampai dengan pelaporan. Selain audit atas kegiatan program, juga dilakukan audit atas kegiatan pengelolaan keuangan, termasuk pengelolaan di lingkungan madrasah negeri dan madrasah swasta penerima BOS. </li></ol>';
																echo is_null($lha) ? $template : $lha->desk; 
	
																?>
																
																	<textarea style="display:none" id="nama" jenis="<?php echo $jes->id; ?>"><?php echo is_null($lha) ? $template : $lha->desk; ?></textarea>
																	 <?php 
																	   if(is_null($lha)){
																		 $this->db->set("kegiatan_id",$_SESSION['kegiatan_id']);
																		 $this->db->set("lhajenissub_id",$jes->id);
																		 $this->db->set("desk",$template);
																		 $this->db->insert("lha");
	
																	   }
																	   ?>
	
																<?php 
															 }else if($jes->id==4){
																	$template ='Periode yang diaudit adalah '.$this->Reff->formattanggalstring($kegiatan->tgl_mulai).' sampai dengan '.$this->Reff->formattanggalstring($kegiatan->tgl_selesai).'.';
																	echo is_null($lha) ? $template : $lha->desk; 
		
																	?>
																	
																		<textarea style="display:none" id="nama" jenis="<?php echo $jes->id; ?>"><?php echo is_null($lha) ? $template : $lha->desk; ?></textarea>
																		 <?php 
																		   if(is_null($lha)){
																			 $this->db->set("kegiatan_id",$_SESSION['kegiatan_id']);
																			 $this->db->set("lhajenissub_id",$jes->id);
																			 $this->db->set("desk",$template);
																			 $this->db->insert("lha");
		
																		   }
																		   ?>
		
																	<?php 
															  }else if($jes->id==5){
																	$template ='Tanggungjawab auditor adalah sebatas pada simpulan yang diperoleh dari hasil audit yang telah dilaksanakan.';
																	echo is_null($lha) ? $template : $lha->desk; 
		
																	?>
																	
																		<textarea style="display:none" id="nama" jenis="<?php echo $jes->id; ?>"><?php echo is_null($lha) ? $template : $lha->desk; ?></textarea>
																		 <?php 
																		   if(is_null($lha)){
																			 $this->db->set("kegiatan_id",$_SESSION['kegiatan_id']);
																			 $this->db->set("lhajenissub_id",$jes->id);
																			 $this->db->set("desk",$template);
																			 $this->db->insert("lha");
		
																		   }
																		   ?>
		
																	<?php 
																}else if($jes->id==6){
																	$template ='Audit kinerja BOS dilakukan berdasarkan Standar Audit Aparat Pengawasan Internal Pemerintah (APIP) dan prosedur lain yang dianggap perlu sesuai dengan keadaan, yang meliputi: audit atas dokumen perencanaan, pelaksanaan, evaluasi sampai dengan pelaporan, wawancara kepada para pelaksana program, penyebaran kuesioner serta konfirmasi dan observasi lapangan.';
																	echo is_null($lha) ? $template : $lha->desk; 
		
																	?>
																	
																		<textarea style="display:none" id="nama" jenis="<?php echo $jes->id; ?>"><?php echo is_null($lha) ? $template : $lha->desk; ?></textarea>
																		 <?php 
																		   if(is_null($lha)){
																			 $this->db->set("kegiatan_id",$_SESSION['kegiatan_id']);
																			 $this->db->set("lhajenissub_id",$jes->id);
																			 $this->db->set("desk",$template);
																			 $this->db->insert("lha");
		
																		   }
																		   ?>
		
																	<?php 
																#Bag II
																}else if($jes->id==8){
																	$template ='<ol  style="list-style-type: lower-alpha;">
																	<li>Kepala Kantor Kementerian Agama '.ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$kegiatan->kota_id),"kota","nama"))).'
																		<table border="0" width="100%">
																			<tr>
																			  <td width="20%"> Nama </td>
																			  <td width="2%"> : </td>
																			  <td>  </td>
																			</tr>
																			<tr>
																			  <td width="20%"> NIP </td>
																			  <td width="2%"> : </td>
																			  <td>  </td>
																			</tr>
																			<tr>
																			  <td width="20%"> Pangkat/Gol. </td>
																			  <td width="2%"> : </td>
																			  <td>  </td>
																			</tr>
																			<tr>
																			  <td width="20%"> Alamat Kantor </td>
																			  <td width="2%"> : </td>
																			  <td>  </td>
																			</tr>
																		 </table>
																	
																	</li>
																	
																	<li>Kepala Seksi Pendidikan Madrasah
																		<table border="0" width=100%">
																			<tr>
																			  <td width="20%"> Nama </td>
																			  <td width="2%"> : </td>
																			  <td>  </td>
																			</tr>
																			<tr>
																			  <td width="20%"> NIP </td>
																			  <td width="2%"> : </td>
																			  <td>  </td>
																			</tr>
																			<tr>
																			  <td width="20%"> Pangkat/Gol. </td>
																			  <td width="2%"> : </td>
																			  <td>  </td>
																			</tr>
																			<tr>
																			  <td width="20%"> Alamat Kantor </td>
																			  <td width="2%"> : </td>
																			  <td>  </td>
																			</tr>
																		 </table>
																	
																	</li>
																	
																	</ol>';
																	echo is_null($lha) ? $template : $lha->desk; 
		
																	?>
																	
																		<textarea style="display:none" id="nama" jenis="<?php echo $jes->id; ?>"><?php echo is_null($lha) ? $template : $lha->desk; ?></textarea>
																		 <?php 
																		   if(is_null($lha)){
																			 $this->db->set("kegiatan_id",$_SESSION['kegiatan_id']);
																			 $this->db->set("lhajenissub_id",$jes->id);
																			 $this->db->set("desk",$template);
																			 $this->db->insert("lha");
		
																		   }
																		   ?>
		
																	<?php 
																}else if($jes->id==9){
																	$template ='<ol  style="list-style-type: lower-alpha;">
																			<li>Kepala Madrasah Negeri
																			<ol style="list-style-type: decimal;">';

																	$madrasahNegeri = $this->db->query("SELECT * from satker where SUBSTRING(kode, 4, 1)='1' and id IN(".$kegiatan->satker_id.")")->RESULT();
																		foreach($madrasahNegeri as $mn){
																			  $madrasahDetail = $this->db->query("SELECT id,nama,alamat from madrasah where nsm='{$mn->kode}'")->row();

																				$template .='<li>) '.$mn->nama.'<br>
																						<table border="0" width="100%">
																							<tr>
																							<td width="20%"> Nama </td>
																							<td width="2%"> : </td>
																							<td>  </td>
																							</tr>
																							<tr>
																							<td width="20%"> NIP </td>
																							<td width="2%"> : </td>
																							<td>  </td>
																							</tr>
																							<tr>
																							<td width="20%"> Pangkat/Gol. </td>
																							<td width="2%"> : </td>
																							<td>  </td>
																							</tr>
																							<tr>
																							<td width="20%"> Alamat  </td>
																							<td width="2%"> : </td>
																							<td>'.ucwords(strtolower($madrasahDetail->alamat)).'</td>
																							</tr>
																						</table>
																					</li>';
																		}
																		
																	$template .='</ol></li>
																	
																	<li>Kepala Madrasah Swasta
																		<ol style="list-style-type: decimal;">';
																		$madrasahNegeri = $this->db->query("SELECT * from satker where SUBSTRING(kode, 4, 1)='2' and id IN(".$kegiatan->satker_id.")")->RESULT();
																		foreach($madrasahNegeri as $mn){
																			$madrasahDetail = $this->db->query("SELECT id,nama,alamat from madrasah where nsm='{$mn->kode}'")->row();
																				$template .='<li>) '.$mn->nama.'<br>
																						<table border="0" width="100%">
																							<tr>
																							<td width="20%"> Nama </td>
																							<td width="2%"> : </td>
																							<td>  </td>
																							</tr>
																							<tr>
																							<td width="20%"> NIP </td>
																							<td width="2%"> : </td>
																							<td>  </td>
																							</tr>
																							<tr>
																							<td width="20%"> Pangkat/Gol. </td>
																							<td width="2%"> : </td>
																							<td>  </td>
																							</tr>
																							<tr>
																							<td width="20%"> Alamat  </td>
																							<td width="2%"> : </td>
																							<td>'.ucwords(strtolower($madrasahDetail->alamat)).'</td>
																							</tr>
																						</table>
																					</li>';
																		}
																		
																	$template .='</ol></li></ol>';
																	echo is_null($lha) ? $template : $lha->desk; 
		
																	?>
																	
																		<textarea style="display:none" id="nama" jenis="<?php echo $jes->id; ?>"><?php echo is_null($lha) ? $template : $lha->desk; ?></textarea>
																		 <?php 
																		   if(is_null($lha)){
																			 $this->db->set("kegiatan_id",$_SESSION['kegiatan_id']);
																			 $this->db->set("lhajenissub_id",$jes->id);
																			 $this->db->set("desk",$template);
																			 $this->db->insert("lha");
		
																		   }
																		   ?>
		
																	<?php 
																}
														   ?>

															<div class="d-grid gap-2">
																<hr>
																<a href="<?php echo site_url('lha/edit?key='.base64_encode($jes->id)); ?>" class="btn btn-outline-primary btn-block" ><i class="bi bi-pencil-fill"></i> Update  </a>
															</div>
													</div>
												</div>
												</div>
												<?php 
												}
												?>


												
											</div>

											</div>
										</div>
								<?php 
								}
								?>


	</div>

  </div>
</section>

</main>

<div class="modal fade" id="largeModal" tabindex="-1">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Detail </h5>
                      
                    </div>
                    <div class="modal-body" id="load-body">
				 		Mohon Tunggu ..
                     
                    </div>
					<div class="modal-footer">
						<button type="button"  class="btn btn-default" data-bs-dismiss="modal">Tutup</button>
					</div>
                   
                  </div>
                </div>
 </div>
	


<script type="text/javascript">


				


</script>
