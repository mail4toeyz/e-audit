<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lha extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		if(!$this->session->userdata("idAuditor")){
			    
			echo $this->Reff->sessionhabis();
			exit();
		
	  }
		  $this->load->model('M_ruang','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('auditor/page_header',$data);	
	}
		
	public function index()
	{  
	   
	    		
         $ajax            = $this->input->get_post("ajax",true);
		 $data['kegiatan']  = $this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
		 $data['title']   = "Laporan Hasil Audit";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}

	public function edit(){
		$ajax     			 = $this->input->get_post("ajax",true);
		$key       			= base64_decode($this->input->get_post("key",true));
		$data['kegiatan']   = $this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
		$data['jenis_sub']  = $this->db->get_where("lha_jenissub",array("id"=>$key))->row();
		$data['data'] 	    = $this->db->get_where("lha",array("lhajenissub_id"=>$key,"kegiatan_id"=>$_SESSION['kegiatan_id']))->row();
		$data['title']   = $this->Reff->get_kondisi(array("id"=>$data['jenis_sub']->jenis_lha),"lha_jenis","nama")." - ".$data['jenis_sub']->nama;
		if(!empty($ajax)){
					   
			$this->load->view('page_edit',$data);
	   
		}else{
			
			$data['konten'] = "page_edit";
			
			$this->_template($data);
		}
 
		

	}

	public function saveRevisi(){
		$id 	= $this->input->get_post("id",true);
		$desk   = $_POST['desk'];

		$this->db->set("tgl_revisi",date("Y-m-d H:i:s"));
		$this->db->set("desk",$desk);
		$this->db->where("id",$id);
		$this->db->update("lha");
		echo "sukses";
	}

	
	 
}
