 <style>
 .progress {
  height: 20px;
  margin-bottom: 20px;
  overflow: hidden;
  background-color: #f5f5f5;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
  box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
}
.progress-bar {
  float: left;
  width: 0%;
  height: 100%;
  font-size: 12px;
  line-height: 20px;
  color: #fff;
  text-align: center;
  background-color: #337ab7;
  -webkit-box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);
  box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);
  -webkit-transition: width 0.6s ease;
  -o-transition: width 0.6s ease;
  transition: width 0.6s ease;
}
.progress-striped .progress-bar,
.progress-bar-striped {
  background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  -webkit-background-size: 40px 40px;
  background-size: 40px 40px;
}
.progress.active .progress-bar,
.progress-bar.active {
  -webkit-animation: progress-bar-stripes 2s linear infinite;
  -o-animation: progress-bar-stripes 2s linear infinite;
  animation: progress-bar-stripes 2s linear infinite;
}
.progress-bar-success {
  background-color: #5cb85c;
}
.progress-striped .progress-bar-success {
  background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
}
.progress-bar-info {
  background-color: #5bc0de;
}
.progress-striped .progress-bar-info {
  background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
}
.progress-bar-warning {
  background-color: #f0ad4e;
}
.progress-striped .progress-bar-warning {
  background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
}
.progress-bar-danger {
  background-color: #d9534f;
}
.progress-striped .progress-bar-danger {
  background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
}

</style>
 

 <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Singkronisasi Data E-learning</h4>
                  <p class="card-category">Anda wajib melakukan singkronisasi data Siswa dan Guru ke server pusat  </p>
                </div>
                <div class="card-body">
				
				<button class="btn btn-primary btn-sm" id="singkronisasiguru"><i class="fa fa-refresh" aria-hidden="true"></i>  lakukan singkronisasi </button>
				
				
                 <b style="color:red">
				  
				  <br>
				  
				   
				  SINGKRONISASI  DATA ELEARNING  </b>
				  <div class="alert alert-danger">
				   Setelah Anda melakukan entry data guru,siswa dan eksekutif silahkan lakukan singkronisasi .
				   singkronisasi bisa dilakukan berulang-ulang.
				  </div>
						    
							  <div class="progress" >
								<div class="progress-bar  progress-bar-info  progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%" id="singkronisasigurudata">
								  
								</div>
							  </div>
							  
							  
							  <br>
							  
							  <hr>
							  
							  <button class="btn btn-primary btn-sm" id="singkronisasiprofile"><i class="fa fa-refresh" aria-hidden="true"></i>  lakukan singkronisasi </button>
				
								  <div class="alert alert-danger">
				   Jika ada perubahan Nama Madrasah Anda tidak sesuai dengan database dalam E-learning, silahkan rubah pada portal elearning (halaman download installer) kemudian lakukan singkronisasi 
				  </div>
				  
							 <b style="color:red">
							  <br>
							  
							  
							  SINGKRONISASI PROFIL MADRASAH (AMBIL DATA DARI SERVER)  </b>
										
										  <div class="progress" >
											<div id="singkronisasiprofiledata" class="progress-bar  progress-bar-info  progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
											  
											</div>
										  </div>
										  
										  
							  <div class="alert alert-danger">
				   Web service Data Guru dan Data siswa serta kalender akademik dapat Anda gunakan untuk publikasi data dalam website Madrasah Anda, localhost:8094 adalah alamat default Aplikasi, jika Anda menggunakan Online Learning, localhost:8094 ganti dengan alamat online learning Anda  :
				   
				   <br>
				    API untu data Guru : <br>
					<u>localhost:8094/api/guru</u>
					
					<br>
				    API untu data Siswa : <br>
					<u>localhost:8094/api/siswa</u>
					
					<br>
				    API untu data Kalender Akademik : <br>
					<u>localhost:8094/api/kalender</u>
				  </div>
							  
							  
							  
							</div>
						  </div>
						</div>
						
						
						
			
<script type="text/javascript">
  $(document).off("click","#singkronisasiguru").on("click","#singkronisasiguru",function(){
	  $("#singkronisasigurudata").attr("style","width:0%");
	  loading();
	  $.post("<?php echo site_url("schoolmadrasah/singkron"); ?>",function(data){
		  
		  
		  $("#singkronisasigurudata").attr("style","width:100%");
		  $("#singkronisasigurudata").text(data);
		  jQuery.unblockUI({ });
		
		  
	  })
	  
	  
  })
  
   $(document).off("click","#singkronisasiprofile").on("click","#singkronisasiprofile",function(){
	  $("#singkronisasiprofiledata").attr("style","width:0%");
	  
	  $.post("<?php echo site_url("schoolmadrasah/singkronprofile"); ?>",function(data){
		  
		
		  $("#singkronisasiprofiledata").attr("style","width:100%");
		  $("#singkronisasiprofiledata").text("Profile diperbaharui");
		  
		  
		  
	  })
	  
	  
  })


  

</script>
            