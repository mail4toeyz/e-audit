<?php

class M_dashboard extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }


    public function grid($paging)
    {
        $query = 'select * from (
            select * from 
            ( 
            SELECT kegiatan_id, groups_id, status as status_approval, notes, tanggal_approve
            FROM tr_status AS a
            WHERE tanggal_approve = (
                SELECT MAX(tanggal_approve)
                FROM tr_status AS b
                WHERE a.kegiatan_id = b.kegiatan_id
            )
            ) s
            left join kegiatan k
            on s.kegiatan_id = k.id
            ) x ';

        if ($paging == true) {
            $query .= 'order by kegiatan_id desc
            LIMIT ' . $_REQUEST['length'] . ' OFFSET ' . $_REQUEST['start'];
        }

        return $this->db->query($query);
    }
}
