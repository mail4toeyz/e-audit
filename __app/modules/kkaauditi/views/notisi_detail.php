


<div class="alert alert-info alert-dismissible fade show" role="alert">
                <i class="bi bi-info-circle me-1"></i>
                <?php echo $kka->nama; ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              <br>
            <form class="row g-3" method="post" action="<?php echo site_url('notisi/saveNotisiData'); ?>">
                <div class="col-12">
                    <input type="hidden" name="satker_id" value="<?php echo $satker_id; ?>">
                    <input type="hidden" name="kka_id" value="<?php echo $kka->id; ?>">
                    <table class="table table-bordered">
                        <tr>
                            <td width="20%"> Kode Temuan </td>
                            
                            <td> 
                                <?php 
                                $temuan = $this->db->get_where("tm_temuan",array("id"=>$notisi->kode_temuan))->row();
                                echo "<b>".$temuan->sub_kel."-".$this->Reff->get_kondisi(array("sub_kel" =>$temuan->sub_kel,"kel"=>1), "tm_temuan", "deskripsi")."</b>";
                                echo "<br>"; 
                                echo $temuan->jenis."-".$temuan->deskripsi;
                                ?>
                                
                            </td>
                        </tr>

                        <tr>
                            <td width="20%"> Kode Sebab </td>
                            
                            <td> 
                                <?php 
                                $temuan = $this->db->get_where("tm_temuan",array("id"=>$notisi->kode_sebab))->row();
                                echo "<b>".$temuan->sub_kel."-".$this->Reff->get_kondisi(array("sub_kel" =>$temuan->sub_kel,"kel"=>2), "tm_temuan", "deskripsi")."</b>";
                                echo "<br>"; 
                                echo $temuan->jenis."-".$temuan->deskripsi;
                                ?>
                                
                            </td>
                        </tr>

                        <tr>
                            <td width="20%"> Kode Rekomendasi </td>
                            
                            <td> 
                                <?php 
                                $temuan = $this->db->get_where("tm_temuan",array("id"=>$notisi->kode_rekomendasi))->row();
                                echo "<b>".$temuan->sub_kel."-".$this->Reff->get_kondisi(array("sub_kel" =>$temuan->sub_kel,"kel"=>3), "tm_temuan", "deskripsi")."</b>";
                                echo "<br>"; 
                                echo $temuan->jenis."-".$temuan->deskripsi;
                                ?>
                                
                            </td>
                        </tr>
                        <tr>
                            <td width="20%"> Kondisi  </td>
                            
                            <td> 
                                <?php 
                                echo $notisi->kondisi;
                                ?>
                                
                            </td>
                        </tr>
                        <tr>
                            <td width="20%"> Kriteria  </td>
                            
                            <td> 
                                <?php 
                                echo $notisi->kriteria;
                                ?>
                                
                            </td>
                        </tr>
                        <tr>
                            <td width="20%"> Akibat  </td>
                            
                            <td> 
                                <?php 
                                echo $notisi->akibat;
                                ?>
                                
                            </td>
                        </tr>
                        <tr>
                            <td width="20%"> Sebab  </td>
                            
                            <td> 
                                <?php 
                                echo $notisi->sebab;
                                ?>
                                
                            </td>
                        </tr>
                        <tr>
                            <td width="20%"> Rekomendasi  </td>
                            
                            <td> 
                                <?php 
                                echo $notisi->rekomendasi;
                                ?>
                                
                            </td>
                        </tr>

                      
                    </table>

                   
              </form>




              