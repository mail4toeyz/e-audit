<script type="text/javascript" src="<?php echo base_url(); ?>__statics/ckeditor/ckeditor.js" ></script>    
  
<ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="overview-tab" data-bs-toggle="tab"
                                data-bs-target="#overview" type="button" role="tab" aria-controls="overview"
                                aria-selected="true">Tanggapan </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="notisi-tab" data-bs-toggle="tab"
                                data-bs-target="#notisi" type="button" role="tab" aria-controls="notisi"
                                aria-selected="false">Notisi </button>
                        </li>
                       

                        
                    </ul>

                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="overview" role="tabpanel"
                            aria-labelledby="overview-tab">



         <?php
           $tanggapan = $this->db->get_where("tr_tanggapan",array("notisi_id"=>$notisi->id))->row();
        
           $disabled="";
           $alert ="";
            if(isset($tanggapan)){ 
                if($tanggapan->ap_anggota_hasil !=0){
                   $disabled  ="disabled";
                   $alert     =' <div class="alert border-primary alert-dismissible fade show" role="alert">
                                   Tanggapan Anda tidak dapat dirubah karena sudah diterima  Auditor
                                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>';

                }

              }
          ?>
          
              <div class="alert alert-info alert-dismissible fade show" role="alert">
                <i class="bi bi-info-circle me-1"></i>
                <?php echo $kka->nama; ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              <?php echo $alert; ?>
              <br>
            <form class="row g-3" method="post" action="javascript:void(0)">
               
                    <input type="hidden" id="notisi_id" value="<?php echo $notisi->id; ?>">
                    <input type="hidden" id="satker_id" value="<?php echo $satker_id; ?>">
                    <input type="hidden" id="kka_id" value="<?php echo $kka->id; ?>">
                    <input type="hidden" id="id" value="<?php if(isset($tanggapan)){ echo $tanggapan->id; }  ?>">

                 
               
                <div class="col-12">
                    <label for="inputNanme4"   class="form-label">Apa tanggapan Anda dengan Notisi tersebut ? </label>
                        <select class="form-control" name="hasil" id="hasil"  <?php echo $disabled; ?>  required>
                            <option value="0">- Pilih Tanggapan -</option>
                            <?php
                        
                            $hasil = array("1"=>"Menerima dan Komitmen","2"=>"Menyanggah");
                            foreach ($hasil as $i=>$h) {
                            ?>
                            <option value="<?php echo $i; ?>" <?php if(isset($tanggapan)){ echo ($tanggapan->hasil==$i) ? "selected":""; }  ?>><?php echo $h; ?> </option>
                            <?php
                            }
                            ?>
                            </select>
                    </div>



            

                <div class="col-12">
                  <label for="inputPassword4" class="form-label">Keterangan  </label>
                  <textarea class="form-control" id="keterangan" <?php echo $disabled; ?> required> <?php if(isset($tanggapan)){ echo $tanggapan->keterangan; }  ?></textarea>
                  <script type="text/javascript">
																
																
                                var kondisi = CKEDITOR.replace( 'keterangan', {
                                        width:"100%",
                                        height:100,
                                        removeButtons: 'NewPage,Save,ExportPdf,Preview,Print,Templates,Image,Table,HorizontalRule,Smiley,About,ShowBlocks,BGColor,TextColor,Link,Unlink,Anchor,Language,BidiRtl,BidiLtr,JustifyLeft,Scayt,SelectAll,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Find,Undo,Cut,Redo,Copy,Replace,Paste,PasteText,PasteFromWord,Source,Styles,Maximize,Format,Font,Iframe,PageBreak,SpecialChar',
                                        allowedContent :true
                               } );
                               
                               
                                                             
                                                     
                               
                                </script>
                </div>

                <div class="col-12">
                     <input id="file" name="file" type="file"  accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps" />
                </div>

             
                <div class="text-center">
                  <button type="submit" class="btn  btn-success btn-submit" <?php echo $disabled; ?>><i class="fa fa-upload"></i> Submit Tanggapan </button>
                 
              </div>
                
              </form>



              <script type="text/javascript">
  
 

  $(document).ready(function() {

$("#file").fileinput({
          uploadUrl: "<?php echo site_url("kkaauditi/saveTanggapan"); ?>",
          overwriteInitial: false,
          showUpload: false,
          showRemove: false,
          browseClass: "btn btn-outline-primary",
          browseLabel: "Ambil File",
          browseIcon: "<i class=\"fa fa-file-signature\"></i> Ambil File ",
          removeClass: "btn btn-danger",
          removeLabel: "Delete",
          removeIcon: "<i class=\"fa fa-trash\"></i>  Hapus File",
          uploadClass: "btn btn-info",
          uploadLabel: "Upload",
          uploadIcon: "<i class=\"fa fa-upload\"></i>  Upload  File",
          initialPreviewAsData: true,
                  uploadExtraData: function() {
                      return {
                         
                          notisi_id: "<?php echo $notisi->id; ?>",
                          keterangan: CKEDITOR.instances['keterangan'].getData(),
                          hasil   : $("#hasil").val(),
                          notisi_id   : $("#notisi_id").val(),
                          satker_id   : $("#satker_id").val(),
                          kka_id   : $("#kka_id").val(),
                          id   : $("#id").val()
                      };
                  },
                  initialPreview: [
                      <?php 
                          
                                 $lihat="";
                              if(!is_null($tanggapan)){
                                 
                                        if($tanggapan->file !=""){
                                      $lihat .= ",'https://drive.google.com/file/d/".$tanggapan->file."/preview'";
                                        }
                               
                                  
                                  
                              }
                          
                          echo substr($lihat,1);
                          
                          
                          ?>
                          
                          
                      ],
                              initialPreviewConfig: [

                            <?php 
                               $lihat="";
                               if(!is_null($tanggapan)){
                                  
                                if($tanggapan->file !=""){
                                  
                                  $lihat .= ',{caption: "", size:200, width: "100%", url: "'.site_url("kkaauditi/hapusTanggapan").'", key: "'.$tanggapan->id.'"}';
                                }
                              }
                                
                                
                              
                            
                            echo substr($lihat,1);


                            ?>


                                
                              ]
                 
                          


                              
                            });


                            $(".btn-submit").on("click", function() {
                                $("#file").fileinput('upload');
                            });

                            $('#file').on('fileuploaded', function(event, previewId, index, fileId) {
                              var form = data.form, files = data.files, extra = data.extra, 
                              response = data.response, reader = data.reader;
                               alertify.alert("Tanggapan berhasil disubmit");
                             location.reload();

                            });
                            $('#file').on('filebatchuploadsuccess', function(event, data, previewId, index) {
                              var form = data.form, files = data.files, extra = data.extra, 
                              response = data.response, reader = data.reader;
                               alertify.alert("Tanggapan berhasil disubmit");
                             location.reload();
                               
                          });


     });		



// function sukses(param){
// 	   let timerInterval;
// 	   Swal.fire({
// 		 type: 'success',
// 		 title: 'Proses Authentication Berhasil ',
// 		 showConfirmButton: false,
// 		  html: 'Dalam <strong></strong> detik<br>Anda akan dialihkan kedalam aplikasi',

// 		 timer: 1000,
// 		 onBeforeOpen: () => {
// 		   Swal.showLoading();
// 		   timerInterval = setInterval(() => {
// 			 Swal.getContent().querySelector('strong')
// 			   .textContent = Swal.getTimerLeft()
// 		   }, 100)
// 		 },
// 		 onClose: () => {
// 		   clearInterval(timerInterval)
// 		 }
// 	   }).then((result) => {
// 		 if (
// 		   /* Read more about handling dismissals below */
// 		   result.dismiss === Swal.DismissReason.timer
		   
// 		 ) {
		
// 		  location.href = base_url+param;
// 		 }
// 	   })
// }
                              

                            </script>


    </div>
    <div class="tab-pane fade " id="notisi" role="tabpanel" aria-labelledby="notisi-tab">
      <?php $this->load->view("notisi_detail"); ?>
    </div>

  </div>