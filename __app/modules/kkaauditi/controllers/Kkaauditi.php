<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kkaauditi extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		if(!$this->session->userdata("is_login")){
			    
			echo $this->Reff->sessionhabis();
			exit();
		
	  }
		  $this->load->model('M_ruang','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('satker/page_header',$data);	
	}
		
	public function data()
	{  
	   
	    		
         $ajax            = $this->input->get_post("ajax",true);	
         $kode            = base64_decode($this->input->get_post("kode",true));	
		 $data['satker']  = $this->db->get_where("satker",array("kode"=>$kode))->row();
		 $data['kegiatan']  = $this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
		 $data['title']   = "Persyaratan Dokumen | ".$data['satker']->nama;
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}

	public function berkas(){

		$data['persyaratan'] = $this->db->get_where("tm_kka",array("id"=>$_POST['id']))->row();
		$this->load->view('berkas',$data);
	}

	public function berkas_upload(){
		$keterangan = $this->input->get_post("keterangan");

		if(isset($_FILES["file"]["name"])){

							$satker     = $this->db->get_where("satker",array("id"=>$_SESSION['satker_id']))->row();
							
							$service 	= new Google_Drive();
							$foldernama = $satker->kode;
							$foldernama = str_replace("/",DIRECTORY_SEPARATOR,$foldernama);
							$folderId   = $satker->folder;

							$temp       = explode(".", $_FILES["file"]["name"]);
							$namafile   = $_SESSION['kegiatan_id']."-".$_POST['kka_id'].str_replace(array(" ","'"),"_",$_FILES["file"]["name"]).time().'.' . end($temp);

							if(!$folderId){
								$folderId = $service->getFileIdByName( BACKUP_FOLDER );
								if( !$folderId ) {
									$folderId = $service->createFolder( BACKUP_FOLDER );					
								}
								$folderId = $service->createMultiFolder($foldernama,$folderId);
								$this->db->set("folder",$folderId);
								
								$this->db->where("id",$_SESSION['satker_id']);
								$this->db->update("satker");
							}

							$fileId = $service->createFileFromPath( $_FILES["file"]['tmp_name'], $namafile, $folderId );
							$service->setPublic($fileId);

							$this->db->set("kegiatan_id",$_SESSION['kegiatan_id']);
							$this->db->set("satker_id",$_SESSION['satker_id']);
							$this->db->set("kka_id",$_POST['kka_id']);
							$this->db->set("keterangan",$keterangan);
							$this->db->set("file",$fileId);
							$this->db->set("tgl_upload",date("Y-m-d H:i:s"));
							$this->db->insert("tr_kegiatan_persyaratan");
							header('Content-Type: application/json');
							echo json_encode(array('success' => true, 'message' => "Upload Sukses"));

			}else{


							$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
							$this->db->where("satker_id",$_SESSION['satker_id']);
							$this->db->where("kka_id",$_POST['kka_id']);
							$this->db->delete("tr_kegiatan_persyaratan");

							$this->db->set("kegiatan_id",$_SESSION['kegiatan_id']);
							$this->db->set("satker_id",$_SESSION['satker_id']);
							$this->db->set("kka_id",$_POST['kka_id']);
							$this->db->set("keterangan",$keterangan);
							
							$this->db->set("tgl_upload",date("Y-m-d H:i:s"));
							$this->db->insert("tr_kegiatan_persyaratan");

							header('Content-Type: application/json');
							echo json_encode(array('success' => true, 'message' => "Upload Sukses"));


			}

	}

	public function hapus_berkas(){

							
							$this->db->where("id",$_POST['key']);
						
							$this->db->delete("tr_kegiatan_persyaratan");
							header('Content-Type: application/json');
							echo json_encode(array('success' => true, 'message' => "Hapus Sukses"));
	}

	public function notisi(){

		$data['satker_id'] 	 = $this->input->get_post("satker_id");
		$data['kka'] 	     = $this->db->get_where("tm_kka",array("id"=>$_POST['id']))->row();

										$this->db->where("satker_id",$data['satker_id']);
										$this->db->where("kka_id",$_POST['id']);
										$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
										$data['notisi'] = $this->db->get("tr_notisi")->row();
		$this->load->view('notisi',$data);
	}
	

	public function dataNotisiSatker(){

		$ajax            = $this->input->get_post("ajax",true);	
		$kode            = base64_decode($this->input->get_post("kode",true));	
		$data['satker']  = $this->db->get_where("satker",array("kode"=>$kode))->row();
		$data['kegiatan']  = $this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
		$data['title']   = "Notisi Audit  | ".$data['satker']->nama;
		if(!empty($ajax)){
					   
			$this->load->view('page_notisi',$data);
	   
		}else{
			
			$data['konten'] = "page_notisi";
			
			$this->_template($data);
		}
	}

	public function notisi_detail(){

		$data['satker_id'] 	 = $this->input->get_post("satker_id");
		$data['kka'] 	     = $this->db->get_where("tm_kka",array("id"=>$_POST['id']))->row();

										$this->db->where("satker_id",$data['satker_id']);
										$this->db->where("kka_id",$_POST['id']);
										$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
										$data['notisi'] = $this->db->get("tr_notisi")->row();
		$this->load->view('notisi_detail',$data);
	}

	public function saveTanggapan(){

		  if(isset($_FILES["file"]["name"])){

				$satker    = $this->db->get_where("satker",array("id"=>$_SESSION['satker_id']))->row();

				$service = new Google_Drive();
				$foldernama = $satker->kode;
				$foldernama = str_replace("/",DIRECTORY_SEPARATOR,$foldernama);
				$folderId   = $satker->folder;

				$temp       = explode(".", $_FILES["file"]["name"]);
				$namafile   = "TanggapanNotisi".$_SESSION['kegiatan_id']."-".$_POST['notisi_id'].str_replace(array(" ","'"),"_",$_FILES["file"]["name"]).time().'.' . end($temp);

				if(!$folderId){
					$folderId = $service->getFileIdByName( BACKUP_FOLDER );
					if( !$folderId ) {
						$folderId = $service->createFolder( BACKUP_FOLDER );					
					}
					$folderId = $service->createMultiFolder($foldernama,$folderId);
					$this->db->set("folder",$folderId);
					
					$this->db->where("id",$_SESSION['satker_id']);
					$this->db->update("satker");
				}

				$fileId = $service->createFileFromPath( $_FILES["file"]['tmp_name'], $namafile, $folderId );
				$service->setPublic($fileId);



				$id = $this->input->get_post("id",true);
				 if(empty($id)){

						$this->db->set("kegiatan_id",$_SESSION['kegiatan_id']);
						$this->db->set("satker_id",$_SESSION['satker_id']);
						$this->db->set("notisi_id",$_POST['notisi_id']);
						$this->db->set("keterangan",$_POST['keterangan']);
						$this->db->set("hasil",$_POST['hasil']);
						$this->db->set("file",$fileId);
						$this->db->set("tgl_upload",date("Y-m-d H:i:s"));
						$this->db->insert("tr_tanggapan");

				 }else{

						$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
						$this->db->where("satker_id",$_SESSION['satker_id']);
						$this->db->set("keterangan",$_POST['keterangan']);
						$this->db->set("file",$fileId);
						$this->db->set("hasil",$_POST['hasil']);
						$this->db->where("id",$id);
						$this->db->where("notisi_id",$_POST['notisi_id']);
						
					
						$this->db->update("tr_tanggapan");

				 }


				
				header('Content-Type: application/json');
				echo json_encode(array('success' => true, 'message' => "Upload Sukses"));

			}else{

				$id = $this->input->get_post("id",true);
				 if(empty($id)){

					  if($_POST['hasil'] !=0 ){

						$this->db->set("kegiatan_id",$_SESSION['kegiatan_id']);
						$this->db->set("satker_id",$_SESSION['satker_id']);
						$this->db->set("notisi_id",$_POST['notisi_id']);
						$this->db->set("keterangan",$_POST['keterangan']);
						$this->db->set("hasil",$_POST['hasil']);
						$this->db->set("tgl_upload",date("Y-m-d H:i:s"));
						$this->db->insert("tr_tanggapan");
						
					  }

				 }else{

						$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
						$this->db->where("satker_id",$_SESSION['satker_id']);
						$this->db->set("keterangan",$_POST['keterangan']);
						$this->db->set("hasil",$_POST['hasil']);
						$this->db->where("id",$id);
						$this->db->where("notisi_id",$_POST['notisi_id']);
						
					
						$this->db->update("tr_tanggapan");
						

				 }
				 header('Content-Type: application/json');
				echo json_encode(array('success' => true, 'message' => "Upload Sukses"));



			}
				

	}

	public function hapusTanggapan(){
		$this->db->where("id",$_POST['key']);
						
		$this->db->delete("tr_tanggapan");
		header('Content-Type: application/json');
		echo json_encode(array('success' => true, 'message' => "Hapus Sukses"));

	}
	
	
	
	 
}
