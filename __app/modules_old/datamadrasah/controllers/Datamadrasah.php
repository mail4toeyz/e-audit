<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datamadrasah extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 if(!$this->session->userdata("aksi_id")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_guru','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('admin/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
	
		 $data['title']   = "Data Madrasah ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {

			  $android = $this->db->query("select count(id) as jml from h_ujian where tmsiswa_id in(select id from tm_siswa where tmmadrasah_id='".$val['id']."')  and device='2'")->row();
			  $laptop  = $this->db->query("select count(id) as jml from h_ujian where tmsiswa_id in(select id from tm_siswa where tmmadrasah_id='".$val['id']."')  and device='1'")->row();
				  
			   $v           = $this->db->query("select count(id) as jml from tm_siswa where   tmmadrasah_id='".$val['id']."'")->row();			   

			   $mengerjakan = $this->db->query("select count(id) as jml from h_ujian where tmsiswa_id in(select id from tm_siswa where tmmadrasah_id='".$val['id']."')")->row();

               $prosentase   = (($mengerjakan->jml/$sesi->jml) * 100);

				$no = $i++;
				$records["data"][] = array(
					$no,
					$this->Reff->get_kondisi(array("id"=>$val['jenjang']),"tm_kategori","nama"),
					$val['nama'],
					$val['nsm'],
					($val['password']),				
				
					
					$val['alamat'],
				
					$v->jml,				
					$mengerjakan->jml,
					($v->jml-$mengerjakan->jml),
					number_format($prosentase,2)." %",
					$android->jml,
					$laptop->jml,
					
					
					
					
					
				    ' 
					
									
									
					<button type="button" class="btn btn-success btn-sm  ubahmodal" style="display:none"  datanya="'.$val['id'].'" urlnya="'.site_url("datamadrasah/form").'" target="#loadform" data-toggle="modal" data-target="#defaultModal">
                                    <i class="fa fa-pencil"></i>
                      </button>
					  <button type="button" style="display:none" class="btn btn-success btn-sm  hapus" datanya="'.$val['id'].'" urlnya="'.site_url("datamadrasah/hapus").'">
                                    <i class="fa fa-trash"></i>
                      </button>
					  '
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	public function form(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("tm_madrasah",array("id"=>$id));
			   
		   }
		$this->load->view("form",$data);
		
	}
	
	
	
	
	
	
	public function save(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NSM lain.');
         $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
         $this->form_validation->set_message('numeric', '%s: Harus terdiri dari Angka');
				 
		 
			
				$config = array(
				    
				  
				    array('field' => 'f[nama]', 'label' => 'Nama Madrasah  ', 'rules' => 'trim|required'),
					 array('field' => 'password', 'label' => 'Password ', 'rules' => 'trim|required|min_length[6]'),
					  
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
				
				
				 
				
							 
							 if(empty($id)){
								
								   $this->m->insert();
								   
								   
								   echo "Data Berhasil disimpan";	

								
								
							 }else{
								$this->m->update($id); 
								echo "Data Berhasil disimpan";	
								 
							 }
							 
						
					 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}
	
	
	
	
	public function form_excel(){
		
		$id = $this->input->get_post("id");
		$data = array();
		 
		$this->load->view("form_excel",$data);
		
	}
	
	public function import_excel()
		{
				$this->load->library('PHPExcel/IOFactory');
       // error_reporting(0);
	
	 
	    $sheetup   = 0;
	    $rowup     = 1;
		
		
		$fileName = time().".xlsx";
         
        $config['upload_path'] = './__statics/excel/tmp/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size'] = 100000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') ){
			$jupload=0;
              echo $this->upload->display_errors();

			  
		}else{
             
        $media = $this->upload->data('file');
		
        $inputFileName = './__statics/excel/tmp/'.$fileName;
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet($sheetup);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
        
             
			 $jupload=0;
            for ($row = $rowup; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
				
       
			 	
			 	$jenjang           = $_POST['jenjang'];
			 	$nama       	   =  (trim($rowData[0][1]));
			 	$alamat            =  (trim($rowData[0][2]));
			 	$password          =  (trim($rowData[0][3]));
			 	$nsm        	   =  (trim($rowData[0][4]));
			  
				
                 $data = array(
                    "jenjang"=> $jenjang,                 
                    "nama"=> $nama,                 
                    
					"alamat"=> $alamat,                   
					"password"=> $password,                   
					"nsm"=> ($nsm)                  
                           
				  );
				  
				  $jupload++;
				 
               
                   $this->db->insert("tm_madrasah",$data);
				
				
			}
		
			echo "<br><br><center>Data yg di upload : ". $jupload ." Data <br> <a href='".site_url("datamadrasah")."' > Kembali ke Aplikasi </a>";
        
        }
   }
	
	public function hapus(){
		
		$id = $this->input->get_post("id",true);
		
		$this->db->delete("tm_madrasah",array("id"=>$id));
		
		echo "sukses";
	}
	
	
	 
}
