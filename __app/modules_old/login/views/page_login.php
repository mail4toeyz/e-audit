<!DOCTYPE html>
<html>
<head>
	<title>CBT <?php echo $this->Reff->owner(); ?> </title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/login/css/style.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>__statics/school/fa/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/alert/alert.css">
	<script src="<?php echo base_url(); ?>__statics/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>__statics/js/alert/alert.js"></script>
	<link rel="shortcut icon" href="<?php echo base_url(); ?>__statics/img/logo.png">
	<meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
	<img class="wave" src="<?php echo base_url(); ?>__statics/login/img/wave.png">
	<div class="container">
	
		<div class="img">
		 
			<img src="<?php echo base_url(); ?>__statics/login/img/bg.svg">
		</div>
		<div class="login-content">
		
		<form id="loginmadrasah" action="javascript:void(0)" url="<?php echo site_url("login/do_login"); ?>" method="post">
				<img src="<?php echo base_url(); ?>__statics/img/logo.png">
				<h4 class="title"><h3>  <br>  Computer Based Test (CBT)  <br> Seleksi Kepala Madrasah <br> Pendidik  Dan Tenaga Kependidikan Tahun 2021  </h3>   </h4>
           		<div class="input-div one">
           		   <div class="i">
           		   		<i class="fa fa-user"></i>
           		   </div>
           		   <div class="div">
           		   		<h5>Username</h5>
           		   		<input type="text" class="input" name="username" required value="<?php echo $this->input->get("username",true); ?>">
           		   </div>
           		</div>
           		<div class="input-div pass">
           		   <div class="i"> 
           		    	<i class="fa fa-lock"></i>
           		   </div>
           		   <div class="div">
           		    	<h5>Password</h5>
           		    	<input type="password" class="input" name="password" required value="<?php echo $this->input->get("password",true); ?>">
            	   </div>
            	</div>
            	<button type="submit" value="submit" class="btn"> <i class="fa fa-sign-in"></i> Masuk </button> 
            	
			
            	
												
													 <br>
				  <img src="<?php echo base_url(); ?>__statics/img/loading.gif" id="loadingm" style="display:none">
            	
            </form>
			
        </div>
		
		
    </div>
	<script type="text/javascript" src="<?php echo base_url(); ?>__statics/login/js/main.js"></script>
	
	

<script type="text/javascript">
var base_url="<?php echo base_url(); ?>";
   
function sukses(param){
	   let timerInterval;
	   Swal.fire({
		 type: 'success',
		 title: 'Proses Authentication Berhasil ',
		 showConfirmButton: false,
		  html: 'Dalam <strong></strong> detik<br>Anda akan dialihkan kedalam aplikasi',

		 timer: 1000,
		 onBeforeOpen: () => {
		   Swal.showLoading();
		   timerInterval = setInterval(() => {
			 Swal.getContent().querySelector('strong')
			   .textContent = Swal.getTimerLeft()
		   }, 100)
		 },
		 onClose: () => {
		   clearInterval(timerInterval)
		 }
	   }).then((result) => {
		 if (
		   /* Read more about handling dismissals below */
		   result.dismiss === Swal.DismissReason.timer
		   
		 ) {
		
		  location.href = base_url+param;
		 }
	   })
}

function gagal(param){
	   let timerInterval;
	   Swal.fire({
		 type: 'warning',
		 title: 'Proses Authentication Gagal ',
		 showConfirmButton: true,
		  html: param

	   })
}


   $(document).on('submit', 'form#loginmadrasah', function (event, messages) {
	event.preventDefault();
	  var form   = $(this);
	  var urlnya = $(this).attr("url");
   
	 
	
			 $("#loadingm").show();
			  $.ajax({
				   type: "POST",
				   url: urlnya,
				   data: form.serialize(),
				   success: function (response, status, xhr) {
					var ct = xhr.getResponseHeader("content-type") || "";
					   if (ct == "application/json") {
					   
						gagal(response.message);
			   
						
						   
						   
					   } else {
						 
						  
						  sukses(response);
						  
						   
						   
							
						   
						 
					   }
					   
					   $("#loadingm").hide();
					   
				   }
			   });
			 
	   return false;
   });
   
   
   
</script>


</body>
</html>
