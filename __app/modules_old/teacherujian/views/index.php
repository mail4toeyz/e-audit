

<div class="col-md-12">
		
		<div class="card">
		 <div class="card-header card-header-success">
		   <h4 class="card-title">Bank Soal </h4>
		   <p class="card-category">Pembuatan Soal <?php echo $ujian->nama; ?> </p>
		 </div>
	   
		  <div class="card-body">
		 <div class="row" >
						
						
					         <div class="acc-setting" style="height:60px">
							 
							    <div  style="float:left; padding:20px 20px 20px 20px"><i class="la la-briefcase"></i> <b id="statussoal">  <?php echo isset($edit) ? "Mohon Tunggu.." : "Tersimpan"; ?> </b> </div>
							    <div  style="float:right; padding:10px 20px 0px 0px">
								 
									 <a href="javascript:void(0)"  ujian-id="<?php echo $ujian->id; ?>" class="simpansemuaujian btn  btn-info btn-sm" title="Simpan " id="cancelll"  style="background-color:#252b87;color:white;"> <i class="fa fa-save"></i>  Simpan dan Tutup  </a>
								     <a href="#" id="cancel"  class="menuajax btn btn-danger btn-sm" title="List ujian "> Batal </a>
							
								</div>
							  
							  </div>
					<div class="row">
					        
					      
					   
					      
					
							<div class="col-lg-3 col-md-3 ">
								<div class="acc-leftbar">
									<div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        
									    
										
										<a class="nav-item nav-link" id="nav-acc-tab" data-toggle="tab" href="#pertanyaan" role="tab" aria-controls="nav-acc" aria-selected="true">
										<span class="fa-stack">
                                              <span class="fa fa-circle-o fa-stack-2x"></span>
                                                    <strong class="fa-stack-1x">1 </strong>
											</span>
											Buat  Soal 
										</a>
										
										
										
								
										<a class="nav-item nav-link"  id="nav-acc-tab" data-toggle="tab" href="#importsoal" role="tab" aria-controls="nav-acc" aria-selected="true">
										<span class="fa-stack">
                                              <span class="fa fa-circle-o fa-stack-2x"></span>
                                                    <strong class="fa-stack-1x">2 </strong>
											</span>
											Import  Excel
										</a>
										<a class="nav-item nav-link" id="nav-acc-tab" data-toggle="tab" href="#importsoalpdf" role="tab" aria-controls="nav-acc" aria-selected="true">
										<span class="fa-stack">
                                              <span class="fa fa-circle-o fa-stack-2x"></span>
                                                    <strong class="fa-stack-1x">3 </strong>
											</span>
											Import  PDF
										</a>

										<a class="nav-item nav-link" id="nav-acc-tab" data-toggle="tab" href="#banksoal" role="tab" aria-controls="nav-acc" aria-selected="true">
										<span class="fa-stack">
                                              <span class="fa fa-circle-o fa-stack-2x"></span>
                                                    <strong class="fa-stack-1x">4 </strong>
											</span>
											Bank Soal 
										</a>
									


                                   </div>
							    </div><!--acc-leftbar end-->
							</div>
							
							
							   <?php $this->load->view("teacherujian/default"); ?>
							
							
							
					</div>
					
				</div>
				</div>
				</div>
				</div>
<div id="myModalpertanyaan" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color:white">Soal  </h4>
      </div>
      <div class="modal-body" id="loadingbanksoal" >
	  <br>
	  <br>
        <center>Bank Soal Madrasah Masih Kosong </center>
      </div>
      <div class="modal-footer">
      
      </div>
    </div>

  </div>
</div>