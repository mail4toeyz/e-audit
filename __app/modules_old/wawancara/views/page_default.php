<script src="<?php echo base_url(); ?>__statics/js/jquery.maskedinput.min.js"></script>

<div class="content">
        <div class="container-fluid">
          <div class="card" style="font-weight:bold">
            <div class="card-header card-header-danger" >
              <h3 class="card-title" style="font-weight:bold">Selamat Datang <?php echo $_SESSION['nama_aksi']; ?>   </h3>
              <p class="card-category">Petunjuk Wawancara <br>
              <ol start="1">
                  <li>Teknis wawancara dapat dilakukan secara luring atau  daring menggunakan  aplikasi wawancara yang sudah disediakan diaplikasi <a target="_blank" style="color:white" href="https://appmadrasah.kemenag.go.id/cbtpkb/aplikasi/wawancara.pdf">(panduan peserta dapat didownload disini)</a> </li>                  
                  <li>Sebelum melakukan wawancara silahkan lakukan konfirmasi ke Peserta, Setelah peserta siap diwawancara silahkan mulai wawancara dengan klik tombol <b> Mulai  </b></li>
               
                 
               </ol>



                
              </p>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">

                     <?php $this->load->view("page_peserta"); ?>
                 
                </div>

                
              </div>
            </div>




          </div>
        </div>
      </div>


      <script type="text/javascript">

var base_url="<?php echo base_url(); ?>";
jQuery(function($) {
                $.mask.definitions['~']='[+-]';
				
				$('.input-mask-notes').mask('99.99.99999');

});

$(document).off("click","#caripeserta").on("click","#caripeserta",function(){

    var no_test = $("#no_test").val();
    
       $.post("<?php echo site_url('wawancara/caripeserta'); ?>",{no_test:no_test},function(data){

          if(data==0){
              alertify.alert("Nomor Tes tidak ditemukan");

          }else{
         
           location.href = base_url+"wawancara/mulai?id="+data;

          }

       })


})

</script>