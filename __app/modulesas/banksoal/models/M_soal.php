<?php

class M_soal extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
    }

    
	public function grid($paging){
      
	   
	   
		$pengaturan_id  = $this->Reff->set();
		$this->db->select("*");
        $this->db->from('tm_ujian');
        $this->db->where("simulasi !=",1); 
		$this->db->where("pengaturan_id",$pengaturan_id);
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 
					 $this->db->order_by("id","ASC");
					
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
}
