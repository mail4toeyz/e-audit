<div id="showform"></div>

<main id="main" class="main">

	<div class="pagetitle">
		<h1><?php echo $title; ?></h1>
		<nav>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="index.html">e-Audit</a></li>

				<li class="breadcrumb-item active"><?php echo $title; ?></li>
			</ol>
		</nav>
	</div><!-- End Page Title -->

	<section class="section">
		<div class="row">
			<div class="col-lg-12">

				<div class="card">
					<div class="card-body">
						<h5 class="card-title">
							<div class="btn-group">
								<button type="button" id="updateData" class="btn btn-outline-primary addmodal"><span class="bi bi-plus-square-fill"></span> Perbaharui Data </button>
								<!-- <button type="button" target="#loadform" url="<?php echo site_url("unitkerja/form"); ?>" data-bs-toggle="modal" data-bs-target="#basicModal" class="btn btn-outline-primary addmodal"><span class="bi bi-plus-square-fill"></span> Perbaharui Data </button> -->
								<!-- <button type="button" target="#loadform" url="<?php echo site_url("unitkerja/form_excel"); ?>" data-bs-toggle="modal" data-bs-target="#basicModal" class="btn btn-outline-primary addmodal"><i class="bi bi-file-earmark-excel-fill"></i> Import Data </button> -->
							</div>

							<div class="row float-end">
								<div class="col-md-6">
									<!-- <select class="form-control" required id="jabatan_id">
										<option value="">- Tampilkan Jabatan Penugasan -</option>
										<?php
										$jabatan = $this->db->get("jabatan")->result();
										foreach ($jabatan as $r) {

										?><option value="<?php echo $r->id; ?>"> <?php echo ($r->nama); ?></option><?php
																												}
																													?>
									</select> -->

								</div>
								<div class="col-md-6">
									<div class="input-group">
										<input type="hidden" id="unitkerja_id" value="<?php echo $data->kode; ?>">
										<input class="form-control border-end-0 border rounded-pill" type="search" id="keyword" placeholder="Cari  disini..">
										<span class="input-group-append">
											<button class="btn btn-outline-secondary bg-white border-bottom-0 border rounded-pill ms-n5" type="button">
												<i class="fa fa-search"></i>
											</button>
										</span>
									</div>

								</div>

							</div>
					</div>

					</h5>


					<!-- Default Table -->
					<div class="table-responsive">
						<table class="table table-bordered table-striped  " id="datatableTable">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Nama</th>
									<th scope="col">NIP</th>
									<th scope="col">Golongan</th>
									<th scope="col">Jabatan</th>
									<th scope="col">Jabatan Penugasan</th>
									<th scope="col">Kategori</th>
									<th scope="col">Username</th>
									<th scope="col">Password</th>
									<!-- <th scope="col">Aksi</th> -->
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>

				</div>
			</div>


		</div>

		</div>
	</section>

</main>

<div class="modal fade" id="updateModal" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"> Pembaharuan Data</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body" id="content">
				<p>Loading...</p>
			</div>
			<div class="modal-footer">

			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	var dataTable = $('#datatableTable').DataTable({
		"processing": true,
		"language": {
			"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
			"oPaginate": {
				"sFirst": "Halaman Pertama",
				"sLast": "Halaman Terakhir",
				"sNext": "Selanjutnya",
				"sPrevious": "Sebelumnya"
			},
			"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
			"sInfoEmpty": "Tidak ada data yang di tampilkan",
			"sZeroRecords": "Data kosong",
			"sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
		},

		"serverSide": true,
		"searching": false,
		"responsive": false,
		"lengthMenu": [
			[10, 25, 50, 100, 200, 300, 500, 1000, 800000000],
			[10, 25, 50, 100, 200, 300, 500, 1000, "All"]
		],

		"sPaginationType": "full_numbers",
		"dom": 'Blfrtip',


		"ajax": {
			url: "<?php echo site_url("unitkerja/gridSimpeg"); ?>",
			type: "post",
			"data": function(data) {

				data.jabatan_id = $("#jabatan_id").val();
				data.unitkerja_id = $("#unitkerja_id").val();
				data.keyword = $("#keyword").val();



			}

		},
		"rowCallback": function(row, data) {


		}
	});


	$(document).on("input", "#keyword", function() {

		dataTable.ajax.reload(null, false);

	});
	$(document).on("change", "#jabatan_id", function() {

		dataTable.ajax.reload(null, false);

	});

	$(document).on("click", "#updateData", function() {
		$('#updateModal').modal('show');
		$.ajax('unitkerja/getDataPegawai', // request url
			{
				success: function(data, status, xhr) { // success callback function
					//$('#content').html(data);
					alert(data);
				}
			});
		// $('#basicModal').modal('toggle');

		// $('#myModal').modal('hide');

		// alert('aaa');


	})
</script>