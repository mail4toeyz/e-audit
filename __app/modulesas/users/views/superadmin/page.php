<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Post-->
<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
	<div id="kt_content_container" class="container-xxl">
	<div class="card card-custom">
						<div class="card-header">
												<div class="card-title">
													<span class="card-icon">
														<i class="flaticon2-chat-1 text-primary"></i>
													</span>
													<h3 class="card-label"> Superadmin 
													</h3>
												</div>
												<div class="card-toolbar">
													<a href="#" class="btn btn-sm btn-primary font-weight-bold addmodal" target="#loadform" url="<?php echo site_url("users/form_superadmin"); ?>"  data-toggle="modal" data-target="#defaultModal" data-color="cyan" >
													<i class="fa fa-plus"></i> Buat Users </a>
												</div>
											
						                     
						</div>
					  
						 <div class="card-body">
						 <div class="row">
													
													
						                     
													
													
													
													<div class="col-md-4">
													  <input class="form-control" id="keyword"  placeholder="Apa yang Anda cari ?">
													
												   
													</div>
													<div class="col-md-1">
													  <button class="btn btn-sm btn-primary" id="tampilkan" > <i class="fa fa-search"></i> </button> 
												   
													</div>
											   
											</div>
										<br>
						


							<div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
									<tr class="fw-bolder text-muted bg-light">
                                            <th width="2px">NO</th>
                                            <th>NAMA </th>
                                            <th>JABATAN </th>                                           
                                            <th>USERNAME  </th>
                                            <th>WHATSAPP  </th>
                                           
                                            <th>AKSI</th>
										 </tr>

									

                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                         </div>
                         </div>
             </div>
        </div>
                 
 
 
              
        
<div id="defaultModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body" id="loadform">
	 
						
      </div>
      
    </div>

  </div>
</div>
	
<script type="text/javascript">

 

  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"scrollY": 300,
                        "scrollX": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					 buttons: [
							{
								extend: 'excel',
								exportOptions: {
									columns: ':visible'
								}
							},
							'colvis'
						],
                    buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
				
					
					"ajax":{
						url :"<?php echo site_url("users/grid_superadmin"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						
						data.keyword = $("#keyword").val();
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				

			

				$(document).on("input","#keyword",function(){


				   dataTable.ajax.reload(null,false);	
				});
				
				
	


</script>
				