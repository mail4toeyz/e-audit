<div class="card card-custom">
		<div class="card-header">
												<div class="card-title">
													<span class="card-icon">
														<i class="flaticon2-chat-1 text-primary"></i>
													</span>
													<h3 class="card-label"> Madrasah yang akan diverifikasi oleh <b><?php echo $data->nama; ?></b>
													</h3>
												</div>
												
											
						                     
		</div>
					  
		<div class="card-body">
                                         <div class="row">
													
													
                                                          <input type="hidden" value="1" class="form-control" id="status">
                                                          <input type="hidden" value="<?php echo $data->id; ?>" class="form-control" id="verifikator">
                                                             
                                                       
    
    
                                                        <div class="col-md-2">
                                                          <select class="form-control" id="jenjang">
                                                               <option value="">- Jenjang - </option>
                                                               <?php 
                                                                 $jenjang = array("mi","mts","ma");
                                                                   foreach($jenjang as $r){
                                                                       ?><option value="<?php echo $r; ?>"  > <?php echo strtoupper($r); ?> </option><?php 
                                                                       
                                                                   }
                                                                  ?>
                                                              
                                                           
                                                           </select>
                                                       
                                                        </div>
                                                        
                                                        <div class="col-md-3">
                                                    
                                                          <select class="form-control onchange " id="provinsi" target="kota" urlnya="<?php echo site_url('publik/kota'); ?>">
                                                               <option value="">- Provinsi - </option>
                                                               <?php 
                                                                   $provinsi = $this->db->get_where("provinsi",array("active"=>1))->result();
                                                                   foreach($provinsi as $row){
                                                                       ?><option value="<?php echo strtoupper($row->kode); ?>"> <?php echo ($row->nama); ?> </option><?php 
                                                                       
                                                                   }
                                                                  ?>
                                                           
                                                           </select>
                                                       
                                                        </div>
    
                                                        <div class="col-md-3">
                                                    
                                                          <select class="form-control kota " id="kota" target="kota" urlnya="<?php echo site_url('publik/kota'); ?>">
                                                               <option value="">- Kabupaten/Kota - </option>
                                                              
                                                           
                                                           </select>
                                                       
                                                        </div>
                                                        
                                                        
                                                        <div class="col-md-3">
                                                          <input class="form-control" id="keyword"  placeholder="Apa yang Anda cari ?">
                                                          <input class="form-control" type="hidden" id="bantuan" value="<?php echo isset($_GET['bantuan']) ? $_GET['bantuan']:''; ?>">
                                                        
                                                       
                                                        </div>
                                                        <div class="col-md-1">
                                                          <button class="btn btn-sm btn-primary" id="tampilkan" > <i class="fa fa-search"></i> </button> 
                                                       
                                                        </div>
                                                   
                                                </div>


                                  <table class="table table-bordered table-striped table-hover " id="datatableTableMadrasahVerif" width="100%">
                                    <thead class="bg-blue">
									   <tr class="fw-bolder bg-light" style="font-weight:bold">
                                            <th width="2px" >NO</th>                                           
                                            <th>#</th>  										                                         
                                            <th >BANTUAN</th>                                            
                                            <th>NSM </th>
                                            <th>NAMA </th>
                                            <th>PROVINSI </th>
                                            <th>KABKOTA </th>                                                                                   
                                            <th><input type="checkbox" class="all" value="1"> </th>
                                        </tr>

                                    </thead>

                                    <tbody>

                                    </tbody>
                                </table>
                          


        </div>
</div>

<script type="text/javascript">
var datatableTableMadrasahVerif = $('#datatableTableMadrasahVerif').DataTable( {
						"processing": true,
						"scrollY": 500,
                        "scrollX": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[50,100,200,300,500,1000, 800000000], [50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					
					
					
					"ajax":{
						url :"<?php echo site_url("users/gridMadrasah"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						data.status = $("#status").val();
						data.jenjang = $("#jenjang").val();
						data.provinsi = $("#provinsi").val();
						data.kota = $("#kota").val();
						data.keyword = $("#keyword").val();
						data.bantuan = $("#bantuan").val();
						data.statusVisit = $("#statusVisit").val();
						data.verifikator = $("#verifikator").val();
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				

				$(document).on("change","#status,#jenjang,#provinsi,#kota,#statusVisit",function(){


					datatableTableMadrasahVerif.ajax.reload(null,false);	
				});

				$(document).on("input","#keyword",function(){


                    datatableTableMadrasahVerif.ajax.reload(null,false);	
				});

                $(document).off("click",".verifikatorpilih").on("click",".verifikatorpilih",function(){
                    var id       = "<?php echo $data->id; ?>";
                    var madrasah = $("#datatableTableMadrasahVerif input:checkbox:checked").map(function(){
                    return $(this).val();
                    }).get();

                     $.post("<?php echo site_url('users/setVerif'); ?>",{madrasah:madrasah,id:id},function(){
                        dataTable.ajax.reload(null,false);	

                     })
                    
                });

               
				
				
	


</script>