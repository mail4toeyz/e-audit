


<main id="main" class="main">

<div class="pagetitle">
  <h1><?php echo $title; ?></h1>
  <nav>
	<ol class="breadcrumb">
	  <li class="breadcrumb-item"><a href="index.html">e-Audit</a></li>
	
	  <li class="breadcrumb-item active"><?php echo $title; ?></li>
	</ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
	<div class="col-lg-12">

	  <div class="card">
		<div class="card-body">
		<div id="showform"></div>
		
		  <h5 class="card-title">
				<div class="btn-group">
				<button type="button" urlnya="<?php echo site_url("users/form"); ?>"    class="btn btn-outline-primary " id="tambahdata"><span class="bi bi-plus-square-fill"></span> Tambah Data </button>
					</div>
		  
			 			<div class="row float-end" >
						
						 <div class="col-md-12">
						 


						 <div class="input-group">
							<input type="hidden"  id="group_id" value="<?php echo $data->id; ?>">
							<input class="form-control border-end-0 border rounded-pill" type="search"   id="keyword"  placeholder="Cari  disini..">
							<span class="input-group-append">
								<button class="btn btn-outline-secondary bg-white border-bottom-0 border rounded-pill ms-n5" type="button">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
									
						</div>
								
					   </div>
					   </div>
					   
		 </h5>
		 

		  <!-- Default Table -->
		  <div class="table-responsive">
		  <table class="table table-bordered table-striped  " id="datatableTable">
			<thead>
			  <tr>
				<th scope="col">#</th>
				<th scope="col">Unit Kerja </th>
				<th scope="col">Nama Users</th>
				<th scope="col">Users Groups </th>
				<th scope="col">Username </th>
				<th scope="col">Password </th>
					
				<th scope="col">Aksi</th>
			  </tr>
			</thead>
			<tbody>
			  
			</tbody>
		  </table>
		</div>
		 
		</div>
	  </div>


	</div>

  </div>
</section>

</main>
    
<div class="modal fade" id="basicModal" >
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title"> Form <?php echo $data->nama; ?></h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body" id="loadform">
					<p>Loading...</p>
                    </div>
                    <div class="modal-footer">
                     
                    </div>
                  </div>
                </div>
</div>

	
<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					
					
					"ajax":{
						url :"<?php echo site_url("users/grid"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						
						data.group_id = $("#group_id").val();
						data.keyword = $("#keyword").val();
						
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
				
				$(document).on("input","#keyword",function(){
	  
				  dataTable.ajax.reload(null,false);	
				  
			  });
			  $(document).on("change","#jabatan_id",function(){
	  
				  dataTable.ajax.reload(null,false);	
				  
			  });


			  $(document).off("change","#pegawai").on("change","#pegawai",function(){
	  
				var nip = $('option:selected', this).attr('nip');
				var nama = $('option:selected', this).attr('nama');
				var id = $('option:selected', this).attr('id');

				$("#pegawai_id").val(id);
				$("#nama").val(nama);
				$("#username").val(nip);
				$("#password").val(nip);
					
				});

	


</script>


				
