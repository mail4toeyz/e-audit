<?php
ini_set('memory_limit', '-1');

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 if(!$this->session->userdata("admin_id")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_guru','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('pusat/page_header',$data);	
	}
		
	public function superadmin()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Superadmin";
	     if(!empty($ajax)){
					    
			 $this->load->view('superadmin/page',$data);
		
		 }else{
			 
		     $data['konten'] = "superadmin/page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid_superadmin(){
		
		  $iTotalRecords = $this->m->grid_superadmin(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid_superadmin(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			   
				$no = $i++;
				$records["data"][] = array(
					$no,
				    $val['nama'],
					$val['jabatan'],
					
					$val['username'],
					$val['whatsapp'],
					' 
					<button type="button" class="btn btn-primary btn-sm  ubahmodal" datanya="'.$val['id'].'" urlnya="'.site_url("users/form_superadmin").'" target="#loadform" data-toggle="modal" data-target="#defaultModal">
                                    <i class="fa fa-edit"></i>
									
									
					
					  <button type="button" class="btn btn-primary btn-sm  hapus" datanya="'.$val['id'].'" urlnya="'.site_url("users/hapus_superadmin").'">
                                    <i class="fa fa-trash"></i>
                      </button>
					  '
					
					
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	
	public function form_superadmin(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("admin",array("id"=>$id));
			   
		   }
		$this->load->view("superadmin/form",$data);
		
	}
	
	
	
	
	public function save_superadmin(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NUPTK lain.');
         $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
         $this->form_validation->set_message('numeric', '%s: Harus terdiri dari Angka');
				 
		 
			
				$config = array(
				    
				 
				    array('field' => 'f[nama]', 'label' => 'Nama   ', 'rules' => 'trim|required'),
				    array('field' => 'f[username]', 'label' => 'Email  ', 'rules' => 'trim|required'),
					 
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
				
				
				 
				
							 
							 if(empty($id)){
								
								   $this->db->insert("admin",$f);	
								   $narasi = "<b>Bantuan Kinerja dan Afirmasi</b> <br>Yth. ".$f['nama'].",<br>Anda ditunjuk sebagai superadmin Bantuan Kinerja dan Afirmasi, silahkan login kedalam aplikasi : <br>https://appmadrasah.kemenag.go.id/bkba <br>Username  : ".$f['username']." <br>Password  : ".$f['password']."\n  ";
								   //$sendEMail    = array("tujuan"=>$f['username'],"username"=>$f['username'],"password"=>$f['password'],"narasi"=>$narasi);		  
								   //$this->Reff->post('https://appmadrasah.kemenag.go.id/cbtpkb/web/sendAkunBKBA', $sendEMail);
		   
								   $message = "*Bantuan Kinerja dan Afirmasi* \nYth. ".$f['nama']."-".$f['jabatan'].",\nAnda ditunjuk sebagai superadmin Bantuan Kinerja dan Afirmasi, Anda dapat login kedalam aplikasi pada alamat: \nhttps://appmadrasah.kemenag.go.id/bkba \nUsername  : ".$f['username']." \nPassword  : ".$f['password']."\n  ";
								
								   $sendWhatsapp = "sender=aplikasi&number=".$f['whatsapp']."&message=".$message.""; 		  
								   $this->Reff->sendWA($sendWhatsapp);
								   
								   echo "Data Berhasil disimpan";	
								 
								
							 }else{
								$this->db->where("id",$id);
								$this->db->update("admin",$f);
								echo "Data Berhasil disimpan";	
								 
							 }
							 
						
					 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}

	public function hapus_superadmin(){

		$this->db->where("id",$_POST['id']);
		$this->db->delete("admin");
		echo "sukses";
	}


	// Verifikator

	public function verifikator()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Verifikator";
	     if(!empty($ajax)){
					    
			 $this->load->view('verifikator/page',$data);
		
		 }else{
			 
		     $data['konten'] = "verifikator/page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid_verifikator(){
		
		  $iTotalRecords = $this->m->grid_verifikator(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid_verifikator(true)->result_array();
		
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			    $madrasah = explode(",",$val['madrasah']);
				 $list = "<ol>";
				 foreach($madrasah as $rm){
					$list .= "<li>".$this->Reff->get_kondisi(array("id"=>$rm),"madrasahedm","nama")."</li>";

				 }
				 $list .= "<ol>";
				$no = $i++;
				$records["data"][] = array(
					$no,
				    $val['nama'],
					$val['jabatan'],
					
					$val['username'],
					$val['whatsapp'],
					$list,
					' 
					<button type="button" class="btn btn-primary btn-sm  ubahmodal" datanya="'.$val['id'].'" urlnya="'.site_url("users/madrasah_verifikator").'" target="#loadform" data-toggle="modal" data-target="#defaultModal">
                    <i class="fa fa-codepen"></i> Madrasah
					<button type="button" class="btn btn-primary btn-sm  ubahmodal" datanya="'.$val['id'].'" urlnya="'.site_url("users/form_verifikator").'" target="#loadform" data-toggle="modal" data-target="#defaultModal">
                                    <i class="fa fa-edit"></i>
									
									
					
					  <button type="button" class="btn btn-primary btn-sm  hapus" datanya="'.$val['id'].'" urlnya="'.site_url("users/hapus_verifikator").'">
                                    <i class="fa fa-trash"></i>
                      </button>
					  '
					
					
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	
	public function form_verifikator(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("verifikator",array("id"=>$id));
			   
		   }
		$this->load->view("verifikator/form",$data);
		
	}

	public function madrasah_verifikator(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("verifikator",array("id"=>$id));
			   
		   }
		$this->load->view("verifikator/madrasah_verifikator",$data);
		
	}
	
	
	
	
	public function save_verifikator(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NUPTK lain.');
         $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
         $this->form_validation->set_message('numeric', '%s: Harus terdiri dari Angka');
				 
		 
			
				$config = array(
				    
				 
				    array('field' => 'f[nama]', 'label' => 'Nama   ', 'rules' => 'trim|required'),
				    array('field' => 'f[username]', 'label' => 'Email  ', 'rules' => 'trim|required'),
					 
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
				
				
				 
				
							 
							 if(empty($id)){
								
								   $this->db->insert("verifikator",$f);	
								   $narasi = "<b>Bantuan Kinerja dan Afirmasi</b> <br>Yth. ".$f['nama'].",<br>Anda ditunjuk sebagai Verifikator Bantuan Kinerja dan Afirmasi, silahkan login kedalam aplikasi : <br>https://appmadrasah.kemenag.go.id/bkba <br>Username  : ".$f['username']." <br>Password  : ".$f['password']."\n  ";
								//    $sendEMail    = array("tujuan"=>$f['username'],"username"=>$f['username'],"password"=>$f['password'],"narasi"=>$narasi);		  
								//    $this->Reff->post('https://appmadrasah.kemenag.go.id/cbtpkb/web/sendAkunBKBA', $sendEMail);
		   
								   $message = "*Bantuan Kinerja dan Afirmasi* \nYth. ".$f['nama'].",\nAnda ditunjuk sebagai Verifikator Bantuan Kinerja dan Afirmasi, silahkan login kedalam aplikasi: \nhttps://appmadrasah.kemenag.go.id/bkba \nUsername  : ".$f['username']." \nPassword  : ".$f['password']."\n  ";
								
								   $sendWhatsapp = "sender=aplikasi&number=".$f['whatsapp']."&message=".$message.""; 		  
								   $this->Reff->sendWA($sendWhatsapp);
								   
								   echo "Data Berhasil disimpan";	
								 
								
							 }else{
								$this->db->where("id",$id);
								$this->db->update("verifikator",$f);
								echo "Data Berhasil disimpan";	
								 
							 }
							 
						
					 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}

	public function hapus_verifikator(){

		$this->db->where("id",$_POST['id']);
		$this->db->delete("verifikator");
		echo "sukses";
	}

	public function gridMadrasah(){
	
		$iTotalRecords = $this->m->gridMadrasah(false)->num_rows();
		
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);
		$verifikator = $this->input->get_post("verifikator");
		$madrasah    = explode(",",$this->Reff->get_kondisi(array("id"=>$verifikator),"verifikator","madrasah"));
		$records = array();
		$records["data"] = array(); 

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;
		
		$datagrid = $this->m->gridMadrasah(true)->result_array();
		$arbantuan = array("1"=>"Kinerja","2"=>"Afirmasi");
		
		 $i= ($iDisplayStart +1);
		
		 $arbantuan = array("1"=>"<span class='badge badge-light-warning'>Kinerja</span>","2"=>"<span class='badge badge-light-primary'>Afirmasi</span>");
		

		 foreach($datagrid as $val) {
			$checked="";
			if (in_array($val['id'], $madrasah)){ $checked="checked";  }
			  
			$no = $i++;
			  $records["data"][] = array(
				  $no,										
				  $val['rank_visitasi'],					 
				  $arbantuan[$val['bantuan']],				
				  $val['nsm'],
				  $val['nama'],
				  $val['provinsi'],
				  $val['kota'],
				  '<input type="checkbox" class="verifikatorpilih" '.$checked.' name="madrasah[]" id="madrasah[]" value="'.$val['id'].'">'
				  
				  
				  

				  
				  
				 

				);
			}
	  
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		
		echo json_encode($records);
  }
	

  public function setVerif(){

	  $id 		= $this->input->get_post("id");
	  $madrasah = implode(",",$this->input->get_post("madrasah"));

	  $this->db->set("madrasah",$madrasah);
	  $this->db->where("id",$id);
	  $this->db->update("verifikator");
	  echo "sukses";

	  
  }



	
	 
}
