<?php

class M_guru extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
    }

    
	public function grid_superadmin($paging){
	
	    $keyword  = $this->input->get_post("keyword");
		$this->db->select("*");
        $this->db->from('admin');
    	
		
	    if(!empty($keyword)){  $this->db->where("(UPPER(nama) LIKE '%".strtoupper($keyword)."%' OR UPPER(username) LIKE '%".strtoupper($keyword)."%')");    }
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
				
					 $this->db->order_by("nama","ASC");
					
		
			 }
      
		
		
		return $this->db->get();
		
	}

	public function grid_verifikator($paging){
	
	    $keyword  = $this->input->get_post("keyword");
		$this->db->select("*");
        $this->db->from('verifikator');
    	
		
	    if(!empty($keyword)){  $this->db->where("(UPPER(nama) LIKE '%".strtoupper($keyword)."%' OR UPPER(username) LIKE '%".strtoupper($keyword)."%')");    }
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
				
					 $this->db->order_by("nama","ASC");
					
		
			 }
      
		
		
		return $this->db->get();
		
	}

	public function gridMadrasah($paging){
		$tahun   = $this->Reff->tahun();

	    $status = $this->input->get_post("status");
	    $jenjang = $this->input->get_post("jenjang");
	    $provinsi = $this->input->get_post("provinsi");
	    $kota    = $this->input->get_post("kota");
	    $keyword  = $this->input->get_post("keyword");
	    $bantuan  = $this->input->get_post("bantuan");
	    $statusVisit  = $this->input->get_post("statusVisit");

		$this->db->select("*");
        $this->db->from('madrasahedm');
		$this->db->where("tahun",$tahun);
		$this->db->where("shortlist",1);
		$this->db->where("dnt",1);
    	
	    if(!empty($bantuan)){  $this->db->where("bantuan",$bantuan);    }
	    if(!empty($jenjang)){  $this->db->where("jenjang",$jenjang);    }
	    if(!empty($provinsi)){  $this->db->where("provinsi_id",$provinsi);    }
	    if(!empty($kota)){  $this->db->where("kota_id",$kota);    }
	    if(!empty($keyword)){  $this->db->where("(UPPER(nama) LIKE '%".strtoupper($keyword)."%' OR UPPER(nsm) LIKE '%".strtoupper($keyword)."%')");    }
		

			$this->db->where("pernyataan",1);
	


		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("provinsi_id","ASC");
					 $this->db->order_by("kota_id","ASC");
					
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
	
}
