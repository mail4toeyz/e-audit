<?php
ini_set('memory_limit', '-1');

defined('BASEPATH') OR exit('No direct script access allowed');

class Longlist extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 if(!$this->session->userdata("admin_id")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_guru','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('pusat/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Longlist Madrasah ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			    
				$no = $i++;
				$records["data"][] = array(
					$no,
					strtoupper($val['jenjang']),
					$val['nsm'],
					$val['nama'],
					$val['provinsi'],
					$val['kota'],
					$val['akreditasi'],
					$val['jml_siswa'],
					$val['jml_guru'],
					
					$val['siswa_pip'],
					number_format($val['persen_pip'],1)." %",
					sprintf('%0.2f',$val['nilai_kedisiplinan']),
					sprintf('%0.2f',$val['nilai_pengembangan_diri']),
					sprintf('%0.2f',$val['nilai_proses_pembelajaran']),
					sprintf('%0.2f',$val['nilai_sarana_prasarana']),
					sprintf('%0.2f',$val['nilai_pembiayaan']),
					number_format($val['nilai_skpm'],2),
					$this->Reff->formatuang2($val['rencana_pendapatan']),
					$this->Reff->formatuang2($val['total_rencana_kegiatan'])
					
					
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	public function form(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("tm_guru",array("id"=>$id));
			   
		   }
		$this->load->view("form",$data);
		
	}
	
	public function form_foto(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("tm_guru",array("id"=>$id));
			   
		   }
		$this->load->view("form_foto",$data);
		
	}
	
	
	
	public function form_excel(){
		
		$id = $this->input->get_post("id");
		$data = array();
		  
		$this->load->view("form_excel",$data);
		
	}
	
	public function import_excel()
		{
				$this->load->library('PHPExcel/IOFactory');
       // error_reporting(0);
	
	 
	    $sheetup   = 0;
	    $rowup     = 2;
		
		
		$fileName = time().".xlsx";
         
        $config['upload_path'] = './__statics/excel/tmp/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size'] = 100000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') ){
			$jupload=0;
              echo $this->upload->display_errors();

			  
		}else{
             
        $media = $this->upload->data('file');
		
        $inputFileName = './__statics/excel/tmp/'.$fileName;
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet($sheetup);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
        
             
			 $jupload=0;
            for ($row = $rowup; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
												FALSE);
												
		    	$nsm        =      (trim($rowData[0][1]));
			 	$nama          =      (trim($rowData[0][2]));
			 	$jenjang         =      (trim($rowData[0][3]));
			    $akreditasi       =      (trim($rowData[0][4])); 
			    $jml_guru       =      (trim($rowData[0][5])); 
			    $murid_rombel          =      (trim($rowData[0][6])); 
			    $guru_s1       =      (trim($rowData[0][7])); 
			    $guru_sertifikat       =      (trim($rowData[0][8])); 
			    $guru_laki       =      (trim($rowData[0][9])); 
			    $guru_wanita       =      (trim($rowData[0][10])); 
			    $guru_pns       =      (trim($rowData[0][11])); 
			    $guru_nonpns       =      (trim($rowData[0][12])); 
			    $guru_tetap       =      (trim($rowData[0][13])); 
			    $guru_tdk_tetap       =      (trim($rowData[0][14])); 
			    $tenaga_kependidikan       =      (trim($rowData[0][15])); 
			    $ruang_belajar       =      (trim($rowData[0][16])); 
			    $kantor_kamad       =      (trim($rowData[0][17])); 
			    $kantor_guru       =      (trim($rowData[0][18])); 
			    $kantor_administrasi       =      (trim($rowData[0][19])); 
			    $perpus       =      (trim($rowData[0][20])); 
			    $lab_ipa       =      (trim($rowData[0][21])); 
			    $lab_bahasa       =      (trim($rowData[0][22])); 
			    $lab_komp       =      (trim($rowData[0][23])); 
			    $toilet_guru       =      (trim($rowData[0][24])); 
			    $toilet_siswa       =      (trim($rowData[0][25])); 
			    $olahraga       =      (trim($rowData[0][26])); 
			    $luas_lahan       =      (trim($rowData[0][27])); 
			    $luas_bangunan       =      (trim($rowData[0][28])); 
			    $luas_lahanterbuka       =      (trim($rowData[0][29])); 
			    $jml_rombel       =      (trim($rowData[0][30])); 
			    $jml_siswa       =      (trim($rowData[0][31])); 
			    $siswa_laki       =      (trim($rowData[0][32])); 
			    $siswa_wanita       =      (trim($rowData[0][33])); 
			    $siswa_berkebutuhan       =      (trim($rowData[0][34])); 
			    $siswa_putus       =      (trim($rowData[0][35])); 
			    $siswa_ipa       =      (trim($rowData[0][36])); 
			    $siswa_ips       =      (trim($rowData[0][37])); 
			    $siswa_bahasa       =      (trim($rowData[0][38])); 
			    $siswa_agama       =      (trim($rowData[0][39])); 
			    $internet       =      (trim($rowData[0][40])); 
			    $kualitas_internet       =      (trim($rowData[0][41])); 
			    $no_urut       =      (trim($rowData[0][42])); 
			    
			 
				$error  ="";
				
				 if(!empty($nsm)){
				
					$jupload++;
                 $data = array(
                    "nsm"=> $nsm,                 
                    "nama"=> $nama,                 
                    "jenjang"=> $jenjang,                 
                    "akreditasi"=> $akreditasi,                 
                    "jml_guru"=> $jml_guru,                 
                    "murid_rombel"=> $murid_rombel,                 
                    "guru_s1"=> $guru_s1,                 
                    "guru_sertifikat"=> $skor_kedisiplinan,                 
                    "guru_laki"=> $guru_laki,                 
                    "guru_wanita"=> $guru_wanita,                 
                    "guru_pns"=> $guru_pns,                 
                    "guru_nonpns"=> $guru_nonpns,                 
                    "guru_tetap"=> $guru_tetap,                 
                    "guru_tdk_tetap"=> $guru_tdk_tetap,                 
                    "tenaga_kependidikan"=> $tenaga_kependidikan,                 
                    "ruang_belajar"=> $ruang_belajar,                 
                    "kantor_kamad"=> $kantor_kamad,                 
                    "kantor_guru"=> $kantor_guru,                 
                    "kantor_administrasi"=> $kantor_administrasi,                 
                    "perpus"=> $perpus,         
                    "lab_ipa"=> $lab_ipa,         
                    "lab_bahasa"=> $lab_bahasa,         
                    "lab_komp"=> $lab_komp,         
                    "toilet_guru"=> $toilet_guru,         
                    "toilet_siswa"=> $toilet_siswa,         
                    "olahraga"=> $olahraga,         
                    "luas_lahan"=> $luas_lahan,         
                    "luas_bangunan"=> $luas_bangunan,         
                    "luas_lahanterbuka"=> $luas_lahanterbuka,         
                    "jml_rombel"=> $jml_rombel,         
                    "jml_siswa"=> $jml_siswa,         
                    "siswa_laki"=> $siswa_laki,         
                    "siswa_wanita"=> $siswa_wanita,         
                    "siswa_berkebutuhan"=> $siswa_berkebutuhan,         
                    "siswa_putus"=> $siswa_putus,         
                    "siswa_ipa"=> $siswa_ipa,         
                    "siswa_ips"=> $siswa_ips,         
                    "siswa_bahasa"=> $siswa_bahasa,         
                    "siswa_agama"=> $siswa_agama,         
                    "internet"=> $internet,         
                    "kualitas_internet"=> $kualitas_internet,         
                    "no_urut"=> $no_urut        
                                   
                           
				  );
				  
				  
				   	  
				

                   $this->db->insert("masteremis",$data);
				
				   
				   
				
		               }
				
       
			 	
			 	// $tahun        =      (trim($rowData[0][0]));
			 	// $nsm          =      (trim($rowData[0][1]));
			 	// $nama         =      (trim($rowData[0][2]));
			    // $jenjang       =      (trim($rowData[0][3])); 
			    // $provinsi       =      (trim($rowData[0][4])); 
			    // $kota          =      (trim($rowData[0][5])); 
			    // $nilai_kedisiplinan       =      (trim($rowData[0][6])); 
			    // $skor_kedisiplinan       =      (trim($rowData[0][7])); 
			    // $nilai_pengembangan_diri       =      (trim($rowData[0][8])); 
			    // $skor_pengembangan_diri       =      (trim($rowData[0][9])); 
			    // $nilai_proses_pembelajaran       =      (trim($rowData[0][10])); 
			    // $skor_proses_pembelajaran       =      (trim($rowData[0][11])); 
			    // $nilai_sarana_prasarana       =      (trim($rowData[0][12])); 
			    // $skor_sarana_prasarana       =      (trim($rowData[0][13])); 
			    // $nilai_pembiayaan       =      (trim($rowData[0][14])); 
			    // $skor_nilai_pembiayaan       =      (trim($rowData[0][15])); 
			    // $nilai_skpm       =      (trim($rowData[0][16])); 
			    // $rencana_pendapatan       =      (trim($rowData[0][17])); 
			    // $total_rencana_kegiatan       =      (trim($rowData[0][18])); 
			    // $persen       =      (trim($rowData[0][19])); 
			 
				// $error  ="";
				
				//  if(!empty($tahun)){
				
				// 	$jupload++;
                //  $data = array(
                //     "tahun"=> $tahun,                 
                //     "nsm"=> $nsm,                 
                //     "nama"=> $nama,                 
                //     "jenjang"=> $jenjang,                 
                //     "provinsi"=> $provinsi,                 
                //     "kota"=> $kota,                 
                //     "nilai_kedisiplinan"=> $nilai_kedisiplinan,                 
                //     "skor_kedisiplinan"=> $skor_kedisiplinan,                 
                //     "nilai_pengembangan_diri"=> $nilai_pengembangan_diri,                 
                //     "skor_pengembangan_diri"=> $skor_pengembangan_diri,                 
                //     "nilai_proses_pembelajaran"=> $nilai_proses_pembelajaran,                 
                //     "skor_proses_pembelajaran"=> $skor_proses_pembelajaran,                 
                //     "nilai_sarana_prasarana"=> $nilai_sarana_prasarana,                 
                //     "skor_sarana_prasarana"=> $skor_sarana_prasarana,                 
                //     "nilai_pembiayaan"=> $nilai_pembiayaan,                 
                //     "skor_nilai_pembiayaan"=> $skor_nilai_pembiayaan,                 
                //     "nilai_skpm"=> $nilai_skpm,                 
                //     "rencana_pendapatan"=> $rencana_pendapatan,                 
                //     "total_rencana_kegiatan"=> $total_rencana_kegiatan,                 
                //     "persen"=> $persen         
                                   
                           
				//   );
				  
				  
				   	  
				

                //    $this->db->insert("madrasahedm",$data);
				
				   
				   
				
		               //}
			}
			
			
		
			echo "<br><br><center>Data yg di upload : ". $jupload ." Data <br> <a href='".site_url("schoolguru")."' > Kembali ke Aplikasi </a>";
        
        }
   }
	
	public function hapus(){
		
		$id = $this->input->get_post("id",true);
		
		$this->db->delete("tm_guru",array("id"=>$id));
		$this->db->delete("tr_post",array("tmguru_id"=>$id));
		$this->db->delete("tr_kelas",array("tmguru_id"=>$id));
		echo "sukses";
	}
	
	
	 
}
