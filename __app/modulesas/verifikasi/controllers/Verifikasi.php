<?php
ini_set('memory_limit', '-1');

defined('BASEPATH') OR exit('No direct script access allowed');

class Verifikasi extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 if(!$this->session->userdata("admin_id")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_guru','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('verifikator/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Verifikasi  Madrasah ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		  error_reporting(0);
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		  $arbantuan = array("1"=>"Kinerja","2"=>"Afirmasi");
		  
		   $i= ($iDisplayStart +1);
		   $status= $_POST['status'];
		   $arbantuan = array("1"=>"<span class='badge badge-light-warning'>Kinerja</span>","2"=>"<span class='badge badge-light-primary'>Afirmasi</span>");
		   $status =  array("0"=>"<span class='fa fa-times' style='color:red'></span>","1"=>"<span class='fa fa-check-square-o primary' style='color:blue'></span>");
	 

		   foreach($datagrid as $val) {
			    
				$no = $i++;

				$buttonnya ="Verifikasi";
				$cata = $this->db->query("select id from verifikasi_catatan where verifikator='{$_SESSION['verifikator_id']}' and madrasah_id='{$val['id']}'")->num_rows();
				  if($cata >0){
					$buttonnya = $cata. " Catatan dari Anda";

				  }
				$visitasi ='<button href="'.site_url('verifikasi/detail?id='.$val['id'].'').'" class="btn btn-primary btn-sm menuajax" > '.$buttonnya.' </button>';

				  $aman      = $this->db->get_where("visitasi_jadwal",array("nsm"=>$val['nsm']))->row();
				  


				  $edm       = $this->db->query("select * from visitasi_edm where madrasah_id='{$val['id']}'  AND nilai_pembiayaan IS NOT NULL AND nilai_proses_pembelajaran IS NOT NULL LIMIT 1")->row();
				 
				  $pd        = $this->db->query("select total from visitasi_pesertadidik where madrasah_id='{$val['id']}' ")->row();
				  $rombel    = $this->db->query("select total from visitasi_rombel where  madrasah_id='{$val['id']}' ")->row();
				  $guru      = $this->db->query("select (guru_tetap+guru_tidak_tetap) as total from visitasi_guru where madrasah_id='{$val['id']}'  ")->row();
				  $ruangbelajar   = $this->db->query("select total from visitasi_ruangbelajar where  madrasah_id='{$val['id']}' ")->row();
				  $toiletsiswa         = $this->db->query("select (siswa+siswi) as total from visitasi_toilet where madrasah_id='{$val['id']}'  ")->row();
				  $toiletguru          = $this->db->query("select (guru) as total from visitasi_toilet where madrasah_id='{$val['id']}'  ")->row();
				  
			     if($val['rank'] <=33){

					$statusnya = "<small class='badge badge-light-primary'>Masuk</small>";
				 }else{
					$statusnya = "<small class='badge badge-light-danger'>Tidak</small>";

				 }

			
				$records["data"][] = array(
					$no,										
					$visitasi,
					$arbantuan[$val['bantuan']],				
					$val['nsm'],
					$val['nama'],
					$val['provinsi'],
					$val['kota'],
					$val['rank_visitasi'],
					$val['rank']." <br>".$statusnya,
					
					number_format($val['skor_akhir_visitasi'],1),
					number_format($val['skor_edm_visitasi'],1),
					number_format($val['skor_pip'],1),
					number_format($val['skor_rombel_visitasi'],1),
					number_format($val['skor_toilet_visitasi'],1),

					number_format($val['skor_akhir'],1),
					number_format($val['skor_edm'],1),
					number_format($val['skor_pip'],1),
					number_format($val['skor_rombel'],1),
					number_format($val['skor_toilet'],1),
				
					round($val['nilai_kedisiplinan'],1),
					$edm->nilai_kedisiplinan,

					
					round($val['nilai_pengembangan_diri'],1),
					$edm->nilai_pengembangan_diri,

					
					round($val['nilai_proses_pembelajaran'],1),
					$edm->nilai_proses_pembelajaran,
					
				
					round($val['nilai_sarana_prasarana'],1),
					$edm->nilai_sarana_prasarana,

					
					round($val['nilai_pembiayaan'],1),
					$edm->nilai_pembiayaan,


					$val['jml_siswa'],
					$pd->total,

					$val['jml_guru'],
					$guru->total,

					$val['jumlah_rombel'],
					$rombel->total,

					$val['rombel'],
					$ruangbelajar->total,

					$val['toilet_guru'],
					$toiletguru->total,

					$val['toilet_guru'],
					$toiletsiswa->total,

					$aman->nik,
					$this->Reff->get_kondisi(array("nik"=>$aman->nik),"asesor","nama"),
					$this->Reff->formattanggalstring($aman->tanggal_mulai)." ".$aman->jam_mulai,
					$this->Reff->formattanggalstring($aman->tanggal_selesai)." ".$aman->jam_selesai
					
					
					

					
					
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
		public function detail(){
			$id            = $this->input->get_post("id",true);
			
			$data['data']    = $this->db->get_where("madrasahedm",array("id"=>$id))->row();
			$cekAsesor       = $this->db->query("select asesor from visitasi_edm where madrasah_id='{$id}' limit 1")->row();

			$data['asesor']  = $this->db->get_where("asesor",array("id"=>$cekAsesor->asesor))->row();

			$ajax            = $this->input->get_post("ajax",true);	
				$data['title']   = "Dokumentasi Visitasi";
				
				if(!empty($ajax)){
								
					$this->load->view('detail',$data);
				
				}else{
					
					
					$data['konten'] = "detail";
					
					$this->_template($data);
				}


		}



		public function bukti(){

			$this->load->helper('exportpdf_helper'); 

			
			$data['madrasah'] = $this->db->query("select * from madrasahedm where nsm='{$_GET['nsm']}' and id  IN(select madrasah_id from visitasi_kegiatan)")->row();
			$data['data']     = $this->db->query("select * from madrasahedm where nsm='{$_GET['nsm']}' and id  IN(select madrasah_id from visitasi_kegiatan)")->row();
			$data['petugas'] = $this->db->get_where("asesor",array("nik"=>$_GET['nik']))->row();
			$data['aman'] = $this->db->get_where("visitasi_jadwal",array("nik"=>$_GET['nik'],"nsm"=>$_GET['nsm']))->row();
			$user_info = $this->load->view('bukti', $data, true);
			$pdf_filename = 'Hasil Visitasi'.$data['data']->nsm.'.pdf';	 
			
			  $output = $user_info;
			
			generate_pdf($output, $pdf_filename);
      }


	  public function saveCatatan(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
			    $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
    
		 
			
				$config = array(
				    
				    array('field' => 'catatan_id', 'label' => 'Kategori Catatan   ', 'rules' => 'trim|required'),
				    array('field' => 'catatan', 'label' => 'Catatan Anda  ', 'rules' => 'trim|required'),
				   
				   
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $madrasah_id = $this->security->xss_clean($this->db->escape_str($this->input->get_post("madrasah_id",true)));
			    $id          = $this->security->xss_clean($this->db->escape_str($this->input->get_post("id",true)));
			    $catatan_id  = $this->security->xss_clean(($this->db->escape_str($this->input->get_post("catatan_id"))));
			    $catatan     = nl2br($this->security->xss_clean(($this->db->escape_str($this->input->get_post("catatan")))));
				
				
				 
				
							 if(empty($id)){
								
								   $this->db->set("catatan_id",$catatan_id);
								   $this->db->set("catatan",$catatan);
								   $this->db->set("tanggal",date("Y-m-d H:i:s"));
								   $this->db->set("madrasah_id",$madrasah_id);
								   $this->db->set("verifikator",$_SESSION['verifikator_id']);
								   $this->db->insert("verifikasi_catatan");
								   $data['data']    = $this->db->get_where("madrasahedm",array("id"=>$madrasah_id))->row();

								   $this->load->view("tablecatatan",$data);
								 
								
								
							 }else{
								   $this->db->set("catatan_id",$catatan_id);
								   $this->db->set("catatan",$catatan);
								   $this->db->set("tanggal",date("Y-m-d H:i:s"));
								   $this->db->set("madrasah_id",$madrasah_id);
								   $this->db->set("verifikator",$_SESSION['verifikator_id']);
								   $this->db->where("id",$id);
								   $this->db->update("verifikasi_catatan");
								   $data['data']    = $this->db->get_where("madrasahedm",array("id"=>$madrasah_id))->row();

	 							 $this->load->view("tablecatatan",$data);
								
								 
							 }
							 
						
							
						
		
		
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}
	
  public function catatan_hapus(){
	$madrasah_id = $this->security->xss_clean($this->db->escape_str($this->input->get_post("madrasah_id",true)));
	$id          = $this->security->xss_clean($this->db->escape_str($this->input->get_post("id",true)));
	  $this->db->where("id",$id);
	  $this->db->delete("verifikasi_catatan");
	  $data['data']    = $this->db->get_where("madrasahedm",array("id"=>$madrasah_id))->row();

	  $this->load->view("tablecatatan",$data);
  }

	
	
	 
}
