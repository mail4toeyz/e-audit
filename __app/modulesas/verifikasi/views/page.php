
		<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Post-->
<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
	<div id="kt_content_container" class="container-xxl">
				       <div class="card">
						<div class="card-header card-header-success">
						  <h4 class="card-title"> Verifikasi Madrasah  </h4>
						  <p class="card-category">
						 
						  </p>
						</div>
					  
						 <div class="card-body">
						 <div class="row">
													
													
						                      

													<div class="col-md-3">
													  <select class="form-control" id="jenjang">
														   <option value="">- Jenjang - </option>
														   <?php 
															 $jenjang = array("mi","mts","ma");
															   foreach($jenjang as $r){
																   ?><option value="<?php echo $r; ?>"  > <?php echo strtoupper($r); ?> </option><?php 
																   
															   }
															  ?>
														  
													   
													   </select>
												   
													</div>
													
													<div class="col-md-3">
												
													  <select class="form-control onchange " id="provinsi" target="kota" urlnya="<?php echo site_url('publik/kota'); ?>">
														   <option value="">- Provinsi - </option>
														   <?php 
															   $provinsi = $this->db->get_where("provinsi",array("active"=>1))->result();
															   foreach($provinsi as $row){
																   ?><option value="<?php echo strtoupper($row->kode); ?>"> <?php echo ($row->nama); ?> </option><?php 
																   
															   }
															  ?>
													   
													   </select>
												   
													</div>

													<div class="col-md-2">
												
													  <select class="form-control kota " id="kota" target="kota" urlnya="<?php echo site_url('publik/kota'); ?>">
														   <option value="">- Kabupaten/Kota - </option>
														  
													   
													   </select>
												   
													</div>
													
													
													<div class="col-md-3">
													  <input class="form-control" id="keyword"  placeholder="Apa yang Anda cari ?">
													  <input class="form-control" type="hidden" id="bantuan" value="<?php echo isset($_GET['bantuan']) ? $_GET['bantuan']:''; ?>">
													
												   
													</div>
													<div class="col-md-1">
													  <button class="btn btn-sm btn-primary" id="tampilkan" > <i class="fa fa-search"></i> </button> 
												   
													</div>
											   
											</div>

							<div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
									   <tr class="fw-bolder  bg-light" style="font-weight:bold">
                                            <th width="2px" rowspan="3">NO</th>                                          
                                             <th rowspan="3">VERIFIKASI </th>                                            
                                            <th width="2px" rowspan="3">BANTUAN</th>                                            
                                            <th rowspan="3">NSM </th>
                                            <th rowspan="3">NAMA </th>
                                            <th rowspan="3">PROVINSI </th>
                                            <th rowspan="3">KABKOTA </th> 
											<th rowspan="3">RANK AKHIR </th>                                                                                  
											<th rowspan="3">RANK AWAL </th>											
                                            <th  rowspan="3">SKOR VISITASI </th>
                                            <th colspan="4" rowspan="2">RINCIAN SKOR VISITASI </th>
											
											<th  rowspan="3">SKOR AWAL </th>
                                            <th colspan="4" rowspan="2">RINCIAN SKOR AWAL </th>


                                            <th colspan="10">ASPEK EVALUASI DIRI MADRASAH  </th>
                                            <th colspan="2" rowspan="2">JUMLAH PESERTA DIDIK  </th>
                                            <th colspan="2" rowspan="2">JUMLAH GURU  </th>
                                            <th colspan="2" rowspan="2">JUMLAH ROMBEL  </th>
											<th colspan="2" rowspan="2">JUMLAH RUANG BELAJAR  </th>
											<th colspan="4">JUMLAH TOILET BERFUNGSI  </th>
											<th colspan="4" rowspan="2">ASESOR </th>
											
                                            
                                        </tr>

										<tr class="fw-bolder  bg-light" style="font-weight:bold">
                                            <th colspan="2"> KEDISIPLINAN </th>                                           
                                            <th colspan="2"> PENGEMBANGAN DIRI</th>  
                                            <th colspan="2"> PROSES PEMBELAJARAN</th>  
                                            <th colspan="2"> SARANA PRASARANA</th>  
                                            <th colspan="2"> PEMBIAYANAAN</th>  

											<th colspan="2"> TOILET GURU</th> 
											<th colspan="2"> TOILET SISWA</th> 
										
											
                                            
											
                                            
                                        </tr>

										<tr class="fw-bolder  bg-light" style="font-weight:bold">
										  <th> EDM </th>
										  <th> PIP </th>
										  <th> Ruang Belajar </th>
										  <th> Toilet </th>

										  <th> EDM </th>
										  <th> PIP </th>
										  <th> Ruang Belajar </th>
										  <th> Toilet </th>


										    <th>APLIKASI</th> 
                                            <th>HASILVISITASI </th>   
											
											<th>APLIKASI</th> 
                                            <th>HASILVISITASI </th>

											<th>APLIKASI</th> 
                                            <th>HASILVISITASI </th>

											<th>APLIKASI</th> 
                                            <th>HASILVISITASI </th>

											<th>APLIKASI</th> 
                                            <th>HASILVISITASI </th>

											<th>APLIKASI</th> 
                                            <th>HASILVISITASI </th>

											<th>APLIKASI</th> 
                                            <th>HASILVISITASI </th>

											<th>APLIKASI</th> 
                                            <th>HASILVISITASI </th>

											<th>APLIKASI</th> 
                                            <th>HASILVISITASI </th>

											<th>APLIKASI</th> 
                                            <th>HASILVISITASI </th>

											<th>APLIKASI</th> 
                                            <th>HASILVISITASI </th>

											
										  <th> NIK  </th>
										  <th> NAMA  </th>
										  <th> MULAI  </th>
										  <th> SELESAI  </th>
                                           

                                            
                                        </tr>

										

                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                         </div>
                         </div>
                         </div>
                         </div>
                         </div>
                 
 
 
              
        
<div id="defaultModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body" id="loadform">
        <p>Loading...</p>
      </div>
      
    </div>

  </div>
</div>

<script type="text/javascript">

function btn_export() {
        var table1 = document.querySelector("#datatableTable");
        var opt = {
            rowIndex: 4
        }; //开头空4行
        var sheet = XLSX2.utils.table_to_sheet(table1, opt);

  
     

        openDownloadDialog(sheet2blob(sheet), ' .Hasil Visitasi.xlsx');
    }

  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"scrollY": 500,
                        "scrollX": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[ 50,100,200,300,500,1000, 800000000], [ 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					
                     buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
					
					"ajax":{
						url :"<?php echo site_url("verifikasi/grid"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						data.status = $("#status").val();
						data.jenjang = $("#jenjang").val();
						data.provinsi = $("#provinsi").val();
						data.kota = $("#kota").val();
						data.keyword = $("#keyword").val();
						data.bantuan = $("#bantuan").val();
						data.statusVisit = $("#statusVisit").val();
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				

				$(document).on("change","#status,#jenjang,#provinsi,#kota,#statusVisit",function(){


					dataTable.ajax.reload(null,false);	
				});

				$(document).on("input","#keyword",function(){


				   dataTable.ajax.reload(null,false);	
				});
				
				
	


</script>
				