<div class="alert alert-default"> Jumlah Guru <b><?php echo $data->nama; ?> (<?php echo $data->nsm; ?>)</b> </div>
<div class="row">
<div class="col-xl-7">
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-striped">
                <thead>
                <tr class="fw-bolder  bg-light" style="font-weight:bold">
                    <th rowspan="2">NO</th>
                    <th rowspan="2">STATUS</th>
                    <th rowspan="2">JUMLAH </th>
                    
                </tr>
                
                </thead>
                <tbody>
                    <?php 
                     $tahun = $this->Reff->tahun();
                     $total=0;
                     foreach($this->db->get("master_guru")->result() as $row){
                      $hasil      = $this->db->query("select * from visitasi_guru where madrasah_id='{$data->id}'")->row();
                      $kolom      = $row->kolom;
                      $nilainya   = isset($hasil->$kolom) ? $hasil->$kolom :0;
                      $total      = $total + $nilainya;

                      ?>
                       <tr>
                        <td><?php echo $row->id; ?></td>
                        <td><?php echo $row->nama; ?></td>
                        <td><?php echo $nilainya; ?></td>     
                     </tr>
                     

                      <?php 

                     }
                     ?>

                    <tr class="fw-bolder  bg-light" style="font-weight:bold">
                        
                        <td colspan="2" align="right">Total Hasil Visitasi  </td>                       
                        <td><?php echo $total; ?></td>
                      </tr>
                     

                </tbody>
            </table>

            

        </div>
   </div>

   <div class="col-xl-5">
   Total Jumlah Guru  yang terdata di EMIS  adalah : <br> <b> <?php echo $data->jml_guru; ?> Orang </b>
    
   <?php
    $hasil      = $this->db->query("select (guru_tetap+guru_tidak_tetap) as total from visitasi_guru where madrasah_id='{$data->id}'")->row();
    $persentase = isset($hasil->total) ? number_format(($hasil->total/$data->jml_guru) * 100,0) :0;
   ?>
      <div class="d-flex flex-column w-100 mr-2">
                    <div class="d-flex align-items-center justify-content-between mb-2">
                      <span class=" mr-2 font-size-sm font-weight-bold" id="persenguru"><?php echo $persentase; ?>%</span>
                      <span class="text-muted font-size-sm font-weight-bold">Akurasi</span>
                    </div>
                      <div class="progress progress-lg w-100">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" id="progressguru" style="width: <?php echo $persentase; ?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
      </div>
     
  </div>
  </div>


<script type="text/javascript">

  $(document).off("input",".inputguru").on("input",".inputguru",function(){
    var kolom       = $(this).data("kolom");
    var madrasah_id = $(this).data("madrasah_id");
    var nsm         = $(this).data("nsm");
    var eksis       = "<?php echo $data->jml_guru; ?>";
    var nilai       = $(this).val();
 
    $.post("<?php echo site_url('asesor/saveVisitasiGuru'); ?>",{kolom:kolom,madrasah_id:madrasah_id,nsm:nsm,eksis:eksis,nilai:nilai},function(data){

        $("#persenguru").html(data);
        $("#progressguru").attr("style","width:"+data);
     

    })



  });


  


</script>