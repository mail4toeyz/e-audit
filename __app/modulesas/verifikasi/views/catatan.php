                                 <div class="alert alert-primary">
                                   Berikan Catatan untuk Madrasah 
                                   <ol start="1">
                                       <li> Pilih Kategori Catatan</li>
                                       <li> Isi catatan sesuai dengan Kategori yang dipilih </li>
                                      
                                       
                                    </ol>
                                    </div>

                                    <form class="form-horizontal" action="javascript:void(0)" id="saveCatatan" url="<?php echo site_url('verifikasi/saveCatatan'); ?>">
                                    <input type="hidden" name="id">
                                    <input type="hidden" name="madrasah_id" value="<?php echo $data->id; ?>">
                                    <div class="form-group">
                                        <label class="control-label " for="email">Pilih Kategori </label>
                                        <div class="col-sm-12">
                                          <select class="form-control" name="catatan_id">
                                            <option value=""> - Pilih Kategori - </option>
                                             <?php 
                                               $catatan = $this->db->get("catatan")->result();
                                                  foreach($catatan as $r){

                                                    ?> <option value="<?php echo $r->id; ?>"><?php echo $r->nama; ?></option><?php 
                                                  }

                                             ?>

                                         </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="pwd">Isi catatan Anda dibawah ini </label>
                                        <div class="col-sm-12">
                                        <textarea class="form-control" name="catatan" placeholder="Ketik disini untuk memberikan catatan"></textarea>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                       <center> <button type="submit" class="btn btn-sm btn-primary"> <i class="fa fa-save"></i> Simpan Catatan</button></center>
                                        </div>
                                    </div>
                                    </form> 
                                    <hr>


                                    <div id="loadtablecatatan"><?php $this->load->view("tablecatatan"); ?></div>


<script type="text/javascript">

function gagal(param){
		let timerInterval;
		Swal.fire({
		  type: 'warning',
		  title: 'Mohon diperhatikan ! ',
		  showConfirmButton: true,
		   html: param

		});
   }
   
   function sukses(param){
		let timerInterval;
		Swal.fire({
		  type: 'success',
		  title: 'Berhasil ! ',
		  showConfirmButton: true,
		   html: param

		});
   }


$(document).off('submit', 'form#saveCatatan').on('submit', 'form#saveCatatan', function (event, messages) {
 event.preventDefault();
   var form   = $(this);
   var urlnya = $(this).attr("url");
   loading();
       $("#loadingm").show();
           $.ajax({
                type: "POST",
                url: urlnya,
                data: form.serialize(),
                success: function (response, status, xhr) {
                    var ct = xhr.getResponseHeader("content-type") || "";
                    if (ct == "application/json") {
                    
                        gagal(response.message);
            
                     
                        
                        
                    } else {
                      
                        $("#loadtablecatatan").html(response);
                       sukses("Catatan Berhasil diberikan");
                       document.getElementById("saveCatatan").reset();
                        
                      
                    }

                    jQuery.unblockUI({ });
                    
                 
                    
                }
            });
          
    return false;
});


$(document).off("click",".hapusCatatan").on("click",".hapusCatatan",function(){

var madrasah_id =  $(this).data("madrasah_id")
var id   =  $(this).data("id")

  alertify.confirm("Apakah yakin menghapus data ini ?",function(){

    $.post("<?php echo site_url('verifikasi/catatan_hapus'); ?>",{id:id,madrasah_id:madrasah_id},function(data){

            $("#loadtablecatatan").html(data);

     });


  })
  


});

</script>