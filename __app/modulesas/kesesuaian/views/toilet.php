

<div class="row">
<div class="col-xl-7">
       
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-striped">
                <thead>
                <tr class="fw-bolder  bg-light" style="font-weight:bold">
                  
                    <th rowspan="2">JENIS</th>
                    <th rowspan="2">JUMLAH </th>
                    
                </tr>
                
                </thead>
                <tbody>
                    <?php 
                     $toilet = array("guru","siswa","siswi");
                    $total =0;
                     foreach($toilet as $row){
                      $hasil      = $this->db->query("select * from visitasi_toilet where madrasah_id='{$data->id}'")->row();
                      $kolom      = $row;
                      $nilainya   = isset($hasil->$kolom) ? $hasil->$kolom :0;
                      $total      = $total + $nilainya;
                      ?>
                       <tr>
                        
                        <td> Toilet <?php echo ucwords($row); ?></td>
                        <td><?php echo $nilainya; ?></td>
                            
                    
                     </tr>
                     

                      <?php 

                     }
                     ?>
                    <tr class="fw-bolder  bg-light" style="font-weight:bold">
                        
                        <td  align="right">Total Hasil Visitasi  </td>                       
                        <td><?php echo $total; ?></td>
                      </tr>
                     

                </tbody>
            </table>

            

        </div>
   </div>

   <div class="col-xl-5">
   Total Jumlah Toilet  yang terdata di EMIS  adalah : <br> <b> <?php echo $data->toilet_total; ?> Toilet  (Toilet Guru : <?php echo $data->toilet_guru; ?>, Toilet Siswa : <?php echo $data->toilet_siswa; ?>) </b>
    
   <?php
    $hasil      = $this->db->query("select (guru+siswa+siswi) as total from visitasi_toilet where madrasah_id='{$data->id}'")->row();
    if(isset($hasil->total)){

        if($hasil->total !=0 and $data->toilet_total !=0 ){
            $persentase =  number_format(($hasil->total/$data->toilet_total) * 100,0);
        }else if($hasil->total ==0 and $data->toilet_total !=0){
            $persentase =  number_format(($hasil->total/$data->toilet_total) * 100,0);

        }else{
          $persentase = 100;
        }
    }else{
         $persentase = 0;
    }
    
    
     
   ?>
      <div class="d-flex flex-column w-100 mr-2">
                    <div class="d-flex align-items-center justify-content-between mb-2">
                      <span class=" mr-2 font-size-sm font-weight-bold" id="persentoilet"><?php echo $persentase; ?>%</span>
                      <span class="text-muted font-size-sm font-weight-bold">Perbandingan</span>
                    </div>
                      <div class="progress progress-lg w-100">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" id="progresstoilet" style="width: <?php echo $persentase; ?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
      </div>
     
  </div>
  </div>
