<div class="row">

		<div class="col-xl-7">
       
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-striped">
                <thead>
                <tr class="fw-bolder  bg-light" style="font-weight:bold">
                    <th>NO</th>
                    <th>KELAS</th>
                    <th>JUMLAH PESERTA DIDIK</th>
                    
                </tr>
                
                </thead>
                <tbody>
                    <?php 
                     $tahun = $this->Reff->tahun();
                     $no=1;
                     $total=0;
                     foreach($this->db->get_where("tm_kelas",array("jenjang"=>$data->jenjang))->result() as $row){
                        $hasil      = $this->db->query("select * from visitasi_pesertadidik where madrasah_id='{$data->id}'")->row();
                        $kolom      = "kelas_".$row->id;
                        $nilainya   = isset($hasil->$kolom) ? $hasil->$kolom :0;
                        $total      = $total + $nilainya;
                       
                       

                      ?>
                       <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $row->nama; ?> (<?php echo $row->satuan; ?>)</td>                       
                        <td><?php echo $nilainya; ?></td>
                      </tr>
                     

                      <?php 

                     }
                     ?>

                <tr class="fw-bolder  bg-light" style="font-weight:bold">
                        
                        <td colspan="2" align="right">Total Hasil Visitasi  </td>                       
                        <td><?php echo $total; ?></td>
                      </tr>

                    

                </tbody>
            </table>

        </div>
   </div>

   <div class="col-xl-5">
   Total Jumlah Siswa yang terdata di EMIS  adalah : <br> <b> <?php echo $data->jml_siswa; ?> Siswa </b>
    
   <?php
    $hasil      = $this->db->query("select total from visitasi_pesertadidik where madrasah_id='{$data->id}'")->row();
    $persentase = isset($total) ? number_format(($total/$data->jml_siswa) * 100,0) :0;
   ?>
      <div class="d-flex flex-column w-100 mr-2">
                    <div class="d-flex align-items-center justify-content-between mb-2">
                      <span class=" mr-2 font-size-sm font-weight-bold" id="persenPD"><?php echo $persentase; ?>%</span>
                      <span class="text-muted font-size-sm font-weight-bold">Perbandingan</span>
                    </div>
                      <div class="progress progress-lg w-100">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" id="progressPD" style="width: <?php echo $persentase; ?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
      </div>
     
  </div>
  </div>

