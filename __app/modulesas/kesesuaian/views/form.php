<style>
.btn-file {
  position: relative;
  overflow: hidden;
}
.btn-file input[type=file] {
  position: absolute;
  top: 0;
  right: 0;
  min-width: 100%;
  min-height: 100%;
  font-size: 100px;
  text-align: right;
  filter: alpha(opacity=0);
  opacity: 0;
  outline: none;
  background: white;
  cursor: inherit;
  display: block;
}
</style>
						
						 <form action="javascript:void(0)" method="post" id="simpangambarmodal" name="simpangambarmodal" url="<?php echo site_url("schoolguru/save"); ?>">
						<input type="hidden" class="form-control"  name='id' value="<?php echo (isset($data)) ? $data->id :""; ?>" data-toggle="tooltip" title="Isi dengan Nama  Organisasi">
                                
							
								

								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                            <input type="text" class="form-control" maxlength="16" onkeypress="javascript:return isNumber(event)" id="nuptk" name="f[nuptk]" value="<?php echo (isset($data)) ? $data->nuptk :""; ?>" placeholder="NUPTK atau NIK" >
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                            <input type="text" class="form-control" id="nama" name="f[nama]" value="<?php echo (isset($data)) ? $data->nama :""; ?>" placeholder="Nama Guru" >
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
									<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                            <input type="password" class="form-control" id="password" name="password" value="<?php echo (isset($data)) ? ($data->alias) :""; ?>" placeholder="Buat Password" >
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                           <select class="form-control  "  name="f[gender]"  style="color:black">
																 <option value=""> Pilih Jenis Kelamin </option>
																 <?php 
															  $gender = $this->Reff->gender();
																 foreach($gender as $i=>$r){
																	 
																	?><option value="<?php echo $i; ?>" <?php if(isset($data)){ echo ($data->gender==$i) ? "selected":""; } ?>> <?php echo ($r); ?></option><?php  
																	 
																 }
															?>
											</select>
											
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                            <input type="text" class="form-control"  name="f[tempat]" value="<?php echo (isset($data)) ? $data->tempat :""; ?>" placeholder="Tempat Lahir" >
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
										<input type="text" class="form-control" id="tgl_lahir"  name="tgl_lahir" value="<?php echo (isset($data)) ? $this->Reff->formattanggaldb($data->tgl_lahir) :""; ?>" placeholder="Tgl Lahir" >
                                             <script>
																$(document).ready(function () {
																	startdate = new Date() - 18;
																	$('#tgl_lahir').datepicker({
																		beforeShow: function(input, inst) {
																				$(document).off('focusin.bs.modal');
																			},
																			onClose:function(){
																				$(document).on('focusin.bs.modal');
																			},
																			 changeMonth: true,
																								changeYear: true,
																								autoclose: true,
																								dateFormat: 'dd-mm-yy',
																								yearRange: "1945:2020",
																							
																	});
																});
																
																
																	$.fn.modal.Constructor.prototype.enforceFocus = function () {
																	$(document)
																	  .off('focusin.bs.modal') // guard against infinite focus loop
																	  .on('focusin.bs.modal', $.proxy(function (e) {
																		if (this.$element[0] !== e.target && !this.$element.has(e.target).length) {
																		  this.$element.focus()
																		}
																	  }, this))
																	}
															</script>
										</div>
										
										
                                       </div>
                                    </div>
                                    
                                 </div>
								 
								 <div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                           <select class="form-control  "  name="f[pendidikan]" style="color:black">
																 <option value=""> Pilih Pendidikan Terakhir</option>
																 <?php 
															  $gender = $this->Reff->pendidikan();
																 foreach($gender as $i=>$r){
																	 
																	?><option value="<?php echo $r; ?>" <?php if(isset($data)){ echo ($data->pendidikan==$r) ? "selected":""; } ?>> <?php echo ($r); ?></option><?php  
																	 
																 }
															?>
											</select>
											
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								
								
								
								
								
								  <br>
								 
								 <div class=" " id="aksimodal">
						       <center>
								   <button type="submit" class="btn btn-success btn-sm ">
										<i class="fa fa-save"></i>
										<span>Simpan Data  </span>
                                    </button>
								    <button type="reset" class="btn btn-success  btn-sm">
									
										<span>Reset   </span>
                                    </button>
									<button type="button" class="btn btn-success btn-sm " data-dismiss="modal">
										
										<span>Tutup   </span>
                                    </button>
									
								   </center>
								
								 </div>
								 
								  <div class="row clearfix" id="loadingmodal" style="display:none">
								  <center><div class="preloader pl-size-md"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div> <br> Data sedang disimpan, Mohon Tunggu ...</center>
								 
								 </div>
							  
					</form>	    
	<script>
	$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
	
	
	
    });
	
	function readURL(input) {
	  if (input.files && input.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function(e) {
		  $('#blah').attr('src', e.target.result);
		}
		
		reader.readAsDataURL(input.files[0]);
	  }
	}

	$("#imgInp").change(function() {
	  readURL(this);
	});

	</script>