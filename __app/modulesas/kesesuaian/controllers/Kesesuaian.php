<?php
ini_set('memory_limit', '-1');

defined('BASEPATH') OR exit('No direct script access allowed');

class Kesesuaian extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 if(!$this->session->userdata("admin_id")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_guru','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('pusat/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Hasil Visitasi Madrasah ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		  error_reporting(0);
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		  $arbantuan = array("1"=>"Kinerja","2"=>"Afirmasi");
		  
		   $i= ($iDisplayStart +1);
		   $status= $_POST['status'];
		   $arbantuan = array("1"=>"<span class='badge badge-light-warning'>Kinerja</span>","2"=>"<span class='badge badge-light-primary'>Afirmasi</span>");
		   $status =  array("0"=>"<span class='fa fa-times' style='color:red'></span>","1"=>"<span class='fa fa-check-square-o primary' style='color:blue'></span>");
	 

		   foreach($datagrid as $val) {
			    
				$no = $i++;
				$visitasi ="";
				  $cekVisitasi = $this->db->query("select count(id) as jml from visitasi_catatan where madrasah_id='{$val['id']}'")->row();
				  if($cekVisitasi->jml >0){
						// if($cekVisitasi > 10){
							$visitasi ='<button href="'.site_url('hasilvisitasi/detail?id='.$val['id'].'').'" class="btn btn-primary btn-sm menuajax" > Detail </button>';
						//}
				  }else{

					$visitasi ="<span class='badge badge-light-danger'> Belum  </button>";
				  }

				  $cekAsesor = $this->db->query("select asesor from visitasi_edm where madrasah_id='{$val['id']}' limit 1")->row();

				  $asesor    = $this->db->get_where("asesor",array("id"=>$cekAsesor->asesor))->row();
				  $aman      = $this->db->get_where("visitasi_jadwal",array("nsm"=>$val['nsm']))->row();
				  


				  $edm       = $this->db->query("select id from visitasi_edm where madrasah_id='{$val['id']}'  AND nilai_pembiayaan IS NOT NULL AND nilai_proses_pembelajaran IS NOT NULL LIMIT 1")->num_rows();
				  $pd        = $this->db->query("select id from visitasi_pesertadidik where madrasah_id='{$val['id']}'  AND total !=0 LIMIT 1")->num_rows();
				  $rombel    = $this->db->query("select id from visitasi_rombel where  madrasah_id='{$val['id']}'  AND total !=0 LIMIT 1")->num_rows();
				  $guru      = $this->db->query("select id from visitasi_guru where madrasah_id='{$val['id']}'   LIMIT 1")->num_rows();
				  $ruangbelajar   = $this->db->query("select id from visitasi_ruangbelajar where  madrasah_id='{$val['id']}'  AND total !=0 LIMIT 1")->num_rows();
				  $toilet         = $this->db->query("select id from visitasi_toilet where madrasah_id='{$val['id']}'   LIMIT 1")->num_rows();
				  $tamuk          = $this->db->query("select id from visitasi_tatapmuka where madrasah_id='{$val['id']}'   LIMIT 1")->num_rows();
				  $rencana        = $this->db->query("select id from visitasi_kegiatan where madrasah_id='{$val['id']}'   LIMIT 1")->num_rows();
				  $dok            = $this->db->query("select count(id) as jml from tr_persyaratan where madrasah_id='{$val['id']}' ")->row();
				//  $manfaat        = $this->db->query("select id from visitasi_kegiatan where madrasah_id='{$val['id']}'   LIMIT 1")->num_rows();


			
				$records["data"][] = array(
					$no,										
					//$val['rank'],	
					$visitasi,
					$arbantuan[$val['bantuan']],				
					$val['nsm'],
					$val['nama'],
					$val['provinsi'],
					$val['kota'],
					$val['edm_visitasi'],
					$val['siswa_visitasi'],
					$val['rombel_visitasi2'],
					$val['guru_visitasi'],
					$val['ruangbelajar_visitasi'],
					$val['toilet_visitasi'],
					$val['skor_visitasi']
					
					
					
					
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
		public function detail(){
			$id            = $this->input->get_post("id",true);
			
			$data['data']    = $this->db->get_where("madrasahedm",array("id"=>$id))->row();
			$cekAsesor       = $this->db->query("select asesor from visitasi_edm where madrasah_id='{$id}' limit 1")->row();

			$data['asesor']  = $this->db->get_where("asesor",array("id"=>$cekAsesor->asesor))->row();

			$ajax            = $this->input->get_post("ajax",true);	
				$data['title']   = "Dokumentasi Visitasi";
				
				if(!empty($ajax)){
								
					$this->load->view('detail',$data);
				
				}else{
					
					
					$data['konten'] = "detail";
					
					$this->_template($data);
				}


		}



		public function bukti(){

			$this->load->helper('exportpdf_helper'); 

			
			$data['madrasah'] = $this->db->query("select * from madrasahedm where nsm='{$_GET['nsm']}' and id  IN(select madrasah_id from visitasi_catatan)")->row();
			$data['data']     = $this->db->query("select * from madrasahedm where nsm='{$_GET['nsm']}' and id  IN(select madrasah_id from visitasi_catatan)")->row();
			$data['petugas'] = $this->db->get_where("asesor",array("nik"=>$_GET['nik']))->row();
			$data['aman'] = $this->db->get_where("visitasi_jadwal",array("nik"=>$_GET['nik'],"nsm"=>$_GET['nsm']))->row();
			$user_info = $this->load->view('bukti', $data, true);
			$pdf_filename = 'Hasil Visitasi'.$data['data']->nsm.'.pdf';	 
			
			 $output = $user_info;
			
			generate_pdf($output, $pdf_filename);
      }
	
	

	  public function generateSkorVisitasi(){
		$tahun    = $this->Reff->tahun();
		$this->db->select("*");
        $this->db->from('madrasahedm');
		$this->db->where("tahun",$tahun);
		$this->db->where("shortlist",1);
		$this->db->where("nsm IN(SELECT nsm FROM conf_bkba where bersedia_menerima ='Ya')");
		$this->db->where("id IN(select madrasah_id from tr_persyaratan where persyaratan_id IN(5))");
		$this->db->where("nsm NOT IN(SELECT nsm FROM tr_penilaian)");
		$data =  $this->db->get()->result();

		  foreach($data as $r){

						$jmlEDM = 0;
						foreach($this->db->get("edm_aspek")->result() as $row){

							$eksis      = number_format($this->Reff->get_kondisi(array("id"=>$r->id,"tahun"=>$tahun),"madrasahedm",$row->kolom),2);
							$hasil      = $this->db->query("select * from visitasi_edm where madrasah_id='{$r->id}'")->row();
							$kolom      = $row->kolom;
							$hasilkolom = ($hasil->$kolom > 10) ? $hasil->$kolom/10 : $hasil->$kolom;
							$nilainya   = isset($hasilkolom) ? $hasilkolom :0;
							$eksis      = round($eksis,1);
							
							$persentase = isset($nilainya) ? number_format(($nilainya/$eksis) * 100,0) :0;

							$jmlEDM 	=  $jmlEDM + $persentase;
							$skor_edm   = $jmlEDM/5;

						}

						// Jumlah Peserta Didik
						$total=0;
						$jmlPeserta = 0;
						foreach($this->db->get_where("tm_kelas",array("jenjang"=>$r->jenjang))->result() as $row){
						   $hasil      = $this->db->query("select * from visitasi_pesertadidik where madrasah_id='{$r->id}'")->row();
						   $kolom      = "kelas_".$row->id;
						   $nilainya   = isset($hasil->$kolom) ? $hasil->$kolom :0;
						   $total      = $total + $nilainya;
						   $jmlPeserta = isset($total) ? number_format(($total/$r->jml_siswa) * 100,0) :0;

						}

						// Jumlah Rombel 
						$tahun = $this->Reff->tahun();
						$no=1;
						$total=0;
						$jmlRombel = 0;
						foreach($this->db->get_where("tm_kelas",array("jenjang"=>$r->jenjang))->result() as $row){
						 $hasil      = $this->db->query("select * from visitasi_rombel where madrasah_id='{$r->id}'")->row();
						 $kolom      = "kelas_".$row->id;
						 $nilainya   = isset($hasil->$kolom) ? $hasil->$kolom :0;
						 $total      = $total + $nilainya;

						 $jmlRombel = isset($total) ? number_format(($total/$r->jumlah_rombel) * 100,0) :0;

						}

						// Jumlah Guru 
						$hasil      = $this->db->query("select (guru_tidak_tetap) as total from visitasi_guru where madrasah_id='{$r->id}'")->row();
    					$jmlGuru  = isset($hasil->total) ? number_format(($hasil->total/$r->jml_guru) * 100,0) :0;
						

						// JmlRuangBelajar

						$no=1;
						$total=0;
						$jmlRuangBelajar= 0;
						foreach($this->db->get_where("tm_kelas",array("jenjang"=>$r->jenjang))->result() as $row){
						   $hasil      = $this->db->query("select * from visitasi_ruangbelajar where madrasah_id='{$r->id}'")->row();
						   $kolom      = "kelas_".$row->id;
						   $nilainya   = isset($hasil->$kolom) ? $hasil->$kolom :0;
						   $total      = $total + $nilainya;
						   $jmlRuangBelajar = isset($total) ? number_format(($total/$r->rombel) * 100,0) :0;

						}

						//Toilet 

						$toilet = array("guru","siswa","siswi");
						$total =0;
						$jmlToilet= 0;
						 foreach($toilet as $row){
						  $hasil      = $this->db->query("select * from visitasi_toilet where madrasah_id='{$r->id}'")->row();
						  $kolom      = $row;
						  $nilainya   = isset($hasil->$kolom) ? $hasil->$kolom :0;
						  $total      = $total + $nilainya;

						  $jmlToilet =  number_format(($total/$r->toilet_total) * 100,0);

						 }

						 $skor_visitasi = $skor_edm + $jmlPeserta + $jmlRombel + $jmlGuru + $jmlRuangBelajar + $jmlToilet;
						 $skor_visitasiPersen  = number_format($skor_visitasi / 6,1);
						 $selisih 	= number_format($skor_visitasiPersen - 100,1);
						
						
						
						
						
						
						$cek = $this->db->query("SELECT count(id) as jml FROM tr_penilaian where nsm='{$r->nsm}'")->row();
							  if($cek->jml==0){
								$this->db->set("madrasah_id",$r->id);
								$this->db->set("nsm",$r->nsm);
								$this->db->set("nama",$r->nama);
								$this->db->set("edm_visitasi",$skor_edm);
								$this->db->set("siswa_visitasi",$jmlPeserta);
								$this->db->set("rombel_visitasi",$jmlRombel);
								$this->db->set("guru_visitasi",$jmlGuru);
								$this->db->set("ruangbelajar_visitasi",$jmlRuangBelajar);
								$this->db->set("toilet_visitasi",$jmlToilet);
								$this->db->set("skor_visitasi",$skor_visitasiPersen);
								$this->db->set("selisih",$selisih);
								$this->db->insert("tr_penilaian");

							  }else{
								$this->db->where("madrasah_id",$r->id);
								$this->db->where("nsm",$r->nsm);
								$this->db->where("nama",$r->nama);
								$this->db->set("edm_visitasi",$skor_edm);
								$this->db->set("siswa_visitasi",$jmlPeserta);
								$this->db->set("rombel_visitasi",$jmlRombel);
								$this->db->set("guru_visitasi",$jmlGuru);
								$this->db->set("ruangbelajar_visitasi",$jmlRuangBelajar);
								$this->db->set("toilet_visitasi",$jmlToilet);
								$this->db->set("skor_visitasi",$skor_visitasiPersen);
								$this->db->set("selisih",$selisih);
								$this->db->update("tr_penilaian");

							  }
		  }
		
	  }
	 
}
