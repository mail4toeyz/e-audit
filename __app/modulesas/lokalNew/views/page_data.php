           <div class="row">
                <div class="col-md-12">                      
					  <div class="card form" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-institution"></i> Data Lembaga </h3>
                        </div>
                         
                          <?php $attributes = array('class' => 'form-horizontal', 'role'=>'form','name' => 'simpannorespon', 'id' => 'simpannorespon', 'method' => "post", 'url' =>site_url("lokal/perbaharui_data")); ?>
			                     <?php echo form_open("javascript:void(0)", $attributes); ?>
                            <div class="col-md-12">
                                
								
							 <br>
                        <br>
                            <div class="row">
							
									<div class="col-md-5">
										<div class="form-group">
											<label for="name">Username </label> <br>
											<?php echo $m->username; ?>
										</div>
										<div class="form-group">
											<label for="pro-qu">Nama Madrasah/Sekolah </label> <br>
											
											<input type="text" name="f[nama]" class="form-control" value="<?php echo $m->nama; ?>">
										</div>
										<div class="form-group">
											<label for="name">Provinsi  </label>
											<?php echo $this->Di->get_kondisi(array("id"=>$m->provinsi),"provinsi","nama"); ?>
										</div>
									</div>
									
									 <div class="col-md-5">
										<div class="form-group">
											<label for="name">Telepon  </label>
											<input type="text" class="form-control" name="f[telepon]" value="<?php echo $m->telepon; ?>">
										</div>
										<div class="form-group">
											<label for="pro-qu">Email </label>
											<input type="email" class="form-control" name="f[email]" value="<?php echo $m->email; ?>">
										</div>
										<div class="form-group">
											<label for="name">Jenjang Pendidikan  </label>
											<select class="form-control" name="f[jenjang]" id="jenjang">
														  <?php 
													    foreach($this->Di->jenjang() as $i=>$r){
															
															?><option value="<?php echo $i; ?>" <?php echo ($i==$m->jenjang) ? "selected":""; ?>> <?php echo $r; ?></option><?php 
															
														}
														?>
													   
										    </select>
										</div>
									</div>
							</div>
							
							
							 <div class="row">
							
									<div class="col-md-5">
										
										
									</div>
									<div class="col-md-5">
										<div class="form-group">
											<label for="name">Jenis Lembaga  </label>
											<select class="form-control" name="f[jenis]" id="jenis">
														<option value="1" <?php echo (1==$m->jenis) ? "selected":""; ?>> Madrasah</option>
														<option value="2" <?php echo (2==$m->jenis) ? "selected":""; ?>> Sekolah</option>
													
															
														
													   
										    </select>
										</div>
										
									</div>
									
									 
							</div>
							
							 <div class="row">
							
									<div class="col-md-5">
										
										
									</div>
									
									 <div class="col-md-5">
										<div class="form-group">
											<label for="name">Kab/Kota   </label>
											<?php echo $this->Di->get_kondisi(array("id"=>$m->kota),"kota","nama"); ?>
											
											<select class="form-control  " name="f[kota]" >
												          
														  
														   <?php
														 $kota = $this->db->query("select id,nama from kota where provinsi_id='".$m->provinsi."'")->result();
													    foreach($kota as $i=>$r){
															
															?><option value="<?php echo $r->id; ?>" <?php echo ($r->id==$m->kota) ? "selected":""; ?>> <?php echo $r->nama; ?></option><?php 
															
														}
														?>

												 </select>
												 
												 
										</div>
									</div>
										
							</div>
							
							<div class="row">
							
									<div class="col-md-12">
										<div class="form-group">
											<label for="name">Alamat   </label>
											 <textarea name="f[alamat]" type="text" class="form-control" id="alamat"> <?php echo $m->alamat; ?></textarea>
			                                   
										</div>
										
									</div>
									
									 
										
							</div>
							
                             

							<center>
                            <button type="submit" class="btn btn-general btn-white">Perbaharui </button>  
                            <button type="reset" class="btn btn-general btn-blue">Reset </button> 
							<center>
							<br>
                        <?php echo form_close(); ?>
                    </div>
                    </div>
                    </div>