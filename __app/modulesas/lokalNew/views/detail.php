           <div class="row">
                <div class="col-md-12">                      
					  <div class="card form" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-user"></i> <?php echo $title; ?>  </h3>
                        </div>
                        <br>
                        
                            <div class="row">
                               
							  
							
									
									
									<div class="col-md-8">
									<ul class="nav nav-tabs nav-pills">
									
									  <li><a data-toggle="tab" href="#peserta" class="nav-item nav-link  "> <span class="fa fa-user"> </span> Data Peserta</a></li>
									  <li><a data-toggle="tab" href="#madrasah" class="nav-item nav-link "><span class="fa fa-institution"> </span>  Data Lembaga</a></li>
									  <li><a data-toggle="tab" href="#kompetisi" class="nav-item nav-link "><span class="fa fa fa-cab"> </span> Kompetisi </a></li>
									  <li><a data-toggle="tab" href="#dokumen" class="nav-item nav-link "><span class="fa fa fa-file"> </span> Berkas Persyaratan </a></li>
									
									 
									</ul>

										<div class="tab-content">
										<div id="dokumen" class="tab-pane  ">
										<div class="col-md-12">
										<?php 
										 $persyaratan = $this->db->query("select * from tr_persyaratan where peserta_id='{$dataform->id}'")->result();
										 if(count($persyaratan)>0){
										   foreach($persyaratan as $row){
											?><div class="col-md-6"><iframe src="https://drive.google.com/file/d/<?php echo $row->file; ?>/preview" width="640" height="480" allow="autoplay"></iframe></div><?php 

										   }
										}else{

											?>
											<div class="alert alert-warning"> Berkas Persyaratan belum lengkap </div>
											<?php 
										}
										?>
										</div>

										

										</div>

										
										
										  <div id="peserta" class="tab-pane  in active in">
										     
											 <table class="table-hover table-bordered table">
											        <tr>
													   <td> NIK Peserta </td>
													   <td> <?php echo isset($dataform) ?  $dataform->nik_siswa :""; ?> </td>
													</tr>
													<tr>
													   <td> NISN </td>
													   <td> <?php echo isset($dataform) ?  $dataform->nisn :""; ?> </td>
													</tr>
													
													<tr>
													   <td> Nama Peserta </td>
													   <td> <?php echo isset($dataform) ?  $dataform->nama :""; ?> </td>
													</tr>
													<tr>
													   <td> Tempat,Tgl Lahir </td>
													   <td> <?php echo $dataform->tempat; ?>,<?php echo $this->Di->formattanggalstring($dataform->tgl_lahir); ?> </td>
													</tr>
													<tr>
													   <td> Gender </td>
													   <td> <?php echo isset($dataform) ?  $dataform->gender :""; ?> </td>
													</tr>
													
													<tr>
													   <td> Kelas  </td>
													   <td> <?php echo isset($dataform) ?  $dataform->kelas :""; ?> </td>
													</tr>
													
													<tr>
													   <td> Kompetisi  </td>
													   <td> <?php echo isset($dataform) ?  $this->Di->get_kondisi(array("id"=>$dataform->trkompetisi_id),"tr_kompetisi","nama") :""; ?> </td>
													</tr>
												  
													<tr>
													   <td> Prestasi  </td>
													   <td> <?php echo isset($dataform) ?  $dataform->prestasi :""; ?> </td>
													</tr>
													  <tr>
													   <td> Alamat  </td>
													   <td> <?php echo isset($dataform) ?  $dataform->alamat :""; ?> </td>
													</tr>

													<tr>
													   <td> NIK Ayah  </td>
													   <td> <?php echo isset($dataform) ?  $dataform->nik_ayah :""; ?> </td>
													</tr>

													<tr>
													   <td> Nama Ayah  </td>
													   <td> <?php echo isset($dataform) ?  $dataform->nama_ayah :""; ?> </td>
													</tr>

													<tr>
													   <td> NIK Ibu  </td>
													   <td> <?php echo isset($dataform) ?  $dataform->nik_ibu :""; ?> </td>
													</tr>

													<tr>
													   <td> Nama Ibu  </td>
													   <td> <?php echo isset($dataform) ?  $dataform->nama_ibu :""; ?> </td>
													</tr>
												</table>
											 
											 
										  </div>
										  <div id="madrasah" class="tab-pane fade">
											
											 <table class="table-hover table-bordered table">
											       <tr>
													   <td> Jenis  </td>
													   <td> <?php  $arjenis = array("1"=>"Madrasah","2"=>"Sekolah"); echo isset($madrasah) ?  $arjenis[$madrasah->jenis] :""; ?> </td>
													</tr>
													<tr>
													   <td> Jenjang </td>
													   <td> <?php echo isset($madrasah) ?  $this->Di->jenjangnama($madrasah->jenjang) :""; ?> </td>
													</tr>
													<tr>
													   <td> NSM/NPSN </td>
													   <td> <?php echo isset($madrasah) ?  $madrasah->username :""; ?> </td>
													</tr>
													
													<tr>
													   <td> Nama Madrasah/Sekolah </td>
													   <td> <?php echo isset($madrasah) ?  $madrasah->nama :""; ?> </td>
													</tr>
													<tr>
													   <td> Telepon </td>
													   <td> <?php echo isset($madrasah) ?  $madrasah->telepon :""; ?> </td>
													</tr>
													<tr>
													   <td> Email  </td>
													   <td> <?php echo isset($madrasah) ?  $madrasah->email :""; ?> </td>
													</tr>
													<tr>
													   <td> Provinsi  </td>
													   <td> <?php echo isset($madrasah) ?  $this->Di->get_kondisi(array("id"=>$madrasah->provinsi),"provinsi","nama") :""; ?> </td>
													</tr>
													<tr>
													   <td> Kabupaten/Kota  </td>
													   <td> <?php echo isset($madrasah) ?  $this->Di->get_kondisi(array("id"=>$madrasah->kota),"kota","nama")  :""; ?> </td>
													</tr>
													<tr>
													   <td> Kecamatan  </td>
													   <td> <?php echo isset($madrasah) ?  $this->Di->get_kondisi(array("id"=>$madrasah->kecamatan),"kecamatan","nama")  :""; ?> </td>
													</tr>
													<tr>
													   <td> Desa/Kelurahan  </td>
													   <td> <?php echo isset($madrasah) ?  $this->Di->get_kondisi(array("id"=>$madrasah->desa),"desa","nama")  :""; ?> </td>
													</tr>
													<tr>
													   <td> Alamat Madrasah/Sekolah </td>
													   <td> <?php echo isset($madrasah) ?  $madrasah->alamat :""; ?> </td>
													</tr>
													<tr>
													   <td> NIK Ketua Delegasi </td>
													   <td> <?php echo $madrasah->delegasi_nik; ?></td>
													</tr>
													<tr>
													   <td> Nama Ketua Delegasi </td>
													   <td> <?php echo $madrasah->delegasi_nama; ?></td>
													</tr>
													<tr>
													   <td> Kontak Ketua Delegasi </td>
													   <td> <?php echo $madrasah->delegasi_hp; ?></td>
													</tr>
													
												</table>
											
										  </div>
										  
										   <div id="kompetisi" class="tab-pane fade">
											<br>
											   <center><b> Jumlah Pendaftar dari <?php echo $madrasah->nama; ?> </b></center>
											 <table class="table-hover table-bordered table">
													<thead>
													<tr>
													   
													   <th> Kompetisi </th>
													   <th> Jumlah Pendaftar </th>
													</tr>
													</thead>
													
													<tbody>
													  <?php 
													   $kompetisi = $this->db->get_where("tr_kompetisi",array("tmmadrasah_id"=>$madrasah->jenjang))->result();
													     foreach($kompetisi as $r){
															 $jml = $this->db->query("select count(id) as jml from tm_siswa where tmmadrasah_id='".$madrasah->id."' and trkompetisi_id='".$r->id."'")->row();
															 ?>
															  <tr>
															     <td> <?php echo $r->nama; ?></td>
															     <td> <?php echo $jml->jml; ?> Peserta</td>
															 </tr>
															 <?php 
															 
														 }
														 
														?>
													
													</tbody>
													
													
													
												</table>
											
										  </div>
										  
										</div>
									     
										
							</div>
							
							<div class="col-md-4">
										
										<br>
										<br>
										
										<div class="form-group">
										   <?php 
										     if(isset($dataform)){
												 if($dataform->foto==""){
													$file = "ksm.png";
												 }else{
													 $file = $dataform->foto;
												 }
												 
											 }else{
												 
												    $file = "ksm.png";
											 }
											 
											 ?>
											<center><img src="<?php echo base_url(); ?>__statics/upload/<?php echo $file; ?>" class="img-thumbnail img-responsive" style="height:200px;" id="blah" ></center>
										</div>
										 <div class="alert alert-info"><b>Status  Peserta </b></div> 
								   <div class="form-check">
								  <table class="table ">
								        <?php 
										  $status = array("0"=>"Draft (Belum terkirim)","1"=>"Belum diVerifikasi","2"=>"Lulus","3"=>"Tidak Lulus");
										  
										  ?>
										  <tr>
										      <td><h4><b> <?php echo $status[$dataform->status]; ?> </b></h4></td>
										  </tr>
										 
								  
								  </table>
								  
								 
								  
								  
								    </div>
								 
									</div>
							
                             

							
                        
                   
                    </div>
					      <center>
                              <button type="button" class="btn btn-general btn-white" id="cancel">Tutup  </button> 
                               
							<center>
							<br>
                    </div>
                    </div>
                    </div>
	