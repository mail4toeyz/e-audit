   <div id="showform"></div>        
                                 
					  <div class="card form" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-file-text-o"></i>  Peserta Kompetisi Sains Madrasah  </h3>
                        </div>
                        <br>
                         
                            <div class="col-md-12">
							<div class="alert alert-info" style="font-size:12px">

								 Petunjuk ! <br>
								 <?php 
								   if($status !=2){
									echo "Dibawah ini adalah data Peserta KSM Tingkat Satuan Pendidikan yang Anda kirimkan, Peserta yang terverifikasi oleh Komite Kabupaten/Kota akan diikutkan dalam KSM tingkat Kabupaten/Kota";

								   }else{
									echo "Dibawah ini adalah data Peserta KSM Tingkat Kabupaten/Kota, Silahkan unduh kartu Peserta pada  menu <b>pengumuman</b> ";

								   }
								   ?>
								</div>
				
					       <input type="hidden" id="status" value="<?php echo $status; ?>">
					              
			
		
			              </div>

		         
	<div class="col-md-12">   				
		   <div class="table-responsive">
			<table id="datatableTable" class="table  table-bordered table-hover table-striped" >
			  <thead>
				<tr>
				  
				   <th class="center" width="2%" rowspan="2">NO</th>	
				   <th class="center" width="10%" rowspan="2">AKSI</th>	
				   <th class="center" rowspan="2">FOTO  </th>				  
				   <th class="center" rowspan="2">NIK</th>				  
				   <th class="center" rowspan="2">NISN</th>				  
				   <th class="center" rowspan="2">NAMA  </th>		  
				  		  
				   <th class="center" rowspan="2">JK</th>				  
				   <!--<th class="center" rowspan="2">TTL</th>	 -->			  
				   				  
				   <th class="center" rowspan="2">KOMPETISI</th>	
				   <th class="center" colspan="6">RINCIAN SKOR HASIL KABUPATEN/KOTA </th>			  
				  
				   			  
				 
				  				  
				
				  
				  
				</tr>
				<tr>
				     <th> BENAR PG</th>
				     <th> BENAR ESSAY</th>
				     <th> JUMLAH BENAR </th>
				     <th> SALAH </th>
				     <th> KOSONG PG & SALAH ESSAY </th>
				     <th> NILAI </th>
				  </tr>
				

			  </thead>
			  <tbody></tbody>
            </table>
			</div>
        </div>
        </div>
		

<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {

						"processing": true,
						"language": {
						"processing": '<b style="color:white;background:#33AFFF">Data sedang di tampilkan..</b>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Tidak Tersedia",
						   "sLengthMenu": "Default menampilkan   _MENU_ Data <small>(Klik pada angkanya untuk memuat lebih banyak data)</small>"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[5,10,25, 50,100,200,300,500,1000, 800000000], [5,10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					 
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							exportOptions: {
							  columns: [ 0,2,3,4,5,6]
							},
							text:'<span class="fa fa-file-excel-o"></span> Cetak Excel',
							className :"btn btn-danger btn-sm"
							},
							
							
						
						
							{
							extend: 'colvis',
							
							text:'<span class="fa fa-columns "></span> Setting Kolom ',
							className :"btn btn-danger "
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("lokal/g_peserta"); ?>", 
						type: "post", 
						"data": function ( data ) {

						data.status = "<?php echo $status; ?>";
						
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
	
		
		$(document).on('input click change', '#keyword,#searchcustom', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
		
		

</script>
			
                    