
<style>
    .nukahiji{}
    .nukahiji header{font-size:13pt;text-align: center;font-weight: bold;margin-bottom: 0.1in;padding-bottom: 5px;border-bottom: 3px #000 solid;}
    .nukahiji header img{float: left;height: 60px;}
    .nukahiji .profil{margin-bottom: 20px;}
      .nukahiji .profil table{width: 100%}
      .nukahiji .profil table tr td{padding:5px 0px;color:#000;}
      .nukahiji .profil table tr td strong{font-size: 14pt;}
    .nukahiji .req_table{margin-bottom: 0px;}
    .nukahiji .req_table table{width: 100%;}
    .nukahiji .req_table table tr td{padding:5px;}
      .nukahiji .req_table table thead tr td{font-weight: bold;text-align: center;}
    .nukahiji .catatan{font:11pt arial;font-style: italic;clear: both;}
    .nukahiji .catatan .textbox{display: block;border:1px #000 solid;margin:20px 0px;padding:10px;font-weight: bolder;}
	#table_detail
	{
		border-bottom:1px #000 solid;
		border-right:1px #000 solid;
	}
	
	#table_detail tr td
	{
		border-top:1px #000 solid;
		border-left:1px #000 solid;
	}
	.photo
	{
		width:117px!important;
		height:140px!important;
		padding:5px;
		border:1px #CCCCCC solid;
		background-color:#ffffff;
	}	
  </style>
  
  <?php 

  //$dokumenroot = $_SERVER["DOCUMENT_ROOT"]."/";
  $dokumenroot = base_url()."/";
?>
<div class="nukahiji" id="printableArea" style="width:590px; margin:0px auto;padding: 0.1in;font: 13pt arial; border:1px #000 solid;">
		  <header><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			 
			 <table border="0" width="100%">
			 <tr>
			   <td width="20%">
			  <img src="<?php echo $dokumenroot; ?>__statics/img/kemenag.png">
			  </td>

			  <td align="center" width="60%">		  
			
			<strong > TANDA BUKTI PENDAFTARAN </strong><br/>
			<strong style="font-size:14px;font-weight:bold"> KOMPETISI SAINS MADRASAH 
			   </strong>  
			<strong style="font-size:14px;font-weight:bold"> <br> TAHUN 2022  </strong>
			</td>

			  <td width="20%"> <img src="<?php echo $dokumenroot; ?>__statics/img/ksm.png"></td>

			  </tr>

			</table>
			  
		  </header>
		  <div class="profil">
				<table style="border:none;">
				  <tr>
					<td width="140px;">Nama</td><td width="2px">:</td><td><?php echo ucwords($siswa->nama); ?></td>
					<td rowspan="5" align="right">
								<?php 
								$foto = $siswa->foto;						

								$foto    = "__statics/upload/".$foto;								
								?>
								<img src="<?php echo $dokumenroot; ?><?php echo $foto; ?>"  class="photo"/>
					</td>
				  </tr>
				 
				  <tr><td>Tanggal Lahir</td><td>:</td><td><?php echo $this->Di->formattanggalstring($siswa->tgl_lahir); ?></td></tr>				  
				  <tr><td>Jenis Kelamin</td><td>:</td><td><?php echo $siswa->gender; ?></td></tr>				  
				  <tr><td>NIK</td><td>:</td><td><?php echo ucwords($siswa->nik_siswa); ?></td></tr>
				  <tr><td>NISN</td><td>:</td><td><?php echo ucwords($siswa->nisn); ?></td></tr>
				  <tr><td>Lembaga Asal </td><td>:</td><td><?php echo $this->Di->get_kondisi(array("id"=>$siswa->tmmadrasah_id),"tm_madrasah","nama"); ?> </td></tr>				 
				  <tr><td>Provinsi  </td><td>:</td><td><?php echo $this->Di->get_kondisi(array("id"=>$siswa->provinsi),"provinsi","nama"); ?></td></tr>
				  <tr><td>Kabupaten/Kota  </td><td>:</td><td><?php echo $this->Di->get_kondisi(array("id"=>$siswa->kota),"kota","nama"); ?></td></tr>
				
				 
				  
				 
				  
				  
				 
				
				
				
				</table>
			  </div>

  <p style="text-decoration:underline; font-size:12px;">Data Bidang Studi  : </p>
  

  <div class="req_table">
    <table id="table_detail" style="font-size:12px;">
	 <tr>
        <td> Jenjang  </td>
        <td><?php  $jenjang = array("1"=>"MI/SD","2"=>"MTs/SMP","3"=>"MA/SMA"); echo $jenjang[$this->Di->get_kondisi(array("id"=>$siswa->trkompetisi_id),"tr_kompetisi","tmmadrasah_id")]; ?></td>
      </tr>

     
     
      <tr>
        <td> Bidang Studi  </td>
        <td><?php echo $this->Di->get_kondisi(array("id"=>$siswa->trkompetisi_id),"tr_kompetisi","nama"); ?></td>
      </tr>	  
    
     
      
      
	  <tr>
	   <td  align="center">
	    
	     
      </td>
	  <td align="center">
	 
	  </td>
	  </tr> 
        
    </table>
  </div>
  
   <small><i style="font-size:9px;font-weight:bold"> Catatan : ini hanya sebagai bukti bahwa anda telah menyelesaikan pendaftaran(tidak perlu di cetak),  QR code dapat digunakan untuk melakukan proses validasi data<br>
       
 
  </i></small>
</div>

