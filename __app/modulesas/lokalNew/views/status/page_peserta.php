   <div id="showform"></div>        
                                 
					  <div class="card form" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-users"></i> Peserta Tes Kabupaten/kota</h3>
                        </div>
                        <br>
                         
                            <div class="col-md-12">
							<div class="alert alert-info">
							 Petunjuk ! <br>
							  Peserta yang Anda daftarkan lulus tahap verifikasi oleh Komite Kabupaten/kota dapat dilihat ditabel ini  <br>
							  Silahkan download Kartu Peserta
							</div>
								 
									<br>
			 
			 
			              </div>
			
		           <hr>
		<div class="col-md-12">   				
		   <div class="table-responsive">
			<table id="datatableTable" class="  table-bordered table-hover table-striped" width="100%">
			  <thead>
				<tr>
				   <th class="center" width="3%">No</th>
				   			  
				   <th class="center">Kartu Peserta</th>				  
				   <th class="center">Nomor  </th>				  
				   <th class="center">NISN</th>				  
				   <th class="center">Nama Peserta </th>			  
				   				  
				   <th class="center">Gender</th>				  
				   <th class="center">TTL</th>				  
				   				  
				   <th class="center">Kompetisi</th>				  
				
				  
				</tr>
			  </thead>
			  <tbody></tbody>
            </table>
			</div>
        </div>
        </div>
		

<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<b style="color:white;background:#33AFFF">Data sedang di tampilkan..</b>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data tidak atau belum tersedia, silahkan input data di menu Input Peserta",
						   "sLengthMenu": "Default menampilkan   _MENU_ Data <small>(Klik pada angkanya untuk memuat lebih banyak data)</small>"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[5,10,25, 50,100,200,300,500,1000, 800000000], [5,10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							exportOptions: {
							  columns: [ 0,1,2,3,4,5,6,7]
							},
							text:'<span class="fa fa-file-excel-o"></span> Cetak Excel',
							className :"btn btn-danger btn-sm"
							},
							
							
						
						
							{
							extend: 'colvis',
							
							text:'<span class="fa fa-columns "></span> Setting Kolom ',
							className :"btn btn-danger "
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("lokal/g_status"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
	
		
		$(document).on('input click change', '#keyword,#searchcustom', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
		
		$(document).on(' click ', '#cancelr', function (event, messages) {			
			   $("#showform").html("");
			   dataTable.ajax.reload(null,false);	        
		  
        });
		

</script>
			
                    