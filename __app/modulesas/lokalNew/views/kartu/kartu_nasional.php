	  
<style>
    .nukahiji{}
    .nukahiji header{font-size:13pt;text-align: center;font-weight: bold;margin-bottom: 0.1in;padding-bottom: 5px;border-bottom: 3px #000 solid;}
    .nukahiji header img{float: left;height: 60px;}
    .nukahiji .profil{margin-bottom: 20px;}
      .nukahiji .profil table{width: 100%}
      .nukahiji .profil table tr td{padding:5px 0px;color:#000;}
      .nukahiji .profil table tr td strong{font-size: 14pt;}
    .nukahiji .req_table{margin-bottom: 0px;}
    .nukahiji .req_table table{width: 100%;}
    .nukahiji .req_table table tr td{padding:5px;}
      .nukahiji .req_table table thead tr td{font-weight: bold;text-align: center;}
    .nukahiji .catatan{font:11pt arial;font-style: italic;clear: both;}
    .nukahiji .catatan .textbox{display: block;border:1px #000 solid;margin:20px 0px;padding:10px;font-weight: bolder;}
	#table_detail
	{
		border-bottom:1px #000 solid;
		border-right:1px #000 solid;
	}
	
	#table_detail tr td
	{
		border-top:1px #000 solid;
		border-left:1px #000 solid;
	}
	.photo
	{
		width:116px!important;
		height:140px!important;
		padding:5px;
		border:1px #CCCCCC solid;
		background-color:#ffffff;
	}	
  </style>

<script type="text/javascript">
	window.print();
  </script>
<div class="nukahiji" id="printableArea" style="width:650px;background:#fff; margin:0px auto;padding: 0.1in;font: 13pt arial; border:1px #000 solid;">
		  <header>
			  <?php 
            //$documentroot =$_SERVER["DOCUMENT_ROOT"];
            $documentroot = base_url();
              
			   $pelaksanaan = array("1"=>"Selasa, 11 Oktober 2022 ","2"=>"Rabu, 12 Oktober 2022");
              
			   $sesiNas     = $this->Di->get_kondisi(array("id"=>$data->id),"tm_siswa","sesi_nas");
			   $sesiData     = $this->db->get_where("tm_sesi",array("id"=>$sesiNas))->row();

			   
			  ?>
			 
			 
			  
			  
			  <table width="100%">
			  
			   
				<tr>
				  <td width="10%" align="left"> <img src="<?php echo $documentroot.'/__statics/img/kemenag.png';?>"></td>
				    <td align="center" width="80%" style="font-weight:bold">
				       <span style="font-size:18px;color:black"> KEMENTERIAN AGAMA REPUBLIK INDONESIA    </span><br/>
					   <span style="font-size:14px;color:black"> DIREKTORAT JENDERAL PENDIDIKAN ISLAM </span><br>
					   <span style="font-size:14px;color:black"> KOMPETISI SAINS MADRASAH <br> TAHUN 2022 </span>
					   
				  </td>
				  <td width="10%" align="right"><img src="<?php echo $documentroot.'/__statics/img/ksm.png';?>"></td>
				  
				</tr>
			 </table>
			 
		  </header>
		   
			  <div class="profil">
			  <center style="font-weight:bold;font-size:12px;color:black"> KARTU PESERTA KSM NASIONAL  </center>
			  
				
				<table  width="100%">
				<tr>
				
				<td  align="left" valign="top" width="25%">
						
							<img src="<?php echo $documentroot; ?>/__statics/upload/<?php echo $data->foto; ?>"  class="photo" />
				</td>
				<td>
							 <table  width="100%" style="font-size:13px">
							 <tr><td>Nomor Peserta  </td><td>:</td><td><?php echo ucwords($data->no_test2); ?></td></tr>
							
							  <tr>
								<td  width="22%" style="height:3px">Nama Lengkap </td>
								<td width="3%" style="height:3px">:</td>
								<td width="50%" style="height:3px"> <?php echo ucwords($data->nama); ?> </td>
							  </tr>
							  
							   <tr><td>Tempat, Tanggal Lahir </td><td>:</td><td><?php echo ($data->tempat); ?>, <?php echo strtoupper($this->Di->formattanggalstring($data->tgl_lahir)); ?></td></tr>
							  <tr><td>Asal Lembaga </td><td>:</td><td><?php echo $data->madrasah; ?></td></tr>								 
							  <tr><td>Kompetisi  </td><td>:</td> <td>  <?php echo strtoupper($this->Di->get_kondisi(array("id"=>$data->trkompetisi_id),"tr_kompetisi","nama")); ?> </td></tr>
							  <tr><td>Lokasi Tes  </td><td>:</td> <td> <?php echo strtoupper("Daerah Khusus Ibukota Jakarta"); ?> </td></tr>
							  
								
							  </table>
							 
				
				
				</td>
				</tr>
					
					 <tr>
					 <td colspan="2"> 
					  
					  
							   <div class="req_table">
									<table id="table_detail" style="font-size:12px;">
								
										<tr style="font-weight:bold;">
										    <td>AGENDA   </td>
											<td>HARI    </td>											
											<td>SESI    </td>											
											<td>PUKUL  </td>
										
											
										  </tr>
									
										
										

                                          <tr>
										    <td> Pelaksanaan  CBT  KSM Nasional     </td>
											<td> <?php echo $pelaksanaan[$sesiData->hari]; ?></td>
											<td> <?php echo $sesiData->sesi; ?> </td>
											<td> <?php echo $sesiData->wib; ?> WIB</td>
											
										
											
										  </tr>
										
                                          <tr>
										    <td> Pelaksanaan  Eksplorasi dan Eksperimen     </td>
											<?php 
											 if($sesiData->hari==1){
												?><td> <?php echo $pelaksanaan[2]; ?> </td><?php
											 }else if($sesiData->hari==2){
												?><td> <?php echo $pelaksanaan[1]; ?> </td><?php

											 }
											 ?>
											
											<td> - </td>
											<td> 08.00 - 13.00 WIB </td>
											
										
											
										  </tr>
											  
											  
									
												   
										
										  
									
										
								  </table>
							  </div>
					 </td>
				</table>
                <ol type="circle" style="font-size:12px">
				<li>Anda dapat mencetak kartu peserta menggunakan <i>printer</i> berwarna </li>
				<li>Login akun <i>CBT</i> KSM Anda  menggunakan : <br>
				
											 <b>  Nomor Peserta : <?php echo ucwords($data->no_test3); ?></b> <br> 
				                             <b>  Tanggal lahir : <?php echo ucwords($data->tgl_lahir); ?> </b> </b></i>
											 
											  </li>
											  
		       
				
			    </ol>
					
				
                </div>
           
                <hr>
				<div style="page-break-before: always;"></div>
			
                <center><img src="<?php echo $documentroot.'/__statics/img/nasional.jpeg';?>" width="100%"></center>
			  
			  
			   

  
</div>



  

 