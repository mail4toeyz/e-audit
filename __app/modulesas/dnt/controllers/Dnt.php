<?php
ini_set('memory_limit', '-1');

defined('BASEPATH') OR exit('No direct script access allowed');

class Dnt extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 if(!$this->session->userdata("admin_id")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_guru','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('pusat/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Daftar Nominasi Tetap ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		  error_reporting(0);
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		  $arbantuan = array("1"=>"Kinerja","2"=>"Afirmasi");
		  
		   $i= ($iDisplayStart +1);
		   $status    = $_POST['status'];
		   $arbantuan = array("1"=>"<span class='badge badge-light-warning'>Kinerja</span>","2"=>"<span class='badge badge-light-primary'>Afirmasi</span>");
		   $status    =  array("0"=>"<span class='fa fa-times' style='color:red'></span>","1"=>"<span class='fa fa-check-square-o primary' style='color:blue'></span>");
	 

		   foreach($datagrid as $val) {
			$cekVisitasi = $this->db->query("select id from visitasi_kegiatan where madrasah_id='{$val['id']}' limit 1")->row();
			if(!is_null($cekVisitasi)){
			  $visitasi ='<button href="'.site_url('hasilvisitasi/detail?id='.$val['id'].'').'" class="btn btn-primary btn-sm menuajax" >Laporan </button>';
			}else{

			  $visitasi ="<span class='badge badge-light-danger'> Belum  </button>";
			}

			if($val['dnt']==1){
				$visitasi .="<br><span class='badge badge-light-danger'> Masuk DNT  </button>";

			}


			$hasilEDM       = $this->db->query("select * from visitasi_edm where madrasah_id='{$val['id']}'")->row();
			$no = $i++;
				$this->db->query("update madrasahedm set rank_visitasi='{$no}' where id='{$val['id']}'");
				


				$cata = $this->db->query("select id from verifikasi_catatan where madrasah_id='{$val['id']}'")->num_rows();

				$visitasi .="<span class='badge badge-light-danger'>".$cata." Catatan  </button>";


				$saldo1 = $this->db->query("select sum(biaya) as total from visitasi_kegiatan where tahun='2021' and madrasah_id='".$val['id']."'")->row();
				$saldo2 = $this->db->query("select sum(biaya) as total from visitasi_kegiatan where tahun='2022' and madrasah_id='".$val['id']."'")->row();

				$total = $saldo1->total+$saldo2->total;

				$records["data"][] = array(
					$no,
					$visitasi,
					$this->Reff->formatuang2($saldo1->total),
					$this->Reff->formatuang2($saldo2->total),
					$this->Reff->formatuang2($total),
					strtoupper($val['jenjang']),
					$val['nsm'],
					$val['nama'],
					$this->Reff->get_kondisi(array("nsm"=>$val['nsm']),"madrasah","telepon"),
					$this->Reff->get_kondisi(array("nsm"=>$val['nsm']),"madrasah","email"),
					$val['akreditasi'],
					$val['provinsi'],
					$val['kota'],
					$val['rank'],
					$val['rank_visitasi'],
					number_format($val['skor_akhir_visitasi'],1),
					number_format($val['skor_edm_visitasi'],1),
					number_format($val['skor_pip'],1),
					number_format($val['skor_rombel_visitasi'],1),
					number_format($val['skor_toilet_visitasi'],1),

					number_format($val['skor_akhir'],1),
					number_format($val['skor_edm'],1),
					number_format($val['skor_pip'],1),
					number_format($val['skor_rombel'],1),
					number_format($val['skor_toilet'],1),
					
				
					$val['jml_siswa'],
					$val['jml_guru'],
					$val['siswa_laki'],
					$val['siswa_perempuan'],
					$val['rombel_visitasi'],
					$val['rombel_ideal'],
					number_format($val['rombel_persen_visitasi'],1),
					$val['toilet_jumlah_visitasi'],
					$val['toilet_ideal'],
					number_format($val['toilet_persen_visitasi'],1),

					$val['jml_guru'],
					$val['siswa_pip'],
					number_format($val['persen_pip'],1)." %",
					$val['skor_pip'],
					sprintf('%0.2f',$hasilEDM->nilai_kedisiplinan),
					sprintf('%0.2f',$hasilEDM->nilai_pengembangan_diri),
					sprintf('%0.2f',$hasilEDM->nilai_proses_pembelajaran),
					sprintf('%0.2f',$hasilEDM->nilai_sarana_prasarana),
					sprintf('%0.2f',$hasilEDM->nilai_pembiayaan),
					number_format($val['skor_edm_visitasi'],2),
					number_format($val['nilai_skpm'],2),
					$this->Reff->formatuang2($val['rencana_pendapatan']),
					$this->Reff->formatuang2($val['total_rencana_kegiatan'])
					
					
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
		public function detail(){
			$id            = $this->input->get_post("id",true);
			
			$data['data']    = $this->db->get_where("madrasahedm",array("id"=>$id))->row();
			$cekAsesor       = $this->db->query("select asesor from visitasi_edm where madrasah_id='{$id}' limit 1")->row();

			$data['asesor']  = $this->db->get_where("asesor",array("id"=>$cekAsesor->asesor))->row();

			$ajax            = $this->input->get_post("ajax",true);	
				$data['title']   = "Dokumentasi Visitasi";
				
				if(!empty($ajax)){
								
					$this->load->view('detail',$data);
				
				}else{
					
					
					$data['konten'] = "detail";
					
					$this->_template($data);
				}


		}



		public function bukti(){

			$this->load->helper('exportpdf_helper'); 

			
			$data['madrasah'] = $this->db->query("select * from madrasahedm where nsm='{$_GET['nsm']}' and id  IN(select madrasah_id from visitasi_kegiatan)")->row();
			$data['data']     = $this->db->query("select * from madrasahedm where nsm='{$_GET['nsm']}' and id  IN(select madrasah_id from visitasi_kegiatan)")->row();
			$data['petugas'] = $this->db->get_where("asesor",array("nik"=>$_GET['nik']))->row();
			$data['aman'] = $this->db->get_where("visitasi_jadwal",array("nik"=>$_GET['nik'],"nsm"=>$_GET['nsm']))->row();
			$user_info = $this->load->view('bukti', $data, true);
			$pdf_filename = 'Hasil Visitasi'.$data['data']->nsm.'.pdf';	 
			
			 $output = $user_info;
			
			generate_pdf($output, $pdf_filename);
      }


	  public function emailBC(){
       
		$data = $this->db->query("SELECT * FROM `madrasahedm` where dnt=1")->result();
				foreach($data as $r){
					
					
						$email = "dendi.raziz@gmail.com";
						$nama  = $r->nama;
						$data = array();

						$tes = $this->m->post('http://appmadrasah.kemenag.go.id/cbtpkb/web/send_mailBKBADNT?email='.$email.'&nama='.$nama.'', $data);
						
						echo $tes;

					

				}
   
   }
	
	
	 
}
