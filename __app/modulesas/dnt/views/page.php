
		<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Post-->
<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
	<div id="kt_content_container" class="container-xxl">
				       <div class="card">
						<div class="card-header card-header-success">
						  <h4 class="card-title"> Daftar Nominasi Tetap </h4>
						  <p class="card-category">
						  <div class="row">
													
													
						                      

													<div class="col-md-2">
													  <select class="form-control" id="statusVisit">
														   <option value="">- Daftan Nominasi Tetap    - </option>
														   <option value="1" selected> Masuk </option>
														   <option value="2"> Tidak  </option>
														   <option value="3"> Tampilkan Keseluruhan  </option>
														  
														  
													   
													   </select>
												   
													</div>

													<div class="col-md-2">
													  <select class="form-control" id="jenjang">
														   <option value="">- Jenjang - </option>
														   <?php 
															 $jenjang = array("mi","mts","ma");
															   foreach($jenjang as $r){
																   ?><option value="<?php echo $r; ?>"  > <?php echo strtoupper($r); ?> </option><?php 
																   
															   }
															  ?>
														  
													   
													   </select>
												   
													</div>
													
													<div class="col-md-2">
												
													  <select class="form-control onchange " id="provinsi" target="kota" urlnya="<?php echo site_url('publik/kota'); ?>">
														   <option value="">- Provinsi - </option>
														   <?php 
															   $provinsi = $this->db->get_where("provinsi",array("active"=>1))->result();
															   foreach($provinsi as $row){
																   ?><option value="<?php echo strtoupper($row->kode); ?>"> <?php echo ($row->nama); ?> </option><?php 
																   
															   }
															  ?>
													   
													   </select>
												   
													</div>

													<div class="col-md-2">
												
													  <select class="form-control kota " id="kota" target="kota" urlnya="<?php echo site_url('publik/kota'); ?>">
														   <option value="">- Kabupaten/Kota - </option>
														  
													   
													   </select>
												   
													</div>
													
													
													<div class="col-md-3">
													  <input class="form-control" id="keyword"  placeholder="Apa yang Anda cari ?">
													  <input class="form-control" type="hidden" id="bantuan" value="<?php echo isset($_GET['bantuan']) ? $_GET['bantuan']:''; ?>">
													
												   
													</div>
													<div class="col-md-1">
													  <button class="btn btn-sm btn-primary" id="tampilkan" > <i class="fa fa-search"></i> </button> 
												   
													</div>
											   
											</div>
						  </p>
						</div>
					  
						 <div class="card-body">
						

						 <button onclick="btn_export()" class="btn btn-default">
    Cetak Excel 
  </button>

						 <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
                                        <tr>
                                            <th width="2px" rowspan="2">NOMOR</th>
                                            <th width="2px" rowspan="2">DETAIL</th>
											<th colspan="3">RENCANA </th>
                                            <th rowspan="2">JENJANG </th>
                                            <th rowspan="2">NSM </th>
                                            <th rowspan="2">NAMA </th>
                                            <th rowspan="2">TELEPON </th>
                                            <th rowspan="2">EMAIL </th>
                                            <th rowspan="2">AKREDITASI </th>
                                            <th rowspan="2">PROVINSI </th>
                                            <th rowspan="2">KABKOTA </th>
											<th rowspan="2">RANK AWAL </th>
											<th rowspan="2">RANK AKHIR </th>
                                            <th rowspan="2">SKOR VISITASI </th>
                                            <th colspan="4">RINCIAN SKOR VISITASI </th>
											

											<th rowspan="2">SKOR AWAL </th>
                                            <th colspan="4">RINCIAN SKOR AWAL </th>


											<th colspan="11">EMIS </th>
											<th colspan="3">PIP </th>
                                            <th colspan="7">SKOR HASIL EDM </th>
                                            <th colspan="2">eRKAM </th>
											
											
                                            
                                        </tr>

										<tr>

										  <th> 2021 </th>
										  <th> 2022 </th>
										  <th> TOTAL </th>


										  <th> EDM </th>
										  <th> PIP </th>
										  <th> Ruang Belajar </th>
										  <th> Toilet </th>

										  <th> EDM </th>
										  <th> PIP </th>
										  <th> Ruang Belajar </th>
										  <th> Toilet </th>

										  <th> Jml Siswa </th>
										  <th> Jml Guru  </th>
										  <th> Siswa (L) </th>
										  <th> Siswa (P) </th>
										  <th> Jml Ruang Belajar </th>
										  <th> Ruang Belajar(Ideal) </th>
										  <th> Ruang Belajar(%) </th>
										  <th> Jml Toilet </th>
										  <th> Toilet (Ideal) </th>
										  <th> Toilet (%) </th>
										  <th> Jml Guru </th>


										  <th> Siswa Penerima PIP</th>
										  <th> Persen </th>
										  <th> Skor </th>
										  <th> Kedisiplinan</th>
										  <th> Pengembangan Diri</th>
										  <th> Pembelajaran</th>
										  <th> Sarana & Prasarana</th>
										  <th> Pembiayaan</th>
										  <th> Skor EDM </th>
										  <th> SKPM </th>
										  <th> Rencana Pendapatan </th>											
										  <th> Total Rencana Kegiatan </th>

										 

										</tr>

                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                         </div>
                         </div>
                         </div>
                         </div>
                         </div>
                 
 
 
              
        
<div id="defaultModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body" id="loadform">
        <p>Loading...</p>
      </div>
      
    </div>

  </div>
</div>
	
<script src="<?php echo base_url(); ?>__statics/js/excel/xlsx.extendscript.js"></script>
<script src="<?php echo base_url(); ?>__statics/js/excel/xlsx-style/xlsx.full.min.js"></script>

<script src="<?php echo base_url(); ?>__statics/js/excel/export.js"></script>
	
<script type="text/javascript">

function btn_export() {
        var table1 = document.querySelector("#datatableTable");
        var opt = {
            rowIndex: 4
        }; //开头空4行
        var sheet = XLSX2.utils.table_to_sheet(table1, opt);

  
     

        openDownloadDialog(sheet2blob(sheet), ' .Hasil Visitasi.xlsx');
    }
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"scrollY": 500,
                        "scrollX": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[ 50,100,200,300,500,1000, 800000000], [50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					
                     buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
					
					"ajax":{
						url :"<?php echo site_url("dnt/grid"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						data.status = $("#status").val();
						data.jenjang = $("#jenjang").val();
						data.provinsi = $("#provinsi").val();
						data.kota = $("#kota").val();
						data.keyword = $("#keyword").val();
						data.bantuan = $("#bantuan").val();
						data.statusVisit = $("#statusVisit").val();
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				

				$(document).on("change","#status,#jenjang,#provinsi,#kota,#statusVisit",function(){


					dataTable.ajax.reload(null,false);	
				});

				$(document).on("input","#keyword",function(){


				   dataTable.ajax.reload(null,false);	
				});
				
				
	


</script>
				