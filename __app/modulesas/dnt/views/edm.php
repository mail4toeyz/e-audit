<div class="row">

   <div class="col-xl-12">
          <div class="table-responsive">
            <table class="table table-hover table-bordered table-striped">
                <thead>
                <tr class="fw-bolder  bg-light" style="font-weight:bold">
                    <th rowspan="2">NO</th>
                    <th rowspan="2">ASPEK</th>
                    <th colspan="3">SKOR KINERJA PENCAPAIAN MUTU</th>
                   
                </tr>
                <tr class="fw-bolder  bg-light" style="font-weight:bold">
                   <th> TERDATA </th>
                   <th> HASIL VISITASI </th>
                   <th> AKURASI </th>
                </tr>
                </thead>
                <tbody>
                    <?php 
                     $tahun = $this->Reff->tahun();
                     foreach($this->db->get("edm_aspek")->result() as $row){
                         $eksis      = number_format($this->Reff->get_kondisi(array("id"=>$data->id,"tahun"=>$tahun),"madrasahedm",$row->kolom),2);
                         $hasil      = $this->db->query("select * from visitasi_edm where madrasah_id='{$data->id}'")->row();
                         $kolom      = $row->kolom;
                         $nilainya   = isset($hasil->$kolom) ? $hasil->$kolom :0;
                         $eksis      = round($eksis,1);
                         $persentase = isset($hasil->$kolom) ? number_format(($hasil->$kolom/$eksis) * 100,0) :0;
                      ?>
                       <tr>
                        <td><?php echo $row->id; ?></td>
                        <td><?php echo $row->nama; ?></td>
                        <td><?php echo $eksis; ?></td>
                        <td><?php echo $nilainya; ?></td>
                        <td>
                               <div class="d-flex flex-column w-100 mr-2">
								<div class="d-flex align-items-center justify-content-between mb-2">
									<span class=" mr-2 font-size-sm font-weight-bold" id="persen<?php echo $row->kolom; ?>"><?php echo $persentase; ?>%</span>
									
								</div>
							    <div class="progress progress-xs w-100">
										<div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" id="progress<?php echo $row->kolom; ?>" style="width: <?php echo $persentase; ?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
								</div>
                        </td>
                     </tr>
                     

                      <?php 

                     }
                     ?>

                </tbody>
            </table>

        </div>
   </div>

   
</div>
