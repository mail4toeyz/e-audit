<?php

class M_guru extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
    }

    
	public function grid($paging){
		$tahun   = $this->Reff->tahun();

	    $status = $this->input->get_post("status");
	    $jenjang = $this->input->get_post("jenjang");
	    $provinsi = $this->input->get_post("provinsi");
	    $kota    = $this->input->get_post("kota");
	    $keyword  = $this->input->get_post("keyword");
	    $bantuan  = $this->input->get_post("bantuan");
	    $status_madrasah  = $this->input->get_post("status_madrasah");
		$this->db->select("*");
        $this->db->from('madrasahedm');
		$this->db->where("tahun",$tahun);
		//$this->db->where("nsm IN(SELECT nsm FROM conf_bkba where bersedia_menerima ='Ya')");
    	//$this->db->where("rombel_persen ",100);
	    
	    if(!empty($status_madrasah)){  $this->db->where("inklusi",1);    }
	    if(!empty($jenjang)){  $this->db->where("jenjang",$jenjang);    }
	    if(!empty($provinsi)){  $this->db->where("provinsi_id",$provinsi);    }
	    if(!empty($kota)){  $this->db->where("kota_id",$kota);    }
	    if(!empty($keyword)){  $this->db->where("(UPPER(nama) LIKE '%".strtoupper($keyword)."%' OR UPPER(nsm) LIKE '%".strtoupper($keyword)."%' OR UPPER(npsn) LIKE '%".strtoupper($keyword)."%' )");    }
		

	
		$this->db->where("longlist",1);
		//$this->db->where("folder","Samsul");

		if($status==1){
			if($bantuan !=0){
				$this->db->where("bantuan",$bantuan);  
			}
			
			 $this->db->where("shortlist",1);
		}else if($status==2){

			$this->db->where("shortlist !=",1);
		}

		if(!empty($status_madrasah)){ 

			if($status_madrasah==1){
				$this->db->where("abk >",0); 
		   }else if($status_madrasah==2){
   
			    $this->db->where("simsarpras",1); 
		   }
			
			
			
		
		
		
		}

		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("provinsi_id","ASC");
				if($status==1){
					 if($bantuan==1){
						
						$this->db->order_by("skor_akhir","DESC");
						$this->db->order_by("skor_edm","DESC");
						$this->db->order_by("skor_pip","DESC");
						$this->db->order_by("skor_rombel","DESC");
						$this->db->order_by("skor_toilet","DESC");
						$this->db->order_by("nsm","ASC");

					 }else if($bantuan==2){


						$this->db->order_by("skor_akhir","ASC");
						$this->db->order_by("skor_edm","ASC");
						$this->db->order_by("skor_pip","ASC");
						$this->db->order_by("skor_rombel","ASC");
						$this->db->order_by("skor_toilet","ASC");
						$this->db->order_by("nsm","ASC");


					 }else{
						$this->db->order_by("skor_akhir","DESC");
						$this->db->order_by("skor_edm","DESC");
						$this->db->order_by("skor_pip","DESC");
						$this->db->order_by("skor_rombel","DESC");
						$this->db->order_by("skor_toilet","DESC");
						$this->db->order_by("nsm","ASC");
						
	

					 }
				}else{
					$this->db->order_by("skor_akhir","DESC");
					$this->db->order_by("skor_edm","DESC");
					$this->db->order_by("skor_pip","DESC");
					$this->db->order_by("skor_rombel","DESC");
					$this->db->order_by("skor_toilet","DESC");
					$this->db->order_by("nsm","ASC");

				}
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
	
	public function insert(){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
	    $id               = $this->Reff->get_max_id2("id","tm_guru");
	                         $foldernama  = $_SESSION['tmmadrasah_id'];
				
											if (!is_dir('__statics/upload/'.$foldernama)) {
												mkdir('./__statics/upload/'.$foldernama, 0777, TRUE);

											}
											
								
								  
								$foldernama  =  $_SESSION['tmmadrasah_id']."/guru/".$id;		
											if (!is_dir('__statics/upload/'.$foldernama)) {
												mkdir('./__statics/upload/'.$foldernama, 0777, TRUE);

											}
							$folder   					= '__statics/upload/'.$foldernama;
							
							
							
							
		$this->db->set("tmmadrasah_id",$_SESSION['tmmadrasah_id']);
		$this->db->set("id",$id);
		$this->db->set("folder",$folder);
		$this->db->set("tgl_lahir",$this->Reff->formattanggaldb($_POST['tgl_lahir']));
		$this->db->set("password",encrypt((($_POST['password']))));
		$this->db->set("alias",$_POST['password']);
		$this->db->set("i_entry",$_SESSION['nama_madrasah']);
		$this->db->set("d_entry",date("Y-m-d H:i:s"));
        
		$this->db->insert("tm_guru",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
	public function update($id){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
		 $foldernama  = $_SESSION['tmmadrasah_id'];
				
											if (!is_dir('__statics/upload/'.$foldernama)) {
												mkdir('./__statics/upload/'.$foldernama, 0777, TRUE);

											}
											
								
								  
								$foldernama  =  $_SESSION['tmmadrasah_id']."/guru/".$id;		
											if (!is_dir('__statics/upload/'.$foldernama)) {
												mkdir('./__statics/upload/'.$foldernama, 0777, TRUE);

											}
							$folder   					= '__statics/upload/'.$foldernama;
							
							
		$this->db->set("folder",$folder);
		
		
		
	    $this->db->set("tgl_lahir",$this->Reff->formattanggaldb($_POST['tgl_lahir']));
		$this->db->set("password",encrypt((($_POST['password']))));
		$this->db->set("i_update",$_SESSION['nama_madrasah']);
		$this->db->set("d_update",date("Y-m-d H:i:s"));
		$this->db->where("id",$id);
		$this->db->update("tm_guru",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
}
