
<h3> Rincian Skor Ruang Belajar  <?php echo $data->nama; ?> </h3>
<table class="table table-hover table-bordered table-striped">

    <tr>
       <td>  Jumlah Siswa   </td>
       <td>  <?php echo $data->jml_siswa; ?> </td>
   </tr>

   <tr>
       <td>  Ruang Belajar yang dimiliki  </td>
       <td>  <?php echo $data->rombel; ?> </td>
   </tr>

   <tr>
       <td> Ruang Belajar Ideal  (Jumlah Siswa / kriteria ) </td>
       <td> <?php echo $data->rombel_ideal; ?> </td>
   </tr>

   <tr>
       <td> Persentase Ruang Belajar   (Ruang Belajar yang dimiliki/ Ruang Belajar Ideal x 100)  </td>
       <td>  <?php echo number_format($data->rombel_persen,1); ?> % </td>
   </tr>

  

   <tr style="font-weight:bold">
       <td> Total Skor Ruang Belajar   </td>
       <td>  <?php echo number_format($data->skor_rombel,1); ?> </td>
   </tr>
</table>


<br>
<h4>PERSENTASE PENILAIAN </h4> 
<table class="table table-bordered table-hover table-striped">
  <thead>
     <tr>
        <th>NO</th>
        <th> JUMLAH RUANG BELAJAR YANG DIMILIKI DIBANDING JUMLAH IDEAL</th>
        <th>SKOR </th>
    </tr>
    </thead>
      
															
															<tbody>
															  <?php 
																$umum = $this->db->get("kriteria_rombel")->result();
																$no=1;
															    foreach($umum as $rum){
																	?>
																   <tr>
																	<th scope="row"><?php echo $no++; ?></th>
																	<td>
																	<?php 
																	   if($rum->nilai2 <=100){
																		 echo $rum->nilai1."-".$rum->nilai2."%";
																	   }else{

																		echo "> 100 %";
																	   }
																	?>
																	</td>
																	<td><?php echo $rum->skor; ?></td>
																		
																  </tr>
																<?php 


																}
																?>
																
															</tbody>
</table>

<div class="alert alert-primary">
Jumlah Ideal Ruang Belajar
<ol type="circle">
<li> MI = Jumlah murid : 28 = X </li>

<li> MTs = Jumlah murid : 32 = X </li>

<li> MA/MAK = Jumlah murid : 36 = X </li>
</ol> 
</div>