
<h3> Rincian Skor EDM <?php echo $data->nama; ?> </h3>
<table class="table table-hover table-bordered table-striped">

   <tr>
       <td>  Aspek Kedisiplinan </td>
       <td>  <?php echo number_format($data->nilai_kedisiplinan,1); ?> </td>
   </tr>

   <tr>
       <td> Aspek Pengembangan Diri </td>
       <td>  <?php echo number_format($data->nilai_pengembangan_diri,1); ?> </td>
   </tr>

   <tr>
       <td> Aspek Pembelajaran  </td>
       <td>  <?php echo number_format($data->nilai_proses_pembelajaran,1); ?> </td>
   </tr>

   <tr>
       <td> Aspek Sarana Prasarana  </td>
       <td>  <?php echo number_format($data->nilai_sarana_prasarana,1); ?> </td>
   </tr>

   <tr>
       <td> Aspek Pembiayaan  </td>
       <td>  <?php echo number_format($data->nilai_pembiayaan,1); ?> </td>
   </tr>

   <tr style="font-weight:bold">
       <td> Total Skor EDM   </td>
       <td>  <?php echo number_format($data->skor_edm,1); ?> </td>
   </tr>
</table>