
<h3> Rincian Skor Program Indonesia Pintar (PIP)  <?php echo $data->nama; ?> </h3>
<table class="table table-hover table-bordered table-striped">

   <tr>
       <td>  Jumlah Siswa Terdata di EMIS  </td>
       <td>  <?php echo $data->jml_siswa; ?> </td>
   </tr>

   <tr>
       <td> Jumlah Siswa Penerima PIP  </td>
       <td> <?php echo $data->siswa_pip; ?> </td>
   </tr>

   <tr>
       <td> Persentase Penerima PIP  (Penerima PIP/Jumlah Siswa x 100)  </td>
       <td>  <?php echo number_format($data->persen_pip,1); ?> % </td>
   </tr>

  

   <tr style="font-weight:bold">
       <td> Total Skor PIP   </td>
       <td>  <?php echo number_format($data->skor_pip,1); ?> </td>
   </tr>
</table>


<br>
<h4>PERSENTASE PENILAIAN </h4> 
<table class="table table-bordered table-hover table-striped">
  <thead>
     <tr>
       <th>NO</th>
       <th>JUMLAH SISWA PENERIMA PIP DIBANDING JUMLAH TOTAL MURID </th>
        <th>SKOR </th>
    </tr>
    </thead>
      
															
															<tbody>
															  <?php 
																$umum = $this->db->get("kriteria_pip")->result();
																$no=1;
															    foreach($umum as $rum){
																	?>
																   <tr>
																	<th scope="row"><?php echo $no++; ?></th>
																	<td>
																	
																	 <?php echo ($rum->nilai1); ?>-<?php echo ($rum->nilai2); ?> %
																	
																	
																	</td>
																	<td><?php echo $rum->skor; ?></td>
																		
																  </tr>
																<?php 


																}
																?>
																
															</tbody>
</table>