
<h3> Rincian Skor Akhir <?php echo $data->nama; ?> </h3>
<table class="table table-hover table-bordered table-striped">

   <tr>
       <td> Skor Evaluasi Diri Madrasah (EDM)</td>
       <td>  <?php echo number_format($data->skor_edm,1); ?> </td>
   </tr>

   <tr>
       <td> Skor Program Indonesia Pintar(PIP) </td>
       <td>  <?php echo number_format($data->skor_pip,1); ?> </td>
   </tr>

   <tr>
       <td> Skor Ruang Belajar </td>
       <td>  <?php echo number_format($data->skor_rombel,1); ?> </td>
   </tr>

   <tr>
       <td> Skor Toilet </td>
       <td>  <?php echo number_format($data->skor_toilet,1); ?> </td>
   </tr>

   <tr>
       <td> Skor Madrasah Inklusi </td>
       <td>  <?php echo number_format($data->skor_inklusi,1); ?> </td>
   </tr>

   <tr style="font-weight:bold">
       <td> Total Skor  </td>
       <td>  <?php echo number_format($data->skor_akhir,1); ?> </td>
   </tr>
</table>