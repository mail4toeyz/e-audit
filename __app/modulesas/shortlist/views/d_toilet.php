
<h3> Rincian Skor Toilet   <?php echo $data->nama; ?> </h3>

<?php 

if($data->jenjang=="mi"){

  $lakimi = 60;
  $permi  = 50;
}else{

  $lakimi = 40;
  $permi  = 30;
}

?>
<table class="table table-hover table-bordered table-striped">

    <tr>
       <td>  Jumlah Siswa Laki (<?php echo $data->siswa_laki; ?> : <?php echo $lakimi; ?> = <?php echo round($data->siswa_laki/$lakimi); ?> )</td>
       <td>  <?php echo $data->siswa_laki; ?> </td>
   </tr>
   <tr>
       <td>  Jumlah Siswa Perempuan (<?php echo $data->siswa_perempuan; ?> : <?php echo $permi; ?> = <?php echo round($data->siswa_perempuan/$permi); ?> ) </td>
       <td>  <?php echo $data->siswa_perempuan; ?> </td>
   </tr>

    <tr>
       <td>  Jumlah Toilet yang dimiliki  </td>
       <td>  <?php echo $data->toilet_jumlah; ?> </td>
   </tr>

   <tr>
       <td>  Jumlah Toilet Ideal (Toilet Ideal laki + Toilet Ideal Perempuan)  </td>
       <td>  <?php echo $data->toilet_ideal; ?> </td>
   </tr>


   <tr>
       <td> Persentase Toilet  (Jumlah Toilet yang dimiliki / Toilet Ideal x 100 )  </td>
       <td>  <?php echo number_format($data->toilet_persen,1); ?> % </td>
   </tr>

  

   <tr style="font-weight:bold">
       <td> Total Skor Toilet    </td>
       <td>  <?php echo number_format($data->skor_toilet,1); ?> </td>
   </tr>
</table>


<br>
<h4>PERSENTASE PENILAIAN </h4> 
<table class="table table-bordered table-hover table-striped">
  <thead>
     <tr>
       <th>NO</th>
       <th> JUMLAH TOILET YANG DIMILIKI DIBANDING JUMLAH IDEAL</th>
        <th>SKOR </th>
    </tr>
    </thead>
      
															
															<tbody>
															  <?php 
																$umum = $this->db->get("kriteria_toilet")->result();
																$no=1;
															    foreach($umum as $rum){
																	?>
																   <tr>
																	<th scope="row"><?php echo $no++; ?></th>
																	<td>
																	<?php 
																	   if($rum->nilai2 <=100){
																		 echo $rum->nilai1."-".$rum->nilai2."%";
																	   }else{

																		echo "> 100 %";
																	   }
																	?>
																	</td>
																	<td><?php echo $rum->skor; ?></td>
																		
																  </tr>
																<?php 


																}
																?>
																
															</tbody>
</table>


<div class="alert alert-primary">
Jumlah Ideal Toilet Murid MI
<ol type="circle">
<li> Jumlah Toilet Murid Perempuan = Jumlah Murid perempuan : 50 = X </li>
<li> Jumlah Toilet Murid Laki-laki = Jumlah murid laki-laki : 60 = Y </li>
<li> Total/Jumlah Ideal Toilet Siswa = X + Y = Z </li>
</ol>
Jumlah Ideal Toilet Murid MTS/MA/MAK
<ol type="circle">
<li> Jumlah Toilet Murid Perempuan = Jumlah Murid perempuan : 30 = X </li>
<li> Jumlah Toilet Murid Laki-laki = Jumlah murid laki-laki : 40 = Y </li>
<li> Total/Jumlah Ideal Toilet Siswa = X + Y = Z </li>
</ol>

</div>