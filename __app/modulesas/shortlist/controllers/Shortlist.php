<?php
ini_set('memory_limit', '-1');
defined('BASEPATH') OR exit('No direct script access allowed');

class Shortlist extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 if(!$this->session->userdata("isKantor")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_guru','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view(''.$_SESSION['isKantor'].'/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Shortlist Madrasah ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		//  error_reporting(0);
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		  $arbantuan = array("1"=>"Kinerja","2"=>"Afirmasi","0"=>"Tidak dapat");
		  $arbadge = array("1"=>"primary","2"=>"success","0"=>"warning");
		  $statusArray = array("1"=>"Negeri","2"=>"Swasta");
		   $i= ($iDisplayStart +1);
		   $status= $_POST['status'];
		   
		   
		   foreach($datagrid as $val) {
			    
				$no = $i++;
			    if($status==1){
				
				 	//$this->db->query("update madrasahedm set rank='{$no}' where id='{$val['id']}'");

				}

				$simsarpras ="";
				if($val['simsarpras']==1){
				
					$simsarpras ="<br><small>Penerima Simsarpras</small>";

			   }
			   $statusMadrasah  = $this->db->query("select SUBSTR(nsm, 4, 1) as status from madrasahedm where  id='{$val['id']}'")->row();

				$records["data"][] = array(
					$no,
					// $val['rank_kinerja'],
					// $val['rank_afirmasi'],
					strtoupper($val['jenjang']),
					$val['nsm'],
					$val['nama'],
					$statusArray[$statusMadrasah->status],
					$val['akreditasi'],
					$this->Reff->get_kondisi(array("nsm"=>$val['nsm']),"tm_madrasah_master","alamat"),
					$val['kota'],
					$val['provinsi'],
					
					// $this->Reff->get_kondisi(array("nsm"=>$val['nsm']),"madrasah","telepon"),
					// $this->Reff->get_kondisi(array("nsm"=>$val['nsm']),"madrasah","email"),
					'<span class="badge badge-'.$arbadge[$val['bantuan']].'">'.$arbantuan[$val['bantuan']].'</span>',
					'<a href="javascript:void(0)" class="detailskor" konten="d_skorakhir" madrasah_id="'.$val['id'].'" data-toggle="modal" data-target="#defaultModalData">'.number_format($val['skor_akhir'],1).'</a>',
					'<a href="javascript:void(0)" class="detailskor" konten="d_edm" madrasah_id="'.$val['id'].'" data-toggle="modal" data-target="#defaultModalData">'.number_format($val['skor_edm'],1).'</a>',
					'<a href="javascript:void(0)" class="detailskor" konten="d_pip" madrasah_id="'.$val['id'].'" data-toggle="modal" data-target="#defaultModalData">'.number_format($val['skor_pip'],1).'</a>',
					'<a href="javascript:void(0)" class="detailskor" konten="d_ruang" madrasah_id="'.$val['id'].'" data-toggle="modal" data-target="#defaultModalData">'.number_format($val['skor_rombel'],1).'</a>',
					'<a href="javascript:void(0)" class="detailskor" konten="d_toilet" madrasah_id="'.$val['id'].'" data-toggle="modal" data-target="#defaultModalData">'.number_format($val['skor_toilet'],1).'</a>',
					number_format($val['skor_inklusi'],1),
					
					$val['jml_siswa'],
					$val['jml_guru'],
					$val['siswa_laki'],
					$val['siswa_perempuan'],
					$val['abk'],
					$val['rombel'],
					$val['rombel_ideal'],
					number_format($val['rombel_persen'],1),
					$val['toilet_jumlah'],
					$val['toilet_ideal'],
					number_format($val['toilet_persen'],1),

					$val['jml_guru'],
					$val['siswa_pip'],
					number_format($val['persen_pip'],1)." %",
					$val['skor_pip'],
					sprintf('%0.2f',$val['nilai_kedisiplinan']),
					sprintf('%0.2f',$val['nilai_pengembangan_diri']),
					sprintf('%0.2f',$val['nilai_proses_pembelajaran']),
					sprintf('%0.2f',$val['nilai_sarana_prasarana']),
					sprintf('%0.2f',$val['nilai_pembiayaan']),
					number_format($val['skor_edm'],2),
					number_format($val['nilai_skpm'],2),
					$this->Reff->formatuang2($val['rencana_pendapatan']),
					$this->Reff->formatuang2($val['total_rencana_kegiatan'])
					
					
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	public function form(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("tm_guru",array("id"=>$id));
			   
		   }
		$this->load->view("form",$data);
		
	}

	public function detailskor(){

		$madrasah_id  = $this->input->post("madrasah_id");
		$konten       = $this->input->post("konten");
		$data['data'] = $this->db->get_where("madrasahedm",array("id"=>$madrasah_id))->row();
		$this->load->view($konten,$data); 
	}
	
	
	
	 
}
