<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kegiatan extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 if(!$this->session->userdata("admin_id")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_guru','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('pusat/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "RENCANA PEMANFAATAN DANA BKBA ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		  error_reporting(0);
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		  $arbantuan = array("1"=>"Kinerja","2"=>"Afirmasi");
		  
		   $i= ($iDisplayStart +1);
		   $status= $_POST['status'];
		
		   foreach($datagrid as $val) {
			    
				$no = $i++;
				$visitasi ="";
				

			
				$records["data"][] = array(
					$no,	
					$val['nsm'],
					$this->Reff->get_kondisi(array("nsm"=>$val['nsm']),"madrasahedm","nama"),
					$val['tahun'],
					$val['jenis'],		
					$val['kode_sub_kegiatan'],
					(!empty($val['sub_kegiatan'])) ? $val['sub_kegiatan'] : $val['namakegiatan'],
					$val['volume'],
					$val['satuan'],
					$this->Reff->formatuang2($val['harga']),
					$this->Reff->formatuang2($val['biaya'])
					
				
					
					
					
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
		public function detail(){
			$id            = $this->input->get_post("id",true);
			
			$data['data']    = $this->db->get_where("madrasahedm",array("id"=>$id))->row();
			$cekAsesor       = $this->db->query("select asesor from visitasi_edm where madrasah_id='{$id}' limit 1")->row();

			$data['asesor']  = $this->db->get_where("asesor",array("id"=>$cekAsesor->asesor))->row();

			$ajax            = $this->input->get_post("ajax",true);	
				$data['title']   = "Dokumentasi Visitasi";
				
				if(!empty($ajax)){
								
					$this->load->view('detail',$data);
				
				}else{
					
					
					$data['konten'] = "detail";
					
					$this->_template($data);
				}


		}



		public function bukti(){

			$this->load->helper('exportpdf_helper'); 

			
			$data['madrasah'] = $this->db->query("select * from madrasahedm where nsm='{$_GET['nsm']}' and id  IN(select madrasah_id from visitasi_kegiatan)")->row();
			$data['data']     = $this->db->query("select * from madrasahedm where nsm='{$_GET['nsm']}' and id  IN(select madrasah_id from visitasi_kegiatan)")->row();
			$data['petugas'] = $this->db->get_where("asesor",array("nik"=>$_GET['nik']))->row();
			$data['aman'] = $this->db->get_where("visitasi_jadwal",array("nik"=>$_GET['nik'],"nsm"=>$_GET['nsm']))->row();
			$user_info = $this->load->view('bukti', $data, true);
			$pdf_filename = 'Hasil Visitasi'.$data['data']->nsm.'.pdf';	 
			
			 $output = $user_info;
			
			generate_pdf($output, $pdf_filename);
      }
	
	
	 
}
