<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdashboard extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		$this->load->model('Auth_model');
		if(!$this->session->userdata("tmprovinsi_id")){						
			$ajax = $this->input->get_post("ajax",true);		 
				if(!empty($ajax)){
			   
				   echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
					  exit();
				 }else{
					 redirect(site_url()."ksm/login");
				 }
			
		   }
		
	  }

	
	public function index(){
		
		
	   
		 
	   
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data = array();  
		  $data['title']  = "Dashboard Komite Provinsi ";
		
	    
        
	   
						
							

									
         $ajax = $this->input->get_post("ajax",true);	
		 
	     if(!empty($ajax)){
			    
			 $this->load->view('page_default',$data);
		 }else{
			 
			
		     $data['konten'] = "page_default";
			 
			 $this->load->view('pdashboard/page_header',$data);
		 }
	

		
	}
	
	
		
	public function statistik(){
		
		
	   
		 	if(!$this->session->userdata("tmprovinsi_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/loginkomite")."'>disini </a></center>";
               exit();
		  }
	   
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data = array();  
		  $data['title']  = "Dashboard Komite ";
		
	    
        
	    
							
								$grid = array();
								$data['categorie_xAxis'] = "";
								$data['json_anggota']    = "";
								$pie   					 = "";
								$data['title']           = "Persentase Jumlah Peserta Per Kabupaten Kota ";
							
								
								
								$bagian  = $this->Di->query("select * from kota where provinsi_id='".$_SESSION['tmprovinsi_id']."' order by nama asc")->result();
									
									 foreach($bagian as $index=>$row){
									   
									   
										$pm = $this->Di->query("select id from v_siswa where  kota_madrasah='".$row->id."'  ")->num_rows();
										
										$data['json_anggota'] .=",".$pm;
										$data["categorie_xAxis"] .=",'".$row->nama."";
										$tempo = array("INDEXES"=>$row->nama,"nama"=>$row->nama,"Jumlah"=>$pm,"rata"=>$pm/count($bagian));
										
										$pie          .=",['".$row->nama."',".(($pm/count($bagian))*100)."]";
										
										
									   $grid[] = $tempo;
									   
									 }
								
								
									 $data['statistik'] = " Nama  ";
									 $data['header']    = $data['statistik'];
									 $data['categorie_xAxis'] = " Nama   ";

									$data['json_pie_chart']  =  substr($pie,1);
									$data['json_anggota']    = substr($data['json_anggota'], 1);
									
									$data['grid'] = $grid;
									
									// Provinsi 
									
										$grid2 = array();
								$data['categorie_xAxis2'] = "";
								$data['json_anggota2']    = "";
								$pie2   					 = "";
								$data['title2']           = "Persentase Jumlah Peserta Per Provinsi ";
							
								
								
								$bagian  = $this->Di->query("select * from provinsi order by nama asc")->result();
									
									 foreach($bagian as $index=>$row){
									   
									   
										$pm = $this->Di->query("select id from v_siswa where  provinsi_madrasah='".$row->id."' ")->num_rows();
										
										$data['json_anggota2'] .=",".$pm;
										$data["categorie_xAxis2"] .=",'".$row->nama."";
										$tempo2 = array("INDEXES"=>$row->nama,"nama"=>$row->nama,"Jumlah"=>$pm,"rata"=>$pm/count($bagian));
										
										$pie2          .=",['".$row->nama."',".(($pm/count($bagian))*100)."]";
										
										
									   $grid2[] = $tempo2;
									   
									 }
								
								
									 $data['statistik'] = " Nama  ";
									 $data['header']    = $data['statistik'];
									 $data['categorie_xAxis2'] = " Nama   ";

									$data['json_pie_chart2']  =  substr($pie,1);
									$data['json_anggota2']    = substr($data['json_anggota2'], 1);
									
									$data['grid2'] = $grid2;

									
         $ajax = $this->input->get_post("ajax",true);	
		 
	     if(!empty($ajax)){
			    
			 $this->load->view('page_statistik',$data);
		 }else{
			 
			
		     $data['konten'] = "page_statistik";
			 
			 $this->load->view('pdashboard/page_header',$data);
		 }
	

		
	}

	public function monitorjumlah(){
		
		
	   
		if(!$this->session->userdata("tmprovinsi_id")){
	   echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
		  exit();
	 }
  
   
							   
	$ajax = $this->input->get_post("ajax",true);	
	 $data = array();  
	 $data['title']  = "Monitore Komite Provinsi ";
   
   
   
  
				   
					   

							   
	$ajax = $this->input->get_post("ajax",true);	
	
	if(!empty($ajax)){
		   
		$this->load->view('page_monitor',$data);
	}else{
		
	   
		$data['konten'] = "page_monitor";
		
		$this->load->view('pdashboard/page_header',$data);
	}


   
}
	

public function monitor(){
		
		
	   

						   
$ajax = $this->input->get_post("ajax",true);	
 $data = array();  
 $data['title']  = "Monitor KSM Provinsi ";




			   
				   

						   
$ajax = $this->input->get_post("ajax",true);	

if(!empty($ajax)){
	   
	$this->load->view('page_monitor',$data);
}else{
	
   
	$data['konten'] = "page_monitor";
	
	$this->load->view('pdashboard/page_header',$data);
}



}

	
}
