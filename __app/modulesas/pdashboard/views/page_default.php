
            <!-- Page Title Header Starts-->
            <div class="row page-title-header">
              <div class="col-12">
                <div class="page-header">
                  <h4 class="page-title">DASHBOARD</h4>
                  <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap">
                    <ul class="quick-links">
                      <li><a href="#">KOMPETISI SAINS MADRASAH</a></li>
                      <li><a href="#"><?php echo $_SESSION['nama']; ?></a></li>
                      
                    </ul>
                  
                  </div>
                </div>
              </div>
            
            </div>
            <!-- Page Title Header Ends-->
            <div class="row">
              <div class="col-md-12 ">
               
                    <div class="row">

                    <div class="col-lg-12 col-md-12">
                      <div class="card">
                       <div class="card-body">
                        <div class="">
                          <div class="wrapper">
                            <?php 
                              $peserta = $this->db->query("select count(id) as jml from v_siswa where provinsi_madrasah='{$_SESSION['tmprovinsi_id']}'")->row();
                            ?>
                            <h3 class="mb-0 font-weight-semibold"><i class="fa fa-users"></i> &nbsp; <?php echo $this->Di->formatuang2($peserta->jml); ?></h3>
                            <h5 class="mb-0 font-weight-medium text-primary"> Total Pendaftar </h5>
                            <small class="mb-0 text-muted"> Keseluruhan Pendaftar Jenjang MI/SD,MTs/SMP,MA/SMA </small>
                          </div>
                         
                        </div>
                      </div>
                      </div>
                      </div>

                     
                      <?php 
                        $jumlah=0;
                        $status = array("0"=>"Draft (Belum dikirim)","1"=>"Belum diverifikasi","2"=>"Verifikasi Lulus","3"=>"Tidak Lulus");
                        $statusket = array("0"=>"Peserta  belum lengkap dan dikirim oleh Lembaga","1"=>"Verifikasi dilakukan oleh Komite Kabupaten/kota","2"=>"Akan mengikuti KSM Tingkat Kabupaten/provinsi","3"=>"Tidak  akan mengikuti KSM Tingkat Kabupaten/provinsi");
                         foreach($status as $i=>$r){
                           $peserta = $this->db->query("select count(id) as jml from v_siswa where provinsi_madrasah='{$_SESSION['tmprovinsi_id']}' and status='{$i}'")->row();
                       
                           $jumlah  = $jumlah + $peserta->jml;
                       ?>
                      <div class="col-lg-3 col-md-6">
                      <div class="card">
                       <div class="card-body">
                        <div class="">
                          <div class="wrapper">
                            <h3 class="mb-0 font-weight-semibold"><i class="fa fa-copy"></i> &nbsp; <?php echo $this->Di->formatuang2($peserta->jml); ?></h3>
                            <h5 class="mb-0 font-weight-medium text-primary"> <?php echo $r; ?> </h5>
                            <small class="mb-0 text-muted"> <?php echo $statusket[$i]; ?> </small>
                          </div>
                         
                        </div>
                      </div>
                      </div>
                      </div>
                      <?php 
                         }
                      ?>

                        </div>
                       
                    
                    <div class="row">
                   <?php 
                    $jenjang = array("1"=>"MI","2"=>"MTs","3"=>"MA");
                     foreach($jenjang as $ijs=>$js){
                      $peserta = $this->db->query("select count(id) as jml from tm_madrasah where provinsi='{$_SESSION['tmprovinsi_id']}' and jenjang='{$ijs}' and jenis='1'")->row();
                     

                     ?>
                    <div class="col-lg-2 col-md-2">
                      <div class="card">
                       <div class="card-body">
                        <div class="">
                          <div class="wrapper">
                            <h4 class="mb-0 font-weight-semibold"><i class="fa fa-bank"></i> &nbsp; <?php echo $this->Di->formatuang2($peserta->jml); ?></h4>
                            <h5 class="mb-0 font-weight-medium text-primary"> <?php echo $js; ?> </h5>
                        
                          </div>
                         
                        </div>
                      </div>
                      </div>
                      </div>
                      <?php 
                     }
                     ?>

                  <?php 
                    $jenjang = array("1"=>"SD","2"=>"SMP","3"=>"SMA");
                     foreach($jenjang as $ijs=>$js){
                      $peserta = $this->db->query("select count(id) as jml from tm_madrasah where provinsi='{$_SESSION['tmprovinsi_id']}' and jenjang='{$ijs}' and jenis='2'")->row();
                     

                     ?>
                    <div class="col-lg-2 col-md-2">
                      <div class="card">
                       <div class="card-body">
                        <div class="">
                          <div class="wrapper">
                            <h4 class="mb-0 font-weight-semibold"><i class="fa fa-bank"></i> &nbsp; <?php echo $this->Di->formatuang2($peserta->jml); ?></h4>
                            <h5 class="mb-0 font-weight-medium text-primary"> <?php echo $js; ?> </h5>
                        
                          </div>
                         
                        </div>
                      </div>
                      </div>
                      </div>
                      <?php 
                     }
                     ?>



                      

                    </div>


                    
                      
                     

              </div>
            </div>



            <div class="row">
              <div class="col-md-8 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                  
                    
                    <div id="mapel"></div>
                  </div>
                </div>
              </div>

              <div class="col-md-4 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body d-flex flex-column">
                    <div class="">
                      <h4 class="card-title mb-0"><b><i class="fa fa-bell"></i> Pengumuman </b> </h4>
                      
                      
                    </div>
                    <be>
                    <div class="">
                      <br>

                         <ol type="1">
                          <li> Komite Provinsi bertugas untuk memonitoring proses verifikasi peserta yang dilakukan oleh Komite Kabupaten/Kota </li>
                          <li> Pastikan Seluruh Komite di Kabupaten/Kota Melakukan Verifikasi Peserta</li>
                          
                         

                        </ol>

                    </div>
                  </div>
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-md-12 ">
              <div class="card">
                       <div class="card-body">



                       

                <div class="table-responsive">

                <table class="table table-hover table-bordered table-striped">
                    <thead>
                       <tr> 
                         <th rowspan="2"> NO </th>
                         
                         <th rowspan="2"> JENJANG </th>
                         <th rowspan="2"> HARI </th>
                         <th rowspan="2"> SESI </th>
                         <th colspan="3"> <center>ZONA </center></th>                       
                         <th rowspan="2"> JUMLAH PESERTA </th>
                         <th rowspan="2"> MAPEL  </th>
                         <th rowspan="2"> KEBUTUHAN PENGAWAS </th>
                        </tr>

                        <tr>
                          <th> WIB </th>
                          <th> WITA </th>
                          <th> WIT </th>
                        </tr>
                      </thead>

                      <tbody>
                       <?php 
                         $sesi = $this->db->get_where("tm_sesi",array("tingkat"=>"provinsi"))->result();
                         $simulasi    = array("1"=>"Onschedule","2"=>"Onschedule","3"=>"Onschedule");
                         $pelaksanaan = array("1"=>"Onschedule","2"=>"Onschedule","3"=>"Onschedule");

                         $jml=0;
                         $jmlkeb=0;
                         
                         foreach($sesi as $rs){

                          $jml_peserta = $this->db->query("select count(id) as jml from v_siswa where hari2='{$rs->hari}' and sesi2='{$rs->sesi}' AND provinsi_madrasah='{$_SESSION['tmprovinsi_id']}'  ")->row();

                          $jml = $jml + $jml_peserta->jml;
                          $kebutuhan =  ceil($jml_peserta->jml/40);
                          

                          $mapel ="";
                          $kebutuhan=0;
                             $mex = explode(",",$rs->kompetisi);
                                foreach($mex as $rk){
                                  $jml_pesertamapel = $this->db->query("select count(id) as jml from v_siswa where hari2='{$rs->hari}' and sesi2='{$rs->sesi}' AND provinsi_madrasah='{$_SESSION['tmprovinsi_id']}' AND trkompetisi_id={$rk}  ")->row();
                                  $pengawasden  =  ceil($jml_pesertamapel->jml/40);
                                  $mapel      .="<br>".$this->Di->get_kondisi(array("id"=>$rk),"tr_kompetisi","nama")." (Pengawas : ".$pengawasden." )";
                                  $kebutuhan =  $kebutuhan+ $pengawasden;
                                  
                                }
                                $jmlkeb  = $jmlkeb +$kebutuhan;

                          ?>
                            <tr>
                               <td><?php echo $rs->id; ?></td>
                               
                               <td><?php echo $rs->keterangan; ?>    </td>
                              <td> Onschedule </td>
                              <td> Onschedule </td>
                              <td> Onschedule </td>
                              <td> Onschedule </td>
                              <td> Onschedule </td>
                               <td><?php echo $this->Di->formatuang2($jml_peserta->jml); ?></td>
                               <td> <?php echo $mapel; ?> </td>
                               <td> <?php echo $kebutuhan; ?> </td>

                            </tr>




                          <?php 


                         }
                        ?>

                            <tr>
                               <td colspan="8">Jumlah </td>
                              
                               <td><?php echo $this->Di->formatuang2($jml); ?></td>
                               <td><?php echo $this->Di->formatuang2($jmlkeb); ?></td>

                            </tr>


                        


                      </tbody>

                  </table>







                </div>
                </div>
                </div>
                </div>
                </div>


          
          

             


<?php 
   
   $kompetisi = $this->db->get("tr_kompetisi")->result();
      $data["categories"] ="";
        foreach($kompetisi as $rkom){
          
            $categories = ",";
            $data["categories"] .=",'".$rkom->nama." (".$this->Di->jenjangnama($rkom->tmmadrasah_id).")'";
          
        }
  

    $categories = substr($data["categories"],1);

?>
              
<script type="text/javascript">
Highcharts.chart('mapel', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Jenjang Pendidikan '
    },
    subtitle: {
        text: 'Pendaftar KSM Perbidang studi '
    },
    xAxis: {
        categories: [<?php echo $categories; ?>],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah ',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' Peserta'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: 0,
       
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [
      <?php 
     
      $status = array("0"=>"Draft (Belum dikirim)","1"=>"Belum diverifikasi","2"=>"Verifikasi Lulus","3"=>"Tidak Lulus");
      $data["status"] ="";
       foreach($status as $is=>$rs){

        $kompetisi = $this->db->get("tr_kompetisi")->result();
        $data["datajumlah"] ="";
          foreach($kompetisi as $rkom){
             $jumlah = $this->db->query("select count(id) as jml from tm_siswa where trkompetisi_id='{$rkom->id}' and status='{$is}' and provinsi='{$_SESSION['tmprovinsi_id']}'")->row();

              $categories = ",";
              $data["datajumlah"] .=",".$jumlah->jml."";
            
          }
          $datajumlah = substr($data["datajumlah"],1);


         ?>
         
        {
          name: '<?php echo $rs; ?>',
          data: [<?php echo $datajumlah; ?>]
        },
     <?php 
       }
       ?>
      
      
      ]
});
</script>

<!--
<div id="pengumumanModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

   
    <div class="modal-content">
     
      <div class="modal-body">
      <h4 style="font-weight:bold">Pengumuman</h4>

      <div class="alert alert-warning">
          <ol>

        
          <li> Sertifikat Official KSM Tingkat Nasional sudah dapat diunduh   pada menu  <b> Pendaftaran Panitia </b></li>
          <li> Sertifikat Juara dan Peserta KSM Nasional sudah dapat diunduh pada  menu  <b> Pemenang Nasional </b></li>
         
         
         
           </ol>

          
           
      </div>
     
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
    // $("#pengumumanModal").modal("toggle");

</script>

-->
              