

<script type="text/javascript">
      
$(function () {

Highcharts.chart('grafik2', {
     title: {
        text: 'GRAFIK PESERTA  DIKABUPATEN/KOTA'
    },
    subtitle: {
        text: ' KOMPETISI SAINS MADRASAH '
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '12px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
	 legend: {
        enabled: false
    },
    labels: {
        items: [{
            html: 'Statistik Peserta ',
            style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
            }
        }]
    },
	 
    series: [
	
	 <?php 
         $htmls ="";
         $rata ="";
         foreach ($grid as $key){
         $varnya = $key['nama'];
         $jumlah = $key['Jumlah'];
         
          $rata .= ",".$key['rata'];
           $htmls .=",
            
            ['$varnya',$jumlah]
            ";

       
         
         }

         
        ?>
	
	{
        type: 'column',
        name:'Peserta',
		 upColor: Highcharts.getOptions().colors[2],
        color: Highcharts.getOptions().colors[3],
        data: [
		
		 <?php 
         
         echo substr($htmls,1);
        ?>
		
		],
		 tooltip: {
        pointFormat: 'Jumlah : <b>{point.y} Peserta</b>'
       },
		    dataLabels: {
             enabled: true,
            rotation: 0,
            verticalAlign: 'top',
            crop: false,
            overflow: 'none',
            x: 0,
            y: -30,
            style: {
                fontWeight: 'bold'
            }
        }
    },
	
	
	
	
	{
        type: 'spline',
        name: 'Rata Rata',
        data: [

		 <?php 
         
         echo substr($rata,1);
        ?>
		
		],
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
        }
    }, 
	
	{
            type: 'pie',
            name: 'Pesentase ',
            data: [
               <?php echo $json_pie_chart; ?>
               
                
                
                
            ],
			   center: [100, 80],
        size: 100,
		 tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}: </span>{point.percentage:.1f}%<br/>',
         },
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
        }
	]
});



Highcharts.chart('grafik', {
     title: {
        text: 'GRAFIK PESERTA PER PROVINSI  '
    },
    subtitle: {
        text: ' KOMPETISI SAINS MADRASAH '
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '12px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
	 legend: {
        enabled: false
    },
    labels: {
        items: [{
            html: 'Statistik Peserta ',
            style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
            }
        }]
    },
	 
    series: [
	
	 <?php 
         $htmls ="";
         $rata ="";
         foreach ($grid2 as $key){
         $varnya = $key['nama'];
         $jumlah = $key['Jumlah'];
         
          $rata .= ",".$key['rata'];
           $htmls .=",
            
            ['$varnya',$jumlah]
            ";

       
         
         }

         
        ?>
	
	{
        type: 'column',
        name:'Peserta',
		 upColor: Highcharts.getOptions().colors[2],
        color: Highcharts.getOptions().colors[3],
        data: [
		
		 <?php 
         
         echo substr($htmls,1);
        ?>
		
		],
		 tooltip: {
        pointFormat: 'Jumlah : <b>{point.y} Peserta</b>'
       },
		    dataLabels: {
             enabled: true,
            rotation: 0,
            verticalAlign: 'top',
            crop: false,
            overflow: 'none',
            x: 0,
            y: -30,
            style: {
                fontWeight: 'bold'
            }
        }
    },
	
	
	
	
	{
        type: 'spline',
        name: 'Rata Rata',
        data: [

		 <?php 
         
         echo substr($rata,1);
        ?>
		
		],
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
        }
    }, 
	
	{
            type: 'pie',
            name: 'Pesentase ',
            data: [
               <?php echo $json_pie_chart2; ?>
               
                
                
                
            ],
			   center: [100, 80],
        size: 100,
		 tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}: </span>{point.percentage:.1f}%<br/>',
         },
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
        }
	]
});

})
</script>
 
<div id="grafik" ></div>
<div id="grafik2"></div>

				
