 <ul class="list-unstyled" style="color:black;font-weight:bold">
                <li class="active">
				<a  class="menuklik" href="<?php echo site_url("pdashboard"); ?>" title="Dashboard "><i class="fa fa-home"></i>Dashboard  </a></li>
                <li class=""> <a class="menuklik" href="<?php echo site_url("plokal/panitia"); ?>" title="Panitia Pelaksana "><i class="fa fa-user"></i>Pendaftaran Panitia  </a></li>
              	
			   
			    <li class=""> <a class="menuklik" href="<?php echo site_url("plokal/madrasah"); ?>" title="Data Madrasah "><i class="fa fa-institution"></i>Data Lembaga  </a></li>
                <li class=""> <a class="menuklik" href="<?php echo site_url("plokal/lokasi"); ?>" title="Lokasi Tes "><i class="fa fa-map-marker"></i>Lokasi Tes </a></li> 
                <li><a href="#komitekabko" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-users"></i>Komite KabKota  </a>
                    <ul id="komitekabko" class="collapse list-unstyled">
                        <li><a class="menuklik" href="<?php echo site_url("plokal/kabkota"); ?>" title="Akun Komite Kabko">  Akun Komite Kabko</a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("plokal/komite"); ?>" title="Komite Kabkota">  Data Komite </a></li> 
                        
 
                    </ul>
                </li> 
                <li><a href="#approval" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-laptop"></i>Laporan dan Statistik  </a>
                    <ul id="approval" class="collapse list-unstyled">
                        <li><a class="" href="<?php echo site_url("plokal/laporan"); ?>" title="Rekap Verifikasi">  Rekap Verifikasi </a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("pdashboard/statistik"); ?>" title="Grafik">  Grafik Pendaftar </a></li> 
                     
 
                    </ul>
                </li> 

				<li><a href="#verifikasipeserta" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-check-circle"></i>Verifikasi Peserta KSM  </a>
                    <ul id="verifikasipeserta" class="collapse list-unstyled">
                        <li><a class="menuklik" href="<?php echo site_url("plokal/peserta?jenjang=".base64_encode(1).""); ?>" title="Verifikasi Peserta MI/SD">  Peserta MI/SD</a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("plokal/peserta?jenjang=".base64_encode(2).""); ?>" title="Verifikasi Peserta MTS/SMP">  Peserta MTs/SMP</a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("plokal/peserta?jenjang=".base64_encode(3).""); ?>" title="Verifikasi Peserta MA/SMA">  Peserta MA/SMA</a></li> 
 
                    </ul>
                </li> 
                <li><a href="#pesertaksm" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-users"></i> Peserta KSM Kabkota  </a>
                    <ul id="pesertaksm" class="collapse list-unstyled">
                        <li><a class="menuklik" href="<?php echo site_url("plokal/peserta?jenjang=".base64_encode(1)."&status=2"); ?>" title=" Peserta MI/SD">Peserta MI/SD</a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("plokal/peserta?jenjang=".base64_encode(2)."&status=2"); ?>" title="Data Peserta MTS/SMP"> Peserta MTs/SMP</a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("plokal/peserta?jenjang=".base64_encode(3)."&status=2"); ?>" title="Data Peserta MA/SMA">Peserta MA/SMA</a></li> 
 
                    </ul>
                </li> 

                <li class="active"><a href="#pelaksanaanKSM" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-laptop"></i> KSM  Kabkota </a>
                    <ul id="pelaksanaanKSM" class="collapse list-unstyled">
                          <?php 
                           $kota = $this->db->query("SELECT * from kota where provinsi_id='".$_SESSION['tmprovinsi_id']."' order by nama asc")->result();
                             foreach($kota as $rkot){
                           ?>
                       
                             <li><a class="menuklik" href="<?php echo site_url("pelaksanaankabko?kota=".base64_encode($rkot->id).""); ?>" title="<?php echo $rkot->nama; ?>"> <?php echo strtolower($rkot->nama); ?> </a></li> 
                          <?php 
                             }
                         ?>
                       
                       
 
                    </ul>
                </li> 

                <li><a href="#monitoirkabkota" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-line-chart"></i> Monitoring  CBT  </a>
                    <ul id="monitoirkabkota" class="collapse list-unstyled">
                        <li><a class="menuklik" href="<?php echo site_url("pdashboard/monitorjumlah"); ?>" title=" Peserta MI/SD">Monitor Jumlah </a></li> 
                       <!-- <li><a class="menuklik" href="<?php echo site_url("plokal/monitorpeserta"); ?>" title="Data Peserta MTS/SMP"> Monitor Peserta </a></li>  -->
                   </ul>
                </li> 
			
				
			
				 
                 <li><a href="#htest" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-copy"></i>Hasil CBT Kabkota </a>
                    <ul id="htest" class="collapse list-unstyled">
                        <li><a class="menuklik" href="<?php echo site_url("plokal/hasiltes?jenjang=".base64_encode(1).""); ?>" title="Hasil MI/SD"> Hasil CBT MI/SD</a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("plokal/hasiltes?jenjang=".base64_encode(2).""); ?>" title="Hasil MTS/SMP"> Hasil CBT  MTs/SMP</a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("plokal/hasiltes?jenjang=".base64_encode(3).""); ?>" title="Hasil MA/SMA"> Hasil CBT MA/SMA </a></li> 
 
                    </ul>
                </li> 
             
                <li><a href="#pesertatesprov" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-users"></i>Peserta KSM Provinsi </a>
                    <ul id="pesertatesprov" class="collapse list-unstyled">
                        <li><a class="menuklik" href="<?php echo site_url("plokal/peserta_provinsi?jenjang=".base64_encode(1).""); ?>" title="Peserta MI/SD"> Peserta MI/SD</a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("plokal/peserta_provinsi?jenjang=".base64_encode(2).""); ?>" title="Peserta MTS/SMP"> Peserta     MTS/SMP</a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("plokal/peserta_provinsi?jenjang=".base64_encode(3).""); ?>" title="Peserta MA/SMA"> Peserta   MA/SMA</a></li> 
 
                    </ul>
                </li>

                <li class="active"><a href="#pelaksanaanKSMProvinsi" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-laptop"></i> KSM Provinsi  </a>
                    <ul id="pelaksanaanKSMProvinsi" class="collapse list-unstyled">
                       
                        <li><a class="menuklik" href="<?php echo site_url("pelaksanaanprovinsi"); ?>" title="Verifikasi Peserta MI/SD">  Peserta KSM Provinsi</a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("plokal/jadwal"); ?>" title="Jadwal Kompetisi Sains Madrasah ">   Jadwal KSM Provinsi</a></li>
                        <li><a class="menuklik" href="<?php echo site_url("pdashboard/monitor"); ?>" title="Monitor CBT ">  Monitor CBT </a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("pelaksanaanprovinsi/ruang"); ?>" title="Akun Ruang CBT ">   Ruang Pengawasan CBT </a></li> 
                       
                       
 
                    </ul>
                </li> 


                <li><a href="#htestprov" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-copy"></i>Hasil KSM Provinsi </a>
                    <ul id="htestprov" class="collapse list-unstyled">
                        <li><a class="menuklik" href="<?php echo site_url("plokal/hasiltesprov?jenjang=".base64_encode(1).""); ?>" title="Hasil MI/SD"> Hasil KSM MI/SD</a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("plokal/hasiltesprov?jenjang=".base64_encode(2).""); ?>" title="Hasil MTS/SMP"> Hasil KSM  MTs/SMP</a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("plokal/hasiltesprov?jenjang=".base64_encode(3).""); ?>" title="Hasil MA/SMA"> Hasil KSM MA/SMA </a></li> 
 
                    </ul>
                </li> 


            <!-- 
                <li><a href="#pesertaksmkabko" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-users"></i> Ruang  KSM Provinsi  </a>
                    <ul id="pesertaksmkabko" class="collapse list-unstyled">
                      <?php 
                        $kompetisi = $this->db->get("tr_kompetisi")->result();
                           foreach($kompetisi as $rkom){

                            ?><li><a class="menuklik" href="<?php echo site_url("plokal/pesertaruang?kompetisi=".$rkom->id.""); ?>" title="<?php echo $rkom->nama; ?>"><?php echo $rkom->tmmadrasah_id; ?>-<?php echo $rkom->nama; ?></a></li> <?php 
                 
                           }
                      ?>
                              
 
                    </ul>
                </li> 

                        -->


               
                <li><a href="#pesertatesnas" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-file"></i>KSM Tingkat Nasional </a>
                    <ul id="pesertatesnas" class=" collapse list-unstyled">
                       <!-- <li><a class="menuklik" href="<?php echo site_url("plokal/fakta"); ?>" title="Upload Dokumen"> Upload Dokumen </a></li>  -->
                        <li><a class="menuklik" href="<?php echo site_url("plokal/peserta_nasional?jenjang=".base64_encode(1).""); ?>" title="Peserta MI/SD"> Peserta   MI/SD</a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("plokal/peserta_nasional?jenjang=".base64_encode(2).""); ?>" title="Peserta MTS/SMP"> Peserta    MTS/SMP</a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("plokal/peserta_nasional?jenjang=".base64_encode(3).""); ?>" title="Peserta MA/SMA"> Peserta   MA/SMA</a></li> 
 
                    </ul>
                </li>

                <li><a href="#pemenang" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-copy"></i>Pemenang  Nasional </a>
                    <ul id="pemenang" class=" list-unstyled">
                        <li><a class="" href="<?php echo site_url("plokal/pemenang?jenjang=".base64_encode(1).""); ?>" title="Peserta MI/SD"> Pemenang   MI/SD</a></li> 
                        <li><a class="" href="<?php echo site_url("plokal/pemenang?jenjang=".base64_encode(2).""); ?>" title="Peserta MTS/SMP"> Pemenang    MTS/SMP</a></li> 
                        <li><a class="" href="<?php echo site_url("plokal/pemenang?jenjang=".base64_encode(3).""); ?>" title="Peserta MA/SMA"> Pemenang   MA/SMA</a></li> 
                       <!--  <li><a class="menuklik" href="<?php echo site_url("plokal/tabulasiMedaliProvinsi"); ?>" title="Provinsi Juara Umum"> Provinsi Juara Umum</a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("plokal/peringkatmadrasahnas"); ?>" title="Peringkat Madrasah Juara Umum"> Peringkat Madrasah</a></li>  -->
 
                    </ul>
                </li>
				
                
              
				
				
	        
				
               
				
				
              
				
			
				<li class=""> <a  class="menuklik" href="<?php echo site_url("plokal/perbaharui_p"); ?>" title="Profile"><i class="fa fa-edit "></i>Perbaharui Akun</a></li>
                <li > 	<a href="javascript:void(0)" id="keluar"><i class="fa fa-unlock-alt"></i>Logout Aplikasi </a></li>
               
</ul>

				
					<script type="text/javascript">	
						 $(document).on("click","#keluar",function(){
					  
					      var base_url = "<?php echo base_url(); ?>ksm/login";
					  
					      alertify.confirm("Anda akan keluar dari aplikasi, apakah anda yakin?",function(){
						  
							   $.post("<?php echo site_url("ksm/logout"); ?>",function(data){
								   
								   location.href= base_url;
								   
							   });
						 });
	  
	  
	  
                         });
		</script>
		