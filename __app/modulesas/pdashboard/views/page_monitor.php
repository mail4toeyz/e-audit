<div class="row">
              <div class="col-md-12 ">
              <div class="row">

              <?php 
                        $jumlah=0;
                        $simulasi    = array("1"=>"Rabu, 5 Juli 2023","2"=>"Kamis, 6 Juli 2023","3"=>"Jumat, 7 Juli 2023");
                        $pelaksanaan = array("1"=>"Sabtu, 8 Juli 2023","2"=>"Minggu, 9 Juli 2023","3"=>"Senin, 10 Juli 2023");
                        $kompetisi = $this->db->query("select * from tm_sesi where tingkat='kabko' and id IN(select sesi_kabko from v_siswa where status=2 and provinsi_madrasah='".$_SESSION['tmprovinsi_id']."')   order by hari ASC, sesi ASC")->result();
                        $cbt = $this->load->database("cbt",true);
                        foreach($kompetisi as $i=>$r){
                          
                           $peserta = $this->db->query("select count(id) as jml from v_siswa where  sesi_kabko='".$r->id."' and status='2' AND provinsi_madrasah='{$_SESSION['tmprovinsi_id']}'")->row();
                           $sdgmengerjakan = $cbt->query("select count(id) as jml from d_psrt where  sesi='".$r->id."' AND SUBSTR(idPtk,1,2)='".$_SESSION['tmprovinsi_id']."'  and id IN(select idPs  FROM t_pkujps where sta = 1 and strt is not null and idUj <14)")->row();
                           $selesaimengerjakan = $cbt->query("select count(id) as jml from d_psrt where sesi='".$r->id."' and SUBSTR(idPtk,1,2)='".$_SESSION['tmprovinsi_id']."'  and id IN(select idPs  FROM t_pkujps where sta = 2 and strt is not null and idUj <14)")->row();
                       
                           $jumlah  = $jumlah + $peserta->jml;

                           $blmikut = $peserta->jml-$sdgmengerjakan->jml-$selesaimengerjakan->jml;

                           $sudahikut = $sdgmengerjakan->jml+$selesaimengerjakan->jml;
                           $persentase = ($sudahikut/$peserta->jml) * 100;
                           $persentasesdg = number_format(($sdgmengerjakan->jml/$peserta->jml) * 100,2);
                           $persentaseselesai = number_format(($selesaimengerjakan->jml/$peserta->jml) * 100,2);
                           $persentaseblm = number_format(($blmikut/$peserta->jml) * 100,2);

                       ?>
                      <div class="col-lg-4 col-md-6 col-sm-6">

                            
                                    <div class="card">
                                    <div class="card-body">
                                        <figure class="highcharts-figure">
                                            <div id="GrafikDataSesiHari<?php echo $r->id; ?>"></div>
                                            <p class="highcharts-description">
                                            <div class="wrapper">
                                                <h4 class="mb-0 font-weight-semibold"><i class="fa fa-users"></i> <?php echo $this->Di->formatuang2($peserta->jml); ?> Jumlah Peserta</h4>
                                                <h4 class="mb-0 font-weight-semibold"><i class="fa fa-bar-chart"></i> <?php echo number_format($persentase,2); ?>% Tingkat  Partisipasi </h4>
                                               
                                            </div>
                                            </p>
                                        </figure>
                                    </div>
                                    </div>
                      </div>


                        <script>
                      




                       //  Build the chart
                        Highcharts.chart('GrafikDataSesiHari<?php echo $r->id; ?>', {
                            chart: {
                                plotBackgroundColor: null,
                                plotBorderWidth: null,
                                plotShadow: false,
                                type: 'pie'
                            },
                            title: {
                                text: '<?php echo $r->keterangan; ?>',
                                align: 'left'
                            },
                            subtitle: {
                                text: '<?php echo $pelaksanaan[$r->hari]; ?> - Sesi <?php echo $r->sesi; ?> -  <?php echo (empty($r->wita)) ? $r->wib : $r->wita; ?> WITA',
                                align: 'left'
                            },
                            tooltip: {
                                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                            },
                            accessibility: {
                                point: {
                                    valueSuffix: '%'
                                }
                            },
                            plotOptions: {
                                pie: {
                                  colors: [
                                  '#9e9695', 
                                  '#fa2a12', 
                                  '#3c93bf'
                                 
                                ],
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: true,
                                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                        connectorColor: 'silver'
                                    }
                                }
                            },
                            series: [{
                                name: 'Peserta ',
                                data: [
                                    { name: '<?php echo $this->Di->formatuang2($blmikut); ?> Belum Mengerjakan', y: <?php echo $persentaseblm; ?> },
                                    { name: '<?php echo $this->Di->formatuang2($sdgmengerjakan->jml); ?> Sedang Mengerjakan', y: <?php echo $persentasesdg; ?> },
                                    { name: '<?php echo $this->Di->formatuang2($selesaimengerjakan->jml); ?> Selesai', y: <?php echo $persentaseselesai; ?> }
                                ]
                            }]
                        });

                        </script>

                      <?php 
                         }
                      ?>
                </div>

                <div class="row">


                
                    <?php 
                      
                      $jumlah=0;
                        
                       
                       
                        $pel = array("Partisipasi KSM Keseluruhan ");
                        $cbt = $this->load->database("cbt",true);
                       
                           $peserta = $this->db->query("select count(id) as jml from v_siswa where status='2' AND provinsi_madrasah='{$_SESSION['tmprovinsi_id']}'")->row();
                           $sdgmengerjakan = $cbt->query("select count(id) as jml from d_psrt where  SUBSTR(idPtk,1,2)='".$_SESSION['tmprovinsi_id']."' AND id IN(select idPs  FROM t_pkujps where sta = 1 and strt is not null and idUj <14)")->row();
                           $selesaimengerjakan = $cbt->query("select count(id) as jml from d_psrt where  SUBSTR(idPtk,1,2)='".$_SESSION['tmprovinsi_id']."' AND id IN(select idPs  FROM t_pkujps where sta = 2 and strt is not null and idUj <14)")->row();
                       
                           $jumlah  = $jumlah + $peserta->jml;

                           $blmikut = $peserta->jml-$sdgmengerjakan->jml-$selesaimengerjakan->jml;

                           $sudahikut = $sdgmengerjakan->jml+$selesaimengerjakan->jml;
                           $persentase = ($sudahikut/$peserta->jml) * 100;
                           $persentasesdg = number_format(($sdgmengerjakan->jml/$peserta->jml) * 100,2);
                           $persentaseselesai = number_format(($selesaimengerjakan->jml/$peserta->jml) * 100,2);
                           $persentaseblm = number_format(($blmikut/$peserta->jml) * 100,2);

                       ?>
                      <div class="col-lg-12 col-md-12 col-sm-12">

                          
                                    <div class="card">
                                    <div class="card-body">
                                        <figure class="highcharts-figure">
                                            <div id="GrafikDataSesiHariPerTotal"></div>
                                            <p class="highcharts-description">
                                            <div class="wrapper">
                                                <h4 class="mb-0 font-weight-semibold"><i class="fa fa-users"></i> <?php echo $this->Di->formatuang2($peserta->jml); ?> Jumlah Peserta</h4>
                                                <h4 class="mb-0 font-weight-semibold"><i class="fa fa-bar-chart"></i> <?php echo number_format($persentase,2); ?>% Tingkat  Partisipasi </h4>
                                               
                                            </div>
                                            </p>
                                        </figure>
                                    </div>
                                    </div>
                      </div>


                      <script>

                      //  Build the chart
                       Highcharts.chart('GrafikDataSesiHariPerTotal', {
                           chart: {
                               plotBackgroundColor: null,
                               plotBorderWidth: null,
                               plotShadow: false,
                               type: 'pie'
                           },
                           title: {
                               text: 'Keseluruhan Partisipasi KSM',
                               align: 'left'
                           },
                          
                           tooltip: {
                               pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                           },
                           accessibility: {
                               point: {
                                   valueSuffix: '%'
                               }
                           },
                           plotOptions: {
                               pie: {
                                colors: [
                                  '#9e9695', 
                                  '#fa2a12', 
                                  '#3c93bf'
                                 
                                ],
                                   allowPointSelect: true,
                                   cursor: 'pointer',
                                   dataLabels: {
                                       enabled: true,
                                       format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                       connectorColor: 'silver'
                                   }
                               }
                           },
                           series: [{
                               name: 'Peserta ',
                               data: [
                                   { name: '<?php echo $this->Di->formatuang2($blmikut); ?> Belum Mengerjakan', y: <?php echo $persentaseblm; ?> },
                                   { name: '<?php echo $this->Di->formatuang2($sdgmengerjakan->jml); ?> Sedang Mengerjakan', y: <?php echo $persentasesdg; ?> },
                                   { name: '<?php echo $this->Di->formatuang2($selesaimengerjakan->jml); ?> Selesai', y: <?php echo $persentaseselesai; ?> }
                               ]
                           }]
                       });

                       </script>

                       <?php 
                        
                        ?>

                </div>


                <div class="row">
                        <div class=" page-title-header">
                            <div class="col-12">
                                <div class="page-header">
                                <h4 class="page-title">PARTISIPASI  PERHARI  </h4>
                            
                                </div>
                            </div>
                            
                            </div>

                </div>


                <div class="row">


                <?php 
                        $jumlah=0;
                        
                       
                        $kompetisi = $this->db->query("select * from tm_sesi where tingkat='kabko'  order by hari ASC, sesi ASC")->result();
                        $pelaksanaan = array("1"=>"Sabtu, 8 Juli 2023","2"=>"Minggu, 9 Juli 2023","3"=>"Senin, 10 Juli 2023");
                        $pel = array("1"=>"6,7,8,9,10,11","2"=>"3,4,5","3"=>"1,2");
                        $cbt = $this->load->database("cbt",true);
                        foreach($pel as $i=>$p){
                           $peserta = $this->db->query("select count(id) as jml from v_siswa where  trkompetisi_id IN(".$p.") and status='2' AND provinsi_madrasah='{$_SESSION['tmprovinsi_id']}'")->row();
                           $sdgmengerjakan = $cbt->query("select count(id) as jml from d_psrt where idJlr IN(".$p.")  AND SUBSTR(idPtk,1,2)='".$_SESSION['tmprovinsi_id']."' and id IN(select idPs  FROM t_pkujps where sta = 1 and strt is not null and idUj <14)")->row();
                           $selesaimengerjakan = $cbt->query("select count(id) as jml from d_psrt where idJlr IN(".$p.") AND SUBSTR(idPtk,1,2)='".$_SESSION['tmprovinsi_id']."'  and id IN(select idPs  FROM t_pkujps where sta = 2 and strt is not null and idUj <14)")->row();
                       
                           $jumlah  = $jumlah + $peserta->jml;

                           $blmikut = $peserta->jml-$sdgmengerjakan->jml-$selesaimengerjakan->jml;

                           $sudahikut = $sdgmengerjakan->jml+$selesaimengerjakan->jml;
                           $persentase = ($sudahikut/$peserta->jml) * 100;
                           $persentasesdg = number_format(($sdgmengerjakan->jml/$peserta->jml) * 100,2);
                           $persentaseselesai = number_format(($selesaimengerjakan->jml/$peserta->jml) * 100,2);
                           $persentaseblm = number_format(($blmikut/$peserta->jml) * 100,2);

                       ?>
                      <div class="col-lg-4 col-md-12 col-sm-12">

                            
                                    <div class="card">
                                    <div class="card-body">
                                        <figure class="highcharts-figure">
                                            <div id="GrafikDataSesiHariPer<?php echo $i; ?>"></div>
                                            <p class="highcharts-description">
                                            <div class="wrapper">
                                                <h4 class="mb-0 font-weight-semibold"><i class="fa fa-users"></i> <?php echo $this->Di->formatuang2($peserta->jml); ?> Jumlah Peserta</h4>
                                                <h4 class="mb-0 font-weight-semibold"><i class="fa fa-bar-chart"></i> <?php echo number_format($persentase,2); ?>% Tingkat  Partisipasi </h4>
                                               
                                            </div>
                                            </p>
                                        </figure>
                                    </div>
                                    </div>
                      </div>


                      


                        <script>
                      




                       //  Build the chart
                        Highcharts.chart('GrafikDataSesiHariPer<?php echo $i; ?>', {
                            chart: {
                                plotBackgroundColor: null,
                                plotBorderWidth: null,
                                plotShadow: false,
                                type: 'pie'
                            },
                            title: {
                                text: '<?php echo $pelaksanaan[$i]; ?>',
                                align: 'left'
                            },
                           
                            tooltip: {
                                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                            },
                            accessibility: {
                                point: {
                                    valueSuffix: '%'
                                }
                            },
                            plotOptions: {
                                pie: {
                                  colors: [
                                  '#9e9695', 
                                  '#fa2a12', 
                                  '#3c93bf'
                                 
                                ],
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: true,
                                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                        connectorColor: 'silver'
                                    }
                                }
                            },
                            series: [{
                                name: 'Peserta ',
                                data: [
                                    { name: '<?php echo $this->Di->formatuang2($blmikut); ?> Belum Mengerjakan', y: <?php echo $persentaseblm; ?> },
                                    { name: '<?php echo $this->Di->formatuang2($sdgmengerjakan->jml); ?> Sedang Mengerjakan', y: <?php echo $persentasesdg; ?> },
                                    { name: '<?php echo $this->Di->formatuang2($selesaimengerjakan->jml); ?> Selesai', y: <?php echo $persentaseselesai; ?> }
                                ]
                            }]
                        });

                        </script>

                      <?php 
                         }
                      ?>

                  
                </div>
                
                
               
               </div>
</div>
                

<script type="text/javascript">
    setTimeout(function () { 
      location.reload();
    }, 60 * 1000);
</script>


           


          

             

