<?php

class M_referensi extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
    }

    
	public function grid($paging){
      
		
	    $keyword      = $this->input->post("keyword",true);
	  
	 
	 
		$this->db->select("*");
        $this->db->from('sbm_hotel');
	
        
	  
	    if(!empty($keyword)){  $this->db->where("UPPER(golongan) LIKE '%".strtoupper($keyword)."%'");    }
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 
					 $this->db->order_by("provinsi_id","ASC");
					 $this->db->order_by("golongan","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
}
