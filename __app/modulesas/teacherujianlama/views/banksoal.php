
<link href="<?php echo base_url(); ?>__statics/js/datatable/datatables.min.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url(); ?>__statics/js/datatable/datatables.min.js" type="text/javascript"></script>

<div class="acc-setting">
										<h3 id="titleujian"> <i class="fa fa-file"></i> Bank Soal Madrasah </h3>
										
										<div class="cp-field ">
										<div class="alert alert-warning" style="background-color:#252b87;color:white;">
													<?php 
													  $ujiannya = $this->db->query("select count(id) as jml from tm_ujian")->row();
													  $soalnya = $this->db->query("select count(id) as jml from tr_soal")->row();
													  
													  
													  ?>
													  
													  
													  Terdapat <u><?php echo ($soalnya->jml); ?> Soal</u> pada Bank Soal Madrasah Anda   dari pelaksanaan <u><?php echo $ujiannya->jml; ?></u> Ujian Berbasis CBT  
													</div>
										
										  <div class="row">
													
													 
													         <div class="col-md-3">
																  <select class="form-control" id="tmguru_id">
																	   <option value="">-  Guru  - </option>
																	 <?php 
																	   $guru = $this->db->query("select id,nama from tm_guru order by nama asc")->result();
																	    foreach($guru as $g){
																	 ?>
																       <option value="<?php echo $g->id; ?>"> <?php echo $g->nama; ?> </option>
																     <?php 
																		}
																		
																	?>
																   </select>
															   
																</div>
																
																
								
																<div class="col-md-3">
																  <select class="form-control" id="ajaran">
																	   <option value="">-  TP - </option>
																	   <?php 
																	    
																		   for($p=2019;$p<=2025;$p++){
																			   ?><option value="<?php echo $p; ?>"  > TP <?php echo $p; ?>/<?php echo $p+1; ?></option><?php 
																			   
																		   }
																		  ?>
																   
																   </select>
															   
																</div>
																
																<div class="col-md-3">
															
																  <select class="form-control" id="semester">
																	   <option value="">-  Semester - </option>
																	   <?php 
																	    
																		   for($p=1;$p<=2;$p++){
																			   ?><option value="<?php echo $p; ?>"  > Semester <?php echo semester($p); ?></option><?php 
																			   
																		   }
																		  ?>
																   
																   </select>
															   
																</div>
																
																<div class="col-md-3">
																  <select class="form-control" id="tmmapel_id">
																	   <option value="">-  Mapel   - </option>
																	 <?php 
																	   $mapel = $this->db->query("select id,nama from tm_mapel order by nama asc")->result();
																	    foreach($mapel as $g){
																	 ?>
																       <option value="<?php echo $g->id; ?>"> <?php echo $g->nama; ?> </option>
																     <?php 
																		}
																		
																	?>
																   </select>
															   
																</div>
																
														</div>
														<br>
														
													     <div class="table-responsive">
														   <table style="font-size:12px" id="datatablebank" width="100%" class="table table-hover table-striped table-bordered">
														    <thead class="bg-blue">
															  <tr>
															    <th width="5px"> NO </th>
																<th> PEMILIK </th>
																<th> KELAS </th>
															    <th> TP</th>
															    <th> NAMA BANK</th>								    
															    <th> MATAPELAJARAN </th>
															    
															    <th> JUMLAH SOAL </th>
																
															   
																
															   
															   
															   
															
															  </tr>
															</thead>
														   
														    <tbody>
															
															  
															  
															</tbody>
														   
														   
														   
														   </table>
														 
														 
														 
														  <br>
														   
														 
														 </div>
													
													
												
										
										</div>
</div>


<div id="MyModalAgenda" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="width:100%; height: 100%;padding:0;margin:0">

    <!-- Modal content-->
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color:white">Detail Bank Soal Madrasah </h4>
      </div>
      <div class="modal-body" id="loadinghasil">
        <p>Sedang memuat, mohon tunggu...</p>
      </div>
      <div class="modal-footer">
        
      </div>
    </div>

  </div>
</div>



<script type="text/javascript">
$(function() {
var datatablebank = $('#datatablebank').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Belum ada Rencana Pembelajaran",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[5,25, 50,100,200,300,500,1000, 800000000], [5,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 
					 
					
					"ajax":{
						url :"<?php echo site_url("teacherujian/grid_bank"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						  data.tmguru_id = $("#tmguru_id").val();
						  data.ajaran = $("#ajaran").val();
						  data.semester = $("#semester").val();
						  data.tmmapel_id = $("#tmmapel_id").val();
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
	

             $(document).off("change","#tmguru_id,#ajaran,#semester,#tmmapel_id").on("change","#tmguru_id,#ajaran,#semester,#tmmapel_id",function(){
	  
				  datatablebank.ajax.reload(null,false);	
				  
			  })	
			  
			
			$(document).off("click",".lihatsoal").on("click",".lihatsoal",function(){
				
				var id = $(this).attr("dataid");
				var tmujian_id = "<?php echo $ujian->id; ?>";
				  $.post("<?php echo site_url('teacherujian/lihatsoal'); ?>",{id:id,tmujian_id:tmujian_id},function(data){
					  
					  
					  $("#loadinghasil").html(data);
				  })
				
				
				
			})
				
			
		} );	  
</script>