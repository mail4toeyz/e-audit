<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pelaksanaancbt extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();

		if(!$this->session->userdata("status")){						
			$ajax = $this->input->get_post("ajax",true);		 
				if(!empty($ajax)){
			   
				   echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("login")."'>disini </a></center>";
					  exit();
				 }else{
					 redirect(site_url()."login");
				 }
			
		   }


		
		  $this->load->model('M_siswa','m');
		  $this->load->helper('exportpdf_helper');  
		
	  }
	  
   function _template($data)
	{
		
		
	      $this->load->view('admin/page_header',$data);	
		
		
	}
		


	 public function ujian()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
         $id              = $this->input->get_post("id",true);	
         $kategori        = $this->input->get_post("kategori",true);	
		
		 $data['title']   	 = "Data Ujian  ";
		 $data['id']   		 =  $id; 
		 $data['kategori']   =  $kategori;
	     if(!empty($ajax)){
					    
			 $this->load->view('page_ujian',$data);
		
		 }else{
			 
		     $data['konten'] = "page_ujian";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid_ujian(){
		
		  $iTotalRecords = $this->m->grid_ujian(false)->num_rows();
		  $kategori_ujian = $this->input->get_post("kategori_ujian");
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid_ujian(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			

         
					
					$nilai      = $val['nilai']; 
					$waktu      = round($val['waktu']/60)." Menit"; 
					$url = site_url("pelaksanaancbt/cetak?id=".$val['id']."&tmujian_id=".$val['id_bank']."&id_ujian=".$val['id_ujian']."");
					$lembarsoal ="<a href='".$url."' class='btn btn-success btn-sm ' tmujian_id='".$val['id_ujian']."'><span class='fa fa-file'></span> Unduh Lembar Jawaban </a>";
					$ljk        ="<a href='#' class='btn btn-primary btn-sm ljk' tmujian_id='".$val['id_ujian']."' data-toggle='modal' data-target='#defaultModal' style='display:none'> </a>";
				
				
			
			

				$bantuan   = $this->db->query("select id from bantuan where tmsiswa_id='".$val['id']."' AND jawaban =''")->row();
				$pertanyaan="";

				if(!is_null($bantuan)){
				$pertanyaan ="<a href='#' class='btn btn-warning btn-sm bantuan' tmsiswa_id='".$val['id']."' data-toggle='modal' data-target='#pelaksanaanmodal'><span class='fa fa-reply'></span> </a>";
			

				}
			   
				$status = "";
				  if($val['status']=="Y"){

					$status = "<br><span class='badge badge-pill badge-primary'>Sedang mengerjakan </span>";
					  if($_SESSION['status']=="admin"){

						$status .="<a href='".site_url('datasiswa/selesaikan?id='.$val['id_ujian'].'')."' class='btn btn-warning btn-sm' target='_blank'><span class='fa fa-stop'></span> </a>";
					  }

				  }else if($val['status']=="N"){

					$status = "<br><span class='badge badge-pill badge-danger'>Selesai mengerjakan </span>";

				  }

				  if($val['posisi']==1){
					

				
				  }else{
					$dposisi = array("1"=>"Guru  ","2"=>"Kepala RA","3"=>"Pengawas Madrasah ","4"=>"Dosen ","5"=>"Praktisi Pendidikan ","6"=>"Widyaiswara","7"=>"Kepala Madrasah");
												
				

				  }
			
			  $provinsi = $this->Reff->get_kondisi(array("id"=>$val['provinsi']),"provinsi","nama");
			  $kota     = $this->Reff->get_kondisi(array("id"=>$val['kota']),"kota","nama");
			  $jml_soal = $this->db->query("select count(id) as jml from tr_soal where tmujian_id='".$val['id_bank']."'")->row();
              $url = site_url("datasiswa/cetak?id=".$val['id']."&tmujian_id=".$val['id_bank']."&id_ujian=".$val['id_ujian']."");
				$no = $i++;
				$records["data"][] = array(
					$no,
					$lembarsoal.$ljk,
					$val['no_test'],
					"<a href='".$url."'>".$val['nama'].'</a>',
					$val['tgl_lahir'],
					$this->Reff->get_kondisi(array("id"=>$val['id_bank']),"tm_ujian","nama"),
				
					
					$waktu
					
						

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}

	public function cetak(){
	    
		$data['peserta']   = $this->db->query("select * from tm_siswa where id='".$_GET['id']."'")->row();
			
		$data['soal']      = $this->db->query("select * from   tm_ujian where id='".$_GET['tmujian_id']."'")->row();
		$data['id_ujian']  = $_GET['id_ujian'];
			
		$pdf_filename = "Jawaban ".str_replace(array(".","'"),"",$data['peserta']->nama).str_replace(",","_",$data['soal']->nama).".pdf";	 
	
		$user_info = $this->load->view('cetak', $data, true);
	
		 $output = $user_info;
		
		// echo $output;
	
		generate_pdf($output, $pdf_filename,TRUE);
		
	
	
	}


  public function selesaikan(){


	$id_tes = $this->input->get_post('id', true);
		$id_tes = ($id_tes);
		$list_jawaban = $this->m->getJawaban($id_tes);
		
		// Pecah Jawaban
		$pc_jawaban = explode(",", $list_jawaban);
		
		$jumlah_benar 	= 0;
		$jumlah_salah 	= 0;
		$jumlah_ragu  	= 0;
		$nilai_bobot 	= 0;
		$total_bobot	= 0;
		$jumlah_soal	= sizeof($pc_jawaban);

		foreach ($pc_jawaban as $jwb) {
			$pc_dt 		= explode(":", $jwb);
			$id_soal 	= $pc_dt[0];
			$jawaban 	= $pc_dt[1];
			$ragu 		= $pc_dt[2];

			$cek_jwb 	 = $this->m->getSoalById($id_soal);
			
			if($cek_jwb->jenis==1){

				if($cek_jwb->jawaban==$jawaban){
					$jumlah_benar++;
					$nilai_bobot = $nilai_bobot + $cek_jwb->bobot;
					$total_bobot = $total_bobot + $cek_jwb->bobot;
				}else{
					$jumlah_salah++;
				} 

			}else if($cek_jwb->jenis==9){
				$arr_nilai = array("A"=>"1","B"=>"2","C"=>"3","D"=>"4","E"=>"5");
				$nilai_bobot = $nilai_bobot + $arr_nilai[$jawaban];
				$total_bobot = $total_bobot + $cek_jwb->bobot;
				
			}
			
		}

		$nilai 			= ($jumlah_benar / ($jumlah_soal))  * 100;
		$nilai_bobot 	= ($total_bobot / ($jumlah_soal))  * 100;

		$d_update = [
			'jml_benar'		=> $jumlah_benar,
			//'tgl_selesai'	=> date("Y-m-d H:i:s"),
			'nilai'			=> $nilai,
			'nilai_bobot'	=> number_format(floor($nilai_bobot), 0),
			'status'		=> 'N'
		];
          
		$this->db->update('h_ujian', $d_update,array("id"=>$id_tes));
		print_r($d_update);


  }

  public function hasilujian()
  {  
	  
		
		
			  
	   $ajax            = $this->input->get_post("ajax",true);	
	   $id              = $this->input->get_post("id",true);	
	   $kategori        = $this->input->get_post("kategori",true);	
	  
	   $data['title']   	 = "Hasil Ujian  ";
	   $data['id']   		 =  $id; 
	   $data['kategori']   =  $kategori;
	   if(!empty($ajax)){
					  
		   $this->load->view('hasilujian',$data);
	  
	   }else{
		   
		   $data['konten'] = "hasilujian";
		   
		   $this->_template($data);
	   }
  

  }

  public function grid_hasilujian(){

	$iTotalRecords = $this->m->grid_hasilujian(false)->num_rows();
	$kategori_ujian = $this->input->get_post("kategori_ujian");
	$iDisplayLength = intval($_REQUEST['length']);
	$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
	$iDisplayStart = intval($_REQUEST['start']);
	$sEcho = intval($_REQUEST['draw']);
	
	$records = array();
	$records["data"] = array(); 

	$end = $iDisplayStart + $iDisplayLength;
	$end = $end > $iTotalRecords ? $iTotalRecords : $end;
	
	$datagrid = $this->m->grid_hasilujian(true)->result_array();
	 
	$sta = array("1"=>"S1","2"=>"S2","3"=>"S3");



	 $i= ($iDisplayStart +1);
	 $no=0;
	 foreach($datagrid as $val) {
	   $no++;

	  // $url = site_url("pelaksanaancbt/cetak?id=".$val['id']."&tmujian_id=".$val['id_bank']."&id_ujian=".$val['id_ujian']."");
	   //$lembarsoal ="<a href='".$url."' class='btn btn-success btn-sm ' tmujian_id='".$val['id_ujian']."'><span class='fa fa-file'></span> Download  </a>";
		
		  $records["data"][] = array(
			  $no,
			 // $lembarsoal,
			  $val['no_test'],
			  $val['nama'],		
			  $val['tgl_lahir'],	
			  $this->Reff->get_kondisi(array("id_admin"=>$val['madrasah_peminatan']),"madrasah_peminatan","owner"),
			  $val['peringkat'],
			  $val['nilai'],
			  $val['islam'],
			  $val['islam_benar'],
			  $val['islam_salah'],
			  
			  $val['inggris'],
			  $val['inggris_benar'],
			  $val['inggris_salah'],

			  $val['arab'],
			  $val['arab_benar'],
			  $val['arab_salah'],

			  $val['kompetensi'],
			  $val['kompetensi_benar'],
			  $val['kompetensi_salah'],

			  $val['moderasi'],
			  $val['moderasi1'],
			  $val['moderasi2'],
			  $val['moderasi3'],
			  $val['moderasi4'],
			  $val['moderasi5']
				  

			);

		//}
	//}
		}
  
	$records["draw"] = $sEcho;
	$records["recordsTotal"] = $iTotalRecords;
	$records["recordsFiltered"] = $iTotalRecords;
	
	echo json_encode($records);
}



public function ljk(){
		
	$tmujian_id = $this->input->get_post("tmujian_id");

	$data = array();
	$data['tmujian_id']  = $tmujian_id;  
		  
		   
	   
	$this->load->view("ljk",$data);
	
}

}
