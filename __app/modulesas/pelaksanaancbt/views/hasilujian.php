

<div class="col-md-12">
		
				       <div class="card">
					   <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
						<div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
							<h6 class="text-white text-capitalize ps-3"><?php echo  $this->Reff->get_kondisi(array("id"=>$kategori),"tm_kategori","nama"); ?> <br>
							Dibawah ini adalah data Hasil Ujian Peserta  </h6>
						</div>
						</div>
						
					
						 <div class="card-body">
						
						 		
					   <div class="row" >
						 
					        

					   	
							   <div class="col-md-3">
								
								<select class="form-control "    id="kategori" >
													 <option value=""> Filter Kategori </option>
													 <?php 
												  $pengaturan_id  = $this->Reff->set();
												  $kelas = $this->db->query("SELECT * from madrasah_peminatan where id_admin IN(select madrasah_peminatan from tm_siswa)")->result();
													 foreach($kelas as $i=>$r){
														 
														?><option value="<?php echo $r->id_admin; ?>" > <?php echo ($r->owner); ?></option><?php  
														 
													 }
												?>
								</select>
							</div>
		


						        		
								<div class="col-md-4">
								
									<div class="input-group">
									<input type="text" class="form-control" id="keyword"  placeholder="Cari  Nomor Tes atau Nama disini..">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="button">CARI</button>
									</div>
									</div>
								</div>
					   </div>
					   
					   <br>

					   <button onclick="btn_export()" class="btn btn-default">
						Cetak Excel with Header
					</button>
							<div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
									<tr>
                                            <th width="2px" rowspan="2">No</th>
                                            
                                    
                                            <th rowspan="2">No Peserta </th>
											<th rowspan="2">Nama </th>                                            
                                            <th rowspan="2">Tgl Lahir </th>                                          
                                            <th rowspan="2">Madrasah </th>                                          
                                            <th rowspan="2">Rangking </th>                                          
                                            <th rowspan="2">Nilai Akhir  </th> 
											<th colspan="3"> Pengetahuan Agama Islam </th>
											<th colspan="3"> Kemampuan Bahasa Inggris </th>
											<th colspan="3"> Kemampuan Bahasa Arab </th>
											<th colspan="3"> Kompetensi Kepala Madrasah </th>
											<th colspan="6"> Moderasi Beragama </th>
                                            

                                            
                                        </tr>

										 <tr>

											<th> Skor </th>
                                            <th> Benar </th>
                                            <th> Salah </th>

											<th> Skor </th>
                                            <th> Benar </th>
                                            <th> Salah </th>

											<th> Skor </th>
                                            <th> Benar </th>
                                            <th> Salah </th>

											<th> Skor </th>
                                            <th> Benar </th>
                                            <th> Salah </th>

											
											<th> Skor </th>
											<th> Sangat tidak setuju </th>
                                            <th> Tidak setuju </th>
                                            <th> Ragu-ragu </th>
                                            <th> Setuju </th>
                                            <th> Sangat setuju </th>

										  </tr>


                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                         </div>
                         </div>
                         </div>
                 
 </div> 
      <div id="defaultModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	     Lembar Jawaban 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body" id="loadform">
        <p>Loading...</p>
      </div>
      
    </div>

  </div>
</div>


<div id="pelaksanaanmodal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body" id="loadbody">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<script src="<?php echo base_url(); ?>__statics/js/excel/xlsx.extendscript.js"></script>
<script src="<?php echo base_url(); ?>__statics/js/excel/xlsx-style/xlsx.full.min.js"></script>

<script src="<?php echo base_url(); ?>__statics/js/excel/export.js"></script>
	
<script type="text/javascript">

function btn_export() {
        var table1 = document.querySelector("#datatableTable");
        var opt = {
            rowIndex: 4
        }; //开头空4行
        var sheet = XLSX2.utils.table_to_sheet(table1, opt);

  
     

        openDownloadDialog(sheet2blob(sheet), ' .Hasil CBT .xlsx');
    }
	

			   
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							
							text:'Cetak Excel',
						
							},
							
							
							
							
						
							{
							extend: 'colvis',
							
							text:' Pengaturan Kolom ',
							
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("pelaksanaancbt/grid_hasilujian"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
							data.keyword 		= $("#keyword").val();
						
							data.kategori 		= $("#kategori").val();
							
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
				
				
	
				$(document).on("input","#keyword",function(){
	  
					dataTable.ajax.reload(null,false);	
					
				});

				$(document).on("change","#tmmadrasah_id,#kategori,#provinsi,#kota,#unsur,#status",function(){
						
						dataTable.ajax.reload(null,false);	
						
					});

			

</script>
				