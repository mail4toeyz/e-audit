<?php

class M_siswa extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
    }
	public function getUjianById($id)
    {
        $this->db->select('*');
        $this->db->from('tm_ujian a');
      
        $this->db->where('a.id', $id);
        return $this->db->get()->row();
    }
	
	 public function getSoal($id)
    {
        $ujian = $this->getUjianById($id);
        $order = $ujian->jenis==="acak" ? 'rand()' : 'urutan';

        $this->db->select('id as id_soal, soal,opsi_a, opsi_b, opsi_c, opsi_d, opsi_e, jawaban,jenis');
        $this->db->from('tr_soal');
  
        $this->db->where('tmujian_id', $id);
        $this->db->order_by($order);
        
        return $this->db->get()->result();
    }
	
	 public function HslUjian($id, $tmsiswa_id)
    {
        $this->db->select('*, UNIX_TIMESTAMP(tgl_selesai) as waktu_habis');
        $this->db->from('h_ujian');
        $this->db->where('tmujian_id', $id);
        $this->db->where('tmsiswa_id', $tmsiswa_id);
        return $this->db->get();
    }
	
	
	  public function ambilSoal($pc_urut_soal1, $pc_urut_soal_arr)
    {
        $this->db->select("*,id as id_soal, {$pc_urut_soal1} AS jawaban");
        $this->db->from('tr_soal');
        $this->db->where('id', $pc_urut_soal_arr);
         $data =  $this->db->get()->row();
         if(!is_null($data)){
            return '';
         }else{

            return $data;
         }
    }
	
	  public function getJawaban($id_tes)
    {
        $this->db->select('list_jawaban');
        $this->db->from('h_ujian');
        $this->db->where('id', $id_tes);
        return $this->db->get()->row()->list_jawaban;
    }
	
	 public function getSoalById($id)
    {
        return $this->db->get_where('tr_soal', ['id' => $id])->row();
    }
	
	public function grid_hasilujian($paging){
       
		$kategori         = $this->input->get_post("kategori");
		$this->db->select("a.*,b.no_test,b.nama,b.madrasah_peminatan,b.tgl_lahir,b.peringkat");
        $this->db->from('tr_skor a');
        $this->db->join('tm_siswa b','a.tmsiswa_id=b.id','LEFT');
		
	
	    if(!empty($key)){  $this->db->where("(UPPER(b.nama) LIKE '%".strtoupper($key)."%' or (b.no_test) LIKE '%".strtoupper($key)."%')");    }
	  
	  
		if(!empty($kategori)){  $this->db->where("madrasah_peminatan",$kategori);    }
	
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					
					 $this->db->order_by("b.madrasah_peminatan","DESC");
					 $this->db->order_by("a.nilai","DESC");
					
					 $this->db->order_by("b.nama","ASC");
					
					
		
			 }
      
		
		
		return $this->db->get();
		
	}


	public function grid($paging){
       
	    $key           = $this->input->get_post("keyword");
	    $tmmadrasah_id = $this->input->get_post("tmmadrasah_id");
	    $kategori         = $this->input->get_post("kategori");
		$sesi          = $this->input->get_post("sesi");
		$provinsi          = $this->input->get_post("provinsi");
		$kota          = $this->input->get_post("kota");
		$pewawancara      = $this->input->get_post("pewawancara");
		$this->db->select("*");
        $this->db->from('tm_siswa');
		
		
		if($_SESSION['status']=="admin"){
			if(!empty($provinsi)){  $this->db->where("provinsi",$provinsi);    }
			if(!empty($kota)){  $this->db->where("kota",$kota);    }
		}else if($_SESSION['status']=="kanwil"){

			$this->db->where("provinsi",$_SESSION['aksi_id']); 
			$this->db->where("posisi",2); 
		}else if($_SESSION['status']=="kankemenag"){

			$this->db->where("kota",$_SESSION['aksi_id']); 
			$this->db->where("posisi",3);
		}


	
	    if(!empty($key)){  $this->db->where("(UPPER(nama) LIKE '%".strtoupper($key)."%' or (no_test) LIKE '%".strtoupper($key)."%')");    }
	  
	    if(!empty($kategori)){  $this->db->where("kategori",$kategori);    }
	    if(!empty($pewawancara)){  $this->db->where("pewawancara",$pewawancara);    }
	   
	   
	
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("kategori","ASC");
					
					
		
			 }
      
		
		
		return $this->db->get();
		
	}


	public function grid_ujian($paging){
       
	    $key           = $this->input->get_post("keyword");
	    $tmmadrasah_id = $this->input->get_post("tmmadrasah_id");
	    $kategori         = $this->input->get_post("kategori");
		$sesi          = $this->input->get_post("sesi");
		$provinsi          = $this->input->get_post("provinsi");
		$kota          = $this->input->get_post("kota");
		$unsur         = $this->input->get_post("unsur");
		$status        = $this->input->get_post("status");

		$kategori_ujian = $this->input->get_post("kategori_ujian");
		
		$this->db->select("a.*,b.id as id_ujian,b.jml_benar,b.nilai,b.tgl_mulai,b.tgl_selesai,b.waktu,b.status,b.tmujian_id as id_bank");
        $this->db->from('tm_siswa a');
		$this->db->join('h_ujian b','a.id=b.tmsiswa_id','LEFT');
		
		if($kategori_ujian==0){
			$this->db->where("b.kategori=0"); 

		}else{
			$this->db->where("b.kategori !=0"); 
		}
		
		
	

	
	    if(!empty($key)){  $this->db->where("(UPPER(a.nama) LIKE '%".strtoupper($key)."%' or (a.no_test) LIKE '%".strtoupper($key)."%')");    }
	  
	    // if(!empty($kategori)){  $this->db->where("a.posisi",$kategori);    }
	    //  if(!empty($provinsi)){  $this->db->where("a.provinsi",$provinsi);    }
	    // if(!empty($kota)){  $this->db->where("a.kota",$kota);    }
	    // if(!empty($unsur)){  $this->db->where("a.unsur",$unsur);    }
	    // if(!empty($status)){  $this->db->where("b.status",$status);    }
	   
	
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("a.no_test","ASC");
					
					
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
	
	public function insert(){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
			
		$this->db->insert("tm_siswa",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
	public function update($id){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
		  
		
		$this->db->where("id",$id);
		$this->db->update("tm_siswa",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	


	public function grid_kelulusan($paging){
       
	    $key           = $this->input->get_post("keyword");
	    $tmmadrasah_id = $this->input->get_post("tmmadrasah_id");
	    $kategori         = $this->input->get_post("kategori");
		$sesi          = $this->input->get_post("sesi");
		$provinsi          = $this->input->get_post("provinsi");
		$kota          = $this->input->get_post("kota");
		$literasi      = $this->input->get_post("literasi");
		$keputusan      = $this->input->get_post("keputusan");
		$pewawancara      = $this->input->get_post("pewawancara");
		$unsur            = $this->input->get_post("unsur");
		$urutkan            = $this->input->get_post("urutkan");
		
		$this->db->select("*");
        $this->db->from('tm_siswa');
		
		//$this->db->where("nilai IS NOT NUll"); 
		
		if($_SESSION['status']=="admin"){
			if(!empty($provinsi)){  $this->db->where("provinsi",$provinsi);    }
			if(!empty($kota)){  $this->db->where("kota",$kota);    }
		}else if($_SESSION['status']=="kanwil"){

			$this->db->where("provinsi",$_SESSION['aksi_id']); 
			$this->db->where("kategori",2); 
		}else if($_SESSION['status']=="kankemenag"){

			$this->db->where("kota",$_SESSION['aksi_id']); 
			$this->db->where("kategori",3);
		}

		if(!empty($keputusan)){  
			
			if($keputusan==1){
				$this->db->where("kelulusan",1);
				//$this->db->where("pri is NULL");
			}else if($keputusan==2){

				$this->db->where("kelulusan !=",1);

			}

		
		
		}


		if(!empty($kategori)){  
			
			if($kategori==1){
				
			}else{

				$this->db->where("keputusan",1); 

			}

		
		
		}
	
	    if(!empty($key)){  $this->db->where("(UPPER(nama) LIKE '%".strtoupper($key)."%' or (no_test) LIKE '%".strtoupper($key)."%')");    }
	  
	    if(!empty($kategori)){  $this->db->where("kategori",$kategori);    }
	    if(!empty($literasi)){  $this->db->where("literasi",$literasi);    }
	    if(!empty($unsur)){  $this->db->where("unsur",$unsur);    }
	     
	   
	
		
		
         if($paging==true){
					 $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 
					if(empty($urutkan)){
						$this->db->order_by("jenjang","ASC");
						$this->db->order_by("literasi","ASC");

					
					 
					 $this->db->order_by("nilai","DESC");
					 $this->db->order_by("pedadogis","DESC");
					 $this->db->order_by("profesional","DESC");
					 $this->db->order_by("wawancara","DESC");
					 $this->db->order_by("no_test","ASC");

					}else{

						if($urutkan =="nilai"){
							$this->db->order_by("nilai","DESC");
							$this->db->order_by("pedadogis","DESC");
							$this->db->order_by("profesional","DESC");
							$this->db->order_by("wawancara","DESC");
							$this->db->order_by("no_test","ASC");


						}else if($urutkan=="provinsi"){
							$this->db->order_by("provinsi","ASC");
							$this->db->order_by("nilai","DESC");
							$this->db->order_by("pedadogis","DESC");
							$this->db->order_by("profesional","DESC");
							$this->db->order_by("wawancara","DESC");
							$this->db->order_by("no_test","ASC");


						}else if($urutkan=="prioritas"){
							$this->db->order_by("pri","DESC");
							$this->db->order_by("nilai","DESC");
							$this->db->order_by("pedadogis","DESC");
							$this->db->order_by("profesional","DESC");
							$this->db->order_by("wawancara","DESC");
							$this->db->order_by("no_test","ASC");


						}



					}
					
					
		
			 }
      
		
		
		return $this->db->get();
		
	}




}
