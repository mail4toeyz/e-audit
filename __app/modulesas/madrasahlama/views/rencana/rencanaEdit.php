

<form class="form" id="kegiatan_saveEdit" enctype="multipart/form-data" url="<?php echo site_url("madrasah/kegiatan_saveEdit"); ?>">
<input type="hidden" name="id" value="<?php echo $data->id; ?>">

                                                    <div class="form-group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Kegiatan</label>
														<div class="col-lg-9 col-xl-9">
														<select class="select2 form-control" name="kegiatan">
                                                         <option value="">- Pilih Kegiatan -</option>

                                                         <?php 
                                                            $masterkegiatan = $this->db->query("select * from visitasi_tatapmuka where madrasah_id='{$_SESSION['madrasah_id']}' order by prioritas ASC")->result();
                                                              foreach($masterkegiatan as $rm){

																$cekViKeg= $this->db->get_where("visitasi_kegiatan",array("kegiatan_id"=>$rm->id,"madrasah_id"=>$_SESSION['madrasah_id']))->num_rows();
																$disabled="";
																  if($cekViKeg==1 AND $rm->id !=$data->kegiatan_id ){
																	  $disabled="disabled";
																  }

                                                                  ?> <option value="<?php echo $rm->id; ?>" <?php echo ($rm->id==$data->kegiatan_id) ? "selected":""; ?> <?php echo $disabled; ?>> <?php echo $rm->kegiatan; ?> (<?php echo ($rm->kegiatan_id==0) ? "Rencana":"Kesiapan Tatap Muka"; ?> Prioritas <?php echo $rm->prioritas; ?>) </option> <?php 
                                                              }
                                                         ?>

                                                      </select>
														</div>
													</div>
													<div class="form-group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Volume</label>
														<div class="col-lg-9 col-xl-9">
															<input class="form-control form-control-lg form-control-solid" type="number" name="volume" id="volumeEdit" value="<?php echo $data->volume; ?>" />
														</div>
													</div>

													<div class="form-group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Satuan</label>
														<div class="col-lg-9 col-xl-9">
															<input class="form-control form-control-lg form-control-solid" type="text" name="satuan" value="<?php echo $data->satuan; ?>" />
														</div>
													</div>

													<div class="form-group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Harga</label>
														<div class="col-lg-9 col-xl-9">
															<input class="form-control form-control-lg form-control-solid" type="text" id="hargaEdit" onkeyup="return FormatCurrency(this)"  name="harga" value="<?php echo $this->Reff->formatuang2($data->harga); ?>" />
														</div>
													</div>

													<div class="form-group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Biaya</label>
														<div class="col-lg-9 col-xl-9">
															<input class="form-control form-control-lg form-control-solid" type="text" onkeyup="return FormatCurrency(this)"  id="biayaEdit" name="biaya" value="<?php echo $this->Reff->formatuang2($data->biaya); ?>" />
														</div>
													</div>


<center>
<button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="fa fa-times"></span> Tutup </button>
<button type="submit" class="btn btn-success btn-sm"><span class="fa fa-save"></span> Simpan Perubahan </button>
</center>
															</form>

															<script>
																$(document).ready(function() {
   $('.select2').select2({
closeOnSelect: true,
width: '100%' 
});
});

$(document).off("input","#hargaEdit").on("input","#hargaEdit",function(){

     
var harga = HapusTitik($(this).val());
var volume = $("#volumeEdit").val();
var total = TambahTitik(volume * harga)
 $("#biayaEdit").val(total);






});

</script>