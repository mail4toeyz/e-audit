<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	
		<meta charset="utf-8" />
		<title> BKBA | Asesor  </title>
		<meta name="description" content="Updates and statistics" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

		<link rel="shortcut icon" href="<?php echo base_url(); ?>__statics/img/logo.png">	
		<link rel="stylesheet" href="<?php echo base_url(); ?>__statics/fa/css/font-awesome.min.css">	
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">	
   
		<link href="<?php echo base_url(); ?>__statics/tema/css/style.bundle1894.css?v=7.1.9" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>__statics/js/datetimepicker/jquery.datetimepicker.css">	
		
		<link href="<?php echo base_url(); ?>__statics/js/datepicker/datepicker.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/bootstrap-toastr/toastr.min.css"/>
		<link href="<?php echo base_url(); ?>__statics/js/alertify/css/alertify.css" rel="stylesheet">
		<link href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
		<link href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
		<script src="<?php echo base_url(); ?>__statics/tema/plugins/global/plugins.bundle1894.js?v=7.1.9"></script>
		<link href="<?php echo base_url(); ?>__statics/js/upload/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
       <script src="<?php echo base_url(); ?>__statics/js/upload/js/fileinput.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>__statics/js/proses.js"></script>

		<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" type="text/javascript"></script>
		<script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
		<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
		<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript"></script>
		<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js" type="text/javascript"></script>
		<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js" type="text/javascript"></script>
		<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.colVis.min.js" type="text/javascript"></script>
		
		<script src="https://code.highcharts.com/highcharts.js"></script>
		<script src="https://code.highcharts.com/highcharts-more.js"></script>
		<script src="https://code.highcharts.com/modules/exporting.js"></script>
		<script src="https://code.highcharts.com/modules/export-data.js"></script>
		<script src="https://code.highcharts.com/modules/accessibility.js"></script>
			
</head>
	
	<body id="kt_body" class="header-mobile-fixed subheader-enabled aside-enabled aside-fixed aside-secondary-enabled page-loading">
		
		<div id="kt_header_mobile" class="header-mobile">
		
			<a href="<?php echo site_url("asesor"); ?>">
				<img alt="Logo" src="<?php echo base_url(); ?>__statics/img/logo.png" class="logo-default max-h-30px" />
			</a>
		
			<div class="d-flex align-items-center">
				<button class="btn p-0 burger-icon burger-icon-left" id="kt_aside_mobile_toggle">
					<span></span>
				</button>
			</div>
		
		</div>
	

		<div class="d-flex flex-column flex-root">
		
			<div class="d-flex flex-row flex-column-fluid page">
				
			    <?php $this->load->view("page_menu"); ?>
			
				<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
					
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<?php $this->load->view("navbar"); ?>
							<div id="kontendefault">
							
							<?php $this->load->view($konten); ?>
									
						</div>
					</div>
				
					
				
				</div>
				
			</div>
		
		</div>
	
	
	
	
	
    <script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#1BC5BD", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#6993FF", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#1BC5BD", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#E1E9FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
		
	<script src="<?php echo base_url(); ?>__statics/tema/plugins/custom/prismjs/prismjs.bundle1894.js?v=7.1.9"></script>
    <script src="<?php echo base_url(); ?>__statics/tema/js/scripts.bundle1894.js?v=7.1.9"></script>

    <script src="<?php echo base_url(); ?>__statics/tema/js/pages/widgets1894.js?v=7.1.9"></script>
    <script src="<?php echo base_url(); ?>__statics/js/alertify/alertify.js"></script>
    <script src="<?php echo base_url(); ?>__statics/js/jquery.blockui.min.js"></script>
    <script src="<?php echo base_url(); ?>__statics/js/datepicker/datepicker.js" ></script>
    <script src="<?php echo base_url(); ?>__statics/js/bootstrap-toastr/toastr.min.js"></script>
    <script src="<?php echo base_url(); ?>__statics/js/datetimepicker/jquery.datetimepicker.full.js"></script>

      
	</body>
  <script>
   
	
var base_url="<?php echo base_url(); ?>";

function sukses(url,title,html){
	   let timerInterval
	   Swal.fire({
		 type: 'success',
		 title: title,
		 showConfirmButton: false,
		  html: html,

		 timer: 1000,
		 onBeforeOpen: () => {
		   Swal.showLoading()
		   timerInterval = setInterval(() => {
			 Swal.getContent().querySelector('strong')
			   .textContent = Swal.getTimerLeft()
		   }, 100)
		 },
		 onClose: () => {
		   clearInterval(timerInterval)
		 }
	   }).then((result) => {
		 if (
		   /* Read more about handling dismissals below */
		   result.dismiss === Swal.DismissReason.timer
		   
		 ) {
		
		  location.href = base_url+url;
		 }
	   })
}

function gagal(param,title){
	   let timerInterval
	   Swal.fire({
		 type: 'warning',
		 title: title,
		 showConfirmButton: true,
		  html: param

	   })
}


		$(document).on("click","#keluar",function(){
	  
				  var base_url = "<?php echo base_url(); ?>";
				  
				  alertify.confirm("Anda akan keluar dari aplikasi, apakah anda yakin?",function(){
					  
						   $.post("<?php echo site_url("pusat/logout"); ?>",function(data){
							   
							   location.href= base_url;
							   
						   });
					});
				  
				  
				  
			  })
			  
		$(document).off("click",".menuajax").on("click",".menuajax",function (event, messages) {
	           event.preventDefault()
			   var url = $(this).attr("href");
			   var title = $(this).attr("title");
			  
			 
				   
				   
			    $("li").siblings().removeClass('active');
			    $(this).parent().addClass('active');
			    
				
			  	  
			   $("#kontendefault").html('....');
		      loading();
			  $.post(url,{ajax:"yes"},function(data){
				  
				
				  history.replaceState(title, title, url);
				  $('title').html(title);
				
				  $("#kontendefault").html(data);
				 
				 
				    jQuery.unblockUI({ });
				 
			  })
		  })
  </script>
</html>