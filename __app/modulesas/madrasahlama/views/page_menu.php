<?php 
   $uri = $this->uri->segment(1);
   $uri2 = $this->uri->segment(2);
   $uri4 = ($this->uri->segment(4));
 
   
     
  ?>
<div class="aside aside-left  aside-fixed" id="kt_aside">
				
				
					<!--end::Primary-->
					<span class="aside-toggle btn btn-icon btn-primary btn-hover-primary shadow-sm" id="kt_aside_toggle" data-toggle="tooltip" data-placement="right" data-container="body" data-boundary="window" title="Toggle Aside">
								<i class="fa fa-arrow-left icon-sm"></i>
							</span>
					<!--begin::Secondary-->
					<div class="aside-secondary d-flex ">
						<!--begin::Workspace-->
						<div class="aside-workspace scroll scroll-push my-2">
							<!--begin::Tab Content-->
							<div class="tab-content">
								<!--begin::Tab Pane-->
							
								<div class="tab-pane p-3 px-lg-7 py-lg-5 fade show active" id="kt_aside_tab_2">
									<!--begin::Aside Menu-->
									<div class="aside-menu-wrapper flex-column-fluid px-3 px-lg-10 py-5" id="kt_aside_menu_wrapper">
										<!--begin::Menu Container-->
										<?php 
										$data = $this->db->get_where("madrasahedm",array("id"=>$_SESSION['madrasah_id']))->row();
										
										?>
										<div id="kt_aside_menu" class="aside-menu min-h-lg-800px" data-menu-vertical="1" data-menu-scroll="1">
										 <center><img alt="Logo" src="<?php echo base_url(); ?>__statics/img/logo.png" onError="this.onerror=null;this.src='<?php echo base_url(); ?>__statics/img/not.png';" class="max-h-70px" style="border-radius: 50%;" />  <br>
										  <br>
										  <br>
										   <h3><b><?php echo $data->nama; ?> </h3>
											<?php echo $data->provinsi; ?> - <?php echo $data->kota; ?>	</b>
										</center>
											<!--begin::Menu Nav-->
											<ul class="menu-nav">
												

												<li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">
													<a href="<?php echo site_url("madrasah"); ?>" title="Dashboard " class="menu-link menuajax">
                                                      <i class="fa fa-laptop"></i>
														<span class="menu-text" style="padding-left:7px;padding-bottom:7px"> Dashboard   </span>
													
													</a>
													
												</li>
												<li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">
													<a href="<?php echo site_url("madrasah/akun"); ?>" title=" Profil Madrasah " class="menu-link menuajax">
                                                      <i class="fa fa-user"></i>
														<span class="menu-text" style="padding-left:7px;padding-bottom:7px"> Profil Madrasah     </span>
													
													</a>
													
												</li>


												<li class="menu-section">
													<h4 class="menu-text">Menu Utama  </h4>
													<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
												</li>

											



													<li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">
													<a href="<?php echo site_url("madrasah/visitasiAct"); ?>" title="Hasil Visitasi Madrasah  " class="menu-link menuajax">
														<i class="fa fa-check-square-o"></i>
															<span class="menu-text" style="padding-left:7px;padding-bottom:7px">Hasil Visitasi     </span>
														
														</a>
														
													</li>

													
												<li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">
												   <a href="<?php echo site_url("madrasah/catatan"); ?>" title="Catatan Verifikator " class="menu-link menuajax">
                                                      <i class="fa fa-edit"></i>
														<span class="menu-text" style="padding-left:7px;padding-bottom:7px">Catatan Verifikator      </span>
													
													</a>
													
												</li>
											

												<li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">
												   <a href="<?php echo site_url("madrasah/rencanaAct"); ?>" title="Rencana  Pemanfaatan Dana BKBA " class="menu-link menuajax">
                                                      <i class="fa fa-file"></i>
														<span class="menu-text" style="padding-left:7px;padding-bottom:7px">Rencana  Pemanfaatan Dana BKBA      </span>
													
													</a>
													
												</li>

												


												<li class="menu-section">
													<h4 class="menu-text">Menu Pendukung  </h4>
													<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
												</li>


											
												<li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">
													<a href="javascript:;" class="menu-link" id="keluar">
                                                      <i class="fa fa-sign-out"></i>
														<span class="menu-text" style="padding-left:7px;padding-bottom:7px">Keluar Aplikasi   </span>
													
													</a>
													
												</li>


				

            
 
        

                      


											</ul>
											
										</div>
									
									</div>
								
								</div>
							
							</div>
							
						</div>
					
					</div>
			
				</div>