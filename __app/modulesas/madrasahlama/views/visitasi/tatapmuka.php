<?php $status = $this->db->query("select status from pengaturan where tahun='{$data->tahun}'")->row(); $disabled = ($status->status==1) ? "disabled":"";  ?>
<div class="row">
<div class="alert alert-default"> Kesiapan Menghadapi Proses Pembelajaran Tatap Muka Pasca Pandemi pada  <b><?php echo $data->nama; ?> (<?php echo $data->nsm; ?>)</b> </div>

   <div class="col-12">
       
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-striped">
                <thead>
                  <tr>
                    <th rowspan="2">NO</th>
                    <th rowspan="2">KEGIATAN</th>
                    <th colspan="2">URUTAN PRIORITAS</th>
                   
                </tr>
                
                </thead>
                <tbody id="loadtatapmuka">
                    <?php 
                     $master_tatapmuka = $this->db->query("select * from master_tatapmuka");
                     $prioritas              = $this->db->query("select id from master_tatapmuka ")->result(); 
                     foreach($master_tatapmuka->result() as $row){
                      ?>
                       <tr>
                        <td><?php echo $row->id; ?></td>
                        <td><?php echo $row->nama; ?></td>
                        <td>
                            <select class="form-control pilihprioritas" <?php echo $disabled; ?> data-kegiatan="<?php echo $row->id; ?>" data-madrasah_id="<?php echo $data->id; ?>" data-nsm="<?php echo $data->nsm; ?>">
                                 
                              <option value=""> Pilih Prioritas </option>
                              <?php 
                               
                                foreach($prioritas as $rp){
                                  $pri = $this->Reff->get_kondisi(array("madrasah_id"=>$data->id,"kegiatan_id"=>$row->id),"visitasi_tatapmuka","prioritas");
                                  $sel ="";
                                    if($rp->id==$pri){
                                    $sel ="selected";
                                  }

                                    ?><option value="<?php echo $rp->id; ?>" <?php echo $sel; ?>><?php echo $rp->id; ?> </option><?php 
                                }
                              ?>
                              

                            </select>
                    
                       </td>
                     
                     </tr>
                     

                      <?php 

                     }
                     ?>

                </tbody>
            </table>

        </div>
   </div>

   
</div>



<script type="text/javascript">

  $(document).off("change",".pilihprioritas").on("change",".pilihprioritas",function(){
    var kegiatan       = $(this).data("kegiatan");
    var madrasah_id = $(this).data("madrasah_id");
    var nsm         = $(this).data("nsm");
    var nilai       = $(this).val();
    //$("#loadtatapmuka").html('<i class="fas fa-spinner fa-pulse"></i> Sedang menyimpan data, Mohon Tunggu..');
    $.post("<?php echo site_url('asesor/saveVisitasiTatapmuka'); ?>",{kegiatan:kegiatan,madrasah_id:madrasah_id,nsm:nsm,nilai:nilai},function(data){

       // $("#loadtatapmuka").html(data);
        
     

    })



  });


  


</script>