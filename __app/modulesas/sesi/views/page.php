
<div id="showform"></div>


<div class="col-md-12">
		
				       <div class="card">
					   <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
						<div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
							<h6 class="text-white text-capitalize ps-3">Data Sesi </h6>
							
						</div>
						</div>
						
					  
						 <div class="card-body">
					    <div class="row" >
						
						        		
							<div class="col-md-5">
							
								<div class="input-group">
								  <input type="text" class="form-control" id="keyword"  placeholder="Cari    disini..">
								  <div class="input-group-append">
									<button class="btn btn-success btn-sm" type="button">CARI</button>
								  </div>
								</div>
						    </div>
						   </div>
						   
						   <br>
						 <button type="button" target="#loadform" url="<?php echo site_url("sesi/form"); ?>"  data-toggle="modal" data-target="#defaultModal" data-color="cyan" class="btn btn-success btn-sm addmodal"><span class="fa fa-plus"></span> Tambah Data </button>
						       <br><br>
							<div class="table-responsive">
                                <table class="table table-bordered table-striped  " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
                                        <tr>
                                            <th width="2px">NO</th>
                                            
                                            <th>SESI   </th>
                                            <th>PUKUL   </th>
                                            <th>AKSI   </th>
                                           
                                            
                                        </tr>
                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                         </div>
                         </div>
                 
 </div>
              
  <div id="defaultModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body" id="loadform" style="color:black">
        <p>Loading...</p>
      </div>
      
    </div>

  </div>
</div>
	
<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							exportOptions: {
							  columns: [ 0,1,2]
							},
							text:'<font color:"black">Cetak Excel</font>',
							
							},
							
							
							
							
						
							{
							extend: 'colvis',
							
							text:' Pengaturan Kolom ',
							
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("sesi/grid"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						data.trkelas_id = $("#trkelas_id").val();
						data.keyword = $("#keyword").val();
						data.ajaran = $("#ajaran").val();
						
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
				
				$(document).on("input","#keyword",function(){
	  
				  dataTable.ajax.reload(null,false);	
				  
			  });
			  $(document).on("change","#trkelas_id,#ajaran",function(){
	  
				  dataTable.ajax.reload(null,false);	
				  
			  });
	


</script>
				