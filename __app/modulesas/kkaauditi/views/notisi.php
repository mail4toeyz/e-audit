            
          <?php
           $tanggapan = $this->db->get_where("tr_tanggapan",array("notisi_id"=>$notisi->id))->row();
           ?>
          <div class="alert alert-info alert-dismissible fade show" role="alert">
                <i class="bi bi-info-circle me-1"></i>
                <?php echo $kka->nama; ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              <br>
            <form class="row g-3" method="post" action="<?php echo site_url('kkaauditi/saveNotisi'); ?>">
                <div class="col-12">
                    <input type="hidden" name="notisi_id" value="<?php echo $notisi->id; ?>">
                    <input type="hidden" name="satker_id" value="<?php echo $satker_id; ?>">
                    <input type="hidden" name="kka_id" value="<?php echo $kka->id; ?>">

                        <span class="badge border-danger border-1 text-danger" style="text-align:left">Temuan : <?php echo  $notisi->kode_temuan."-".$this->Reff->get_kondisi(array("jenis" => $notisi->kode_temuan), "tm_temuan", "deskripsi"); ?></span>
                       
                </div>

                <div class="col-12">
                <span class="badge border-danger border-1 text-danger" style="text-align:left">Rekomendasi : <?php echo  $notisi->kode_rekomendasi."-".$this->Reff->get_kondisi(array("id" => $notisi->kode_rekomendasi), "tm_rekomendasi", "nama"); ?></span>
                    
                </div>


                <div class="col-12">
                   
                <span class="badge border-secondary border-1 text-secondary"><?php echo  $notisi->keterangan; ?></span>
                     
                  
               </div>


                <div class="col-12">
                  <label for="inputPassword4" class="form-label">Tanggapan </label>
                  <textarea class="form-control" id="keterangan" required> <?php if(isset($tanggapan)){ echo $tanggapan->keterangan; }  ?></textarea>
                </div>

                <div class="col-12">
                     <input id="file" name="file" type="file"  accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps" />
                </div>
               
                
              </form>



              <script type="text/javascript">
  
 

  $(document).ready(function() {

$("#file").fileinput({
          uploadUrl: "<?php echo site_url("kkaauditi/saveTanggapan"); ?>",
          overwriteInitial: false,
          
          browseClass: "btn btn-success",
          browseLabel: "Ambil File",
          browseIcon: "<i class=\"fa fa-file-signature\"></i> Ambil File ",
          removeClass: "btn btn-danger",
          removeLabel: "Delete",
          removeIcon: "<i class=\"fa fa-trash\"></i>  Hapus File",
          uploadClass: "btn btn-info",
          uploadLabel: "Upload",
          uploadIcon: "<i class=\"fa fa-upload\"></i>  Upload  File",
          initialPreviewAsData: true,
                  uploadExtraData: function() {
                      return {
                         
                          notisi_id: "<?php echo $notisi->id; ?>",
                          keterangan: $("#keterangan").val()
                      };
                  },
                  initialPreview: [
                      <?php 
                          
                                 $lihat="";
                              if(!is_null($tanggapan)){
                                 
                                      
                                      $lihat .= ",'https://drive.google.com/file/d/".$tanggapan->file."/preview'";
                               
                                  
                                  
                              }
                          
                          echo substr($lihat,1);
                          
                          
                          ?>
                          
                          
                      ],
                              initialPreviewConfig: [

                            <?php 
                               $lihat="";
                               if(!is_null($tanggapan)){
                                  
                                
                                  
                                  $lihat .= ',{caption: "", size:200, width: "100%", url: "'.site_url("kkaauditi/hapusTanggapan").'", key: "'.$tanggapan->id.'"}';
                                }
                                
                                
                              
                            
                            echo substr($lihat,1);


                            ?>


                                
                              ]
                 
                          


                              
                            });





                              });		
                              

                            </script>


              