
  <script src="https://meet.jit.si/external_api.js"></script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/cbt/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/cbt/dist/css/skins/skin-blue.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/cbt/dist/css/mystyle.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/cbt/bower_components/pace/pace-theme-flash.css">


      <div class="row" style="color:black">

         
         <div class="col-md-6">
                    <?php echo form_open('', array('id'=>'ujian'), array('id'=> $id_tes)); ?>
                    <?php  
                    $jmsoal = $this->db->query("select count(id) as jmsoal from tm_wawancara where tmujian_id='".$ujian->id."'")->row(); 
                    
                    ?>

                <div class="box box-warning" >
                    <div class="box-header with-border">
                    

                    <div class="box-tools pull-right">
                    <span class="badge ">Soal No. <span id="soalke"></span>  Dari <span id="darisoal"><?php echo $jmsoal->jmsoal; ?> Soal </span> </span>
                        <button class="btn btn-box-tool" onclick="increaseFontSizeBy100px()" type="button"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" onclick="increaseFontSizeBy1px()" type="button"><i class="fa fa-plus"></i></button>

                    </div>
                   
                    </div>
                
                    <div class="box-body" id="lembarsoal_dendi">
                       <?php echo $html; ?>

                    </div>

                    <div class="box-footer text-center">
                            <a class="action back btn btn-info" rel="0"  onclick="return back();"><i class="glyphicon glyphicon-chevron-left"></i> Sebelumnya</a>
                            
                            <a class="ragu_ragu btn btn-warning" rel="1" onclick="return tidak_jawab();" style="display:none">Ragu-ragu</a>
                            
                            <a class="action next btn btn-info" rel="2"  onclick="return next();"><i class="glyphicon glyphicon-chevron-right"></i> Selanjutnya </a>
                            <a class="selesai action submit btn btn-danger"  onclick="return simpan_akhir();"><i class="glyphicon glyphicon-stop"></i> Selesai</a>
                            <input type="hidden" name="jml_soal" id="jml_soal" value="<?=$no; ?>">
                    </div>


                </div>
         </div>


         <div class="col-md-6">
                   
                <div class="box box-warning" >
                    <div class="box-header with-border">
                    <h3 class="box-title" style="color:black"><?php echo $data->no_test; ?> - <?php echo $data->nama; ?>  </h3>

                   
                   
                    </div>
                
                    <div class="box-body" >
                    <div id='jitsi-meet-conf-container'></div>
                    </div>

                   


                </div>
         </div>
</div>




<script type="text/javascript">

function increaseFontSizeBy100px() {
    txt = document.getElementById('lembarsoal_dendi');
    style = window.getComputedStyle(txt, null).getPropertyValue('font-size');
    currentSize = parseFloat(style);
    txt.style.fontSize = (currentSize - 1) + 'px';
}
function increaseFontSizeBy1px() {
    txt = document.getElementById('lembarsoal_dendi');
    style = window.getComputedStyle(txt, null).getPropertyValue('font-size');
    currentSize = parseFloat(style);
    txt.style.fontSize = (currentSize + 1) + 'px';
}



    var base_url        = "<?php echo base_url(); ?>";
    var id_tes          = "<?php echo $id_tes; ?>";
    var widget          = $(".step");
    var total_widget    = widget.length;
	
	



</script>

<script type="text/javascript">

$(document).ready(function () {
    var t = $('.sisawaktu');
    if (t.length) {
        sisawaktu(t.data('time'));
    }

    buka(1);
    simpan_sementara();

    widget = $(".step");
    btnnext = $(".next");
    btnback = $(".back");
    btnsubmit = $(".submit");

    $(".step, .back, .selesai").hide();
    $("#widget_1").show();
});

function getFormData($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};
    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });
    return indexed_array;
}

function buka(id_widget) {
    $(".next").attr('rel', (id_widget + 1));
    $(".back").attr('rel', (id_widget - 1));
    $(".ragu_ragu").attr('rel', (id_widget));
    cek_status_ragu(id_widget);
    cek_terakhir(id_widget);

    $("#soalke").html(id_widget);

    $(".step").hide();
    $("#widget_" + id_widget).show();

    simpan();
}

function next() {
    var berikutnya = $(".next").attr('rel');
    berikutnya = parseInt(berikutnya);
    berikutnya = berikutnya > total_widget ? total_widget : berikutnya;

    $("#soalke").html(berikutnya);

    $(".next").attr('rel', (berikutnya + 1));
    $(".back").attr('rel', (berikutnya - 1));
    $(".ragu_ragu").attr('rel', (berikutnya));
    cek_status_ragu(berikutnya);
    cek_terakhir(berikutnya);

    var sudah_akhir = berikutnya == total_widget ? 1 : 0;

    $(".step").hide();
    $("#widget_" + berikutnya).show();

    if (sudah_akhir == 1) {
        $(".back").show();
        $(".next").hide();
    } else if (sudah_akhir == 0) {
        $(".next").show();
        $(".back").show();
    }

    simpan();
}

function back() {
    var back = $(".back").attr('rel');
    back = parseInt(back);
    back = back < 1 ? 1 : back;

    $("#soalke").html(back);

    $(".back").attr('rel', (back - 1));
    $(".next").attr('rel', (back + 1));
    $(".ragu_ragu").attr('rel', (back));
    cek_status_ragu(back);
    cek_terakhir(back);

    $(".step").hide();
    $("#widget_" + back).show();

    var sudah_awal = back == 1 ? 1 : 0;

    $(".step").hide();
    $("#widget_" + back).show();

    if (sudah_awal == 1) {
        $(".back").hide();
        $(".next").show();
    } else if (sudah_awal == 0) {
        $(".next").show();
        $(".back").show();
    }

    simpan();
}

function tidak_jawab() {
    var id_step = $(".ragu_ragu").attr('rel');
    var status_ragu = $("#rg_" + id_step).val();

    if (status_ragu == "N") {
        $("#rg_" + id_step).val('Y');
        $("#btn_soal_" + id_step).removeClass('btn-success');
        $("#btn_soal_" + id_step).addClass('btn-warning');

    } else {
        $("#rg_" + id_step).val('N');
        $("#btn_soal_" + id_step).removeClass('btn-warning');
        $("#btn_soal_" + id_step).addClass('btn-success');
    }

    cek_status_ragu(id_step);

    simpan();
}

function cek_status_ragu(id_soal) {
    var status_ragu = $("#rg_" + id_soal).val();

    if (status_ragu == "N") {
        $(".ragu_ragu").html('Ragu');
    } else {
        $(".ragu_ragu").html('Tidak Ragu');
    }
}

function cek_terakhir(id_soal) {
    var jml_soal = $("#jml_soal").val();
    jml_soal = (parseInt(jml_soal) - 1);

    if (jml_soal === id_soal) {
        $('.next').hide();
        $(".selesai, .back").show();
    } else {
        $('.next').show();
        $(".selesai, .back").hide();
    }
}

function simpan_sementara() {
    var f_asal = $("#ujian");
    var form = getFormData(f_asal);
   
    var jml_soal = form.jml_soal;
    jml_soal = parseInt(jml_soal);
   
    var hasil_jawaban = "";
    var nomor=0;
    for (var i = 1; i < jml_soal; i++) {
		nomor++;
		  if(nomor <10){
			  nomor = "0"+nomor;
		  }
        var idx = 'jawaban_' + i;
        var idx2 = 'rg_' + i;
        var jawab = form[idx];
        var ragu = form[idx2];
      
          if(i % 3==1){
			  
			  hasil_jawaban +='<br>';
		  }
        if (jawab != undefined) {
            if (ragu == "Y") {
                if (jawab == "-") {
                    hasil_jawaban += '<a id="btn_soal_' + (i) + '" class="btn btn-default btn_soal btn-sm" style="color:white" onclick="return buka(' + (i) + ');">' + (nomor) + ". " + jawab + "</a>";
                } else {
                    hasil_jawaban += '<a id="btn_soal_' + (i) + '" class="btn btn-warning btn_soal btn-sm" style="color:white" onclick="return buka(' + (i) + ');">' + (nomor) + ". " + jawab + "</a>";
                }
            } else {
                if (jawab == "-") {
                    hasil_jawaban += '<a id="btn_soal_' + (i) + '" class="btn btn-default btn_soal btn-sm" style="color:white" onclick="return buka(' + (i) + ');">' + (nomor) + ". " + jawab + "</a>";
                } else {
                    hasil_jawaban += '<a id="btn_soal_' + (i) + '" class="btn btn-success btn_soal btn-sm" style="color:white" onclick="return buka(' + (i) + ');">' + (nomor) + ". " + jawab + "</a>";
                }
            }
        } else {
			
			
            hasil_jawaban += '<a id="btn_soal_' + (i) + '" class="btn btn-default btn_soal btn-sm"  style="color:white" onclick="return buka(' + (i) + ');">' + (nomor) + ". -</a>";
        }
      
    }
    $("#tampil_jawaban").html('<div id="yes"></div>' + hasil_jawaban);
  
}

 $(document).off("click",".ayolah").on("click",".ayolah",function(){
    
     var nomor = $(this).attr("nomor");

       $("#btn_soal_"+nomor).addClass('btn-success').removeClass('btn-default');

 });


function simpan() {
    

    var form = $("#ujian");

    $.ajax({
        type: "POST",
        url: base_url + "wawancara/simpan_satu",
        data: form.serialize(),
        dataType: 'json',
        success: function (data) {
            // $('.ajax-loading').show();
            console.log(data);
        }
    });
}


function selesai() {
    simpan();
    
    $.ajax({
        type: "POST",
        url: base_url + "wawancara/simpan_akhir",
        data: { id: id_tes },
        beforeSend: function () {
            simpan();
            // $('.ajax-loading').show();    
        },
        success: function (r) {
            
            window.location.href = base_url + 'wawancara';
          
        }
    });
}


function simpan_akhir() {
 
    simpan();   
			Swal.fire({
			  title: 'Apakah Anda yakin mengakhiri wawancara  ?',
			  text: "silahkan pilih dibawah ini ",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Ya',
			  cancelButtonText: 'Tidak'
			}).then((result) => {
			  if (result.value) {
				  
				   Swal.fire(
				  'Terimakasih !',
				  'Anda sudah mengakhiri wawancara .',
				  'success'
				);
				
				
				   selesai();
				   
				   
				
			  }
			})

	
    
}



</script>



<script type="text/javascript">
// VIdeo con
$("#selesaikanwawancara").show();
StartMeeting();
var apiObj = null;
var base_url = "<?php echo base_url(); ?>";
function StartMeeting(){
	
	

    const domain = 'meet.jit.si';

    var  id   = "<?php echo $data->no_test; ?>";
    
    const options = {
        roomName: "<?php echo $data->no_test; ?>",
        width: '100%',
        height: 500,
        parentNode: document.querySelector('#jitsi-meet-conf-container'),
        userInfo: {
            displayName: '<?php echo $_SESSION['nama_aksi']; ?>'
        },
         configOverwrite:{
            
            startWithAudioMuted: true,
            enableWelcomePage: false,           
            disableRemoteMute: true
            
        },
		
		
          interfaceConfigOverwrite: {
             filmStripOnly: false,
             SHOW_JITSI_WATERMARK: false,
             SHOW_WATERMARK_FOR_GUESTS: false,
			},
        onload: function () {
            $.post("<?php echo site_url('wawancara/mulai_meeting'); ?>",{id:id},function(data){
				
				alertify.success("Wawancara dimulai");
				
			})
        }
    };
    apiObj = new JitsiMeetExternalAPI(domain, options);

    apiObj.addEventListeners({
        readyToClose: function () {
             $.post("<?php echo site_url('wawancara/mulai_meeting'); ?>",{id:id},function(data){
				
				alertify.alert("Terimakasih, Anda menutup video call ini");
				
				
			})
        }
    });

    apiObj.executeCommand('subject', '<?php echo $vc->nama; ?>');
	apiObj.executeCommand('avatarUrl', '<?php echo base_url(); ?>__statics/img/logomadrasah.png');
}

function HangupCall(){
    apiObj.executeCommand('hangup');
}

</script>