<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wawancara extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		  if(!$this->session->userdata("users_id")){
			    
				echo $this->Reff->sessionhabis();
				exit();
			
		  }
		  $this->load->model('M_dashboard','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('wawancara/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Pewawancara ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page_default',$data);
		
		 }else{
			 
			
		     $data['konten'] = "page_default";
			 
			 $this->_template($data);
		 }
	

	}
	public function caripeserta(){

		$no_test = $this->input->get_post("no_test");
		$cek     = $this->db->get_where("tm_siswa",array("no_test"=>$no_test))->row();

		  if(!is_null($cek)){

			echo $cek->id;
		  }else{

			echo "0";
		  }
	}



	public function mulai()
	{
		
		$id = $this->input->get_post("id");
		$tmujian_id = 1;
		$siswa      = $this->db->get_where("tm_siswa",array("id"=>$id))->row();
		
		$ujian 		= $this->m->getUjianById($tmujian_id);
		$soal 		= $this->m->getSoal($tmujian_id);
		   
		
		$ujian 		= $this->m->getUjianById($tmujian_id);
		$soal 		= $this->m->getSoal($tmujian_id);
		
		
		
		

		$h_ujian 	= $this->m->HslUjian($tmujian_id,$siswa->id);
	
		$cek_sudah_ikut = $h_ujian->num_rows(); 

		if ($cek_sudah_ikut < 1) {
			$soal_urut_ok 	= array();
			$i = 0;
			foreach ($soal as $s) {
				$soal_per = new stdClass();
				$soal_per->id_soal 		= $s->id_soal;
				$soal_per->soal 		= $s->soal;
				$soal_per->file 		= $s->file;				
				$soal_per->opsi_a 		= $s->opsi_a;
				$soal_per->opsi_b 		= $s->opsi_b;
				$soal_per->opsi_c 		= $s->opsi_c;
				$soal_per->opsi_d 		= $s->opsi_d;
				$soal_per->opsi_e 		= $s->opsi_e;
				$soal_per->jawaban 		= $s->jawaban;
				$soal_urut_ok[$i] 		= $soal_per;
				$i++;
			}
			$soal_urut_ok 	= $soal_urut_ok;
			$list_id_soal	= "";
			$list_jw_soal 	= "";
			if (!empty($soal)) {
				foreach ($soal as $d) {
					$list_id_soal .= $d->id_soal.",";
					$list_jw_soal .= $d->id_soal.":::,";
				}
			}
			$list_id_soal 	= substr($list_id_soal, 0, -1);
			$list_jw_soal 	= substr($list_jw_soal, 0, -1);
			
			$time_mulai		= date('Y-m-d H:i:s');

			//print_r($list_jw_soal); exit();

			$device = $this->Reff->mobile();


			$input = [
				'tmujian_id' 		=> $tmujian_id,	
				'tmsiswa_id'	=> $siswa->id,
				'list_soal'		=> $list_id_soal,
				'list_jawaban' 	=> $list_jw_soal,
				'jml_benar'		=> 0,
				'nilai'			=> 0,
				'nilai_bobot'	=> 0,
				'tgl_mulai'		=> $time_mulai,
				
				'pewawancara'	    => $_SESSION['users_id'],
				'status'		=> 'Y'
			];
			
			
			$this->db->insert('h_ujian_w', $input);

			redirect('wawancara/mulai?id='.($id), 'location', 301);
		}
		
		$q_soal = $h_ujian->row();
		
		$urut_soal 		= explode(",", $q_soal->list_jawaban);
		$soal_urut_ok	= array();
		for ($i = 0; $i < sizeof($urut_soal); $i++) {
			$pc_urut_soal	= explode(":",$urut_soal[$i]);
			$pc_urut_soal1 	= empty($pc_urut_soal[1]) ? "''" : "'{$pc_urut_soal[1]}'";
			$ambil_soal 	= $this->m->ambilSoal($pc_urut_soal1, $pc_urut_soal[0]);
			$soal_urut_ok[] = $ambil_soal; 
		}

		$detail_tes = $q_soal;
		$soal_urut_ok = $soal_urut_ok;

		$pc_list_jawaban = explode(",", $detail_tes->list_jawaban);
		$arr_jawab = array();
		
		foreach ($pc_list_jawaban as $v) {
			$pc_v 	= explode(":", $v);
			$idx 	= $pc_v[0];
			$val 	= $pc_v[1];
			$rg 	= $pc_v[2];

			$arr_jawab[$idx] = array("j"=>$val,"r"=>$rg);
		}








		$arr_opsi = array("a","b","c","d","e");
		$html = '';
			
		$no = 1;
		//print_r($soal_urut_ok); exit();
		if (!empty($soal)) {
			foreach ($soal as $s) {
				$path = $s->folder."/";
				$jwbn       = isset($h_jawaban) ? $h_jawaban :"";
				$vrg = $arr_jawab[$s->id_soal]["r"] == "" ? "N" : $arr_jawab[$s->id_soal]["r"];
				$jwbn = $arr_jawab[$s->id_soal]["j"] == "" ? "" : $arr_jawab[$s->id_soal]["j"];
				$ket  = $arr_jawab[$s->id_soal]["r"] == "" ? "" : $arr_jawab[$s->id_soal]["r"];
				
				$html .= '<input type="hidden" name="id_soal_'.$no.'" value="'.$s->id_soal.'">';
				$html .= '<input type="hidden" name="jenis_'.$no.'" value="'.$s->jenis.'">';
				$html .= '<input type="hidden" name="jwbn'.$no.'" value="'.$jwbn.'">';
				$html .= '<input type="hidden" name="rg_'.$no.'" id="rg_'.$no.'" value="'.$vrg.'">';
				$html .= '<div class="step" id="widget_'.$no.'">';

				$html .= '<div class="papers"><b>'.$this->Reff->get_kondisi(array("id"=>$s->jenis),"jenis_wawancara","nama").'</b><hr><div class="text-center"></div>'.$s->soal.'</div>';
				
			
					$html .= '<hr><div class="funkyradio">';
						for ($j = 0; $j < 5; $j++) {
							$opsi 			= "opsi_".$arr_opsi[$j];
							
							
							$pilihan_opsi 	= !empty($s->$opsi) ? trim($s->$opsi) : "";
							if($pilihan_opsi !=""){
								$checked 		= "";
							
							 if($jwbn==strtoupper($arr_opsi[$j])){
								$checked = "checked";
							

							 }
							$html .= '<div class="funkyradio-success" onclick="return simpan_sementara();" style="color:black">
								<input type="radio" id="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'"   name="jawaban_'.$no.'"  nomor="'.$no.'" value="'.strtoupper($arr_opsi[$j]).'" '.$checked.'> <label for="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'"><div class="huruf_opsi" style="color:black">'.$arr_opsi[$j].'</div> <p style="color:black">'.$pilihan_opsi.'</p></label></div>';
							}
						}
						
				
			
						
					
				
				
				
				$html .= '</div></div>';
				$no++;
			}
		}

		$id_tes = ($detail_tes->id);

		$data = [
			
			'judul'		=> 'wawancara',
			'subjudul'	=> 'Lembar wawancara',
			'soal'		=> $detail_tes,
			'no' 		=> $no,
			'html' 		=> $html,
			'id_tes'	=> $id_tes,
			'waktu'  	=> $detail_tes->waktu
		];
		 $data['data']	      = $siswa;
		 $data['ujian']	      = $this->Reff->get_where("tm_ujian",array("id"=>$id));
		 $data['title']	      = "WAWANCARA - ".$data['ujian']->nama;
		
		 $data['konten'] = "wawancara/cbt/sheet";
			 
		 $this->_template($data);
		 
		 
	}


	public function simpan_satu()
	{
	
		$id_tes = $this->input->post('id', true);
		$id_tes = ($id_tes);
		
		$input 	= $this->input->post(null, true);
		$list_jawaban 	= "";
		for ($i = 1; $i < $input['jml_soal']; $i++) {
			$_tjawab 	= "jawaban_".$i;
			$_tidsoal 	= "id_soal_".$i;
			$_ragu 		= "rg_".$i;
			$jawaban_ 	= empty($input[$_tjawab]) ? "" : $input[$_tjawab];
			$list_jawaban	.= "".$input[$_tidsoal].":".$jawaban_.":".$input[$_ragu].",";
		}
		$list_jawaban	= substr($list_jawaban, 0, -1);
		$d_simpan = [
			'list_jawaban' => $list_jawaban
		];
	
		$this->db->update('h_ujian_w', $d_simpan,array("id"=>$id_tes));
		$this->output_json(['status'=>true]);
	
	}

	public function simpan_akhir()
	{
		// Decrypt Id
		$this->Reff->zona_waktu();
		
	
		$id_tes = $this->input->post('id', true);
		$id_tes = ($id_tes);
		$list_jawaban = $this->m->getJawaban($id_tes);
		
		// Pecah Jawaban
		$pc_jawaban = explode(",", $list_jawaban);
		
		$jumlah_benar 	= 0;
		$jumlah_salah 	= 0;
		$jumlah_ragu  	= 0;
		$nilai_bobot 	= 0;
		$total_bobot	= 0;
		$jumlah_soal	= sizeof($pc_jawaban);

		foreach ($pc_jawaban as $jwb) {
			$pc_dt 		= explode(":", $jwb);
			$id_soal 	= $pc_dt[0];
			$jawaban 	= $pc_dt[1];
			$ragu 		= $pc_dt[2];

			
			
				$arr_nilai = array("A"=>"5","B"=>"4","C"=>"3","D"=>"2","E"=>"1");
				$nilai_bobot = $nilai_bobot + $arr_nilai[$jawaban];
				
			
			
		}


		$d_update = [
			
			'tgl_selesai'	=> date("Y-m-d H:i:s"),
			'nilai'			=> $nilai_bobot,
			'nilai_bobot'	=> number_format(floor($nilai_bobot), 0),
			'status'		=> 'N'
		];
        
		$this->db->update('h_ujian_w', $d_update,array("id"=>$id_tes));



		
		$this->output_json(['status'=>TRUE, 'data'=>"sukses", 'id'=>$id_tes]);
	}
	
	
	 public function output_json($data, $encode = true)
	{
        if($encode) $data = json_encode($data);
        $this->output->set_content_type('application/json')->set_output($data);
	}
	
	
	public function mulai_meeting(){
		 $no_test = $this->input->get_post("id");

		 $cek = $this->db->query("select * from tr_vicon where no_test='".$no_test."'")->row();

		   if(is_null($cek)){

			$this->db->set("no_test",$no_test);
			$this->db->set("pewawancara",$_SESSION['users_id']);
			$this->db->set("mulai",date("Y-m-d H:i:s"));
			$this->db->insert("tr_vicon");

		   }else{

			$this->db->where("no_test",$no_test);
			$this->db->set("pewawancara",$_SESSION['users_id']);
			$this->db->set("mulai",date("Y-m-d H:i:s"));
			$this->db->set("selesai",date("Y-m-d H:i:s"));
			$this->db->update("tr_vicon");
		   }



	}
	public function logout(){
		     $this->session->sess_destroy();
			 
		
			echo "yes";
		 
	 }
	 
}
