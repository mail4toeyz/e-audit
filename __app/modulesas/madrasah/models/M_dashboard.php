<?php

class M_dashboard extends CI_Model {
 
    public function __construct() {
        parent::__construct();
    }

    
	public function visitasi_grid($paging){
       // $tahun   = $this->Reff->tahun(); 
        $tahun   = 2020;
	    $status = $this->input->get_post("status");
	    $jenjang = $this->input->get_post("jenjang");
	    $provinsi = $this->input->get_post("provinsi");
	    $keyword  = $this->input->get_post("keyword");
	    $bantuan  = $this->input->get_post("bantuan");

		$this->db->select("*");
        $this->db->from('madrasahedm');
        $this->db->where('id',$_SESSION['madrasah_id']);
    	
	    if(!empty($bantuan)){  $this->db->where("bantuan",$bantuan);    }
	    if(!empty($jenjang)){  $this->db->where("jenjang",$jenjang);    }
	    if(!empty($provinsi)){  $this->db->where("provinsi",$provinsi);    }
	    if(!empty($keyword)){  $this->db->where("(UPPER(nama) LIKE '%".strtoupper($keyword)."%' OR UPPER(nsm) LIKE '%".strtoupper($keyword)."%')");    }
		

	
		$this->db->where("shortlist",1);

		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("skor_akhir","DESC");
					 $this->db->order_by("nilai_skpm","DESC");
					
		
			 }
      
		
		
		return $this->db->get();
		
	}

	
	
	
	
	
}
