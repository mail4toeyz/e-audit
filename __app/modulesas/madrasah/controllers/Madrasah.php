<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Madrasah extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		  if(!$this->session->userdata("madrasah_id")){
			    
				echo $this->Reff->sessionhabis();
				exit();
			
		  }
		  $this->load->model('M_dashboard','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('madrasah/page_header',$data);	
	}
		
	public function index()
	{  
	     
		 $ajax            = $this->input->get_post("ajax",true);	
		 $data['title']   = "Madrasah BKBA";
		 $data['data']    =  $this->db->get_where("madrasahedm",array("id"=>$_SESSION['madrasah_id']))->row();
	     if(!empty($ajax)){
					    
			 $this->load->view('page_default',$data);
		
		 }else{
			 
			
		     $data['konten'] = "page_default";
			 
			 $this->_template($data);
		 }
	

	}

	public function profil()
	{  
	     
		 $ajax            = $this->input->get_post("ajax",true);	
		 $data['title']   = "Profil Asesor BKBA";
		 $data['data']    =  $this->db->get_where("madrasahedm",array("id"=>$_SESSION['madrasah_id']))->row();
	     if(!empty($ajax)){
					    
			 $this->load->view('profil/page',$data);
		
		 }else{
			 
			
		     $data['konten'] = "profil/page";
			 
			 $this->_template($data);
		 }
	

	}

	public function save_profile(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NUPTK lain.');
         $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
         $this->form_validation->set_message('numeric', '%s: Harus terdiri dari Angka');
				 
		 
			
				$config = array(
				    
				   
				    array('field' => 'f[nama]', 'label' => 'Nama Lengkap  ', 'rules' => 'trim|required'),
					array('field' => 'f[tempat_lahir]', 'label' => 'Tempat Lahir  ', 'rules' => 'trim|required'),
					array('field' => 'f[tanggal_lahir]', 'label' => 'Tanggal Lahir  ', 'rules' => 'trim|required'),
					array('field' => 'f[no_telepon]', 'label' => 'Nomor Telepon  ', 'rules' => 'trim|required'),
					array('field' => 'f[whatsapp]', 'label' => 'Whatsapp  ', 'rules' => 'trim|required'),
					array('field' => 'f[email]', 'label' => 'Email  ', 'rules' => 'trim|required'),
				   
				   
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    
			    $f  = ($this->input->get_post("f",true));

				$config['upload_path']      = './__statics/upload/foto';
				$folder   					= '__statics/upload/foto';
		        $config['allowed_types']    = "jpg|jpeg|png"; 	
				$config['overwrite']        = true; 
                $config['max_size']         = '2200';				
				$filenamepecah			    = explode(".",$_FILES['file']['name']);
				$imagenew				    = "logo".time().".".pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
				$config['file_name'] = $imagenew;
				
				
				
				   $this->load->library('upload', $config);

						if ( ! $this->upload->do_upload('file'))
						{
							
							$this->db->where("id",$_SESSION['peserta_id']);
							$this->db->update("asesor",$f);
						
						}else{


							$this->db->set("foto",$imagenew);
							$this->db->where("id",$_SESSION['peserta_id']);
							$this->db->update("asesor",$f);

						}
				
				
							   
							
						$this->Reff->log($_SESSION['nama']. " Memperbaharui Data Diri pada ".formattimestamp(date("Y-m-d H:i:s"))."","2",$_SESSION['peserta_id'],$_SESSION['peserta_id']);			
	
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}

	// surtug

	public function surtug()
	{  
	     
		 $ajax            = $this->input->get_post("ajax",true);	
		 $data['title']   = "Profil Asesor BKBA";
		 $data['data']    =  $this->db->get_where("asesor",array("id"=>$_SESSION['peserta_id']))->row();
	     if(!empty($ajax)){
					    
			 $this->load->view('profil/surtug',$data);
		
		 }else{
			 
			
		     $data['konten'] = "profil/surtug";
			 
			 $this->_template($data);
		 }
	

	}

	public function save_surtug(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NUPTK lain.');
         $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
         $this->form_validation->set_message('numeric', '%s: Harus terdiri dari Angka');
				 
		 
			
				$config = array(
				    
				   
				
					array('field' => 'id', 'label' => 'ID  ', 'rules' => 'trim|required'),
				   
				   
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    
			  

				$config['upload_path']      = './__statics/upload/foto';
				$folder   					= '__statics/upload/foto';
		        $config['allowed_types']    = "pdf"; 	
				$config['overwrite']        = true; 
                $config['max_size']         = '2200';				
				$filenamepecah			    = explode(".",$_FILES['file']['name']);
				$imagenew				    = "surattugas".time().".".pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
				$config['file_name'] = $imagenew;
				
				
				
				   $this->load->library('upload', $config);

						if ( ! $this->upload->do_upload('file'))
						{
							
						
						
						}else{


							$this->db->set("surattugas",$imagenew);
							$this->db->where("id",$_SESSION['peserta_id']);
							$this->db->update("asesor");

						}
				
				
							   
							
						$this->Reff->log($_SESSION['nama']. " Memperbaharui Surat Tugas pada ".formattimestamp(date("Y-m-d H:i:s"))."","2",$_SESSION['peserta_id'],$_SESSION['peserta_id']);			
	
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}

	public function akun()
	{  
	     
		 $ajax            = $this->input->get_post("ajax",true);	
		 $data['title']   = "Akun Asesor BKBA";
		 $data['data']    =  $this->db->get_where("madrasahedm",array("id"=>$_SESSION['madrasah_id']))->row();
	     if(!empty($ajax)){
					    
			 $this->load->view('profil/akun',$data);
		
		 }else{
			 
			
		     $data['konten'] = "profil/akun";
			 
			 $this->_template($data);
		 }
	

	}

	public function save_akun(){
     
		$this->form_validation->set_message('required', '{field} Wajib diisi. ');
		$this->form_validation->set_message('is_unique', '%s: Sudah terdaftar ');
		$this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
		$this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
		$id   = $this->input->get_post("id",true);
	
		   
			   $config = array(
				   
				array('field' => 'password', 'label' => '  Password  ', 'rules' => 'trim|required|min_length[6]'),
				array('field' => 'cpassword', 'label' => ' Konfirmasi Password', 'rules' => 'trim|required|matches[password]'),
			  
				 
				  
   
				   
			   );
			   
			   $this->form_validation->set_rules($config);	
	   
	   if ($this->form_validation->run() == true) {
			 
		
			  
			  
			            $password           = ($this->input->get_post("password",true));
						   
						 $this->db->set("password",(($password)));						
						 $this->db->where("id",$_SESSION['madrasah_id']);
						 $this->db->update("madrasahedm");
						 echo "sukses";
					
												
		} else {
			
		  
			   header('Content-Type: application/json');
			   echo json_encode(array('error' => true, 'message' => validation_errors()));
		   
	   }

	   
   
   
   }
	

   // Visitasi Madrasah 

   public function visitasi()
	{  
	     
		 $ajax            = $this->input->get_post("ajax",true);	
		 $data['title']   = "Visitasi Madrasah ";
		 $data['data']    =  $this->db->get_where("madrasahedm",array("id"=>$_SESSION['madrasah_id']))->row();
	     if(!empty($ajax)){
					    
			 $this->load->view('visitasi/page',$data);
		
		 }else{
			 
			
		     $data['konten'] = "visitasi/page";
			 
			 $this->_template($data);
		 }
	

	}

	public function visitasi_grid(){
		error_reporting(0);
		$iTotalRecords = $this->m->visitasi_grid(false)->num_rows();
		
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);
		
		$records = array();
		$records["data"] = array(); 

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;
		
		$datagrid = $this->m->visitasi_grid(true)->result_array();
		$arbantuan = array("1"=>"Kinerja","2"=>"Afirmasi");
		
		 $i= ($iDisplayStart +1);
		 $status =  array("0"=>"<span class='fa fa-times' style='color:red'></span>","1"=>"<span class='fa fa-check-square-o primary' style='color:blue'></span>");
		 
		 
		 foreach($datagrid as $val) {
			  
			  $no        = $i++;
			  $edm       = $this->db->query("select id from visitasi_edm where madrasah_id='{$val['id']}'  AND nilai_kedisiplinan IS NOT NULL AND nilai_pengembangan_diri IS NOT NULL AND nilai_sarana_prasarana IS NOT NULL  AND nilai_pembiayaan IS NOT NULL AND nilai_proses_pembelajaran IS NOT NULL LIMIT 1")->num_rows();
			  $pd        = $this->db->query("select id from visitasi_pesertadidik where madrasah_id='{$val['id']}'  AND total !=0 LIMIT 1")->num_rows();
			  $rombel    = $this->db->query("select id from visitasi_rombel where  madrasah_id='{$val['id']}'  AND total !=0 LIMIT 1")->num_rows();
			  $guru      = $this->db->query("select id from visitasi_guru where madrasah_id='{$val['id']}'   LIMIT 1")->num_rows();
			  $ruangbelajar   = $this->db->query("select id from visitasi_ruangbelajar where  madrasah_id='{$val['id']}'  AND total !=0 LIMIT 1")->num_rows();
			  $toilet         = $this->db->query("select id from visitasi_toilet where madrasah_id='{$val['id']}'   LIMIT 1")->num_rows();
			  $tamuk          = $this->db->query("select id from visitasi_tatapmuka where madrasah_id='{$val['id']}' and kegiatan_id !=0  LIMIT 1")->num_rows();
			  $kesesuaian     = $this->db->query("select id from visitasi_tatapmuka where madrasah_id='{$val['id']}' and kegiatan_id =0   LIMIT 1")->num_rows();
		

			  $records["data"][] = array(
				  $no,
				  
				  '<button href="'.site_url('madrasah/visitasiAct?id='.$val['id'].'').'" class="btn btn-primary btn-shadow-hover font-weight-bold mr-2 menuajax" type="button"><i class="fa fa-tasks"></i>Visitasi </button>',
				  strtoupper($val['jenjang']),
				  $val['nsm'],
				  $val['nama'],
				  $val['akreditasi'],
				  $val['provinsi'],
				  $val['kota'],
				  "<span class='badge badge-primary'>".$arbantuan[$val['bantuan']]."</span>",
				  $status[$edm],
				  $status[$pd],				 
				  $status[$rombel],				 
				  $status[$guru],				 
				  $status[$ruangbelajar],				 
				  $status[$toilet],				 
				  $status[$tamuk],
				  $status[$kesesuaian]
				 
				  
				  
				 

				);
			}
	  
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		
		echo json_encode($records);
  }

  public function visitasiAct(){
    $id            = $this->input->get_post("id",true);
	$data['data']    =  $this->db->get_where("madrasahedm",array("id"=>$_SESSION['madrasah_id']))->row();

	$ajax            = $this->input->get_post("ajax",true);	
		 $data['title']   = "Hasil Visitasi";
		
	     if(!empty($ajax)){
					    
			 $this->load->view('visitasi/visitasi',$data);
		
		 }else{
			 
			
		     $data['konten'] = "visitasi/visitasi";
			 
			 $this->_template($data);
		 }


  }

  public function saveVisitasiEDM(){

	  $kolom       = $this->input->post("kolom");
	  $madrasah_id = $this->input->post("madrasah_id");
	  $nsm         = $this->input->post("nsm");
	  $eksis       = $this->input->post("eksis");
	  $nilai       = $this->input->post("nilai");
	  if(empty($nilai)){ $nilai=0; }

	  $cek = $this->db->query("select id from visitasi_edm where madrasah_id='{$madrasah_id}'")->num_rows();
	  if($cek==0){

		  $this->db->set("madrasah_id",$madrasah_id);
		  $this->db->set("nsm",$nsm);
		  $this->db->set($kolom,$nilai);
		  $this->db->set("asesor",$_SESSION['peserta_id']);
		  $this->db->insert("visitasi_edm");

		  echo number_format((($nilai/$eksis) * 100),2)."%";

	  }else{

		  $this->db->where("madrasah_id",$madrasah_id);
		  $this->db->where("nsm",$nsm);
		  $this->db->set($kolom,$nilai);
		  $this->db->update("visitasi_edm");

		  echo number_format((($nilai/$eksis) * 100),2)."%";

	  }
  }


  public function saveVisitasiPD(){

	$kolom       = $this->input->post("kolom");
	$madrasah_id = $this->input->post("madrasah_id");
	$nsm         = $this->input->post("nsm");
	$eksis       = $this->input->post("eksis");
	$nilai       = $this->input->post("nilai");
	if(empty($nilai)){ $nilai=0; }

	$cek = $this->db->query("select id from visitasi_pesertadidik where madrasah_id='{$madrasah_id}'")->num_rows();
	if($cek==0){
		$data  = $this->db->query("select (kelas_1+kelas_2+kelas_3+kelas_4+kelas_5+kelas_6+kelas_7+kelas_8+kelas_9+kelas_10+kelas_11+kelas_12) as total from visitasi_pesertadidik where madrasah_id='{$madrasah_id}'")->row();
		$total = isset($data->total) ? ($data->total + $nilai) : $nilai;
		$this->db->set("madrasah_id",$madrasah_id);
		$this->db->set("nsm",$nsm);
		$this->db->set($kolom,$nilai);
		$this->db->set("total",$total);
		$this->db->set("asesor",$_SESSION['peserta_id']);
		$this->db->insert("visitasi_pesertadidik");
		echo  number_format((($total/$eksis) * 100),2)."%";

		

	}else{
		$data  = $this->db->query("select (kelas_1+kelas_2+kelas_3+kelas_4+kelas_5+kelas_6+kelas_7+kelas_8+kelas_9+kelas_10+kelas_11+kelas_12) as total from visitasi_pesertadidik where madrasah_id='{$madrasah_id}'")->row();
		$total = $data->total + $nilai;

		$this->db->where("madrasah_id",$madrasah_id);
		$this->db->where("nsm",$nsm);
		$this->db->set($kolom,$nilai);
		$this->db->set("total",$total);
		$this->db->update("visitasi_pesertadidik");

		echo  number_format((($total/$eksis) * 100),2)."%";

	}
}
	 

public function saveVisitasirombel(){

	$kolom       = $this->input->post("kolom");
	$madrasah_id = $this->input->post("madrasah_id");
	$nsm         = $this->input->post("nsm");
	$eksis       = $this->input->post("eksis");
	$nilai       = $this->input->post("nilai");
	if(empty($nilai)){ $nilai=0; }

	$cek = $this->db->query("select id from visitasi_rombel where madrasah_id='{$madrasah_id}'")->num_rows();
	if($cek==0){
		$data  = $this->db->query("select (kelas_1+kelas_2+kelas_3+kelas_4+kelas_5+kelas_6+kelas_7+kelas_8+kelas_9+kelas_10+kelas_11+kelas_12) as total from visitasi_rombel where madrasah_id='{$madrasah_id}'")->row();
		$total = isset($data->total) ? ($data->total + $nilai) : $nilai;
		$this->db->set("madrasah_id",$madrasah_id);
		$this->db->set("nsm",$nsm);
		$this->db->set($kolom,$nilai);
		$this->db->set("total",$total);
		$this->db->set("asesor",$_SESSION['peserta_id']);
		$this->db->insert("visitasi_rombel");
		echo  number_format((($total/$eksis) * 100),2)."%";

		

	}else{
		$data  = $this->db->query("select (kelas_1+kelas_2+kelas_3+kelas_4+kelas_5+kelas_6+kelas_7+kelas_8+kelas_9+kelas_10+kelas_11+kelas_12) as total from visitasi_rombel where madrasah_id='{$madrasah_id}'")->row();
		$total = $data->total + $nilai;

		$this->db->where("madrasah_id",$madrasah_id);
		$this->db->where("nsm",$nsm);
		$this->db->set($kolom,$nilai);
		$this->db->set("total",$total);
		$this->db->update("visitasi_rombel");

		echo  number_format((($total/$eksis) * 100),2)."%";

	}
}

public function saveVisitasiGuru(){

	$kolom       = $this->input->post("kolom");
	$madrasah_id = $this->input->post("madrasah_id");
	$nsm         = $this->input->post("nsm");
	$eksis       = $this->input->post("eksis");
	$nilai       = $this->input->post("nilai");
	if(empty($nilai)){ $nilai=0; }

	$cek = $this->db->query("select id from visitasi_guru where madrasah_id='{$madrasah_id}'")->num_rows();
	if($cek==0){

		$this->db->set("madrasah_id",$madrasah_id);
		$this->db->set("nsm",$nsm);
		$this->db->set($kolom,$nilai);
		$this->db->set("asesor",$_SESSION['peserta_id']);
		$this->db->insert("visitasi_guru");

		$total = $this->db->query("select (guru_tetap+guru_tidak_tetap) as total from visitasi_guru where madrasah_id='{$madrasah_id}'")->row();

		echo number_format((($total->total/$eksis) * 100),2)."%";

	}else{

		$this->db->where("madrasah_id",$madrasah_id);
		$this->db->where("nsm",$nsm);
		$this->db->set($kolom,$nilai);
		$this->db->update("visitasi_guru");

		$total = $this->db->query("select (guru_tetap+guru_tidak_tetap) as total from visitasi_guru where madrasah_id='{$madrasah_id}'")->row();

		echo number_format((($total->total/$eksis) * 100),2)."%";

	}
}



public function saveVisitasiruangbelajar(){

	$kolom       = $this->input->post("kolom");
	$madrasah_id = $this->input->post("madrasah_id");
	$nsm         = $this->input->post("nsm");
	$eksis       = $this->input->post("eksis");
	$nilai       = $this->input->post("nilai");
	if(empty($nilai)){ $nilai=0; }

	$cek = $this->db->query("select id from visitasi_ruangbelajar where madrasah_id='{$madrasah_id}'")->num_rows();
	if($cek==0){
		$data  = $this->db->query("select (kelas_1+kelas_2+kelas_3+kelas_4+kelas_5+kelas_6+kelas_7+kelas_8+kelas_9+kelas_10+kelas_11+kelas_12) as total from visitasi_ruangbelajar where madrasah_id='{$madrasah_id}'")->row();
		$total = isset($data->total) ? ($data->total + $nilai) : $nilai;
		$this->db->set("madrasah_id",$madrasah_id);
		$this->db->set("nsm",$nsm);
		$this->db->set($kolom,$nilai);
		$this->db->set("total",$total);
		$this->db->set("asesor",$_SESSION['peserta_id']);
		$this->db->insert("visitasi_ruangbelajar");
		echo  number_format((($total/$eksis) * 100),2)."%";

		

	}else{
		$data  = $this->db->query("select (kelas_1+kelas_2+kelas_3+kelas_4+kelas_5+kelas_6+kelas_7+kelas_8+kelas_9+kelas_10+kelas_11+kelas_12) as total from visitasi_ruangbelajar where madrasah_id='{$madrasah_id}'")->row();
		$total = $data->total + $nilai;

		$this->db->where("madrasah_id",$madrasah_id);
		$this->db->where("nsm",$nsm);
		$this->db->set($kolom,$nilai);
		$this->db->set("total",$total);
		$this->db->update("visitasi_ruangbelajar");

		echo  number_format((($total/$eksis) * 100),2)."%";

	}
}



public function saveVisitasiToilet(){

	$kolom       = $this->input->post("kolom");
	$madrasah_id = $this->input->post("madrasah_id");
	$nsm         = $this->input->post("nsm");
	$eksis       = $this->input->post("eksis");
	$nilai       = $this->input->post("nilai");
	if(empty($nilai)){ $nilai=0; }

	$cek = $this->db->query("select id from visitasi_toilet where madrasah_id='{$madrasah_id}'")->num_rows();
	if($cek==0){

		$this->db->set("madrasah_id",$madrasah_id);
		$this->db->set("nsm",$nsm);
		$this->db->set($kolom,$nilai);
		$this->db->set("asesor",$_SESSION['peserta_id']);
		$this->db->insert("visitasi_toilet");

		$total = $this->db->query("select (guru+siswa+siswi) as total from visitasi_toilet where madrasah_id='{$madrasah_id}'")->row();

		

		if($total->total !=0 and $eksis !=0 ){
			echo number_format((($total->total/$eksis) * 100),2)."%";

		}else if($total->total ==0 and $eksis !=0){
		 echo number_format((($total->total/$eksis) * 100),2)."%";
        }else{
            echo   "100%";

        }

	}else{

		$this->db->where("madrasah_id",$madrasah_id);
		$this->db->where("nsm",$nsm);
		$this->db->set($kolom,$nilai);
		$this->db->update("visitasi_toilet");

		$total = $this->db->query("select (guru+siswa+siswi) as total from visitasi_toilet where madrasah_id='{$madrasah_id}'")->row();

		if($total->total !=0 and $eksis !=0 ){
			echo number_format((($total->total/$eksis) * 100),2)."%";

		}else if($total->total ==0 and $eksis !=0){
		 echo number_format((($total->total/$eksis) * 100),2)."%";
        }else{
            echo   "100%";

        }

	}
}

  
public function saveVisitasiTatapmuka(){

	$kegiatan    = $this->input->post("kegiatan");
	$madrasah_id = $this->input->post("madrasah_id");
	$nsm         = $this->input->post("nsm");
	$nilai       = $this->input->post("nilai");
	$kegiatannama    = $this->Reff->get_kondisi(array("id"=>$kegiatan),"master_tatapmuka","nama");
	if(empty($nilai)){ $nilai=0; }

	$cek = $this->db->query("select kegiatan from visitasi_tatapmuka where madrasah_id='{$madrasah_id}' and kegiatan_id='{$kegiatan}'")->row();
	if(is_null($cek)){

		
		$this->db->set("madrasah_id",$madrasah_id);
		$this->db->set("nsm",$nsm);
		$this->db->set("kegiatan_id",$kegiatan);
		$this->db->set("kegiatan",$kegiatannama);
		$this->db->set("prioritas",$nilai);
		$this->db->set("asesor",$_SESSION['peserta_id']);
		$this->db->insert("visitasi_tatapmuka");

		

		

	}else{

		$this->db->where("madrasah_id",$madrasah_id);
		$this->db->where("nsm",$nsm);
		$this->db->where("kegiatan_id",$kegiatan);
		$this->db->set("kegiatan",$kegiatannama);
		$this->db->set("prioritas",$nilai);
		$this->db->update("visitasi_tatapmuka");
		

	}
}

public function erkam_save(){

	$this->db->set("jenis",$_POST['jenis']);
	$this->db->set("kegiatan",$_POST['kegiatan']);
	$this->db->set("id_keg",$_POST['id_keg']);
	$this->db->set("prioritas",$_POST['prioritas']);
	$this->db->set("nsm",$_POST['nsm']);
	

	$this->db->set("madrasah_id",$_POST['madrasah_id']);
	$this->db->insert("visitasi_tatapmuka");

	$data['id'] = $_POST['madrasah_id'];

	$this->load->view("visitasi/load_erkam",$data);


 }

 public function erkam_saveEdit(){

	//$cekViTam = $this->db->get_where("visitasi_tatapmuka",array("id"=>$_POST['id'],"madrasah_id"=>$_SESSION['madrasah_id']))->row();

	// if($cekViTam->s_update !=1){
	// 	$this->db->set("awal",$cekViTam->kegiatan);
	// 	$this->db->set("s_update",1);
	// }

	$kegiatan = $this->db->query("select * from list_kegiatan where id='{$_POST['kegiatan']}'")->row();

	$this->db->set("id_keg",$_POST['kegiatan']);
	$this->db->set("kegiatan",$kegiatan->sub_kegiatan);
	$this->db->set("prioritas",$_POST['prioritas']);
	$this->db->set("jenis",$_POST['jenis']);
	$this->db->where("id",$_POST['id']);
	$this->db->where("madrasah_id",$_SESSION['madrasah_id']);
	$this->db->update("visitasi_tatapmuka");


	// $cekViKeg= $this->db->get_where("visitasi_kegiatan",array("kegiatan_id"=>$_POST['id'],"madrasah_id"=>$_SESSION['madrasah_id']))->row();

	// if($cekViKeg->s_update !=1){
	// 	$this->db->set("awal",$cekViKeg->kegiatan);
	// 	$this->db->set("s_update",1);
	// }


	$this->db->set("kegiatan",($kegiatan->sub_kegiatan));
	$this->db->where("kegiatan_id",$_POST['id']);
	$this->db->where("madrasah_id",$_SESSION['madrasah_id']);
	$this->db->update("visitasi_kegiatan");

	$data['id'] = $_SESSION['madrasah_id'];

	$this->load->view("visitasi/load_erkam",$data);


 }

 public function erkam_hapus(){

   
   $this->db->where("id",$_POST['id']);
   $this->db->where("madrasah_id",$_POST['madrasah_id']);
   $this->db->delete("visitasi_tatapmuka");

   $data['id'] = $_POST['madrasah_id'];

   $this->load->view("visitasi/load_erkam",$data);


}
	

 // Dokumentasi 

 public function dokumentasi()
 {  
	  
	  $ajax            = $this->input->get_post("ajax",true);	
	  $data['title']   = "dokumentasi Madrasah ";
	  $data['data']    =  $this->db->get_where("asesor",array("id"=>$_SESSION['peserta_id']))->row();
	  if(!empty($ajax)){
					 
		  $this->load->view('dokumentasi/page',$data);
	 
	  }else{
		  
		 
		  $data['konten'] = "dokumentasi/page";
		  
		  $this->_template($data);
	  }
 

 }

 public function dokumentasi_grid(){
	 error_reporting(0);
	 $iTotalRecords = $this->m->visitasi_grid(false)->num_rows();
	 
	 $iDisplayLength = intval($_REQUEST['length']);
	 $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
	 $iDisplayStart = intval($_REQUEST['start']);
	 $sEcho = intval($_REQUEST['draw']);
	 
	 $records = array();
	 $records["data"] = array(); 

	 $end = $iDisplayStart + $iDisplayLength;
	 $end = $end > $iTotalRecords ? $iTotalRecords : $end;
	 
	 $datagrid = $this->m->visitasi_grid(true)->result_array();
	 $arbantuan = array("1"=>"Kinerja","2"=>"Afirmasi");
	 
	  $i= ($iDisplayStart +1);
	  $status =  array("0"=>"<span class='fa fa-times' style='color:red'></span>","1"=>"<span class='fa fa-check-square-o primary' style='color:blue'></span>");
	  
	  
	  foreach($datagrid as $val) {
		   
		   $no        = $i++;
		   $edm       = $this->db->query("select id from visitasi_edm where madrasah_id='{$val['id']}'  AND nilai_pembiayaan IS NOT NULL AND nilai_proses_pembelajaran IS NOT NULL LIMIT 1")->num_rows();
		   $pd        = $this->db->query("select id from visitasi_pesertadidik where madrasah_id='{$val['id']}'  AND total !=0 LIMIT 1")->num_rows();
		   $rombel    = $this->db->query("select id from visitasi_rombel where  madrasah_id='{$val['id']}'  AND total !=0 LIMIT 1")->num_rows();
		   $guru      = $this->db->query("select id from visitasi_guru where madrasah_id='{$val['id']}'   LIMIT 1")->num_rows();
		   $ruangbelajar   = $this->db->query("select id from visitasi_ruangbelajar where  madrasah_id='{$val['id']}'  AND total !=0 LIMIT 1")->num_rows();
		   $toilet         = $this->db->query("select id from visitasi_toilet where madrasah_id='{$val['id']}'   LIMIT 1")->num_rows();
		   $tamuk          = $this->db->query("select id from visitasi_tatapmuka where madrasah_id='{$val['id']}'   LIMIT 1")->num_rows();
	 
           
		   $records["data"][] = array(
			   $no,
			   
			   '<button href="'.site_url('asesor/documentasiAct?id='.$val['id'].'').'" class="btn btn-primary btn-shadow-hover font-weight-bold mr-2 menuajax" type="button"><i class="fa fa-camera"></i>Dokumentasi </button>',
			   strtoupper($val['jenjang']),
			   $val['nsm'],
			   $val['nama'],
			   $val['akreditasi'],
			   $val['provinsi'],
			   $val['kota'],
			   "<span class='badge badge-primary'>".$arbantuan[$val['bantuan']]."</span>",
			   $status[$edm],
			   $status[$pd],				 
			   $status[$rombel],				 
			   $status[$guru],				 
			   $status[$ruangbelajar],				 
			   $status[$toilet],				 
			   $status[$tamuk]
			  
			   
			   
			  

			 );
		 }
   
	 $records["draw"] = $sEcho;
	 $records["recordsTotal"] = $iTotalRecords;
	 $records["recordsFiltered"] = $iTotalRecords;
	 
	 echo json_encode($records);
}
// Rencana 

 public function rencana()
 {  
	  
	  $ajax            = $this->input->get_post("ajax",true);	
	  $data['title']   = "Rencana Madrasah ";
	  $data['data']    =  $this->db->get_where("madrasahedm",array("id"=>$_SESSION['madrasah_id']))->row();
	  if(!empty($ajax)){
					 
		  $this->load->view('rencana/page',$data);
	 
	  }else{
		  
		 
		  $data['konten'] = "rencana/page";
		  
		  $this->_template($data);
	  }
 

 }

 public function rencana_grid(){
	 error_reporting(0);
	 $iTotalRecords = $this->m->visitasi_grid(false)->num_rows();
	 
	 $iDisplayLength = intval($_REQUEST['length']);
	 $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
	 $iDisplayStart = intval($_REQUEST['start']);
	 $sEcho = intval($_REQUEST['draw']);
	 
	 $records = array();
	 $records["data"] = array(); 

	 $end = $iDisplayStart + $iDisplayLength;
	 $end = $end > $iTotalRecords ? $iTotalRecords : $end;
	 
	 $datagrid = $this->m->visitasi_grid(true)->result_array();
	 $arbantuan = array("1"=>"Kinerja","2"=>"Afirmasi");
	 
	  $i= ($iDisplayStart +1);
	  $status =  array("0"=>"<span class='fa fa-times' style='color:red'></span>","1"=>"<span class='fa fa-check-square-o primary' style='color:blue'></span>");
	  
	  
	  foreach($datagrid as $val) {
		   
		   $no        = $i++;
		   $edm       = $this->db->query("select id from visitasi_edm where madrasah_id='{$val['id']}'  AND nilai_pembiayaan IS NOT NULL AND nilai_proses_pembelajaran IS NOT NULL LIMIT 1")->num_rows();
		   $pd        = $this->db->query("select id from visitasi_pesertadidik where madrasah_id='{$val['id']}'  AND total !=0 LIMIT 1")->num_rows();
		   $rombel    = $this->db->query("select id from visitasi_rombel where  madrasah_id='{$val['id']}'  AND total !=0 LIMIT 1")->num_rows();
		   $guru      = $this->db->query("select id from visitasi_guru where madrasah_id='{$val['id']}'   LIMIT 1")->num_rows();
		   $ruangbelajar   = $this->db->query("select id from visitasi_ruangbelajar where  madrasah_id='{$val['id']}'  AND total !=0 LIMIT 1")->num_rows();
		   $toilet         = $this->db->query("select id from visitasi_toilet where madrasah_id='{$val['id']}'   LIMIT 1")->num_rows();
		   $tamuk          = $this->db->query("select id from visitasi_tatapmuka where madrasah_id='{$val['id']}'   LIMIT 1")->num_rows();
		   $rencanakeg     = $this->db->query("select id from visitasi_kegiatan where madrasah_id='{$val['id']}'   LIMIT 1")->num_rows();
	       
		   $records["data"][] = array(
			   $no,
			   
			   '<button href="'.site_url('madrasah/rencanaAct?id='.$val['id'].'').'" class="btn btn-primary btn-shadow-hover font-weight-bold mr-2 menuajax" type="button"><i class="fa fa-file"></i>Rencana  </button>',
			  
			   strtoupper($val['jenjang']),
			   $val['nsm'],
			   $val['nama'],
			   $val['akreditasi'],
			   $val['provinsi'],
			   $val['kota'],
			   "<span class='badge badge-primary'>".$arbantuan[$val['bantuan']]."</span>",
			   $status[$edm],
			   $status[$pd],				 
			   $status[$rombel],				 
			   $status[$guru],				 
			   $status[$ruangbelajar],				 
			   $status[$toilet],				 
			   $status[$tamuk]
			  
			   
			   
			  

			 );
		 }
   
	 $records["draw"] = $sEcho;
	 $records["recordsTotal"] = $iTotalRecords;
	 $records["recordsFiltered"] = $iTotalRecords;
	 
	 echo json_encode($records);
}

public function rencanaAct(){
   

	$data['data']    = $this->db->get_where("madrasahedm",array("id"=>$_SESSION['madrasah_id']))->row();

	$ajax            = $this->input->get_post("ajax",true);	
		 $data['title']   = "Rencana Visitasi";
		
	     if(!empty($ajax)){
					    
			 $this->load->view('rencana/rencana',$data);
		
		 }else{
			 
			
		     $data['konten'] = "rencana/rencana";
			 
			 $this->_template($data);
		 }


  }

  public function kegiatan_save(){
	 $kegiatannama    = $this->Reff->get_kondisi(array("id"=>$_POST['kegiatan']),"visitasi_tatapmuka","kegiatan");
     
	 $cek = $this->db->query("select sum(biaya) as totalbiaya from visitasi_kegiatan where madrasah_id='{$_POST['madrasah_id']}'")->row();
     $nilai = $cek->totalbiaya + str_replace(".","",$_POST['biaya']);
     
	 $cekkegiatan = $this->db->query("select id from visitasi_kegiatan where madrasah_id='{$_POST['madrasah_id']}' and kegiatan_id='{$_POST['kegiatan']}'")->num_rows();
   
	if($cekkegiatan==0){
			if($nilai <= 150000000){
					$this->db->set("kegiatan",$kegiatannama);
					$this->db->set("tahun",$_POST['tahun']);
					$this->db->set("kegiatan_id",$_POST['kegiatan']);
					$this->db->set("volume",$_POST['volume']);
					$this->db->set("satuan",$_POST['satuan']);
					$this->db->set("harga",str_replace(".","",$_POST['harga']));
					$this->db->set("biaya",str_replace(".","",$_POST['biaya']));
					$this->db->set("madrasah_id",$_POST['madrasah_id']);
					$this->db->set("s_update",1);
					$this->db->insert("visitasi_kegiatan");

					$data['id'] = $_POST['madrasah_id'];

	 		$this->load->view("rencana/loadrencana",$data);

			}else{

			 echo "error";

			}
    }else{
		$data['id'] = $_POST['madrasah_id'];

		$this->load->view("rencana/loadrencana",$data);

	}

	 


  }


  public function kegiatan_saveEdit(){
	$kegiatannama    = $this->Reff->get_kondisi(array("id"=>$_POST['kegiatan']),"visitasi_tatapmuka","kegiatan");
	
	$cek = $this->db->query("select sum(biaya) as totalbiaya from visitasi_kegiatan where madrasah_id='{$_SESSION['madrasah_id']}' and id !='{$_POST['id']}'")->row();
	$nilai = $cek->totalbiaya + str_replace(".","",$_POST['biaya']);
	
	
		   if($nilai <= 150000000){

			$cekViKeg= $this->db->get_where("visitasi_kegiatan",array("id"=>$_POST['id'],"madrasah_id"=>$_SESSION['madrasah_id']))->row();

			if($cekViKeg->s_update !=1){
				$this->db->set("awal",$cekViKeg->kegiatan);
				$this->db->set("s_update",1);
			}

		     	 $this->db->set("tahun",$_POST['tahun']);
				   $this->db->set("kegiatan",$kegiatannama);
				   $this->db->set("kegiatan_id",$_POST['kegiatan']);
				   $this->db->set("volume",$_POST['volume']);
				   $this->db->set("satuan",$_POST['satuan']);
				   $this->db->set("harga",str_replace(".","",$_POST['harga']));
				   $this->db->set("biaya",str_replace(".","",$_POST['biaya']));
				  
				   $this->db->where("madrasah_id",$_SESSION['madrasah_id']);
				   $this->db->where("id",$_POST['id']);
				   $this->db->update("visitasi_kegiatan");

				   $data['id'] = $_SESSION['madrasah_id'];

		        	$this->load->view("rencana/loadrencana",$data);

		   }else{

			header('Content-Type: application/json');
			echo json_encode(array('error' => true, 'message' => "Biaya Total tidak boleh melebihi 150.000.000"));

		   }
  

	


 }



  public function kegiatan_hapus(){

	
	$this->db->where("id",$_POST['id']);
	$this->db->where("madrasah_id",$_POST['madrasah_id']);
	$this->db->delete("visitasi_kegiatan");

	$data['id'] = $_POST['madrasah_id'];

	$this->load->view("rencana/loadrencana",$data);


 }

 public function buktivisitasi(){

	$this->load->helper('exportpdf_helper'); 

	            $pdf_filename = 'Bukti Visitasi.pdf';	 
				$data['madrasah'] = $this->db->get_where("madrasahedm",array("id"=>$_GET['id']))->row();
				$asesor           = $this->db->query("select asesor from visitasi_kegiatan where madrasah_id='{$_GET['id']}' limit 1")->row();
				$data['petugas'] = $this->db->get_where("asesor",array("id"=>$asesor->asesor))->row();
				$user_info = $this->load->view('rencana/bukti', $data, true);
			
				
			 	$output = $user_info;
				
				generate_pdf($output, $pdf_filename);
 }


  public function uploadDok(){

			$madrasah_id = $this->input->get_post("madrasah_id");
			$madrasah    = $this->db->query("select id,nama,nsm,folder from madrasahedm where id='{$madrasah_id}'")->row();
			
			if(empty($_FILES["file"]['name'])){
				header("Content-Type: application/json");
				echo json_encode(array('error' =>'Semua File sudah terkirim',"uploaded"=>"error"));
				exit();

			}

			$service    = new Google_Drive();
			$foldernama = $madrasah->nsm."_".str_replace(array("'"," ","."," ","-"),"_",$madrasah->nama);
			$foldernama = str_replace("/",DIRECTORY_SEPARATOR,$foldernama);
			$folderId   = $madrasah->folder;

				if(!$folderId){
					$folderId = $service->getFileIdByName( BACKUP_FOLDER );
					if( !$folderId ) {
						$folderId = $service->createFolder( BACKUP_FOLDER );					
					}
					$folderId = $service->createMultiFolder($foldernama,$folderId);
					$this->db->set("folder",$folderId);
					
					$this->db->where("id",$madrasah_id);
					$this->db->update("madrasahedm");
				}

				$fileId = $service->createFileFromPath( $_FILES["file"]['tmp_name'], $_FILES["file"]['name'], $folderId );
				$service->setPublic($fileId);

				if (empty($fileId)) {
						header("Content-Type: application/json");
						echo json_encode(array('error' =>'Proses upload gagal dilakukan, silahkan coba kembali',"uploaded"=>"error"));
					
				}else{
				$cek = $this->db->get_where("tr_persyaratan",array("madrasah_id"=>$madrasah_id))->num_rows();
				
				    $this->db->set("madrasah_id",$madrasah_id);
					$this->db->set("asesor",$_SESSION['peserta_id']);					
					$this->db->set("file",$fileId);
					$this->db->insert("tr_persyaratan");
									
						header('Content-Type: application/json');
						echo json_encode(array('success' => true, 'message' => "Upload Sukses"));


				}
		}

	public function hapusDok(){

		$this->db->where("id",$this->input->get_post("key"));
		$this->db->delete("tr_persyaratan");

		header('Content-Type: application/json');
	    echo json_encode(array('success' => true, 'message' => "Upload Sukses"));
	}

	public function generatePassword(){

		$madrasah = $this->db->query("select * from madrasahedm where dnt=1")->result_array();
		foreach($madrasah as $val){

			$cekAsesor = $this->db->query("select asesor from visitasi_edm where madrasah_id='{$val['id']}' limit 1")->row();
			$asesor    = $this->db->get_where("asesor",array("id"=>$cekAsesor->asesor))->row();

			$this->db->set("password",$asesor->username);
			$this->db->where("id",$val['id']);
			$this->db->update("madrasahedm");
			
		}
	}

	

	public function catatan()
	{  
	     
		 $ajax            = $this->input->get_post("ajax",true);	
		 $data['title']   = "Catatan Madrasah ";
		 $data['data']    =  $this->db->get_where("madrasahedm",array("id"=>$_SESSION['madrasah_id']))->row();
	     if(!empty($ajax)){
					    
			 $this->load->view('catatan/page',$data);
		
		 }else{
			 
			
		     $data['konten'] = "catatan/page";
			 
			 $this->_template($data);
		 }
	

	}

	public function editrencana(){
		$data['data']    =  $this->db->get_where("visitasi_tatapmuka",array("id"=>$_POST['id']))->row();

		$this->load->view("visitasi/erkamEdit",$data);

	}

	public function editrencanaDana(){
		$data['data']    =  $this->db->get_where("visitasi_kegiatan",array("id"=>$_POST['id']))->row();

		$this->load->view("rencana/rencanaEdit",$data);

	}


	public function listkeg(){

		$jenis = $_POST['jenis'];
		  if($jenis=="EDM"){
             ?> <input type="text" class="form-control" id="kegiatan"> <?php 

		  }else{

			?> <select class="form-control" id="kegiatan" name="kegiatan">
				<option value="">= Pilih Kegiatan = </option>
				<?php 
				 $kegiatanList = $this->db->query("select * from list_kegiatan where jenis='".$jenis."' order by sub_kegiatan ASC")->result();
				   foreach($kegiatanList as $rli){
					?><option value="<?php echo $rli->id; ?>" nama="<?php echo $rli->sub_kegiatan; ?>"><?php echo $rli->sub_kegiatan; ?></option><?php 

				   }
				?>
				
				
				
		      </select>
				<?php 
		  }
	}


}
