<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<style>

.select2-hidden-accessible {
    border: 0 !important;
    clip: rect(0 0 0 0) !important;
    height: 1px !important;
    margin: -1px !important;
    overflow: hidden !important;
    padding: 0 !important;
    position: absolute !important;
    width: 1px !important
}

.form-control {
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s
}

.select2-container--default .select2-selection--single,
.select2-selection .select2-selection--single {
    border: 1px solid #d2d6de;
    border-radius: 0;
    padding: 6px 12px;
    height: 34px
}

.select2-container--default .select2-selection--single {
    background-color: #fff;
    border: 1px solid #aaa;
    border-radius: 4px
}

.select2-container .select2-selection--single {
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    height: 28px;
    user-select: none;
    -webkit-user-select: none
}

.select2-container .select2-selection--single .select2-selection__rendered {
    padding-right: 10px
}

.select2-container .select2-selection--single .select2-selection__rendered {
    padding-left: 0;
    padding-right: 0;
    height: auto;
    margin-top: -3px
}

.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 28px
}

.select2-container--default .select2-selection--single,
.select2-selection .select2-selection--single {
    border: 1px solid #d2d6de;
    border-radius: 0 !important;
    padding: 6px 12px;
    height: 40px !important
}

.select2-container--default .select2-selection--single .select2-selection__arrow {
    height: 26px;
    position: absolute;
    top: 6px !important;
    right: 1px;
    width: 20px
}

</style>


<?php $status = $this->db->query("select status from pengaturan where tahun='{$data->tahun}'")->row(); $disabled = ($status->status==1) ? "disabled":"";  ?>
<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<!--begin::Card-->
								<div class="card card-custom">
									<div class="card-header">
										<div class="card-title">
											<h3 class="card-label">Bimbingan Penyusunan Rencana Pemanfaatan Dana Bantuan  <b><?php echo $data->nama; ?> (<?php echo $data->nsm; ?>)</b>
											</h3>
										</div>
										<div class="card-toolbar">
											
									
										</div>
									</div>
									<div class="card-body">
                             
                                   <div class="alert alert-primary">
                                   Bimbinglah Tim Inti Madrasah dalam melakukan kegiatan berikut.
                                   <ol start="1">
                                      
                                       <li> Perkirakan volume pekerjaan, satuan, harga satuan, dan total biaya. </li>
                                       <li> Pastikan total biaya tidak melebihi Rp. 150 juta. </li>
                                       <li> Pastikan kegiatan tersebut belum didanai oleh sumber lain. </li>
                                    </ol>
                                    </div>
                                 

                                      <div class="table-responsive">
                                          <table class="table table-hover table-bordered table-striped">
                                              <thead>
                                                <tr>
                                                   <th>NO</th>
                                                   <th>KEGIATAN</th>
                                                   <th>VOLUME</th>
                                                   <th>SATUAN</th>
                                                   <th>HARGA </th>
                                                   <th>BIAYA</th>
                                                   <th>TAHUN</th>
                                                   <th>AKSI </th>
                                                </tr>
                                              </thead>

                                              <thead>
                                                <tr>
                                                   <th>#</th>
                                                   <th>

                                                      <select class="select2 form-control" id="kegiatan">
                                                         <option value="">- Pilih Kegiatan -</option>

                                                         <?php 
                                                            $masterkegiatan = $this->db->query("select * from visitasi_tatapmuka where madrasah_id='{$_SESSION['madrasah_id']}' order by prioritas ASC")->result();
                                                              foreach($masterkegiatan as $rm){

                                                                  ?> <option value="<?php echo $rm->id; ?>"> <?php echo ($rm->jenis); ?> - <?php echo $rm->kegiatan; ?> (Prioritas <?php echo $rm->prioritas; ?>) </option> <?php 
                                                              }
                                                         ?>

                                                      </select>

                                                   </th>
                                                   <th><input type="number" class="form-control" id="volume"></th>
                                                   <th><input type="text" class="form-control" id="satuan"></th>
                                                   <th><input type="text" class="form-control" id="harga" onkeyup="return FormatCurrency(this)" ></th>
                                                   <th><input type="text" class="form-control" id="biaya" onkeyup="return FormatCurrency(this)" ></th>
                                                   <th>

                                                    <select class="select2 form-control" id="tahun">
                                                         <option value="">- Pilih Tahun -</option>
                                                         <option value="2021">2021</option>
                                                         <option value="2022">2022</option>

                                                       

                                                      </select>

                                                            </th>
                                                   <th><button type="button" class="btn btn-primary btn-sm" id="tambahrencana" > <i class="fa fa-plus"></i> </button></th>
                                                </tr>
                                              </thead>
                                              <tbody id="loadbodyrencana">

                                                 <?php 
                                                   $kegiatan = $this->db->get_where("visitasi_kegiatan",array("madrasah_id"=>$data->id))->result();
                                                   $no=1;   
                                                   $total = 0;
                                                   foreach($kegiatan as $rk){
                                                      $total = $total + $rk->biaya;

                                                         ?>

                                                                <tr>
                                                                  <td><?php echo $no++; ?></td>
                                                                  <td><b><?php echo $this->Reff->get_kondisi(array("id"=>$rk->kegiatan_id),"visitasi_tatapmuka","jenis"); ?></b> <br> <?php echo $rk->kegiatan; ?></td>
                                                                  <td><?php echo $rk->volume; ?></td>
                                                                  <td><?php echo $rk->satuan; ?></td>
                                                                  <td><?php echo $this->Reff->formatuang2($rk->harga); ?></td>
                                                                  <td><?php echo $this->Reff->formatuang2($rk->biaya); ?></td>
                                                                  <td><?php echo $rk->tahun; ?></td>
                                                                  
                                                                  <td>
                                                                     
                                                                  <button type="button" class="btn btn-primary btn-sm editrencanaDana" data-toggle="modal" data-target="#myModalDana"  data-madrasah_id="<?php echo $rk->madrasah_id; ?>" data-id="<?php echo $rk->id; ?>">  <i class="fa fa-edit"></i> </button>
                                                                     <button type="button" class="btn btn-danger btn-sm hapusrencana"  data-madrasah_id="<?php echo $rk->madrasah_id; ?>" data-id="<?php echo $rk->id; ?>">  <i class="fa fa-trash"></i> </button>
                                                               
                                                               
                                                               </td>
                                                               </tr>




                                                         <?php



                                                      }
                                                   ?>

                                                   <tr style="font-weight:bold">
                                                      <td colspan="5"> Total Kegiatan </td>
                                                      <td> <?php echo $this->Reff->formatuang2($total); ?> </td>
                                                      <td> - </td>
                                                      <td> - </td>
                                                   </tr>


                                                   <tr style="font-weight:bold">
                                                      <td colspan="5"> Saldo Tahun 2021 </td>
                                                      <td> 
                                                        
                                                      <?php 
                                                      $saldo = $this->db->query("select sum(biaya) as total from visitasi_kegiatan where tahun='2021' and madrasah_id='".$_SESSION['madrasah_id']."'")->row();
                                                      echo $this->Reff->formatuang2(90000000-$saldo->total); ?>
                                                    
                                                    </td>
                                                      <td> - </td>
                                                      <td> - </td>
                                                   </tr>

                                                   <tr style="font-weight:bold">
                                                      <td colspan="5"> Saldo Tahun 2022 </td>
                                                      <td> <?php 
                                                      $saldo = $this->db->query("select sum(biaya) as total from visitasi_kegiatan where tahun='2022' and madrasah_id='".$_SESSION['madrasah_id']."'")->row();
                                                      echo $this->Reff->formatuang2(60000000-$saldo->total); ?> </td>
                                                      <td> - </td>
                                                      <td> - </td>
                                                   </tr>


                                             </tbody>



                                        </table>
                                    </div>
                            

                                        </div>

                                    
                                     </div>
										
									</div>
								</div>
								<!--end::Card-->
								<!--begin: Code-->
								
								<!--end: Code-->
							</div>
							<!--end::Container-->
						</div>
<div id="myModalDana" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       
        <h4 class="modal-title" style="float:left">Rencana Pemanfaatan Dana </h4>
      </div>
      <div class="modal-body" id="loadrencanaEditDana">
        <p>loading...</p>
      </div>
      
    </div>

  </div>
</div>


<script type="text/javascript">



$(document).off("click",".editrencanaDana").on("click",".editrencanaDana",function(){

var id   =  $(this).data("id")
loading();
$.post("<?php echo site_url('madrasah/editrencanaDana'); ?>",{id:id},function(data){

 $("#loadrencanaEditDana").html(data);

 jQuery.unblockUI({ });

});


});



$(document).ready(function() {
   $('.select2').select2({
closeOnSelect: true,
width: '100%' 
});
});


  $(document).off("click","#tambahrencana").on("click","#tambahrencana",function(){

  var kegiatan= $("#kegiatan").val();
  var volume= $("#volume").val();
  var satuan= $("#satuan").val();
  var harga= $("#harga").val();
  var biaya= $("#biaya").val();
  var tahun= $("#tahun").val();
  var madrasah_id = "<?php echo $data->id; ?>";
 
   if(kegiatan=="" && biaya==""  && harga=="" && tahun==""){ return false; }
    
     $.post("<?php echo site_url('madrasah/kegiatan_save'); ?>",{kegiatan:kegiatan,volume:volume,satuan:satuan,harga:harga,biaya:biaya,madrasah_id:madrasah_id,tahun:tahun},function(data){


     if(data !="error"){
      $("#loadbodyrencana").html(data);
      $("#kegiatan").val("");
      $("#volume").val("");
      $("#satuan").val("");
      $("#harga").val("");
      $("#biaya").val("");
      $("#tahun").val("");
     }else{

        alertify.alert('Total biaya tidak boleh melebihi Rp. 150.000.000');
     }

     });


  });

$(document).off("click",".hapusrencana").on("click",".hapusrencana",function(){

var madrasah_id =  $(this).data("madrasah_id")
var id   =  $(this).data("id")
 alertify.confirm("Jika menghapus data, Data akan hilang, apakah Anda yakin ?",function(){
  
   $.post("<?php echo site_url('madrasah/kegiatan_hapus'); ?>",{id:id,madrasah_id:madrasah_id},function(data){

    $("#loadbodyrencana").html(data);

   });
})


});
$(document).off("input","#harga").on("input","#harga",function(){

     
  var harga = HapusTitik($(this).val());
  var volume = $("#volume").val();
  var total = TambahTitik(volume * harga)
   $("#biaya").val(total);






});

</script>



<script type="text/javascript">
$(document).off('submit', 'form#kegiatan_saveEdit').on('submit', 'form#kegiatan_saveEdit', function (event, messages) {
	 event.preventDefault()
       var form   = $(this);
       var urlnya = $(this).attr("url");
	
	   var formData = new FormData(this);
   
	  loading();
        $.ajax({
            type: "POST",
            url: urlnya,
            
			data: formData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,    
			success: function (response, status, xhr) {
                var ct = xhr.getResponseHeader("content-type") || "";
                if (ct == "application/json") {
                    
                   alertify.alert(response.message);
                } else {
					
                  toastr.success("Data Berhasil disimpan", "Sukses !", {
							"timeOut": "0",
							"extendedTImeout": "0",
							"closeButton": true,
							  "debug": false,
							  "positionClass": "toast-top-right",
							  "onclick": null,
							  "showDuration": "2000",
							  "hideDuration": "2000",
							  "timeOut": "2000",
							  "extendedTimeOut": "2000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
						});

					
                  $("#loadbodyrencana").html(response);
				   
				    $("#myModalDana").modal("toggle");
				   
                }
				
				jQuery.unblockUI({ });
            }
        });

        return false;
    });	

</script>