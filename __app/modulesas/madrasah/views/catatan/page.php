
<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<!--begin::Card-->
								<div class="card card-custom">
									<div class="card-header">
										<div class="card-title">
											<h3 class="card-label">Catatan dari Tim Verifikator Untuk  <b><?php echo $data->nama; ?> (<?php echo $data->nsm; ?>)</b>
											</h3>
										</div>
										<div class="card-toolbar">
											
									
										</div>
									</div>
									<div class="card-body">
                              
                                   <div class="alert alert-primary">
                                   Dibawah ini adalah catatan dari Verifikator untuk Madrasah Anda <br>
                                   Silahkan perbaiki Rencana Pemanfaatan Dana sesuai dengan catatan dari verifikator.
                                    </div>
                                 

                                    <div class="table-responsive">
                                          <table class="table table-hover table-bordered table-striped">
                                              <thead>
                                                <tr>
                                                   <th>NO</th>
                                                   <th>VERIFIKATOR</th>
                                                   <th>KATEGORI</th>
                                                   <th>CATATAN</th>
                                                   <th>TANGGAL</th>                                                  
                                                 
                                                </tr>
                                              </thead>

                                              <tbody id="loadbodyrencana">

                                                 <?php 
                                                   $kegiatan = $this->db->get_where("verifikasi_catatan",array("madrasah_id"=>$_SESSION['madrasah_id']))->result();
                                                   $no=1;   
                                                   $total = 0;
                                                   foreach($kegiatan as $rk){
                                                      

                                                         ?>

                                                                <tr>
                                                                  <td><?php echo $no++; ?></td>
                                                                  <td><?php echo $this->Reff->get_kondisi(array("id"=>$rk->verifikator),"verifikator","nama"); ?></td>
                                                                  <td><?php echo $this->Reff->get_kondisi(array("id"=>$rk->catatan_id),"catatan","nama"); ?></td>
                                                                  <td><?php echo str_replace('\r\n',"<br>",$rk->catatan); ?></td>
                                                                  <td><?php echo $this->Reff->formattimestamp($rk->tanggal); ?></td>
                                                                 
                                                                  
                                                                  
                                                               </tr>




                                                         <?php



                                                      }
                                                   ?>

                                                   

                                             </tbody>



                                        </table>
                                    </div>

                            

                                        </div>

                                    
                                     </div>
										
									</div>
								</div>
								<!--end::Card-->
								<!--begin: Code-->
								
								<!--end: Code-->
							</div>
							<!--end::Container-->
						</div>

<script type="text/javascript">

  $(document).off("click","#tambahrencana").on("click","#tambahrencana",function(){

  var kegiatan= $("#kegiatan").val();
  var volume= $("#volume").val();
  var satuan= $("#satuan").val();
  var harga= $("#harga").val();
  var biaya= $("#biaya").val();
  var madrasah_id = "<?php echo $data->id; ?>";
 
   if(kegiatan=="" && biaya==""  && harga==""){ return false; }
    
     $.post("<?php echo site_url('asesor/kegiatan_save'); ?>",{kegiatan:kegiatan,volume:volume,satuan:satuan,harga:harga,biaya:biaya,madrasah_id:madrasah_id},function(data){

      $("#loadbodyrencana").html(data);
      $("#kegiatan").val("");
      $("#volume").val("");
      $("#satuan").val("");
      $("#harga").val("");
      $("#biaya").val("");

     });


  });

$(document).off("click",".hapusrencana").on("click",".hapusrencana",function(){

var madrasah_id =  $(this).data("madrasah_id")
var id   =  $(this).data("id")

  
   $.post("<?php echo site_url('asesor/kegiatan_hapus'); ?>",{id:id,madrasah_id:madrasah_id},function(data){

    $("#loadbodyrencana").html(data);

   });


});
$(document).off("input","#harga").on("input","#harga",function(){

     
  var harga = HapusTitik($(this).val());
  var volume = $("#volume").val();
  var total = TambahTitik(volume * harga)
   $("#biaya").val(total);






});

</script>