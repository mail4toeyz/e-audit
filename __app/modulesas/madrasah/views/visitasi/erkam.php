

<?php $status = $this->db->query("select status from pengaturan where tahun='{$data->tahun}'")->row(); $disabled = ($status->status==1) ? "disabled":"";  ?>
<div class="row">

   <div class="col-xl-12">
   <div class="alert alert-default"> Identifikasi capaian indikator yang rendah <br> 
Tentukan bersama kegiatan yang dapat meningkatkan capaian indikator yang rendah tersebut dan belum didanai oleh sumber lain  <b><?php echo $data->nama; ?> (<?php echo $data->nsm; ?>)</b> </div>

   <div class="table-responsive">
                                          <table class="table table-hover table-bordered table-striped">
                                              <thead>
                                                <tr>
                                                   <th>NO</th>
                                                   <th>JENIS</th>
                                                   <th>KEGIATAN</th>
                                                   <th>PRIORITAS</th>
                                                   <th>AKSI</th>
                                                   
                                                </tr>
                                              </thead>

                                              <thead>
                                                <tr>
                                                   <th>#</th>
                                                   <th>
                                                     <select class="form-control" id="jenis">
                                                        <option value=""> = Pilih Jenis = </option>
                                                        <option value="PTM"> PTM</option>
                                                        <option value="EDM"> EDM </option>
                                                        <option value="REHAB"> REHAB </option>



                                                    </select>
                                                    
                                                    </th>
                                                   <th id="loadListKeg"></th>
                                                   <th valign="top"><input type="number" class="form-control" id="prioritas"></th>
                                                   
                                                   <th><button type="button" class="btn btn-primary btn-sm" id="tambahrencana" > <i class="fa fa-plus"></i> </button></th>
                                                </tr>
                                              </thead>

                                              <tbody id="loadbodyrencana">

                                                 <?php 
                                                   $kegiatan = $this->db->get_where("visitasi_tatapmuka",array("madrasah_id"=>$data->id,"kegiatan_id"=>0))->result();
                                                   $no=1;   
                                                   foreach($kegiatan as $rk){

                                                      $cekKegiatan = $this->db->get_where("visitasi_kegiatan",array("madrasah_id"=>$_SESSION['madrasah_id'],"kegiatan_id"=>$rk->id))->num_rows();

                                                         ?>

                                                                <tr>
                                                                  <td><?php echo $no++; ?></td>
                                                                  <td><?php echo $rk->jenis; ?></td>
                                                                  <td><?php echo $rk->kegiatan; ?></td>
                                                                  <td><?php echo $rk->prioritas; ?></td>
                                                                
                                                                  
                                                                  <td><button type="button" class="btn btn-primary btn-sm editrencana" data-toggle="modal" data-target="#myModal"  data-madrasah_id="<?php echo $rk->madrasah_id; ?>" data-id="<?php echo $rk->id; ?>">  <i class="fa fa-edit"></i> </button>
                                                                     <?php 
                                                                       if($cekKegiatan == 0){
                                                                        ?><button type="button" class="btn btn-danger btn-sm hapusrencana"  data-madrasah_id="<?php echo $rk->madrasah_id; ?>" data-id="<?php echo $rk->id; ?>">  <i class="fa fa-trash"></i> </button><?php 

                                                                       }
                                                                     ?>

                                                            
                                                            
                                                            </td>
                                                               </tr>




                                                         <?php



                                                      }
                                                   ?>


                                             </tbody>

                                        </table>
                                    </div>
   </div>

   
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       
        <h4 class="modal-title" style="float:left">Rencana Kegiatan </h4>
      </div>
      <div class="modal-body" id="loadrencanaEdit">
        <p>loading...</p>
      </div>
      
    </div>

  </div>
</div>

<script type="text/javascript">


$(document).off("change","#jenis").on("change","#jenis",function(){

var jenis   =  $(this).val()
loading();
$.post("<?php echo site_url('madrasah/listkeg'); ?>",{jenis:jenis},function(data){

 $("#loadListKeg").html(data);

 jQuery.unblockUI({ });

});


});




  $(document).off("click","#tambahrencana").on("click","#tambahrencana",function(){

  var jenis= $("#jenis").val();
  var id_keg= $("#kegiatan").val();
  var prioritas= $("#prioritas").val();
  var kegiatan = $("#kegiatan option:selected").attr("nama");
  if (jenis == "EDM"){
  var kegiatan= $("#kegiatan").val();
  }
  var madrasah_id = "<?php echo $data->id; ?>";
  var nsm         = "<?php echo $data->nsm; ?>";
 
   if(kegiatan==""){ return false; }
   loading();
     $.post("<?php echo site_url('madrasah/erkam_save'); ?>",{kegiatan:kegiatan,prioritas:prioritas,madrasah_id:madrasah_id,nsm:nsm,jenis:jenis,id_keg:id_keg},function(data){

      $("#loadbodyrencana").html(data);
      $("#kegiatan").val("");
      $("#prioritas").val("");
      jQuery.unblockUI({ });

     });


  });

  $(document).off("click",".editrencana").on("click",".editrencana",function(){

   var id   =  $(this).data("id")
 loading();
   $.post("<?php echo site_url('madrasah/editrencana'); ?>",{id:id},function(data){

    $("#loadrencanaEdit").html(data);
   
    jQuery.unblockUI({ });

   });


});

$(document).off("click",".hapusrencana").on("click",".hapusrencana",function(){

var madrasah_id =  $(this).data("madrasah_id")
var id   =  $(this).data("id")

loading();
   $.post("<?php echo site_url('madrasah/erkam_hapus'); ?>",{id:id,madrasah_id:madrasah_id},function(data){

    $("#loadbodyrencana").html(data);
    jQuery.unblockUI({ });
   });


});



</script>


<script type="text/javascript">
$(document).off('submit', 'form#erkam_saveEdit').on('submit', 'form#erkam_saveEdit', function (event, messages) {
	 event.preventDefault()
       var form   = $(this);
       var urlnya = $(this).attr("url");
	
	   var formData = new FormData(this);
   
	  loading();
        $.ajax({
            type: "POST",
            url: urlnya,
            
			data: formData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,    
			success: function (response, status, xhr) {
                var ct = xhr.getResponseHeader("content-type") || "";
                if (ct == "application/json") {
                    
                    toastr.error(response.message, "Gagal  , perhatikan !  ", {
							"timeOut": "0",
							"extendedTImeout": "0",
							"closeButton": true,
							  "debug": false,
							  "positionClass": "toast-top-right",
							  "onclick": null,
							  "showDuration": "2000",
							  "hideDuration": "2000",
							  "timeOut": "2000",
							  "extendedTimeOut": "2000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
						});
                } else {
					
                  toastr.success("Data Berhasil disimpan", "Sukses !", {
							"timeOut": "0",
							"extendedTImeout": "0",
							"closeButton": true,
							  "debug": false,
							  "positionClass": "toast-top-right",
							  "onclick": null,
							  "showDuration": "2000",
							  "hideDuration": "2000",
							  "timeOut": "2000",
							  "extendedTimeOut": "2000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
						});

					
                  $("#loadbodyrencana").html(response);
				   
				    $("#myModal").modal("toggle");
				   
                }
				
				jQuery.unblockUI({ });
            }
        });

        return false;
    });	

</script>