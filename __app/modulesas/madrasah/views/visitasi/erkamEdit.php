<form class="form" id="erkam_saveEdit" enctype="multipart/form-data" url="<?php echo site_url("madrasah/erkam_saveEdit"); ?>">
                                                   <input type="hidden" name="id" value="<?php echo $data->id; ?>">

												   <div class="form-group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Jenis</label>
														<div class="col-lg-9 col-xl-9">
																<select class="form-control" id="jenisEdit" name="jenis">
																<option value=""> = Pilih Jenis = </option>
																<option value="PTM"> PTM</option>
																<option value="EDM"> EDM </option>
																<option value="REHAB"> REHAB </option>



															</select>
														</div>
													</div>



                                                    <div class="form-group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Kegiatan</label>
														<div class="col-lg-9 col-xl-9" id="KegiatanList">
															
														</div>
													</div>
													<div class="form-group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Prioritas</label>
														<div class="col-lg-9 col-xl-9">
															<input class="form-control form-control-lg form-control-solid" type="number" name="prioritas" value="<?php echo $data->prioritas; ?>" />
														</div>
													</div>


<center>
<button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="fa fa-times"></span> Tutup </button>
<button type="submit" class="btn btn-success btn-sm"><span class="fa fa-save"></span> Simpan Perubahan </button>
</center>
</form>


<script type="text/javascript">


$(document).off("change","#jenisEdit").on("change","#jenisEdit",function(){

var jenis   =  $(this).val()
loading();
$.post("<?php echo site_url('madrasah/listkeg'); ?>",{jenis:jenis},function(data){

 $("#KegiatanList").html(data);

 jQuery.unblockUI({ });

});


});


</script>