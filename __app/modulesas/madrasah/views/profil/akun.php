<div class="d-flex flex-column-fluid">
							
<div class="container">

					
					
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<!--begin::Profile Personal Information-->
								<div class="d-flex flex-row">
									<!--begin::Aside-->
									<div class="flex-row-auto offcanvas-mobile w-250px w-xxl-350px" id="kt_profile_aside">
										<!--begin::Profile Card-->
										<div class="card card-custom card-stretch">
											<!--begin::Body-->
											<div class="card-body pt-4">
												
											
												<!--begin::User-->
												<div class="d-flex align-items-center">
													<div class="symbol symbol-60 symbol-xxl-100 mr-5 align-self-start align-self-xxl-center">
													<img alt="Logo" src="<?php echo base_url(); ?>__statics/img/logo.png" onError="this.onerror=null;this.src='<?php echo base_url(); ?>__statics/img/not.png';" class="max-h-70px" style="border-radius: 50%;" />
														<i class="symbol-badge bg-success"></i>
													</div>
													<div>
														<a href="#" class="font-weight-bolder font-size-h5 text-dark-75 text-hover-primary"><?php echo $data->nama; ?></a>
														<div class="text-muted">  <?php echo $data->provinsi; ?> - <?php echo $data->kota; ?> </div>
														
													</div>
												</div>
												<!--end::User-->
												<!--begin::Contact-->
											
												<!--end::Contact-->
												<!--begin::Nav-->
												<div class="navi navi-bold navi-hover navi-active navi-link-rounded">
													
													
													<div class="navi-item mb-2">
														<a href="<?php echo site_url('madrasah/akun'); ?>" class="menuajax navi-link py-4 active" title="Perbaharui Akun Asesor">
															<span class="navi-icon mr-2">
																<span class="svg-icon">
																	<!--begin::Svg Icon | path:/metronic/theme/html/demo3/dist/assets/media/svg/icons/Code/Compiling.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24" />
																			<path d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z" fill="#000000" opacity="0.3" />
																			<path d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z" fill="#000000" />
																		</g>
																	</svg>
																	<!--end::Svg Icon-->
																</span>
															</span>
															<span class="navi-text font-size-lg">Perbaharui Akun BKBA </span>
														</a>
													</div>
													
												</div>
												<!--end::Nav-->
											</div>
											<!--end::Body-->
										</div>
										<!--end::Profile Card-->
									</div>
									<!--end::Aside-->
									<!--begin::Content-->
									<div class="flex-row-fluid ml-lg-8">
										<!--begin::Card-->
										<form class="form" id="perbaharuiprofile" enctype="multipart/form-data" url="<?php echo site_url("madrasah/save_akun"); ?>">
										<div class="card card-custom card-stretch">
											<!--begin::Header-->
											<div class="card-header py-3">
												<div class="card-title align-items-start flex-column">
													<h3 class="card-label font-weight-bolder text-dark">Akun  Madrasah BKBA </h3>
													<span class="text-muted font-weight-bold font-size-sm mt-1">Perbaharui Password Anda secara berkala </span>
												</div>
												<div class="card-toolbar">
													<button type="submit" class="btn btn-success btn-sm"><span class="fa fa-save"></span> Simpan Perubahan </button>
													
												</div>
											</div>
											<!--end::Header-->
											<!--begin::Form-->
											
												<!--begin::Body-->
												<div class="card-body">
													
													
													<div class="form-group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Username</label>
														<div class="col-lg-9 col-xl-6">
															<input class="form-control form-control-lg form-control-solid" type="text" disabled value="<?php echo $data->nsm; ?>" />
														</div>
													</div>
													<div class="form-group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Password Baru </label>
														<div class="col-lg-9 col-xl-6">
															<input class="form-control form-control-lg form-control-solid" type="password" name="password"  />
														</div>
													</div>

													<div class="form-group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Konfirmasi Password Baru </label>
														<div class="col-lg-9 col-xl-6">
															<input class="form-control form-control-lg form-control-solid" type="password" name="cpassword" />
														</div>
													</div>

													
													
												</div>
												<!--end::Body-->
											</form>
											<!--end::Form-->
										</div>
									</div>
									<!--end::Content-->
								</div>
								
			
			<script src="<?php echo base_url(); ?>__statics/tema/js/pages/custom/profile/profile1894.js"></script>

<script type="text/javascript">
$(document).on('submit', 'form#perbaharuiprofile', function (event, messages) {
	 event.preventDefault()
       var form   = $(this);
       var urlnya = $(this).attr("url");
	
	   var formData = new FormData(this);
   
	  loading();
        $.ajax({
            type: "POST",
            url: urlnya,
            
			data: formData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,    
			success: function (response, status, xhr) {
                var ct = xhr.getResponseHeader("content-type") || "";
                if (ct == "application/json") {
                    
                    toastr.error(response.message, "Gagal  , perhatikan !  ", {
							"timeOut": "0",
							"extendedTImeout": "0",
							"closeButton": true,
							  "debug": false,
							  "positionClass": "toast-top-right",
							  "onclick": null,
							  "showDuration": "2000",
							  "hideDuration": "2000",
							  "timeOut": "2000",
							  "extendedTimeOut": "2000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
						});
                } else {
					
                  toastr.success("Data Berhasil disimpan", "Sukses !", {
							"timeOut": "0",
							"extendedTImeout": "0",
							"closeButton": true,
							  "debug": false,
							  "positionClass": "toast-top-right",
							  "onclick": null,
							  "showDuration": "2000",
							  "hideDuration": "2000",
							  "timeOut": "2000",
							  "extendedTimeOut": "2000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
						});

				
				  
				   
				    
				   
                }
				
				jQuery.unblockUI({ });
            }
        });

        return false;
    });	

</script>
							