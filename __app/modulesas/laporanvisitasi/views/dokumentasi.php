<link href="<?php echo base_url(); ?>__statics/js/galleri/css/lightgallery.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>__statics/js/galleri/js/lightgallery-all.min.js"></script>
<div class="card mb-5 mb-xl-10" >
<div class="card-body p-9">
<div class="demo-gallery">
            <ul id="lightgallery" class="list-unstyled row">

             <?php 
               $dokumentasi = $this->db->get_where("tr_persyaratan",array("madrasah_id"=>$data->id))->result();
                foreach($dokumentasi as $rdok){
                   
                ?>
                <li class="col-xs-6 col-sm-4 col-md-4" data-responsive="https://drive.google.com/uc?export=view&id=<?php echo $rdok->file; ?>" data-src="https://drive.google.com/uc?export=view&id=<?php echo $rdok->file; ?>" data-sub-html="Dokumentasi Pelaksanaan Visitasi " >
                    <a href="https://drive.google.com/file/d/<?php echo $rdok->file; ?>/view">
                        <img class="img-responsive img-thumbnail" style="height:300px;width:300px" src="https://drive.google.com/uc?export=view&id=<?php echo $rdok->file; ?>" alt="Dokumentasi">
                    </a>
                </li>
                <?php 
                }
                ?>
                
            </ul>
        </div>
    </div>
</div>

        <script type="text/javascript">
        $(document).ready(function(){
            $('#lightgallery').lightGallery();
           
        });
        </script>