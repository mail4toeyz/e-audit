	  
<style>
    .nukahiji{}
    .nukahiji header{font-size:13pt;text-align: center;font-weight: bold;margin-bottom: 0.1in;padding-bottom: 5px;border-bottom: 3px #000 solid;}
    .nukahiji header img{float: left;height: 60px;}
    .nukahiji .profil{margin-bottom: 20px;}
      .nukahiji .profil table{width: 100%}
      .nukahiji .profil table tr td{padding:5px 0px;color:#000;}
      .nukahiji .profil table tr td strong{font-size: 14pt;}
   .req_table{margin-bottom: 0px;}
    .req_table table{width: 100%;}
     .req_table table tr td{padding:5px;}
     .req_table table thead tr td{font-weight: bold;text-align: center;}
    .catatan{font:11pt arial;font-style: italic;clear: both;}
     .catatan .textbox{display: block;border:1px #000 solid;margin:20px 0px;padding:10px;font-weight: bolder;}
	.table_detail
	{
		border-bottom:1px #000 solid;
		border-right:1px #000 solid;
	}
	
  .table_detail tr th
	{
		border-top:1px #000 solid;
		border-left:1px #000 solid;
	}
	.table_detail tr td
	{
		border-top:1px #000 solid;
		border-left:1px #000 solid;
	}
	.photo
	{
		width:116px!important;
		height:140px!important;
		padding:5px;
		border:1px #CCCCCC solid;
		background-color:#ffffff;
	}	
  </style>
<?php
$documentroot = $_SERVER["DOCUMENT_ROOT"]."/bkba";
//$documentroot = base_url();

?>
<style>

#bodinya { margin-right: 40px;
  margin-left: 60px; 
  font-family: Bookman Old Style;
font-size: 16px;
  }
</style>
<table width="100%"  style="margin-top:0px">
			  
			    <tr>
				  <td width="105px"> <img src="<?php echo $documentroot.'/__statics/img/logo.png';?>" style="width:80px"></td>
				    <td align="center"   valign="top">
				       <span style="font-size:19px;font-weight:bold;font-family:Bookman Old Style">KEMENTERIAN AGAMA REPUBLIK INDONESIA</span><br/>
					   <span style="font-size:17px;font-weight:bold;font-family:Bookman Old Style">DIREKTORAT JENDERAL PENDIDIKAN ISLAM</span><br/>

					   <span style="font-size:12px;font-family:Bookman Old Style">  Jl. Lapangan Banteng Barat No. 3 – 4 Jakarta</span> <br>
					   <span style="font-size:12px;font-family:Bookman Old Style">  Telp. (021) 3811523 Pes. 528, Fax. (021) 3520951</span>
					   
					  </span>
				  </td>
				   <td width="20px">&nbsp;</td>
				</tr>
				
			 </table>
			 
<hr style="height: 2px;">
<div id="bodinya">
<p align="center"><b>HASIL VISITASI BANTUAN KINERJA DAN AFIRMASI </b> <br>  </p>
<b>Telah dilakukan visitasi kepada Madrasah sebagai berikut : </b>
<table>
<tr>
<td>Nomor Statistik Madrasah </td>
<td>:</td>
<td><?php echo $madrasah->nsm; ?> </td>
</tr>

<tr>
<td>Nama Madrasah</td>
<td>:</td>
<td> <?php echo $madrasah->nama; ?> </td>
</tr>

<tr>
<td>Provinsi  </td>
<td>:</td>
<td> <?php echo $madrasah->provinsi; ?></td>
</tr>

<tr>
<td>Kabupaten/Kota </td>
<td>:</td>
<td><?php echo $madrasah->kota; ?></td>
</tr>

</table><br>
<b>Petugas Visitasi  : </b>

<table>
<tr>
<td>Nomor Induk Kependudukan </td>
<td>:</td>
<td><?php echo $petugas->nik; ?> </td>
</tr>
<tr>
<td>Nama Asesor</td>
<td>:</td>
<td> <?php echo $petugas->nama; ?> </td>
</tr>

<tr>
<td>Tanggal Visitasi</td>
<td>:</td>
<td> <?php echo $this->Reff->formattanggalstring($aman->tanggal_mulai); ?> Pukul <?php echo $aman->jam_mulai; ?> </td>
</tr>


<tr>
<td>Selesai Visitasi</td>
<td>:</td>
<td> <?php echo $this->Reff->formattanggalstring($aman->tanggal_selesai); ?>  Pukul <?php echo $aman->jam_selesai; ?></td>
</tr>

</table><br>

<b>1. Skor Kinerja Pencapaian Mutu EDM :</b>
<br>
<br>

<div class="req_table">
<table border="1" width="100%" style="border-collapse: collapse; line-height: 1;" id="table_detail">
<tr style="font-weight:bold;">
    <th style="padding: 6px;" align="center" rowspan="2">No.</th>
    <th style="padding: 6px;" align="center" rowspan="2">Aspek</th>
    <th style="padding: 6px;" align="center" colspan="3">Skor </th>
  
</tr>

<tr style="font-weight:bold;">
   
    <th>Hasil Visitasi </th>
    <th>Terdata </th>
    <th>Persentase </th>
  
</tr>

<?php 
                     $tahun = $this->Reff->tahun();
                     foreach($this->db->get("edm_aspek")->result() as $row){
               
                      $eksis      = number_format($this->Reff->get_kondisi(array("id"=>$madrasah->id,"tahun"=>$tahun),"madrasahedm",$row->kolom),2);
                      $hasil      = $this->db->query("select * from visitasi_edm where madrasah_id='{$madrasah->id}'")->row();
                      $kolom      = $row->kolom;
                      $nilainya   = isset($hasil->$kolom) ? $hasil->$kolom :0;
                      $eksis      = round($eksis,1);
                      $persentase = isset($hasil->$kolom) ? number_format(($hasil->$kolom/$eksis) * 100,0) :0;


                        
                      ?>
                       <tr>
                        <td><?php echo $row->id; ?></td>
                        <td><?php echo $row->nama; ?></td>
                        
                        <td><?php echo $nilainya; ?></td>
                        <td><?php echo $eksis; ?></td>
                        <td><?php echo $persentase; ?> %</td>
                       
                     </tr>

                     <?php 
                     }
                     ?>
                     
	
</table>

<br>
<b>2. Jumlah Peserta Didik :</b>
<br>
<br>

<table border="1" width="100%" style="border-collapse: collapse; line-height: 1;" id="table_detail">
<tr style="font-weight:bold;">
    <th style="padding: 6px;" align="center">No.</th>
    <th style="padding: 6px;" align="center">Kelas</th>
    <th style="padding: 6px;" align="center">Jumlah Peserta Didik </th>
</tr>
<?php 
                     $tahun = $this->Reff->tahun();
                     $no=1;
                     $total=0;
                     foreach($this->db->get_where("tm_kelas",array("jenjang"=>$madrasah->jenjang))->result() as $row){
                        $hasil      = $this->db->query("select * from visitasi_pesertadidik where madrasah_id='{$madrasah->id}'")->row();
                        $kolom      = "kelas_".$row->id;
                        $nilainya   = isset($hasil->$kolom) ? $hasil->$kolom :0;
                        $total      = $total + $nilainya;
                       

                      ?>
                       <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $row->nama; ?> (<?php echo $row->satuan; ?>)</td>                       
                        <td><?php echo $nilainya; ?></td>
                      </tr>
                     

                      <?php 

                     }
                     ?>

                    <tr  style="font-weight:bold">
                        
                        <td colspan="2" align="right">Total Hasil Visitasi  </td>                       
                        <td><?php echo $total; ?></td>
                      </tr>
                     
	
</table>

<br>
   Total Jumlah Peserta Didik yang terdata di EMIS  adalah :  <b> <?php echo $data->jml_siswa; ?> Siswa </b>
    
   (<?php
    $hasil      = $this->db->query("select total from visitasi_pesertadidik where madrasah_id='{$data->id}'")->row();
    echo  $persentase = isset($hasil->total) ? number_format(($hasil->total/$data->jml_siswa) * 100,0) :0;
   ?> %)
 

<div style="page-break-before:always;"> </div>

<br>
<b>3. Jumlah Rombongan Belajar :</b>
<br>
<br>

<table border="1" width="100%" style="border-collapse: collapse; line-height: 1;" id="table_detail">
<tr style="height: 32px;">
    <th style="padding: 6px;" align="center">No.</th>
    <th style="padding: 6px;" align="center">Kelas</th>
    <th style="padding: 6px;" align="center">Jumlah Rombongan Belajar (Rombel) </th>
</tr>
<?php 
                     $tahun = $this->Reff->tahun();
                     $no=1;
                     foreach($this->db->get_where("tm_kelas",array("jenjang"=>$madrasah->jenjang))->result() as $row){
                      $hasil      = $this->db->query("select * from visitasi_rombel where madrasah_id='{$madrasah->id}'")->row();
                      $kolom      = "kelas_".$row->id;
                      $nilainya   = isset($hasil->$kolom) ? $hasil->$kolom :0;

                      ?>
                       <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $row->nama; ?> (<?php echo $row->satuan; ?>)</td>                       
                        <td><?php echo $nilainya; ?></td>
                       </tr>
                     

                      <?php 

                     }
                     ?>
                     
	
</table>

<br>
   Total Rombongan Belajar yang terdata di EMIS  adalah :  <b> <?php echo $data->jumlah_rombel; ?> Rombel </b>
    
   (<?php
    $hasil      = $this->db->query("select total from visitasi_rombel where madrasah_id='{$data->id}'")->row();
    echo  $persentase = isset($hasil->total) ? number_format(($hasil->total/$data->jumlah_rombel) * 100,0) :0;
   ?> %)

<br>

<b>4. Jumlah Guru :</b>
<br>
<br>

<table border="1" width="100%" style="border-collapse: collapse; line-height: 1;" id="table_detail">
<tr style="height: 32px;">
    <th style="padding: 6px;" align="center">No.</th>
    <th style="padding: 6px;" align="center">Status</th>
    <th style="padding: 6px;" align="center">Jumlah  </th>
</tr>
<?php 
                     $tahun = $this->Reff->tahun();
                     foreach($this->db->get("master_guru")->result() as $row){
                      $hasil      = $this->db->query("select * from visitasi_guru where madrasah_id='{$madrasah->id}'")->row();
                      $kolom      = $row->kolom;
                      $nilainya   = isset($hasil->$kolom) ? $hasil->$kolom :0;

                      ?>
                       <tr>
                        <td><?php echo $row->id; ?></td>
                        <td><?php echo $row->nama; ?></td>
                        <td><?php echo $nilainya; ?></td>
                            
                     </tr>
                     

                      <?php 

                     }
                     ?>
                     
	
</table>


<br>
   Total Guru yang terdata di EMIS  adalah :  <b> <?php echo $data->jml_guru; ?> Guru </b>
    
   (<?php
    $hasil      = $this->db->query("select (guru_tetap+guru_tidak_tetap) as total from visitasi_guru where madrasah_id='{$data->id}'")->row();
    echo $persentase = isset($hasil->total) ? number_format(($hasil->total/$data->jml_guru) * 100,0) :0;
   ?> %)

<br>
<br>

<b>5. Jumlah Ruang Belajar :</b>
<br>
<br>

<table border="1" width="100%" style="border-collapse: collapse; line-height: 1;" id="table_detail">
<tr style="height: 32px;">
    <th style="padding: 6px;" align="center">No.</th>
    <th style="padding: 6px;" align="center">Kelas</th>
    <th style="padding: 6px;" align="center">Jumlah Ruang Belajar  </th>
</tr>
<?php 
                     $tahun = $this->Reff->tahun();
                     $no=1;
                     foreach($this->db->get_where("tm_kelas",array("jenjang"=>$madrasah->jenjang))->result() as $row){
                        $hasil      = $this->db->query("select * from visitasi_ruangbelajar where madrasah_id='{$madrasah->id}'")->row();
                        $kolom      = "kelas_".$row->id;
                        $nilainya   = isset($hasil->$kolom) ? $hasil->$kolom :0;
                      ?>
                       <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $row->nama; ?> (<?php echo $row->satuan; ?>)</td>                       
                        <td><?php echo $nilainya; ?></td>
                       </tr>
                     

                      <?php 

                     }
                     ?>

                     
	
</table>


<br>
   Total Ruang Belajar yang terdata di EMIS  adalah :  <b> <?php echo $data->rombel; ?> Ruang Belajar </b>
    
   (
   <?php
    $hasil      = $this->db->query("select total from visitasi_ruangbelajar where madrasah_id='{$data->id}'")->row();
    echo $persentase = isset($hasil->total) ? number_format(($hasil->total/$data->rombel) * 100,0) :0;
   ?>%)

<br>
<br>
<b>6. Jumlah Toilet Berfungsi :</b>
<br>
<br>

<table border="1" width="100%" style="border-collapse: collapse; line-height: 1;" id="table_detail">
<tr style="height: 32px;">
    
    <th style="padding: 6px;" align="center">Jenis</th>
    <th style="padding: 6px;" align="center">Jumlah  </th>
</tr>
<?php 
                     $toilet = array("guru","siswa","siswi");

                     foreach($toilet as $row){
                      $hasil      = $this->db->query("select * from visitasi_toilet where madrasah_id='{$madrasah->id}'")->row();
                      $kolom      = $row;
                      $nilainya   = isset($hasil->$kolom) ? $hasil->$kolom :0;
                      ?>
                       <tr>
                        
                        <td> Toilet <?php echo ucwords($row); ?></td>
                        <td><?php echo $nilainya; ?></td>
                            
                    
                     </tr>
                     

                      <?php 

                     }
                     ?>

                     
	
</table>




<br>
Total Jumlah Toilet  yang terdata di EMIS  adalah : <b> <?php echo $data->toilet_total; ?> Toilet  (Toilet Guru : <?php echo $data->toilet_guru; ?>, Toilet Siswa : <?php echo $data->toilet_siswa; ?>) </b>
<?php
    $hasil      = $this->db->query("select (guru+siswa+siswi) as total from visitasi_toilet where madrasah_id='{$data->id}'")->row();
    if(isset($hasil->total)){

        if($hasil->total !=0 and $data->toilet_total !=0 ){
            $persentase =  number_format(($hasil->total/$data->toilet_total) * 100,0);
        }else if($hasil->total ==0 and $data->toilet_total !=0){
            $persentase =  number_format(($hasil->total/$data->toilet_total) * 100,0);

        }else{
          $persentase = 100;
        }
    }else{
         $persentase = 0;
    }
    
    
     
   ?>
    
   (
   <?php
   
    echo $persentase;
   ?>%)

<br>


<div style="page-break-before:always;"> </div>
<b>7. Kesiapan Tatap Muka  :</b>
<br>
<br>

<table border="1" width="100%" style="border-collapse: collapse; line-height: 1;" id="table_detail">
<tr style="height: 32px;">
    
    <th style="padding: 6px;" align="center">Kegiatan</th>
    <th style="padding: 6px;" align="center">Prioritas  </th>
</tr>
<?php 
                     $toilet = $this->db->query("select * from visitasi_tatapmuka where madrasah_id='{$madrasah->id}' and kegiatan_id !=0 order by prioritas ASC")->result();

                     foreach($toilet as $row){
                       
                      ?>
                       <tr>
                        
                        <td>  <?php echo ($row->kegiatan); ?></td>
                        <td>  <?php echo ($row->prioritas); ?></td>
                       
                            
                    
                     </tr>
                     

                      <?php 

                     }
                     ?>

                     
	
</table>


<br>
<b>8. Rencana Kegiatan Kesesuaian Hasil EDM dan eRKAM  :</b>
<br>
<br>

<table border="1" width="100%" style="border-collapse: collapse; line-height: 1;" id="table_detail">
<tr style="height: 32px;">
    
    <th style="padding: 6px;" align="center">Kegiatan</th>
    <th style="padding: 6px;" align="center">Prioritas  </th>
</tr>
<?php 
                     $toilet = $this->db->query("select * from visitasi_tatapmuka where madrasah_id='{$madrasah->id}' and kegiatan_id =0 order by prioritas ASC")->result();

                     foreach($toilet as $row){
                      
                      
                      ?>
                       <tr>
                        
                        <td>  <?php echo ($row->kegiatan); ?></td>
                        <td>  <?php echo ($row->prioritas); ?></td>
                       
                            
                    
                     </tr>
                     

                      <?php 

                     }
                     ?>

                     
	
</table>


<div style="page-break-before:always;"> </div>

<b>9. Rencana Pemanfaatan Dana Bantuan  :</b>
<br>
<br>

<table border="1" width="100%" style="border-collapse: collapse; line-height: 1;" id="table_detail">
<tr style="height: 32px;">
    
    <th style="padding: 6px;" align="center">No</th>
    <th style="padding: 6px;" align="center">Kegiatan</th>
    <th style="padding: 6px;" align="center">Volume  </th>
    <th style="padding: 6px;" align="center">Satuan  </th>
    <th style="padding: 6px;" align="center">Harga Satuan  </th>
    <th style="padding: 6px;" align="center">Jumlah Biaya  </th>
</tr>
<?php 
                                                   $kegiatan = $this->db->get_where("visitasi_kegiatan",array("madrasah_id"=>$madrasah->id))->result();
                                                   $no=1;   
                                                   $total = 0;
                                                   foreach($kegiatan as $rk){
                                                      $total = $total + $rk->biaya;

                                                         ?>

                                                                <tr>
                                                                  <td><?php echo $no++; ?></td>
                                                                  <td><?php echo $rk->kegiatan; ?></td>
                                                                  <td><?php echo $rk->volume; ?></td>
                                                                  <td><?php echo $rk->satuan; ?></td>
                                                                  <td><?php echo $this->Reff->formatuang2($rk->harga); ?></td>
                                                                  <td><?php echo $this->Reff->formatuang2($rk->biaya); ?></td>
                                                                </tr>




                                                         <?php



                                                      }
                                                   ?>

                                                   <tr style="font-weight:bold">
                                                      <td colspan="5"> Total Kegiatan </td>
                                                      <td> <?php echo $this->Reff->formatuang2($total); ?> </td>
                                                   
                                                   </tr>

                     
	
</table>



