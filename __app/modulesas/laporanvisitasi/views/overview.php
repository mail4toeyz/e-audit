<div class="row">
<div class="col-xl-6">
<div class="card mb-5 mb-xl-10" id="kt_profile_details_view">
									<!--begin::Card header-->
									<div class="card-header cursor-pointer">
										<!--begin::Card title-->
										<div class="card-title m-0">
											<h3 class="fw-bolder m-0">Informasi Detail  </h3>
										</div>
										<!--end::Card title-->
										<!--begin::Action-->
										
									</div>
									<!--begin::Card header-->
									<!--begin::Card body-->
									<div class="card-body p-9">
										
										<!--begin::Row-->
										<div class="row mb-7">
											<!--begin::Label-->
											<label class="col-lg-4 fw-bold text-muted">NIK Asesor </label>
											<!--end::Label-->
											<!--begin::Col-->
											<div class="col-lg-8">
												<span class="fw-bolder fs-6 text-gray-800"><?php echo $asesor->nik; ?></span>
											</div>
											<!--end::Col-->
										</div>
										<!--end::Row-->
										<!--begin::Input group-->
										<div class="row mb-7">
											<!--begin::Label-->
											<label class="col-lg-4 fw-bold text-muted">Nama Asesor </label>
											<!--end::Label-->
											<!--begin::Col-->
											<div class="col-lg-8">
												<span class="fw-bolder fs-6 text-gray-800"><?php echo $asesor->nama; ?></span>
											</div>
											<!--end::Col-->
										</div>

                                        <div class="row mb-7">
											<!--begin::Label-->
											<label class="col-lg-4 fw-bold text-muted">Kontak  </label>
										    <!--begin::Col-->
											<div class="col-lg-8 d-flex align-items-center">
												<span class="fw-bolder fs-6 text-gray-800 me-2"><?php echo $asesor->whatsapp; ?> <?php echo $asesor->email; ?> </span>
												<span class="badge badge-success">Verified</span>
											</div>
											<!--end::Col-->
										</div>
                                        <div class="row mb-7">
											<!--begin::Label-->
											<label class="col-lg-4 fw-bold text-muted">Jabatan  </label>
											<!--end::Label-->
											<!--begin::Col-->
											<div class="col-lg-8">
												<span class="fw-bolder fs-6 text-gray-800"><?php echo $asesor->jabatan; ?> <?php echo $this->Reff->get_kondisi(array("kode"=>$asesor->jabatan_provinsi_id),"provinsi","nama"); ?> <?php echo $this->Reff->get_kondisi(array("kode"=>$asesor->jabatan_kabupaten_id),"kota","nama"); ?></span>
											</div>
											<!--end::Col-->
										</div>

                                        <div class="row mb-7">
											<!--begin::Label-->
											<label class="col-lg-4 fw-bold text-muted">Data Madrasah yang divisitasi   </label>
											<!--end::Label-->
											<!--begin::Col-->
											<div class="col-lg-8">
												<span class="fw-bolder fs-6 text-gray-800"><?php echo $data->nsm; ?> - <?php echo $data->nama; ?></span>
											</div>
											<!--end::Col-->
										</div>

                                       
										<!--end::Input group-->
										<!--begin::Input group-->
										
										<!--end::Input group-->
										<!--begin::Input group-->
										<div class="row mb-7">
											<!--begin::Label-->
											<label class="col-lg-4 fw-bold text-muted">Alamat </label>
											<!--end::Label-->
											<!--begin::Col-->
											<div class="col-lg-8">
												<a href="#" class="fw-bold fs-6 text-gray-800 text-hover-primary"><?php echo $data->provinsi; ?> - <?php echo $data->kota; ?> </a>
											</div>
											<!--end::Col-->
										</div>
										<!--end::Input group-->
										<!--begin::Input group-->

                                       
										<!--end::Notice-->
									</div>
									<!--end::Card body-->
								</div>
	</div>

	<div class="col-xl-6">
	  <div id="chartnyaspider"></div>
	</div>

	<?php 
                                         $visitasi_jadwal = $this->db->get_where("visitasi_jadwal",array("nik"=>$asesor->nik,"nsm"=>$data->nsm))->row();
                                         if(is_null($visitasi_jadwal)){
                                        ?>
										<div class="notice d-flex bg-light-warning rounded border-warning border border-dashed p-6">
											<!--begin::Icon-->
											<!--begin::Svg Icon | path: icons/duotune/general/gen044.svg-->
											<span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
													<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
													<rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black" />
													<rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black" />
												</svg>
											</span>
											<!--end::Svg Icon-->
											<!--end::Icon-->
											<!--begin::Wrapper-->
											<div class="d-flex flex-stack flex-grow-1">
												<!--begin::Content-->
												<div class="fw-bold">
													<h4 class="text-gray-900 fw-bolder">Asesor Belum Mengisi Madrasah Visitasi Pada Aplikasi AMAN </h4>
													<div class="fs-6 text-gray-700"> Oke, coba kami cek disini 
													<a class="fw-bolder" href="javascript:void(0)" id="cekAman">Cek Data Ke AMAN </a>.</div>
												</div>
												<!--end::Content-->
											</div>
											<!--end::Wrapper-->
										</div>
                                    <?php 
                                         }else{
                                            ?>

                                    <div class="notice d-flex bg-light-success rounded border-success border border-dashed p-6">
											<!--begin::Icon-->
											<!--begin::Svg Icon | path: icons/duotune/general/gen044.svg-->
											<span class="svg-icon svg-icon-2tx svg-icon-success me-4">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
													<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
													<rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black" />
													<rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black" />
												</svg>
											</span>
											<!--end::Svg Icon-->
											<!--end::Icon-->
											<!--begin::Wrapper-->
											<div class="d-flex flex-stack flex-grow-1">
												<!--begin::Content-->
												<div class="fw-bold">
													<h4 class="text-gray-900 fw-bolder">Asesor Melakukan Visitasi pada <?php echo $this->Reff->formattanggalstring($visitasi_jadwal->tanggal_mulai); ?> <?php echo $visitasi_jadwal->jam_mulai; ?> Sampai <?php echo $this->Reff->formattanggalstring($visitasi_jadwal->tanggal_selesai); ?> <?php echo $visitasi_jadwal->jam_selesai; ?> </h4>
													<div class="fs-6 text-gray-700"> Oke, pastikan sama dengan data di Aman
													<a class="fw-bolder" href="javascript:void(0)" id="cekAman">Cek Data Ke AMAN </a>.</div>
												</div>
												<!--end::Content-->
											</div>
											<!--end::Wrapper-->
										</div>


                                            <?php 

                                         }
                                    ?>
</div>

<?php 
 $edmTerdata  = $this->db->query("select (nilai_kedisiplinan+nilai_pengembangan_diri+nilai_proses_pembelajaran+nilai_sarana_prasarana+nilai_pembiayaan) as jml from madrasahedm where id='{$data->id}'")->row();
 $edmVisitasi = $this->db->query("select (nilai_kedisiplinan+nilai_pengembangan_diri+nilai_proses_pembelajaran+nilai_sarana_prasarana+nilai_pembiayaan) as jml from visitasi_edm where madrasah_id='{$data->id}'")->row();
 $edmTerdatares  = round($edmTerdata->jml);
 $edmVisitasires = round($edmVisitasi->jml);
 $edmPersen      = ($edmVisitasires/$edmTerdatares) *100;

 $siswa          = $this->db->query("select total from visitasi_pesertadidik where madrasah_id='{$data->id}'")->row();
 $siswaPersen    = isset($siswa->total) ? (($siswa->total/$data->jml_siswa) * 100) :0;

 $rombel       = $this->db->query("select total from visitasi_rombel where madrasah_id='{$data->id}'")->row();
 $rombelPersen = isset($rombel->total) ? number_format(($rombel->total/$data->jumlah_rombel) * 100,0) :0;

 $guru       = $this->db->query("select (guru_tetap+guru_tidak_tetap) as total from visitasi_guru where madrasah_id='{$data->id}'")->row();
 $guruPersen = isset($guru->total) ? number_format(($guru->total/$data->jml_guru) * 100,0) :0;

 $rubel      = $this->db->query("select total from visitasi_ruangbelajar where madrasah_id='{$data->id}'")->row();
 $rubelPersen = isset($rubel->total) ? number_format(($rubel->total/$data->rombel) * 100,0) :0;

 $toilet      = $this->db->query("select (guru+siswa+siswi) as total from visitasi_toilet where madrasah_id='{$data->id}'")->row();
 if(isset($toilet->total)){

	 if($toilet->total !=0 and $data->toilet_total !=0 ){
		 $toiletPersen =  number_format(($toilet->total/$data->toilet_total) * 100,0);
	 }else if($toilet->total ==0 and $data->toilet_total !=0){
		 $toiletPersen =  number_format(($toilet->total/$data->toilet_total) * 100,0);

	 }else{
	   $toiletPersen = 100;
	 }
 }else{
	  $toiletPersen = 0;
 }
 ?>
<script type="text/javascript">

Highcharts.chart('chartnyaspider', {

chart: {
	polar: true,
	type: 'line'
},


title: {
	text: 'Terdata vs Hasil Visitasi ' ,
	x: -80
},

pane: {
	size: '80%'
},

xAxis: {
	categories: ['SKPM EDM', 'Peserta Didik', 'Rombongan Belajar', 'Guru',
		'Ruang Belajar', 'Toilet Berfungsi'],
	tickmarkPlacement: 'on',
	lineWidth: 0
},

yAxis: {
	gridLineInterpolation: 'polygon',
	lineWidth: 0,
	min: 0
},

tooltip: {
	shared: true,
	pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
},

legend: {
	align: 'right',
	verticalAlign: 'middle',
	layout: 'vertical'
},

series: [{
	name: 'Tingkat Perbandingan (%)',
	data: [<?php echo ($edmPersen); ?>, <?php echo $siswaPersen; ?>, <?php echo $rombelPersen; ?>, <?php echo $guruPersen; ?>, <?php echo $rubelPersen; ?>, <?php echo $toiletPersen; ?>],
	pointPlacement: 'on'
},{
	name: 'Terdata',
	data: [<?php echo ($edmTerdatares); ?>, <?php echo $data->jml_siswa; ?>, <?php echo $data->jumlah_rombel; ?>, <?php echo $data->jml_guru; ?>, <?php echo $data->rombel; ?>, <?php echo $data->toilet_total; ?>],
	pointPlacement: 'on'
}, {
	name: 'Hasil Visitasi',
	data: [<?php echo ($edmVisitasires); ?>, <?php echo $siswa->total; ?>, <?php echo $rombel->total; ?>, <?php echo $guru->total; ?>, <?php echo $rubel->total; ?>, <?php echo $toilet->total; ?>],
	pointPlacement: 'on'
}
],

responsive: {
	rules: [{
		condition: {
			maxWidth: 500
		},
		chartOptions: {
			legend: {
				align: 'center',
				verticalAlign: 'bottom',
				layout: 'horizontal'
			},
			pane: {
				size: '70%'
			}
		}
	}]
}

});

</script>
<script type="text/javascript">

   $(document).off("click","#cekAman").on("click","#cekAman",function(){

      
       var nik = "<?php echo $asesor->nik; ?>";
       var nsm = "<?php echo $data->nsm; ?>";
         $.post("<?php echo site_url('login/cekAsesorVisitasi'); ?>",{nik:nik,nsm:nsm},function(data){

            alert(data);
         })
   })


</script>