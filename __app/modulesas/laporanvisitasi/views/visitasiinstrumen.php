<div class="card card-custom">
									<div class="card-header">
										<div class="card-title">
											<h3 class="card-label">LAPORAN VISITASI  <b><?php echo $data->nama; ?> (<?php echo $data->nsm; ?>)</b>
											</h3>
										</div>
									
									</div>
									<div class="card-body">

                                    <div class="row">
                                    <div class="col-xl-3">
                                    <ul class="nav flex-column nav-pills">
                                        <li class="nav-item mb-2">
                                            <a class="nav-link active" id="EDM-tab-5" data-toggle="tab" href="#EDM">
                                                <span class="nav-icon">
                                                <i class="fa fa-calculator" aria-hidden="true"></i>
                                            </span>
                                            <span class="nav-text">SKPM Evaluasi Diri Madrasah</span>
                                        </a>
                                        </li> 
                                        <li class="nav-item mb-2">
                                            <a class="nav-link " id="jpd-tab-5" data-toggle="tab" href="#jpd">
                                                <span class="nav-icon">
                                                <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                            </span>
                                            <span class="nav-text">Jumlah Peserta Didik</span>
                                        </a>
                                        </li> 
                                        <li class="nav-item mb-2">
                                            <a class="nav-link " id="jrb-tab-5" data-toggle="tab" href="#jrb">
                                                <span class="nav-icon">
                                                <i class="fa fa-building-o" aria-hidden="true"></i>

                                            </span>
                                            <span class="nav-text">Jumlah Rombongan Belajar</span>
                                        </a>
                                        </li> 

                                        <li class="nav-item mb-2">
                                            <a class="nav-link " id="jmlguru-tab-5" data-toggle="tab" href="#jmlguru">
                                                <span class="nav-icon">
                                                <i class="fa fa-users" aria-hidden="true"></i>

                                            </span>
                                            <span class="nav-text">Jumlah Guru</span>
                                        </a>
                                        </li> 

                                        <li class="nav-item mb-2">
                                            <a class="nav-link " id="jmlruang-tab-5" data-toggle="tab" href="#jmlruang">
                                                <span class="nav-icon">
                                                <i class="fa fa-bank" aria-hidden="true"></i>

                                            </span>
                                            <span class="nav-text">Jumlah Ruang Belajar</span>
                                        </a>
                                        </li> 

                                        <li class="nav-item mb-2">
                                            <a class="nav-link " id="jmltoilet-tab-5" data-toggle="tab" href="#jmltoilet">
                                                <span class="nav-icon">
                                                <i class="fa fa-child" aria-hidden="true"></i>


                                            </span>
                                            <span class="nav-text">Jumlah Toilet Berfungsi</span>
                                        </a>
                                        </li> 

                                        <li class="nav-item mb-2">
                                            <a class="nav-link " id="tatapmuka-tab-5" data-toggle="tab" href="#tatapmuka">
                                                <span class="nav-icon">
                                                <i class="fa fa-check-square-o" aria-hidden="true"></i>



                                            </span>
                                            <span class="nav-text">Kesiapan Tatap Muka </span>
                                        </a>
                                        </li> 
                                        <li class="nav-item mb-2">
                                            <a class="nav-link " id="erkam-tab-5" data-toggle="tab" href="#erkam">
                                                <span class="nav-icon">
                                                <i class="fa fa-money" aria-hidden="true"></i>



                                            </span>
                                            <span class="nav-text">Rencana Kegiatan </span>
                                        </a>
                                        </li>


                                    </ul>
                                    </div>														
                                                                                                    
                                    <div class="col-xl-9">
                                            <div class="tab-content" id="myTabContent5">
                                            
                                            <div class="tab-pane fade show active" id="EDM" role="tabpanel" aria-labelledby="EDM-tab-5"><?php $this->load->view("edm"); ?></div>
                                            <div class="tab-pane fade" id="jpd" role="tabpanel" aria-labelledby="jpd-tab-5"><?php $this->load->view("pesertadidik"); ?></div>
                                            <div class="tab-pane fade" id="jrb" role="tabpanel" aria-labelledby="jrb-tab-5"><?php $this->load->view("rombonganbelajar"); ?></div>
                                            <div class="tab-pane fade" id="jmlguru" role="tabpanel" aria-labelledby="jmlguru-tab-5"><?php $this->load->view("jumlahguru"); ?></div>
                                            <div class="tab-pane fade" id="jmlruang" role="tabpanel" aria-labelledby="jmlruang-tab-5"><?php $this->load->view("ruangbelajar"); ?></div>
                                            <div class="tab-pane fade" id="jmltoilet" role="tabpanel" aria-labelledby="jmltoilet-tab-5"><?php $this->load->view("toilet"); ?></div>
                                            <div class="tab-pane fade" id="tatapmuka" role="tabpanel" aria-labelledby="tatapmuka-tab-5"><?php $this->load->view("tatapmuka"); ?></div>
                                            <div class="tab-pane fade" id="erkam" role="tabpanel" aria-labelledby="erkam-tab-5"><?php $this->load->view("erkam"); ?></div>
                                            
                                            </div>
                                  </div>	
                                    
                                    
                                </div> <!-- row -->


                                </div> <!-- card body -->
										
				</div>
								