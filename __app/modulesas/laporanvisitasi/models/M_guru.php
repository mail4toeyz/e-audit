<?php

class M_guru extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
    }

    
	public function grid($paging){
		$tahun   = $this->Reff->tahun();

	    $status = $this->input->get_post("status");
	    $jenjang = $this->input->get_post("jenjang");
	    $provinsi = $this->input->get_post("provinsi");
	    $kota    = $this->input->get_post("kota");
	    $keyword  = $this->input->get_post("keyword");
	    $bantuan  = $this->input->get_post("bantuan");
	    $statusVisit  = $this->input->get_post("statusVisit");

		$this->db->select("*");
        $this->db->from('madrasahedm');
		$this->db->where("tahun",$tahun);
		$this->db->where("shortlist",1);
		
    	
	    if(!empty($bantuan)){  $this->db->where("bantuan",$bantuan);    }
	    if(!empty($jenjang)){  $this->db->where("jenjang",$jenjang);    }
	    if(!empty($provinsi)){  $this->db->where("provinsi_id",$provinsi);    }
	    if(!empty($kota)){  $this->db->where("kota_id",$kota);    }
	    if(!empty($keyword)){  $this->db->where("(UPPER(nama) LIKE '%".strtoupper($keyword)."%' OR UPPER(nsm) LIKE '%".strtoupper($keyword)."%')");    }
		

		if($status==1){
			$this->db->where("pernyataan",1);
		}else if($status==2){

			$this->db->where("pernyataan !=",1);
		}

		if($statusVisit==1){
			$this->db->where("id IN(select madrasah_id from visitasi_kegiatan)");
		}else if($status==2){

			$this->db->where("id NOT IN(select madrasah_id from visitasi_kegiatan)");
		}

		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("skor_akhir_visitasi","ASC");
					 $this->db->order_by("skor_akhir","ASC");
					
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
	
	public function insert(){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
	    $id               = $this->Reff->get_max_id2("id","tm_guru");
	                         $foldernama  = $_SESSION['tmmadrasah_id'];
				
											if (!is_dir('__statics/upload/'.$foldernama)) {
												mkdir('./__statics/upload/'.$foldernama, 0777, TRUE);

											}
											
								
								  
								$foldernama  =  $_SESSION['tmmadrasah_id']."/guru/".$id;		
											if (!is_dir('__statics/upload/'.$foldernama)) {
												mkdir('./__statics/upload/'.$foldernama, 0777, TRUE);

											}
							$folder   					= '__statics/upload/'.$foldernama;
							
							
							
							
		$this->db->set("tmmadrasah_id",$_SESSION['tmmadrasah_id']);
		$this->db->set("id",$id);
		$this->db->set("folder",$folder);
		$this->db->set("tgl_lahir",$this->Reff->formattanggaldb($_POST['tgl_lahir']));
		$this->db->set("password",encrypt((($_POST['password']))));
		$this->db->set("alias",$_POST['password']);
		$this->db->set("i_entry",$_SESSION['nama_madrasah']);
		$this->db->set("d_entry",date("Y-m-d H:i:s"));
        
		$this->db->insert("tm_guru",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
	public function update($id){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
		 $foldernama  = $_SESSION['tmmadrasah_id'];
				
											if (!is_dir('__statics/upload/'.$foldernama)) {
												mkdir('./__statics/upload/'.$foldernama, 0777, TRUE);

											}
											
								
								  
								$foldernama  =  $_SESSION['tmmadrasah_id']."/guru/".$id;		
											if (!is_dir('__statics/upload/'.$foldernama)) {
												mkdir('./__statics/upload/'.$foldernama, 0777, TRUE);

											}
							$folder   					= '__statics/upload/'.$foldernama;
							
							
		$this->db->set("folder",$folder);
		
		
		
	    $this->db->set("tgl_lahir",$this->Reff->formattanggaldb($_POST['tgl_lahir']));
		$this->db->set("password",encrypt((($_POST['password']))));
		$this->db->set("i_update",$_SESSION['nama_madrasah']);
		$this->db->set("d_update",date("Y-m-d H:i:s"));
		$this->db->where("id",$id);
		$this->db->update("tm_guru",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
}
