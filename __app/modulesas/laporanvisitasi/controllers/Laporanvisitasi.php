<?php
ini_set('memory_limit', '-1');

defined('BASEPATH') OR exit('No direct script access allowed');

class Laporanvisitasi extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		if(!$this->session->userdata("isKantor")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_guru','m');
		
	  }
	  
   function _template($data)
	{
		$this->load->view(''.$_SESSION['isKantor'].'/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Hasil Visitasi Madrasah ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		  error_reporting(0);
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		  $arbantuan = array("1"=>"Kinerja","2"=>"Afirmasi");
		  
		   $i= ($iDisplayStart +1);
		   $status= $_POST['status'];
		   $arbantuan = array("1"=>"<span class='badge badge-light-warning'>Kinerja</span>","2"=>"<span class='badge badge-light-primary'>Afirmasi</span>");
		   $status =  array("0"=>"<span class='fa fa-times' style='color:red'></span>","1"=>"<span class='fa fa-check-square-o primary' style='color:blue'></span>");
	 

		   foreach($datagrid as $val) {
			    
				$no = $i++;
				$visitasi ="";
				  $cekVisitasi = $this->db->query("select id from visitasi_kegiatan where madrasah_id='{$val['id']}' limit 1")->row();
				  if(!is_null($cekVisitasi)){
					$visitasi ='<button href="'.site_url('hasilvisitasi/detail?id='.$val['id'].'').'" class="btn btn-primary btn-sm menuajax" >Selesai </button>';
				  }else{

					$visitasi ="<span class='badge badge-light-danger'> Belum  </button>";
				  }

				  $cekAsesor = $this->db->query("select asesor from visitasi_edm where madrasah_id='{$val['id']}' limit 1")->row();

				  $asesor    = $this->db->get_where("asesor",array("id"=>$cekAsesor->asesor))->row();
				  $aman      = $this->db->get_where("visitasi_jadwal",array("nsm"=>$val['nsm']))->row();
				  


				  $edm       = $this->db->query("select * from visitasi_edm where madrasah_id='{$val['id']}'  AND nilai_pembiayaan IS NOT NULL AND nilai_proses_pembelajaran IS NOT NULL LIMIT 1")->row();
				 
				  $pd        = $this->db->query("select total from visitasi_pesertadidik where madrasah_id='{$val['id']}' ")->row();
				  $rombel    = $this->db->query("select total from visitasi_rombel where  madrasah_id='{$val['id']}' ")->row();
				  $guru      = $this->db->query("select (guru_tetap+guru_tidak_tetap) as total from visitasi_guru where madrasah_id='{$val['id']}'  ")->row();
				  $ruangbelajar   = $this->db->query("select total from visitasi_ruangbelajar where  madrasah_id='{$val['id']}' ")->row();
				  $toiletsiswa         = $this->db->query("select (siswa+siswi) as total from visitasi_toilet where madrasah_id='{$val['id']}'  ")->row();
				  $toiletguru          = $this->db->query("select (guru) as total from visitasi_toilet where madrasah_id='{$val['id']}'  ")->row();
				 


			
				$records["data"][] = array(
					$no,										
				
					$visitasi,
					$arbantuan[$val['bantuan']],				
					$val['nsm'],
					$val['nama'],
					$val['provinsi'],
					$val['kota'],
					$asesor->nama,
					
					
					$val['rank'],
					
					number_format($val['skor_akhir_visitasi'],4),
					number_format($val['skor_edm_visitasi'],1),
					number_format($val['skor_pip'],1),
					number_format($val['skor_rombel_visitasi'],1),
					number_format($val['skor_toilet_visitasi'],1),

					number_format($val['skor_akhir'],4),
					number_format($val['skor_edm'],1),
					number_format($val['skor_pip'],1),
					number_format($val['skor_rombel'],1),
					number_format($val['skor_toilet'],1),
					round($val['nilai_kedisiplinan'],1),
					$edm->nilai_kedisiplinan,

					
					round($val['nilai_pengembangan_diri'],1),
					$edm->nilai_pengembangan_diri,

					
					round($val['nilai_proses_pembelajaran'],1),
					$edm->nilai_proses_pembelajaran,
					
				
					round($val['nilai_sarana_prasarana'],1),
					$edm->nilai_sarana_prasarana,

					
					round($val['nilai_pembiayaan'],1),
					$edm->nilai_pembiayaan,


					$val['jml_siswa'],
					$pd->total,

					$val['jml_guru'],
					$guru->total,

					$val['jumlah_rombel'],
					$rombel->total,

					$val['rombel'],
					$ruangbelajar->total,

					$val['toilet_guru'],
					$toiletguru->total,

					$val['toilet_guru'],
					$toiletsiswa->total
					
					
					

					
					
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
		public function detail(){
			$id            = $this->input->get_post("id",true);
			
			$data['data']    = $this->db->get_where("madrasahedm",array("id"=>$id))->row();
			$cekAsesor       = $this->db->query("select asesor from visitasi_edm where madrasah_id='{$id}' limit 1")->row();

			$data['asesor']  = $this->db->get_where("asesor",array("id"=>$cekAsesor->asesor))->row();

			$ajax            = $this->input->get_post("ajax",true);	
				$data['title']   = "Dokumentasi Visitasi";
				
				if(!empty($ajax)){
								
					$this->load->view('detail',$data);
				
				}else{
					
					
					$data['konten'] = "detail";
					
					$this->_template($data);
				}


		}



		public function bukti(){

			$this->load->helper('exportpdf_helper'); 

			
			$data['madrasah'] = $this->db->query("select * from madrasahedm where nsm='{$_GET['nsm']}' and id  IN(select madrasah_id from visitasi_kegiatan)")->row();
			$data['data']     = $this->db->query("select * from madrasahedm where nsm='{$_GET['nsm']}' and id  IN(select madrasah_id from visitasi_kegiatan)")->row();
			$data['petugas'] = $this->db->get_where("asesor",array("nik"=>$_GET['nik']))->row();
			$data['aman'] = $this->db->get_where("visitasi_jadwal",array("nik"=>$_GET['nik'],"nsm"=>$_GET['nsm']))->row();
			$user_info = $this->load->view('bukti', $data, true);
			$pdf_filename = 'Hasil Visitasi'.$data['data']->nsm.'.pdf';	 
			
			 $output = $user_info;
			
			generate_pdf($output, $pdf_filename);
      }
	
	
	 
}
