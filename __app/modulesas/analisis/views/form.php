<style>
.btn-file {
  position: relative;
  overflow: hidden;
}
.btn-file input[type=file] {
  position: absolute;
  top: 0;
  right: 0;
  min-width: 100%;
  min-height: 100%;
  font-size: 100px;
  text-align: right;
  filter: alpha(opacity=0);
  opacity: 0;
  outline: none;
  background: white;
  cursor: inherit;
  display: block;
}
</style>
						
						 <form action="javascript:void(0)" method="post" id="simpangambarmodal" name="simpangambarmodal" url="<?php echo site_url("datasiswa/save"); ?>">
						<input type="hidden" class="form-control"  name='id' value="<?php echo (isset($data)) ? $data->id :""; ?>" data-toggle="tooltip" title="Isi dengan Nama  Organisasi">
							
                           <div class="row">
                           <div class="col-md-6">
                         
						   
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                            <input type="text" class="form-control" onkeypress="javascript:return isNumber(event)" id="nisn" name="f[nisn]" maxlength="10" value="<?php echo (isset($data)) ? $data->nisn :""; ?>" placeholder="NISN/M " >
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                            <input type="text" class="form-control" id="nama" name="f[nama]" value="<?php echo (isset($data)) ? $data->nama :""; ?>" placeholder="Nama Siswa" >
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                           <select class="form-control  "  name="f[kelas]" urlnya="<?php echo site_url("publik/rombel"); ?>" target="rombel" style="color:black">
																 <option value=""> Pilih Kelas </option>
																 <?php 
															   $kelas = $this->db->get("tm_kelas")->result();
																 foreach($kelas as $i=>$r){
																	 
																	?><option value="<?php echo $r->id; ?>" <?php if(isset($data)){ echo ($data->kelas==$r->id) ? "selected":""; } ?>> <?php echo ($r->nama); ?></option><?php  
																	 
																 }
															?>
											</select>
											
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                        <select class="form-control " required   name="f[tmmadrasah_id]" style="color:black">
																 <option value=""> Pilih Madrasah </option>
																 <?php 
															 $madrasah = $this->db->query(" select * from tm_madrasah order by nama asc")->result();
																 foreach($madrasah as $i=>$r){
																	 
																	?><option value="<?php echo $r->id; ?>"  <?php if(isset($data)){ echo ($data->tmmadrasah_id==$r->id) ? "selected":""; } ?>> <?php echo ($r->nama); ?></option><?php  
																	 
																 }
															?>
											</select>
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								
								
								
								
								
							</div>
							
							<div class="col-md-6">
							
							<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                            <input type="password" class="form-control" id="password" name="password" value="<?php echo (isset($data)) ? dekrip($data->password) :""; ?>" placeholder="Buat Password" >
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                            <input type="text" class="form-control" id="password" name="f[tempat]" value="<?php echo (isset($data)) ? $data->tempat :""; ?>" placeholder="Tempat Lahir" >
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
										<input type="text" class="form-control" id="tgl_lahir"  name="f[tgl_lahir]" value="<?php echo (isset($data)) ? $data->tgl_lahir :""; ?>" placeholder="Tgl Lahir" >
                                             <script>
																$(document).ready(function () {
																	startdate = new Date() - 18;
																	$('#tgl_lahir').datepicker({
																		beforeShow: function(input, inst) {
																				$(document).off('focusin.bs.modal');
																			},
																			onClose:function(){
																				$(document).on('focusin.bs.modal');
																			},
																			 changeMonth: true,
																								changeYear: true,
																								autoclose: true,
																								dateFormat: 'dd-mm-yy',
																								yearRange: "1945:2020",
																							
																	});
																});
																
																
																	$.fn.modal.Constructor.prototype.enforceFocus = function () {
																	$(document)
																	  .off('focusin.bs.modal') // guard against infinite focus loop
																	  .on('focusin.bs.modal', $.proxy(function (e) {
																		if (this.$element[0] !== e.target && !this.$element.has(e.target).length) {
																		  this.$element.focus()
																		}
																	  }, this))
																	}
															</script>
										</div>
										
										
                                       </div>
                                    </div>
                                    
                                 </div>
								 
								
								
							
							
							</div>
								<!--
								
							<div class="col-md-6">
						   <h4> DATA ORANG TUA </h4>
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                            <input type="text" class="form-control" onkeypress="javascript:return isNumber(event)"  name="f[nik_ayah]" value="<?php echo (isset($data)) ? $data->nik_ayah :""; ?>" placeholder="NIK Ayah " >
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                            <input type="text" class="form-control" id="nama" name="f[nama_ayah]" value="<?php echo (isset($data)) ? $data->nama_ayah :""; ?>" placeholder="Nama Ayah" >
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                           <select class="form-control  "  name="f[pekerjaan_ayah]" >
																 <option value=""> Pilih Pekerjaan Ayah </option>
																 <?php 
															  $gender = $this->Reff->get_pekerjaan();
																 foreach($gender as $i=>$r){
																	 
																	?><option value="<?php echo $r; ?>" <?php if(isset($data)){ echo ($data->pekerjaan_ayah==$r) ? "selected":""; } ?>> <?php echo ($r); ?></option><?php  
																	 
																 }
															?>
											</select>
											
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                            <input type="text" class="form-control" onkeypress="javascript:return isNumber(event)"  name="f[nik_ibu]" value="<?php echo (isset($data)) ? $data->nik_ibu :""; ?>" placeholder="NIK Ibu " >
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                            <input type="text" class="form-control" id="nama" name="f[nama_ibu]" value="<?php echo (isset($data)) ? $data->nama_ibu :""; ?>" placeholder="Nama Ibu" >
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                           <select class="form-control  "  name="f[pekerjaan_ibu]" >
																 <option value=""> Pilih Pekerjaan Ibu </option>
																 <?php 
															  $gender = $this->Reff->get_pekerjaan();
																 foreach($gender as $i=>$r){
																	 
																	?><option value="<?php echo $r; ?>" <?php if(isset($data)){ echo ($data->pekerjaan_ibu==$r) ? "selected":""; } ?>> <?php echo ($r); ?></option><?php  
																	 
																 }
															?>
											</select>
											
                                        </div>
                                    </div>
                                    
                                 </div> 
                                </div>
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                            <textarea class="form-control" id="password" name="f[alamat_ortu]" placeholder="Alamat Orang tua" ><?php echo (isset($data)) ? $data->alamat_ortu :""; ?></textarea>
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								
						</div>
								-->
							
							</div>
							
								
								 
								 
								 <div class=" " id="aksimodal">
						       <center>
								   <button type="submit" class="btn btn-success btn-sm">
										<i class="fa fa-save"></i>
										<span>Simpan Data  </span>
                                    </button>
								    <button type="reset" class="btn btn-success btn-sm">
									
										<span>Reset   </span>
                                    </button>
									<button type="button" class="btn btn-success btn-sm" data-dismiss="modal">
										
										<span>Tutup   </span>
                                    </button>
									
								   </center>
								
								 </div>
								 
								  <div id="loadingmodal" style="display:none">
								      <div class="col-sm-12">
								  <center>  <i class="fa  fa-spinner fa-spin"></i>   <br> Data sedang disimpan, Mohon Tunggu ...</center>
								   </div>
								 </div>
							  
					</form>	    
	<script>
	$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
	
	
	
    });
	
	function readURL(input) {
	  if (input.files && input.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function(e) {
		  $('#blah').attr('src', e.target.result);
		}
		
		reader.readAsDataURL(input.files[0]);
	  }
	}

	$("#imgInp").change(function() {
	  readURL(this);
	});

	</script>