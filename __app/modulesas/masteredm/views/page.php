
	<div class="card card-custom">
						<div class="card-header">
												<div class="card-title">
													<span class="card-icon">
														<i class="flaticon2-chat-1 text-primary"></i>
													</span>
													<h3 class="card-label">Data Calon Penerima BKBA 
													</h3>
												</div>
												<div class="card-toolbar">
													<a href="<?php echo site_url("pengaturan/generate_pip"); ?>" target="_blank" class="btn btn-sm btn-primary font-weight-bold">
													<i class="fa fa-database"></i>Combine PIP </a>

													<a href="<?php echo site_url("pengaturan/generate_emis"); ?>" target="_blank" class="btn btn-sm btn-primary font-weight-bold">
													<i class="fa fa-database"></i>Combine EMIS </a>


												</div>
											
						                     
						</div>
						  
						 <div class="card-body">

						 <div class="row">
													
													
						                         <div class="col-md-2">
													  <select class="form-control" id="status">
														   <option value="">- Status  - </option>
														   <option value="5" selected> Calon Penerima </option>
														   <option value="1"> Masuk Longlist </option>
														   <option value="2"> Tidak Masuk Longlist </option>
														   <option value="3"> Masuk Shortlist </option>
														   <option value="4"> Tidak Masuk Shortlist </option>
														   <option value="6"> Sudah menerima BKBA </option>
														   <option value="7"> Sudah menerima SIMSARPRAS </option>
														   <option value="9"> Sudah menerima PUPR </option>
														   <option value="8"> Madrasah Inklusi </option>
														  
														  
														  
													   
													   </select>
												   
													</div>
													


													<div class="col-md-2">
													  <select class="form-control" id="jenjang">
														   <option value="">- Jenjang - </option>
														   <?php 
															 $jenjang = array("mi","mts","ma");
															   foreach($jenjang as $r){
																   ?><option value="<?php echo $r; ?>"  > <?php echo strtoupper($r); ?> </option><?php 
																   
															   }
															  ?>
														  
													   
													   </select>
												   
													</div>
													
													
													<div class="col-md-2">
												
													  <select class="form-control onchange " id="provinsi" target="kota" urlnya="<?php echo site_url('publik/kota'); ?>">
														   <option value="">- Provinsi - </option>
														   <?php 
															   $provinsi = $this->db->get("provinsi")->result();
															   foreach($provinsi as $row){
																   ?><option value="<?php echo strtoupper($row->id); ?>"> <?php echo ($row->nama); ?> </option><?php 
																   
															   }
															  ?>
													   
													   </select>
												   
													</div>

													<div class="col-md-2">
												
													  <select class="form-control kota " id="kota" target="kota" urlnya="<?php echo site_url('publik/kota'); ?>">
														   <option value="">- Kabupaten/Kota - </option>
														  
													   
													   </select>
												   
													</div>
													
													
													<div class="col-md-3">
													  <input class="form-control" id="keyword"  placeholder="Apa yang Anda cari ?">
													
												   
													</div>
													<div class="col-md-1">
													  <button class="btn btn-sm btn-primary" id="tampilkan" > <i class="fa fa-search"></i> </button> 
												   
													</div>
											   
											</div>
						

										<br>
							<div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
									<tr class="fw-bolder text-muted bg-light">
                                            <th width="2px" rowspan="2">NO</th>
                                            <th rowspan="2">JENJANG </th>
                                            <th rowspan="2">NSM </th>
                                            <th rowspan="2">NPSN </th>
                                            <th rowspan="2">NAMA </th>
                                            <th rowspan="2">PROVINSI </th>
                                            <th rowspan="2">KABKOTA </th>
											<th colspan="3">EMIS </th>
											<th colspan="2">PIP </th>
                                            <th colspan="6">ASPEK HASIL EDM </th>
                                            <th colspan="2">eRKAM </th>
                                            <th colspan="2">SIMSARPRAS </th>
                                            <th colspan="2">INKLUSI </th>
                                            <th rowspan="2">SUDAH MENERIMA BKBA </th>
											
											
                                            
                                        </tr>

										<tr class="fw-bolder text-muted bg-light">
										  <th> Akreditasi </th>
										  <th> Jumlah Siswa </th>
										  <th> Jumlah Guru </th>
										  <th> Siswa Penerima PIP</th>
										  <th> Persen </th>
										  <th> Kedisiplinan</th>
										  <th> Pengembangan Diri</th>
										  <th> Pembelajaran</th>
										  <th> Sarana & Prasarana</th>
										  <th> Pembiayaan</th>
										  <th> Nilai SKPM</th>

										  <th> Rencana Pendapatan </th>											
										  <th> Total Rencana Kegiatan </th>

										  <th> PENERIMA </th>											
										  <th> BANTUAN </th>

										  <th> STATUS </th>											
										  <th> ABK  </th>
										  

										 

										</tr>

                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                         </div>
                
                 
 
 
              
        
<div id="defaultModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body" id="loadform">
        <p>Loading...</p>
      </div>
      
    </div>

  </div>
</div>
	
<script type="text/javascript">

 $(document).off("click",".detail").on("click",".detail",function(){

    var nsm = $(this).attr("nsm");
	  $.post("<?php echo site_url('masteredm/detail'); ?>",{nsm:nsm},function(data){

       $("#loadform").html(data);

	  })

 });


  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"scrollY": 500,
                        "scrollX": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					 buttons: [
							{
								extend: 'excel',
								exportOptions: {
									columns: ':visible'
								}
							},
							'colvis'
						],
                    // buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
				
					
					"ajax":{
						url :"<?php echo site_url("masteredm/grid"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						data.status = $("#status").val();
						data.jenjang = $("#jenjang").val();
						data.provinsi = $("#provinsi").val();
						data.kota = $("#kota").val();
						data.keyword = $("#keyword").val();
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				

				$(document).on("change","#status,#jenjang,#provinsi",function(){


					dataTable.ajax.reload(null,false);	
				});

				$(document).on("input","#keyword",function(){


				   dataTable.ajax.reload(null,false);	
				});
				
				
	


</script>
				