
	<div class="card card-custom">
						<div class="card-header">
												<div class="card-title">
													<span class="card-icon">
														<i class="flaticon2-chat-1 text-primary"></i>
													</span>
													<h3 class="card-label">Data EMIS 
													</h3>
												</div>
												<div class="card-toolbar">
													<a href="#" class="btn btn-sm btn-primary font-weight-bold" data-toggle="modal" data-target="#defaultModal">
													<i class="fa fa-upload"></i>Upload Data </a>
												</div>
											
						                     
						</div>
					  
						 <div class="card-body">
						 <div class="row">
													
													
						                       

													<div class="col-md-2">
													  <select class="form-control" id="jenjang">
														   <option value="">- Jenjang - </option>
														   <?php 
															 $jenjang = array("mi","mts","ma");
															   foreach($jenjang as $r){
																   ?><option value="<?php echo $r; ?>"  > <?php echo strtoupper($r); ?> </option><?php 
																   
															   }
															  ?>
														  
													   
													   </select>
												   
													</div>
													
													
													
													<div class="col-md-4">
													  <input class="form-control" id="keyword"  placeholder="Apa yang Anda cari ?">
													
												   
													</div>
													<div class="col-md-1">
													  <button class="btn btn-sm btn-primary" id="tampilkan" > <i class="fa fa-search"></i> </button> 
												   
													</div>
											   
											</div>
										<br>
						


							<div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
									<tr class="fw-bolder text-muted bg-light">
                                            <th width="2px">NO</th>
                                            <th>JENJANG </th>
                                            <th>NSM </th>                                           
                                            <th>NAMA </th>
                                            <th>AKREDITASI </th>
                                            <th>JUMLAH GURU </th>
                                            
                                            <th>GURU LAKI </th>
                                            <th>GURU PEREMPUAN </th>
                                            
                                            <th>RUANG BELAJAR</th>
                                            
                                            <th>TOTAL TOILET GURU</th>
                                            <th>TOILET GURU (L)</th>
                                            <th>TOILET GURU (P)</th>
                                            <th>TOILET GURU UMUM</th>
                                            <th>TOILET SISWA</th>
											<th>TOILET GURU (L)</th>
                                            <th>TOILET GURU (P)</th>
                                            <th>TOILET GURU UMUM</th>
                                            
                                            <th>JUMLAH ROMBEL</th>
                                            <th>JUMLAH SISWA</th>
                                            <th>SISWA LAKI</th>
                                            <th>SISWA PEREMPUAN</th>
                                            <th>SISWA BERKEBUTUHAN KHUSUS</th>
                                            <th>SISWA PUTUS SEKOLAH</th>
                                           
                                            <th>TAHUN</th>
										 </tr>

									

                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                         </div>
                     
                 
 
 
              
        
<div id="defaultModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body" id="loadform">
	 
						
						 <form action="<?php echo site_url("masteredm/import_emis"); ?>" method="post" enctype="multipart/form-data">
						
                                
							
								

								<div class="row clearfix">
                                 <div class="col-sm-12">
								 
								
								<div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                           <input type="text" name="tahun" class="form-control">
											
                                        </div>
                                    </div>
                                    
                                 </div>
                                </div>
								 
                                    <div class="form-group">
									    <input id="file-upload" type="file" required name="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" >

										
                                    </div>
                                    
                                 </div>
                                </div>
								
								 <div class=" " id="aksimodal">
						       <center>
								   <button type="submit" class="btn btn-success btn-sm ">
										<i class="fa fa-save"></i>
										<span>Import Excel </span>
                                    </button>
								    
									<button type="button" class="btn btn-success btn-sm " data-dismiss="modal">
										
										<span>Close   </span>
                                    </button>
									
								   </center>
								
								 </div>
								 
								  <div class="row clearfix" id="loadingmodal" style="display:none">
								  <center><div class="preloader pl-size-md"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div> <br> Data sedang disimpan, Mohon Tunggu ...</center>
								 
								 </div>
							  
					</form>	    
      </div>
      
    </div>

  </div>
</div>
	
<script type="text/javascript">

 

  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"scrollY": 500,
                        "scrollX": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					 buttons: [
							{
								extend: 'excel',
								exportOptions: {
									columns: ':visible'
								}
							},
							'colvis'
						],
                    // buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
				
					
					"ajax":{
						url :"<?php echo site_url("masteredm/grid_emis"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						data.status = $("#status").val();
						data.jenjang = $("#jenjang").val();
						data.provinsi = $("#provinsi").val();
						data.kota = $("#kota").val();
						data.keyword = $("#keyword").val();
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				

				$(document).on("change","#status,#jenjang,#provinsi,#kota",function(){


					dataTable.ajax.reload(null,false);	
				});

				$(document).on("input","#keyword",function(){


				   dataTable.ajax.reload(null,false);	
				});
				
				
	


</script>
				