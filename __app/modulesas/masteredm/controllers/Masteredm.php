<?php
ini_set('memory_limit', '-1');

defined('BASEPATH') OR exit('No direct script access allowed');

class Masteredm extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 if(!$this->session->userdata("admin_id")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_guru','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('pusat/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Data Master EDM eRKAM ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			    $siswa_pip ='<a href="javascript:void(0)" nsm="'.$val['nsm'].'" class="detail" data-toggle="modal" data-target="#defaultModal">'.$val['siswa_pip'].'</a>';
				$no = $i++;
				$records["data"][] = array(
					$no,
					strtoupper($val['jenjang']),
					$val['nsm'],
					$val['npsn'],
					
					$val['nama'],
					$val['provinsi'],
					$val['kota'],
					$val['akreditasi'],
					$val['jml_siswa'],
					$val['jml_guru'],
				
					$siswa_pip,
					number_format($val['persen_pip'],1)." %",
					sprintf('%0.2f',$val['nilai_kedisiplinan']),
					sprintf('%0.2f',$val['nilai_pengembangan_diri']),
					sprintf('%0.2f',$val['nilai_proses_pembelajaran']),
					sprintf('%0.2f',$val['nilai_sarana_prasarana']),
					sprintf('%0.2f',$val['nilai_pembiayaan']),
					number_format($val['nilai_skpm'],2),
					$this->Reff->formatuang2($val['rencana_pendapatan']),
					$this->Reff->formatuang2($val['total_rencana_kegiatan']),
					($val['simsarpras']==1) ? "Ya" :"Tidak",
					$val['simsarpras_bantuan'],
					($val['inklusi'] !=0) ? "Ya" :"Tidak",
					$val['abk'],
					($val['sudah_menerima_bkba']==1) ? "Ya" :"Tidak",
					
					
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}



	
	public function detail(){
		
		$nsm = $this->input->get_post("nsm");
		$data = array();
		$data['nsm'] = $nsm;
		  
		$this->load->view("detail",$data);
		
	}

	// Edm ERKAM 

	public function erkam()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Data Master EDM eRKAM ";
	     if(!empty($ajax)){
					    
			 $this->load->view('erkam',$data);
		
		 }else{
			 
		     $data['konten'] = "erkam";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid_erkam(){
		
		  $iTotalRecords = $this->m->grid_erkam(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid_erkam(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			  	$no = $i++;
				$records["data"][] = array(
					$no,
					strtoupper($val['jenjang']),
					$val['nsm'],
					$val['npsn'],
					
					$val['nama'],
					$val['provinsi'],
					$val['kota'],					
					sprintf('%0.2f',$val['nilai_kedisiplinan']),
					sprintf('%0.2f',$val['nilai_pengembangan_diri']),
					sprintf('%0.2f',$val['nilai_proses_pembelajaran']),
					sprintf('%0.2f',$val['nilai_sarana_prasarana']),
					sprintf('%0.2f',$val['nilai_pembiayaan']),
					number_format($val['nilai_skpm'],2),
					$this->Reff->formatuang2($val['rencana_pendapatan']),
					$this->Reff->formatuang2($val['total_rencana_kegiatan'])
					
					
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}


	// Emis 

	public function emis()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Data Master EMIS  ";
	     if(!empty($ajax)){
					    
			 $this->load->view('emis',$data);
		
		 }else{
			 
		     $data['konten'] = "emis";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid_emis(){
		
		  $iTotalRecords = $this->m->grid_emis(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid_emis(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			  	$no = $i++;
				$records["data"][] = array(
					$no,
					strtoupper($val['jenjang']),
					$val['nsm'],
					$val['nama_lembaga'],
					$val['akreditasi_huruf'],
					$val['jumlahguru'],
				
					$val['jumlah_guru_laki'],
					$val['jumlah_guru_perempuan'],
					
					$val['ruang_belajar'],
					
					$val['toilet_guru'],
					$val['toilet_guru_laki'],
					$val['toilet_guru_perempuan'],
					$val['toilet_guru_umum'],
					$val['toilet_siswa'],
					$val['toilet_siswa_laki'],
					$val['toilet_siswa_perempuan'],
					$val['toilet_siswa_umum'],
					
					$val['jumlah_rombel'],
					$val['jumlah_siswa'],
					$val['siswa_laki'],
					$val['siswa_perempuan'],
					$val['siswa_berkebutuhan_khusus'],
					$val['siswa_putus_sekolah'],
					$val['tahun']
					
					
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	

	public function import_emis()
		{
				$this->load->library('PHPExcel/IOFactory');
     
	    $sheetup   = 0;
	    $rowup     = 2;
		
		$tahun = $this->input->get_post("tahun");
	
		$fileName = time().".xlsx";
         
        $config['upload_path'] = './__statics/upload/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size'] = 100000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') ){
			$jupload=0;
              echo $this->upload->display_errors();

			  
		}else{
             
        $media = $this->upload->data('file');
		
        $inputFileName = './__statics/upload/'.$fileName;
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet($sheetup);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
        
             
			 $jupload=0;
            for ($row = $rowup; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
				
       
			 	
			
				$error  ="";
			
					$jupload++;
                

					$this->db->set("tahun","2021");
					$this->db->set("nsm",(trim($rowData[0][1])));
					$this->db->set("npsn",(trim($rowData[0][2])));
					$this->db->set("nama_lembaga",(trim($rowData[0][3])));
					$this->db->set("jenjang",(trim($rowData[0][4])));
					$this->db->set("status",(trim($rowData[0][5])));
					$this->db->set("akreditasi_huruf",(trim($rowData[0][6])));
					$this->db->set("jumlah_rombel",(trim($rowData[0][7])));
					$this->db->set("jumlahguru",(trim($rowData[0][8])));
					$this->db->set("gurus1",(trim($rowData[0][9])));
					$this->db->set("jumlah_guru_laki",(trim($rowData[0][10])));
					$this->db->set("jumlah_guru_perempuan",(trim($rowData[0][11])));
					$this->db->set("pns",(trim($rowData[0][12])));
					$this->db->set("nonpns",(trim($rowData[0][13])));
					$this->db->set("guru_tetap",(trim($rowData[0][14])));
					$this->db->set("guru_tidak_tetap",(trim($rowData[0][15])));
					$this->db->set("tenaga_kependidikan",(trim($rowData[0][16])));
					$this->db->set("ruang_belajar",(trim($rowData[0][17])));
					$this->db->set("kantor_kamad",(trim($rowData[0][18])));
					$this->db->set("kantor_guru",(trim($rowData[0][19])));
					$this->db->set("kantor_administrasi",(trim($rowData[0][20])));
					$this->db->set("perpustakaan",(trim($rowData[0][21])));
					$this->db->set("lab_ipa",(trim($rowData[0][22])));
					$this->db->set("lab_bahasa",(trim($rowData[0][23])));
					$this->db->set("lab_komp",(trim($rowData[0][24])));
					$this->db->set("toilet_guru",(trim($rowData[0][25])));
					$this->db->set("toilet_siswa",(trim($rowData[0][26])));
					$this->db->set("olahraga",(trim($rowData[0][27])));
					$this->db->set("luas_lahan",(trim($rowData[0][28])));
					$this->db->set("luas_bangunan",(trim($rowData[0][29])));
					$this->db->set("luas_lahan_terbuka",(trim($rowData[0][30])));
					$this->db->set("jumlah_siswa",(trim($rowData[0][31])));
					$this->db->set("siswa_laki",(trim($rowData[0][32])));
					$this->db->set("siswa_perempuan",(trim($rowData[0][33])));
					$this->db->set("siswa_berkebutuhan_khusus",(trim($rowData[0][34])));
					$this->db->set("siswa_putus_sekolah",(trim($rowData[0][35])));
					$this->db->set("ipa",(trim($rowData[0][36])));
					$this->db->set("ips",(trim($rowData[0][37])));
					$this->db->set("bahasa",(trim($rowData[0][38])));
					$this->db->set("agama",(trim($rowData[0][39])));
					$this->db->set("kepemilikan_internet",(trim($rowData[0][40])));
					//$this->db->set("kualitas_internet",(trim($rowData[0][41])));

					$this->db->insert("datamadrasahemis_csv");
				
					
				
				  
				
				   
				   
			
					
            
		   
			}
			
			
		
			echo "<br><br><center>Data yg di upload : ". $jupload ." Data <br> <a href='".site_url("schoolsiswa")."' > Kembali ke Aplikasi </a>";
        
        }
   }
	

	public function aspirasi()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Aspirasi Madrasah ";
	     if(!empty($ajax)){
					    
			 $this->load->view('aspirasi',$data);
		
		 }else{
			 
		     $data['konten'] = "aspirasi";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function gridAspirasi(){
		error_reporting(0);
		  $iTotalRecords = $this->m->gridAspirasi(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->gridAspirasi(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {

			$calonPenerima = $this->db->query("select count(id) as jml from madrasahedm where tahun='2023' and  nsm='".str_replace("'","",$val['nsm'])."'")->row();
			$longlist      = $this->db->query("select count(id) as jml from madrasahedm where tahun='2023' and  nsm='".str_replace("'","",$val['nsm'])."' and longlist=1")->row();
			$shortlist      = $this->db->query("select count(id) as jml from madrasahedm where tahun='2023' and  nsm='".str_replace("'","",$val['nsm'])."' and shortlist=1")->row();
			$simsarpras      = $this->db->query("select * from madrasah_penerima where anggaran='2023' and nsm='".str_replace("'","",$val['nsm'])."'")->row();

			 $cp ="Tidak";
			if(!is_null($calonPenerima)){
			   if($calonPenerima->jml >0){
				// $this->db->set("edm",1);
				// $this->db->where("nsm",$val['nsm']);
				// $this->db->update("aspirasi");

				$cp ="Ya";
			   }else{

				$cp ="Tidak";
			   }
			}


			   $lp ="Tidak";
			   if(!is_null($longlist)){
			   if($longlist->jml >0){
				$lp ="Ya";

				// $this->db->set("longlist",1);
				// $this->db->where("nsm",$val['nsm']);
				// $this->db->update("aspirasi");


			   }else{

				$lp ="Tidak";
			   }
			}

              $arbantuan = array("1"=>"Masuk Bantuan Kinerja","2"=>"Masuk Bantuan Afirmasi");
			   $sp ="Tidak";
			   if(!is_null($shortlist)){
			   if(count($shortlist) > 0){
			
				// $sp = $arbantuan[$shortlist->bantuan];

				// $this->db->set("shortlist",1);
				// $this->db->where("nsm",$val['nsm']);
				// $this->db->update("aspirasi");


			   }else{

				$sp ="Tidak";
			   }
			}

		
			$sarpras ="Tidak";
			if(!is_null($simsarpras)){
			if(count($simsarpras) > 0){
		 
			 $sarpras = $simsarpras->bantuan;

			//  $this->db->set("simsarpras",1);
			//  $this->db->set("simsarpras_bantuan",$sarpras);
			//  $this->db->where("nsm",$val['nsm']);
			//  $this->db->update("aspirasi");


			}else{

			 $sarpras ="Tidak";
			}
		 }

			   	$no = $i++;
				$records["data"][] = array(
					$no,
				
					$val['nsm'],
				
					$val['nama'],
					$cp,
					$lp,
					$sp,
					
					$val['titipan']
					
					
					
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	 

	public function import_aspirasi()
		{
				$this->load->library('PHPExcel/IOFactory');
     
	    $sheetup   = 0;
	    $rowup     = 1;
		
		$kategori = $this->input->get_post("kategori");
		//echo $file_ext = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION); exit();
		$fileName = time().".xlsx";
         
        $config['upload_path'] = './__statics/upload/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = '*';
        $config['max_size'] = 100000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') ){
			$jupload=0;
              echo $this->upload->display_errors();

			  
		}else{
             
        $media = $this->upload->data('file');
		
        $inputFileName = './__statics/upload/'.$fileName;
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet($sheetup);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
        
             
			 $jupload=0;
            for ($row = $rowup; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
				
       
			 	
			
				$error  ="";
			
					$jupload++;
                

					
					$this->db->set("nsm",(trim($rowData[0][0])));
					
					$this->db->set("nama",(trim($rowData[0][1])));
					// $this->db->set("provinsi",(trim($rowData[0][2])));		
					// $this->db->set("kabko",(trim($rowData[0][3])));
					
					$this->db->set("titipan",$kategori);
					//$this->db->set("titipan",$kategori);
					$this->db->set("kategori",$kategori);
					$this->db->insert("aspirasi");
						
				
				  
				
				   
				   
			
					
            
		   
			}
			
			
		
			echo "<br><br><center>Data yg di upload : ". $jupload ." Data <br> <a href='".site_url("schoolsiswa")."' > Kembali ke Aplikasi </a>";
        
        }
   }

   public function generateNPSNPBNU(){

	$m =  $this->db->query("SELECT * from madrasah_pbnu where nsm=''")->result();

	   foreach($m as $rm){
		  
		 

		 $nsm             = $this->Reff->get_kondisi(array("npsn"=>$rm->npsn),"tm_madrasah_master","nsm");
		 $this->db->set("nsm",$nsm);
		 $this->db->where("npsn",$rm->npsn);
		 $this->db->update("madrasah_pbnu");
	   }
 }

   public function pindahAspirasi(){

	        $bhpnu = $this->db->get("madrasah_pbnu")->result();
			  foreach($bhpnu as $row){

			  
					$this->db->set("nsm",$row->nsm);
					$this->db->set("npsn",$row->npsn);
					$this->db->set("nama",$row->nama);
					
					$this->db->set("titipan","BHPNU");
					$this->db->set("kategori","BHPNU");
					$this->db->insert("aspirasi");

			  }


   }

   public function generateAspirasi(){
	$aspirasi = $this->db->get("aspirasi")->result();
	foreach($aspirasi as $row){

		  $madrasah = $this->db->get_where("tm_madrasah_master",array("nsm"=>$row->nsm))->row();
		  $provinsi = $this->Reff->get_kondisi(array("id"=>$madrasah->provinsi_id),"provinsi","nama");
		  $kota     = $this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama");
	    
			if($row->npsn==""){
				$this->db->set("npsn",$madrasah->npsn);
			}
		  $this->db->set("jenjang",$madrasah->jenjang);
		  $this->db->set("alamat",$madrasah->alamat);
		  $this->db->set("provinsi_id",$madrasah->provinsi_id);
		  $this->db->set("provinsi",$provinsi);
		  $this->db->set("kabko",$kota);
		  $this->db->where("id",$row->id);
		  
		  $this->db->update("aspirasi");

	}

   }
	
}
