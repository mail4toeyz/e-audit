<?php

class M_guru extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
    }

    
	public function grid($paging){
		$tahun   = $this->Reff->tahun();
	    $status = $this->input->get_post("status");
	    $jenjang = $this->input->get_post("jenjang");
	    $provinsi = $this->input->get_post("provinsi");
	    $kota     = $this->input->get_post("kota");
	    $keyword  = $this->input->get_post("keyword");
		$this->db->select("*");
        $this->db->from('madrasahedm');
    	$this->db->where("tahun",$tahun);

    	//$this->db->where("blacklist",0);
	   
	    if(!empty($jenjang)){  $this->db->where("jenjang",$jenjang);    }
	    if(!empty($provinsi)){  $this->db->where("provinsi_id",$provinsi);    }
	    if(!empty($kota)){  $this->db->where("kota_id",$kota);    }
	    if(!empty($keyword)){  $this->db->where("(UPPER(nama) LIKE '%".strtoupper($keyword)."%' OR UPPER(nsm) LIKE '%".strtoupper($keyword)."%')");    }
		

		if($status==1){
			$this->db->where("blacklist",0);
			$this->db->where("longlist",1);
			$this->db->where("provinsi_id  IN(select id from provinsi where kuota_kinerja+kuota_afirmasi !=0)");
		}else if($status==2){
			$this->db->where("blacklist",0);
			$this->db->where("longlist !=",1);
			$this->db->where("provinsi_id  IN(select id from provinsi where kuota_kinerja+kuota_afirmasi !=0)");
		}else if($status==3){
			$this->db->where("blacklist",0);
			$this->db->where("shortlist",1);
			$this->db->where("provinsi_id  IN(select id from provinsi where kuota_kinerja+kuota_afirmasi !=0)");
		}else if($status==4){
			$this->db->where("blacklist",0);
			$this->db->where("shortlist !=",1);
			$this->db->where("provinsi_id  IN(select id from provinsi where kuota_kinerja+kuota_afirmasi !=0)");
		}else if($status==5){
			
			$this->db->where("blacklist",0);
			$this->db->where("provinsi_id  IN(select id from provinsi where kuota_kinerja+kuota_afirmasi !=0)");

		}else if($status==6){
		
			$this->db->where("sudah_menerima_bkba !=",0);

		}else if($status==7){
		
			$this->db->where("simsarpras !=",0);

		}else if($status==8){
		
			$this->db->where("inklusi !=",0);

		}else if($status==9){
		
			$this->db->where("pupr !=",0);

		}
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					// $this->db->order_by("jenjang","ASC");
					 $this->db->order_by("provinsi_id","ASC");
					 $this->db->order_by("kota_id","ASC");
					 $this->db->order_by("nama","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
	
	public function grid_erkam($paging){
        
		$tahun   = $this->Reff->tahun();
	    $status  = $this->input->get_post("status");
	    $jenjang = $this->input->get_post("jenjang");
	    $provinsi = $this->input->get_post("provinsi");
	    $kota     = $this->input->get_post("kota");
	    $keyword  = $this->input->get_post("keyword");
		$this->db->select("*");
        $this->db->from('madrasahedm');
    	$this->db->where("tahun",$tahun);
		//$this->db->where("provinsi_id IN(73,33,11)");
	    if(!empty($jenjang)){  $this->db->where("jenjang",$jenjang);    }
	    if(!empty($provinsi)){  $this->db->where("provinsi_id",$provinsi);    }
	    if(!empty($kota)){  $this->db->where("kota_id",$kota);    }
	    if(!empty($keyword)){  $this->db->where("(UPPER(nama) LIKE '%".strtoupper($keyword)."%' OR UPPER(nsm) LIKE '%".strtoupper($keyword)."%')");    }
		

		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					// $this->db->order_by("jenjang","ASC");
					 $this->db->order_by("provinsi_id","ASC");
					 $this->db->order_by("kota_id","ASC");
					 $this->db->order_by("nama","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}

	public function grid_emis($paging){
        
		$tahun   = $this->Reff->tahun();
	  
	    $jenjang = $this->input->get_post("jenjang");
	    $keyword  = $this->input->get_post("keyword");

		$this->db->select("*");
        $this->db->from('datamadrasahemis_csv');
    	$this->db->where("tahun",$tahun);
		//$this->db->where("provinsi_id IN(73,33,11)");
	    if(!empty($jenjang)){  $this->db->where("jenjang",$jenjang);    }
	    
	    if(!empty($keyword)){  $this->db->where("(UPPER(nama_lembaga) LIKE '%".strtoupper($keyword)."%' OR UPPER(nsm) LIKE '%".strtoupper($keyword)."%')");    }
		

		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					
					 $this->db->order_by("nama_lembaga","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}

	public function gridAspirasi($paging){
        
		$tahun   = $this->Reff->tahun();
	  
	    $status = $this->input->get_post("status");
	    $kategori = $this->input->get_post("kategori");
	    $keyword  = $this->input->get_post("keyword");

		$this->db->select("*");
        $this->db->from('aspirasi');
    
	    if(!empty($kategori)){  $this->db->where("kategori",$kategori);    }
	    
	    if(!empty($keyword)){  $this->db->where("(UPPER(`nama`) LIKE '%".strtoupper($keyword)."%' OR UPPER(nsm) LIKE '%".strtoupper($keyword)."%')");    }
		

		 if($status==1){
			 $this->db->where("edm",1);
		 }else if($status==2){
			$this->db->where("longlist",1);
		}else if($status==3){
			$this->db->where("shortlist",1);
		 }
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("shortlist","DESC");
					 $this->db->order_by("nsm","ASC");
					 $this->db->order_by("nama","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
}
