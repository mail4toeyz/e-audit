
<?php 
  $cekhasil = $this->db->query("select * from h_ujian where tmujian_id='".$ujian->id."' and tmsiswa_id='".$data->id."' and status='N'")->row();
  $kelas    = $this->db->get_where("tr_kelas",array("id"=>$ujian->trkelas_id))->row();
?>
<div class="callout callout-danger">
    <h4>Petunjuk !</h4>
	<p>
	 <ol start="1">
	   <li> Untuk memulai ujian, silahkan klik tombol "Mulai Ujian", setelah Anda memasukkan token ujian  </li>
	   <li> Waktu akan dihitung ketika Anda memulai ujian </li>
	 </ol>
	
	</p>
   </div>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">DETAIL UJIAN </h3>
    </div>
    <div class="box-body">
        <span id="id_ujian" data-key="<?php echo base64_encode($ujian->id); ?>" data-hash="<?php echo base64_encode($ujian->nama); ?>"></span>
        <div class="row">
            <div class="col-sm-6">
                <table class="table table-bordered">
                    <tr>
                        <th>Nama Siswa </th>
                        <td><?php echo $data->nama; ?></td>
                    </tr>
                    <tr>
                        <th>Nama Guru </th>
                        <td><?php echo $this->Reff->get_kondisi(array("id"=>$ujian->tmguru_id),"tm_guru","nama"); ?></td>
                    </tr>
                    <tr>
                        <th>Class Room</th>
                       
						<td><?php echo $this->Reff->get_kondisi(array("id"=>$ujian->trkelas_id),"tr_kelas","nama"); ?></td>
                    </tr>
                    <tr>
                        <th>Nama Ujian</th>
                        <td><?php echo $ujian->nama; ?></td>
                    </tr>
                    <tr>
                        <th>Jumlah Soal</th>
						<?php 
						  $jumlahsoal = $this->db->query("select count(id) as jml from tr_soal where tmujian_id='".$ujian->id."'")->row();
						  ?>
                        <td><?php echo $jumlahsoal->jml; ?> Soal</td>
                    </tr>
                    <tr>
                        <th>Waktu</th>
                        <td><?php echo $ujian->waktu; ?> Menit</td>
                    </tr>
                    <tr>
                        <th>Terlambat</th>
                        <td>
                            <?php echo strftime('%d %B %Y', strtotime($ujian->terlambat)); ?> 
                            <?php echo date('H:i:s', strtotime($ujian->terlambat)); ?>
                        </td>
                    </tr>
					
					<?php 
					  if(count($cekhasil) ==0){
						 ?>
					
							<tr>
								<th style="vertical-align:middle">Token</th>
								<td>
									<?php 
									  $token ="";
										if($ujian->publish==1){
											$token = $ujian->token;
										}
									?>
									<input autocomplete="off" id="token" placeholder="Token" value="<?php echo $token; ?>" type="text" class="input-sm form-control">
								</td>
							</tr>
					  <?php } ?>
                </table>
            </div>
            <div class="col-sm-6">
                <div class="box box-solid">
                    <div class="box-body pb-0">
					
									  <?php 
									  if(count($cekhasil) ==0){
										 ?>
										<div class="callout callout-success">
											<p>
												Waktu boleh mengerjakan ujian adalah saat tombol "MULAI UJIAN" sudah tersedia.
											</p>
										</div>
										<?php
										$mulai = strtotime($ujian->tgl_mulai);
										$terlambat = strtotime($ujian->terlambat);
										$now = time();
										if($mulai > $now) {
										?>
										<div class="callout callout-success">
											<strong><i class="fa fa-clock-o"></i> Ujian akan dimulai pada</strong>
											<br>
											<span class="countdown" data-time="<?=date('Y-m-d H:i:s', strtotime($ujian->tgl_mulai))?>">00 Hari, 00 Jam, 00 Menit, 00 Detik</strong><br/>
										</div>
										<?php } else if( $terlambat > $now ) { ?>
										<button id="btncek" data-id="<?php echo $ujian->id; ?>" class="btn btn-success btn-lg mb-4">
											<i class="fa fa-pencil-square"></i> MULAI UJIAN 
										</button>
										<div class="callout callout-danger">
											<i class="fa fa-clock-o"></i> <strong class="countdown" data-time="<?=date('Y-m-d H:i:s', strtotime($ujian->terlambat))?>">00 Hari, 00 Jam, 00 Menit, 00 Detik</strong><br/>
											Batas waktu menekan tombol mulai.
										</div>
										<?php  } else { ?>
										<div class="callout callout-danger">
											Waktu untuk menekan tombol <strong>"MULAI"</strong> sudah habis.<br/>
											Silahkan hubungi dosen anda untuk bisa mengikuti ujian pengganti.
										</div>
										<?php } ?>
										
										 <a href="<?php echo site_url("studentkelas/ujian/".base64_encode($kelas->kode)."/".base64_encode($kelas->nama)."/".base64_encode($kelas->id).""); ?>" class="btn btn-success btn-sm mb-4">
												<i class="fa fa-history"></i> Kembali Ke E-Learning
											</a> 
										
									<?php 
									  }else{
									   ?>
										  <div class="callout callout-success" style="font-weight:bold">
											<p>
											  KETERANGAN ! <br>
												Anda sudah menyelesaikan Ujian ini pada <br> <u><i> <?php echo $this->Reff->formattimestamp($cekhasil->tgl_selesai); ?> </i></u>
											</p>
										</div>
										
										<div class="table-responsive table-hover">
										  <table class="table table-hover">
										    <thead>
											  <tr>
											     <td align="center"> JUMLAH BENAR </td>
											     <td align="center"> JUMLAH SALAH </td>
											     <td align="center"> NILAI UJIAN </td>
										     </tr>
											 </thead>
											 
											 <tbody>
												 <tr>
													 <td align="center"> <?php echo $cekhasil->jml_benar; ?> </td>
													 <td align="center"> <?php echo ($jumlahsoal->jml-$cekhasil->jml_benar); ?> </td>
													 <td align="center"> <?php echo $cekhasil->nilai; ?> </td>
												 </tr>
											 </tbody>
											 
										  </table>
										</div>
										<center> 
											 <a href="<?php echo site_url("studentkelas/ujian/".base64_encode($kelas->kode)."/".base64_encode($kelas->nama)."/".base64_encode($kelas->id).""); ?>" class="btn btn-success btn-sm mb-4">
												<i class="fa fa-history"></i> Kembali Ke E-Learning
											</a> 
										</center>
										  
									  <?php   
									  }
									  
									  
									  ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?=base_url()?>__statics/cbt/dist/js/app/ujian/token.js"></script>