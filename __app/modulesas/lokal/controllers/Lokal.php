<?php
use setasign\Fpdi\Fpdi;

require_once(APPPATH.'libraries/fpdf/fpdf.php');
require_once(APPPATH.'libraries/fpdi/src/autoload.php');
defined('BASEPATH') OR exit('No direct script access allowed');

class Lokal extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		$this->load->model('M_lokal');
         $this->load->helper('exportpdf_helper');  
		 if(!$this->session->userdata("tmmadrasah_id")){						
			$ajax = $this->input->get_post("ajax",true);		 
				if(!empty($ajax)){
			   
				   echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
					  exit();
				 }else{
					 redirect(site_url()."ksm/login");
				 }
			
		   }
		
	  }

	
	public function data(){
		
		
		
		 	
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Data Madrasah/Sekolah";
		  $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('page_data',$data);
		 }else{
			 
			
		     $data['konten'] = "page_data";
			 
			 $this->load->view('dashboard/page_header',$data);
		 }
	
		
	}
	
	public function perbaharui_p(){
		
		
		
		 if(!$this->session->userdata("tmmadrasah_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
               exit();
		  }
	   
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Perbaharui Profil Anda";
		  $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('page_profile',$data);
		 }else{
			 
			
		     $data['konten'] = "page_profile";
			 
			 $this->load->view('dashboard/page_header',$data);
		 }
	
		
	}
	
	public function perbaharui_data(){
     
        $this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
        
        $this->form_validation->set_message('min_length', '{field} Minimal 6 Karakter .');
        $this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
        $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , mungkin anda sudah pernah melakukan pendaftaran, silahkan klik menu login .');
				 
		 
			
				$config = array(
				  
				    array('field' => 'f[jenjang]', 'label' => 'Jenjang Pendidikan ', 'rules' => 'trim|required'),
				    array('field' => 'f[telepon]', 'label' => 'Telepon  ', 'rules' => 'trim|required'),
				    array('field' => 'f[email]', 'label' => 'Email  ', 'rules' => 'trim|required'),
				 	array('field' => 'f[alamat]', 'label' => 'Alamat ', 'rules' => 'trim|required'),
				 
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			  
			  
			   $this->db->update("tm_madrasah",$_POST['f'],array("id"=>$_SESSION['tmmadrasah_id']));
			   $this->Di->log($_SESSION['nama']. "Merubah Data Madrasah","m");
			   echo "sukses";
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }	
	
	
	}
	
	
	public function delegasi(){
		
		
		
		 if(!$this->session->userdata("tmmadrasah_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
               exit();
		  }
	   
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Ketua Delegasi";
		  $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('page_delegasi',$data);
		 }else{
			 
			
		     $data['konten'] = "page_delegasi";
			 
			 $this->load->view('dashboard/page_header',$data);
		 }
	
		
	}
	
	public function delegasi_simpan(){
     
        $this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
        
        $this->form_validation->set_message('min_length', '{field} Minimal 6 Karakter .');
        $this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
        $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , mungkin anda sudah pernah melakukan pendaftaran, silahkan klik menu login .');
				 
		 
			
				$config = array(
				    array('field' => 'f[delegasi_nik]', 'label' => 'NIK Ketua Delegasi ', 'rules' => 'trim|required'),
				    array('field' => 'f[delegasi_nama]', 'label' => 'Nama Ketua Delegasi  ', 'rules' => 'trim|required'),
				    array('field' => 'f[delegasi_hp]', 'label' => 'Handphone Ketua Delegasi   ', 'rules' => 'trim|required'),
				  
				 
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			  
			  
			   $this->db->update("tm_madrasah",$_POST['f'],array("id"=>$_SESSION['tmmadrasah_id']));
			   $this->Di->log($_SESSION['nama']. "Merubah Data Madrasah","m");
			   echo "sukses";
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }	
	
	
	}
	
	
	
	public function peserta($status){
		
		
		
		 if(!$this->session->userdata("tmmadrasah_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
               exit();
		  }
	   
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']   = "Pendaftaran Peserta KSM ";
		  $data['status']  = $status; 
		  $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('page_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "page_peserta";
			 
			 $this->load->view('dashboard/page_header',$data);
		 }
	
		
	}
	
	
	public function g_peserta(){
		  error_reporting(0);
		  $iTotalRecords = $this->M_lokal->m_peserta(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->M_lokal->m_peserta(true)->result_array();
		  $ketkabko = array("1"=>"Juara I","2"=>"Juara II","3"=>"Juara III","4"=>"Juara Harapan I","5"=>"Juara Harapan II","6"=>"Juara Harapan III");
		


		   $i= ($iDisplayStart +1);
		     $status       = array("0"=>"Draft (Belum terkirim)","1"=>"Belum diverifikasi","2"=>"Lulus Verifikasi","3"=>"Tidak Lulus Verifikasi");
		     $statusbadge  = array("0"=>"danger","1"=>"primary","2"=>"success","3"=>"warning");

			
		   foreach($datagrid as $val) {
			     $foto =base_url()."__statics/upload/".$val['foto'];
				 if($val['foto']==""){
					$foto =  base_url()."__statics/img/not.png";
				  }
				 $img ='<img src="'.$foto.'" alt="" class="img-responsive" width="100px">';  
				
				$no = $i++;
				  $btn="";
				if($val['status'] !=0){
					$btn ='<a class="btn btn-success  btn-sm" href = "'.site_url("lokal/cetak?nik=".base64_encode($val['nik_siswa'])."").'" title="Detail Data"><i class=" fa fa-file"></i> Bukti Pendaftaran </a>';
				}

				$keterangan ="";
					if($val['status']==3){
						$keterangan = "Alasan Tidak Lulus :<br>". $val['keterangan'];

					}

			    $hasilkabko = $this->db->get_where("v_siswa_kabkota",array("id"=>$val['id']))->row();


				$records["data"][] = array(
				    $no,
					'
					<div class="btn-group" role="group" aria-label="Basic example">
						<button type="button" class="btn btn-primary btn-sm ubah" urlnya = "'.site_url("lokal/f_peserta").'"  disabled datanya="'.$val['id'].'" title="Ubah Data" ><i class=" fa fa-pencil-square"></i> Perbaiki  </button>
						<button type="button" class="btn btn-danger hapus btn-sm" urlnya = "'.site_url("lokal/h_peserta").'" disabled datanya="'.$val['id'].'" title="Hapus Data" ><i class=" fa fa-trash"></i>  Hapus </button>
						<button type="button" class="btn btn-info ubah btn-sm" urlnya = "'.site_url("lokal/detail").'"  datanya="'.$val['id'].'" title="Detail Data"><i class=" fa fa-list"></i>  Detail </button>
						'.$btn.'
					</div>
										
					 ',
				    '<span class="badge badge-'.$statusbadge[$val['status']].'">'.$status[$val['status']].'</span><br>'.$keterangan,
					$img,
				  	$val['nik_siswa'],
				  	$val['nisn'],
				  	$val['nama'],
				
				//	'<a href="#" urlnya = "'.site_url("lokal/f_peserta_u").'" class="ubah" datanya="'.$val['id'].'">'.$val['nama'].'</a>',	
					
					
					$val['gender'],
				//	$val['tempat'].",".$this->Di->formattanggalstring($val['tgl_lahir']),
					
					$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
					$hasilkabko->bnr_pg,
					$hasilkabko->bnr_isi,
					$hasilkabko->jml_benar,
					$hasilkabko->slh,
					$hasilkabko->ksg,
					$hasilkabko->nla
					
					
					
					 
					
					
                			
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}

	public function detail(){
		
		$id = $this->input->get_post("id",TRUE);
		
		$data = array();
	   
			   
		$data['dataform'] = $this->db->query("select * from v_siswa where id='".$id."'")->row();		
		$data['madrasah']	     = $this->Di->get_where("tm_madrasah",array("id"=>$data['dataform']->tmmadrasah_id));
		$data['title']  = "Detail Peserta ".$data['dataform']->nama;
		$this->load->view('detail',$data);
   }
   
	public function pendaftaran(){
		if(!$this->session->userdata("tmmadrasah_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
               exit();
		  }

		 $ajax = $this->input->get_post("ajax",TRUE);
		 $id = $this->input->get_post("id",TRUE);
		 
		 
	     $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
		 $data['title']  = "Pendaftaran Peserta KSM";
		 
		 if(!empty($ajax)){
					    
			 $this->load->view('form_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "form_peserta";
			 
			 $this->load->view('dashboard/page_header',$data);
		 }
	}
	
	public function pendaftaran_tambahan(){
		if(!$this->session->userdata("tmmadrasah_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
               exit();
		  }

		 $ajax = $this->input->get_post("ajax",TRUE);
		 $id = $this->input->get_post("id",TRUE);
		 
		 
	     $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
		 $data['title']  = "Pendaftaran Peserta KSM";
		 
		 if(!empty($ajax)){
					    
			 $this->load->view('form_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "form_peserta";
			 
			 $this->load->view('dashboard/page_header',$data);
		 }
	}

	public function f_peserta(){
		
		 $id = $this->input->get_post("id",TRUE);
		 
		 $data = array();
		    if(!empty($id)){
				
				$data['dataform'] = $this->Di->get_where("tm_siswa",array("id"=>$id));
			}
	     $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
		 $data['title']  = "Form Input Peserta KSM";
		 $this->load->view('form_peserta',$data);
	}
	
	public function f_detail(){
		
		 $id = $this->input->get_post("id",TRUE);
		 
		 $data = array();
		    if(!empty($id)){
				
				$data['dataform'] = $this->Di->get_where("tm_siswa",array("id"=>$id));
			}
	     $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
		 $data['title']  = "Detail ";
		 $this->load->view('detail',$data);
	}
	
	public function simpanpeserta(){
		
		error_reporting(0);
		
		 $this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
         $this->form_validation->set_message('min_length', '{field} Minimal 16 Karakter .');
        $this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
        $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , mungkin anda sudah pernah melakukan pendaftaran, silahkan klik menu login .');
				 
		
    	 
			
				$config = array(
				    //array('field' => 'f[nisn]', 'label' => 'NISN ', 'rules' => 'trim|required'),
				    array('field' => 'f[nik_siswa]', 'label' => 'NIK Siswa  ', 'rules' => 'trim|required|is_unique[tm_siswa.nik_siswa]|min_length[16]'),
				    array('field' => 'f[nama]', 'label' => 'Nama Siswa  ', 'rules' => 'trim|required'),
				    array('field' => 'f[gender]', 'label' => 'Gender  ', 'rules' => 'trim|required'),
				    array('field' => 'f[trkompetisi_id]', 'label' => 'Kompetisi  ', 'rules' => 'trim|required'),
				    array('field' => 'f[tempat]', 'label' => 'Tempat Lahir  ', 'rules' => 'trim|required'),
				   
				    array('field' => 'tgl_lahir', 'label' => 'Tanggal Lahir  ', 'rules' => 'trim|required'),
				    array('field' => 'f[kelas]', 'label' => 'Kelas  ', 'rules' => 'trim|required'),
				    array('field' => 'f[prestasi]', 'label' => 'Prestasi   ', 'rules' => 'trim|required'),
				    array('field' => 'f[nik_ayah]', 'label' => 'NIK Ayah   ', 'rules' => 'trim|required'),
				    array('field' => 'f[nik_ibu]', 'label' => 'NIK Ibu   ', 'rules' => 'trim|required'),
				    array('field' => 'f[nama_ayah]', 'label' => 'Nama Ayah   ', 'rules' => 'trim|required'),
				    array('field' => 'f[nama_ibu]', 'label' => 'Nama Ibu   ', 'rules' => 'trim|required'), 
				  
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
           if ($this->form_validation->run() == true) {
			  
		
		
				
					$f                     = $this->input->get_post("f");
					if($id ==""){
					 $cek     = $this->Di->jumlahdata("tm_siswa",array("tmmadrasah_id"=>$_SESSION['tmmadrasah_id'],"trkompetisi_id"=>$f['trkompetisi_id']));
					  if($cek < 2){

					    $file1 = ($_FILES['file1']['name']); 
					    $file2 = ($_FILES['file2']['name']); 
					    $file3 = ($_FILES['file3']['name']); 
					   $file_id1 ="";
					   $file_id2 ="";
					   $file_id3 ="";

					   $madrasah = $this->db->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']))->row();

					     if(!empty($file1)){
					
							$service = new Google_Drive();
							$foldernama = $madrasah->username;
							$foldernama = str_replace("/",DIRECTORY_SEPARATOR,$foldernama);
							$folderId   = $madrasah->folder;
							if(!$folderId){
								$folderId = $service->getFileIdByName( BACKUP_FOLDER );
								if( !$folderId ) {
									$folderId = $service->createFolder( BACKUP_FOLDER );					
								}
								$folderId = $service->createMultiFolder($foldernama,$folderId);
								$this->db->set("folder",$folderId);
								
								$this->db->where("id",$_SESSION['tmmadrasah_id']);
								$this->db->update("tm_madrasah");
							}

							$fileId = $service->createFileFromPath( $_FILES["file1"]['tmp_name'], $_FILES["file1"]['name'], $folderId );
							$service->setPublic($fileId);


						    $file_id1  	= $fileId;
								   
						 }
						 
						 if(!empty($file2)){
					
							$service = new Google_Drive();
							$foldernama = $madrasah->npsn."-".$madrasah->nama;
							$foldernama = str_replace("/",DIRECTORY_SEPARATOR,$foldernama);
							$folderId   = $madrasah->folder;
							if(!$folderId){
								$folderId = $service->getFileIdByName( BACKUP_FOLDER );
								if( !$folderId ) {
									$folderId = $service->createFolder( BACKUP_FOLDER );					
								}
								$folderId = $service->createMultiFolder($foldernama,$folderId);
								$this->db->set("folder",$folderId);
								
								$this->db->where("id",$_SESSION['tmmadrasah_id']);
								$this->db->update("tm_madrasah");
							}

							$fileId = $service->createFileFromPath( $_FILES["file2"]['tmp_name'], $_FILES["file2"]['name'], $folderId );
							$service->setPublic($fileId);


						    $file_id2  	= $fileId;
								   
						 }
						 if(!empty($file3)){
					
							$service = new Google_Drive();
							$foldernama = $madrasah->npsn."-".$madrasah->nama;
							$foldernama = str_replace("/",DIRECTORY_SEPARATOR,$foldernama);
							$folderId   = $madrasah->folder;
							if(!$folderId){
								$folderId = $service->getFileIdByName( BACKUP_FOLDER );
								if( !$folderId ) {
									$folderId = $service->createFolder( BACKUP_FOLDER );					
								}
								$folderId = $service->createMultiFolder($foldernama,$folderId);
								$this->db->set("folder",$folderId);
								
								$this->db->where("id",$_SESSION['tmmadrasah_id']);
								$this->db->update("tm_madrasah");
							}

							$fileId = $service->createFileFromPath( $_FILES["file3"]['tmp_name'], $_FILES["file3"]['name'], $folderId );
							$service->setPublic($fileId);


						    $file_id3  	= $fileId;
								   
						 }
						 
						 
					  $this->db->set("foto1",$file_id1);
					  $this->db->set("foto2",$file_id2);
					  $this->db->set("foto3",$file_id3);
					  $this->M_lokal->i_peserta();
					  
					   } else {
	         
					   
							header('Content-Type: application/json');
							echo json_encode(array('error' => true, 'message' =>"Anda hanya dapat mengirimkan 2 Peserta Per Kompetisi/Bidang Studi, Silahkan Pilih Kompetisi/Bidang Studi lain "));
						
						}	
	
	
	
					}else{

					 $this->M_lokal->u_peserta($id);
				   
				    }					
												
							
						
						
			 } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
             }	
	
		
	}
	public function h_peserta(){
		
		$this->db->delete("tm_siswa",array("id"=>$_POST['id']));
		$this->db->delete("tr_persyaratan",array("peserta_id"=>$_POST['id']));
		echo "sukses";
		
	}
	
	
	public function persyaratan(){
		
		
		
		 if(!$this->session->userdata("tmmadrasah_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
               exit();
		  }
	   
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Persyaratan Peserta KSM ";
		  $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('persyaratan/page_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "persyaratan/page_peserta";
			 
			 $this->load->view('dashboard/page_header',$data);
		 }
	
		
	}
	
	
	public function g_persyaratan(){
		
		  $iTotalRecords = $this->M_lokal->m_peserta(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->M_lokal->m_peserta(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
				                                if($val['foto']==""){
													$file = "not.png";
												 }else{
													 $file = $val['foto'];
												 }
												 
				 $jp = (3-$this->Di->jumlahdata("tr_persyaratan",array("tmsiswa_id"=>$val['id'])));
				   $keterangan = ($jp ==0) ? " Semua persyaratan sudah dilengkapi": $jp." Persyaratan belum di upload";
												 
				$no = $i++;
				$records["data"][] = array(
					$no,
				    '<img src="'.base_url().'__statics/img/peserta/'.$file.'" class="img-circle img-responsive" style="height:50px;width:50px">',
					$val['nisn'],
					$val['nama'],
					
					$val['gender'],
					$val['tempat'].",".$this->Di->formattanggalstring($val['tgl_lahir']),
					
					$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
					
					
                    '
					<a style="color:white" class="btn btn-info  ubah" urlnya = "'.site_url("lokal/f_persyaratan").'"  datanya="'.$val['id'].'" title="Ubah Data">
					<i class=" fa fa-upload " ></i> Upload 
					</a>
					
					 ',
					$keterangan
										
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	public function f_persyaratan(){
		
		 $id = $this->input->get_post("id",TRUE);
		 
		 $data = array();
		    if(!empty($id)){
				
				$data['dataform'] = $this->Di->get_where("tm_siswa",array("id"=>$id));
			}
	     $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
		 $data['title']  = "Upload Persyaratan ".$data['dataform']->nama;
		 $this->load->view('persyaratan/form_peserta',$data);
	}
	 
	 
	 
	 public function u_persyaratan(){
		
		error_reporting(0);
		
		
				
				$tmpersyaratan_id                    = $this->input->get_post("tmpersyaratan_id");
				$tmsiswa_id                    = $this->input->get_post("tmsiswa_id");
				$persyaratan                    = $this->input->get_post("persyaratan");
					
				$id = $this->input->get_post("id",true);			
				$config['upload_path'] = './__statics/img/peserta/berkas';		
				$config['allowed_types'] = 'jpg|png|jpeg|gif|'; 	
				$config['overwrite'] = true; 	
				$imagenew				 = $_SESSION['tmmadrasah_id']."_".$tmsiswa_id.str_replace(array(" ","-","/","?"),"_",$persyaratan).".".pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
			
				$config['file_name'] = $imagenew;
				
				$this->load->library('upload', $config);

						if ( ! $this->upload->do_upload('file'))
						{
						echo "error[split]";
						
						}
						else{
							 
							 $this->db->delete("tr_persyaratan",array("tmpersyaratan_id"=>$tmpersyaratan_id,"tmsiswa_id"=>$tmsiswa_id));
							 $this->db->set("tmpersyaratan_id",$tmpersyaratan_id);
							 $this->db->set("tmsiswa_id",$tmsiswa_id);
							 $this->db->set("nama",$imagenew);
							 $this->db->insert("tr_persyaratan");
							 
							 echo "sukses[split]";
							
						}
						
						
						
						
						
		
	
		
	}
	
	public function pengumuman(){
		
		
		
		if(!$this->session->userdata("tmmadrasah_id")){
		   echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
			  exit();
		 }
	  
	   
								   
		$ajax = $this->input->get_post("ajax",true);	
		 $data['title']  = "Pengumuman Kompetisi Sains Madrasah ";
		 $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
		if(!empty($ajax)){
					   
			$this->load->view('pengumuman',$data);
		}else{
			
		   
			$data['konten'] = "pengumuman";
			
			$this->load->view('dashboard/page_header',$data);
		}
   
	   
   }

   public function pengumumankartu(){
		
		
		
	if(!$this->session->userdata("tmmadrasah_id")){
	   echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
		  exit();
	 }
  
   
							   
	$ajax = $this->input->get_post("ajax",true);	
	 $data['title']  = "Pengumuman Kompetisi Sains Madrasah ";
	 $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
	if(!empty($ajax)){
				   
		$this->load->view('pengumuman_kotakartu',$data);
	}else{
		
	   
		$data['konten'] = "pengumuman_kotakartu";
		
		$this->load->view('dashboard/page_header',$data);
	}

   
}


   public function pengumumanprov(){
		
	
   
							   
	$ajax = $this->input->get_post("ajax",true);	
	 $data['title']  = "Pengumuman Kompetisi Sains Madrasah ";
	 $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
	if(!empty($ajax)){
				   
		$this->load->view('pengumuman_prov',$data);
	}else{
		
	   
		$data['konten'] = "pengumuman_prov";
		
		$this->load->view('dashboard/page_header',$data);
	}

   
}

public function kartunasional(){
		
		
		
	if(!$this->session->userdata("tmmadrasah_id")){
	   echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
		  exit();
	 }
  
   
							   
	$ajax = $this->input->get_post("ajax",true);	
	 $data['title']  = "Pengumuman Kompetisi Sains Madrasah ";
	 $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
	if(!empty($ajax)){
				   
		$this->load->view('pengumuman_naskartu',$data);
	}else{
		
	   
		$data['konten'] = "pengumuman_naskartu";
		
		$this->load->view('dashboard/page_header',$data);
	}

   
}

	public function status(){
		
		
		
		 if(!$this->session->userdata("tmmadrasah_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
               exit();
		  }
	   
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Kartu Peserta ";
		  $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('status/page_kartu',$data);
		 }else{
			 
			
		     $data['konten'] = "status/page_kartu";
			 
			 $this->load->view('dashboard/page_header',$data);
		 }
	
		
	}
	

	
	
	public function g_status(){
		
		  $iTotalRecords = $this->M_lokal->m_peserta_kabko(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->M_lokal->m_peserta_kabko(true)->result_array();
		   $sarray = array("0"=>"Belum/Tidak di Approval","1"=>"Sudah di Approval");
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
				  //$notest = "<a class='btn btn-info btn-sm' disabled href='".site_url("lokal/kartutes?tmsiswa_id=".base64_encode($val['id'])."")."' target='_blank' ><span class='fa fa-download'></span> Download Kartu  </a>";	
                    $notest ="Closed";          
												 
				 									 
				$no = $i++;
				$records["data"][] = array(
					$no,
					$notest,
					$val['no_test'],
				 	$val['nisn'],
					$val['nama'],
					
					$val['gender'],
					$val['tempat'].",".$this->Di->formattanggalstring($val['tgl_lahir']),
					
					$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama")
					
					
										
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
		public function kartu(){
		
		
		
		 if(!$this->session->userdata("tmmadrasah_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
               exit();
		  }
	   
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Kartu Peserta KSM ";
		  $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('kartu/page_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "kartu/page_peserta";
			 
			 $this->load->view('dashboard/page_header',$data);
		 }
	
		
	}
	
	
	public function perbaharui_akun(){
		  $this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
        
        $this->form_validation->set_message('min_length', '{field} Minimal 6 Karakter .');
        $this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
        $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , mungkin anda sudah pernah melakukan pendaftaran, silahkan klik menu login .');
				 
		 
			
				$config = array(
					    array('field' => 'password', 'label' => 'Password  ', 'rules' => 'trim|required|min_length[6]'),
				        array('field' => 'passwordc', 'label' => 'Ulangi Password ', 'rules' => 'trim|required|matches[password]'),
			
				 
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			
			
		    $this->db->set("password",(($_POST['password'])));
		    $this->db->where("id",$_SESSION['tmmadrasah_id']);
		    $this->db->update("tm_madrasah");
			echo "sukses";
		}else{
			
			 header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
			
		}
		
	
	}
	
	public function kartuPesertaKabkoUnduh(){
	    
	    	 		 
				$ret = '';
				$data['breadcumb']  ="Kartu Tes";
				$data['data']      = $this->db->query("select * from   v_siswa where id='".base64_decode($_GET['key'])."'")->row();
					
			    $this->db->query("update tm_siswa set statuskabko=1 where id='{$data['data']->id}'");
				$pdf_filename = 'Kartutes'.str_replace(array("'",",","-"),"",$data['data']->nama).'.pdf';	
				$user_info = $this->load->view('kartu/kartukabko', $data, true);
			
				 $output = $user_info;
				
				
			
				generate_pdf($output, $pdf_filename,TRUE);
				
	    
	}
	public function kartuPesertaKabkoPrint(){
	    
	    	 		 
		$ret = '';
		$data['breadcumb']  ="Kartu Tes";
		$data['data']      = $this->db->query("select * from   v_siswa where id='".base64_decode($_GET['key'])."'")->row();
			
		$this->db->query("update tm_siswa set statuskabko=1 where id='{$data['data']->id}'");
		$pdf_filename = 'Kartutes'.str_replace(array("'",",","-"),"",$data['data']->nama).'.pdf';	
		$user_info = $this->load->view('kartu/kartukabkoprint', $data, true);
	
		echo $output = $user_info;
		
		
	
	
		

}

public function kartuPesertaprovPrint(){
	    
	    	 		 
	$ret = '';
	$data['breadcumb']  ="Kartu Tes";
	$data['data']      = $this->db->query("select * from   v_siswa where id='".base64_decode($_GET['key'])."'")->row();
		
	$this->db->query("update tm_siswa set statuskabko=1 where id='{$data['data']->id}'");
	$pdf_filename = 'Kartutes'.str_replace(array("'",",","-"),"",$data['data']->nama).'.pdf';	
	$user_info = $this->load->view('kartu/kartuprovprint', $data, true);

	echo $output = $user_info;
	
	


	

}


	public function kartutesprovinsi(){
	    
	    	 		 
		$ret = '';
		
			
 
			$data['breadcumb']  ="Kartu Tes";
		
			$data['data']      = $this->db->query(" select * from   v_siswa where id='".base64_decode($_GET['key'])."'")->row();
			
		$this->db->query("update tm_siswa set status_provinsi=1 where id='{$data['data']->id}'");
		$pdf_filename = 'Kartutes'.str_replace(array("'",",","-"),"",$data['data']->nama).'.pdf';	 
		$data_header = array('title' => 'Kartu Tes',);
	
		
		$user_info = $this->load->view('kartu/kartu_provinsi', $data, true);
	
		$output = $user_info;
		
		
	
		generate_pdf($output, $pdf_filename,TRUE);
		

}

public function kartutesnasional(){
	    
	    	 		 
	$ret = '';
	
		

		$data['breadcumb']  ="Kartu Tes";
	
		$data['data']      = $this->db->query(" select * from   v_siswa where id='".base64_decode($_GET['key'])."'")->row();
		
	
	$pdf_filename = 'Kartutes'.str_replace(array("'",",","-"),"",$data['data']->nama).'.pdf';	 
	$data_header = array('title' => 'Kartu Tes',);

	
	$user_info = $this->load->view('kartu/kartu_nasional', $data, true);

	echo $output = $user_info;
	
	

	//generate_pdf($output, $pdf_filename,TRUE);
	

}

// Kabkota

public function kabkota(){
		
		
					   
	$ajax = $this->input->get_post("ajax",true);	
	 $data['title']  = "Peserta KSM  Kabkota ";
	 $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
	if(!empty($ajax)){
				   
		$this->load->view('kabkota/page_peserta',$data);
	}else{
		
	   
		$data['konten'] = "kabkota/page_peserta";
		
		$this->load->view('dashboard/page_header',$data);
	}

   
}


public function g_kabkota(){
	error_reporting(0);
	 $iTotalRecords = $this->M_lokal->m_peserta_provinsi(false)->num_rows();
	 
	 $iDisplayLength = intval($_REQUEST['length']);
	 $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
	 $iDisplayStart = intval($_REQUEST['start']);
	 $sEcho = intval($_REQUEST['draw']);
	 
	 $records = array();
	 $records["data"] = array(); 

	 $end = $iDisplayStart + $iDisplayLength;
	 $end = $end > $iTotalRecords ? $iTotalRecords : $end;
	 
	 $datagrid = $this->M_lokal->m_peserta_provinsi(true)->result_array();
	  $sarray = array("0"=>"Belum/Tidak di Approval","1"=>"Sudah di Approval");
	  $i= ($iDisplayStart +1);
	  foreach($datagrid as $val) {
										   
	   $hapus ="";
	   $img ='<img src="https://ksm.kemenag.go.id/__statics/upload/'.$val['foto'].'" alt="" class="img-responsive" width="100px">';  
   
		 $ket="";
		 if($val['skrn']==1){

		  $ket="Terdapat catatan pelanggaran dari pengawas dan berhasil dikonfirmasi oleh Komite Nasional, Skor Peserta ini dikurangi 10";
		 }
	  $no = $i++;
	  $records["data"][] = array(
	  
		  $img,
			$val['no_test2'],
			$val['nama'],
		   $val['gender'],
		   $val['tempat'].",".$this->Di->formattanggalstring($val['tgl_lahir']),
		  
		  $this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
		 
		  $val['bnr_pg'],
		  $val['bnr_isi'],
		  $val['jml_benar'],
		  $val['slh'],
		  $val['ksg'],
		  $val['nla'],
		  $val['d_p'],
		  $ket,
			   
			   
			 
								   
					   
		   
			  

			 );
		 }
   
	 $records["draw"] = $sEcho;
	 $records["recordsTotal"] = $iTotalRecords;
	 $records["recordsFiltered"] = $iTotalRecords;
	 
	 echo json_encode($records);
}



	
	// Provinsi 
	
	public function provinsi(){
		
		
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Peserta Tes Provinsi ";
		  $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('provinsi/page_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "provinsi/page_peserta";
			 
			 $this->load->view('dashboard/page_header',$data);
		 }
	
		
	}
	
	
	public function g_provinsi(){
		 error_reporting(0);
		  $iTotalRecords = $this->M_lokal->m_peserta_provinsi(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->M_lokal->m_peserta_provinsi(true)->result_array();
		  $ketkabko = array("1"=>"Juara I","2"=>"Juara II","3"=>"Juara III","4"=>"Juara Harapan I","5"=>"Juara Harapan II","6"=>"Juara Harapan III");
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
				                                
			$hapus ="";
			$img ='<img src="https://ksm.kemenag.go.id/__statics/upload/'.$val['foto'].'" alt="" class="img-responsive" width="80px">';  
		
			 // $ket="";
			//   if($val['skrn']==1){

			//    $ket="Terdapat catatan pelanggaran dari pengawas dan berhasil dikonfirmasi oleh Komite Nasional, Skor Peserta ini dikurangi 10";
			//   }
		   $no = $i++;
		   $records["data"][] = array(
		   
			   $no,
			   '
					<div class="btn-group" role="group" aria-label="Basic example">
						
						<button type="button" class="btn btn-info ubah btn-sm" urlnya = "'.site_url("lokal/detail").'"  datanya="'.$val['id'].'" title="Detail Data"><i class=" fa fa-list"></i>  Detail </button>
						<button type="button" class="btn btn-info rincian btn-sm" data-toggle="modal" data-target="#myModalRincian"  datanya="'.$val['id'].'" title="Rincian Jawaban"><i class=" fa fa-list"></i>  Rincian Jawaban </button>
						
					</div>',
			   $img,
				 $val['nik_siswa'],
				 $val['nama'],
				$val['gender'],
			  //  $val['tempat'].",".$this->Di->formattanggalstring($val['tgl_lahir']),
			   
			   $this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
			   $val['peringkat_prov'],
			   $val['bnr_pg'],
			   $val['bnr_isi'],
			   $val['jml_benar'],
			   $val['slh'],
			   $val['ksg'],
			   $val['nla'],
			   $ketkabko[$val['d_p']]
			  // $ket,
					
					
                  
										
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	

	public function rincianJawaban(){

		$id = $this->input->get_post("id",true);

		$data = $this->db->query("SELECT * from t_hsl where idPs='{$id}' order by nmr ASC")->result();
		  ?>
		    <div class="table-responsive">
				<table class="table table-hover table-bordered table-striped">
					<thead>
						<tr>
							<th>NOMOR SOAL </th>
							<th>JAWABAN PESERTA</th>
							<th>HASIL</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						foreach($data as $r){
						?>
						<tr>
							<td><?php echo $r->nmr; ?></td>
							<td><?php echo $r->jwb; ?></td>
							<td><?php echo $r->skor; ?></td>
						</tr>
						<?php 
						}
						?>
					</tbody>
				</table>
			</div>

			<?php 
		   


		   
	}
	
	// Nasional 
	
	public function nasional(){
		
		
		
		 if(!$this->session->userdata("tmmadrasah_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
               exit();
		  }
	   
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Peserta Tes Nasional ";
		  $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('nasional/page_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "nasional/page_peserta";
			 
			 $this->load->view('dashboard/page_header',$data);
		 }
	
		
	}
	
	
	public function g_nasional(){
		
		error_reporting(0);
		$iTotalRecords = $this->M_lokal->m_hasilNasional(false)->num_rows();
		$jenjang = array("1"=>"MI/SD","2"=>"MTs/SMP","3"=>"MA/SMA");
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);
		
		$records = array();
		$records["data"] = array(); 

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;
		
		$datagrid = $this->M_lokal->m_hasilNasional(true)->result_array();
		$arjenis = array("1"=>"Madrasah","2"=>"Sekolah");
		 $i= ($iDisplayStart +1);
		 foreach($datagrid as $val) {
			 $cbtNasional = $this->db->get_where("nilai_nasional",array("idPs"=>$val['id']))->row();							 
	   

			  $foto ="img/ksm.png";
			   if(!empty($val['foto'])){

				  $foto ="upload/{$val['foto']}";
			   }
			   $img ='<img src="https://ksm.kemenag.go.id/__statics/'.$foto.'" class="img-responsive circle" style="width:80px;height:80px;border-radius: 30%;">';
											   
			  $no = $i++;
			   $nasional_juara="-";
			  if($val['nasional_juara']=="emas"){

				  $nasional_juara = '<span class="badge badge-warning"> Emas </span>';
			  }else if($val['nasional_juara']=="perak"){

				   $nasional_juara = '<span class="badge badge-info"> Perak </span>';
			  }else if($val['nasional_juara']=="perunggu"){
			   $nasional_juara = '<span class="badge badge-dark"> Perunggu </span>';
			  }
		   
			  $records["data"][] = array(
				
				  $val['nasional_peringkat'],
				  "<a href='".site_url('lokal/sertifikatnasional?q='.base64_encode($val['id']).'&nik='.base64_encode($val['nik_siswa']).'')."' target='_blank' class=' btn-sm btn btn-danger'><i class='fa fa-file'></i> Unduh Sertifikat</a>",
				  $img,
				  $val['no_test3'],
				  $val['nama'],
				  $this->Di->get_kondisi(array("id"=>$val['provinsi_madrasah']),"provinsi","nama"),
				  $val['madrasah'],
				  $this->Di->jenjangnama($val['jenjang'])." ".$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
				  $nasional_juara,
				  $val['nasional_nilai_akhir'],
				  $val['nasional_cbt'],
				  $val['nasional_eksplorasi'],
				  
				  $cbtNasional->bnr_pg,
				  $cbtNasional->bnr_isi,
				  $cbtNasional->bnr,
				  $cbtNasional->slh,
				  $cbtNasional->ksg,
				  $val['nilai_provinsi'],
				  $val['jml_benar_provinsi'],
				  $val['slh_provinsi'],
				  $val['ksg_provinsi'],

				  $val['nilai_kabko'],
				  $val['jml_benar_kabko'],
				  $val['slh_kabko'],
				  $val['ksg_kabko'],
				  $val['nik_siswa'],
				  $val['nisn'],
				  $val['tempat'],
				  $this->Di->formattanggaldb($val['tgl_lahir']),
				  $val['nik_ayah'],
				  $val['nama_ayah'],
				  $val['nik_ibu'],
				  $val['nama_ibu'],		   
			   
				  $this->Di->get_kondisi(array("id"=>$val['kota']),"kota","nama"),
				  $this->Di->get_kondisi(array("id"=>$val['kecamatan']),"kecamatan","nama"),
				  $this->Di->get_kondisi(array("id"=>$val['desa']),"desa","nama"),
				 
				  
				  $arjenis[$val['jenis']],
				  $this->Di->jenjangnama($val['jenjang']),
				  $val['username'],
				  $val['madrasah'],
				  $val['delegasi_nik'],
				  $val['delegasi_nama'],
				 
				  $val['delegasi_hp'],

				  $val['bnr_pg'],
				  $val['bnr_isi'],
				  $val['slh'],
				  $val['ksg'],
				  $val['nla'],
				  $this->Di->get_kondisi(array("tmsiswa_id"=>$val['id']),"h_ujian","list_jawaban"),
				  $val['nilai_provinsi'],
				  $val['nilai_kabko']
				  
				  
									  
						  
			  
				 

				);
			}
	  
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		
		echo json_encode($records);
	
	}
	public function build_pwd(){
		
		foreach($this->db->get("provinsi")->result() as $r){
			
			$pwd= strtoupper($this->Di->get_id(6));
			
			$this->db->set("password",sha1(md5($pwd)));
			$this->db->set("alias",$pwd);
			$this->db->where("id",$r->id);
			$this->db->update("provinsi");
			
			
			
		}
		echo "sukses";
	}

	public function step1(){
		error_reporting(0);
		
		$this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
		$this->form_validation->set_message('min_length', '{field} Minimal 16 Karakter .');
	   $this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
	   $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , mungkin anda sudah pernah melakukan pendaftaran, silahkan klik menu login .');
				
	   
		
		   
			   $config = array(
				   //array('field' => 'f[nisn]', 'label' => 'NISN ', 'rules' => 'trim|required'),
				   array('field' => 'f[nik_siswa]', 'label' => 'NIK Siswa  ', 'rules' => 'trim|required|min_length[16]'),
				   array('field' => 'f[nama]', 'label' => 'Nama Siswa  ', 'rules' => 'trim|required'),
				   array('field' => 'f[gender]', 'label' => 'Gender  ', 'rules' => 'trim|required'),
				   array('field' => 'f[trkompetisi_id]', 'label' => 'Kompetisi  ', 'rules' => 'trim|required'),
				   array('field' => 'f[tempat]', 'label' => 'Tempat Lahir  ', 'rules' => 'trim|required'),
				  
				   array('field' => 'tgl_lahir', 'label' => 'Tanggal Lahir  ', 'rules' => 'trim|required'),
				   array('field' => 'f[kelas]', 'label' => 'Kelas  ', 'rules' => 'trim|required'),
				   array('field' => 'f[prestasi]', 'label' => 'Prestasi   ', 'rules' => 'trim|required'),
				   array('field' => 'f[nik_ayah]', 'label' => 'NIK Ayah   ', 'rules' => 'trim|required'),
				   array('field' => 'f[nik_ibu]', 'label' => 'NIK Ibu   ', 'rules' => 'trim|required'),
				   array('field' => 'f[nama_ayah]', 'label' => 'Nama Ayah   ', 'rules' => 'trim|required'),
				   array('field' => 'f[nama_ibu]', 'label' => 'Nama Ibu   ', 'rules' => 'trim|required'), 
				 
   
				   
			   );
			   
			   $this->form_validation->set_rules($config);	
	   
		  if ($this->form_validation->run() == true) {
			 
	   
	   
			   
				   $f                     = $this->input->get_post("f");
				   $id                    = $this->input->get_post("id");
				   if($id ==""){

					$cekNik = $this->Di->jumlahdata("tm_siswa",array("tmmadrasah_id"=>$_SESSION['tmmadrasah_id'],"nik_siswa"=>$f['nik_siswa']));

					if($cekNik == 0){
								$cek     = $this->Di->jumlahdata("tm_siswa",array("tmmadrasah_id"=>$_SESSION['tmmadrasah_id'],"trkompetisi_id"=>$f['trkompetisi_id']));
								if($cek < 2){
									
								$id = $this->M_lokal->i_peserta();
								$data['dataform'] = $this->db->get_where("tm_siswa",array("id"=>$id))->row();

								$this->load->view("syarat",$data);
								
								} else {
						
								
									header('Content-Type: application/json');
									echo json_encode(array('error' => true, 'message' =>"Anda hanya dapat mengirimkan 2 Peserta PerBidang Studi, Silahkan Pilih Kompetisi/Bidang Studi lain "));
								
								}	

					}else{

						header('Content-Type: application/json');
									echo json_encode(array('error' => true, 'message' =>" NIK ".$f['nik']." sudah terdaftar, Peserta ini sudah terdaftar silahkan cek pada data peserta KSM "));
					}
			
   
   
				   }else{

					$this->M_lokal->u_peserta($id);
					$data['dataform'] = $this->db->get_where("tm_siswa",array("id"=>$id))->row();

					   $this->load->view("syarat",$data);
				  
				   }					
											   
						   
					   
					   
			} else {
			
		  
			   header('Content-Type: application/json');
			   echo json_encode(array('error' => true, 'message' => validation_errors()));
		   
			}	

	}

	public function berkas_upload(){
	
		$persyaratan_id = $this->input->get_post("persyaratan_id");
		$peserta_id 	= $this->input->get_post("peserta_id");
		$namasiswa	    = $this->Di->get_kondisi(array("id"=>$peserta_id),"tm_siswa","nama");
		$nik		    = $this->Di->get_kondisi(array("id"=>$peserta_id),"tm_siswa","nik_siswa")."-".str_replace(array("'","-",":","."," ",","),"_",$namasiswa)."-".time();
		$persyaratan    = $this->db->select("*")->from("persyaratan")->where("id",$persyaratan_id)->get()->row();

	   if($persyaratan_id !=1){
		$madrasah = $this->db->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']))->row();

	
   
		   $service = new Google_Drive();
		   $foldernama = $madrasah->username."-".str_replace(array("'","-",":","."),"_",$madrasah->nama);
		   $foldernama = str_replace("/",DIRECTORY_SEPARATOR,$foldernama);
		   $folderId   = $madrasah->folder;
		   	
		   if(!$folderId){
			   $folderId = $service->getFileIdByName( BACKUP_FOLDER );
			   if( !$folderId ) {
				   $folderId = $service->createFolder( BACKUP_FOLDER );					
			   }
			   $folderId = $service->createMultiFolder($foldernama,$folderId);
			   $this->db->set("folder",$folderId);
			   
			   $this->db->where("id",$_SESSION['tmmadrasah_id']);
			   $this->db->update("tm_madrasah");
		   }
		   
		   
			$namafile   =  $nik."-".$persyaratan_id.".".pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
		   

			  $fileId = $service->createFileFromPath( $_FILES["file"]['tmp_name'], $namafile, $folderId );
			  $service->setPublic($fileId);

			  if (empty($fileId)) {
					  header("Content-Type: application/json");
					  echo json_encode(array('error' =>'Proses upload gagal dilakukan, silahkan coba kembali',"uploaded"=>"error"));
				  
			  }else{
			   $cek = $this->db->get_where("tr_persyaratan",array("peserta_id"=>$peserta_id,"persyaratan_id"=>$persyaratan_id))->num_rows();
			   
			   if($cek==0){
				   $this->db->set("peserta_id",$peserta_id);
				   $this->db->set("persyaratan_id",$persyaratan_id);
				   $this->db->set("file",$fileId);
				   $this->db->insert("tr_persyaratan");

				}else{
				   if($persyaratan->multiple==1){

					   $this->db->set("peserta_id",$peserta_id);
					   $this->db->set("persyaratan_id",$persyaratan_id);
					   $this->db->set("file",$fileId);
					   $this->db->insert("tr_persyaratan");

				   }else{
					   $this->db->where("peserta_id",$peserta_id);
					   $this->db->where("persyaratan_id",$persyaratan_id);
					   $this->db->set("file",$fileId);
					   $this->db->update("tr_persyaratan");
				   }

				}
				  
			   
				   header('Content-Type: application/json');
					   echo json_encode(array('success' => true, 'message' => "Upload Sukses"));


			  }



	   }else{	


		   $config['upload_path'] = './__statics/upload';
				   $folder   = '__statics/upload';
				   $config['allowed_types'] = "*"; 	
				   $config['overwrite'] = true; 	
				   $filenamepecah			 = explode(".",$_FILES['file']['name']);
				   $imagenew				 = $nik."-".$persyaratan_id.".".pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
				   
				   $config['file_name'] = $imagenew;
				   
				   $this->load->library('upload', $config);

						   if ( ! $this->upload->do_upload('file'))
						   {
							   header('Content-Type: application/json');
							   echo json_encode(array('error' => true, 'message' => $this->upload->display_errors()));
									   
							   
						   
						   }else{


									$madrasah = $this->db->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']))->row();

			
		
									$service = new Google_Drive();
									$foldernama = $madrasah->username."-".str_replace(array("'","-",":","."),"_",$madrasah->nama);
									$foldernama = str_replace("/",DIRECTORY_SEPARATOR,$foldernama);
									$folderId   = $madrasah->folder;
									if(!$folderId){
										$folderId = $service->getFileIdByName( BACKUP_FOLDER );
										if( !$folderId ) {
											$folderId = $service->createFolder( BACKUP_FOLDER );					
										}
										$folderId = $service->createMultiFolder($foldernama,$folderId);
										$this->db->set("folder",$folderId);
										
										$this->db->where("id",$_SESSION['tmmadrasah_id']);
										$this->db->update("tm_madrasah");
									}

								   $fileId = $service->createFileFromPath($_FILES["file"]['tmp_name'], $imagenew, $folderId );
								   $service->setPublic($fileId);

								   if (empty($fileId)) {
										   header("Content-Type: application/json");
										   echo json_encode(array('error' =>'Proses upload gagal dilakukan, silahkan coba kembali',"uploaded"=>"error"));
									   
								   }else{


									   $cek = $this->db->get_where("tr_persyaratan",array("peserta_id"=>$peserta_id,"persyaratan_id"=>$persyaratan_id))->num_rows();
			   
										   if($cek==0){
											   $this->db->set("peserta_id",$peserta_id);
											   $this->db->set("persyaratan_id",$persyaratan_id);
											   $this->db->set("file",$fileId);
											   $this->db->insert("tr_persyaratan");

										   }else{

											   $this->db->where("peserta_id",$peserta_id);
											   $this->db->where("persyaratan_id",$persyaratan_id);
											   $this->db->set("file",$fileId);
											   $this->db->update("tr_persyaratan");

										   }

										   $this->db->query("update tm_siswa set foto='".$imagenew."' where id='".$peserta_id."'");

										   header('Content-Type: application/json');
										   echo json_encode(array('success' => true, 'message' => "Upload Sukses"));


								   }





						   }



	   }


	}

   public function hapus_berkas(){

	   $this->db->where("id",$this->input->get_post("key"));
	   $this->db->delete("tr_persyaratan");
	   echo json_encode(array('success' => true, 'message' => "Berhasil Menghapus"));

   }

   public function step3(){
	$id = $this->input->get_post("id");
	$data['dataform'] = $this->db->get_where("tm_siswa",array("id"=>$id))->row();

     $this->load->view("finish",$data);


	 
   }

   public function kirim(){

	$this->db->where("id",$this->input->get_post("id"));
	$this->db->set("status",1);
	$this->db->update("tm_siswa");
	echo "sukses";

}


public function f_peserta_u(){
		
	$id = $this->input->get_post("id",TRUE);
	
	$data = array();
	   if(!empty($id)){
		   
		   $data['dataform'] = $this->Di->get_where("tm_siswa",array("id"=>$id));
	   }
  
	$data['title']  = "Perbaiki Data Peserta KSM";
	$this->load->view('form_peserta_u',$data);
}

public function simpanpeserta_u(){
   
   //error_reporting(0);
   
	$this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
   
	
	   
		   $config = array(
			 //  array('field' => 'f[nisn]', 'label' => 'NISN ', 'rules' => 'trim|required'),
			   array('field' => 'f[nama]', 'label' => 'Nama Siswa  ', 'rules' => 'trim|required'),
			   array('field' => 'f[gender]', 'label' => 'Gender  ', 'rules' => 'trim|required'),
		   
			   array('field' => 'f[tempat]', 'label' => 'Tempat Lahir  ', 'rules' => 'trim|required'),
			   array('field' => 'f[tgl_lahir]', 'label' => 'Tanggal Lahir  ', 'rules' => 'trim|required'),
			 
			

			   
		   );
		   
		   $this->form_validation->set_rules($config);	
   
	  if ($this->form_validation->run() == true) {
		 
   
   
		   
			 $f  = $this->input->get_post("f");
			 $tgl_lahir  = $this->input->get_post("tgl_lahir");
			 $id         = $this->input->get_post("id");
			
			  if(!empty($_FILES['file']['name'])){
					   $config['upload_path'] = './__statics/upload';
					   $folder   = '__statics/upload';
					   $config['allowed_types'] = "*"; 	
					   $config['overwrite'] = true; 	
					   $filenamepecah			 = explode(".",$_FILES['file']['name']);
					   $imagenew				 = $peserta_id.$persyaratan_id.time().".".pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
					   
					   $config['file_name'] = $imagenew;
					   
					   $this->load->library('upload', $config);
	
							   if ( ! $this->upload->do_upload('file'))
							   {
								$this->db->where("id",$id);
								
								$this->db->update("tm_siswa",$f);
								   
							   
							   }else{
								$id = $this->input->get_post("id",true);
								$siswanya = $this->db->get_where("v_siswa",array("id"=>$id))->row();
	
										$madrasah = $this->db->get_where("tm_madrasah",array("id"=>$siswanya->tmmadrasah_id))->row();
	
				
			
										$service = new Google_Drive();
										$foldernama = $madrasah->username;
										$foldernama = str_replace("/",DIRECTORY_SEPARATOR,$foldernama);
										$folderId   = $madrasah->folder;
										if(!$folderId){
											$folderId = $service->getFileIdByName( BACKUP_FOLDER );
											if( !$folderId ) {
												$folderId = $service->createFolder( BACKUP_FOLDER );					
											}
											$folderId = $service->createMultiFolder($foldernama,$folderId);
											$this->db->set("folder",$folderId);
											
											$this->db->where("id",$siswanya->tmmadrasah_id);
											$this->db->update("tm_madrasah");
										}
	
									   $fileId = $service->createFileFromPath( $_FILES["file"]['tmp_name'], $_FILES["file"]['name'], $folderId );
									   $service->setPublic($fileId);
	
									   if (empty($fileId)) {
										header('Content-Type: application/json');
										echo json_encode(array('error' => true, 'message' =>"Gagal"));
										   
									   }else{
	
											$this->db->where("peserta_id",$id);
											$this->db->where("persyaratan_id",1);
											$this->db->set("file",$fileId);
											$this->db->update("tr_persyaratan");

																		
												$this->db->where("id",$id);
												
												$this->db->update("tm_siswa",$f);
	
												 $this->db->query("update tm_siswa set foto='".$imagenew."' where id='".$id."'");
	
	
	
									   }
	
	
	
	
	
							   }


							}else{

								$this->db->where("id",$id);
							
								$this->db->update("tm_siswa",$f);


							}


					  

					   echo "sukses";
				   
							   
							   
			 
				   
				   
		} else {
		
	  
		   header('Content-Type: application/json');
		   echo json_encode(array('error' => true, 'message' => validation_errors()));
	   
		}	

   
}

// Piagam 2021

public function sertifikatkota(){

	$tmsiswa_id = base64_decode($this->input->get("q")); 
	$nik       = base64_decode($this->input->get("nik")); 

   $datasiswa = $this->db->query("select d_p,nik_siswa,madrasah,provinsi,trkompetisi_id,id,nama,jenjang_madrasah,kota_madrasah from v_siswa_kabkota where nla IS NOT NULL AND id='".$tmsiswa_id."' and nik_siswa='{$nik}'")->row();
	   
   if(count($datasiswa) >0){

	   $pdf = new Fpdi();

	   $this->load->library('ciqrcode'); //pemanggilan library QR CODE

	   $config['cacheable']	= true; 
	   $config['cachedir']		= './tmp/chache'; 
	   $config['errorlog']		= './tmp/'; 
	   $config['imagedir']		= './tmp/'; 
	   $config['quality']		= true; 
	   $config['size']			= '1024'; 
	   $config['black']		= array(224,255,255);
	   $config['white']		= array(70,130,180); 
	   $this->ciqrcode->initialize($config);

	   //$image_name=$datasiswa->id.'.png'; 
	   $image_name= $datasiswa->id.'.png'; 

	   $params['data'] = "https://ksm.kemenag.go.id/publik/validitas?q=".base64_encode($datasiswa->nik_siswa)."&s=".base64_encode($datasiswa->id).""; 
	   $params['level'] = 'H'; 
	   $params['size'] = 10;
	   $params['savename'] = FCPATH.$config['imagedir'].$image_name; 
	   $this->ciqrcode->generate($params);

	   $ketkabko = array("1"=>"Juara I","2"=>"Juara II","3"=>"Juara III","4"=>"Juara Harapan I","5"=>"Juara Harapan II","6"=>"Juara Harapan III");
	   $jenjangn = array("1"=>"Madrasah Ibtidaiyah/Sederajat ","2"=>"Madrasah Tsanawiyah/Sederajat","3"=>"Madrasah Aliyah/Sederajat");
	   $kompetisi = $this->Di->get_kondisi(array("id"=>$datasiswa->trkompetisi_id),"tr_kompetisi","nama");
	   $kota_madrasah = $this->Di->get_kondisi(array("id"=>$datasiswa->kota_madrasah),"kota","nama");
	   $wilayah = strtoupper($jenjangn[$datasiswa->jenjang_madrasah])." ".$kota_madrasah;


		 if($datasiswa->d_p <= 6){
   
		   $status =   $ketkabko[$datasiswa->d_p];
		  
		  
	   


		   $pdf->addPage('L');
		   
		   $pdf->setSourceFile('juara.pdf');
	   
		   $tplIdx = $pdf->importPage(1);
		   $pdf ->useTemplate($tplIdx, null, null, null, null,FALSE);
		   
		   $pdf->Ln(5);
		   $pdf->SetFont('Arial','B',26);
		   $pdf->SetTextColor(6, 0, 0);

		   $pdf->Cell(null,93,$datasiswa->nama,0,0,'C');

		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,$datasiswa->madrasah,0,0,'C');

		   $pdf->Ln(20);
		   $pdf->SetFont('Arial','B',26);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(271,93,$status,0,0,'C');


		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,"Bidang Studi ".$kompetisi,0,0,'C');

		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,"JENJANG ".$wilayah,0,0,'C');
		   
		   
		   //$pdf->Write(0, '');


		   $pdf->Image("tmp/".$image_name,260,170,30,30);


		   $pdf->Output('I', 'PIAGAM  '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf');

		   }else{
			   $status = " Peserta ";




			   $pdf->addPage('L');
		   
			   $pdf->setSourceFile('peserta.pdf');
		   
			   $tplIdx = $pdf->importPage(1);
			   $pdf ->useTemplate($tplIdx, null, null, null, null,FALSE);
			   
			   
			   $pdf->SetFont('Arial','B',26);
			   $pdf->SetTextColor(6, 0, 0);
   
			   $pdf->Cell(null,100,$datasiswa->nama,0,0,'C');
   
			   $pdf->Ln(15);
			   $pdf->SetFont('Arial','B',15);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(null,93,$datasiswa->madrasah,0,0,'C');
   
			   $pdf->Ln(20);
			   $pdf->SetFont('Arial','B',26);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(271,93,null,0,0,'C');
   
   
			   $pdf->Ln(10);
			   $pdf->SetFont('Arial','B',15);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(null,93,"Bidang Studi ".$kompetisi,0,0,'C');
   
			   $pdf->Ln(10);
			   $pdf->SetFont('Arial','B',15);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(null,93,"JENJANG ".$wilayah,0,0,'C');
			   
			   
			   //$pdf->Write(0, '');
   
   
			   $pdf->Image("tmp/".$image_name,260,170,30,30);
   
   
			   $pdf->Output('I', 'PIAGAM  '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf');


	   
		   }


   }else{

	   echo "404 Not Found";
   }
 }



public function sertifikatprovinsi(){

	$tmsiswa_id = base64_decode($this->input->get("q")); 
	$nik       = base64_decode($this->input->get("nik")); 

   $datasiswa = $this->db->query("select peringkat_prov,nik_siswa,madrasah,provinsi,trkompetisi_id,id,nama,jenjang_madrasah,provinsi_madrasah,tmmadrasah_id from v_siswa_provinsi where nla IS NOT NULL AND id='".$tmsiswa_id."' and tmmadrasah_id='{$_SESSION['tmmadrasah_id']}'")->row();
	   
   if(count($datasiswa) >0){

	   $pdf = new Fpdi();

	   $this->load->library('ciqrcode'); //pemanggilan library QR CODE

	   $config['cacheable']	= true; 
	   $config['cachedir']		= './tmp/chache'; 
	   $config['errorlog']		= './tmp/'; 
	   $config['imagedir']		= './tmp/'; 
	   $config['quality']		= true; 
	   $config['size']			= '1024'; 
	   $config['black']		= array(224,255,255);
	   $config['white']		= array(70,130,180); 
	   $this->ciqrcode->initialize($config);

	   //$image_name=$datasiswa->id.'.png'; 
	   $image_name= $datasiswa->id.'prov.png'; 

	   $params['data'] = "https://ksm.kemenag.go.id/publik/validitasprovinsi?q=".base64_encode($datasiswa->tmmadrasah_id)."&s=".base64_encode($datasiswa->id).""; 
	   $params['level'] = 'H'; 
	   $params['size'] = 10;
	   $params['savename'] = FCPATH.$config['imagedir'].$image_name; 
	   $this->ciqrcode->generate($params);

	   $ketkabko = array("1"=>"Juara I","2"=>"Juara II","3"=>"Juara III","4"=>"Juara Harapan I","5"=>"Juara Harapan II","6"=>"Juara Harapan III");
	   $jenjangn = array("1"=>"Madrasah Ibtidaiyah/Sederajat ","2"=>"Madrasah Tsanawiyah/Sederajat","3"=>"Madrasah Aliyah/Sederajat");
	   $kompetisi = $this->Di->get_kondisi(array("id"=>$datasiswa->trkompetisi_id),"tr_kompetisi","nama");
	   $kota_madrasah = $this->Di->get_kondisi(array("id"=>$datasiswa->provinsi_madrasah),"provinsi","nama");
	   $wilayah = strtoupper($jenjangn[$datasiswa->jenjang_madrasah])."  PROVINSI ".$kota_madrasah;


		 if($datasiswa->peringkat_prov <= 6){
   
		   $status =   $ketkabko[$datasiswa->peringkat_prov];
		  
		  
	   


		   $pdf->addPage('L');
		   
		   $pdf->setSourceFile('juaraprovinsi.pdf');
	   
		   $tplIdx = $pdf->importPage(1);
		   $pdf ->useTemplate($tplIdx, null, null, null, null,FALSE);
		   
		   $pdf->Ln(5);
		   $pdf->SetFont('Arial','B',26);
		   $pdf->SetTextColor(6, 0, 0);

		   $pdf->Cell(null,93,$datasiswa->nama,0,0,'C');

		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,$datasiswa->madrasah,0,0,'C');

		   $pdf->Ln(20);
		   $pdf->SetFont('Arial','B',26);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(276,93,$status,0,0,'C');


		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,"Bidang Studi ".$kompetisi,0,0,'C');

		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,"JENJANG ".$wilayah,0,0,'C');
		   
		   
		   //$pdf->Write(0, '');


		   $pdf->Image("tmp/".$image_name,0,190,20,20);


		   $pdf->Output('I', 'PIAGAM  '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf');

		   }else{
			   $status = " Peserta ";




			   $pdf->addPage('L');
		   
			   $pdf->setSourceFile('pesertaprovinsi.pdf');
		   
			   $tplIdx = $pdf->importPage(1);
			   $pdf ->useTemplate($tplIdx, null, null, null, null,FALSE);
			   
			   $pdf->Ln(5);
			   $pdf->SetFont('Arial','B',26);
			   $pdf->SetTextColor(6, 0, 0);
   
			   $pdf->Cell(null,100,$datasiswa->nama,0,0,'C');
   
			   $pdf->Ln(15);
			   $pdf->SetFont('Arial','B',15);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(null,93,$datasiswa->madrasah,0,0,'C');

			   $pdf->Ln(20);
			   $pdf->SetFont('Arial','B',26);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(null,93,"PESERTA",0,0,'C');
   
			
   
   
			   $pdf->Ln(10);
			   $pdf->SetFont('Arial','B',15);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(null,93,"Bidang Studi ".$kompetisi,0,0,'C');
   
			   $pdf->Ln(10);
			   $pdf->SetFont('Arial','B',15);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(null,93,"JENJANG ".$wilayah,0,0,'C');
			   
			   
			   //$pdf->Write(0, '');
   
   
			   $pdf->Image("tmp/".$image_name,276,190,20,20);
   
   
			   $pdf->Output('I', 'PIAGAM  '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf');


	   
		   }


   }else{

	   echo "404 Not Found";
   }
 }


 public function pengumumannas(){
		
		
		
	if(!$this->session->userdata("tmmadrasah_id")){
	   echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
		  exit();
	 }
  
   
							   
	$ajax = $this->input->get_post("ajax",true);	
	 $data['title']  = "Pengumuman Kompetisi Sains Madrasah ";
	 $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
	if(!empty($ajax)){
				   
		$this->load->view('pengumuman_nas',$data);
	}else{
		
	   
		$data['konten'] = "pengumuman_nas";
		
		$this->load->view('dashboard/page_header',$data);
	}

   
}




public function sertifikatnasional(){

	$tmsiswa_id = base64_decode($this->input->get("q")); 
	$nik       = base64_decode($this->input->get("nik")); 

   $datasiswa = $this->db->query("select nasional_peringkat,nasional_juara,nik_siswa,madrasah,provinsi,trkompetisi_id,id,nama,jenjang_madrasah,provinsi_madrasah,tmmadrasah_id from v_siswa_nasional where nla IS NOT NULL AND id='".$tmsiswa_id."'")->row();
	   
   if(count($datasiswa) >0){

	   $pdf = new Fpdi();

	   $this->load->library('ciqrcode'); //pemanggilan library QR CODE

	   $config['cacheable']	= true; 
	   $config['cachedir']		= './tmp/chache'; 
	   $config['errorlog']		= './tmp/'; 
	   $config['imagedir']		= './tmp/'; 
	   $config['quality']		= true; 
	   $config['size']			= '1024'; 
	   $config['black']		= array(224,255,255);
	   $config['white']		= array(70,130,180); 
	   $this->ciqrcode->initialize($config);

	   //$image_name=$datasiswa->id.'.png'; 
	   $image_name= $datasiswa->id.'nasional.png'; 

	   $params['data'] = "https://ksm.kemenag.go.id/publik/validitasnasional?q=".base64_encode($datasiswa->tmmadrasah_id)."&s=".base64_encode($datasiswa->id).""; 
	   $params['level'] = 'H'; 
	   $params['size'] = 10;
	   $params['savename'] = FCPATH.$config['imagedir'].$image_name; 
	   $this->ciqrcode->generate($params);

	   $ketkabko = array("1"=>"Juara I","2"=>"Juara II","3"=>"Juara III","4"=>"Juara Harapan I","5"=>"Juara Harapan II","6"=>"Juara Harapan III");
	   $jenjangn = array("1"=>"Madrasah Ibtidaiyah/Sederajat ","2"=>"Madrasah Tsanawiyah/Sederajat","3"=>"Madrasah Aliyah/Sederajat");
	   $kompetisi = $this->Di->get_kondisi(array("id"=>$datasiswa->trkompetisi_id),"tr_kompetisi","nama");
	  
	   $wilayah = strtoupper($jenjangn[$datasiswa->jenjang_madrasah]);


		 if($datasiswa->nasional_peringkat <= 15){
   
		   $status =   "PERAIH MEDALI ".strtoupper($datasiswa->nasional_juara);
		  
		  
	   


		   $pdf->addPage('L');
		   
		   $pdf->setSourceFile('nasionalJuara.pdf');
	   
		
		   $tplIdx = $pdf->importPage(1);
		   $pdf ->useTemplate($tplIdx, null, null, null, null,FALSE);
		   
		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',26);
		   $pdf->SetTextColor(6, 0, 0);

		   $pdf->Cell(null,93,$datasiswa->nama,0,0,'C');

		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,$datasiswa->madrasah,0,0,'C');

		   $pdf->Ln(20);
		   $pdf->SetFont('Arial','B',26);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,$status,0,0,'C');


		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,"Bidang Studi ".($kompetisi),0,0,'C');

		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,"JENJANG ".$wilayah,0,0,'C');
		   
		   
		   //$pdf->Write(0, '');


		   $pdf->Image("tmp/".$image_name,15,179,30,30);


		   $pdf->Output('I', 'PIAGAM  '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf');

		   }else{
			   
		   


		   $pdf->addPage('L');
		   
		   $pdf->setSourceFile('nasionalPeserta.pdf');
	   
		  
		   $tplIdx = $pdf->importPage(1);
		   $pdf ->useTemplate($tplIdx, null, null, null, null,FALSE);
		   
		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',26);
		   $pdf->SetTextColor(6, 0, 0);

		   $pdf->Cell(null,93,$datasiswa->nama,0,0,'C');

		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,$datasiswa->madrasah,0,0,'C');

		   $pdf->Ln(20);
		   $pdf->SetFont('Arial','B',26);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(270,93,"PESERTA",0,0,'C');


		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,"Bidang Studi ".($kompetisi),0,0,'C');

		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,"JENJANG ".$wilayah,0,0,'C');
		   
		   
		   //$pdf->Write(0, '');


		   $pdf->Image("tmp/".$image_name,265,179,30,30);


		   $pdf->Output('I', 'PIAGAM  '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf');


	   
		   }


   }else{

	   echo "404 Not Found";
   }
 }
	

 public function getSiswaEMIS(){

	$nik = $this->input->get_post("nik");
	$data = $this->Di->getSiswaEMIS($nik);
	
	echo json_encode($data['results'],true);

 }


 public function cetak()
				{

					//echo "Maintenence, Mohon Tunggu"; exit();
					 
				 $this->load->helper('exportpdf_helper');  
				$ret = '';
				
				
				
				
		 
					$data['breadcumb']  ="Kartu Pendaftaran";
					
					$data['siswa']    = $this->db->select("*")->from("v_siswa")->where("nik_siswa",base64_decode($this->input->get('nik')))->get()->row();
					

			
					if($data['siswa']->qrcode ==""){
			
			   
						$this->load->library('ciqrcode'); //pemanggilan library QR CODE
		 
					 $config['cacheable']	= true; 
					 $config['cachedir']		= './__statics/upload/qrcode/chache'; 
					 $config['errorlog']		= './__statics/upload/qrcode/'; 
					 $config['imagedir']		= './__statics/upload/qrcode/'; 
					 $config['quality']		= true; 
					 $config['size']			= '1024'; 
					 $config['black']		= array(224,255,255);
					 $config['white']		= array(70,130,180); 
					 $this->ciqrcode->initialize($config);
		 
					 $image_name=$data['siswa']->id.'.png'; 
		 
					 $params['data']  = site_url("web/lacak?token=".($data['siswa']->id."94")."&v=".(base64_decode($this->input->get('nik'))).""); 
					 $params['level'] = 'H'; 
					 $params['size'] = 10;
					 $params['savename'] = FCPATH.$config['imagedir'].$image_name; 
					 $this->ciqrcode->generate($params);
					 
					 
						 $this->db->query("update tm_siswa set qrcode='".$image_name."' where nik_siswa='".base64_decode($this->input->get('nik'))."'");
					}


					$data['siswa']    = $this->db->select("*")->from("tm_siswa")->where("nik_siswa",base64_decode($this->input->get('nik')))->get()->row();
					


				$pdf_filename = 'BuktiPendaftaran'.str_replace(array("'",",","-"),"",$data['siswa']->nama).'.pdf';	 
				$data_header = array('title' => 'Kartu Pendaftaran',);
			
				
				$user_info = $this->load->view('bukti', $data, true);
			
				
				 $output = $user_info;
				
		
			 	generate_pdf($output, $pdf_filename);
				
				 
	 }

}
