<?php

class M_lokal extends CI_Model {
 
    public function __construct() {
        parent::__construct();
    }

    
	
	
	public function m_peserta($paging){

		$status = $this->input->get_post("status",true);

		$this->db->select("*");
        $this->db->from('tm_siswa');
        $this->db->where('tmmadrasah_id',$_SESSION['tmmadrasah_id']);
        $this->db->where('status',$status);
			
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					  $this->db->order_by("nama","asc");
					//$this->db->order_by("no_test","ASC");
					
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
	public function m_peserta_kabko($paging){

		$this->db->select("skrn,d_p,gender,tempat,foto,medali,id,no_test,nama,tgl_lahir,skrn,provinsi_madrasah,kota_madrasah,trkompetisi_id,ketkabko,bnr_pg,bnr_isi,jml_benar,slh,ksg,nla,jenis,jenjang,username,madrasah");
        $this->db->from('tm_siswa');
        $this->db->where('nla is not null');		
        $this->db->where('tmmadrasah_id',$_SESSION['tmmadrasah_id']);
     		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					  $this->db->order_by("nama","asc");
					//$this->db->order_by("no_test","ASC");
					
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
	public function m_peserta_provinsi($paging){

		$this->db->select("*");
        $this->db->from('v_siswa_provinsi');
        $this->db->where('tmmadrasah_id',$_SESSION['tmmadrasah_id']);
        $this->db->where('status2',1);
       
	
	//	$kolom    = array("0"=>"id","1"=>"nisn","2"=>"nama","3"=>"gender","4"=>"tempat","5"=>"hp","6"=>"kompetisi");
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					  $this->db->order_by("peringkat_prov","asc");
					//$this->db->order_by("no_test","ASC");
					
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
	public function m_hasilNasional($paging){
           
		$kolom     = $_POST['kolom'];
        $jenjang     = $_POST['jenjang'];
		$kota     = $_POST['kota'];
		$provinsi     = $_POST['provinsi'];
        $trkompetisi_id   = $_POST['trkompetisi_id'];
        $status   = $this->input->post("status",true);
		$keyword   = $_POST['keyword'];
		
		
		$this->db->select("*");
        $this->db->from('v_siswa_nasional');
		$this->db->where('tmmadrasah_id',$_SESSION['tmmadrasah_id']);
      
			
	   if(!empty($kota)){ $this->db->where("kota_madrasah",$kota); } 
	   if(!empty($provinsi)){ $this->db->where("provinsi_madrasah",$provinsi); } 
   
	   
	   if(!empty($keyword)){ $this->db->where("UPPER({$kolom}) LIKE '%".strtoupper($keyword)."%'"); }
	   if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 

	  

		
	   
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					
					 $this->db->order_by("nasional_nilai_akhir","DESC");
					 $this->db->order_by("jml_benar","DESC");
					 $this->db->order_by("slh","ASC");
					 $this->db->order_by("nilai_provinsi","DESC");
					 $this->db->order_by("nilai_kabko","DESC");
					 $this->db->order_by("tgl_lahir","DESC");
					 
		
			 }
      
		
		
		return $this->db->get();
		
	}


	public function m_peserta_nasional($paging){

		$this->db->select("*");
        $this->db->from('tm_siswa');
        $this->db->where('tmmadrasah_id',$_SESSION['tmmadrasah_id']);
        $this->db->where('status3',1);
	
	//	$kolom    = array("0"=>"id","1"=>"nisn","2"=>"nama","3"=>"gender","4"=>"tempat","5"=>"hp","6"=>"kompetisi");
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					  $this->db->order_by("nama","asc");
					//$this->db->order_by("no_test","ASC");
					
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
	
	public function i_peserta(){
		
		
		$madrasah   = $this->db->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']))->row();
		$f                     = $this->input->get_post("f");
		
		$this->db->set("tmmadrasah_id",$_SESSION['tmmadrasah_id']);
		$this->db->set("provinsi",$madrasah->provinsi);
		$this->db->set("kota",$madrasah->kota);
		$this->db->set("jenjang",$madrasah->jenjang);
		$this->db->set("tgl_lahir",($_POST['tgl_lahir']));
		$this->db->set('d_entry', date('Y-m-d H:i:s'));
	    $this->db->set('i_entry', $_SESSION['nama']);
		
		$this->db->insert("tm_siswa",$f);
		return $this->db->insert_id();
		
		
		       
		
	}
	
	
	public function u_peserta($id){
		$madrasah   = $this->db->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']))->row();
		$f                     = $this->input->get_post("f");
		$this->db->set("tgl_lahir",($_POST['tgl_lahir']));
		$this->db->set('d_update', date('Y-m-d H:i:s'));
	    $this->db->set('i_update', $_SESSION['nama']);
		$this->db->set("jenjang",$madrasah->jenjang);
		$this->db->set("provinsi",$madrasah->provinsi);
		$this->db->set("kota",$madrasah->kota);
	    $this->db->where('id', $id);
		
		$this->db->update("tm_siswa",$f);
		
		
		        if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
				   
				} else {
						
					$this->db->trans_commit();
					
				}
		
	}
	
	
}
