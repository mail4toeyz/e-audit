<style>
.customSlider {
  opacity: 0;
  display: none;
	+ label {
		position: relative;
		top: 35px;
		left: 15px;
		cursor: pointer;
		&:before, &:after {
			background: #FFFFFF;
			position: absolute;
			content: '';
		}
		&:before {
			top: -90px; left: -60px;
			width: 80px;
			height: 40px;
			background: #ccc;
			border-radius: 20px;
			transition: background .75s;
		}
		&:after {
			height: 34px; width: 34px;
			top: -87px; left: -55px;
			border-radius: 50%;
			transition: all .5s;
		}
	}
	
	&:checked + label:after {
		height: 34px; width: 34px;
		top: -87px; left: -19px;
		border-radius: 50%;
	}
	
	&:checked ~ .wrapper {
		color: #fff;
		transition: background .6s ease;
		background: #31394D;
		
		.top-icons {
			i {
				color: #fff;
			}
		}
		
		.profile {
			// &:after {background: #ececece;}
			.name {
				color: #FDFEFF;
			}
			
			.title {
				color: #7C8097;
			} 
			
			.description {
				color: #FDFEFF;
			}
		}
		.icon {
			h4 {
				color: #FFFFFF;
			}
			
			p {
				color: #666B7D;
			}
		}
	}
}

.wrapper {
  width: 320px;
  height: 540px;
  background: #FDFEFF;
	transition: background .6s ease;
	border-radius: 10px;
	padding: 20px 20px 20px 20px;
	box-shadow: 0 8px 40px rgba(#000000, .2);
	
	.top-icons {
		i {
			color: #080911;
			&:nth-of-type(1) {
				float: left;
			}
			&:nth-of-type(2) {
				float: right;
			}
			&:nth-of-type(3) {
				float: right;
				padding-right: .8em;
			}
		}
	}
	.profile {
		margin-top: 2.2em;
		position: relative;
		
		&:after {
			width: 100%;
			height: 1px;
			content: ' ';
			display: block;
			margin-top: 1.3em;
			background: #E9EFF6;
		}
		
		.check {
			position: absolute;
			right: 5em;
			bottom: 12.7em;
			i {
				color: #fff;
				width: 20px;
				height: 20px;
				font-size: 12px;
				line-height: 20px;
				text-align: center;
				border-radius: 100%;
				background: $purple-gradient;	
			}
		}
		.thumbnail {
			width: 124px;
			height: 124px;
			display: flex;
			margin-left: auto;
			margin-right: auto;
			margin-bottom: 1.5em;
			border-radius: 100%;
			box-shadow: $shadow;
		}
		.name {
			color: #2D354A;
			font-size: 24px;
			font-weight: 600;
			text-align: center;
		}
		
		.title {
			color: #7C8097;
			font-size: .75em;
			font-weight: 300;
			text-align: center;
			padding-top: .5em;
			padding-bottom: .7em;
			letter-spacing: 1.5px;
			text-transform: uppercase;
		}
		
		.description {
			color: #080911;
			font-size: 14px;
			font-weight: 300;
			text-align: center;
			margin-bottom: 1.3em;
		}
		
		.btn {
			color: #fff;
			width: 130px;
			height: 42px;
			outline: none;
			border: none;
			display: block;
			cursor: pointer;
			font-weight: 300;
			margin-left: auto;
			margin-right: auto;
			border-radius: 70px;
			box-shadow: 0 13px 26px rgba(#000, .16), 0 3px 6px rgba(#000, .16);
			background: $purple-gradient;
		}
	}
	
	.social-icons {
		display: flex;
		margin-top: 1.2em;
		justify-content: space-between;
		
		.icon {
			display: flex;
			align-items: center;
			flex-direction: column;
			a {
				color: #fff;
				width: 60px;
				height: 60px;
				font-size: 28px;
				line-height: 60px;
				text-align: center;
				border-radius: 30px;
				box-shadow: $shadow;
			}
			&:nth-of-type(1) {
				a {
					background: $purple-gradient;
				}
			}
			&:nth-of-type(2) {
				a {
					background: $blue1-gradient;
				}
			}
			&:nth-of-type(3) {
				a {
					background: $blue2-gradient;
				}
			}
			
			h4 {
				color: #080911;
				font-size: 1em;
				margin-top: 1.3em;
				margin-bottom: .2em;
			}
			
			p {
				color: #666B7D;
				font-size: 12px;		
			}
		}
	}
}

.concept {
	position: absolute;
	bottom: 25px;
	color: #AAB0C4;
	font-size: .9em;
	font-weight: 400;
	a {
		color: rgba(172,25,102,1);
		text-decoration: none;
	}
}
</style>
<div class="row">


<?php 


  


   $siswa = $this->db->query("select id,nik_siswa,no_test,nama,d_p,trkompetisi_id,nla,foto,kota_madrasah from v_siswa_kabkota where status=2 and tmmadrasah_id='{$_SESSION['tmmadrasah_id']}' and nla is not null and bnr_isi is not null and jml_benar is not null and ksg is not null order by d_p ASC")->result();
    foreach($siswa as $data){
		$sertifikat = "<a href='".site_url('lokal/sertifikatkota?q='.base64_encode($data->id).'&nik='.base64_encode($data->nik_siswa).'')."' target='_blank' class='btn btn-danger' ><i class='fa fa-file'></i> Unduh Sertifikat</a>";

        $jml_peserta = $this->db->query("select count(id) as jml from v_siswa_kabkota where trkompetisi_id='{$data->trkompetisi_id}' and kota_madrasah='{$data->kota_madrasah}' and nla IS NOT NULL ")->row();
?>

<div class="col-md-4">


<div class="wrapper">
	
	
	<div class="profile" style="padding-bottom:50px">
		<center><img src="https://ksm.kemenag.go.id/__statics/upload/<?php echo $data->foto; ?>" width="100px" class=" img-responsive thumbnail" style="border-radius:50%"> 
	 <br>
	 <br>
	 <small> <?php echo ucwords($data->no_test); ?> </small> <br>
    <h5 style="color:#FF5722"><?php echo ($data->nama); ?>   </h5>
    <?php 
    $ketkabko = array("1"=>"Juara I","2"=>"Juara II","3"=>"Juara III","4"=>"Juara Harapan I","5"=>"Juara Harapan II","6"=>"Juara Harapan III");
      if($data->d_p <=6){

       ?><h3 style="color:#83b851"> <?php  echo $ketkabko[$data->d_p]; ?> </h3><?php 
      }else{
        ?><h3 style="color:#000"> Peserta  </h3><?php 

      }
    ?>   
    <h5 style="color:#FF5722">Skor <?php echo ($data->nla); ?>   </h5>
    <h5 style="color:#FF5722">Peringkat <?php echo ($data->d_p); ?>   </h5>
    <h5 style="color:#000">Dari  <?php echo ($jml_peserta->jml); ?> Peserta   </h5>
       
    <h6 style="color:#000"><?php echo ($this->Di->get_kondisi(array("id"=>$data->trkompetisi_id),"tr_kompetisi","nama")); ?>    </h6>
    <h6 style="color:#000"><?php echo ($this->Di->get_kondisi(array("id"=>$data->kota_madrasah),"kota","nama")); ?>    </h6>
		<?php echo $sertifikat; ?>

        </center>
	</div>
	
</div>




    </div>

<?php 
    }
?>

</div>



<div id="pengumumanModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
     
      <div class="modal-body">
          <?php 
		   $siswa = $this->db->query("select count(id) as jml from v_siswa_kabkota where status=2 and tmmadrasah_id='{$_SESSION['tmmadrasah_id']}' and d_p In(1,2,3)")->row();
		  ?>
		  <center>
			  <br>
			  <br>
			  <h3><b> SELAMAT !!!</b></h3>
			  <h4> <b><?php echo $siswa->jml; ?></b> Siswa dari <b><?php echo $m->nama; ?> </b> menjadi juara dan berhak mengikuti KSM Tingkat Provinsi </h4>
			  <small> Peserta KSM Provinsi diikuti oleh 3 siswa terbaik (Juara I,II,III) tiap bidang studi <br>  Kartu Peserta KSM Tingkat Provinsi dapat diunduh pada Senin, 5 September 2022 </small>
		  </center>
		     
   
          
     
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>

  </div>
</div>
<?php 
if($siswa->jml >0){
	?>
<script type="text/javascript">
     $("#pengumumanModal").modal("toggle");

</script>

<?php 
} ?>