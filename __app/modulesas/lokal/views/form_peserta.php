<link href="<?php echo base_url(); ?>__statics/js/wizard/css/smart_wizard_all.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>__statics/js/wizard/js/jquery.smartWizard.min.js"></script>           
		   
		   <div class="row">
                <div class="col-md-12">                      
					  <div class="card form" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-edit"></i>  PENDAFTARAN PESERTA KSM </h3>
                        </div>

						<div id="smartwizard">

                            <ul class="nav">
                                <li class="nav-item">
                                <a class="nav-link" href="#step-1">
                                    <strong>Tahap  1</strong> <br> Mengisi Data Peserta
                                </a>
                                </li>
								
                                <li class="nav-item">
                                <a class="nav-link" href="#step-2">
                                    <strong>Tahap  2</strong> <br> Unggah Persyaratan
                                </a>
                                </li>
                                <li class="nav-item">
                                <a class="nav-link" href="#step-3">
                                    <strong>Tahap  3</strong> <br> Pendaftaran
                                </a>
                                </li>
                                
                            </ul>
                            <form method="post" action="javascript:void(0)" id="simpanidentitas" url="<?php echo site_url("lokal/kirim"); ?>">
                            <div class="tab-content">
                            
                                <div id="step-1" class="tab-pane" role="tabpanel" aria-labelledby="step-1">
                                  <?php $this->load->view("form"); ?>
                                </div>
                                <div id="step-2" class="tab-pane" role="tabpanel" aria-labelledby="step-2">
                                   <div class="col-md-12"><div class="alert alert-danger">   </div></div>
                                </div>
                                <div id="step-3" class="tab-pane" role="tabpanel" aria-labelledby="step-3">
                                
                                </div>
                                
                            </div>

                            </form>

                            </div>

                            

                        <br>

                    </div>
                    </div>
                    </div>

<script type="text/javascript">
        $(document).ready(function(){

            $("#smartwizard").on("stepContent", function(e, anchorObject, stepIndex, stepDirection) {


                if(stepIndex==1){
                        var form      = $("form").serialize();                       
                        var url ="<?php echo site_url("lokal/step1"); ?>";
                        var tab_id = "step-2";
                        $('#smartwizard').smartWizard("loader", "show");
                           
							
							$.ajax({
                                         type: "POST",
                                         url: url,
                                         data: $("form").serialize(),
                                         success: function (response, status, xhr) {
                                             var ct = xhr.getResponseHeader("content-type") || "";
                                             if (ct == "application/json") {
                                             
												
												 $("#"+tab_id).html(' <div class="col-md-12"><div class="alert alert-warning">'+response.message+'</div><div>');
												 $('#smartwizard').smartWizard("loader", "hide");
												
                                                 
                                                 
                                             } else {
                                               
                                                
                                                $("#"+tab_id).html(response);
                                                $('#smartwizard').smartWizard("loader", "hide");
                                                
                                                 
                                                 
                                                  
                                                 
                                               
                                             }
                                            
                                             
                                         }
                                     });



                }


                    
                    if(stepIndex==2){
                        var id = $("#id").val();
                        var url ="<?php echo site_url("lokal/step3"); ?>";
                        var tab_id = "step-3";
                        $('#smartwizard').smartWizard("loader", "show");
                            $.post(url,{id:id},function(data){

                                $("#"+tab_id).html(data);
                                $('#smartwizard').smartWizard("loader", "hide");
                            });
                    }
                    

            });
                    

            $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
                $("#prev-btn").removeClass('disabled');
                $("#next-btn").removeClass('disabled');
                if(stepPosition === 'first') {
                    $("#prev-btn").addClass('disabled');
                } else if(stepPosition === 'last') {
                    $("#next-btn").addClass('disabled');
                } else {
                    $("#prev-btn").removeClass('disabled');
                    $("#next-btn").removeClass('disabled');
                }
            });

            // Smart Wizard
            $('#smartwizard').smartWizard({
                selected: 0,
                theme: 'arrows', // default, arrows, dots, progress
                // darkMode: true,
                transition: {
                    animation: 'slide-horizontal', // Effect on navigation, none/fade/slide-horizontal/slide-vertical/slide-swing
                },
                toolbarSettings: {
                    toolbarPosition: 'top'
                },
                lang: {  // Language variables
                next: 'Tahapan Selanjutnya',
                previous: 'Tahapan Sebelumnya'
            }
            });

            // External Button Events
            $("#reset-btn").on("click", function() {
                // Reset wizard
                $('#smartwizard').smartWizard("reset");
                return true;
            });

            $("#prev-btn").on("click", function() {
                // Navigate previous
                $('#smartwizard').smartWizard("prev");
                return true;
            });

            $("#next-btn").on("click", function() {
                // Navigate next
                $('#smartwizard').smartWizard("next");
                return true;
            });


            // Demo Button Events
            $("#got_to_step").on("change", function() {
                // Go to step
                var step_index = $(this).val() - 1;
                $('#smartwizard').smartWizard("goToStep", step_index);
                return true;
            });


            $("#dark_mode").on("click", function() {
                // Change dark mode
                var options = {
                  darkMode: $(this).prop("checked")
                };

                $('#smartwizard').smartWizard("setOptions", options);
                return true;
            });

            $("#is_justified").on("click", function() {
                // Change Justify
                var options = {
                  justified: $(this).prop("checked")
                };

                $('#smartwizard').smartWizard("setOptions", options);
                return true;
            });

            $("#animation").on("change", function() {
                // Change theme
                var options = {
                  transition: {
                      animation: $(this).val()
                  },
                };
                $('#smartwizard').smartWizard("setOptions", options);
                return true;
            });

            $("#theme_selector").on("change", function() {
                // Change theme
                var options = {
                  theme: $(this).val()
                };
                $('#smartwizard').smartWizard("setOptions", options);
                return true;
            });

		});
		

		var base_url="<?php echo base_url(); ?>";
       
    
    
	   function sukses(param){
			  let timerInterval;
			  Swal.fire({
				type: 'success',
				title: 'Berhasil ',
				showConfirmButton: false,
				 html: 'Pendaftaran Anda berhasil dikirim, Dalam <strong></strong> detik<br>Anda akan dialihkan kehalaman <b> Data Peserta </b>',
	   
				timer: 3000,
				onBeforeOpen: () => {
				  Swal.showLoading();
				  timerInterval = setInterval(() => {
					Swal.getContent().querySelector('strong')
					  .textContent = Swal.getTimerLeft()
				  }, 100)
				},
				onClose: () => {
				  clearInterval(timerInterval)
				}
			  }).then((result) => {
				if (
				  
				  result.dismiss === Swal.DismissReason.timer
				  
				) {
			   
				 location.href = param;
				}
			  })
	   }
	   
	   function gagal(param){
			  let timerInterval;
			  Swal.fire({
				type: 'warning',
				title: 'Gagal, Perhatikan ! ',
				showConfirmButton: true,
				 html: param
	   
			  })
	   }
	   
	   
		  $(document).off('submit', 'form#simpanidentitas').on('submit', 'form#simpanidentitas', function (event, messages) {
		   event.preventDefault();
			 var form   = $(this);
			 var urlnya = $(this).attr("url");
		  
			  //alert("sd"); return false;
					
					loading();
					 $.ajax({
						  type: "POST",
						  url: urlnya,
						  data: form.serialize(),
						  success: function (response, status, xhr) {
							  var ct = xhr.getResponseHeader("content-type") || "";
							  if (ct == "application/json") {
							  
								  gagal(response.message);
					  
							   
								  
								  
							  } else {
								
								 
							   sukses("<?php echo site_url("lokal/peserta/1"); ?>","Pendaftaran KSM berhasil terkirim");
								 
								  
								  
								   
								  
								
							  }
							  
							  jQuery.unblockUI({ });
							  
						  }
					  });
					
			  return false;
		  });
		  
    </script>