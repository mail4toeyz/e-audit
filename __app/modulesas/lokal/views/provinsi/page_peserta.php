   <div id="showform"></div>        
                                 
					  <div class="card form" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-users"></i> Peserta KSM Provinsi</h3>
                        </div>
                        <br>
                         
                            <div class="col-md-12">
							<div class="alert alert-info" style="font-size:12px">
							 Petunjuk ! <br>
							  Peserta KSM Provinsi adalah Juara I,II,III Tingkat Kabupaten/Kota,  <br>
							  Kartu Peserta KSM Tingkat Provinsi dapat diunduh pada Rabu, 7 September 2022
							  
							</div>
								 
									<br>
			 
			 
			              </div>
			
		           <hr>
		<div class="col-md-12">   				
		   <div class="table-responsive">
		   <table id="datatableTable" class="table  table-bordered table-hover table-striped" >
			  <thead>
				<tr>
				  
				   <!--<th class="center" width="10%">AKSI</th>	-->	  
				   <th class="center" rowspan="2">NO  </th>			  
				   <th class="center" rowspan="2"> AKSI </th>			  
				   <th class="center" rowspan="2"> FOTO </th>			  
				   <th class="center" rowspan="2"> NIK </th>			  
				  			  
				   <th class="center" rowspan="2">NAMA  </th>		  
				  		  
				   <th class="center" rowspan="2">JK</th>				  
				  			  
				   				  
				   <th class="center" rowspan="2">KOMPETISI</th>				  
				   <th class="center" rowspan="2">PERINGKAT</th>				  
				 			  
				   <th class="center" colspan="6">RINCIAN SKOR KSM PROVINSI </th>
				   <th class="center" rowspan="2">STATUS KABKOTA</th>				  
							  
							  
								  
				  				  
				
				  
				  
				</tr>
				  <tr>
				     <th> BENAR PG</th>
				     <th> BENAR ESSAY</th>
				     <th> JUMLAH BENAR </th>
				     <th> SALAH </th>
				     <th> KOSONG </th>
				     <th> NILAI </th>
				  </tr>

			  </thead>
			  <tbody></tbody>
            </table>
			</div>
        </div>
        </div>
		
<div id="myModalRincian" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
     
      <div class="modal-body" id="loadRincianJawaban">
        <p>loading ...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<b style="color:white;background:#33AFFF">Data sedang di tampilkan..</b>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data tidak atau belum tersedia",
						   "sLengthMenu": "Default menampilkan   _MENU_ Data <small>(Klik pada angkanya untuk memuat lebih banyak data)</small>"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[5,10,25, 50,100,200,300,500,1000, 800000000], [5,10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							
							text:'<span class="fa fa-file-excel-o"></span> Cetak Excel',
							className :"btn btn-danger btn-sm"
							},
							
							
						
						
							{
							extend: 'colvis',
							
							text:'<span class="fa fa-columns "></span> Setting Kolom ',
							className :"btn btn-danger "
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("lokal/g_provinsi"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
	
		
		$(document).on('input click change', '#keyword,#searchcustom', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
		
		$(document).on(' click ', '#cancelr', function (event, messages) {			
			   $("#showform").html("");
			   dataTable.ajax.reload(null,false);	        
		  
        });

		$(document).on(' click ', '.rincian', function (event, messages) {			

			  var id = $(this).attr("datanya");
			  $.post("<?php echo site_url('lokal/rincianJawaban'); ?>",{id:id},function(data){

				$("#loadRincianJawaban").html(data);
			  })        
		  
        });
		
		

</script>
			
                    