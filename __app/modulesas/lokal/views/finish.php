

    <div class="row">
      <div class="col-md-6">
          <table class="table table-hover table-bordered table-striped">
            <tr>
                <td colspan="3"> <b> Data Peserta </b>   </td>
                
            </tr>

            <tr>
                <td colspan="2"> NIK Peserta    </td>
                <td> <?php echo $dataform->nik_siswa; ?>    </td>
                
            </tr>

            <tr>
                <td colspan="2"> Nama Peserta    </td>
                <td> <?php echo $dataform->nama; ?>    </td>
                
            </tr>

            <tr>
                <td colspan="2"> Kompetisi    </td>
                <td> <?php echo $this->Di->get_kondisi(array("id"=>$dataform->trkompetisi_id),"tr_kompetisi","nama"); ?>    </td>
                
            </tr>

           

            
        

      </table>
    </div>

    <div class="col-md-6">
     <div class="table-responsive">
      <table class="table table-hover table-bordered table-striped">
             <tr>
                <td colspan="3"> <b> Berkas Persyaratan  </b>   </td>
                
             </tr>
             <thead>
                <tr>
                  <th> Persyaratan </th>
                  <th> Status Kelengkapan</th>
                </tr>
             </thead>

             <tbody>
               <?php 
                $persyaratan = $this->db->get_where("persyaratan",array("status"=>1))->result();
                 foreach($persyaratan as $row){
                  
                  $eksis = $this->db->query("select id,file from tr_persyaratan where peserta_id='".$dataform->id."' and persyaratan_id='".$row->id."'  ")->row();
                   ?>
                    <tr>
                      <td> <?php echo $row->nama; ?> 
                      
                      
                       </td>
                      <td>
                        <?php 
                          if(!is_null($eksis)){
                              ?>
                              Lengkap
                             <?php 

                          }else{
                            ?>
                             Belum 
                            <?php 

                          }
                        ?>
                           
                         
                      </td>
                    </tr>
                <?php 


                 }
                ?>

             </tbody>
      </table>
 <br>
      <?php 
  $status = $this->db->query("select count(id) as jml from persyaratan where status=1 and id NOT IN(select persyaratan_id from tr_persyaratan where peserta_id='".$dataform->id."'   )")->row();
   if($status->jml >0 ){
     ?>

     <div class="alert alert-warning"> Ada <?php echo $status->jml; ?> Berkas persyaratan(wajib) yang belum Anda lengkapi, Peserta dapat dikirim setelah Anda melengkapi seluruh berkas persyaratan </div>
  


    <?php 
   }else{
?>
    

    

    <div class="alert alert-primary"> 

                              <button type="submit" class="btn btn-primary btn-sm  ">
                               
                                <span>Kirim  Pendaftaran  </span>
                              </button>

    </div>


    <?php 
   }


   ?>
      </div>

    </div>
  
  
  </div>


