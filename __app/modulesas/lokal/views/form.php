<input type="hidden" class="form-control" name="id" value="<?php echo isset($dataform) ?  $dataform->id :""; ?>">
<div class="col-md-12">
             <div class="row">
							   <div class="col-md-12">
									<div class="form-group">

									<label for="name">Nomor Induk Kependudukan </label>
										<div class="input-group">
											         
													 <input type="text" class="form-control"  id="nik" placeholder="Masukkan Nomor Induk Kependudukan Siswa" data-toggle="tooltip" title="Isi jawaban Anda kemudian klik tombol validasi " name="f[nik_siswa]" maxlength="16" value="<?php echo isset($dataform) ?  $dataform->nik_siswa :""; ?>">
														 <div class="input-group-append">
															 <button class="btn btn-danger" style="" type="button" id="validasi"> Tarik Data Dari EMIS </button>
														 </div>
										 </div>											
										
										</div>
									</div>
								</div>
				<div class="row">
									<div class="col-md-4">
									<div class="form-group">
											<label for="name">Nama Peserta </label>
											
											<input type="text" class="form-control" id="nama" name="f[nama]" value="<?php echo isset($dataform) ?  $dataform->nama :""; ?>">
										</div>
									</div>
									<div class="col-md-4">
									<div class="form-group">
											<label for="name">NISN </label>
											
											<input type="text" class="form-control" id="nisn" name="f[nisn]" maxlength="10" value="<?php echo isset($dataform) ?  $dataform->nisn :""; ?>">
										</div>
									</div>
									
						</div>
						
						 <div class="row">
									<div class="col-md-4">
									    <div class="form-group">
											<label for="name">Tempat Lahir (*)</label>
											<input type="text" class="form-control" id="tempat" name="f[tempat]" value="<?php echo isset($dataform) ?  $dataform->tempat :""; ?>">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label for="name">Tanggal Lahir (*)  </label>
											 <input type="text" class="form-control" name="tgl_lahir" id="tgl_lahir" autocomplete="no" id="tgl_lahir" readonly placeholder="Format : dd-mm-yyyy" value="<?php echo isset($dataform) ?  ($dataform->tgl_lahir) :""; ?>">
														    <script>
																$(document).ready(function () {
																	startdate = new Date() - 18;
																	$('#tgl_lahir').datepicker({
																	 changeMonth: true,
																								changeYear: true,
																								autoclose: true,
																								dateFormat: 'yy-mm-dd',
																								yearRange: "1990:2020",
																							
																	});
																});
															</script>
										</div>
									</div>
									<div class="col-md-4">
									  <div class="form-group">
											<label for="name">Jenis Kelamin (*)</label> <br>
											<?php 
															      $gender = $this->Di->gender();
																    foreach($gender as $index=>$row){
																		
																		?><label for="<?php echo $index; ?>"> <input type="radio" name="f[gender]" id="gender" value="<?php echo $row; ?>" id="<?php echo $index; ?>" <?php if(isset($dataform)){ echo  ($dataform->gender==$row) ? "checked":"";  } ?>> <?php echo $row; ?> </label> &nbsp; <?php
																	}
															   ?>
															   
										</div>
										
									</div>
									
						</div>
						
						<div class="row">
									<div class="col-md-4">
									   <div class="form-group">
											<label for="name">Kelas  </label>
											<input type="text" class="form-control" name="f[kelas]" id="kelas" value="<?php echo isset($dataform) ?  $dataform->kelas :""; ?>" placeholder="Contoh  : VII (Tujuh) ">
										</div>
									</div>
									<div class="col-md-8">
										 <div class="form-group" >
											<label for="name">Kompetisi yang akan diikuti ?  (*) </label>
											
											<select class="form-control" name="f[trkompetisi_id]">
											 <option value="">- Pilih Kompetisi </option>
											
											    <?php 
												   foreach($this->Di->get_wherearray("tr_kompetisi",array("tmmadrasah_id"=>$m->jenjang)) as $r){
													   
													  ?> <option value="<?php echo $r->id; ?>" <?php if(isset($dataform)){ echo  ($dataform->trkompetisi_id==$r->id) ? "selected":"";  } ?>><?php echo $r->nama; ?> </option><?php  
												   }
												   
												  ?>
											
											</select>
											
										</div>
									</div>
									
						</div>
						
						<div class="row">
									<div class="col-md-6">
									   <div class="form-group">
											<label for="name">Prestasi Siswa   </label>
											<textarea type="text" class="form-control" name="f[prestasi]" placeholder="Contoh : Juara KSM Satuan Pendidikan Bidang Studi Matematika"><?php echo isset($dataform) ?  $dataform->prestasi :""; ?></textarea>
										</div>
									</div>
									<div class="col-md-6">
										 <div class="form-group">
											<label for="name">Alamat Lengkap Siswa </label>
											<textarea type="text" class="form-control" name="f[alamat]" id="alamat" value=""><?php echo isset($dataform) ?  $dataform->alamat :""; ?></textarea>
										</div>
									</div>
									
						</div>

						<div class="row">
							   <div class="col-md-6">
									<div class="form-group">
											<label for="name">No HP Peserta (Bila ada) </label>
										
											<input type="text" class="form-control" name="f[hp]" id="hp" maxlength="16" value="<?php echo isset($dataform) ?  $dataform->hp :""; ?>" placeholder="No HP Siswa (Bila ada)">
										</div>
									</div>
									
									<div class="col-md-6">
									<div class="form-group">
											<label for="name">Email Peserta (Bila ada) </label>
											
											<input type="text" class="form-control" name="f[email]"  value="<?php echo isset($dataform) ?  $dataform->email :""; ?>" placeholder="Email  Siswa (Bila ada)">
										</div>
									</div>
									
						</div>


                        <h5>Data Orang tua  </h5>  <hr> 
                        <div class="row">
							   <div class="col-md-6">
									<div class="form-group">
											<label for="name">Nomor Induk Kependudukan Ayah </label>
											<input type="text" class="form-control" name="f[nik_ayah]" id="nik_ayah" maxlength="16" value="<?php echo isset($dataform) ?  $dataform->nik_ayah :""; ?>" placeholder="NIK Ayah dapat dilihat dalam KK">
										</div>
									</div>
									<div class="col-md-6">
									<div class="form-group">
											<label for="name">Nama Ayah </label>
											
											<input type="text" class="form-control" name="f[nama_ayah]" id="nama_ayah" value="<?php echo isset($dataform) ?  $dataform->nama_ayah :""; ?>">
										</div>
									</div>
									
									
						</div>
						
						  <div class="row">
							   <div class="col-md-6">
									<div class="form-group">
											<label for="name">Nomor Induk Kependudukan Ibu </label>
											<input type="text" class="form-control" name="f[nik_ibu]" id="nik_ibu" maxlength="16" value="<?php echo isset($dataform) ?  $dataform->nik_ibu :""; ?>" placeholder="NIK Ibu dapat dilihat dalam KK">
										</div>
									</div>
									<div class="col-md-6">
									<div class="form-group">
											<label for="name">Nama Ibu </label>
											
											<input type="text" class="form-control" name="f[nama_ibu]" id="nama_ibu" value="<?php echo isset($dataform) ?  $dataform->nama_ibu :""; ?>">
										</div>
									</div>
									



									
						</div>
</div>


<script type="text/javascript">
 $(document).off("click","#validasi").on("click","#validasi",function(){
			var nik = $("#nik").val();
			
			if(nik==""){
				gagal("Silahkan isi NIK siswa terlebih dahulu !");
				return false;
			}
			 
			loading();

			 $.post("<?php echo site_url('lokal/getSiswaEMIS'); ?>",{nik:nik},function(data){

				
				
			//alert(JSON.stringify(obj[0].parents.father_full_name));
		
			if(data !="null") {
				var obj = JSON.parse(data);
				$("#nama").val(obj[0].full_name);
				$("#nisn").val(obj[0].nisn);
				$("#tempat").val(obj[0].birth_place);
				$("#tgl_lahir").val(obj[0].birth_date);
				$("input[id=gender][value='"+obj[0].gender+"']").prop("checked",true);
				$("#kelas").val(obj[0].level);
				$("#alamat").val(obj[0].full_address);
				$("#hp").val(obj[0].handphone);
				$("#nik_ayah").val(obj[0].parents.father_nik);
				$("#nik_ibu").val(obj[0].parents.mother_nik);
				$("#nama_ayah").val(obj[0].parents.father_full_name);
				$("#nama_ibu").val(obj[0].parents.mother_full_name);
				
			}else{
				alertify.alert("NIK yang Anda masukkan tidak terdaftar didalam EMIS, silahkan isi data peserta  manual");
			}
			jQuery.unblockUI({ });


			 });
	});
</script>