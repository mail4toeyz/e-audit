
 <div class="card form" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-file-text-o"></i>  Pengumuman Peserta KSM Tingkat Kabupaten/Kota </h3>
                        </div>

<div class="alert alert-primary">
									<h3>Petunjuk  </h3>
									  <ol>
										  <li> Silahkan Unduh Kartu Peserta KSM Tingkat Kabupaten/Kota</li>
										  <li> Lokasi dan Jadwal KSM  Tingkat Kabupaten/Kota ada pada kartu Peserta masing-masing</li>
										  <li> Pelaksanaan  KSM Tingkat Kabupaten/Kota wajib dilokasi yang sudah ditentukan Komite Kabupaten/Kota </li>
										  <li> Bagi lembaga yang ingin melaksanakan Ujicoba di Satuan Pendidikan masing-masing, silahkan berkoordinasi dengan Komite Kabupaten/Kota untuk mendapatkan akses ke Aplikasi Ruang Ujian  </li>
										  <li> Informasi lebih lanjut, Anda dapat menghubungi Komite Kabupaten/Kota (Kantor Kementerian Agama Kabupaten/Kota) <?php echo $this->Di->get_kondisi(array("id"=>$m->kota),"kota","nama"); ?> </li>
									  </ol>
										 
								
									 
									  <center>
										 
										<a href="https://youtu.be/j59pP4Ep7Kg" class="btn btn-danger" target="_blank"><i class="fa fa-video-camera"></i> Panduan CBT KSM  (Ruang Ujian KSM) </a> 
									</center>
	</div>
							</br>
						
<style>
    .nukahiji{}
    .nukahiji header{font-size:13pt;text-align: center;font-weight: bold;margin-bottom: 0.1in;padding-bottom: 5px;border-bottom: 3px #000 solid;}
    .nukahiji header img{float: left;height: 60px;}
    .nukahiji .profil{margin-bottom: 20px;}
      .nukahiji .profil table{width: 100%}
      .nukahiji .profil table tr td{padding:5px 0px;color:#000;}
      .nukahiji .profil table tr td strong{font-size: 14pt;}
    .nukahiji .req_table{margin-bottom: 0px;}
    .nukahiji .req_table table{width: 100%;}
    .nukahiji .req_table table tr td{padding:5px;}
      .nukahiji .req_table table thead tr td{font-weight: bold;text-align: center;}
    .nukahiji .catatan{font:11pt arial;font-style: italic;clear: both;}
    .nukahiji .catatan .textbox{display: block;border:1px #000 solid;margin:20px 0px;padding:10px;font-weight: bolder;}
	#table_detail
	{
		border-bottom:1px #000 solid;
		border-right:1px #000 solid;
	}
	
	#table_detail tr td
	{
		border-top:1px #000 solid;
		border-left:1px #000 solid;
	}
	.photo
	{
		width:116px!important;
		height:140px!important;
		padding:5px;
		border:1px #CCCCCC solid;
		background-color:#ffffff;
	}	
  </style>

 <?php 
   $siswa = $this->db->query("select * from v_siswa where status=2 and tmmadrasah_id='{$_SESSION['tmmadrasah_id']}' and no_test !=''")->result();
    foreach($siswa as $data){
?>
	 <div class="nukahiji" id="printableArea" style="width:650px; margin:0px auto;padding: 0.1in;font: 13pt arial; border:1px #000 solid;">
		  <header>
			  <?php 
               $documentroot = base_url();
			   $zona	     = $this->Di->get_kondisi(array("id"=>$data->provinsi),"provinsi","zona");
			   $simulasi    = array("1"=>"Rabu, 5 Juli 2023","2"=>"Kamis, 6 Juli 2023","3"=>"Jumat, 7 Juli 2023");
			   $pelaksanaan = array("1"=>"Sabtu, 8 Juli 2023","2"=>"Minggu, 9 Juli 2023","3"=>"Senin, 10 Juli 2023");
	   
               $zonaujicoba         = strtolower($zona."_ujicoba");
               $zona         = strtolower($zona);
			   $sesiData     = $this->db->get_where("tm_sesi",array("id"=>$data->sesi_kabko))->row();
			   $lokasites    ="Belum ditentukan";
			   $alamatlokasi ="Komite Kabupaten/Kota belum menentukan lokasi KSM, silahkan hubungi Kankemenag ".$this->Di->get_kondisi(array("id"=>$data->kota_madrasah),"kota","nama");
			   if($data->lokasi_kabko !=0){
					$lokasites    = $this->Di->get_kondisi(array("id"=>$data->lokasi_kabko),"tr_lokasites","nama");
					$alamatlokasi = $this->Di->get_kondisi(array("id"=>$data->lokasi_kabko),"tr_lokasites","alamat");
			   }
			  ?>
			 
			 
			  
			  
			 <table width="100%">
			  
			  <tr>
				<td width="10%" align="left"> <img src="<?php echo $documentroot.'/__statics/img/kemenag.png';?>"></td>
				  <td align="center" width="80%" style="font-weight:bold">
					 <span style="font-size:18px;color:black"> KEMENTERIAN AGAMA REPUBLIK INDONESIA    </span><br/>
					 <span style="font-size:14px;color:black"> KANTOR  KEMENTERIAN AGAMA  <?php echo $this->Di->get_kondisi(array("id"=>$data->kota_madrasah),"kota","nama"); ?></span><br>
					 <span style="font-size:14px;color:black"> KOMPETISI SAINS MADRASAH <br> TAHUN 2023 </span>
					 
				</td>
				<td width="10%" align="right"><img src="<?php echo $documentroot.'/__statics/img/ksm.png';?>"></td>
				
			  </tr>
			  
		   </table>
		   
		</header>
		 
			<div class="profil">
			<center style="font-weight:bold;font-size:12px;color:black"> KARTU PESERTA KSM <br>  TINGKAT KABUPATEN/KOTA  </center>
			  
				
				<table  width="100%">
				  <tr>
				
					<td  align="left" valign="top" width="25%">
							
								<img src="<?php echo $documentroot; ?>/__statics/upload/<?php echo $data->foto; ?>"  class="photo" />
					</td>
					<td>
								 <table  width="100%" style="font-size:13px">
                                 <tr><td>Nomor Peserta  </td><td>:</td><td><?php echo ucwords($data->no_test); ?></td></tr>
								
								  <tr>
									<td  width="22%" style="height:3px">Nama Lengkap </td>
									<td width="3%" style="height:3px">:</td>
									<td width="50%" style="height:3px"> <?php echo ucwords($data->nama); ?> </td>
								  </tr>
								  
								   <tr><td>Tempat, Tanggal Lahir </td><td>:</td><td><?php echo ($data->tempat); ?>, <?php echo $this->Di->formattanggalstring($data->tgl_lahir); ?></td></tr>
                                  <tr><td>Asal Lembaga </td><td>:</td><td><?php echo $data->madrasah; ?></td></tr>								 
								  <tr><td>Kompetisi  </td><td>:</td> <td>  <?php echo ($this->Di->get_kondisi(array("id"=>$data->trkompetisi_id),"tr_kompetisi","nama")); ?> </td></tr>
								  <tr><td>Lokasi KSM  </td><td>:</td> <td> <?php echo $lokasites; ?> </td></tr>
								  <tr><td valign="top">Alamat Lokasi   </td><td valign="top">:</td> <td valign="top"> <?php echo $alamatlokasi; ?> </td></tr>
								 
									
							      </table>
								 
					
					
					</td>
					</tr>
					
					 <tr>
					 <td colspan="2"> 
					  
					  
						 <div class="req_table">
									<table id="table_detail" style="font-size:12px;">
								
										<tr style="font-weight:bold;">
										    <td>AGENDA   </td>
											<td>HARI    </td>											
											<td>SESI    </td>											
											<td>PUKUL  </td>
										
											
										  </tr>
										  
										
										 <tr>
										    <td> Ujicoba KSM Kabupaten/Kota </td>
											<td> <?php echo $simulasi[$sesiData->hari]; ?></td>
											<td> <?php echo $sesiData->sesi; ?> </td>
											<td> <?php echo $sesiData->$zonaujicoba; ?> <?php echo strtoupper($zona); ?></td>
											
											
										  </tr>

                                          <tr>
										    <td> KSM Kabupaten/Kota    </td>
											<td> <?php echo $pelaksanaan[$sesiData->hari]; ?></td>
											<td> <?php echo $sesiData->sesi; ?> </td>
											<td> <?php echo $sesiData->$zona; ?> <?php echo strtoupper($zona); ?></td>
											
										  </tr>
										
											  
											  
									
												   
										
										  
									
										
								  </table>
							  </div>
					 </td>
				</table>
				
                <ol type="circle" style="font-size:12px">
				<li>Anda dapat mencetak kartu peserta menggunakan <i>printer</i> berwarna </li>
				<li>Login akun <i>CBT</i> KSM Anda  menggunakan : <br>
				
											 <b>  Nomor Peserta : <?php echo ucwords($data->no_test); ?></b> <br> 
				                             <b>  Tanggal lahir : <?php echo ucwords($data->tgl_lahir); ?> </b> </b></i>
											 
											  </li>
											  
		       
				
			    </ol>
					
				
                </div>
           
			
				
			
			  
			  
			  
			   

  
</div>


<center>
<br>
<br>
<a class="btn btn-info"  href="<?php echo site_url("lokal/kartuPesertaKabkoPrint?id=".base64_encode($data->nik_siswa)."&key=".base64_encode($data->id).""); ?>" target="_blank" id="selesaikanpendaftaran"><i class="fa fa-print"></i> CETAK KARTU  <?php echo $data->nama; ?></i></a> 
<a class="btn btn-info"  href="<?php echo site_url("lokal/kartuPesertaKabkoUnduh?id=".base64_encode($data->nik_siswa)."&key=".base64_encode($data->id).""); ?>" target="_blank" id="selesaikanpendaftaran"><i class="fa fa-download"></i>  UNDUH KARTU  <?php echo $data->nama; ?></i></a> 

</center>
<hr>


<?php 
    }
?>
</div>


		
			   
