           <div class="row">
                <div class="col-md-12">                      
					  <div class="card form" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-phone"></i> Kontak Person </h3>
                        </div>
                        <br>
                          <?php $attributes = array('class' => 'form-horizontal', 'role'=>'form','name' => 'simpannorespon', 'id' => 'simpannorespon', 'method' => "post", 'url' =>site_url("lokal/delegasi_simpan")); ?>
			                     <?php echo form_open("javascript:void(0)", $attributes); ?>
                            <div class="col-md-12">
							<div class="alert alert-info">
							 Petunjuk ! <br>
							Masukkan kontak  Kepala Madrasah/Sekolah pada form dibawah ini 
							 </div>
                            <div class="row">
							
									<div class="col-md-12">
										<div class="form-group">
											<label for="name">NIK Ketua Delegasi </label>
											<input type="text" class="form-control" name="f[delegasi_nik]" maxlength="16" value="<?php echo $m->delegasi_nik; ?>">
										</div>
										<div class="form-group">
											<label for="pro-qu">Nama Ketua Delegasi </label>
											<input type="text" class="form-control" name="f[delegasi_nama]" value="<?php echo $m->delegasi_nama; ?>">
										</div>
										<div class="form-group">
											<label for="pro-qu">Nomor Handphone  </label>
											<input type="text" class="form-control" name="f[delegasi_hp]" value="<?php echo $m->delegasi_hp; ?>">
										</div>
									</div>
									
									
							</div>
							
							
							
                             

							<center>
                            <button type="submit" class="btn btn-general btn-white">Simpan </button>  
                            <button type="reset" class="btn btn-general btn-blue">Reset </button> 
							<center>
							<br>
                        <?php echo form_close(); ?>
                    </div>
                    </div>
                    </div>