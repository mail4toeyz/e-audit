   <div id="showform"></div>        
                                 
					  <div class="card form" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-users"></i> Peserta Tes Nasional</h3>
                        </div>
                        <br>
                         
                            <div class="col-md-12">
							<div class="alert alert-info">
							 Petunjuk ! <br>
							  Peserta KSM Tingkat Nasional <br>
							  Jika sertifikat tidak dapat diunduh, silahkan gunakan  browser mozilla firefox mode incognito 
							</div>
								 
									<br>
			 
			 
			              </div>
			
		           <hr>
		<div class="col-md-12">   				
		<div class="table-responsive">
			<table id="datatableTable"  class="table table-striped table-bordered  " cellspacing="0" width="100%">
     
			  <thead>
				<tr>
				   <th class="center" width="3%" rowspan="2">NO</th>
                   <th class="center" rowspan="2">SERTIFIKAT </th>
                   <th class="center" rowspan="2">FOTO </th>
				   <th class="center" rowspan="2">NO PESERTA</th> 
				   <th class="center" rowspan="2">NAMA </th> 
				   <th class="center" rowspan="2">PROVINSI</th> 
				   <th class="center" rowspan="2">LEMBAGA </th>                  
                   <th class="center" rowspan="2">KOMPETISI</th>
                  	
                   <th class="center" rowspan="2">MEDALI </th>	
				   <th class="center" colspan="3">NILAI  </th>
				   <th class="center" colspan="5">RINCIAN NILAI KSM NASIONAL  </th>
				   <th class="center" colspan="4">RINCIAN NILAI KSM PROVINSI  </th>
				   <th class="center" colspan="4">RINCIAN NILAI KSM KABUPATEN/KOTA  </th>
				   <th class="center" rowspan="2">NIK</th>
				   <th class="center" rowspan="2">NISN</th>
				   <th class="center"  rowspan="2">LAHIR</th>
				   <th class="center"  rowspan="2">TGL LAHIR</th>
				  
				  
				   <th class="center"  rowspan="2">NIK AYAH</th>					  
				   <th class="center"  rowspan="2">NAMA AYAH</th>					  
				   <th class="center"  rowspan="2">NIK IBU</th>					  
				   <th class="center"  rowspan="2">NAMA IBU</th>	
				   
				   <th class="center" rowspan="2">KABUPATEN/KOTA </th> 
				   <th class="center" rowspan="2">KECAMATAN </th> 
				   <th class="center" rowspan="2">DESA </th> 			  
				   <th class="center" colspan="5">DETAIL LEMBAGA </th>
				   
				</tr>
				<tr>
				  <th> NILAI  </th>
                  <th> CBT </th>
                  <th> EKSPLORASI </th>
                

				  <th> BNR PG </th>
                  <th> BNR ESSAY </th>
                  <th> JML BNR </th>
                  <th> SLH </th>
                  <th> KSG </th>

				  <th> NILAI </th>
                  <th> JML BNR </th>
                  <th> SLH </th>
                  <th> KSG </th>

				  <th> NILAI </th>
                  <th> JML BNR </th>
                  <th> SLH </th>
                  <th> KSG </th>

				  <th> JENIS </th>
				  <th> JENJANG </th>
				  <th> NSM/NPSN </th>
				  <th> LEMBAGA  </th>
				  <th> NIK KEPALA  </th>
				  <th> NAMA KEPALA  </th>
				  <th> HP KEPALA </th>

				
				</tr>
			  </thead>
			  <tbody></tbody>
            </table>
			</div>
        </div>
        </div>
		

<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<b style="color:white;background:#33AFFF">Data sedang di tampilkan..</b>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data tidak atau belum tersedia",
						   "sLengthMenu": "Default menampilkan   _MENU_ Data <small>(Klik pada angkanya untuk memuat lebih banyak data)</small>"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[5,10,25, 50,100,200,300,500,1000, 800000000], [5,10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
						
							text:'<span class="fa fa-file-excel-o"></span> Cetak Excel',
							className :"btn btn-danger btn-sm"
							},
							
							
						
						
							{
							extend: 'colvis',
							
							text:'<span class="fa fa-columns "></span> Setting Kolom ',
							className :"btn btn-danger "
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("lokal/g_nasional"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
	
		
		$(document).on('input click change', '#keyword,#searchcustom', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
		
		$(document).on(' click ', '#cancelr', function (event, messages) {			
			   $("#showform").html("");
			   dataTable.ajax.reload(null,false);	        
		  
        });
		

</script>
			
                    