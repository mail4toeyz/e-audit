           <div class="row">
                <div class="col-md-12">                      
					  <div class="card form" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-user"></i> <?php echo $title; ?> </h3>
                        </div>
                        <br>
                          <?php $attributes = array('class' => 'form-horizontal', 'role'=>'form','name' => 'simpangambar', 'id' => 'simpangambar', 'method' => "post","enctype"=>"multipart/form-data", 'url' =>site_url("lokal/simpanpeserta_u")); ?>
			                     <?php echo form_open("javascript:void(0)", $attributes); ?>
                            <div class="col-md-12">
							
                            <div class="row">
							<div class="col-md-3">
										<div class="form-group">
											<label for="name">Upload Foto Peserta (3x4) </label>
											<input type="file" class="form-control" name="file" id="image">
										</div>
										
										<div class="form-group">
										   <?php 
										     if(isset($dataform)){
												 if($dataform->foto==""){
													$file = "not.png";
												 }else{
													 $file = $dataform->foto;
												 }
												 
											 }else{
												 
												    $file = "not.png";
											 }
											 
											 ?>
											<img src="<?php echo base_url(); ?>__statics/upload/<?php echo $file; ?>" class="img-thumbnail img-responsive" style="height:200px" id="blah" >
										</div>
										
									</div>
									
									<div class="col-md-9">
										<div class="form-group">
											<label for="name">NISN (*) </label>
											<input type="hidden" class="form-control" name="id" value="<?php echo isset($dataform) ?  $dataform->id :""; ?>">
											<input type="text" class="form-control" name="f[nisn]" value="<?php echo isset($dataform) ?  $dataform->nisn :""; ?>">
										</div>
										<div class="form-group">
											<label for="name">Nama Siswa (*)</label>
											<input type="text" class="form-control" name="f[nama]" readonly value="<?php echo isset($dataform) ?  $dataform->nama :""; ?>">
										</div>
										
										<div class="form-group">
											<label for="name">Gender (*)</label> <br>
											<?php 
															      $gender = $this->Di->gender();
																    foreach($gender as $index=>$row){
																		
																		?><label for="<?php echo $index; ?>"> <input type="radio" name="f[gender]" value="<?php echo $row; ?>" id="<?php echo $index; ?>" <?php if(isset($dataform)){ echo  ($dataform->gender==$row) ? "checked":"";  } ?>> <?php echo $row; ?> </label> &nbsp; <?php
																	}
															   ?>
															   
										</div>
										
										
									</div>
									
									
									   <div class="col-md-6">
									   
									     <div class="form-group">
											<label for="name">Tempat Lahir (*)</label>
											<input type="text" class="form-control" name="f[tempat]" value="<?php echo isset($dataform) ?  $dataform->tempat :""; ?>">
										</div>
										</div>
										<div class="col-md-6">
										<div class="form-group">
											<label for="name">Tanggal Lahir (*)  </label>
											 <input type="text" class="form-control" name="f[tgl_lahir]" autocomplete="no" id="tgl_lahir" readonly placeholder="Format : dd-mm-yyyy" value="<?php echo isset($dataform) ?  ($dataform->tgl_lahir) :""; ?>">
														    <script>
																$(document).ready(function () {
																	startdate = new Date() - 18;
																	$('#tgl_lahir').datepicker({
																	 changeMonth: true,
																								changeYear: true,
																								autoclose: true,
																								dateFormat: 'yy-mm-dd',
																								yearRange: "2000:2020",
																							
																	});
																});
															</script>
										</div>
										</div>
										
										
										<div class="col-md-6">
									   
									     <div class="form-group">
											<label for="name">Email  </label>
											<input type="email" class="form-control" name="f[email]" value="<?php echo isset($dataform) ?  $dataform->email :""; ?>">
										</div>
										</div>
										<div class="col-md-6">
										<div class="form-group">
											<label for="name">Nomor Handphone  </label>
											<input type="text" class="form-control" name="f[hp]" value="<?php echo isset($dataform) ?  $dataform->hp :""; ?>">
										</div>
										</div>
									
									
									  <div class="col-md-12">
										<div class="form-group">
											<label for="name">Alamat Lengkap </label>
											<textarea type="text" class="form-control" name="f[alamat]" value=""><?php echo isset($dataform) ?  $dataform->alamat :""; ?></textarea>
										</div>
									  </div>
									
									
									
							</div>
							
							
							
                             

							<center>
                            <button type="submit" class="btn btn-general btn-white">Simpan Data </button>  
                            <button type="reset" class="btn btn-general btn-blue">Reset </button> 
                            <button type="button" class="btn btn-general btn-white" id="cancel">Tutup Form </button> 
							<center>
							<br>
                        <?php echo form_close(); ?>
                    </div>
                    </div>
                    </div>
					
<script>
					function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blah').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#image").change(function() {
  readURL(this);
});
</script>