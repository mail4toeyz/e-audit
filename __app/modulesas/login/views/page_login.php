<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
		
		<meta charset="utf-8" />
		<title>Login |  BKBA</title>
		<meta name="description" content="Aplikasi Bantuan Kinerja dan Afirmasi" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
	
		<link href="<?php echo base_url(); ?>__statics/tema/css/pages/login/login-31894.css?v=7.1.9" rel="stylesheet" type="text/css" />
		
		<link href="<?php echo base_url(); ?>__statics/tema/plugins/global/plugins.bundle1894.css?v=7.1.9" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>__statics/tema/plugins/custom/prismjs/prismjs.bundle1894.css?v=7.1.9" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>__statics/tema/css/style.bundle1894.css?v=7.1.9" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>__statics/fa/css/font-awesome.min.css">
		<link rel="shortcut icon" href="<?php echo base_url(); ?>__statics/img/logo.png" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/alert/alert.css">
		<script src="<?php echo base_url(); ?>__statics/js/jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>__statics/js/alert/alert.js"></script>
</head>
	
	<body id="kt_body" class="header-mobile-fixed subheader-enabled aside-enabled aside-fixed aside-secondary-enabled page-loading">
		
		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Login-->
			<div class="login login-3 wizard d-flex flex-column flex-lg-row flex-column-fluid">
				<!--begin::Aside-->
				<div class="login-aside d-flex flex-column flex-row-auto">
					<!--begin::Aside Top-->
					<div class="d-flex flex-column-auto flex-column pt-lg-40">
						
						<a href="#" class="login-logo text-center ">
							<img src="<?php echo base_url(); ?>__statics/img/logo.png" class="max-h-70px" alt="" /> &nbsp;&nbsp;
							<img src="<?php echo base_url(); ?>__statics/img/logocolor.png" class="max-h-70px" alt="" />
						</a>
						
						<h2 class="font-weight-bolder text-center font-size-h1 line-height-xl"> <br> Bantuan Kinerja &amp; Bantuan Afirmasi <br> (BKBA)</h2>
						<!--end::Aside Title-->
					</div>
					<!--end::Aside Top-->
					<!--begin::Aside Bottom-->
					<br>
					<div class="aside-img d-flex flex-row-fluid bgi-no-repeat" style=" background-image: url(<?php echo base_url(); ?>__statics/img/login.svg)"></div>
					<!--end::Aside Bottom-->
				</div>
				<!--begin::Aside-->
				<!--begin::Content-->
				<div class="login-content flex-row-fluid d-flex flex-column p-10">
					<!--begin::Top-->
					<div class="text-right d-flex justify-content-center">
						<div class="top-signin text-right d-flex justify-content-end pt-5 pb-lg-0 pb-10">
							<span class="font-weight-bold text-muted font-size-h4"> Madrasah Reform </span>
							<a href="https://madrasahreform.kemenag.go.id" class="font-weight-bold text-primary font-size-h4 ml-2" id="kt_login_signup">Portal </a>
						</div>
					</div>
					<!--end::Top-->
					<!--begin::Wrapper-->
					<div class="d-flex flex-row-fluid flex-center">
						<!--begin::Signin-->
						<div class="login-form">
							<!--begin::Form-->
							<form id="login" class="form" action="javascript:void(0)" url="<?php echo site_url("login/do_login"); ?>" method="post">
						
								<!--begin::Title-->
								<div class="pb-5 pb-lg-15">
									<h3 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Silahkan Masuk </h3>
									
								</div>
								<!--begin::Title-->
								<!--begin::Form group-->
								<div class="form-group">
									<label class="font-size-h6 font-weight-bolder text-dark">Username </label>
									<input class="form-control h-auto py-7 px-6 rounded-lg border-0" type="text" name="username" autocomplete="off" placeholder="Masukkan Username Anda" />
								</div>
								<!--end::Form group-->
								<!--begin::Form group-->
								<div class="form-group">
									<div class="d-flex justify-content-between mt-n5">
										<label class="font-size-h6 font-weight-bolder text-dark pt-5">Password</label>
										
									</div>
									<input class="form-control h-auto py-7 px-6 rounded-lg border-0" type="password" name="password" autocomplete="off" placeholder="Masukkan Password Anda" />
								</div>
								<!--end::Form group-->
								<!--begin::Action-->
								<div class="pb-lg-0 pb-5">
									<button type="submit"  class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3"><span class="fa fa-sign-in"></span> Masuk Aplikasi </button>
									<a href="<?php echo site_url("login/aktivasi"); ?>"  class="btn btn-danger font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3" ><span class="fa fa-edit"></span> Aktivasi Asesor   </a>
									
									<img src="<?php echo base_url(); ?>__statics/img/loading.gif" id="loadingm" style="display:none">
								</div>
								Lupa Password Asesor ? <a href="<?php echo site_url("login/forget"); ?>">Klik disini untuk Reset Akun BKBA </a> 
								<!--end::Action-->
							</form>
							<!--end::Form-->
						</div>
						<!--end::Signin-->
					</div>
					<!--end::Wrapper-->
				</div>
				<!--end::Content-->
			</div>
			<!--end::Login-->
		</div>
		<!--end::Main-->
		
	</body>
	<!--end::Body-->
</html>


<script type="text/javascript">

var base_url="<?php echo base_url(); ?>";

function sukses(param){
	   let timerInterval
	   Swal.fire({
		 type: 'success',
		 title: 'Proses Authentication Berhasil ',
		 showConfirmButton: false,
		  html: 'Dalam <strong></strong> detik<br>Anda akan dialihkan kedalam aplikasi',

		 timer: 1000,
		 onBeforeOpen: () => {
		   Swal.showLoading()
		   timerInterval = setInterval(() => {
			 Swal.getContent().querySelector('strong')
			   .textContent = Swal.getTimerLeft()
		   }, 100)
		 },
		 onClose: () => {
		   clearInterval(timerInterval)
		 }
	   }).then((result) => {
		 if (
		   /* Read more about handling dismissals below */
		   result.dismiss === Swal.DismissReason.timer
		   
		 ) {
		
		  location.href = base_url+param;
		 }
	   })
}

function gagal(param){
	   let timerInterval
	   Swal.fire({
		 type: 'warning',
		 title: 'Proses Authentication Gagal ',
		 showConfirmButton: true,
		  html: param

	   })
}


   $(document).on('submit', 'form#login', function (event, messages) {
	event.preventDefault()
	  var form   = $(this);
	  var urlnya = $(this).attr("url");
   
	 
	
			 $("#loadingm").show();
			  $.ajax({
				   type: "POST",
				   url: urlnya,
				   data: form.serialize(),
				   success: function (response, status, xhr) {
					   var ct = xhr.getResponseHeader("content-type") || "";
					   if (ct == "application/json") {
					   
						   gagal(response.message);
			   
						
						   
						   
					   } else {
						 
						  
						  sukses(response);
						  
						   
						   
							
						   
						 
					   }
					   
					   $("#loadingm").hide();
					   
				   }
			   });
			 
	   return false;
   });
   
   
   
</script>