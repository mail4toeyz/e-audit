<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	
		<meta charset="utf-8" />
		<title>Lupa Password Asesor Bantuan Kinerja dan Bantuan Afirmasi </title>
		<meta name="description" content="Singin page example" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<link href="<?php echo base_url(); ?>__statics/tema/css/pages/login/login-31894.css?v=7.1.9" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>__statics/tema/plugins/global/plugins.bundle1894.css?v=7.1.9" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>__statics/tema/plugins/custom/prismjs/prismjs.bundle1894.css?v=7.1.9" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>__statics/tema/css/style.bundle1894.css?v=7.1.9" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/alert/alert.css">
		<link rel="shortcut icon" href="<?php echo base_url(); ?>__statics/img/logo.png" />
		</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-mobile-fixed subheader-enabled aside-enabled aside-fixed aside-secondary-enabled page-loading">
		
		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Login-->
			<div class="login login-3 wizard d-flex flex-column flex-lg-row flex-column-fluid wizard" id="kt_login">
				<!--begin::Aside-->
				<div class="login-aside d-flex flex-column flex-row-auto">
            
				
					<!--begin::Aside Top-->
					<div class="d-flex flex-column-auto flex-column pt-20 px-25">
						<!--begin::Aside header-->
                        <a href="#" class="login-logo text-center ">
							<img src="<?php echo base_url(); ?>__statics/img/logo.png" class="max-h-70px" alt="" /> 
						</a>
						
						<h3 class="font-weight-bolder text-center font-size-h2 line-height-xl"> <br> Bantuan Kinerja &amp; Bantuan Afirmasi  (BKBA)</h3>
			
						<!--end::Aside header-->
						<!--begin: Wizard Nav-->
						<div class="wizard-nav pt-5 pt-lg-30">
							<!--begin::Wizard Steps-->
							<div class="wizard-steps">
								<!--begin::Wizard Step 1 Nav-->
								<div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
									<div class="wizard-wrapper">
										<div class="wizard-icon">
											<i class="wizard-check ki ki-check"></i>
											<span class="wizard-number">1</span>
										</div>
										<div class="wizard-label">
											<h3 class="wizard-title">Validasi Asesor  </h3>
											<div class="wizard-desc">BKBA akan melakukan validasi data asesor  </div>
										</div>
									</div>
								</div>
								<!--end::Wizard Step 1 Nav-->
								<!--begin::Wizard Step 2 Nav-->
								<div class="wizard-step" data-wizard-type="step">
									<div class="wizard-wrapper">
										<div class="wizard-icon">
											<i class="wizard-check ki ki-check"></i>
											<span class="wizard-number">2</span>
										</div>
										<div class="wizard-label">
											<h3 class="wizard-title">Reset Password    </h3>
											<div class="wizard-desc">BKBA akan melakukan aktivasi Akun Asesor   </div>
										</div>
									</div>
								</div>

								
								<!--end::Wizard Step 2 Nav-->
								<!--begin::Wizard Step 3 Nav-->
								
								<!--end::Wizard Step 3 Nav-->
							
								<!--end::Wizard Step 4 Nav-->
							</div>
							<!--end::Wizard Steps-->
						</div>
						<!--end: Wizard Nav-->
					</div>
					<!--end::Aside Top-->
					<!--begin::Aside Bottom-->
								<!--end::Aside Bottom-->
				</div>
				<!--begin::Aside-->
				<!--begin::Content-->
				<div class="login-content flex-column-fluid d-flex flex-column p-10">
					<!--begin::Top-->
					
					<div class="text-right d-flex justify-content-center">
						<div class="top-signup text-right d-flex justify-content-end pt-5 pb-lg-0 pb-10">
							<span class="font-weight-bold text-muted font-size-h4">Login  ?</span>
							<a href="<?php echo site_url("login"); ?>" class="font-weight-bolder text-primary font-size-h4 ml-2" id="kt_login_signup">Klik disini  </a>
						</div>
					</div>
					<!--end::Top-->
					<!--begin::Wrapper-->
					
					<div class="d-flex flex-row-fluid flex-center">
					
						<!--begin::Signin-->
						<div class="login-form login-form-signup" id="loginPage">
							<!--begin::Form-->
							<form class="form" novalidate="novalidate" id="kt_login_signup_form" url="<?php echo site_url("login/do_resetPassword"); ?>" method="post">
								<!--begin: Wizard Step 1-->
								<input type="hidden" name="f[npsn]" id="npsn">
								<div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
									<!--begin::Title-->
									<div class="pb-10 pb-lg-15"> 
									<img src="<?php echo base_url(); ?>__statics/img/logocolor.png" class="max-h-90px" alt="" /> <br><br>
										<h3 class="font-weight-bolder text-dark display5">Reset Password Asesor BKBA  </h3> 
										<div class="text-muted font-weight-bold font-size-h4"> Jika Anda sudah membuat akun BKBA namun tidak bisa login, silahkan reset password Anda </div>
										
									</div>
									<!--begin::Title-->
									<!--begin::Form Group-->
									<div class="row">
										
										<div class="col-xl-12">
											<!--begin::Input-->
											<label class="font-size-h6 font-weight-bolder text-dark" id="usernameLabel"> Masukkan Nomor Induk Kependudukan (NIK) Anda  </label>
											<div class="input-group">
											         
														<input type="text" class="form-control" name="nik" id="nik" maxlength="16" placeholder="Ketik disini .. " data-toggle="tooltip" title="Isi jawaban Anda kemudian klik tombol validasi ">
															<div class="input-group-append">
																<button class="btn btn-primary" type="button" id="validasi"> Validasi </button>
															</div>
											</div>
											<!--end::Input-->
										</div>
										

									</div>

                                    <div class="row">
											<div class="col-xl-12"  id="loader" style="display:none;color:red">
											   <center><i class="fas fa-spinner fa-pulse"></i> <b id="text-loader"> Proses Validasi</b> </center> 
											</div>
									</div>

                                    <div class="row">
											<div class="col-xl-12" id="loaddata">
											  
											</div>
									</div>
									


									<!--end::Form Group-->
								</div>
								<!--end: Wizard Step 1-->
								<!--begin: Wizard Step 2-->
								
								<!--begin: Wizard Step 3-->
								<div class="pb-5" data-wizard-type="step-content">
									<!--begin::Title-->
									<div class="pt-lg-0 pt-5 pb-15">
										<h3 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Reset Akun BKBA </h3>
										<div class="text-muted font-weight-bold font-size-h4">Masukkan Email dan Nomor Whatsapp  Anda untuk dilakukan Reset akun, Akun akan dikirim kedalam Whatsapp dan email Anda</div>
									</div>
									<!--end::Title-->
									<!--begin::Form Group-->
                                   <div class="form-group">
										<label class="font-size-h6 font-weight-bolder text-dark">Email  :</label>
										<input type="email" class="form-control" name="email" placeholder="Pastikan email yang dimasukkan valid agar dapat diverifikasi" />
									</div> 

									<div class="form-group">
										<label class="font-size-h6 font-weight-bolder text-dark">Nomor Whatsapp  :</label>
										<input type="text" class="form-control" name="whatsapp" placeholder="Pastikan   Nomor Whatsapp yang dimasukkan valid agar dapat diverifikasi" />
									</div>
									<!--end::Form Group-->
									<!--begin::Form Group-->
									
									<!--end::Form Group-->

                                    <div class="row">
											<div class="col-xl-12"  id="loaderaktivasi" style="display:none;color:red">
											   <center><i class="fas fa-spinner fa-pulse"></i> <b id="text-loader"> Proses Validasi</b> </center> 
											</div>
									</div>


								</div>
								<!--end: Wizard Step 3-->
								<!--begin: Wizard Step 4-->
								
								<!--end: Wizard Step 4-->
								<!--begin: Wizard Actions-->
								<div class="d-flex justify-content-between pt-3">
									<div class="mr-2">
										<button type="button" class="btn btn-light-primary font-weight-bolder font-size-h6 pl-6 pr-8 py-4 my-3 mr-3" data-wizard-type="action-prev">
										<span class="svg-icon svg-icon-md mr-1">
											<!--begin::Svg Icon | path:/metronic/theme/html/demo3/dist/assets/media/svg/icons/Navigation/Left-2.svg-->
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<polygon points="0 0 24 0 24 24 0 24" />
													<rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000)" x="14" y="7" width="2" height="10" rx="1" />
													<path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997)" />
												</g>
											</svg>
											<!--end::Svg Icon-->
										</span>Langkah Sebelumnya</button>
									</div>
									<div>
										<button class="btn btn-primary font-weight-bolder font-size-h6 pl-5 pr-8 py-4 my-3" data-wizard-type="action-submit" type="submit" id="kt_login_signup_form_submit_button">Reset Password 
										<span class="svg-icon svg-icon-md ml-2">
											<!--begin::Svg Icon | path:/metronic/theme/html/demo3/dist/assets/media/svg/icons/Navigation/Right-2.svg-->
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<polygon points="0 0 24 0 24 24 0 24" />
													<rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000)" x="7.5" y="7.5" width="2" height="9" rx="1" />
													<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
												</g>
											</svg>
											<!--end::Svg Icon-->
										</span></button>
										<button type="button" style="display:none" id="selanjutnya" class="btn btn-primary font-weight-bolder font-size-h6 pl-8 pr-4 py-4 my-3" data-wizard-type="action-next">Langkah Selanjutnya 
										<span class="svg-icon svg-icon-md ml-1">
											<!--begin::Svg Icon | path:/metronic/theme/html/demo3/dist/assets/media/svg/icons/Navigation/Right-2.svg-->
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<polygon points="0 0 24 0 24 24 0 24" />
													<rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000)" x="7.5" y="7.5" width="2" height="9" rx="1" />
													<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
												</g>
											</svg>
											<!--end::Svg Icon-->
										</span></button>
									</div>
								</div>
								<!--end: Wizard Actions-->
							</form>
							<!--end::Form-->
						</div>
						<!--end::Signin-->
					</div>
					<!--end::Wrapper-->
				</div>
				<!--end::Content-->
			</div>
			<!--end::Login-->
		</div>
		<!--end::Main-->
		<script>var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";</script>
		<!--begin::Global Config(global config for global JS scripts)-->
		<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#1BC5BD", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#6993FF", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#1BC5BD", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#E1E9FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
		<!--end::Global Config-->
		<!--begin::Global Theme Bundle(used by all pages)-->
		<script src="<?php echo base_url(); ?>__statics/tema/plugins/global/plugins.bundle1894.js?v=7.1.9"></script>
		<script src="<?php echo base_url(); ?>__statics/tema/plugins/custom/prismjs/prismjs.bundle1894.js?v=7.1.9"></script>
		<script src="<?php echo base_url(); ?>__statics/tema/js/scripts.bundle1894.js?v=7.1.9"></script>
		<!--end::Global Theme Bundle-->
		<!--begin::Page Scripts(used by this page)-->
		<script src="<?php echo base_url(); ?>__statics/js/alert/alert.js"></script>
		<script src="<?php echo base_url(); ?>__statics/tema/js/pages/custom/login/login-31894.js?v=7.1.9"></script>
		<!--end::Page Scripts-->
		<script src="<?php echo base_url(); ?>__statics/js/proses.js" ></script>
		<script type="text/javascript">
		 

		  $(document).off("click","#validasi").on("click","#validasi",function(){
			var nik  = $("#nik").val();
		
			if(nik==""){
				gagal("Silahkan isi kolom validasi !");
				return false;
			}
			  

			  $("#text-loader").html("<br> Sedang melakukan pengecekan data Anda  <br> Mohon Tunggu..");

			  $("#loader").show();

			 $.post("<?php echo site_url('login/forget_get'); ?>",{nik:nik},function(data){
				$("#loader").hide();	
                if(data !=400){		
                $("#selanjutnya").show();	
				$("#loaddata").html(data);
                }else{

                 $("#loaddata").html("<br><div class='alert alert-danger'> NIK Anda tidak ditemukan dalam Aplikasi BKBA, silahkan melakukan Aktivasi Akun terlebih dahulu ");
                }


			 });


		  });


		  
var base_url="<?php echo base_url(); ?>";
   


   function sukses(param){
		  let timerInterval;
		  Swal.fire({
			type: 'success',
			title: 'Berhasil ',
			showConfirmButton: false,
			 html: 'Pembuatan Akun berhasil, Dalam <strong></strong> detik<br>Anda akan dialihkan kehalaman login',
   
			timer: 3000,
			onBeforeOpen: () => {
			  Swal.showLoading();
			  timerInterval = setInterval(() => {
				Swal.getContent().querySelector('strong')
				  .textContent = Swal.getTimerLeft()
			  }, 100)
			},
			onClose: () => {
			  clearInterval(timerInterval)
			}
		  }).then((result) => {
			if (
			  /* Read more about handling dismissals below */
			  result.dismiss === Swal.DismissReason.timer
			  
			) {
		   
			 location.href = base_url+param;
			}
		  })
   }
   
   
   function sukses2(param){
		  let timerInterval;
		  Swal.fire({
			type: 'success',
			title: 'Berhasil ',
			showConfirmButton: false,
			 html: 'Proses authentikasi berhasil, Dalam <strong></strong> detik<br>Anda akan dialihkan kedalam aplikasi',
   
			timer: 1000,
			onBeforeOpen: () => {
			  Swal.showLoading();
			  timerInterval = setInterval(() => {
				Swal.getContent().querySelector('strong')
				  .textContent = Swal.getTimerLeft()
			  }, 100)
			},
			onClose: () => {
			  clearInterval(timerInterval)
			}
		  }).then((result) => {
			if (
			  /* Read more about handling dismissals below */
			  result.dismiss === Swal.DismissReason.timer
			  
			) {
		   
			 location.href = base_url+param;
			}
		  })
   }
   
   function gagal(param){
		  let timerInterval;
		  Swal.fire({
			type: 'warning',
			title: 'Gagal, Perhatikan ! ',
			showConfirmButton: true,
			 html: param
   
		  })
   }

//    $(document).off('submit', 'form#kt_login_signup_form').on('submit', 'form#kt_login_signup_form', function (event, messages) {
// 	event.preventDefault();
// 	  var form   = $(this);
// 	  var urlnya = $(this).attr("url");
   
	   
// 			 $("#kt_login_signup_form_submit_button").attr("disabled","disabled");
// 			 loading();
// 			  $.ajax({
// 				   type: "POST",
// 				   url: urlnya,
// 				   data: form.serialize(),
// 				   success: function (response, status, xhr) {
// 					   var ct = xhr.getResponseHeader("content-type") || "";
// 					   if (ct == "application/json") {
					   
// 						   gagal(response.message);
			   
						
						   
						   
// 					   } else {
						 
// 						  $("#loginPage").html(response);
						  
						   
						   
							
						   
						 
// 					   }
					   
// 					   $("#kt_login_signup_form_submit_button").removeAttr("disabled");
// 					   $(".loading").hide();
					   
// 				   }
// 			   });
			 
// 	   return false;
//    });


		</script>
	</body>
	<!--end::Body-->
</html>