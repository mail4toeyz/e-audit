<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		$this->load->model('M_model',"mp");
		date_default_timezone_set("Asia/Jakarta");	
        
		
	  }
		

	
	public function index()
	{  
	   
	       $data 			= array();		   	   
		   $this->load->view('page_login',$data);
	

	}
	
		
	
		
	public function do_login(){
     
        $this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
        
        $this->form_validation->set_message('min_length', '{field} Minimal 6 Karakter .');
        $this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
        $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , mungkin anda sudah pernah melakukan pendaftaran, silahkan klik menu login .');
				 
		 
			
				$config = array(
				    array('field' => 'username', 'label' => 'Username   ', 'rules' => 'trim|required'),
				  
				    array('field' => 'password', 'label' => 'Password ', 'rules' => 'trim|required|min_length[4]'),
				  
				 
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			  
			   $data = $this->mp->login();
			    if($data=="gagal"){
			         header('Content-Type: application/json');
                     echo json_encode(array('error' => true, 'message' => "Akun Anda tidak ditemukan, Mohon periksa Username atau Password yang Anda masukkan "));
				}else if($data=="tokensalah"){
					 header('Content-Type: application/json');
					 
                     echo json_encode(array('error' => true, 'message' => "Token Ujian yang Anda masukkan tidak ditemukan, mohon periksa token ujian atau hubungi Guru yang bersangkutan untuk mendapatkan token ujian "));
					
				}else{
					
					echo $data;
				}
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }	
	
	
	}
	
		public function logout(){
		     $this->session->sess_destroy();
			 
		
			echo "yes";
		 
	 }
	 

	 public function aktivasi()
	{  
	   
	       $data 			= array();		   	   
		   $this->load->view('page_aktivasi',$data);
	

	}

	public function aktivasi_get(){
      $nik     = $this->input->get_post("nik");
      $result  = $this->mp->getProd($nik);
		$data['data'] = $result;
		   if($data['data']['code']==200){

			 $this->load->view("page_data",$data);

		   }else{

			echo $data['data']['code'];
		   }

	}


	public function forget()
	{  
	   
	       $data 			= array();		   	   
		   $this->load->view('page_forget',$data);
	

	}

	public function forget_get(){
      $nik     = $this->input->get_post("nik");
      $result  = $this->db->get_where("asesor",array("nik"=>$nik))->row();
	 
		$data['data'] = $result;
		   if(!is_null($data['data'])){

			 $this->load->view("page_forget_data",$data);

		   }else{

			echo "NIK Anda tidak ditemukan";
		   }

	}



	public function do_registrasi(){
     
        $this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
        
        $this->form_validation->set_message('min_length', '{field} Minimal 6 Karakter .');
        $this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
        $this->form_validation->set_message('is_unique', '{field} Sudah diaktivasi , silahkan login menggunakan username dan password yang sudah kami kirim ke whatsapp dan Email Anda ');
				 
		 
			
				$config = array(
				    array('field' => 'nik', 'label' => 'NIK ', 'rules' => 'trim|required|is_unique[asesor.nik]'),				  
				    array('field' => 'email', 'label' => 'Email ', 'rules' => 'trim|required'),
				    array('field' => 'whatsapp', 'label' => 'Nomor Whatsapp ', 'rules' => 'trim|required'),
				   
				 
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			   $nik      = $this->input->post("nik");
			   $username = $nik;
			   $email    = $this->input->post("email");
			   $whatsapp = $this->input->post("whatsapp");
			   $password = $this->Reff->randomangka(6);

			   $dataAsesor = $this->mp->getProd($nik);

			   if($dataAsesor['code']==200){

					//	$this->db->set("id",$dataAsesor['data']['id']);
						$this->db->set("nik",$dataAsesor['data']['nik']);
						$this->db->set("nama",$dataAsesor['data']['nama_lengkap']);
						$this->db->set("tempat_lahir",$dataAsesor['data']['tempat_lahir']);
						$this->db->set("tanggal_lahir",$dataAsesor['data']['tanggal_lahir']);
						$this->db->set("status_pegawai",$dataAsesor['data']['status_pegawai']);
						$this->db->set("golongan",$dataAsesor['data']['golongan']);
						$this->db->set("nip",$dataAsesor['data']['nip']);
						$this->db->set("no_telepon",$dataAsesor['data']['no_telepon']);
						$this->db->set("email",$dataAsesor['data']['email']);
						$this->db->set("whatsapp",$whatsapp);
						$this->db->set("alamat",$dataAsesor['data']['alamat']);
						$this->db->set("provinsi_id",$dataAsesor['data']['provinsi_id']);
						$this->db->set("kabupaten_id",$dataAsesor['data']['kabupaten_id']);
						$this->db->set("kecamatan_id",$dataAsesor['data']['kecamatan_id']);
						$this->db->set("kelurahan_id",$dataAsesor['data']['kelurahan_id']);
						$this->db->set("jabatan",$dataAsesor['data']['jabatan']);
						$this->db->set("jabatan_provinsi_id",$dataAsesor['data']['jabatan_provinsi_id']);
						$this->db->set("jabatan_kabupaten_id",$dataAsesor['data']['jabatan_kabupaten_id']);
						$this->db->set("username",$username);
						$this->db->set("password",$password);
						$this->db->set("tgl_daftar",date("Y-m-d H:i:s"));
						$this->db->insert("asesor");

						$sendEMail    = array("tujuan"=>$email,"username"=>$username,"password"=>$password);		  
					    $this->mp->post('https://appmadrasah.kemenag.go.id/cbtpkb/web/sendAktivasiBKBA', $sendEMail);


						// $message = "*Aktivasi Akun Asesor BKBA*\nYth. ".$dataAsesor['data']['nama_lengkap'].",\nAkun Asesor Anda berhasil diaktivasi, silahkan login kedalam aplikasi BKBA sebagai berikut : \nhttps://appmadrasah.kemenag.go.id/bkba \nUsername  : ".$username." \nPassword  : ".$password."\n  ";
						// //$message =" Hallo";
						// $sendWhatsapp = "sender=dendi&number=".$whatsapp."&message=".$message.""; 		  
					    // $this->mp->sendWA($sendWhatsapp);



						$this->Reff->log($dataAsesor['data']['nama_lengkap']. " Aktivasi akun asesor pada ".formattimestamp(date("Y-m-d H:i:s"))."","2",$dataAsesor['data']['id'],$dataAsesor['data']['id']);

					    echo "<div class='alert alert-primary' style='font-size:25px'><center> Terimakasih sudah melakukan aktivasi <br> Data Aktivasi sudah dikirim, silahkan login menggunakan username dan password yang kami kirim  melalui email dan Whatsapp Anda <br> <a href=".site_url('login')." class='btn btn-danger'> Login Asesor </a> </center></div>";


			   }else{

				header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => "NIK Anda belum terdaftar didalam Aplikasi AMAN"));
			   }

			   


												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }	
	
	
	}


	public function do_resetPassword(){
     
        $this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
        
        $this->form_validation->set_message('min_length', '{field} Minimal 6 Karakter .');
        $this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
        $this->form_validation->set_message('is_unique', '{field} Sudah diaktivasi , silahkan login menggunakan username dan password yang sudah kami kirim ke whatsapp dan Email Anda ');
				 
		 
			
				$config = array(
				    array('field' => 'nik', 'label' => 'NIK ', 'rules' => 'trim|required'),				  
				    array('field' => 'email', 'label' => 'Email ', 'rules' => 'trim|required'),
				    array('field' => 'whatsapp', 'label' => 'Nomor Whatsapp ', 'rules' => 'trim|required'),
				   
				 
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			   $nik      = $this->input->post("nik");
			   $username = $nik;
			   $email    = $this->input->post("email");
			   $whatsapp = $this->input->post("whatsapp");
			   $password = $this->Reff->randomangka(6);

			//    $dataAsesor = $this->mp->getProd($nik);
			$result  = $this->db->get_where("asesor",array("nik"=>$nik))->row();
			if(!is_null($result)){

						
						$this->db->where("nik",$result->nik);
						
						$this->db->set("password",$password);
						$this->db->set("whatsapp",$whatsapp);
						$this->db->set("email",$email);
						
						$this->db->update("asesor");

						$sendEMail    = array("tujuan"=>$email,"username"=>$username,"password"=>$password);		  
					    $this->mp->post('https://appmadrasah.kemenag.go.id/cbtpkb/web/sendAktivasiBKBA', $sendEMail);


						// $message = "*Reset Akun Asesor BKBA*\nYth. ".$result->nama.",\nAkun Asesor Anda berhasil reset, silahkan login kedalam aplikasi BKBA sebagai berikut : \nhttps://appmadrasah.kemenag.go.id/bkba \nUsername  : ".$username." \nPassword  : ".$password."\n  ";
						// //$message =" Hallo";
						// $sendWhatsapp = "sender=dendi&number=".$whatsapp."&message=".$message.""; 		  
					    // $this->mp->sendWA($sendWhatsapp);



						$this->Reff->log($result->nama. " Reset akun asesor pada ".formattimestamp(date("Y-m-d H:i:s"))."","2",$result->id,$result->id);

					    echo "<div class='alert alert-primary' style='font-size:25px'><center> Akun Anda berhasil direset <br>  silahkan login menggunakan username dan password yang kami kirim  melalui email dan Whatsapp Anda <br> <a href=".site_url('login')." class='btn btn-danger'> Login Asesor </a> </center></div>";


			   }else{

				header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => "NIK Anda belum terdaftar didalam Aplikasi AMAN"));
			   }

			   


												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }	
	
	
	}
	
   public function broadcastWA(){

	  $asesor = $this->db->query("select * from asesor")->result();
	     foreach($asesor as $rs){

			$message = "*Informasi  Asesor BKBA* \n\nYth. ".$rs->nama.",\nSistem Kami mendeteksi bahwa Anda sudah melakukan aktivasi akun asesor BKBA, Anda dapat login kedalam aplikasi BKBA pada alamat : \nhttps://appmadrasah.kemenag.go.id/bkba \nUsername Anda : ".$rs->username." \nPassword Anda  : ".$rs->password." \n\n  ";
						//$message =" Hallo";
			$sendWhatsapp = "sender=dendi&number=".$rs->whatsapp."&message=".$message.""; 

		  	$this->mp->sendWA($sendWhatsapp);

		    $sendEMail    = array("tujuan"=>$rs->email,"username"=>$rs->username,"password"=>$rs->password);		  
			$this->mp->post('https://appmadrasah.kemenag.go.id/cbtpkb/web/sendAktivasiBKBA', $sendEMail);


		 }
   }

   public function sendEmail(){
	$sendEMail    = array("tujuan"=>"dendi.raziz@gmail.com","username"=>"dendi","password"=>"123456");		  
	echo $this->mp->post('https://appmadrasah.kemenag.go.id/cbtpkb/web/sendAktivasiBKBA', $sendEMail);
   }
   public function notdendi(){
	   
	   
	   $this->load->view("notdendi");
   }
	

   public function getAsesorVisitasi(){
       $tahun   = $this->Reff->tahun();
	    $asesor = $this->db->query("select * from asesor")->result();
	    $madrasah = $this->db->query("select * from madrasahedm where tahun='{$tahun}' and shortlist=1")->result();

		  foreach($asesor as $rs){
				foreach($madrasah as $rm){
					
						$datanya = $this->mp->getAsesorVisitasi($rs->nik,$rm->nsm);

						
						if($datanya['code']=="200"){
							

							$this->db->set("nik",$datanya['data']['nik']);
							$this->db->set("nsm",$datanya['data']['nsm']);
							$this->db->set("nama_madrasah",$datanya['data']['nama_madrasah']);
							$this->db->set("tanggal_mulai",$datanya['data']['tanggal_mulai']);
							$this->db->set("tanggal_selesai",$datanya['data']['tanggal_selesai']);
							$this->db->set("jam_mulai",$datanya['data']['jam_mulai']);
							$this->db->set("jam_selesai",$datanya['data']['jam_selesai']);							
							$this->db->insert("visitasi_jadwal");

						}
						
				}
		  }

   }

		public function cekAsesorVisitasi(){
			
		    $datanya = $this->mp->getAsesorVisitasi($_POST['nik'],$_POST['nsm']);
			 
			 
			 if($datanya['code']=="200"){

				 $cekValid = $this->db->get_where("visitasi_jadwal",array("nik"=>$datanya['data']['nik'],"nsm"=>$datanya['data']['nsm']))->num_rows();
				    if($cekValid==0){

							$this->db->set("nik",$datanya['data']['nik']);
							$this->db->set("nsm",$datanya['data']['nsm']);
							$this->db->set("nama_madrasah",$datanya['data']['nama_madrasah']);
							$this->db->set("tanggal_mulai",$datanya['data']['tanggal_mulai']);
							$this->db->set("tanggal_selesai",$datanya['data']['tanggal_selesai']);
							$this->db->set("jam_mulai",$datanya['data']['jam_mulai']);
							$this->db->set("jam_selesai",$datanya['data']['jam_selesai']);							
							$this->db->insert("visitasi_jadwal");
					}



			 }
				

		}


		public function cekJadwalVisitasi($nik){
			
		    $datanya = $this->mp->getJadwalVisitasi($nik);

			// print_r($datanya)."sd"; exit();
			
			 
			 if($datanya['code']=="200"){
				
				foreach($datanya['data'] as $row){

					$this->db->where("nik",$row['nik']);
					$this->db->where("nsm",$row['nsm']);
											
					$this->db->delete("visitasi_jadwal");
					
					
				 $cekValid = $this->db->get_where("visitasi_jadwal",array("nik"=>$row['nik'],"nsm"=>$row['nsm']))->num_rows();
				   // if($cekValid==0){
					

							$this->db->set("nik",$row['nik']);
							$this->db->set("nsm",$row['nsm']);
							$this->db->set("nama_madrasah",$row['nama_madrasah']);
							$this->db->set("tanggal_mulai",$row['tanggal_mulai']);
							$this->db->set("tanggal_selesai",$row['tanggal_selesai']);
							$this->db->set("jam_mulai",$row['jam_mulai']);
							$this->db->set("jam_selesai",$row['jam_selesai']);							
							$this->db->set("jenis_program",$row['jenis_program']);							
							$this->db->insert("visitasi_jadwal");
					//}

					
				}

				echo "sukses";



			 }
				

		}
	
}
