 <script type="text/javascript" src="<?php echo base_url(); ?>__statics/ckeditor/ckeditor.js" ></script>     
           
		   
<?php 
  
     if(isset($edit)){
		 ?>
		 
					 
			 <div class="acc-setting row" style="margin-top:10px;margin-bottom:10px">
			<div class="col-md-2" style="padding:10px">
			  <b> NAVIGASI SOAL </b> 
			   
			</div>

			<div class="col-md-8" style="padding:10px">
			<ul class="nav nav-tabs navigasisoal">
			 <?php 
				$no=0;
				foreach($soal as $a){
						 $no++;
					   ?>
				<li id="navigasidendi<?php echo $a->id; ?>"><a data-toggle="tab" class="navigasidendi" href="#detailpertanyaan<?php echo $a->id; ?>" id_soal="<?php echo $a->id; ?>">
							  <span class="fa-stack" style="color:blue;border:1px solid blue">
																	  <span class="fa fa-square-o fa-stack-2x"></span>
																			<strong class="fa-stack-1x"><?php echo $a->urutan; ?></strong>
																	</span>
				
				</a></li>
				
				
			   <?php 
					 }
					?>
			  </ul>
			</div>
			<div class="col-md-2" style="padding:10px">
			  <button class="btn btn-sm btn-warning tambahsoal" tmujian_id="<?php echo $tmujian_id ?>"><i class="la la-plus-circle"></i> Tambah Soal  </button>
			   
			</div>
			</div>

			<?php 
	 }else{
		 if(isset($dendiganteng)){
		?>
		 
		  <div class="acc-setting row" style="margin-top:10px;margin-bottom:10px">
			<div class="col-md-2" style="padding:10px">
			  <b> NAVIGASI SOAL </b> 
			   
			</div>

			<div class="col-md-8" style="padding:10px">
			<ul class="nav nav-tabs navigasisoal">
			
				<li id="navigasidendi<?php echo $soal->id; ?>"><a data-toggle="tab" class="navigasidendi" href="#detailpertanyaan<?php echo $soal->id; ?>" id_soal="<?php echo $soal->id; ?>">
							  <span class="fa-stack" style="color:blue;border:1px solid blue">
																	  <span class="fa fa-square-o fa-stack-2x"></span>
																			<strong class="fa-stack-1x"><?php echo $soal->urutan; ?></strong>
																	</span>
				
				</a></li>
				
				
			  </ul>
			</div>
			<div class="col-md-2" style="padding:10px">
			  <button class="btn btn-sm btn-warning tambahsoal" tmujian_id="<?php echo $tmujian_id ?>"><i class="la la-plus-circle"></i> Tambah Soal  </button>
			   
			</div>
			</div>
		 
		 
		<?php  
		 }
	
	 }
?>	 

  <?php 
     if(!isset($edit)){
		 ?>
		   <div class="tab-content acc-setting">
					  <div class="tab-pane in active detailpertanyaan" id="detailpertanyaan<?php echo $soal->id; ?>">
						 
						 
						 
						 
						 
												<h3 id="titleujian<?php echo $soal->id; ?>">
												  
												 	  <span class="badge badge-sm bg-gradient-success" style="font-size:20px">PERTANYAAN KE - <?php echo $soal->urutan; ?>  </span> </h3>
													   <div class="notbarpertanyaan">
													Nomor Soal 
												   <div class="input-group input-group-outline my-3">
														   <input type="text" placeholder="Nomor  Soal" class="form-control  ketiksoal" onkeypress="return formatnilai(this);" trsoal_id="<?php echo $soal->id; ?>" kolom="urutan"   value="<?php echo $soal->urutan; ?>">  
														</div>
												</div>
												
												<div class="notbarpertanyaan">
													Jenis Soal 
													<div class="input-group input-group-outline my-3">
														  <select class="form-control jenissoal" trsoal_id="<?php echo $soal->id; ?>">
														  <option value=""> - Pilih Jenis Soal - </option>
														    <option value="1" <?php echo ($soal->jenis==1) ? "selected":""; ?>> Pilihan Ganda (Multiple Choice) </option>
															<option value="9" <?php echo ($soal->jenis==9) ? "selected":""; ?>> Pilihan Ganda (Scoring) </option>
															<option value="2" <?php echo ($soal->jenis==2) ? "selected":""; ?>> Salah Benar (True/False) </option>
															<option value="3" <?php echo ($soal->jenis==3) ? "selected":""; ?>> Short Answer </option>
															<option value="4" <?php echo ($soal->jenis==4) ? "selected":""; ?> > Menjodohkan (Matching) </option>
															<option value="5" <?php echo ($soal->jenis==5) ? "selected":""; ?>> Essay  </option>
														    <option value="6" <?php echo ($soal->jenis==6) ? "selected":""; ?>> Pilihan  Multiple Answer </option> 
														    <option value="7" <?php echo ($soal->jenis==7) ? "selected":""; ?>> Checked Box </option> 
															<option value="8" <?php echo ($soal->jenis==8) ? "selected":""; ?>> Tebakan  </option>
														    
														  </select>
														</div> 
														
														
														Pertanyaan
														<div class="input-group input-group-outline my-3">
														    
															 <textarea id="editor<?php echo $soal->id; ?>"   trsoal_id="<?php echo $soal->id; ?>" kolom="soal"></textarea>
															 <script type="text/javascript">
																
																
																 var editor = CKEDITOR.replace( 'editor<?php echo $soal->id; ?>', {
		
																			
																			  uploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json&show=1',
																			  filebrowserBrowseUrl: base_url+'__statics/ckdrive/ckfinder.html',
																			  filebrowserImageBrowseUrl: base_url+'__statics/ckdrive/ckfinder.html?type=Images&show=1',
																			  filebrowserUploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Files&show=1',
																			  filebrowserImageUploadUrl: base_url+'__statics/ckdrive/core/connector/php/connector.php?command=QuickUpload&type=Images&show=1',
																			  enterMode : CKEDITOR.ENTER_BR,
                                                                              shiftEnterMode: CKEDITOR.ENTER_P,
																			  height:300,
																			  removeDialogTabs: 'image:advanced;link:advanced',
																			   allowedContent :true
																} );
																editor.on('instanceReady', function(evt) {
																		this.dataProcessor.htmlFilter.addRules({
																			elements: {
																				img: function(element) {
																					if (!element.hasClass('img-responsive')) {
																						element.addClass('img-responsive');
																					}
																				}
																			}
																		});
																	});

																															
																editor.on('change',function(){
																	cek_opsi("<?php echo $soal->id; ?>");
																	
																});														
																															
																
																 </script>
														    
														     
														</div>
														<?php 
														if($soal->urutan==1){
														?>
														<div style="float:right">
														
														
														
													    </div>
														<?php
														  }else{
															 ?>
															  <div style="float:right">
															  <button class="btn btn-sm btn-danger hapus_soal" trsoal_id="<?php echo $soal->id; ?>"><i class="la la-trash-o"></i> Hapus Soal  </button>
															  </div>

															 <?php 
														  }							
														  ?>
															 
													</div><!--notbar end-->
													
													<div id="opsisoal<?php echo $soal->id; ?>">
													
													   
													
										           </div>
												   
												   
			</div><!--save-stngs end-->
			</div><!--save-stngs end-->
										  
										  
										  
				<?php 
	 }else{
		 
		 
		  $this->load->view("teacherujian/pertanyaanedit");
	 }

?>	 






															 
															 
<script type="text/javascript">
   $(document).off("change",".jenissoal").on("change",".jenissoal",function(){
	      var trsoal_id = $(this).attr("trsoal_id");
	      var jenis     = $(this).val();
		  if(jenis =="") {  $("#opsisoal"+trsoal_id).html(""); } 
	      $.post("<?php echo site_url("teacherujian/jenissoal"); ?>",{trsoal_id:trsoal_id,jenis:jenis},function(data){
			  
			  $("#opsisoal"+trsoal_id).html(data);
			  
			  
		  })
	   
	   
    });
	
	
   $(document).off("click",".banksoal").on("click",".banksoal",function(){
	      var tmujian_id = $(this).attr("tmujian_id");
	      $.post("<?php echo site_url("teacherujian/banksoal"); ?>",{tmujian_id:tmujian_id},function(){
			  
			  
			  
		  })
	   
	   
    });


   $(document).off("click",".loadsoal").on("click",".loadsoal",function(){
	      var tmujian_id = $(this).attr("tmujian_id");
	      $.post("<?php echo site_url("teacherujian/loadsoal"); ?>",{tmujian_id:tmujian_id},function(data){
			  
			    $("#loadingbanksoal").html(data);
			   
		  })
	   
	   
    });
	
	function cek_opsi(soal){
																	var nilai = CKEDITOR.instances['editor'+soal+''].getData();
																	var trsoal_id  = $("#editor"+soal+"").attr("trsoal_id");
																	var kolom      = $("#editor"+soal+"").attr("kolom");
																		 $("#statussoal").html("<i class='fas fa-cog fa-spin'></i>    Sedang menyimpan..");
																			 $.post("<?php echo site_url("teacherujian/simpan_soal"); ?>",{trsoal_id:trsoal_id,kolom:kolom,nilai:nilai},function(){
																					 $("#statussoal").html("Tersimpan");
																			 })
     }



</script>
										