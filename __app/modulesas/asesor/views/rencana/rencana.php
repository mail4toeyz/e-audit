<?php $status = $this->db->query("select status from pengaturan where tahun='{$data->tahun}'")->row(); $disabled = ($status->status==1) ? "disabled":"";  ?>
<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<!--begin::Card-->
								<div class="card card-custom">
									<div class="card-header">
										<div class="card-title">
											<h3 class="card-label">Bimbingan Penyusunan Rencana Pemanfaatan Dana Bantuan  <b><?php echo $data->nama; ?> (<?php echo $data->nsm; ?>)</b>
											</h3>
										</div>
										<div class="card-toolbar">
											<a href="<?php echo site_url("asesor/rencana"); ?>" class="menuajax btn btn-light-primary font-weight-bolder mr-2" id="cancel">
											<i class="fa fa-arrow-circle-left icon-xs"></i>Kembali </a>
									
										</div>
									</div>
									<div class="card-body">
                              <?php 
                                    $edm       = $this->db->query("select id from visitasi_edm where madrasah_id='{$data->id}'  AND nilai_kedisiplinan IS NOT NULL AND nilai_pengembangan_diri IS NOT NULL AND nilai_sarana_prasarana IS NOT NULL  AND nilai_pembiayaan IS NOT NULL AND nilai_proses_pembelajaran IS NOT NULL LIMIT 1")->num_rows();
                                    $pd        = $this->db->query("select id from visitasi_pesertadidik where madrasah_id='{$data->id}'  AND total !=0 LIMIT 1")->num_rows();
                                    $rombel    = $this->db->query("select id from visitasi_rombel where  madrasah_id='{$data->id}'  AND total !=0 LIMIT 1")->num_rows();
                                    $guru      = $this->db->query("select id from visitasi_guru where madrasah_id='{$data->id}'   LIMIT 1")->num_rows();
                                    $ruangbelajar   = $this->db->query("select id from visitasi_ruangbelajar where madrasah_id='{$data->id}'  AND total !=0 LIMIT 1")->num_rows();
                                    $toilet         = $this->db->query("select id from visitasi_toilet where madrasah_id='{$data->id}'   LIMIT 1")->num_rows();
                                    $tamuk          = $this->db->query("select id from visitasi_tatapmuka where madrasah_id='{$data->id}'   LIMIT 1")->num_rows();

                                    $validasi       = $edm+ $pd + $rombel +$guru +$ruangbelajar+$toilet+$tamuk;

                                    $dokumentasi    = $this->db->query("select id from tr_persyaratan where madrasah_id='{$data->id}'   LIMIT 1")->num_rows();


                                    if($validasi < 7 ){
                                       ?>
                                         <div class="alert alert-danger">
                                         Penyusunan Rencana Pemanfaatan Dana Bantuan dapat dilakukan setelah Anda mengisi hasil visitasi secara lengkap, silahkan lengkapi terlebih dahulu hasil visitasi Anda
                                         </div>

                                       <?php
                                    }else if($dokumentasi ==0){
                                       ?>
                                       <div class="alert alert-danger">
                                       Dokumentasi belum diunggah, silahkan unggah dulu foto/video dokumentasi visitasi 
                                       </div>

                                     <?php
                                    }else{
                               ?>
                                 
                                   <div class="alert alert-primary">
                                   Bimbinglah Tim Inti Madrasah dalam melakukan kegiatan berikut.
                                   <ol start="1">
                                       <li> Gabungkan kegiatan dari kegiatan C dan D pada dokumen panduan, sehingga menghasilkan urutan prioritas sesuai kebutuhan nyata. </li>
                                       <li> Perkirakan volume pekerjaan, satuan, harga satuan, dan total biaya. </li>
                                       <li> Pastikan total biaya tidak melebihi Rp. 150 juta. </li>
                                       <li> Pastikan kegiatan tersebut belum didanai oleh sumber lain. </li>
                                    </ol>
                                    </div>
                                 

                                      <div class="table-responsive">
                                          <table class="table table-hover table-bordered table-striped">
                                              <thead>
                                                <tr>
                                                   <th>NO</th>
                                                   <th>KEGIATAN</th>
                                                   <th>VOLUME</th>
                                                   <th>SATUAN</th>
                                                   <th>HARGA SATUAN</th>
                                                   <th>JUMLAH BIAYA</th>
                                                   <th>AKSI </th>
                                                </tr>
                                              </thead>

                                              <thead>
                                                <tr>
                                                   <th>#</th>
                                                   <th>

                                                      <select class="form-control" id="kegiatan">
                                                         <option value="">- Pilih Kegiatan -</option>

                                                         <?php 
                                                            $masterkegiatan = $this->db->query("select * from visitasi_tatapmuka where madrasah_id='{$data->id}' order by prioritas ASC")->result();
                                                              foreach($masterkegiatan as $rm){

                                                                  ?> <option value="<?php echo $rm->id; ?>"> <?php echo $rm->kegiatan; ?> (<?php echo ($rm->kegiatan_id==0) ? "Rencana":"Kesiapan Tatap Muka"; ?> Prioritas <?php echo $rm->prioritas; ?>) </option> <?php 
                                                              }
                                                         ?>

                                                      </select>

                                                   </th>
                                                   <th><input type="number" class="form-control" id="volume"></th>
                                                   <th><input type="text" class="form-control" id="satuan"></th>
                                                   <th><input type="text" class="form-control" id="harga" onkeyup="return FormatCurrency(this)" ></th>
                                                   <th><input type="text" class="form-control" id="biaya" onkeyup="return FormatCurrency(this)" ></th>
                                                   <th><button type="button" class="btn btn-primary btn-sm" id="tambahrencana2" <?php echo $disabled; ?>> <i class="fa fa-plus"></i> </button></th>
                                                </tr>
                                              </thead>
                                              <tbody id="loadbodyrencana">

                                                 <?php 
                                                   $kegiatan = $this->db->get_where("visitasi_kegiatan",array("madrasah_id"=>$data->id))->result();
                                                   $no=1;   
                                                   $total = 0;
                                                   foreach($kegiatan as $rk){
                                                      $total = $total + $rk->biaya;

                                                         ?>

                                                                <tr>
                                                                  <td><?php echo $no++; ?></td>
                                                                  <td><?php echo $rk->kegiatan; ?></td>
                                                                  <td><?php echo $rk->volume; ?></td>
                                                                  <td><?php echo $rk->satuan; ?></td>
                                                                  <td><?php echo $this->Reff->formatuang2($rk->harga); ?></td>
                                                                  <td><?php echo $this->Reff->formatuang2($rk->biaya); ?></td>
                                                                  
                                                                  <td><button type="button" class="btn btn-primary btn-sm hapusrencana" <?php echo $disabled; ?> data-madrasah_id="<?php echo $rk->madrasah_id; ?>" data-id="<?php echo $rk->id; ?>">  <i class="fa fa-trash"></i> </button></td>
                                                               </tr>




                                                         <?php



                                                      }
                                                   ?>

                                                   <tr style="font-weight:bold">
                                                      <td colspan="5"> Total Kegiatan </td>
                                                      <td> <?php echo $this->Reff->formatuang2($total); ?> </td>
                                                      <td> - </td>
                                                   </tr>


                                             </tbody>



                                        </table>
                                    </div>
                              <?php 
                                    }
                              ?>

                                        </div>

                                    
                                     </div>
										
									</div>
								</div>
								<!--end::Card-->
								<!--begin: Code-->
								
								<!--end: Code-->
							</div>
							<!--end::Container-->
						</div>

<script type="text/javascript">

  $(document).off("click","#tambahrencana").on("click","#tambahrencana",function(){

  var kegiatan= $("#kegiatan").val();
  var volume= $("#volume").val();
  var satuan= $("#satuan").val();
  var harga= $("#harga").val();
  var biaya= $("#biaya").val();
  var madrasah_id = "<?php echo $data->id; ?>";
 
   if(kegiatan=="" && biaya==""  && harga==""){ return false; }
    
     $.post("<?php echo site_url('asesor/kegiatan_save'); ?>",{kegiatan:kegiatan,volume:volume,satuan:satuan,harga:harga,biaya:biaya,madrasah_id:madrasah_id},function(data){

      $("#loadbodyrencana").html(data);
      $("#kegiatan").val("");
      $("#volume").val("");
      $("#satuan").val("");
      $("#harga").val("");
      $("#biaya").val("");

     });


  });

$(document).off("click",".hapusrencana").on("click",".hapusrencana",function(){

var madrasah_id =  $(this).data("madrasah_id")
var id   =  $(this).data("id")

  
   $.post("<?php echo site_url('asesor/kegiatan_hapus'); ?>",{id:id,madrasah_id:madrasah_id},function(data){

    $("#loadbodyrencana").html(data);

   });


});
$(document).off("input","#harga").on("input","#harga",function(){

     
  var harga = HapusTitik($(this).val());
  var volume = $("#volume").val();
  var total = TambahTitik(volume * harga)
   $("#biaya").val(total);






});

</script>