

<div class="d-flex flex-column-fluid">
							
<div class="container">
	
					

<div class="col-lg-12 col-xxl-12">
    <div class="alert alert-primary">
Selamat Datang <b> <?php echo $_SESSION['nama']; ?> </b>, Anda login sebagai <b>Asesor <?php echo $this->Reff->get_kondisi(array("id"=>$data->jabatan_provinsi_id),"provinsi","nama"); ?> </b>   
</div>

<center>

<a href="javascript:void(0)" id="tarikJadwalVisitasi"  class="btn btn-danger btn-sm"> <i class="fa fa-refresh" aria-hidden="true"></i>
  Tarik Jadwal dan Data Visitasi  </a>
</center>
<br>
 <div class="alert alert-danger" style="font-size:20px">
<h2> PENGUMUMAN </h2>
Proses Visitasi  dinyatakan selesai ketika Anda sudah mengisi hasil  visitasi dan mengunggah semua dokumentasi visitasi termasuk mengunggah Berita Acara <br>
Klaim di AMAN hanya dapat dilakukan apabila visitasi dinyatakan selesai.
	
	</div> 
  <?php 
  $tahun = $this->Reff->tahun();
  //$tahun   = 2020;
  $madrasah  = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and tahun='{$tahun}' AND nsm IN(select nsm from visitasi_jadwal where nik='{$data->nik}')")->row();
  $visitasi  = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and  tahun='{$tahun}' and nsm IN(select nsm from visitasi_jadwal where nik='{$data->nik}') ")->row();
  
  $sudahvisitasi  = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1  and tahun='{$tahun}' AND nsm IN(select nsm from visitasi_jadwal where nik='{$data->nik}') AND id  IN(select madrasah_id from tr_persyaratan where persyaratan_id IN(5))")->row();
  $belumvisitasi  = $visitasi->jml - $sudahvisitasi->jml;

  


                             
   ?>
  <div class="row">
                  <div class="col-xl-4">
										<!--begin: Stats Widget 19-->
										<div class="card card-custom bg-primary card-stretch gutter-b rounded-xl ">
											<!--begin::Body-->
											<div class="card-body my-3">
                      <span class="fa fa-bank " style="color:white;font-size:40px"> <?php echo $visitasi->jml; ?>   </span>  <br><br>
												<a href="#"  style="color:white;font-size:20px">Madrasah Visitasi  </a>
												
												
											</div>
											<!--end:: Body-->
										</div>
										<!--end: Stats:Widget 19-->
									</div>

                  <div class="col-xl-4">
										<!--begin: Stats Widget 19-->
										<div class="card card-custom bg-primary card-stretch gutter-b rounded-xl ">
											<!--begin::Body-->
											<div class="card-body my-3">
                      <span class="fa fa-check-square-o " style="color:white;font-size:40px">  <?php echo $sudahvisitasi->jml; ?>   </span>  <br><br>
												<a href="#"  style="color:white;font-size:20px">Sudah Visitasi  </a>
												
												
											</div>
											<!--end:: Body-->
										</div>
										<!--end: Stats:Widget 19-->
									</div>

                  <div class="col-xl-4">
										<!--begin: Stats Widget 19-->
										<div class="card card-custom bg-primary card-stretch gutter-b rounded-xl ">
											<!--begin::Body-->
											<div class="card-body my-3">
                      <span class="fa fa-times " style="color:white;font-size:40px"><?php echo $belumvisitasi; ?>   </span>  <br><br>
												<a href="#"  style="color:white;font-size:20px">Belum Visitasi  </a>
											
												
											</div>
											<!--end:: Body-->
										</div>
										<!--end: Stats:Widget 19-->
									</div>





													
	</div>

  <br>

                    
  <!-- 
  <figure class="highcharts-figure">
    <div id="container"></div>
  
</figure>  -->

<div class="row">
     <div class="col-md-12">
         <div class="table-responsive">
         <h2> Jadwal Visitasi Madrasah Bantuan Kinerja dan Afirmasi </h2>
             <table class="table table-hover table-bordered table-striped">
                
                 <thead>
                     <tr>
                         <th> NSM </th>
                         <th> NAMA MADRASAH </th>
                         <th> JENIS PROGRAM </th>
                         <th> MULAI </th>
                         <th> SELESAI </th>
                     </tr>
                 </thead>
                 <tbody>
                     <?php 
                      $tahun   = $this->Reff->tahun(); 
                      $visitasiJadwal = $this->db->query("select * from visitasi_jadwal where nik='".$data->nik."' and nsm IN(select nsm from madrasahedm where tahun='".$tahun."' and shortlist=1) order by tanggal_mulai ASC,jam_mulai ASC")->result();
                        if(count($visitasiJadwal) >0){
                        foreach($visitasiJadwal as $row){
                            $mulai   = $row->tanggal_mulai." ".$row->jam_mulai;
                            $selesai = $row->tanggal_selesai." ".$row->jam_selesai;

                            ?>
                          <tr>
                              <td><?php echo $row->nsm; ?></td>
                              <td><?php echo $row->nama_madrasah; ?></td>
                              <td><?php echo $row->jenis_program; ?></td>
                              <td><?php echo $this->Reff->formattimestamp($mulai); ?></td>
                              <td><?php echo $this->Reff->formattimestamp($selesai); ?></td>
                         

                         </tr>
                     
                     <?php 

                        }
                    }else{
                        ?>
                        <tr>
                              <td align="center" colspan="5"> Belum ada data jadwal visitasi, untuk menampilkan jadwal visitasi pastikan Anda sudah mengisi jadwal visitasi pada aplikasi AMAN kemudian klik  <a href="javascript:void(0)" id="tarikJadwalVisitasi"  > disini </a> untuk menarik jadwal visitasi</td>
                             
                         

                         </tr>


                        <?php 


                    }
                      ?>
                    
                 </tbody>
             </table>
         </div>
     </div>

</div>


              
              
         
              
              
              
              


<?php 
//  $edm       = $this->db->query("select distinct(madrasah_id) from visitasi_edm where  nilai_pembiayaan IS NOT NULL AND nilai_proses_pembelajaran IS NOT NULL  AND asesor='".$_SESSION['peserta_id']."'")->num_rows();
//  $pd        = $this->db->query("select distinct(madrasah_id) from visitasi_pesertadidik where  total !=0 AND asesor='".$_SESSION['peserta_id']."'")->num_rows();
//  $rombel    = $this->db->query("select distinct(madrasah_id) from visitasi_rombel where  total !=0 AND asesor='".$_SESSION['peserta_id']."'")->num_rows();
//  $guru      = $this->db->query("select distinct(madrasah_id) from visitasi_guru where asesor='".$_SESSION['peserta_id']."' ")->num_rows();
//  $ruangbelajar   = $this->db->query("select distinct(madrasah_id) from visitasi_ruangbelajar where   total !=0 AND asesor='".$_SESSION['peserta_id']."'")->num_rows();
//  $toilet         = $this->db->query("select distinct(madrasah_id) from visitasi_toilet  where asesor='".$_SESSION['peserta_id']."' ")->num_rows();
//  $tamuk          = $this->db->query("select distinct(madrasah_id) from visitasi_tatapmuka where asesor='".$_SESSION['peserta_id']."' ")->num_rows();




?>
        
<script type="text/javascript">
/*
Highcharts.chart('container', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Laporan Visitasi '
    },
    subtitle: {
        text: 'Timeline Progres hasil laporan visitasi '
    },
    xAxis: {
        categories: ['Madrasah'],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah ',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' Madrasah'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: 0,
       
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'SKPM EDM ',
        data: [<?php //echo $edm; ?>]
    }, {
        name: 'Peserta Didik',
        data: [<?php //echo $pd; ?>]
    }, {
        name: 'Rombongan Belajar',
        data: [<?php //echo $rombel; ?>]
    }, {
        name: 'Jumlah Guru',
        data: [<?php //echo $guru; ?>]
    }, {
        name: 'Jumlah Ruang Belajar',
        data: [<?php // echo $ruangbelajar; ?>]
    }, {
        name: 'Jumlah Toilet Berfungsi',
        data: [<?php // echo $toilet; ?>]
    }, {
        name: 'Kesiapan Tatap Muka',
        data: [<?php // echo $tamuk; ?>]
    }]
});

*/

$(document).off("click","#tarikJadwalVisitasi").on("click","#tarikJadwalVisitasi",function(){

    loading();
    $.post("<?php echo site_url("login/cekJadwalVisitasi/".$data->nik.""); ?>",function(data){
        jQuery.unblockUI({ });

        if(data=="sukses"){
            alertify.success("Proses penarikan data berhasil dilakukan");
            location.reload();
        }else{
            alertify.error("Proses penarikan data Gagal dilakukan, Sepertinya Anda belum mengisi jadwal di AMAN ");

        }
      
      
        
    })
});
</script>

              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              </div>


</div>