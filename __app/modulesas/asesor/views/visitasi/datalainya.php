<link rel="stylesheet" type="text/css" href="https://weareoutman.github.io/clockpicker/dist/bootstrap-clockpicker.min.css">
<script type="text/javascript" src="https://weareoutman.github.io/clockpicker/dist/bootstrap-clockpicker.min.js"></script>
<script src="https://cdn.ckeditor.com/4.21.0/standard/ckeditor.js"></script>

<form action="javascript:void(0)" method="post" id="simpannorespon" name="simpannorespon" url="<?php echo site_url("asesor/saveVisitasiCatatan"); ?>">
<input type="hidden" class="form-control" name="f[madrasah_id]" value="<?php echo $data->id; ?>" required>
<input type="hidden" class="form-control" name="f[nsm]" value="<?php echo $data->nsm; ?>" required>
<?php 
 $hasil      = $this->db->query("select * from visitasi_catatan where madrasah_id='{$data->id}'")->row();

 ?>

                                                 <div class="form-group row">
                                                        <div class="col-md-6">
                                                                <label class="col-xl-12  col-form-label">Jam Datang ke Madrasah</label>
                                                                <div class="col-lg-12 col-xl-12">
                                                                <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                                                        <input type="text" class="form-control" name="f[jam_datang]" value="<?php echo isset($hasil->jam_datang) ? $hasil->jam_datang:"08:00"; ?>" required>
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-clock "></span>
                                                                        </span>
                                                                    </div>
                                                                   
                                                                </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                                <label class="col-xl-12  col-form-label">Jam Pulang dari Madrasah </label>
                                                                <div class="col-lg-12 col-xl-12">
                                                                <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                                                        <input type="text" class="form-control"  name="f[jam_pulang]" value="<?php echo isset($hasil->jam_pulang) ? $hasil->jam_pulang:"17:00"; ?>" required>
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-clock "></span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                        </div>

													</div>


                                                    <div class="form-group row">
                                                    <div class="col-md-12">
                                                                <label class="col-xl-12  col-form-label">Catatan atau Komentar Asesor untuk Madrasah ini </label>
                                                                <div class="col-lg-12 col-xl-12">
                                                                <textarea id="editor1" class="form-control" name="f[catatan]" row="5" required placeholder="Bagaimana pelaksanaan Visitasi di Madrasah ini ? masukkan catatan atau komentar selama visitasi di Madrasah ini"><?php echo isset($hasil->catatan) ? $hasil->catatan:""; ?></textarea>
                                                           
                                                                   
                                                                </div>
                                                        </div>
													</div>

                                                    <div class="form-group row">
                                                    <div class="col-md-12">
                                                            <center>
                                                            <button type="submit" class="btn btn-success btn-sm ">
                                                                    <i class="fa fa-save"></i>
                                                                    <span>Simpan Hasil Visitasi  </span>
                                                                </button>
                                                            </center>
                                                        </div>
													</div>
													
 <script type="text/javascript">
$('.clockpicker').clockpicker({
    placement: 'top',
    align: 'left',
    donetext: 'Done'
});

</script>	

