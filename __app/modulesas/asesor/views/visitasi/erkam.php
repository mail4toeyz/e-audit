<?php $status = $this->db->query("select status from pengaturan where tahun='{$data->tahun}'")->row(); $disabled = ($status->status==1) ? "disabled":"";  ?>
<div class="row">

   <div class="col-xl-12">
   <div class="alert alert-default"> Identifikasi capaian indikator yang rendah <br> 
Tentukan bersama kegiatan yang dapat meningkatkan capaian indikator yang rendah tersebut dan belum didanai oleh sumber lain  <b><?php echo $data->nama; ?> (<?php echo $data->nsm; ?>)</b> </div>

   <div class="table-responsive">
                                          <table class="table table-hover table-bordered table-striped">
                                              <thead>
                                                <tr>
                                                   <th>NO</th>
                                                   <th>KEGIATAN</th>
                                                   <th>PRIORITAS</th>
                                                   
                                                </tr>
                                              </thead>

                                              <thead>
                                                <tr>
                                                   <th>#</th>
                                                   <th><input type="text" class="form-control" id="kegiatan"></th>
                                                   <th><input type="number" class="form-control" id="prioritas"></th>
                                                   
                                                   <th><button type="button" class="btn btn-primary btn-sm" id="tambahrencana" <?php echo $disabled; ?>> <i class="fa fa-plus"></i> </button></th>
                                                </tr>
                                              </thead>
                                              <tbody id="loadbodyrencana">

                                                 <?php 
                                                   $kegiatan = $this->db->get_where("visitasi_tatapmuka",array("madrasah_id"=>$data->id,"kegiatan_id"=>0))->result();
                                                   $no=1;   
                                                   foreach($kegiatan as $rk){

                                                         ?>

                                                                <tr>
                                                                  <td><?php echo $no++; ?></td>
                                                                  <td><?php echo $rk->kegiatan; ?></td>
                                                                  <td><?php echo $rk->prioritas; ?></td>
                                                                
                                                                  
                                                                  <td><button type="button" class="btn btn-primary btn-sm hapusrencana" <?php echo $disabled; ?> data-madrasah_id="<?php echo $rk->madrasah_id; ?>" data-id="<?php echo $rk->id; ?>">  <i class="fa fa-trash"></i> </button></td>
                                                               </tr>




                                                         <?php



                                                      }
                                                   ?>


                                             </tbody>

                                        </table>
                                    </div>
   </div>

   
</div>

<script type="text/javascript">

  $(document).off("click","#tambahrencana").on("click","#tambahrencana",function(){

  var kegiatan= $("#kegiatan").val();
  var prioritas= $("#prioritas").val();
  
  var madrasah_id = "<?php echo $data->id; ?>";
  var nsm         = "<?php echo $data->nsm; ?>";
 
   if(kegiatan==""){ return false; }
    
     $.post("<?php echo site_url('asesor/erkam_save'); ?>",{kegiatan:kegiatan,prioritas:prioritas,madrasah_id:madrasah_id,nsm:nsm},function(data){

      $("#loadbodyrencana").html(data);
      $("#kegiatan").val("");
      $("#prioritas").val("");
      

     });


  });

$(document).off("click",".hapusrencana").on("click",".hapusrencana",function(){

var madrasah_id =  $(this).data("madrasah_id")
var id   =  $(this).data("id")

  
   $.post("<?php echo site_url('asesor/erkam_hapus'); ?>",{id:id,madrasah_id:madrasah_id},function(data){

    $("#loadbodyrencana").html(data);

   });


});

</script>