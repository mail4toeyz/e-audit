<?php $status = $this->db->query("select status from pengaturan where tahun='{$data->tahun}'")->row(); $disabled = ($status->status==1) ? "disabled":"";  ?>
<div class="alert alert-default"> Jumlah Rombongan Belajar <b><?php echo $data->nama; ?> (<?php echo $data->nsm; ?>)</b> </div>
<div class="row">


<div class="col-xl-12">
       
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-striped">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>KELAS</th>
                    <th>JUMLAH ROMBONGAN BELAJAR (ROMBEL)</th>                    
                  </tr>
                
                </thead>
                <tbody>
                    <?php 
                     $tahun = $this->Reff->tahun();
                     $no=1;
                     foreach($this->db->get_where("tm_kelas",array("jenjang"=>$data->jenjang))->result() as $row){
                      $hasil      = $this->db->query("select * from visitasi_rombel where madrasah_id='{$data->id}'")->row();
                      $kolom      = "kelas_".$row->id;
                      $nilainya   = isset($hasil->$kolom) ? $hasil->$kolom :0;

                      ?>
                       <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $row->nama; ?> (<?php echo $row->satuan; ?>)</td>                       
                        <td><input type="text" onkeypress="return isNumber(event)" class="form-control inputrombel" <?php echo $disabled; ?> data-kolom="<?php echo $kolom; ?>" data-madrasah_id="<?php echo $data->id; ?>" data-nsm="<?php echo $data->nsm; ?>" value="<?php echo $nilainya; ?>"></td>
                       </tr>
                     

                      <?php 

                     }
                     ?>

                   

                </tbody>
            </table>

        </div>
   </div>

  </div>


<script type="text/javascript">

  $(document).off("input",".inputrombel").on("input",".inputrombel",function(){
    var kolom       = $(this).data("kolom");
    var madrasah_id = $(this).data("madrasah_id");
    var nsm         = $(this).data("nsm");
    var eksis       = "<?php echo $data->jumlah_rombel; ?>";
    var nilai       = $(this).val();
 
    $.post("<?php echo site_url('asesor/saveVisitasirombel'); ?>",{kolom:kolom,madrasah_id:madrasah_id,nsm:nsm,eksis:eksis,nilai:nilai},function(data){

      
     

    })



  });


  


</script>