<?php $status = $this->db->query("select status from pengaturan where tahun='{$data->tahun}'")->row(); 
$disabled = ($status->status==1) ? "disabled":""; 
 ?>
<div class="row">

   <div class="col-xl-12">
   <div class="alert alert-default"> Kutip Hasil Visitasi Skor Kinerja Pencapaian Mutu (SKPM) yang dihasilkan aplikasi EDM-eRKAM <b><?php echo $data->nama; ?> (<?php echo $data->nsm; ?>)</b> </div>
   <a href="javascript:void(0)" style="float:right;display:none" id="tarikJadwalVisitasi"  class="btn btn-danger btn-sm"> <i class="fa fa-refresh" aria-hidden="true"></i>
  Tarik Data EDM  </a>
  <br>
  <br>

  
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-striped">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>ASPEK</th>
                    <th>SKOR KINERJA PENCAPAIAN MUTU</th>
                  
                </tr>
                
                </thead>
                <tbody>
                    <?php 
                     $tahun = $this->Reff->tahun();
                     foreach($this->db->get("edm_aspek")->result() as $row){
                         $eksis      = number_format($this->Reff->get_kondisi(array("id"=>$data->id,"tahun"=>$tahun),"madrasahedm",$row->kolom),2);
                         $hasil      = $this->db->query("select * from visitasi_edm where madrasah_id='{$data->id}'")->row();
                         $kolom      = $row->kolom;
                         $nilainya   = isset($hasil->$kolom) ? $hasil->$kolom :0;
                         $persentase = isset($hasil->$kolom) ? number_format(($hasil->$kolom/$eksis) * 100,2) :0;
                      ?>
                       <tr>
                        <td><?php echo $row->id; ?></td>
                        <td><?php echo $row->nama; ?></td>
                        
                        <td><input type="text" onkeypress="return isNumber(event)" <?php echo $disabled; ?> class="form-control inputvisitasi" data-kolom="<?php echo $row->kolom; ?>" data-madrasah_id="<?php echo $data->id; ?>" data-nsm="<?php echo $data->nsm; ?>" data-eksis="<?php echo $eksis; ?>" value="<?php echo $nilainya; ?>"></td>
                       
                     </tr>
                     

                      <?php 

                     }
                     ?>

                </tbody>
            </table>

        </div>
   </div>

   
</div>

<script type="text/javascript">
  $(document).off("input",".inputvisitasi").on("input",".inputvisitasi",function(){
    var kolom       = $(this).data("kolom");
    var madrasah_id = $(this).data("madrasah_id");
    var nsm         = $(this).data("nsm");
    var eksis       = $(this).data("eksis");
    var nilai       = $(this).val();
 
    $.post("<?php echo site_url('asesor/saveVisitasiEDM'); ?>",{kolom:kolom,madrasah_id:madrasah_id,nsm:nsm,eksis:eksis,nilai:nilai},function(data){

      

    })



  })
</script>