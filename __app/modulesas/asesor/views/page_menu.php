<?php 
   $uri = $this->uri->segment(1);
   $uri2 = $this->uri->segment(2);
   $uri4 = ($this->uri->segment(4));
 
   
     
  ?>
<div class="aside aside-left  aside-fixed" id="kt_aside">
				
				
					<!--end::Primary-->
					<span class="aside-toggle btn btn-icon btn-primary btn-hover-primary shadow-sm" id="kt_aside_toggle" data-toggle="tooltip" data-placement="right" data-container="body" data-boundary="window" title="Toggle Aside">
								<i class="fa fa-arrow-left icon-sm"></i>
							</span>
					<!--begin::Secondary-->
					<div class="aside-secondary d-flex ">
						<!--begin::Workspace-->
						<div class="aside-workspace scroll scroll-push my-2">
							<!--begin::Tab Content-->
							<div class="tab-content">
								<!--begin::Tab Pane-->
							
								<div class="tab-pane p-3 px-lg-7 py-lg-5 fade show active" id="kt_aside_tab_2">
									<!--begin::Aside Menu-->
									<div class="aside-menu-wrapper flex-column-fluid px-3 px-lg-10 py-5" id="kt_aside_menu_wrapper">
										<!--begin::Menu Container-->
										<?php 
										$data = $this->db->get_where("asesor",array("id"=>$_SESSION['peserta_id']))->row();
										?>
										<div id="kt_aside_menu" class="aside-menu min-h-lg-800px" data-menu-vertical="1" data-menu-scroll="1">
										 <center><img alt="Logo" src="<?php echo base_url(); ?>__statics/upload/foto/<?php echo $data->foto; ?>" onError="this.onerror=null;this.src='<?php echo base_url(); ?>__statics/img/not.png';" class="max-h-70px" style="border-radius: 50%;" />  <br>
										  <br>
										   <h3><b><?php echo $data->nama; ?> </h3>
										    ASESOR <?php echo $this->Reff->get_kondisi(array("id"=>$data->jabatan_provinsi_id),"provinsi","nama"); ?>   </b><br>
										</center>
											<!--begin::Menu Nav-->
											<ul class="menu-nav">
												

												<li class="menu-item menu-item-active" aria-haspopup="true" data-menu-toggle="hover">
													<a href="<?php echo site_url("asesor"); ?>" title="Dashboard " class="menu-link menuajax">
                                                      <i class="fa fa-laptop"></i>
														<span class="menu-text" style="padding-left:7px;padding-bottom:7px"> Dashboard   </span>
													
													</a>
													
												</li>
												

                                                
											<!--	<li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">
													<a href="<?php echo base_url(); ?>panduanvisitasi.pdf" title="Panduan Visitasi " class="menu-link" target="_blank">
                                                      <i class="fa fa-file-pdf-o"></i>
														<span class="menu-text" style="padding-left:7px;padding-bottom:7px"> Panduan Visitasi     </span>
													
													</a>
													
												</li>
-->

                                               

												<li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">
													<a href="<?php echo site_url("asesor/profil"); ?>" title="Master Data Madrasah " class="menu-link menuajax">
                                                      <i class="fa fa-user"></i>
														<span class="menu-text" style="padding-left:7px;padding-bottom:7px"> Profil Asesor     </span>
													
													</a>
													
												</li>

												<!--<li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">
													<a href="<?php echo site_url("asesor/surtug"); ?>" title="Upload Surat Tugas" class="menu-link menuajax">
                                                      <i class="fa fa-upload"></i>
														<span class="menu-text" style="padding-left:7px;padding-bottom:7px"> Upload Surat Tugas     </span>
													
													</a>
													
												</li> -->


												<li class="menu-section">
													<h4 class="menu-text">Menu Utama  </h4>
													<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
												</li>


												<li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">
												   <a href="<?php echo site_url("asesor/visitasi"); ?>" title="Visitasi Madrasah  " class="menu-link menuajax">
                                                      <i class="fa fa-check-square-o"></i>
														<span class="menu-text" style="padding-left:7px;padding-bottom:7px">Visitasi Madrasah     </span>
													
													</a>
													
												</li>

												<li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">
												   <a href="<?php echo site_url("asesor/dokumentasi"); ?>" title="Dokumentasi Visitasi  " class="menu-link menuajax">
                                                      <i class="fa fa-camera"></i>
														<span class="menu-text" style="padding-left:7px;padding-bottom:7px">Dokumentasi Visitasi      </span>
													
													</a>
													
												</li>

												<li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">
												   <a href="<?php echo site_url("asesor/komentar"); ?>" title="Catatan Umum  " class="menu-link ">
                                                      <i class="fa fa-comment"></i>
														<span class="menu-text" style="padding-left:7px;padding-bottom:7px">Catatan Umum     </span>
													
													</a>
													
												</li>

												<!-- <li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">
												   <a href="<?php echo site_url("asesor/rencana"); ?>" title="Rencana  Pemanfaatan Dana BKBA " class="menu-link menuajax">
                                                      <i class="fa fa-file"></i>
														<span class="menu-text" style="padding-left:7px;padding-bottom:7px">Rencana  Pemanfaatan Dana BKBA      </span>
													
													</a>
													
												</li> -->


												<li class="menu-section">
													<h4 class="menu-text">Menu Pendukung  </h4>
													<i class="menu-icon ki ki-bold-more-hor icon-md"></i>
												</li>


												<li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">
												   <a href="<?php echo site_url("asesor/akun"); ?>" title="Perbaharui Akun Asesor " class="menu-link menuajax">
                                                      <i class="fa fa-lock"></i>
														<span class="menu-text" style="padding-left:7px;padding-bottom:7px">Perbaharui Akun     </span>
													
													</a>
													
												</li>
												<li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">
													<a href="javascript:;" class="menu-link" id="keluar">
                                                      <i class="fa fa-sign-out"></i>
														<span class="menu-text" style="padding-left:7px;padding-bottom:7px">Keluar Aplikasi   </span>
													
													</a>
													
												</li>


				

            
 
        

                      


											</ul>
											
										</div>
									
									</div>
								
								</div>
							
							</div>
							
						</div>
					
					</div>
			
				</div>