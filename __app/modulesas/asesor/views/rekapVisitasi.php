
<style>

.table-scroll {
    position: relative;
    width:100%;
    z-index: 1;
    margin: auto;
    overflow: scroll;
    height: 90vh;
}
.table-scroll table {
    width: 100%;
    min-width: 100px;
    margin: auto;
    border-collapse: separate;
    border-spacing: 0;
}
.table-wrap {
    position: relative;
}
.table-scroll th,
.table-scroll td {
    padding: 5px 10px;
    border: 1px solid #000;
    
    vertical-align: top;
    text-align: center;
}
.table-scroll thead th {
    background: #333;
    color: #fff;
    position: -webkit-sticky;
    position: sticky;
    top: 0;
}
th.sticky {
    position: -webkit-sticky;
    position: sticky;
    left: 0;
    z-index: 2;
    background: #ccc;
}
thead th.sticky {
    z-index: 5;
}
.table-scroll thead tr:nth-child(2) th {
    top: 30px;
}

						</style>

<button onclick="btn_export()" class="btn btn-default">
    Cetak Excel 
  </button>
<div class="table-responsive table-scroll ">
                              
                              <table class="table table-condensed table-bordered table-hover  " id="datatableTable" >
                                  <thead>
                                      <tr>
                                          <th rowspan="2"> NO </th>
                                          <th rowspan="2"> # </th>
                                          <th rowspan="2"> PROVINSI/KABKOTA </th>
                                          <th colspan="2"> Longlist </th>
                                          <th colspan="2">  Bantuan Kinerja  </th>
                                          <th colspan="2">  Bantuan 	Afirmasi  </th>
                                          
                                  </tr>
                                  <tr>

                                    <th> Calon </th>
                                    <th> Longlist </th>

                                    <th> Kuota </th>
                                    <th> Shortlist </th>


                                  
                                    <th> Kuota </th>
                                    <th> Shortlist </th>

                                  </tr>
                      </thead>
                          <tbody>

                              <?php 
                              $kinerjakuota=0;
                              $afirmasikuota=0;
                              $provinsi = $this->db->query("select * from provinsi  where (kuota_kinerja+kuota_afirmasi) !=0")->result();
                              $no_prov=1;
                              $tahun    = $this->Reff->tahun();

                              $totalcalon=0;
                              $totallonglist=0;
                              
                              $kinerjashort=0;
                             
                              $afirmasishort=0;
                               foreach($provinsi as $prov){
                                $kinerjakuota = $kinerjakuota + $prov->kuota_kinerja;
                                $afirmasikuota = $afirmasikuota + $prov->kuota_afirmasi;

                                   $calon    = $this->db->query("select count(id) as jml from madrasahedm where blacklist=0 and provinsi_id='{$prov->id}' and tahun='{$tahun}'")->row();
                                   $longlist    = $this->db->query("select count(id) as jml from madrasahedm where longlist=1 and provinsi_id='{$prov->id}' and tahun='{$tahun}'")->row();
                                   $kinerjashortlist = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and bantuan=1 and provinsi_id='{$prov->id}' and tahun='{$tahun}'")->row();
                                   $afirmasishortlist = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and bantuan=2 and provinsi_id='{$prov->id}' and tahun='{$tahun}'")->row();
                                  ?>

                                  <tr style="font-weight:bold">
                                          <th> <?php echo $prov->id; ?> </th>
                                          <th> # </th>
                                          <th> <?php echo $prov->nama; ?> </th>
                                          <th> <?php echo $calon->jml; ?>  </th>
                                          <th> <?php echo $longlist->jml; ?>  </th>
                                          <th> <?php echo $prov->kuota_kinerja; ?>  </th>
                                          <th> <?php echo $kinerjashortlist->jml; ?>  </th>
                                          <th> <?php echo $prov->kuota_afirmasi; ?>   </th>
                                          <th> <?php echo $afirmasishortlist->jml; ?>   </th>
                                          
                                  </tr>

                                  <?php 

                               
                              $kota = $this->db->query(" select * from kota where  provinsi_id='{$prov->id}'")->result();
                          
                              
                               $nokot=1;
                               foreach($kota as $rkot){
                                  $kinerjashortlist = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and bantuan=1 and kota_id='{$rkot->id}' and tahun='{$tahun}'")->row();
                                   $afirmasishortlist = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and bantuan=2 and kota_id='{$rkot->id}' and tahun='{$tahun}'")->row();
                                  
                                   $calon             = $this->db->query("select count(id) as jml from madrasahedm where blacklist=0 and kota_id='{$rkot->id}' and tahun='{$tahun}'")->row();
                                   $longlist          = $this->db->query("select count(id) as jml from madrasahedm where longlist=1 and kota_id='{$rkot->id}' and tahun='{$tahun}'")->row();
                                  

                                

                                   $totalshortlist    = $kinerjashortlist->jml + $afirmasishortlist->jml;
                                  // $this->db->query("UPDATE kota set shortlist_kinerja='".$kinerjashortlist->jml."',shortlist_afirmasi='".$afirmasishortlist->jml."' where id='".$rkot->id."' ");
                                  $totalkuota         = $rkot->kinerja+$rkot->afirmasi;
                                  
                                 

                                 $totalcalon = $totalcalon+$calon->jml;
                                 $totallonglist = $totallonglist+$longlist->jml;
                                
                                 $kinerjashort = $kinerjashort+$kinerjashortlist->jml;
                                
                                
                                 $afirmasishort = $afirmasishort+$afirmasishortlist->jml;
                               ?>

                                 <tr>
                                         <th> # </th>
                                         <th> <?php echo $rkot->id; ?> </th>
                                         <th> <?php echo $rkot->nama; ?> </th>
                                         <th> <?php echo $calon->jml; ?>  </th>
                                         <th> <?php echo $longlist->jml; ?>  </th>
                                         <th> <?php echo $rkot->shortlist_kinerja; ?>  </th>
                                         <th> <?php echo $kinerjashortlist->jml; ?>  </th>
                                         <th> <?php echo $rkot->shortlist_afirmasi; ?>  </th>
                                         <th> <?php echo $afirmasishortlist->jml; ?>   </th>
                                         
                                        
                                 </tr>

                                 <?php 

                              }
                          }


                              ?>


                              

                          </tbody>
                             <tr>
                                         <th> # </th>
                                         <th>#</th>
                                         <th>Total</th>
                                         <th><?php echo $totalcalon; ?></th>
                                         <th><?php echo $totallonglist; ?></th>
                                         <th><?php echo $kinerjakuota; ?></th>
                                         <th><?php echo $kinerjashort; ?></th>
                                         <th><?php echo $afirmasikuota; ?></th>
                                         <th><?php echo $afirmasishort; ?></th>
                                       
                                         
                                        
                                 </tr>

                              </table>

                      </div>


		
				       <div class="card">
						<div class="card-header card-header-success">
						  <h4 class="card-title">Daftar Pendek Madrasah - Shortlist </h4>
						 
						  <p class="card-category">
						  <br>
						  <br>
						  <div class="row">
													
													
						                        <div class="col-md-2">
													  <select class="form-control" id="status">
														   <option value="">- Tampilkan Semua   - </option>
														   <option value="1" selected> Masuk Shortlist </option>
														   <option value="2"> Tidak Masuk Shortlist </option>
														  
														  
													   
													   </select>
												   
													</div>
													<div class="col-md-1">
													  <select class="form-control" id="status_madrasah">
														   <option value="">- Filter  - </option>
														  
														   <option value="1"> Madrasah Inklusi </option>
														   <option value="2"> Penerima Simsarpras </option>
														  
														  
														  
													   
													   </select>
												   
													</div>



													<div class="col-md-2">
													  <select class="form-control" id="jenjang">
														   <option value="">- Jenjang - </option>
														   <?php 
															 $jenjang = array("mi","mts","ma");
															   foreach($jenjang as $r){
																   ?><option value="<?php echo $r; ?>"  > <?php echo strtoupper($r); ?> </option><?php 
																   
															   }
															  ?>
														  
													   
													   </select>
												   
													</div>
													
													<div class="col-md-2">
												
													  <select class="form-control onchange " id="provinsi" target="kota" urlnya="<?php echo site_url('publik/kota'); ?>">
														   <option value="">- Provinsi - </option>
														   <?php 
															   $provinsi = $this->db->query("SELECT * from provinsi where (kuota_kinerja+kuota_afirmasi) !=0")->result();
															   foreach($provinsi as $row){
																   ?><option value="<?php echo strtoupper($row->id); ?>"> <?php echo ($row->nama); ?> </option><?php 
																   
															   }
															  ?>
													   
													   </select>
												   
													</div>

													<div class="col-md-2">
												
													  <select class="form-control kota " id="kota" target="kota" urlnya="<?php echo site_url('publik/kota'); ?>">
														   <option value="">- Kabupaten/Kota - </option>
														  
													   
													   </select>
												   
													</div>
													
													
													<div class="col-md-2">
													  <input class="form-control" id="keyword"  placeholder="Apa yang Anda cari ?">
													  <input class="form-control" type="hidden" id="bantuan" value="<?php echo isset($_GET['bantuan']) ? $_GET['bantuan']:''; ?>">
													
												   
													</div>
													<div class="col-md-1">
													  <button class="btn btn-sm btn-primary" id="tampilkan" > <i class="fa fa-search"></i> </button> 
												   
													</div>
											   
											</div>
						  </p>
						</div>
					  
						 <div class="card-body">
						
						 <button onclick="btn_export()" class="btn btn-default">
    Cetak Excel 
  </button>

							<div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
                                        <tr>
                                            <th width="2px" rowspan="2">NOMOR</th>
                                          
                                           <!--  <th rowspan="2">RANK BK </th>
                                            <th rowspan="2">RANK BA </th> -->
                                            <th rowspan="2">JENJANG </th>
                                            <th rowspan="2">NSM </th>
                                            <th rowspan="2">NAMA </th>
											<th rowspan="2">STATUS </th>
                                            <th rowspan="2">AKREDITASI </th>
                                            <th rowspan="2">ALAMAT </th>
                                            <th rowspan="2">KABKOTA </th>
                                            <th rowspan="2">PROVINSI </th>
                                            
                                            <th rowspan="2">BANTUAN </th>
                                            <th rowspan="2">SKOR </th>
                                            <th colspan="5">RINCIAN SKOR </th>
											<th colspan="11">EMIS </th>
											<th colspan="3">PIP </th>
                                            <th colspan="7">SKOR HASIL EDM </th>
                                            <th colspan="2">eRKAM </th>
											
											
                                            
                                        </tr>

										<tr>

										  <th> EDM </th>
										  <th> PIP </th>
										  <th> Ruang Belajar </th>
										  <th> Toilet </th>
										  <th> Inklusi </th>

										  <th> Jml Siswa </th>
										  <th> Jml Guru  </th>
										  <th> Siswa (L) </th>
										  <th> Siswa (P) </th>
										  <th> ABK </th>
										  <th> Jml Ruang Belajar </th>
										  <th> Ruang Belajar(Ideal) </th>
										  <th> Ruang Belajar(%) </th>
										  <th> Jml Toilet </th>
										  <th> Toilet (Ideal) </th>
										  <th> Toilet (%) </th>
										  <th> Jml Guru </th>


										  <th> Siswa Penerima PIP</th>
										  <th> Persen </th>
										  <th> Skor </th>
										  <th> Kedisiplinan</th>
										  <th> Pengembangan Diri</th>
										  <th> Pembelajaran</th>
										  <th> Sarana & Prasarana</th>
										  <th> Pembiayaan</th>
										  <th> Skor EDM </th>
										  <th> SKPM </th>
										  <th> Rencana Pendapatan </th>											
										  <th> Total Rencana Kegiatan </th>

										 

										</tr>

                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                         </div>
                         </div>
                        
                 
 
 
              
        
<div id="defaultModalData" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       
      </div>
      <div class="modal-body kontenmodalData" >
        <p>Loading...</p>
      </div>
      
    </div>

  </div>
</div>
	
<script src="<?php echo base_url(); ?>__statics/js/excel/xlsx.extendscript.js"></script>
<script src="<?php echo base_url(); ?>__statics/js/excel/xlsx-style/xlsx.full.min.js"></script>

<script src="<?php echo base_url(); ?>__statics/js/excel/export.js"></script>
	
<script type="text/javascript">

function btn_export() {
        var table1 = document.querySelector("#datatableTable");
        var opt = {
            rowIndex: 4
        }; //开头空4行
        var sheet = XLSX2.utils.table_to_sheet(table1, opt);

  
     

        openDownloadDialog(sheet2blob(sheet), ' .Hasil Visitasi.xlsx');
    }

    </script>