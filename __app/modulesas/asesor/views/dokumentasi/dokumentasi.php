<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<!--begin::Card-->
								<div class="card card-custom">
									<div class="card-header">
										<div class="card-title">
											<h3 class="card-label">DOKUMENTASI VISITASI  <b><?php echo $data->nama; ?> (<?php echo $data->nsm; ?>)</b>
											</h3>
										</div>
										<div class="card-toolbar">
											<a href="<?php echo site_url("asesor/dokumentasi"); ?>" class="menuajax btn btn-light-primary font-weight-bolder mr-2" id="cancel">
											<i class="fa fa-arrow-circle-left icon-xs"></i>Kembali </a>
									
										</div>
									</div>
									<div class="card-body">

                 

                        <?php 
                         $toilet         = $this->db->query("select id from visitasi_catatan where madrasah_id='{$data->id}'   LIMIT 1")->num_rows();

                            if($toilet >0){
                          ?>
                                    
                                    <ul class="nav nav-pills" id="myTab1" role="tablist">
                                   <?php 
                                      $posisi = $this->db->query("select * from persyaratan ")->result();
                                      $no=0;
                                        foreach($posisi as $row){
                                            $act="";
                                            $no++;
                                              if($no==1){
                                                  $act="active";
                                              }
                                            ?>

                                        <li class="nav-item <?php echo $act; ?>">
                                        <a class="nav-link <?php echo $act; ?>" id="home-tab-1" data-toggle="tab" href="#sya<?php echo $row->id; ?>">
                                           

                                          <span class="nav-text"><?php echo $row->nama; ?>   </span>
                                        </a>
                                      </li>

                                            <?php 
                                        }
                                    ?>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                    <?php 
                                                  $posisi = $this->db->query("select * from persyaratan ")->result();
                                                  $no=0;
                                        foreach($posisi as $row){
                                            $act="";
                                            $no++;
                                            if($no==1){
                                                  $act="active";
                                              }
                                            ?>
                                        <div class="tab-pane <?php echo $act; ?>" id="sya<?php echo $row->id; ?>" role="tabpanel">
                                        <div class="alert alert-danger"><?php echo $row->keterangan; ?>  -  Max File Upload : 20 MB </div>
                                        
                                        <input id="file<?php echo $row->id; ?>" name="file" type="file" multiple  />

                                        <script type="text/javascript">
                                          
                                        

                                          $(document).ready(function() {

                                        $("#file<?php echo $row->id; ?>").fileinput({
                                                  uploadUrl: "<?php echo site_url("asesor/uploadDok"); ?>",
                                                  overwriteInitial: false,
                                                  
                                                  browseClass: "btn btn-success",
                                                  browseLabel: "<?php echo $row->nama; ?>",
                                                  browseIcon: "<i class=\"fa fa-file-signature\"></i> Ambil File ",
                                                  removeClass: "btn btn-danger",
                                                  removeLabel: "",
                                                  maxFileSize: 24240,
                                                  removeIcon: "<i class=\"fa fa-trash\"></i>  Hapus File",
                                                  uploadClass: "btn btn-info",
                                                  uploadLabel: "",
                                                  uploadIcon: "<i class=\"fa fa-upload\"></i>  Upload Semua File",
                                                  initialPreviewAsData: true,
                                                          uploadExtraData: function() {
                                                              return {
                                                                
                                                                  madrasah_id: "<?php echo $data->id; ?>",
                                                                  persyaratan_id: "<?php echo $row->id; ?>",
                                                              };
                                                          },
                                                          initialPreview: [
                                                              <?php 
                                                                  
                                                                  $eksis = $this->db->query("select id,file from tr_persyaratan where madrasah_id='".$data->id."' and persyaratan_id='".$row->id."' ")->result();
                                                                      $lihat="";
                                                                      if(!is_null($eksis)){
                                                                          foreach($eksis as $r){
                                                                              
                                                                              $lihat .= ",'https://drive.google.com/file/d/".$r->file."/preview'";
                                                                          }
                                                                          
                                                                          
                                                                      }
                                                                  
                                                                  echo substr($lihat,1);
                                                                  
                                                                  
                                                                  ?>
                                                                  
                                                                  
                                                              ],
                                                                      initialPreviewConfig: [

                                                                    <?php 
                                                                      $eksis = $this->db->query("select id,file from tr_persyaratan where madrasah_id='".$data->id."' and persyaratan_id='".$row->id."' ")->result();
                                                                      $lihat="";
                                                                      if(!is_null($eksis)){
                                                                      
                                                                        foreach($eksis as $r){
                                                                        
                                                                          
                                                                          $lihat .= ',{caption: "", size:200, width: "100%", url: "'.site_url("asesor/hapusDok").'", key: "'.$r->id.'"}';
                                                                        }
                                                                        
                                                                        
                                                                      }
                                                                    
                                                                    echo substr($lihat,1);


                                                                    ?>


                                                                        
                                                                      ]



                                                                      
                                                                    });





                                                                      });		
                                                                      

                                                                    </script>
                                                                                
                                                                                
                                                                                </div>


                                                                                <?php 
                                                                                }
                                                                                ?>
                                                                            </div>
                                       



                          <?php
                            }else{

                              ?>
                              <div class="alert alert-danger">
                                 Anda dapat mengunggah dokumentasi visitasi setelah melengkapi hasil visitasi, silahkan lengkapi terlebih dahulu hasil visitasi
                               
                              </div>
                              

                    <?php

                            }
                          ?>

                 





                                   </div>
                            
										
									</div>
								</div>
								<!--end::Card-->
								<!--begin: Code-->
								
								<!--end: Code-->
							</div>
							<!--end::Container-->
						</div>
