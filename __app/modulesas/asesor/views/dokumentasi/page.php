<div class="d-flex flex-column-fluid">
							
<div class="container">

<div class="card">
						
					  
						 <div class="card-body">
							<div class="row">
									
									<div class="col-md-2">
										<select class="form-control" id="jenjang">
											<option value="">- Jenjang - </option>
											<?php 
												$jenjang = array("mi","mts","ma");
												foreach($jenjang as $r){
													?><option value="<?php echo $r; ?>"  > <?php echo strtoupper($r); ?> </option><?php 
													
												}
												?>
											
										
										</select>
									
									</div>
									
									
									
									
									<div class="col-md-4">
										<input class="form-control" id="keyword"  placeholder="Cari NSM atau Nama Madrasah disini">
										<input class="form-control" type="hidden" id="bantuan" value="<?php echo isset($_GET['bantuan']) ? $_GET['bantuan']:''; ?>">
									
									
									</div>
									<div class="col-md-1">
										<button class="btn btn-sm btn-primary" id="tampilkan" > <i class="fa fa-search"></i> </button> 
									
									</div>
								
							</div>
							<hr>
						


							<div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover " id="datatableTable" width="100%" >
                                    <thead class="bg-blue">
                                        <tr>
                                            <th width="2px" rowspan="2">NO</th>
                                            <th rowspan="2">DOKUMENTASI  </th>
                                            <th rowspan="2">BUKTI </th>
                                            <th rowspan="2">JENJANG </th>
                                            <th rowspan="2">NSM </th>
                                            <th rowspan="2">NAMA </th>
                                            <th rowspan="2">AKREDITASI </th>
                                            <th rowspan="2">PROVINSI </th>
                                           <!-- <th rowspan="2">KABKOTA </th> -->
                                            <th rowspan="2">BANTUAN </th>
                                            <th colspan="6">HASIL VISITASI </th>
                                           
											
											
                                            
                                        </tr>

										<tr>

										  <th> SKPM EDM </th>
										  <th> PESERTA DIDIK </th>
										  <th> ROMBONGAN BELAJAR </th>
										  <th> GURU </th>

										  <th> RUANG BELAJAR </th>
										  <th> TOILET </th>
										
										 

										 

										</tr>

                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                         </div>
                         </div>
                         </div>
                         </div>
                 
 
 
              
 
	
<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
                        "scrollY": 500,
                        "scrollX": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[5,10,30, 50,100,200,300,500,1000, 800000000], [5,10,30, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					
                     buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
					
					"ajax":{
						url :"<?php echo site_url("asesor/dokumentasi_grid"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						data.status = $("#status").val();
						data.jenjang = $("#jenjang").val();
						data.provinsi = $("#provinsi").val();
						data.keyword = $("#keyword").val();
						data.bantuan = $("#bantuan").val();
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				

				$(document).on("change","#status,#jenjang,#provinsi",function(){


					dataTable.ajax.reload(null,false);	
				});

				$(document).on("input","#keyword",function(){


				   dataTable.ajax.reload(null,false);	
				});
				
				
	


</script>
				