<?php

class M_guru extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
    }


	function getClosest($search,$arr) {

		
		$closest = null;
		$indek   = null;
		foreach ($arr as $i=>$item) {
		   if ($closest === null || abs($search - $closest) > abs($item - $search)) {
			  $closest = $item;
			  $indek   = $i;
		   }
		}
		return  $indek;
	 }
	 
    
	public function grid($paging){
      
	    $key = $_REQUEST['search']['value'];
		$this->db->select("*");
        $this->db->from('madrasahedm');
    	
	    if(!empty($key)){  $this->db->where("UPPER(nama) LIKE '%".strtoupper($key)."%'");    }
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("id","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
	
	public function insert(){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
	    $id               = $this->Reff->get_max_id2("id","tm_guru");
	                         $foldernama  = $_SESSION['tmmadrasah_id'];
				
											if (!is_dir('__statics/upload/'.$foldernama)) {
												mkdir('./__statics/upload/'.$foldernama, 0777, TRUE);

											}
											
								
								  
								$foldernama  =  $_SESSION['tmmadrasah_id']."/guru/".$id;		
											if (!is_dir('__statics/upload/'.$foldernama)) {
												mkdir('./__statics/upload/'.$foldernama, 0777, TRUE);

											}
							$folder   					= '__statics/upload/'.$foldernama;
							
							
							
							
		$this->db->set("tmmadrasah_id",$_SESSION['tmmadrasah_id']);
		$this->db->set("id",$id);
		$this->db->set("folder",$folder);
		$this->db->set("tgl_lahir",$this->Reff->formattanggaldb($_POST['tgl_lahir']));
		$this->db->set("password",encrypt((($_POST['password']))));
		$this->db->set("alias",$_POST['password']);
		$this->db->set("i_entry",$_SESSION['nama_madrasah']);
		$this->db->set("d_entry",date("Y-m-d H:i:s"));
        
		$this->db->insert("tm_guru",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
	public function update($id){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
		 $foldernama  = $_SESSION['tmmadrasah_id'];
				
											if (!is_dir('__statics/upload/'.$foldernama)) {
												mkdir('./__statics/upload/'.$foldernama, 0777, TRUE);

											}
											
								
								  
								$foldernama  =  $_SESSION['tmmadrasah_id']."/guru/".$id;		
											if (!is_dir('__statics/upload/'.$foldernama)) {
												mkdir('./__statics/upload/'.$foldernama, 0777, TRUE);

											}
							$folder   					= '__statics/upload/'.$foldernama;
							
							
		$this->db->set("folder",$folder);
		
		
		
	    $this->db->set("tgl_lahir",$this->Reff->formattanggaldb($_POST['tgl_lahir']));
		$this->db->set("password",encrypt((($_POST['password']))));
		$this->db->set("i_update",$_SESSION['nama_madrasah']);
		$this->db->set("d_update",date("Y-m-d H:i:s"));
		$this->db->where("id",$id);
		$this->db->update("tm_guru",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
}
