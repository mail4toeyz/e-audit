<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Post-->
<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
	<div id="kt_content_container" class="container-xxl">
<div class="col-lg-12 col-xxl-12">
		
				       <div class="card">
						<div class="card-header card-header-success">
						  <h4 class="card-title">Pengaturan Kriteria Umum  </h4>
						 </div>
					  
						 <div class="card-body">
						 
						 
						 <div class="row">
						 
						 <div class="col-sm-5">
										<!--begin::Card-->
										<div class="card card-custom gutter-b">
											<div class="card-header">
												<div class="card-title">
													<h3 class="card-label">Kriteria Umum Eligibility</h3>
												</div>
											</div>
											<div class="card-body">
											
											  <ol start="1">
											    <li> Madrasah telah mengkuti Bimtek penerapan EDM dan RKAM. </li>
												<li> Madrasah telah melaksanakan EDM dengan menggunakan aplikasi yang disediakan. </li>
												<li>Madrasah telah menyusun RKAM dengan memanfaatkan aplikasi e-RKAM. </li>
												<li>Madrasah menerima dana BOS pada tahun berjalan.</li>
												<li>Memiliki Jumlah Minimal Peserta Didik <br>
													a. MI	: 60 - 336 orang (12 rombel) <br>
													b. MTs	: 60 - 480 orang (18 rombel)<br>
													c. MA	: 60 - 540 orang (24 rombel) <br>
												
											

											  </ol>
											  <?php
											   $tahun   = $this->Reff->tahun();
											   $longlist = $this->db->query("select count(id) as jml from madrasahedm where longlist=1 AND tahun='{$tahun}'")->row();
											   $tlonglist = $this->db->query("select count(id) as jml from madrasahedm where longlist=0 AND tahun='{$tahun}' and blacklist=0 and provinsi_id  IN(select id from provinsi where kuota_kinerja+kuota_afirmasi !=0)")->row();
											   ?>

											  <?php echo $longlist->jml; ?> Madrasah Masuk Longlist   <br>
											  <?php echo $tlonglist->jml; ?>  Madrasah Tidak Masuk Longlist  
											
											<h3> Longlist Hasil Visitasi  </h3>
											<!-- <?php
											   $tahun   = $this->Reff->tahun();
											   $longlist = $this->db->query("select count(id) as jml from madrasahedm where dnt_longlist=1 and pernyataan=1 AND tahun='{$tahun}'")->row();
											   $tlonglist = $this->db->query("select count(id) as jml from madrasahedm where dnt_longlist=0 and pernyataan=1 AND tahun='{$tahun}'")->row();
											   ?>

											  <button class="btn font-weight-bold btn-lg btn-outline-success btn-block"><span class="label label-xl label-danger ml-2" style="color:black"><?php echo $longlist->jml; ?></span> Madrasah Visitasi Masuk Longlist  </button>
											  <button class="btn font-weight-bold btn-lg btn-outline-danger btn-block"><span class="label label-xl label-danger ml-2" style="color:black"><?php echo $tlonglist->jml; ?></span> Madrasah Visitasi Tidak Masuk Longlist  </button>
											  <a href="<?php echo site_url("pengaturan/longlistGenerateDNT"); ?>" class="btn btn-sm btn-block btn-primary"><i class="fa fa-codepen"></i> Tentukan Longlist </a>
											  <a href="<?php echo site_url("pengaturan/batallonglistGenerateDNT"); ?>" class="btn btn-sm btn-block btn-danger"><i class="fa fa-times"></i> Batalkan  Longlist </a>
-->
											 </div>

										</div>
					     </div>

						 <div class="col-sm-7">
										<!--begin::Card-->
										<div class="card card-custom gutter-b">
											<div class="card-header">
												<div class="card-title">
													<h3 class="card-label">Pengaturan Kriteria </h3>
												</div>
											</div>
											<div class="card-body"> 

											<table class="table">
															<thead>
																<tr>
																	<th scope="col">#</th>
																	<th scope="col">Kriteria </th>
																	<th scope="col">Jenjang </th>
																	<th scope="col">Minimal</th>
																	<th scope="col">Maksimal</th>
																	<th> Status </th>
																</tr>
															</thead>
															<tbody>
															  <?php 
																$umum = $this->db->get("kriteria_umum")->result();
																$no=1;
															    foreach($umum as $rum){
																	?>
																   <tr>
																	<th scope="row"><?php echo $no++; ?></th>
																	<td><?php echo str_replace("_"," ",$rum->nama); ?></td>
																	<td><?php echo strtoupper($rum->jenjang); ?></td>
																	<td><input type="text" class="form-control perbaiki" datanya="<?php echo $rum->id; ?>" value="<?php echo $rum->nilai1; ?>"></td>
																	<td>
																	<?php 
																	 if($rum->nilai2==10000){
																		 echo "-";
																	 }else{
																		 ?>
																	
																	<input type="text" class="form-control perbaiki" datanya="<?php echo $rum->id; ?>" value="<?php echo $rum->nilai2; ?>">

																	 <?php 
																	 }
																	 ?>
																	
																	</td>
																	<td>
																	<span class="switch switch-outline  switch-success">
																		<label>
																			<input type="checkbox" checked="checked" name="select" />
																			<span></span>
																		</label>
																	</span>
																	</td>
																	
																  </tr>
																<?php 


																}
																?>
																
															</tbody>
														</table>

														<a href="<?php echo site_url("pengaturan/longlistGenerate"); ?>" class="btn btn-sm btn-block btn-primary"><i class="fa fa-codepen"></i> Tentukan Longlist </a>
														<a href="<?php echo site_url("pengaturan/batallonglistGenerate"); ?>" class="btn btn-sm btn-block btn-danger"><i class="fa fa-times"></i> Batalkan  Longlist </a>
											
											
											
											</div>

										</div>
					     </div>
						 
						 
						 
						 </div>
						 
						 
						 
						 
						 
						 
						 
						 
						  </div>
					    
                     </div>
 </div>
 </div>
 </div>
              
        
				