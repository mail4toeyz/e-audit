						
						<style>

.table-scroll {
    position: relative;
    width:100%;
    z-index: 1;
    margin: auto;
    overflow: scroll;
    height: 90vh;
}
.table-scroll table {
    width: 100%;
    min-width: 100px;
    margin: auto;
    border-collapse: separate;
    border-spacing: 0;
}
.table-wrap {
    position: relative;
}
.table-scroll th,
.table-scroll td {
    padding: 5px 10px;
    border: 1px solid #000;
    
    vertical-align: top;
    text-align: center;
}
.table-scroll thead th {
    background: #333;
    color: #fff;
    position: -webkit-sticky;
    position: sticky;
    top: 0;
}
th.sticky {
    position: -webkit-sticky;
    position: sticky;
    left: 0;
    z-index: 2;
    background: #ccc;
}
thead th.sticky {
    z-index: 5;
}
.table-scroll thead tr:nth-child(2) th {
    top: 30px;
}

						</style>
						<div class="alert alert-primary">
														<?php
														$tahun    = $this->Reff->tahun();
														$longlist  = $this->db->query("select count(id) as jml from madrasahedm where longlist=1 and tahun='{$tahun}'")->row();
														$shortlist = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and tahun='{$tahun}'")->row();
														$tshortlist = $this->db->query("select count(id) as jml from madrasahedm where shortlist=0 and tahun='{$tahun}' and  longlist=1")->row();
														$kinerja    = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and bantuan=1 and tahun='{$tahun}'")->row();
														$afirmasi   = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and bantuan=2 and tahun='{$tahun}'")->row();
														$dnt   = $this->db->query("select count(id) as jml from madrasahedm where pernyataan=1 and bantuan=2 and tahun='{$tahun}' and dnt=1")->row();
														$nondnt   = $this->db->query("select count(id) as jml from madrasahedm where pernyataan=1 and bantuan=2 and tahun='{$tahun}' and dnt=0")->row();
														?>
														 Longlist Madrasah  : <?php echo $longlist->jml; ?> |
														 Masuk Shortlist : <?php echo $shortlist->jml; ?> |
														 Tidak Masuk  : <?php echo $tshortlist->jml; ?> |
														 Bantuan Kinerja   : <?php echo $kinerja->jml; ?> |
														 Bantuan Afirmasi   : <?php echo $afirmasi->jml; ?> |
														 Daftar Nominasi Tetap   : <?php echo $dnt->jml; ?> |
														Tidak Masuk DNT   : <?php echo $nondnt->jml; ?> |


														</div>

														<br>
													<center><a href="<?php echo site_url("pengaturan/shortlistGenerate"); ?>" class="btn btn-sm btn-block btn-primary"><i class="fa fa-codepen"></i> Tentukan Shortlist </a>
														<a href="<?php echo site_url("pengaturan/batalshortlistGenerate"); ?>" class="btn btn-sm btn-block btn-danger"><i class="fa fa-times"></i> Batalkan  Shortlist </a> 
													</center><br>

														<div class="table-responsive table-scroll ">
                              
															<table class="table table-condensed table-bordered   " id="datatableTable" >
																<thead>
																	<tr>
																		<th rowspan="2"> NO </th>
																		
																		<th rowspan="2"> PROVINSI </th>
																		<th colspan="2"> Longlist </th>
																		<th colspan="2">  Bantuan Kinerja  </th>
																		<th colspan="2">  Bantuan 	Afirmasi  </th>
																		<th rowspan="2"> Total Kuota </th>
																		
																</tr>
																<tr>

																  <th> Calon </th>
																  <th> Longlist </th>

																  <th> Kuota </th>
																  <th> Shortlist </th>


																
																  <th> Kuota </th>
																  <th> Shortlist </th>

																</tr>
													</thead>
														<tbody>

															<?php 
                                                            $provinsi = $this->db->query(" select * from provinsi where (kuota_kinerja+kuota_afirmasi) !=0")->result();
															$no_prov=1;
															$tahun    = $this->Reff->tahun();

															$totalcalon=0;
															$totallonglist=0;
															$kinerjakuota=0;
															$kinerjashort=0;
															$afirmasikuota=0;
															$afirmasishort=0;
															$kuotatotal=0;
															 foreach($provinsi as $prov){
															

																 $calon    = $this->db->query("select count(id) as jml from madrasahedm where blacklist=0 and provinsi_id='{$prov->id}' and tahun='{$tahun}'")->row();
																 $longlist    = $this->db->query("select count(id) as jml from madrasahedm where longlist=1 and provinsi_id='{$prov->id}' and tahun='{$tahun}'")->row();
																 $kinerjashortlist = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and bantuan=1 and provinsi_id='{$prov->id}' and tahun='{$tahun}'")->row();
																 $afirmasishortlist = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and bantuan=2 and provinsi_id='{$prov->id}' and tahun='{$tahun}'")->row();
																
																 $totalcalon= $totalcalon + $calon->jml;
																 $totallonglist= $totallonglist + $longlist->jml;
																 $kinerjakuota= $kinerjakuota + $prov->kuota_kinerja;
																 $kinerjashort= $kinerjashort + $kinerjashortlist->jml;
																 $afirmasikuota= $afirmasikuota + $prov->kuota_afirmasi;
																 $afirmasishort= $afirmasishort + $afirmasishortlist->jml;
																 $kuotatotal= $kuotatotal + ($prov->kuota_afirmasi+$prov->kuota_kinerja);

																$styleKinerja = "";
																  if($kinerjashortlist->jml < $prov->kuota_kinerja){
																	$styleKinerja = "style='background-color:red;color:white'";
																  }

																  $styleafirmasi = "";
																  if($afirmasishortlist->jml < $prov->kuota_afirmasi){
																	$styleafirmasi = "style='background-color:red;color:white'";
																  }
																?>

																<tr style="font-weight:bold">
																		<th> <?php echo $prov->id; ?> </th>
																		
																		<th> <?php echo $prov->nama; ?> </th>
																		<th> <?php echo $calon->jml; ?>  </th>
																		<th> <?php echo $longlist->jml; ?>  </th>
																		<th> <?php echo $prov->kuota_kinerja; ?>  </th>
																		<th <?php echo $styleKinerja; ?>> <?php echo $kinerjashortlist->jml; ?>  </th>
																		<th> <?php echo $prov->kuota_afirmasi; ?>   </th>
																		<th <?php echo $styleafirmasi; ?>> <?php echo $afirmasishortlist->jml; ?>   </th>
																		<th> <?php echo $prov->kuota_afirmasi+$prov->kuota_kinerja; ?>   </th>
																		
																</tr>

																

															   <?php 

															
														}


															?>


															

														</tbody>
														   <tr>
																	   <th> # </th>
																	
																	   <th>Total</th>
																	   <th><?php echo $totalcalon; ?></th>
																	   <th><?php echo $totallonglist; ?></th>
																	   <th><?php echo $kinerjakuota; ?></th>
																	   <th><?php echo $kinerjashort; ?></th>
																	   <th><?php echo $afirmasikuota; ?></th>
																	   <th><?php echo $afirmasishort; ?></th>
																	  
																	   <th><?php echo $kuotatotal; ?></th>
																	 
																	   
																	  
															   </tr>

															</table>

													</div>


                                                        <!--<table class="table table-bordered table-hover table-striped">
															<thead>
																<tr>
																	<th scope="col">#</th>
																	<th scope="col">Jenis </th>
																	<th scope="col">Kuota </th>
																
																</tr>
															</thead>
															<tbody>
															  <?php 
															  
																$umum = $this->db->get_where("bantuan",array("tahun"=>$tahun))->result();
																$no=1;
															    foreach($umum as $rum){
																	?>
																   <tr>
																	<th scope="row"><?php echo $no++; ?></th>
																	<td><?php echo ($rum->nama); ?></td>
																	<td><input type="text" class="form-control perbaiki" datanya="<?php echo $rum->id; ?>" value="<?php echo $rum->kuota; ?>"></td>
																		
																  </tr>
																<?php 


																}
																?>
																
															</tbody>
														</table> -->

														

													<!--			<hr>
														<h3> HASIL VISITASI </h3>

														<br>
														<a href="<?php echo site_url("pengaturan/shortlistGenerateDNT"); ?>" class="btn btn-sm btn-block btn-primary"><i class="fa fa-codepen"></i> Tentukan Shortlist Visitasi </a>
														<a href="<?php echo site_url("pengaturan/batalshortlistGenerateDNT"); ?>" class="btn btn-sm btn-block btn-danger"><i class="fa fa-times"></i> Batalkan  Shortlist Visitasi </a>
															-->
											
		
<script type="text/javascript">


//  var dataTable = $('#datatableTable').DataTable( {
// 					   "processing": false,
// 					   "scrollY": 500,
// 					   "scrollX": true,
// 					   "language": {
// 					   "processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
// 						 "oPaginate": {
// 						   "sFirst": "Halaman Pertama",
// 						   "sLast": "Halaman Terakhir",
// 							"sNext": "Selanjutnya",
// 							"sPrevious": "Sebelumnya"
// 							},
// 					   "sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
// 						"sInfoEmpty": "Tidak ada data yang di tampilkan",
// 						  "sZeroRecords": "Data kosong",
// 						  "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
// 				   },
				   
// 				   "serverSide": false,
// 				   "searching": true,
// 				   "responsive": false
				   
// 			   } );
			   

			   
			   
   


</script>