<div class="d-flex flex-column-fluid">
							
<div class="container">
<div class="col-lg-12 col-xxl-12">
		
				       <div class="card">
						<div class="card-header card-header-success">
						  <h4 class="card-title">Aktivasi Bantuan Kinerja dan Afirmasi   </h4>
						 </div>
					  
						 <div class="card-body">
						 
						 
						 <div class="row">
						 
						 

						 <div class="col-sm-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b">
											<div class="card-header">
												<div class="card-title">
													<h3 class="card-label">Pengaturan Aktivasi </h3>
												</div>
											</div>
											<div class="card-body"> 

											<table class="table">
															<thead>
																<tr>
																	<th scope="col">#</th>
																	<th scope="col">Tahun </th>
																	<th scope="col">Sasaran Bantuan  </th>
																	
																	<th> Status </th>
																</tr>
															</thead>
															<tbody>
															  <?php 
																$umum = $this->db->get("pengaturan")->result();
																$no=1;
															    foreach($umum as $rum){
																	$checked ="";
																	  if($rum->active==1){
																		$checked ="checked";
																	  }
																	?>
																   <tr>
																	<th scope="row"><?php echo $no++; ?></th>																
																	<td><?php echo ($rum->tahun); ?></td>
																	<td><?php echo ($rum->nama); ?></td>
																		
																	<td>
																	<span class="switch switch-outline  switch-success">
																		<label>
																			<input type="checkbox" value="1" <?php echo $checked; ?> data-id="<?php echo $rum->id; ?>" class="aktivasi" />
																			<span></span>
																		</label>
																	</span>
																	</td>
																	
																  </tr>
																<?php 


																}
																?>
																
															</tbody>
														</table>
								
											
											
											</div>

										</div>
					     </div>
						 
						 
						 
						 </div>

						 <?php            
$service = new Google_Drive();   
if(!$service->statusApi()){
	$authlink = $service->createAuthUrl();
	$buttonlink =  "<a href=\"" . $authlink . "\" target=\"_googleAuthLogin\" class=\"btn btn-danger \">\n   <i class=\"fa fa-ban\"></i>\n Get Token GDrive </a>";
}else{
	$buttonlink = "";
}  


echo "  <div class=\"row\">";
  if($buttonlink==""){
	  $dataakun = $service->getOuthData();
	  if(empty($dataakun)){
		  $dataakun['picture'] = "https://www.gstatic.com/images/branding/product/1x/drive_48dp.png";
		  $dataakun['name'] = "Google Akun";
	  }
      echo "<div class=\"col-md-12\">
	  <div class=\"card\">
	  <div class=\"card-header card-header-success\">
	  <h4 class=\"card-title\">Pengaturan Upload kedalam Google Drive</h4>
	  <p class=\"card-category\"> Form ini digunakan untuk mengaktifkan upload Google Drive  </p>
	  </div>	                  
	  <div class=\"card-body\">
	  <div class=\"col-md-12\">
	  <div class=\"row\"> 
	  <div class=\"col-md-3\">
	  <div  style=\"max-width: 130px;max-height: 130px;margin: 0px auto 0;border-radius: 50%; 
    overflow: hidden;padding: 0;box-shadow: 0 16px 38px -12px rgba(0,0,0,.56), 0 4px 25px 0 rgba(0,0,0,.12), 0 8px 10px -5px rgba(0,0,0,.2);\">
                  <a ><img  style=\"width: 100%;height: auto;\" src=\"".$dataakun['picture']."\" >
                  </a>
               </div>
				  <br><h4 style=\"text-align: center;\">".$dataakun['name']."</h4>
                 </div>";	  
				 
	  echo "<div class=\"col-md-9\"><p class=\"card-category\"> Upload Google Drive sudah aktif  </p>";
	  echo "<p class=\"card-category\">  Seluruh file yang diupload melalui aplikasi akan di upload kedalam google drive sehingga tidak membebani server </p>";
	  echo "<a href=\"" . site_url("pengaturan/hapusGdrive") . "\" class=\"btn btn-danger \">\n   <i class=\"fa fa-trash\"></i>\n Non Aktifkan </a>";   
	
	echo"               </div>\r\n</div>\r\n</div>\r\n  </div>\r\n              </div>\r\n            </div></div>\r\n         \r\n         \r\n  "; 
	  
  }else{
  echo "<div class=\"col-md-12\">
	  <div class=\"card\">
	  <div class=\"card-header card-header-success\">
	  <h4 class=\"card-title\">Pengaturan Upload GDrive</h4>
	  <p class=\"card-category\"> Form ini digunakan untuk mengaktifkan upload gdrive  </p>
	  </div>
	  <div class=\"card-body\"> ";
	  echo "<p > Untuk mendapatkan token, silahkan klik  <b> Get Token GDrive </b>. Kemudian pilih akun gmail yang akan di integrasikan. Setelah muncul kode token, Isikan ke form berikut dan Klik Simpan </p>";
echo "  <form  method=\"post\" action=\"";
echo site_url("pengaturan/authapisave");
echo "\">\r\n                    <div class=\"row\">\r\n                      <div class=\"col-md-12\">\r\n                        <div class=\"form-group\">\r\n 
<input type=\"text\" class=\"form-control\" name=\"userauth\" placeholder=\"Masukkan Token Gdive\"  title=\"Masukkan NSM \" data-placement=\"right\">
\r\n                    \r\n                    \r\n                    \r\n                        </div>\r\n                      </div>\r\n                     \r\n                      \r\n                    </div>\r\n                      \r\n            \r\n                  \r\n".$buttonlink."                    <button type=\"submit\" class=\"btn btn-success pull-right\">Simpan Token</button>\r\n                    <div class=\"clearfix\"></div>\r\n                  </form>\r\n ";
echo"               </div>\r\n              </div>\r\n            </div></div>\r\n         \r\n         \r\n  "; 
  }

  ?>
						 
						 
						 
						 
						 
						 
						 
						 
						  </div>
					    
                     </div>
 </div>
 </div>
 </div>
              
        
<script type="text/javascript">
   
   $(document).off("click",".aktivasi").on("click",".aktivasi",function(){
	var active =0;
	var id = $(this).data("id");
	if ($(this).is(':checked')) {
		var active =1;
	 }

	 $.post("<?php echo site_url('pengaturan/aktivasi_set'); ?>",{id:id,active:active},function(){

		 location.reload();
	 })


   });
				
	


</script>
				