<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Post-->
<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
	<div id="kt_content_container" class="container-xxl">

	
<div class="col-lg-12 col-xxl-12">
		
				       <div class="card">
						<div class="card-header card-header-success">
						  <h4 class="card-title">Pengaturan Kriteria Khusus (Shortlist)  </h4>
						 </div>
					  
						 <div class="card-body">
						     <ul class="nav nav-pills" id="myTab1" role="tablist">
															<li class="nav-item">
																<a class="nav-link active" id="home-tab-1" data-toggle="tab" href="#home-1">
																    <span class="nav-icon">
																		<i class="fa fa-star"></i>
																	</span>

																	<span class="nav-text">Generate Shortlist </span>
																</a>
															</li>
															<li class="nav-item">
																<a class="nav-link " id="home-tab-2" data-toggle="tab" href="#EDM">
																	
																	<span class="nav-text">EDM </span>
																</a>
															</li>
															<li class="nav-item">
																<a class="nav-link " id="home-tab-3" data-toggle="tab" href="#PIP">
																	
																	<span class="nav-text">PIP  </span>
																</a>
															</li>
															<li class="nav-item">
																<a class="nav-link " id="home-tab-4" data-toggle="tab" href="#ROMBEL">
																	
																	<span class="nav-text">Jumlah Ruang Belajar  </span>
																</a>
															</li>
															<li class="nav-item">
																<a class="nav-link " id="home-tab-5" data-toggle="tab" href="#toilet">
																	
																	<span class="nav-text">Jumlah Toilet  </span>
																</a>
															</li>
															<li class="nav-item">
																<a class="nav-link " id="home-tab-6" data-toggle="tab" href="#inklusi">
																	
																	<span class="nav-text">Madrasah Inklusi  </span>
																</a>
															</li>
						    </ul>
					 <div class="tab-content mt-5" id="myTabContent1">
					        <div class="tab-pane fade show active" id="home-1" role="tabpanel" aria-labelledby="home-tab-1"><?php $this->load->view('skor'); ?></div>
							<div class="tab-pane fade show " id="EDM" role="tabpanel" aria-labelledby="home-tab-2"><?php $this->load->view('edm'); ?>  </div>
							<div class="tab-pane fade show " id="PIP" role="tabpanel" aria-labelledby="home-tab-3"><?php $this->load->view('pip'); ?>  </div>
							<div class="tab-pane fade show " id="ROMBEL" role="tabpanel" aria-labelledby="home-tab-4"><?php $this->load->view('rombel'); ?>  </div>
							<div class="tab-pane fade show " id="toilet" role="tabpanel" aria-labelledby="home-tab-5"><?php $this->load->view('toilet'); ?>  </div>
							<div class="tab-pane fade show " id="inklusi" role="tabpanel" aria-labelledby="home-tab-6"> 
							 
							  <div class="table-responsive">
								  <table class="table table-hover table-bordered table-striped">
									  <thead>
										  <tr>
											  <th>NO</th>
											  <th>Jumlah ABK dimadrasah</th>
											  <th>Skor</th>
										  </tr>
									  </thead>
									  <tbody>
										  <tr>
											  <td>1</td>
											  <td>< 6 siswa</td>
											  <td>0</td>
										  </tr>
										  <tr>
											  <td>2</td>
											  <td>6 - 10 siswa</td>
											  <td>5</td>
										  </tr>

										  <tr>
											  <td>3</td>
											  <td>11 - 15 siswa</td>
											  <td>10</td>
										  </tr>
										  <tr>
											  <td>4</td>
											  <td>16 - 20 siswa</td>
											  <td>15</td>
										  </tr>

										  <tr>
											  <td>5</td>
											  <td>> 20 siswa</td>
											  <td>20</td>
										  </tr>
									  </tbody>
								  </table>
							  </div>
						
						    </div>
					</div>
						 
						

						
						 
						 
						 
						 
						  </div>
					    
                     </div>
 </div>
 </div>
 </div>
 </div>
              