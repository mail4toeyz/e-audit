<?php
ini_set('memory_limit', '-1');
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 if(!$this->session->userdata("admin_id")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_guru','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('pusat/page_header',$data);	
	}
		
	public function longlist()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Pengaturan Longlist";
	     if(!empty($ajax)){
					    
			 $this->load->view('longlist',$data);
		
		 }else{
			 
		     $data['konten'] = "longlist";
			 
			 $this->_template($data);
		 }
	

	}
	

	public function shortlist()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Pengaturan Shortlist";
	     if(!empty($ajax)){
					    
			 $this->load->view('shortlist',$data);
		
		 }else{
			 
		     $data['konten'] = "shortlist";
			 
			 $this->_template($data);
		 }
	

	}

	public function aktivasi()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Aktivasi Bantuan";
	     if(!empty($ajax)){
					    
			 $this->load->view('aktivasi',$data);
		
		 }else{
			 
		     $data['konten'] = "aktivasi";
			 
			 $this->_template($data);
		 }
	

	}

	public function aktivasi_set(){

		$this->db->set("active",0);
		$this->db->update("pengaturan");


		$this->db->set("active",1);
		$this->db->where("id",$_POST['id']);
		$this->db->update("pengaturan");

		redirect(base_url()."pengaturan/aktivasi");

	}


	public function longlistGenerate(){
		$tahun   = $this->Reff->tahun();

		//$m = $this->db->get_where("madrasahedm",array("tahun"=>$tahun,"blacklist"=>0))->result();
		$m = $this->db->query("SELECT * from madrasahedm where tahun='{$tahun}' and blacklist=0  and provinsi_id  IN(select id from provinsi where kuota_kinerja+kuota_afirmasi !=0)")->result();
		//$m = $this->db->get_where("madrasahedm",array("nsm"=>"131236720021"))->result();
		foreach($m as $rm){
			

		  $kriteria_umum = $this->db->query("select * from kriteria_umum where UPPER(jenjang)='".strtoupper($rm->jenjang)."' and nama='Minimal_Peserta_Didik' and status=0")->row();
		  $guru          = $this->db->query("select * from kriteria_umum where UPPER(jenjang)='".strtoupper($rm->jenjang)."' and nama='Minimal_Guru' and status=0")->row();
		  
		  $minimalGuru   = isset($guru->nilai1) ? $guru->nilai1 :0; 
		  //echo $rm->nsm; exit();
		    if($rm->jml_siswa >= $kriteria_umum->nilai1 &&  $rm->jml_siswa <= $kriteria_umum->nilai2 ){

				if($rm->jml_guru >= $minimalGuru){
						
						$this->db->set("longlist",1);
						$this->db->where("id",$rm->id);
						$this->db->update("madrasahedm");
				}else{

					
				}


			}


		}

		redirect(base_url()."pengaturan/longlist");


	}

	public function batallonglistGenerate(){
	
		        $tahun   = $this->Reff->tahun();
				$this->db->set("longlist",0);
				$this->db->where("tahun",$tahun);
				$this->db->update("madrasahedm");


			


		

		redirect(base_url()."pengaturan/longlist");


	}

	public function longlistGenerateDNT(){
		$tahun   = $this->Reff->tahun();

		$m = $this->db->get_where("madrasahedm",array("tahun"=>$tahun,"pernyataan"=>1))->result();
		//$m = $this->db->get_where("madrasahedm",array("nsm"=>"131236720021"))->result();
		foreach($m as $rm){

		  $kriteria_umum = $this->db->query("select * from kriteria_umum where UPPER(jenjang)='".strtoupper($rm->jenjang)."' and nama='Minimal_Peserta_Didik' and status=0")->row();
		  $guru          = $this->db->query("select * from kriteria_umum where UPPER(jenjang)='".strtoupper($rm->jenjang)."' and nama='Minimal_Guru' and status=0")->row();
		  
		  $minimalGuru   = isset($guru->nilai1) ? $guru->nilai1 :0; 
		  $jml_siswa     = $this->db->query("select total from visitasi_pesertadidik where madrasah_id='".$rm->id."'")->row();
		  $jml_guru      = $this->db->query("select (guru_tetap+guru_tidak_tetap) as total from visitasi_guru where madrasah_id='{$rm->id}'")->row();
		  $jml_siswa     = $jml_siswa->total;
		  $jml_guru     = $jml_guru->total;
		    if($jml_siswa >= $kriteria_umum->nilai1 &&  $jml_siswa <= $kriteria_umum->nilai2 ){

				if($jml_guru >= $minimalGuru){

				
						$this->db->set("dnt_longlist",1);
						$this->db->where("id",$rm->id);
						$this->db->update("madrasahedm");
					
				}else{

					
				}


			}


		}

		//redirect(base_url()."pengaturan/longlist");


	}

	


	

	public function shortlistGenerate(){
		//error_reporting(0);
		$tahun   = $this->Reff->tahun();

		$m = $this->db->get_where("madrasahedm",array("tahun"=>$tahun,"blacklist"=>0))->result();

		$pengaturan_edm    = $this->Reff->get_kondisi(array("tabel"=>"kriteria_edm"),"pengaturan_shortlist","status");
		$pengaturan_pip    = $this->Reff->get_kondisi(array("tabel"=>"kriteria_pip"),"pengaturan_shortlist","status");
		$pengaturan_rombel = $this->Reff->get_kondisi(array("tabel"=>"kriteria_rombel"),"pengaturan_shortlist","status");
		$pengaturan_toilet = $this->Reff->get_kondisi(array("tabel"=>"kriteria_toilet"),"pengaturan_shortlist","status"); 
		

		foreach($m as $rm){
          $persen_pip     = ceil($rm->persen_pip); 
          $persen_rombel  = ceil($rm->rombel_persen); 
          $toilet_persen  = ceil($rm->toilet_persen); 
		  $kriteria_pip = $this->db->query("SELECT skor FROM `kriteria_pip` where $persen_pip BETWEEN nilai1 and nilai2")->row();
		  $skor_rombel  = $this->db->query("SELECT skor FROM `kriteria_rombel` where $persen_rombel BETWEEN nilai1 and nilai2")->row();
		  $skor_toilet  = $this->db->query("SELECT skor FROM `kriteria_toilet` where $toilet_persen BETWEEN nilai1 and nilai2")->row();
		  $skor_edm     = $rm->nilai_kedisiplinan+$rm->nilai_pengembangan_diri+$rm->nilai_proses_pembelajaran+$rm->nilai_sarana_prasarana+$rm->nilai_pembiayaan;
		  
		  $skor_inklusi=0;
		    if($rm->abk < 6){
			  $skor_inklusi=0;
		    }else if($rm->abk >= 6 && $rm->abk <= 10){
			    $skor_inklusi=5;
			}else if($rm->abk >= 11 && $rm->abk <= 15){
				$skor_inklusi=10;
			}else if($rm->abk >= 16 && $rm->abk <= 20){
				$skor_inklusi=15;
			}else if($rm->abk > 20){
				$skor_inklusi=20;
			}


		  $skor_edm2     = ($pengaturan_edm==0) ? $skor_edm :0;
		  if(!is_null($kriteria_pip)){
			$kriteria_pipSkor = $kriteria_pip->skor;
		    $skor_pip2     = ($pengaturan_pip==0) ? $kriteria_pipSkor :0;
		  }else{
			$kriteria_pipSkor = 0;

		  }
		  $skor_rombel2  = ($pengaturan_rombel==0) ? $skor_rombel->skor :0;
		  $skor_toilet2  = ($pengaturan_toilet==0) ? $skor_toilet->skor :0;
		  
		  
		 
		  $skor_akhir   = $skor_edm2 + $skor_pip2 + $skor_rombel2 + $skor_toilet2 + $skor_inklusi;

				$this->db->set("skor_inklusi",$skor_inklusi);
				$this->db->set("skor_edm",$skor_edm);
				$this->db->set("skor_pip",$kriteria_pipSkor);
				$this->db->set("skor_rombel",$skor_rombel->skor);
				$this->db->set("skor_toilet",$skor_toilet->skor);
				$this->db->set("skor_akhir",$skor_akhir);				
				$this->db->where("id",$rm->id);
				$this->db->update("madrasahedm");
		}




		
				$tahun    = $this->Reff->tahun();

				// Generate Kinerja
				$provinsi = $this->db->get_where("provinsi",array("kuota_kinerja !="=>0))->result();
				
				foreach($provinsi as $rp){
					$afirmasi  = $this->db->query("select * from madrasahedm where provinsi_id='".$rp->id."' and tahun='{$tahun}' and longlist=1 and shortlist=0 order by  skor_akhir DESC, skor_edm DESC,skor_pip DESC,skor_rombel DESC,skor_toilet DESC,nsm ASC")->result();
					$sudahdpt  = $this->db->query("select count(id) as jml from madrasahedm where provinsi_id='".$rp->id."' and tahun='{$tahun}' and shortlist=1 and bantuan=1")->row();
				     $kuota     = $rp->kuota_kinerja - $sudahdpt->jml; 
					$rankKinerja=0;
						foreach($afirmasi as $rm){
							$rankKinerja++;
								// if($rankKinerja==$rp->kuota_kinerja){

								// 	$this->db->set("max_kinerja",$rm->skor_akhir);
								// 	$this->db->where("id",$rp->id);
								// 	$this->db->update("provinsi");
									
								// }

								$arr   = array('1'=>$rp->max_kinerja,"2"=>$rp->max_afirmasi);

								
									if($rankKinerja <= $kuota){
										$this->db->set("shortlist",1);
										$this->db->set("bantuan",1);
										$this->db->where("id",$rm->id);
									   $this->db->update("madrasahedm");

									}
									
									
									
				
				
							}

				}


			

						
				// Generate Afirmasi
				$provinsi = $this->db->get_where("provinsi",array("kuota_afirmasi !="=>0))->result();

				foreach($provinsi as $rp){
					$afirmasi = $this->db->query("select * from madrasahedm where provinsi_id='".$rp->id."' and tahun='{$tahun}' and longlist=1  and shortlist=0 and akreditasi !='A' order by skor_akhir ASC, skor_edm ASC,skor_pip ASC,skor_rombel ASC,skor_toilet ASC,nsm ASC")->result();
					$rankAfirmasi=0;

					$sudahdpt  = $this->db->query("select count(id) as jml from madrasahedm where provinsi_id='".$rp->id."' and tahun='{$tahun}' and shortlist=1 and bantuan=2")->row();
				    $kuota     = $rp->kuota_afirmasi - $sudahdpt->jml;


						foreach($afirmasi as $rm){
							$rankAfirmasi++;


							$arr   = array('1'=>$rp->max_kinerja,"2"=>$rp->max_afirmasi);

							
							 
								if($rm->bantuan==0){

									
									
									
									if($rankAfirmasi <= $kuota ){
										
										$this->db->set("shortlist",1);
										$this->db->set("bantuan",2);
										$this->db->where("id",$rm->id);
								    	$this->db->update("madrasahedm");
										

									}
									  
								}

								// if($rankAfirmasi==$rp->kuota_afirmasi){

								// 	$this->db->set("max_afirmasi",$rm->skor_akhir);
								// 	$this->db->where("id",$rp->id);
								// 	$this->db->update("provinsi");
									
								// }

									
									
				
				
							}

				}
	

		redirect(base_url()."pengaturan/shortlist");


	}


	public function shortlistGenerateAspiratorSet(){
		//error_reporting(0);
		$tahun   = $this->Reff->tahun();

		$m = $this->db->get_where("madrasahedm",array("tahun"=>$tahun,"blacklist"=>0))->result();

		$pengaturan_edm    = $this->Reff->get_kondisi(array("tabel"=>"kriteria_edm"),"pengaturan_shortlist","status");
		$pengaturan_pip    = $this->Reff->get_kondisi(array("tabel"=>"kriteria_pip"),"pengaturan_shortlist","status");
		$pengaturan_rombel = $this->Reff->get_kondisi(array("tabel"=>"kriteria_rombel"),"pengaturan_shortlist","status");
		$pengaturan_toilet = $this->Reff->get_kondisi(array("tabel"=>"kriteria_toilet"),"pengaturan_shortlist","status"); 
		

		foreach($m as $rm){
          $persen_pip     = ceil($rm->persen_pip); 
          $persen_rombel  = ceil($rm->rombel_persen); 
          $toilet_persen  = ceil($rm->toilet_persen); 
		  $kriteria_pip = $this->db->query("SELECT skor FROM `kriteria_pip` where $persen_pip BETWEEN nilai1 and nilai2")->row();
		  $skor_rombel  = $this->db->query("SELECT skor FROM `kriteria_rombel` where $persen_rombel BETWEEN nilai1 and nilai2")->row();
		  $skor_toilet  = $this->db->query("SELECT skor FROM `kriteria_toilet` where $toilet_persen BETWEEN nilai1 and nilai2")->row();
		  $skor_edm     = $rm->nilai_kedisiplinan+$rm->nilai_pengembangan_diri+$rm->nilai_proses_pembelajaran+$rm->nilai_sarana_prasarana+$rm->nilai_pembiayaan;
		  
		  $skor_inklusi=0;
		    if($rm->abk < 6){
			  $skor_inklusi=0;
		    }else if($rm->abk >= 6 && $rm->abk <= 10){
			    $skor_inklusi=5;
			}else if($rm->abk >= 11 && $rm->abk <= 15){
				$skor_inklusi=10;
			}else if($rm->abk >= 16 && $rm->abk <= 20){
				$skor_inklusi=15;
			}else if($rm->abk > 20){
				$skor_inklusi=20;
			}


		  $skor_edm2     = ($pengaturan_edm==0) ? $skor_edm :0;
		  if(!is_null($kriteria_pip)){
			$kriteria_pipSkor = $kriteria_pip->skor;
		    $skor_pip2     = ($pengaturan_pip==0) ? $kriteria_pipSkor :0;
		  }else{
			$kriteria_pipSkor = 0;

		  }
		  $skor_rombel2  = ($pengaturan_rombel==0) ? $skor_rombel->skor :0;
		  $skor_toilet2  = ($pengaturan_toilet==0) ? $skor_toilet->skor :0;
		  
		  
		 
		  $skor_akhir   = $skor_edm2 + $skor_pip2 + $skor_rombel2 + $skor_toilet2 + $skor_inklusi;

				$this->db->set("skor_inklusi",$skor_inklusi);
				$this->db->set("skor_edm",$skor_edm);
				$this->db->set("skor_pip",$kriteria_pipSkor);
				$this->db->set("skor_rombel",$skor_rombel->skor);
				$this->db->set("skor_toilet",$skor_toilet->skor);
				$this->db->set("skor_akhir",$skor_akhir);				
				$this->db->where("id",$rm->id);
				$this->db->update("madrasahedm");
		}




		
				$tahun    = $this->Reff->tahun();

				// Generate Kinerja
				$provinsi = $this->db->get_where("kota",array("shortlist_kinerja !="=>0))->result();
				
				foreach($provinsi as $rp){
					$afirmasi  = $this->db->query("select * from madrasahedm where kota_id='".$rp->id."' and tahun='{$tahun}' and longlist=1 and aspirasi_status =1 order by akreditasi asc,  skor_akhir DESC, skor_edm DESC,skor_pip DESC,skor_rombel DESC,skor_toilet DESC,nsm ASC")->result();
				
					$rankKinerja=0;
						foreach($afirmasi as $rm){
							$rankKinerja++;
								// if($rankKinerja==$rp->kuota_kinerja){

								// 	$this->db->set("max_kinerja",$rm->skor_akhir);
								// 	$this->db->where("id",$rp->id);
								// 	$this->db->update("provinsi");
									
								// }


								
									if($rankKinerja <= $rp->shortlist_kinerja){
										$this->db->set("shortlist",1);
										$this->db->set("bantuan",1);
										$this->db->where("id",$rm->id);
									   $this->db->update("madrasahedm");

									}
									
									
									
				
				
							}

				}


			

						
				// Generate Afirmasi
				$provinsi = $this->db->get_where("kota",array("shortlist_afirmasi !="=>0))->result();

				foreach($provinsi as $rp){
					$afirmasi = $this->db->query("select * from madrasahedm where kota_id='".$rp->id."' and tahun='{$tahun}' and longlist=1 and aspirasi_status =2 and akreditasi !='A' order by skor_akhir ASC, skor_edm ASC,skor_pip ASC,skor_rombel ASC,skor_toilet ASC,nsm ASC")->result();
					$rankAfirmasi=0;
						foreach($afirmasi as $rm){
							$rankAfirmasi++;


						

							
							 
								if($rm->bantuan==0){

									
									
									
									if($rankAfirmasi <= $rp->shortlist_afirmasi){
										
										$this->db->set("shortlist",1);
										$this->db->set("bantuan",2);
										$this->db->where("id",$rm->id);
								    	$this->db->update("madrasahedm");
										

									}
									  
								}

								// if($rankAfirmasi==$rp->kuota_afirmasi){

								// 	$this->db->set("max_afirmasi",$rm->skor_akhir);
								// 	$this->db->where("id",$rp->id);
								// 	$this->db->update("provinsi");
									
								// }

									
									
				
				
							}

				}
	

		redirect(base_url()."pengaturan/shortlist");


	}

	public function shortlistGenerateAspirasi(){
		//error_reporting(0);
		$tahun   = $this->Reff->tahun();

		$m = $this->db->get_where("madrasahedm",array("tahun"=>$tahun,"blacklist"=>0))->result();

		$pengaturan_edm    = $this->Reff->get_kondisi(array("tabel"=>"kriteria_edm"),"pengaturan_shortlist","status");
		$pengaturan_pip    = $this->Reff->get_kondisi(array("tabel"=>"kriteria_pip"),"pengaturan_shortlist","status");
		$pengaturan_rombel = $this->Reff->get_kondisi(array("tabel"=>"kriteria_rombel"),"pengaturan_shortlist","status");
		$pengaturan_toilet = $this->Reff->get_kondisi(array("tabel"=>"kriteria_toilet"),"pengaturan_shortlist","status"); 
		

		foreach($m as $rm){
          $persen_pip     = ceil($rm->persen_pip); 
          $persen_rombel  = ceil($rm->rombel_persen); 
          $toilet_persen  = ceil($rm->toilet_persen); 
		  $kriteria_pip = $this->db->query("SELECT skor FROM `kriteria_pip` where $persen_pip BETWEEN nilai1 and nilai2")->row();
		  $skor_rombel  = $this->db->query("SELECT skor FROM `kriteria_rombel` where $persen_rombel BETWEEN nilai1 and nilai2")->row();
		  $skor_toilet  = $this->db->query("SELECT skor FROM `kriteria_toilet` where $toilet_persen BETWEEN nilai1 and nilai2")->row();
		  $skor_edm     = $rm->nilai_kedisiplinan+$rm->nilai_pengembangan_diri+$rm->nilai_proses_pembelajaran+$rm->nilai_sarana_prasarana+$rm->nilai_pembiayaan;
		  
		  $skor_inklusi=0;
		    if($rm->abk < 6){
			  $skor_inklusi=0;
		    }else if($rm->abk >= 6 && $rm->abk <= 10){
			    $skor_inklusi=5;
			}else if($rm->abk >= 11 && $rm->abk <= 15){
				$skor_inklusi=10;
			}else if($rm->abk >= 16 && $rm->abk <= 20){
				$skor_inklusi=15;
			}else if($rm->abk > 20){
				$skor_inklusi=20;
			}


		  $skor_edm2     = ($pengaturan_edm==0) ? $skor_edm :0;
		  if(!is_null($kriteria_pip)){
			$kriteria_pipSkor = $kriteria_pip->skor;
		    $skor_pip2     = ($pengaturan_pip==0) ? $kriteria_pipSkor :0;
		  }else{
			$kriteria_pipSkor = 0;

		  }
		  $skor_rombel2  = ($pengaturan_rombel==0) ? $skor_rombel->skor :0;
		  $skor_toilet2  = ($pengaturan_toilet==0) ? $skor_toilet->skor :0;
		  
		  
		 
		  $skor_akhir   = $skor_edm2 + $skor_pip2 + $skor_rombel2 + $skor_toilet2 + $skor_inklusi;

				$this->db->set("skor_inklusi",$skor_inklusi);
				$this->db->set("skor_edm",$skor_edm);
				$this->db->set("skor_pip",$kriteria_pipSkor);
				$this->db->set("skor_rombel",$skor_rombel->skor);
				$this->db->set("skor_toilet",$skor_toilet->skor);
				$this->db->set("skor_akhir",$skor_akhir);				
				$this->db->where("id",$rm->id);
				$this->db->update("madrasahedm");
		}




		
				$tahun    = $this->Reff->tahun();

				// Generate Kinerja
				$provinsi = $this->db->get_where("provinsi",array("kuota_kinerja !="=>0))->result();
				
				foreach($provinsi as $rp){
					$afirmasi = $this->db->query("select * from madrasahedm where provinsi_id='".$rp->id."' and tahun='{$tahun}' and longlist=1 and nsm IN(select nsm from aspirasi)  order by  skor_akhir DESC, skor_edm DESC,skor_pip DESC,skor_rombel DESC,skor_toilet DESC,nsm ASC")->result();
				    $rankKinerja=0;
						foreach($afirmasi as $rm){
							$rankKinerja++;
								
								$arr   = array('1'=>$rp->max_kinerja,"2"=>$rp->max_afirmasi);

								    $close = $this->m->getClosest($rm->skor_akhir,$arr);

									$this->db->set("aspirasi_status",$close);
									

								
									
									
									$this->db->where("id",$rm->id);
									$this->db->update("madrasahedm");
				
				
							}

				}


			

						
			
	

		redirect(base_url()."pengaturan/shortlist");


	}

	public function generateAspirasi(){

		$m     = $this->db->get_where("madrasahedm",array("tahun"=>2022))->result();
		  
		$arrrombel = array("mi"=>28,"mts"=>32,"ma"=>36); 
		
		  foreach($m as $rm){
			 
			$aspirator           = $this->Reff->get_kondisi(array("nsm"=>$rm->nsm),"aspirasi","id");
			




			$this->db->set("shortlist",1);
			$this->db->set("bantuan",$rm->aspirasi_status);
			$this->db->set("aspirator",$aspirator);


			
			$this->db->where("id",$rm->id);
			$this->db->update("madrasahedm");

		  }

		  echo "sukses";

	
	}


	public function batalshortlistGenerate(){
		$tahun   = $this->Reff->tahun();

		$this->db->set("rank",0);
		$this->db->set("rank_kinerja",0);
		$this->db->set("rank_afirmasi",0);
		$this->db->set("shortlist",0);
		$this->db->set("bantuan",0);
		$this->db->set("skor_akhir",0);
		$this->db->set("skor_pip",0);
		$this->db->set("skor_inklusi",0);
		$this->db->set("skor_edm",0);
		$this->db->set("skor_rombel",0);
		$this->db->set("skor_toilet",0);
		$this->db->where("tahun",$tahun);
		$this->db->update("madrasahedm");

		// $this->db->set("max_kinerja",0);
		// $this->db->set("max_afirmasi",0);
		// $this->db->update("provinsi");


		redirect(base_url()."pengaturan/shortlist");

	}


	public function shortlistGenerateDNT(){
		error_reporting(0);
		$tahun   = $this->Reff->tahun();

		$m = $this->db->get_where("madrasahedm",array("tahun"=>$tahun,"dnt_longlist"=>1))->result();

		$pengaturan_edm    = $this->Reff->get_kondisi(array("tabel"=>"kriteria_edm"),"pengaturan_shortlist","status");
		$pengaturan_pip    = $this->Reff->get_kondisi(array("tabel"=>"kriteria_pip"),"pengaturan_shortlist","status");
		$pengaturan_rombel = $this->Reff->get_kondisi(array("tabel"=>"kriteria_rombel"),"pengaturan_shortlist","status");
		$pengaturan_toilet = $this->Reff->get_kondisi(array("tabel"=>"kriteria_toilet"),"pengaturan_shortlist","status"); 
		

		foreach($m as $rm){
          $persen_pip     = ceil($rm->persen_pip); 
          $persen_rombel  = ceil($rm->rombel_persen_visitasi); 
          $toilet_persen  = ceil($rm->toilet_persen_visitasi); 
		  $hasilEDM       = $this->db->query("select * from visitasi_edm where madrasah_id='{$rm->id}'")->row();
		
		  $skor_rombel  = $this->db->query("SELECT skor FROM `kriteria_rombel` where $persen_rombel BETWEEN nilai1 and nilai2")->row();
		  $skor_toilet  = $this->db->query("SELECT skor FROM `kriteria_toilet` where $toilet_persen BETWEEN nilai1 and nilai2")->row();
		  $skor_edm     = $hasilEDM->nilai_kedisiplinan+$hasilEDM->nilai_pengembangan_diri+$hasilEDM->nilai_proses_pembelajaran+$hasilEDM->nilai_sarana_prasarana+$hasilEDM->nilai_pembiayaan;
		  


		  $skor_edm2     = $skor_edm;
		  $skor_pip2     = $rm->skor_pip;
		  $skor_rombel2  = $skor_rombel->skor;
		  $skor_toilet2  = $skor_toilet->skor;
		  
		  
		 
		  $skor_akhir   = $skor_edm2 + $skor_pip2 + $skor_rombel2 + $skor_toilet2;

				$this->db->set("skor_edm_visitasi",$skor_edm2);			
				$this->db->set("skor_rombel_visitasi",$skor_rombel2);
				$this->db->set("skor_toilet_visitasi",$skor_toilet2);
				$this->db->set("skor_akhir_visitasi",$skor_akhir);				
				$this->db->where("id",$rm->id);
				$this->db->update("madrasahedm");
		}





  

				
		 
		
			$afirmasi = $this->db->query("select * from madrasahedm where dnt_longlist=1 and tahun='{$tahun}' order by skor_akhir_visitasi ASC,skor_akhir ASC  LIMIT 33 ")->result();
		  
				foreach($afirmasi as $rm){
		
							$this->db->set("dnt",1);						
							$this->db->where("id",$rm->id);
							$this->db->update("madrasahedm");
		
				}
		
		redirect(base_url()."pengaturan/shortlist");


	}

	 public function generate_pip(){
		   
		$m     = $this->db->get_where("madrasahedm",array("tahun"=>2022))->result();
	
		   foreach($m as $rm){
			  
			 $jml_siswa  = $this->Reff->get_kondisi(array("nsm"=>$rm->nsm,"tahun"=>$rm->tahun),"datamadrasahemis_csv","jumlah_siswa");
			 $siswa_pip  = $this->db->query("select count(id) as jml from siswa_pip where nsm='".$rm->nsm."'")->row();
		     $piloting   	= $this->db->query("select dnt from madrasahedm where nsm='".$rm->nsm."'")->row();
		     $penerima2021   = $this->db->query("SELECT count(id) as jml from madrasahedm where shortlist=1 and pernyataan=0 and tahun='2021' and nsm  IN(SELECT nsm FROM conf_bkba where bersedia_menerima ='Ya') and nsm='".$rm->nsm."'")->row();
			 $simsarpras = $this->db->query("select  id,bantuan,nilai from madrasah_penerima where nsm='".$rm->nsm."' and anggaran='2022'")->row();
			 $pupr       = $this->db->query("select  * from penerima_bantuan where anggaran LIKE '%2022%' and kategori ='PUPR' AND nsm='".$rm->nsm."'")->row();
             $persen_pip =0;

			  if($siswa_pip->jml !=0 AND $jml_siswa !=0){
			     $persen_pip = ($siswa_pip->jml/$jml_siswa) * 100;
			  }

			  if(!is_null($pupr)){
				
				$this->db->set("pupr",1);
				$this->db->set("blacklist",1);
				

			  }

			  if(!is_null($simsarpras)){
				$nilai = $this->Reff->formatuang2($simsarpras->nilai);
				$this->db->set("simsarpras",1);
				$this->db->set("blacklist",1);
				$this->db->set("simsarpras_bantuan",$simsarpras->bantuan."-".$nilai);

			  }

			  if(!is_null($piloting)){
				if($piloting->dnt==1){
				  $this->db->set("blacklist",1);
				  $this->db->set("sudah_menerima_bkba",1);
				}
				

			  }

			  if($penerima2021->jml > 0){
				
				  $this->db->set("blacklist",1);
				  $this->db->set("sudah_menerima_bkba",1);
				
				

			  }
					
			
			 $this->db->set("siswa_pip",$siswa_pip->jml);
			 $this->db->set("persen_pip",$persen_pip);
			 $this->db->where("id",$rm->id);
			 $this->db->update("madrasahedm");

		   }

		   echo "sukses";

	 }

	 public function generate_emis(){
		
		   
		$m     = $this->db->get_where("madrasahedm",array("tahun"=>2022))->result();
		  
		 $arrrombel = array("mi"=>28,"mts"=>32,"ma"=>36); 
		 
		   foreach($m as $rm){
			  
			 $npsn           = $this->Reff->get_kondisi(array("nsm"=>$rm->nsm,"tahun"=>$rm->tahun),"datamadrasahemis_csv","npsn");
			 $jumlah_rombel  = (int)$this->Reff->get_kondisi(array("nsm"=>$rm->nsm,"tahun"=>$rm->tahun),"datamadrasahemis_csv","jumlah_rombel");
			 $jml_siswa      = (int)$this->Reff->get_kondisi(array("nsm"=>$rm->nsm,"tahun"=>$rm->tahun),"datamadrasahemis_csv","jumlah_siswa");
			 $jml_guru   = (int)$this->Reff->get_kondisi(array("nsm"=>$rm->nsm,"tahun"=>$rm->tahun),"datamadrasahemis_csv","jumlahguru");
			 $rombel     = (int)$this->Reff->get_kondisi(array("nsm"=>$rm->nsm,"tahun"=>$rm->tahun),"datamadrasahemis_csv","ruang_belajar");
			 
			 $toilet_guru 		 = $this->Reff->get_kondisi(array("nsm"=>$rm->nsm,"tahun"=>$rm->tahun),"datamadrasahemis_csv","toilet_guru");
			 $toilet_siswa 		 = $this->Reff->get_kondisi(array("nsm"=>$rm->nsm,"tahun"=>$rm->tahun),"datamadrasahemis_csv","toilet_siswa");
			 $toilet_total 		 = (int)$toilet_guru + (int)$toilet_siswa;

			 $akreditasi_huruf  = $this->Reff->get_kondisi(array("nsm"=>$rm->nsm,"tahun"=>$rm->tahun),"datamadrasahemis_csv","akreditasi_huruf");
			 $siswa_laki   		 = (int)$this->Reff->get_kondisi(array("nsm"=>$rm->nsm,"tahun"=>$rm->tahun),"datamadrasahemis_csv","siswa_laki");
			 $siswa_perempuan    = (int)$this->Reff->get_kondisi(array("nsm"=>$rm->nsm,"tahun"=>$rm->tahun),"datamadrasahemis_csv","siswa_perempuan");
			 $abk                = (int)$this->Reff->get_kondisi(array("nsm"=>$rm->nsm,"tahun"=>$rm->tahun),"datamadrasahemis_csv","siswa_berkebutuhan_khusus");
       
			 // RUang Belajar
			 $rombel_ideal  = round($jml_siswa/$arrrombel[$rm->jenjang]);
			
			 $rombel_persen=0;
			 if($rombel !=0 and $rombel_ideal !=0){
				$rombel_persen = ($rombel/$rombel_ideal) * 100;
			 }

			 // Hitung Toilet 
			  if($rm->jenjang=="mi"){

                $toiletIdeal_laki = round($siswa_laki/60);
                $toiletIdeal_Per  = round($siswa_perempuan/50);

			  }else{

				$toiletIdeal_laki = round($siswa_laki/40);
				$toiletIdeal_Per  = round($siswa_perempuan/30);
				
			  }

			  $toilet_ideal  = $toiletIdeal_laki + $toiletIdeal_Per;
			  $toilet_persen=0;
			  if($toilet_total !=0 and $toilet_ideal !=0){
			  $toilet_persen = ($toilet_total/$toilet_ideal) * 100;
			  }




			 $this->db->set("npsn",$npsn);
			 $this->db->set("toilet_guru",$toilet_guru);
			 $this->db->set("toilet_siswa",$toilet_siswa);
			 $this->db->set("toilet_total",$toilet_total);
			 $this->db->set("jumlah_rombel",$jumlah_rombel);
			 $this->db->set("akreditasi",$akreditasi_huruf);
			 $this->db->set("jml_siswa",$jml_siswa);
			 $this->db->set("jml_guru",$jml_guru);
			 $this->db->set("rombel",$rombel);  // RUang Belajar
			 $this->db->set("rombel_ideal",$rombel_ideal);  // RUang Belajar
			 $this->db->set("rombel_persen",$rombel_persen);  // RUang Belajar

			 $this->db->set("toilet_jumlah",$toilet_total);
			 $this->db->set("toilet_ideal",$toilet_ideal);
			 $this->db->set("toilet_persen",$toilet_persen);

			 $this->db->set("siswa_laki",$siswa_laki);
			 $this->db->set("siswa_perempuan",$siswa_perempuan);
			 $this->db->set("inklusi",$abk);
			 $this->db->set("abk",$abk);


			 
			 $this->db->where("id",$rm->id);
			 $this->db->update("madrasahedm");

		   }

		   echo "sukses";

	 }


	 public function authapisave()
    {
       $service = new Google_Drive();
       $aksestoken = $this->input->get_post("userauth");
	   $service->fetchAccessTokenWithAuthCode($aksestoken);
	   redirect("/pengaturan/aktivasi");
    }
	 public function hapusGdrive()
    {
       $service = new Google_Drive();
	   $service->hapusToken();
	  
	   redirect("/pengaturan/aktivasi");
    }


	public function generate_emisDNT(){
		$tahun   = $this->Reff->tahun();
		$m =  $this->db->get_where("madrasahedm",array("tahun"=>$tahun,"dnt_longlist"=>1))->result();
		  
		 $arrrombel = array("mi"=>28,"mts"=>32,"ma"=>36); 
		 
		   foreach($m as $rm){
			  
			 $jumlah_rombel  = $this->db->query("select total from visitasi_rombel where madrasah_id='{$rm->id}'")->row();
			 $jml_siswa      = $this->db->query("select total from visitasi_pesertadidik where madrasah_id='{$rm->id}'")->row();
			 $jml_guru       = $this->db->query("select (guru_tetap+guru_tidak_tetap) as total from visitasi_guru where madrasah_id='{$rm->id}'")->row();
			 $rombel         = $this->db->query("select total from visitasi_ruangbelajar where madrasah_id='{$rm->id}'")->row();

			 $jumlah_rombel  = $jumlah_rombel->total;
			 $jml_siswa      = $jml_siswa->total;
			 $jml_guru       = $jml_guru->total;
			 $rombel         = $rombel->total;


			 
			 $toilet_guru 		 =  $this->db->query("select (guru) as total from visitasi_toilet where madrasah_id='{$rm->id}'")->row();
			 $toilet_siswa 		 =  $this->db->query("select (siswa+siswi) as total from visitasi_toilet where madrasah_id='{$rm->id}'")->row(); 
			 $toilet_total 		 = $toilet_guru->total + $toilet_siswa->total;

			
       
			 $rombel_ideal       = $rm->rombel_ideal;
			
			
		
			  $rombel_persen = ($rombel/$rombel_ideal) * 100;
		

			

			  $toilet_ideal      = $rm->toilet_ideal;
			 
			 
			  $toilet_persen = ($toilet_siswa->total/$toilet_ideal) * 100;
			  




			 $this->db->set("rombel_visitasi",$rombel);			 
			 $this->db->set("rombel_persen_visitasi",$rombel_persen);

			 $this->db->set("toilet_jumlah_visitasi",$toilet_siswa->total);			
			 $this->db->set("toilet_persen_visitasi",$toilet_persen);

			
			 $this->db->where("id",$rm->id);
			 $this->db->update("madrasahedm");

		   }

		   echo "sukses";

	 }
	

	 
	
	 
}
