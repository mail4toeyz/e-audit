   <div id="showform"></div>        
                                 
					  <div class="card form" id="form1">
                        <div class="card-header">
                           <?php 
						  $arje = array("1"=>"MI/SD Sederajat","2"=>"MTS/SMP Sederajat","3"=>"MA/SMA Sederajat");
						 ?>
                            <h3><i class="fa fa-users"></i> Penilaian Hasil Tes <?php echo $arje[$jenjang]; ?></h3>
                        </div>
                        <br>
                         
                            <div class="col-md-12">
							<div class="alert alert-info" style="font-size:12px">
							 Petunjuk ! <br>
							 Dibawah ini adalah hasil pelaksanaan CBT KSM tingkat Kabupaten/Kota,
							 Anda dapat monitor pelaksanaan CBT secara realtime pada laman ini 
							</div>
								
			                   
			 
			 
			              </div>
						  
						  <form>
							<div class="col-md-12">
					 					 <div class="row">
										 
										    <div class="col-md-2"> <b> PENCARIAN  : </b></div>
                                           
										   
										
										 
										  
										     <div class="col-md-3">
												<div class="form-group ">
													<select id="trkompetisi_id" class="form-control">
													<option value=""> - Cari kompetisi -</option>
													  <?php 
													    $ko = $this->db->get_where("tr_kompetisi",array("tmmadrasah_id"=>$jenjang))->result();
														  $a=0;
														  foreach($ko as $r){
															  $a++;
															  
															?> <option value="<?php echo $r->id; ?>" > <?php echo $r->nama; ?> </option><?php   
															  
														  }
														  
														?>
													  
													  
													
													</select>
												</div>
										  </div>
										  
										  
										   <div class="col-md-3">
													
													<div class="form-group">
															<input type="text" size="30" name="keyword" id="keyword" class="form-control" placeholder="Cari Nama Siswa ">
															<input type="hidden" id="jenjang" class="form-control" value="<?php echo $jenjang; ?>">
															
														
												   </div>
											 </div>
											 
										 <div class="col-md-1"><button type="button" class="btn btn-danger "  id="searchcustom"><span class="fa fa-search"></span> </button>
								      
										  
										  
										 
										 </div>
									
							
					</div>
					</div>
			</form>
			
		           <hr>
		<div class="col-md-12">  
		          
					 
					    
		
		<div class="table-responsive">
			<table id="datatableTable" class=" table table-bordered table-hover table-striped" width="100%">
			  <thead>
				<tr>
				   <th class="center" width="3%" rowspan="2">NO</th>
				   <th class="center" rowspan="2">PIAGAM </th>
				   <th class="center" rowspan="2">FOTO  </th>
				   <th class="center" rowspan="2">NO PESERTA </th>
				   <th class="center" rowspan="2">NAMA PESERTA </th>
				   <th class="center" rowspan="2">TGL LAHIR </th>
				   <th class="center" rowspan="2">PROVINSI</th> 
				   <th class="center" rowspan="2">KAB/KOTA </th> 
				   <th class="center" rowspan="2">KOMPETISI</th>
				   <th class="center" rowspan="2">STATUS </th>
				   
				 				  
				   <th class="center" colspan="6">HASIL CBT  </th>
				   <th class="center" colspan="4">DETAIL  LEMBAGA </th>
				   <th class="center" rowspan="2">CATATAN</th>
				   <th class="center" rowspan="2">KONFIRMASI</th>
				</tr>
				<tr>
				  <th> BNR_PG </th>
				  <th> BNR_ESSAY </th>
				  <th> JML_BENAR </th>
				  <th> SALAH  </th>
				  <th> KOSONG PG & SALAH ESSAY </th>
				  <th> NILAI </th>


				  <th> JENIS </th>
				  <th> JENJANG </th>
				  <th> NSM/NPSN </th>
				  <th> LEMBAGA  </th>
			
				</tr>
			  </thead>
			  <tbody></tbody>
            </table>
			</div>
			
		
        </div>
        </div>
		
<div id="myModalgg" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> </h4>
      </div>
      <div class="modal-body" id="load_data">
        loading.. mohon tunggu 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<b style="color:white;background:#33AFFF">Data sedang di tampilkan..</b>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data tidak atau belum tersedia, silahkan input data",
						   "sLengthMenu": "Default menampilkan   _MENU_ Data <small>(Klik pada angkanya untuk memuat lebih banyak data )</small>"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": true,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						{
							extend: 'excelHtml5',
							
							text:'<span class="fa fa-file-excel-o"></span> Cetak Excel',
							className :"btn btn-danger btn-sm"
							},
							
						
							{
							extend: 'colvis',
							
							text:'<span class="fa fa-columns "></span> Setting Kolom ',
							className :"btn btn-danger "
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("klokal/g_hasiltes"); ?>", 
						type: "post", 
						"data": function ( data ) {
						 data.kota    = $('#kota').val();
						 data.jenjang = $('#jenjang').val();
						 data.madrasah = $('#madrasah').val();
						 data.trkompetisi_id = $('#trkompetisi_id').val();
						
						 data.keyword = $('#keyword').val();
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
	
	
		
			$(document).on('change', '#kota,#jenjang,#madrasah,#trkompetisi_id', function (event, messages) {			
			 
			     dataTable.ajax.reload(null,false);	        
		  
        });
		
			$(document).on('input', '#keyword', function (event, messages) {			
			 
			     dataTable.ajax.reload(null,false);	        
		  
        });
		
		
		 $(document).on("change",".onchange2",function(event){
				 event.preventDefault()
				 var kota    = $("#kota").val();
				 var jenjang    = $("#jenjang").val();
			
				 
				 loading();
				
				 $.post("<?php echo site_url("publik/madrasah"); ?>",{kota:kota,jenjang:jenjang},function(data){
					 
					 $("#madrasah").html(data).show();
				
					 
					   
						jQuery.unblockUI({ });
						
						
				 })
			  
			  
			  
			 })

			 
			  $(document).on("change","#jenjang",function(event){
				 event.preventDefault()
				 var tmmadrasah_id    = $(this).val();
				 
			
				 
				 loading();
				
				 $.post("<?php echo site_url("publik/kompetisi"); ?>",{tmmadrasah_id:tmmadrasah_id},function(data){
					 
					 $("#trkompetisi_id").html(data).show();
				
					 
					   
						jQuery.unblockUI({ });
						
						
				 })
			  
			  
			  
			 })
		
		
		  	 $(document).off('input', '.nilai').on('input', '.nilai', function (event, messages) {			
			 
			 var tmsiswa_id = $(this).attr("tmsiswa_id");
			 var status      = $(this).attr("status");
			 var jumlah     = $(this).val();
			 
			 
			
			  if(jumlah % 1 !=0){
			      $(this).val("");
			      alertify.alert("Inputan hanya diperbolehkan angka");
			      return false;
			  }
			  
			   $.post("<?php echo site_url("klokal/nilaites"); ?>",{tmsiswa_id:tmsiswa_id,jumlah:jumlah,status:status},function(data){
				   
				   
				   $("#nilaiakhir"+tmsiswa_id).html(data);
				   
			   })
			  	  
        });

        

</script>
			
                    