           <div class="row">
                <div class="col-md-12">                      
					  <div class="card form" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-user"></i> <?php echo $title; ?> </h3>
                        </div>
                        <br>
                          <?php $attributes = array('class' => 'form-horizontal', 'role'=>'form','name' => 'simpangambar', 'id' => 'simpangambar', 'method' => "post","enctype"=>"multipart/form-data", 'url' =>site_url("lokal/simpanpeserta")); ?>
			                     <?php echo form_open("javascript:void(0)", $attributes); ?>
                            <div class="col-md-12">
							<div class="alert alert-info">
							 Petunjuk ! <br>
							Upload Semua berkas persyaratan atas nama <i><?php echo $dataform->nama;  ?> </i>
							 </div>
                            <div class="col-md-12">
							
									<table class="table table-hover">
									    <thead>
										  <tr>
										    <td> No </td>
											<td> Nama Persyaratan</td>
											<td> Status </td>
											<td> Upload Berkas  </td>
											<td> Berkas   </td>
										  </tr>
										 </thead>
										 
										 <?php 
										 $no=1;
										   foreach($this->db->get("tm_persyaratan")->result() as $r){
											     $data = $this->db->get_where("tr_persyaratan",array("tmpersyaratan_id"=>$r->id,"tmsiswa_id"=>$dataform->id))->row();
												 
												 
											?>
														
											 <tr>
												<td> <?php echo $no++; ?> </td>
												<td> <?php echo $r->nama; ?></td>
												<td> <?php echo (count($data) >0) ? "sudah di upload" :"Belum di upload"; ?> </td>
												<td> 

													  <div class="form-group">
														<label for="name">Format jpg/png (max 3 MB) </label>
														<input type="file" class="form-control" name="file" tmpersyaratan_id="<?php echo $r->id; ?>" tmsiswa_id="<?php echo $dataform->id; ?>" persyaratan="<?php echo $r->nama; ?>">
													</div>
													
													
													
													





												</td>
												<td>
												<div class="form-group">
													   <?php 
														 if(count($data) >0 ){
															 if($data->nama==""){
																$file = "not.png";
															 }else{
																 $file = $data->nama;
															 }
															 
														 }else{
															 
																$file = "not.png";
														 }
														 
														 ?>
														<img src="<?php echo base_url(); ?>__statics/img/peserta/berkas/<?php echo $file; ?>" class="img-thumbnail img-responsive" style="height:200px" id="blah<?php echo $no; ?>" >
													</div>
											</td>
											  </tr>



											<?php 
											   
											   
										   }
										  ?>
									</table>
											
										
										
									</div>
									
									
									
									
							
							
							
							
                             

							<center>
                            <button type="button" class="btn btn-general btn-white" id="cancelr">Simpan Data </button>  
                            <button type="reset" class="btn btn-general btn-blue">Reset </button> 
                            <button type="button" class="btn btn-general btn-white" id="cancel">Tutup Form </button> 
							<center>
							<br>
                        <?php echo form_close(); ?>
                    </div>
                    </div>
                    </div>
					
<script>
  
  
   
$(document).off('change',"input[type=file]").on('change',"input[type=file]",function(){
            var tmpersyaratan_id     = $(this).attr("tmpersyaratan_id");
            var persyaratan			 = $(this).attr("persyaratan");
            var tmsiswa_id			 = $(this).attr("tmsiswa_id");

             
			 
            
			var file = this.files[0];
            var form = new FormData();
            form.append('tmpersyaratan_id', tmpersyaratan_id);
            form.append('persyaratan', persyaratan);
           
            form.append('tmsiswa_id', tmsiswa_id);
            form.append('file', file);
          
			loading();
            $.ajax({
                url : "<?php echo site_url("lokal/u_persyaratan"); ?>",
                type: "POST",
                cache: false,
                contentType: false,
                processData: false,
                data : form,
                success: function(response){
				    
					var data = response.split("[split]");
					  if(data[0]=="sukses"){
						  alertify.success("Upload Berhasil");
						  
					  }else{
						  
						 alertify.error("Upload Gagal karena "+data[1]);
					  }
				    
				   jQuery.unblockUI({ });	  
                }
            });
		});
</script>