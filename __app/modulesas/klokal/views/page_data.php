           <div class="row">
                <div class="col-md-12">                      
					  <div class="card form" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-institution"></i> Data Madrasah/Sekolah </h3>
                        </div>
                        <br>
                          <?php $attributes = array('class' => 'form-horizontal', 'role'=>'form','name' => 'simpannorespon', 'id' => 'simpannorespon', 'method' => "post", 'url' =>site_url("lokal/perbaharui_data")); ?>
			                     <?php echo form_open("javascript:void(0)", $attributes); ?>
                            <div class="col-md-12">
                            <div class="row">
							
									<div class="col-md-5">
										<div class="form-group">
											<label for="name">NPSN /NSM </label>
											<input type="text" class="form-control" name="f[npsn]" value="<?php echo $m->npsn; ?>">
										</div>
										<div class="form-group">
											<label for="pro-qu">Nama Madrasah/Sekolah </label>
											<input type="text" class="form-control" name="f[nama]" value="<?php echo $m->nama; ?>">
										</div>
									</div>
									
									 <div class="col-md-5">
										<div class="form-group">
											<label for="name">Telepon  </label>
											<input type="text" class="form-control" name="f[telepon]" value="<?php echo $m->telepon; ?>">
										</div>
										<div class="form-group">
											<label for="pro-qu">Email </label>
											<input type="email" class="form-control" name="f[email]" value="<?php echo $m->email; ?>">
										</div>
									</div>
							</div>
							
							
							 <div class="row">
							
									<div class="col-md-12">
										<div class="form-group">
											<label for="name">Jenjang Pendidikan  </label>
											<select class="form-control" name="f[jenjang]" id="jenjang">
														  <?php 
													    foreach($this->Di->jenjang() as $i=>$r){
															
															?><option value="<?php echo $i; ?>" <?php echo ($i==$m->jenjang) ? "selected":""; ?>> <?php echo $r; ?></option><?php 
															
														}
														?>
													   
										    </select>
										</div>
										
									</div>
									
									 
							</div>
							
							 <div class="row">
							
									<div class="col-md-5">
										<div class="form-group">
											<label for="name">Provinsi  </label>
											<?php echo $this->Di->get_kondisi(array("id"=>$m->provinsi),"provinsi","nama"); ?>
										</div>
										
									</div>
									
									 <div class="col-md-5">
										<div class="form-group">
											<label for="name">Kab/Kota   </label>
											<?php echo $this->Di->get_kondisi(array("id"=>$m->kota),"kota","nama"); ?>
										</div>
									</div>
										
							</div>
							
							<div class="row">
							
									<div class="col-md-12">
										<div class="form-group">
											<label for="name">Alamat   </label>
											 <textarea name="f[alamat]" type="text" class="form-control" id="alamat"> <?php echo $m->alamat; ?></textarea>
			                                   
										</div>
										
									</div>
									
									 
										
							</div>
							
                             

							<center>
                            <button type="submit" class="btn btn-general btn-white">Perbaharui </button>  
                            <button type="reset" class="btn btn-general btn-blue">Reset </button> 
							<center>
							<br>
                        <?php echo form_close(); ?>
                    </div>
                    </div>
                    </div>