   <div id="showform"></div>        
  						 <?php 
						$status_siswa = $this->input->get_post("status");
					   ?>                       
					  <div class="card " id="form1">
                        <div class="card-header">
						<?php 
						  $arje = array("1"=>"MI/SD Sederajat","2"=>"MTS/SMP Sederajat","3"=>"MA/SMA Sederajat");
						 ?>
                            <h3><i class="fa fa-users"></i> Verifikasi Peserta <?php echo $arje[$jenjang]; ?></h3>
                        </div>
                        <br>
                         
                            <div class="col-md-12">
							<div class="alert alert-info" style="font-size:10px">
							 Petunjuk ! <br>
							 <?php 
							 if(!empty($status_siswa)){
								?>	 Dibawah ini adalah data Peserta yang sudah dilakukan verifikasi lulus
					  			<?php 

							 }else{
								?>	 Silahkan lakukan verifikasi Peserta KSM dibawah ini, Peserta yang Anda verifikasi akan mengikuti KSM Tingkat Kabupaten/kota diwilayah Anda
								<?php 

							 }
							 ?>
						      <br>
							</div>
								
			 
			 
			              </div>
						  
						  <form>
							<div class="col-md-12">
					 <div class="row">
					  
					 <input type="hidden" id="status_siswa" value="<?php echo $status_siswa; ?>">
										 
										  
										     <div class="col-md-3">
												<div class="form-group ">
													<select id="trkompetisi_id" class="form-control">
													<option value=""> - Cari kompetisi -</option>
													  <?php 
													    $ko = $this->db->get_where("tr_kompetisi",array("tmmadrasah_id"=>$jenjang))->result();
														  foreach($ko as $r){
															?> <option value="<?php echo $r->id; ?>"> <?php echo $r->nama; ?> </option><?php   
															  
														  }
														  
														?>
													  
													  
													
													</select>
												</div>
										  </div>
										  
										  <div class="col-md-3">
												<div class="form-group ">
													<select id="status" class="form-control">
													<option value=""> - Cari Status Verifikasi -</option>
													
												
													<option value="0"> Draft </option>
													<option value="1"> Belum diverifikasi</option>
													<option value="3"> Tidak Lulus </option>
													  
													
													</select>
												</div>
										  </div>
										  <div class="col-md-2">
												<div class="form-group ">
												<select id="kolom" class="form-control">
												<option value="nisn">  NISN </option>
												<option value="nama" selected>  Nama Peserta </option>
												<option value="username">  NSM/NPSN Lembaga </option>
												<option value="madrasah"> Nama Lembaga </option>
												
												 
												
												</select>
										       </div>
										 </div>
										  
										   <div class="col-md-3">
													
													<div class="form-group">
															<input type="text" size="30" name="keyword" id="keyword" class="form-control" placeholder="Cari disini ">
															<input type="hidden" id="jenjang" class="form-control" value="<?php echo $jenjang; ?>">
															
														
												   </div>
											 </div>
											 
										 <div class="col-md-1"><button type="button" class="btn btn-danger "  id="searchcustom"><span class="fa fa-search"></span> </button>
								       
										  
										 
										 </div>
									
							
					</div>
					</div>
			</form>
			
		           <hr>
		<div class="col-md-12">   				
		   <div class="table-responsive">
			<table id="datatableTable" class=" table table-bordered table-hover table-striped" width="100%">
			  <thead>
				<tr>
				   <th class="center" width="3%" rowspan="2">NO</th>
				   <!-- <th class="center" rowspan="2">NOPESERTA</th> -->
				   <th class="center" rowspan="2">STATUS</th> 
				  
				   <th class="center" rowspan="2">KOMPETISI</th>				   	
				   <th class="center" rowspan="2">FOTO </th>   
				   <th class="center" rowspan="2">NISN </th>		  
				   <th class="center" rowspan="2">NAMA </th>		  
				   <th class="center" rowspan="2">GENDER</th>				  
				   <th class="center"  rowspan="2">TTL</th>	
				   <th class="center" colspan="5"> DETAIL TES </th>
				   <th class="center" colspan="7">ASAL LEMBAGA </th>
				  
				</tr>
				<tr>
				<th> LOKASI </th>
				  <th> SIMULASI </th>
				  <th> PELAKSANAAN </th>
				  <th> SESI  </th>
				  <th> RUANG  </th>
				  <th> JENIS </th>
				  <th> JENJANG </th>
				  <th> NSM/NPSN </th>
				  <th> LEMBAGA  </th>
				  <th> KECAMATAN  </th>
				  <th> DESA  </th>
				  <th> KONTAK </th>

			


				</tr>
			  </thead>
			  <tbody></tbody>
            </table>
			</div>
        </div>
        </div>
		



<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<b style="color:white;background:#33AFFF">Data sedang di tampilkan..</b>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data tidak atau belum tersedia",
						   "sLengthMenu": "Default menampilkan   _MENU_ Data <small>(Klik pada angkanya untuk memuat lebih banyak data sebelum mencetak excel)</small>"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": true,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							
							text:'<span class="fa fa-file-excel-o"></span> Cetak Excel',
							className :"btn btn-danger btn-sm"
							},
						
							{
							extend: 'colvis',
							
							text:'<span class="fa fa-columns "></span> Setting Kolom ',
							className :"btn btn-danger "
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("klokal/g_peserta"); ?>", 
						type: "post", 
						"data": function ( data ) {
							
						 data.kota    = $('#kota').val();
						 data.kolom    = $('#kolom').val();
						 data.jenjang = $('#jenjang').val();
						 data.madrasah = $('#madrasah').val();
						 data.trkompetisi_id = $('#trkompetisi_id').val();
						 data.status = $('#status').val();
						 data.keyword = $('#keyword').val();
						 data.status_siswa = $('#status_siswa').val();
						
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
	
		
		$(document).on('input click change', '#keyword,#searchcustom', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
		$(document).on('input', '#keyword', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
		
		$(document).off("click",".approv").on("click",".approv",function(){
			var tmsiswa_id = $(this).attr("tmsiswa_id");
			
			if($(this).is(':checked')){
				
				var status = 1;
			}else{
				var status = 0;
			}
			
			$.post("<?php echo site_url("klokal/status_peserta"); ?>",{tmsiswa_id:tmsiswa_id,status:status},function(){
				
				
				alertify.success("Perubahan Berhasil");
				
			})
			
		})
		
		$(document).on('change', '#kota,#jenjang,#madrasah,#trkompetisi_id,#status', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
			
	
				
				
		$(document).off("click",'input[name="status"]').on("click",'input[name="status"]',function(){
			
			var status= $('input[name="status"]:checked').val();
			var tmsiswa_id= $(this).attr("tmsiswa_id");
			  
			  loading();
				
				 $.post("<?php  echo site_url("klokal/save_verval"); ?>",{tmsiswa_id:tmsiswa_id,status:status},function(data){
					   
					    alertify.success("Verifikasi Berhasil dilakukan");
						dataTable.ajax.reload(null,false);	
						jQuery.unblockUI({ });
						
						
				 })
			
		})
		
		$(document).off('input', '#keterangan').on('input', '#keterangan', function (event, messages) {			
			 
			   var tmsiswa_id= $(this).attr("tmsiswa_id"); 
			   
               var keterangan = $(this).val();
			  
			   
			   $.post("<?php echo site_url("klokal/save_keterangan"); ?>",{tmsiswa_id:tmsiswa_id,keterangan:keterangan},function(data){
					   
					    
						
				 })
		  
        });
		
		
		 $(document).on("change",".onchange2",function(event){
				 event.preventDefault()
				 var kota    = $("#kota").val();
				 var jenjang    = $("#jenjang").val();
			
				 
				 loading();
				
				 $.post("<?php echo site_url("publik/madrasah"); ?>",{kota:kota,jenjang:jenjang},function(data){
					 
					 $("#madrasah").html(data).show();
				
					 
					   
						jQuery.unblockUI({ });
						
						
				 })
			  
			  
			  
			 })

			 
			  $(document).on("change","#jenjang",function(event){
				 event.preventDefault()
				 var tmmadrasah_id    = $(this).val();
				 
			
				 
				 loading();
				
				 $.post("<?php echo site_url("publik/kompetisi"); ?>",{tmmadrasah_id:tmmadrasah_id},function(data){
					 
					 $("#trkompetisi_id").html(data).show();
				
					 
					   
						jQuery.unblockUI({ });
						
						
				 })
			  
			  
			  
			 })

</script>
			
                    