
 <script type="text/javascript">
   var url_persyaratan ='<?php echo base_url(); ?>klokal/save_pakta';
   var csrf_token      ='gtg';
   var csrf_hash       ='gtk';
   var trpersyaratan_id='<?php echo $_SESSION['tmkota_id']; ?>';
   var namapersyaratan ='pakta';
   var format    	   ='jpg,png,jpeg';
 
 </script>
 <link href="<?php echo base_url(); ?>__statics/js/dropzone/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url(); ?>__statics/js/dropzone/js/fileinput.js" type="text/javascript"></script>
			
 
 <?php 
  $datakota = $this->db->get_where("kota",array("id"=>$_SESSION['tmkota_id']))->row();
  ?>
 <?php $attributes = array('class' => 'form-horizontal', 'role'=>'form','name' => 'simpan', 'id' => 'save','class'=>'dropzone', 'method' => "post", 'url' =>site_url("dashpendaftar/simpanlokasi"),"enctype"=>"multipart/form-data"); ?>
                                            <?php echo form_open(site_url("klokal/save_pakta"), $attributes); ?>

 <div class="row">
                <div class="col-md-12">                      
					  <div class="card form" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-user"></i> PAKTA INTEGRITAS</h3>
                        </div>

						
		 <div class="alert alert-info">
		   <b>Perhatian !</b> <br>
		    <ol type="square">

			<li> Pakta Integritas  yang sudah ditandatangan diatas materai Wajib di Upload disini </li>
			<li> Jika Pakta Integritas tidak di upload, Nilai Hasil tes tidak akan dimasuk ke database pusat  </li>
			<li> Batas maksimal ukuran file yang di upload adalah 1 MB</li>
		  </ol>
		 
		 </div>
        
		<?php 
		  if(!empty($datakota->fakta)){
		?>
			        <style>
					.dz-message{
						
						display:none;
					}
					
					
					</style>
					<div class="file-preview-thumbnails">
							
						<div class="file-preview-frame krajee-default  kv-preview-thumb" id="preview-1510251708028_30-0" data-fileindex="0" data-template="image"><div class="kv-file-content">
						<img src="<?php echo base_url(); ?>__statics/pakta/<?php echo $datakota->fakta; ?>" title="Pakta " alt="Loading.." style="width:auto;height:auto;max-width:100%;max-height:100%;">
				           
						</div><div class="file-thumbnail-footer">
							<div class="file-footer-caption" title="Pakta Integritas">
								<div class="file-caption-info">Pakta Integritas</div>
							
							</div>
							<div class="file-thumb-progress kv-hidden"><div class="progress">
							<div class="progress-bar bg-success progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
								Initializing...
							 </div>
						</div></div>
					<div class="file-actions">
							<div class="file-footer-buttons">
							    
								  <a href="javascript:void(0)" data-id="<?php echo $_SESSION['tmkota_id']; ?>" class="hapushela  kv-file-remove btn btn-kv btn-default btn-outline-secondary" title="Remove file"><i class="fa fa-trash"></i></a> 
						         <button  imgdata="<?php echo base_url(); ?>__statics/pakta/<?php echo $datakota->fakta; ?>" type="button"  class="gedekenbro kv-file-zoom btn btn-kv btn-default btn-outline-secondary" title="View Details" data-toggle="modal" data-target="#mypreview"><i class="fa fa-eye"></i></button>     </div>
						</div>

						<div class="clearfix"></div>
						</div>
						</div>

						</div>
						
						
							
			    
			   
			   
			   <?php 
			  
			  
			  
		  }else{
			  ?>
			        
			   <div class="form-group">
					<div class="file-loading">
						 <input id="file-4" accept="image/x-png, image/gif, image/jpeg" type="file" class="file"  name="file" data-upload-url="#">
					</div>
				</div>
		
		 <?php 
			  
			  
		  }
		  
		 ?>
        
        <hr>
       
        <hr>
        
    

				
				 <?php echo form_close(); ?>
		</div>		 
		</div>		 
		</div>		 
		
<script>
    $(document).off('change',"input[type=file]").on('change',"input[type=file]",function(){
		 
		
		  var ext = $('#file-4').val().split('.').pop().toLowerCase();
		  
			if ($.inArray(ext, ['png','jpg','jpeg']) == -1){
				
				 alertify.alert("File yang di upload harus gambar format (jpg/png)");
				 window.setTimeout( function(){
											 location.reload();
										 }, 2000 );
				 return false;
				 
				}else{
					    var picsize = (this.files[0].size);
							if (picsize > 1000000){
							       
							 
							  alertify.alert("Ukuran file tidak boleh melebihi 1 MB");
							  window.setTimeout( function(){
											 location.reload();
										 }, 2000 );
					           return false;
							}
				}
	});
	
    $(document).on("click",".hapushela",function(){
		var dataid = $(this).attr("data-id");
		alertify.confirm("Apakah anda yakin menghapus persyaratan ini ?",function(){
			
		   $.post("<?php echo site_url("klokal/hapus_pakta"); ?>",{id:dataid},function(){
			   
			   alertify.success("Berhasil dihapus");
			   location.reload();
			   
		     })
		});
		
		
	});
	
	
	
    $("#file-4").fileinput({
        theme: 'fa',
        uploadExtraData: {csrf_token: csrf_hash}
    });
    $(".btn-warning").on('click', function () {
        var $el = $("#file-4");
        if ($el.attr('disabled')) {
            $el.fileinput('enable');
        } else {
            $el.fileinput('disable');
        }
    });
    $(".btn-info").on('click', function () {
        $("#file-4").fileinput('refresh', {previewClass: 'bg-info'});
    });
	
	$(document).off("click",".kv-file-zoom").on("click",".kv-file-zoom",function(){
		
		var dataimg = $(this).attr("imgdata"); 
		
		 $("#loadbody").html("<center><img src='"+dataimg+"' class='img-responsive' ></center>");
	   
		
		
	})
	
    
</script>

<div id="mypreview" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Preview</h4>
      </div>
      <div class="modal-body" id="loadbody">
       Loading..
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
				
				