<?php $attributes = array('class' => 'form-horizontal', 'role'=>'form','name' => 'simpan', 'id' => 'simpan', 'method' => "post", 'url' =>site_url("dashpanitia/savelokasitespemetaan")); ?>
<?php echo form_open("javascript:void(0)", $attributes); ?>

<input type="hidden" name="id" class="form-control" value="<?php echo isset($dataform->id) ? $dataform->id:"";  ?>">
<input type="text" name="madrasah" class="form-control" value="<?php echo isset($dataform->tmmadrasahc_id) ? $dataform->tmmadrasahc_id:"";  ?>">
<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
							     <i class="icon-note "></i>
								   Form Pengaturan  Jenis Tes 
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
							
								
							</div>
							<div class="actions">
								
								<a class="btn btn-icon-only btn-default btn-sm fullscreen" href="javascript:;" data-original-title="" title="">
								</a>
								
							</div>
						</div>
						<div class="portlet-body" id="headerawal">
						
						           <div class="row"  id="errorvalidation">
										<div class="col-md-12">
											<div class="note note-danger note-bordered" style="display:none" id="messagevalidation">
											</div>
											
											<div class="note note-info note-bordered" >
											<b> Pengaturan Jenis Tes </b>
											</div>
										</div>
									</div>
									
									 
									   <div class="table-responsive">
									   
									      <table class="table table-hover table-bordered table-striped">
                                          
                                            <tr>
                                               <td colspan="3"><center>Jumlah Peserta </center> </td>
                                            </tr>
                                            <tr>
                                               <td>Dirumah </td>
                                               <td>Dimadrasah  </td>
                                               <td>Total  </td>
                                            </tr>
                                            <?php 
                                            $dirumah    = $this->db->query("select count(id) as pemilih from tm_siswa where pilihan1='{$dataform->tmmadrasahc_id}' and hasilpilihan IS NULL and status_persyaratan='1' and covid='dirumah' and tmmadrasah_id='2'")->row();
                                            $dimadrasah = $this->db->query("select count(id) as pemilih from tm_siswa where trlokasites_id='{$dataform->id}' and hasilpilihan IS NULL and status_persyaratan='1' and covid='dimadrasah' and tmmadrasah_id='2'")->row();
                                            ?>

                                            <tr>
                                               <td><?php echo $dirumah->pemilih; ?></td>
                                               <td><?php echo $dimadrasah->pemilih; ?></td>
                                               <td><?php echo $dirumah->pemilih+$dimadrasah->pemilih; ?></td>
                                            </tr>

                                            
										  </table>

										  <div class="alert alert-info">
											  <b> Perhatian </b>
											   Jika Anda ingin mengalihkan seluruh Peserta dilokasi ini untuk tes dirumah, silahkan klik tombol  dibawah ini :
										  </div> 
										  <br> 
											   <center> <button id="alihkankerumah" trlokasites_id='<?php echo $dataform->id; ?>' type="button" class="btn btn-primary"> Alihkan semua peserta tes dirumah </button></center>
											<br>

											<div class="alert alert-info">
											  <b> Perhatian </b>
											   Jika Anda ingin mengalihkan seluruh Peserta dilokasi ini untuk tes dimadrasah, silahkan klik tombol  dibawah ini :
										  </div> 
										  <br> 
											   <center> <button id="alihkankemadrasah" trlokasites_id='<?php echo $dataform->id; ?>'  type="button" class="btn btn-primary"> Alihkan semua peserta tes dimadrasah </button></center>
											
									   </div>
									
									 <div class="form-actions">
										<div class="row">
											<div class="col-md-5 navbar-right">
												
												<button type="button" class="btn default" id="cancel"><i class="fa fa-repeat "></i> Tutup </button>
											</div>
										</div>
									</div>
								
								<div class="clear"><br></div>
								
						</div>
</div>
						
<?php echo form_close(); ?>
	
				
							
							
<script type="text/javascript">
  $(document).off("click","#alihkankerumah").on("click","#alihkankerumah",function(){

   alert("Fitur ini hanya dapat dilakukan setelah proses seleksi berkas selesai");

  });

  $(document).off("click","#alihkankemadrasah").on("click","#alihkankemadrasah",function(){

alert("Fitur ini hanya dapat dilakukan setelah proses seleksi berkas selesai");

})
</script>