
<?php 

$dmadrasah = $this->db->get_where("tm_madrasah_c",array("id"=>$_SESSION['tmpanitia_id']))->row();
$lokasites  = $this->db->get_where("tr_lokasites",array("tmmadrasahc_id"=>$_SESSION['tmpanitia_id']))->result();
if(count($lokasites)>0){
foreach($lokasites as $d){

    
    if($_SESSION['tmmadrasah_id'] == 2){
                
        $jml_peserta=0;
            if($d->zona !=""){

               $jumlahpeserta  = $this->db->query("select count(id) as total from tm_siswa where trlokasites_id='".$d->id."'  and hasilpilihan IS NULL and status_persyaratan='1' and covid='dimadrasah'")->row();
                 $jml_peserta = $jumlahpeserta->total;
            }else{
                
                 $jml_peserta = 0;
                
            }
            $totpesertadirumah=0;
            if($d->mdr==0){
            $jumlahpeserta2  = $this->db->query("select count(id) as total from tm_siswa where pilihan1='".$_SESSION['tmpanitia_id']."' and hasilpilihan IS NULL and status_persyaratan='1' and covid='dirumah'")->row();
            $totpesertadirumah = $jumlahpeserta2->total;
            }


        }else{
            
            
               $jumlahpeserta  = $this->db->query("select count(id) as total from tm_siswa where trlokasites_id='".$d->id."'  and covid='dimadrasah' and hasilpilihan IS NULL and status_persyaratan='1'")->row();
                 $jml_peserta = $jumlahpeserta->total;

               if($d->mdr==0){
                $jumlahpeserta2  = $this->db->query("select count(id) as total from tm_siswa where pilihan1='".$_SESSION['tmpanitia_id']."' and covid='dirumah' and hasilpilihan IS NULL and status_persyaratan='1'")->row();
                $totpesertadirumah = $jumlahpeserta2->total;
                }
        }



        $totalpeserta = $totpesertadirumah+$jml_peserta;

        

?>

<?php $attributes = array('class' => 'form-horizontal', 'role'=>'form','name' => 'simpan', 'id' => 'simpannorespon2', 'method' => "post", 'url' =>site_url("dashpanitia/savesesi")); ?>
<?php echo form_open(site_url("dashpanitia/savesesi"), $attributes); ?>



<input type="hidden" name="id" class="form-control" value="<?php echo $d->id;   ?>">
<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
							     <i class="icon-note "></i>
								  Pengaturan Sesi Pelaksanaan Tes di <?php echo $d->nama; ?> (<?php echo ($d->mdr==0) ? "Koordinator":"Non Koordinator"; ?>)
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
							
								
							</div>
							<div class="actions">
								
								<a class="btn btn-icon-only btn-default btn-sm fullscreen" href="javascript:;" data-original-title="" title="">
								</a>
								
							</div>
						</div>
						<div class="portlet-body" id="headerawal">
						
						           <div class="row"  id="errorvalidation">
										<div class="col-md-12">
											<div class="note note-warning note-bordered" id="messagevalidation" style="font-size:16px">
                                            1 Pengawas akan ditugaskan untuk memonitoring 1 ruang virtual yang berisi 20 Peserta Tes <br>
                                             Jumlah Total Peserta Tes dimadrasah : <?php echo $jml_peserta; ?> Peserta <br>
                                             <?php 
                                             if($d->mdr==0){
                                                 ?>
                                             Jumlah Total Peserta Tes Rumah  : <?php echo $jumlahpeserta2->total; ?> Peserta <br>
                                             <?php 
                                             }
                                             ?>
                                             Jumlah Total Keseluruhan Peserta   : <?php echo $totalpeserta; ?> Peserta <br>

                                             <br>
                                            <h3><b> <u> Simulasi Perhitungan Pengawas dan Komputer di <?php echo $d->nama; ?></u> </b></h3>
                                             <ol type="square">
                                                <li> Dengan Jumlah total peserta <u><?php echo $totalpeserta; ?></u> , Jika ingin Pelaksanaan Tes dalam : </br>
                                                1 Sesi : Anda harus menyediakan <u><?php echo $jml_peserta; ?> Komputer </u>  dimadrasah dengan jumlah pengawas <u><?php echo ceil($totalpeserta/20); ?> Orang</u>   </br>
                                                2 Sesi : Anda harus menyediakan <u><?php echo ceil($jml_peserta/2); ?> Komputer </u>  dimadrasah dengan jumlah pengawas <?php echo ceil(ceil($totalpeserta/20) / 2); ?> Orang  masing-masing sesi,  Isi pada  sesi 1 = <?php echo ceil(ceil($totalpeserta/20) / 2); ?> dan Sesi 2 = <?php echo ceil(ceil($totalpeserta/20) / 2); ?> </u>   </br>
                                                3 Sesi : Anda harus menyediakan <u><?php echo ceil($jml_peserta/3); ?> Komputer </u>  dimadrasah dengan jumlah pengawas <?php echo ceil(ceil($totalpeserta/20) / 3); ?> Orang  masing-masing sesi,  Isi pada  sesi 1 = <?php echo ceil(ceil($totalpeserta/20) / 3); ?>, Sesi 2 = <?php echo ceil(ceil($totalpeserta/20) / 3); ?> dan Sesi 3 = <?php echo ceil(ceil($totalpeserta/20) / 3); ?> </u>   </br>
                                                4 Sesi : Anda harus menyediakan <u><?php echo ceil($jml_peserta/4); ?> Komputer </u>  dimadrasah dengan jumlah pengawas <?php echo ceil(ceil($totalpeserta/20) / 4); ?> Orang  masing-masing sesi,  Isi pada  sesi 1 = <?php echo ceil(ceil($totalpeserta/20) / 4); ?>, Sesi 2 = <?php echo ceil(ceil($totalpeserta/20) / 4); ?> , Sesi 3 = <?php echo ceil(ceil($totalpeserta/20) / 4); ?>  dan Sesi 4 = <?php echo ceil(ceil($totalpeserta/20) / 4); ?> </u>   </li>

                                             </ol>


                                             Keterangan : Jumlah Peserta tes dirumah adalah jumlah Peserta yang memilih Madrasah Anda sebagai pilihan 1 kemudian memilih tes dirumah. <br>
                                             Proses generate sesi peserta tes akan dilakukan order by peserta yang memilih tes dimadrasah terlebih dahulu kemudian dilanjutkan dengan peserta yang memilih tes dirumah <br>
                                         
                                             
                                            
                                            </div>
										</div>
									</div>
									
									 
								
									  
									
									 <div class="form-group">
											
											 <div class="col-md-12 " id="dayatampung">
												
												       <table class="table table-bordered" style="font-size:14px">
																						<tr> 
																						  <td width="30%"> Jumlah Pengawas Sesi 1 (Sabtu Pukul 07:30 ) </td>
																						  <td> <input type="number"  trlokasites_id="<?php echo $d->id; ?>" disabled name="sesi1" kolom="sesi1" value="<?php echo $d->sesi1; ?>"   placeholder="Isi jumlah pengawas " class="form-control setsesi"> </td>
																					
																						</tr>

                                                                                        <tr> 
																						  <td> Jumlah Pengawas Sesi 2 (Sabtu Pukul 13:00 ) </td>
																						  <td> <input type="number"   trlokasites_id="<?php echo $d->id; ?>" disabled name="sesi2" kolom="sesi2" value="<?php echo $d->sesi2; ?>"  placeholder="Isi jumlah pengawas " class="form-control setsesi"> </td>
																					
																						</tr>

                                                                                        <tr> 
																						  <td> Jumlah Pengawas Sesi 3 (Minggu Pukul 07:30 ) </td>
																						  <td> <input type="number"   trlokasites_id="<?php echo $d->id; ?>" disabled  name="sesi3" kolom="sesi3"  value="<?php echo $d->sesi3; ?>"  placeholder="Isi jumlah pengawas " class="form-control setsesi"> </td>
																					
																						</tr>

                                                                                        <tr> 
																						  <td> Jumlah Pengawas Sesi 4 (Minggu Pukul 13:00 ) </td>
																						  <td> <input type="number"   trlokasites_id="<?php echo $d->id; ?>" disabled  name="sesi4" kolom="sesi4"  value="<?php echo $d->sesi4; ?>"  placeholder="Isi jumlah pengawas" class="form-control setsesi"> </td>
																					
																						</tr>
																	
																					  
																				
																					
																					
																			</table>
																			
																			 
																		
																						
											</div>
									 </div>
									
									   
									 
									
									 
								
						</div>
</div>

<div class="form-actions">
										<div class="row">
											<div class="col-md-12 navbar-right">
											<!-- <a href="<?php echo base_url(); ?>dashpanitia/generatePeserta?lokasi=<?php echo base64_encode($d->id); ?>" target="_blank" class="btn blue"><i class="fa fa-codepen"></i> Generate Peserta Persesi </a>


												<a href="<?php echo base_url(); ?>dashpanitia/generateRUangSesi?lokasi=<?php echo base64_encode($d->id); ?>" target="_blank" class="btn blue"><i class="fa fa-codepen"></i> Generate Ruang dan Sesi </a>
												<a href="<?php echo base_url(); ?>dashpanitia/generateStatusTes?lokasi=<?php echo base64_encode($d->id); ?>" target="_blank" class="btn blue"><i class="fa fa-codepen"></i> Generate Status Tes </a> 
												<button type="submit" class="btn blue"><i class="fa fa-save"></i> Simpan Pengaturan Sesi </button> -->
													</div>
										</div>
									</div>

<?php 
}

?>

<?php 

}else{
?> <center><h1><b> ANDA BELUM MENGISI LOKASITES </b></h1></center><?php 

}
?>	
				

<script>
  $(document).off("input",".setsesi").on("input",".setsesi",function(){
 

     var kolom = $(this).attr("kolom");
	 var trlokasites_id = $(this).attr("trlokasites_id");
	 var nilai   = $(this).val();

	   $.post("<?php echo site_url('dashpanitia/setsesi'); ?>",{kolom:kolom,trlokasites_id:trlokasites_id,nilai:nilai},function(){


		   
	  })


  })

</script>
							
							