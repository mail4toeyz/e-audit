<?php $attributes = array('class' => 'form-horizontal', 'role'=>'form','name' => 'simpan', 'id' => 'simpan', 'method' => "post", 'url' =>site_url("dashpanitia/savelokasitespemetaan")); ?>
<?php echo form_open("javascript:void(0)", $attributes); ?>

<input type="hidden" name="id" class="form-control" value="<?php echo isset($dataform->id) ? $dataform->id:"";  ?>">
<input type="hidden" name="madrasah" class="form-control" value="<?php echo isset($dataform->tmmadrasahc_id) ? $dataform->tmmadrasahc_id:"";  ?>">
<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
							     <i class="icon-note "></i>
								   Form Pemetaan Lokasi Tes
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
							
								
							</div>
							<div class="actions">
								
								<a class="btn btn-icon-only btn-default btn-sm fullscreen" href="javascript:;" data-original-title="" title="">
								</a>
								
							</div>
						</div>
						<div class="portlet-body" id="headerawal">
						
						           <div class="row"  id="errorvalidation">
										<div class="col-md-12">
											<div class="note note-danger note-bordered" style="display:none" id="messagevalidation">
											</div>
											
											<div class="note note-info note-bordered" >
											<b> Keterangan : Dibawah ini adalah Pendaftar yang memilih Lokasi Tes di Provinsi <?php echo $this->Acuan_model->get_kondisi($dataform->provinsi,"id","provinsi","nama"); ?> </br> Lakukan Pemetaan LOKASI TES </b>
											</div>
										</div>
									</div>
									
									 
									   <div class="table-responsive">
									   
									      <table class="table table-hover table-bordered table-striped">
										    
											 <thead>
											   <tr>
											   <th rowspan="1">PROVINSI </th>
											   <th rowspan="1">KABUPATEN/KOTA</th>
											   <th rowspan="1">JUMLAH PEMILIH LOKASI DIMADRASAH</th>
											  <!-- <th colspan="3">Status Seleksi Berkas</th> -->
											   <th rowspan="1"><input type="checkbox" id="checkAllData"> </th>
											   </tr>
											   
											  <!-- <tr>
											     
											     <td> Belum diverifikasi</td>
												   <td> Tidak Lulus</td>
											     <td> Lulus Bekas</td>
											   
											    <tr> -->
										     </thead>
										    
											 <tbody>
											   <?php 
											   $cek="";
											   $jumlahtotal = 0;
											   $jumlahtotal2= 0;
											     $kabko = $this->db->query("select * from kota where provinsi_id IN (".$dataform->pemetaan.") order by provinsi_id asc")->result();
												   foreach($kabko as $row){
													   
													   $dataprov    = $this->db->query("select nama,tmmadrasahc_id from tr_lokasites where zona LIKE '%".$row->id."%'")->row();
													
													   
													    $jumlah = $this->db->query("select count(id) as pemilih from tm_siswa where lokasi_id='".$row->id."'  and hasilpilihan IS NULL and status_persyaratan='1' and covid='dimadrasah' and tmmadrasah_id='2'")->row();
													    $jumlahtotal = $jumlahtotal + $jumlah->pemilih; 
													     
														 $cek     = $this->db->query("select * from tr_lokasites where zona LIKE '%".$row->id."%' and id='".$dataform->id."'")->num_rows();
														
												
														$dataset ="";
														$disabled ="";
													     if(!is_null($dataprov)){

															$dataset  = "Sudah dipilih oleh ".$dataprov->nama;

															if($dataprov->tmmadrasahc_id !=$_SESSION['tmpanitia_id']){
															$disabled = "disabled";
															}


														 }

													  ?>
													     <tr>
														    <td> <?php echo $this->Acuan_model->get_kondisi($row->provinsi_id,"id","provinsi","nama"); ?> </td>
														    <td> <?php echo $row->nama; ?> - <?php echo $dataset; ?> </td>
														    <td> <?php echo $jumlah->pemilih; ?></td>
														    
														    <td> <input type="checkbox" class="pemetaancheck" value="<?php echo $row->id; ?>"  name="zona[]" <?php echo ($cek >0) ? "checked":""; ?>> </td>
													   
													   
													   
													     </tr>
													   <?php
													   
													   
												   }
												   
												   ?>
												   
												     <tr>
														    <td> Jumlah
															</td>
															<td></td>
														    <td> <?php echo $jumlahtotal; ?></td>
														   
														   
														    <td> </td>
														    
													   
													   
													     </tr>
											 
											 </tbody>
										  
										  </table>
									   
									   </div>
									
									 <div class="form-actions">
										<div class="row">
											<div class="col-md-5 navbar-right">
												<button type="submit" class="btn blue"><i class="fa fa-save"></i> Simpan Pemetaan</button>
												<button type="button" class="btn default" id="cancel"><i class="fa fa-repeat "></i> Tutup </button>
											</div>
										</div>
									</div>
								
								<div class="clear"><br></div>
								
						</div>
</div>
						
<?php echo form_close(); ?>

<script type="text/javascript">

  $("#nama").focus();
  $("#checkAllData").click(function () {
     $('.pemetaancheck').not(this).prop('checked', this.checked);
 });


</script>		
				
							
							