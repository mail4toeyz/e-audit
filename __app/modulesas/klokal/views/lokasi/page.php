
<div id="showform"></div>        
                                 
								 <div class="card" id="form1">
								   <div class="card-header">
									   <h3><i class="fa fa-institution"></i><?php echo $title; ?></h3>
								   </div>
								   <br>
									
									   <div class="col-md-12">

									   <div class="alert alert-info" style="font-size:14px">
										Petunjuk ! <br>
										<ol>
										<li>Lokasi Pelaksanaan Kompetisi Sains Madrasah  Tingkat Kabupaten/Kota ditentukan oleh komite Kabupaten/Kota</li>
										<li>Pengisian lokasi tes hanya dapat dilakukan setelah peserta  dilakukan verifikasi oleh komite Kabupaten/Kota </li>

										<li>Setelah input lokasi tes, silahkan klik tombol generate Lokasi Peserta untuk menempatkan peserta ke lokasi yang sudah ditentukan. </li>
										
										</ol>
										<br>
										

									   </div>

									  <!-- <center>
									 <a href="<?php echo site_url('pelaksanaankabko/generateLokasi'); ?>" class="btn btn-primary btn-circle"><i class="fa fa-refresh fa-spin"></i> Generate Lokasi Peserta </a>
									</center> 
-->
											   
									<button class="btn  btn-primary" id="tambahdata" style="display:none" urlnya="<?php echo site_url("klokal/f_lokasi"); ?>" >
									  <i class="fa  fa-plus-square "></i>
											 Tambah Lokasi Tes 
									</button>
									<br>
									<br>
									 </div>
									 
								<div class="col-md-12">
								<div class="row">
												
		   
													
												   
													<div class="col-md-3">
														   <div class="form-group ">
														   <div class="form-group">
																   <input type="text" size="30" name="keyword" id="keyword" class="form-control" placeholder=" Ketik disini.. ">
																   
															   </div>
														  </div>
													</div>
													
													<div class="col-md-1"><button type="submit" class="btn green tooltips" data-container="body" data-placement="bottom" title="Lakukan Pencarian" id="searchcustom"><span class="fa fa-search"></span></button>
												   </div>
									   
							   </div>
							   </div>
					   
						
				   <div class="col-md-12">   				
					  <div class="table-responsive">
					   <table id="datatableTable" class="table  table-bordered table-hover table-striped" >
						 <thead>
						   <tr>
							  <th class="center" width="3%" rowspan="2" >No</th>
							  <th class="center" width="10%" rowspan="2">Aksi </th>	
							  <th class="center" rowspan="2">Lokasi </th>				  
							  <th class="center" rowspan="2">Alamat </th>				  
							  <th class="center" rowspan="2">Komputer </th>				  
							 	  
							  <th colspan="3"> Hari ke-1</th>
							  <th colspan="3"> Hari ke-2</th>
							  <th colspan="4"> Hari ke-3</th>  
							  <th class="center" rowspan="2">Total Peserta </th>
							  <th class="center" rowspan="2">Cakupan Wilayah </th>		  
							  <th class="center" rowspan="2">Jenjang </th>		
							 
						   </tr>

						   <tr>
							   <?php 
							   $zona =  strtolower($this->Di->get_kondisi(array("id"=>$dt->provinsi_id),"provinsi","zona"));
							   ?>
							   <th> Sesi1 <br> (<?php echo $this->Di->get_kondisi(array("id"=>1),"tm_sesi",$zona); ?>)</th>
							   <th> Sesi2<br> (<?php echo $this->Di->get_kondisi(array("id"=>2),"tm_sesi",$zona); ?>)</th>
							   <th> Sesi3<br> (<?php echo $this->Di->get_kondisi(array("id"=>3),"tm_sesi",$zona); ?>)</th>
							   <th> Sesi1<br> (<?php echo $this->Di->get_kondisi(array("id"=>4),"tm_sesi",$zona); ?>)</th>
							   <th> Sesi2<br> (<?php echo $this->Di->get_kondisi(array("id"=>5),"tm_sesi",$zona); ?>)</th>
							   <th> Sesi3<br> (<?php echo $this->Di->get_kondisi(array("id"=>6),"tm_sesi",$zona); ?>)</th>
							   <th> Sesi1<br> (<?php echo $this->Di->get_kondisi(array("id"=>7),"tm_sesi",$zona); ?>)</th>
							   <th> Sesi2<br> (<?php echo $this->Di->get_kondisi(array("id"=>8),"tm_sesi",$zona); ?>)</th>
							   <th> Sesi3<br> (<?php echo $this->Di->get_kondisi(array("id"=>9),"tm_sesi",$zona); ?>)</th>
							   <th> Sesi4<br> (<?php echo $this->Di->get_kondisi(array("id"=>10),"tm_sesi",$zona); ?>)</th>
							   
						   </tr>
							
						 </thead>
						 <tbody></tbody>
					   </table>
					   </div>
				   </div>
				   </div>
				   
		   
		  <script type="text/javascript">
			 var dataTable = $('#datatableTable').DataTable( {
								   "processing": true,
								   "language": {
								   "processing": '<b style="color:white;background:#33AFFF">Data sedang di tampilkan..</b>',
									 "oPaginate": {
									   "sFirst": "Halaman Pertama",
									   "sLast": "Halaman Terakhir",
										"sNext": "Selanjutnya",
										"sPrevious": "Sebelumnya"
										},
								   "sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
									"sInfoEmpty": "Tidak ada data yang di tampilkan",
									  "sZeroRecords": "Data belum tersedia, silahkan input ",
									  "sLengthMenu": "Default menampilkan   _MENU_ Data <small></small>"
							   },
							   
							   "serverSide": true,
							   "searching": false,
							   "responsive": true,
							   "lengthMenu": [[5,10,25, 50,100,200,300,500,1000, 800000000], [5,10,25, 50,100,200,300,500,1000,"All"]],
								"dom": 'Blfrtip',
								"sPaginationType": "full_numbers",
							   "buttons": [
							   
										
									   {
									   extend: 'excelHtml5',
									   
									   text:'<span class="fa fa-file-excel-o"></span> Cetak Excel',
									   className :"btn btn-danger btn-sm"
									   },
									   
									   
									   {
									   extend: 'copyHtml5',
									   
									   text:'<span class="fa fa-copy "></span> Copy Data ',
									   className :"btn btn-danger btn-sm"
								   },
									   {
									   extend: 'csvHtml5',
									   
									   text:'<span class="fa fa-file-archive-o "></span> Cetak CSV ',
									   className :"btn btn-danger "
								   },
								   
									   {
									   extend: 'colvis',
									   
									   text:'<span class="fa fa-columns "></span> Setting Kolom ',
									   className :"btn btn-danger "
								   }
							   ],
							   
							   "ajax":{
								   url :"<?php echo site_url("klokal/g_lokasi"); ?>", 
								   type: "post", 
								   "data": function ( data ) {
								   data.jenis = $('#jenis').val();
								   data.jenjang = $('#jenjang').val();
								   data.status = $('#status').val();
								   data.kolom = $('#kolom').val();
								   data.keyword = $('#keyword').val();
						   
							   }
								   
							   },
							   "rowCallback": function( row, data ) {
								   
								   
							   }
						   } );
						   
			   
				   
				   $(document).on('input click change', '#keyword,#searchcustom,#status,#jenis,#jenjang', function (event, messages) {			
						
						  dataTable.ajax.reload(null,false);	        
					 
				   });
				   
				   $(document).on(' click ', '#cancelr', function (event, messages) {			
						  $("#showform").html("");
						  dataTable.ajax.reload(null,false);	        
					 
				   });
		   
				   
				   $(document).on(' click ', '#cancelr', function (event, messages) {			
						  $("#showform").html("");
						  dataTable.ajax.reload(null,false);	        
					 
				   });
				   
				   
				   
					   $(document).on('input  change', '#kota,#kolom,#keyword,#searchcustom', function (event, messages) {			
						
						  dataTable.ajax.reload(null,false);	        
					 
				   });
				   
				   
				   
			
			
		   
		   </script>
		
							   