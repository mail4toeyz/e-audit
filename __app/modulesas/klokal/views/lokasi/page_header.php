<!DOCTYPE html>

<html lang="en" class="no-js">

<head>
<meta charset="utf-8"/>
<title><?php echo $title; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>

<link href="<?php echo base_url(); ?>__statics/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>__statics/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>__statics/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>__statics/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>__statics/global/css/components-md.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>__statics/global/css/plugins-md.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>__statics/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>__statics/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?php echo base_url(); ?>__statics/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>__statics/js/datepicker/datepicker.css" rel="stylesheet" type="text/css">


<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/global/plugins/bootstrap-toastr/toastr.min.css"/>

<link href="<?php echo base_url(); ?>__statics/js/alertify/css/alertify.css" rel="stylesheet">

<script src="<?php echo base_url(); ?>__statics/global/plugins/jquery.min.js" ></script>
<script src="<?php echo base_url(); ?>__statics/global/plugins/bootstrap-toastr/toastr.min.js"></script>
<script>
 var base_url = '<?php echo base_url(); ?>';
</script>
<!--
<style>
.goog-te-banner-frame.skiptranslate {display: none !important;} 
.goog-te-gadget-simple {display: none !important;} 

</style>

-->
<style>
.alert-info{
	
	background:#1536ED;font-weight:bold
}
.btn-info{
	background:#1536ED;font-weight:bold
	
}
.btn-info:hover{
	background:#1536ED;font-weight:bold
	
}
.btn-info:active{
	background:#1536ED;font-weight:bold
	
}
#overlay{
  position:fixed;
  z-index:99999;
  top:0;
  left:0;
  bottom:0;
  right:0;
  background:rgba(0,0,0,0.9);
  transition: 1s 0.4s;
}
#progress{
  height:1px;
  background:#fff;
  position:absolute;
  width:0;
  top:50%;
  transition: 1s;
}
#progstat{
  font-size:0.7em;
  letter-spacing: 3px;
  position:absolute;
  top:50%;
  margin-top:-40px;
  width:100%;
  text-align:center;
  color:#fff;
}

</style>

<script src="<?php echo base_url(); ?>__statics/js/proses.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>__statics/js/jquerycookie.js"></script>


<link rel="shortcut icon" href="<?php echo base_url(); ?>__statics/img/madrasah/<?php echo $madrasah->logo; ?>"/>
</head>
<body class="page-md page-header-fixed page-sidebar-closed-hide-logo page-sidebar-closed-hide-logo" style="background:white">
<!-- BEGIN HEADER -->
<div class="page-header md-shadow-z-1-i navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo hidden-xs">
			<a href="<?php echo site_url("dashpendaftar"); ?>" style="text-decoration:none">
			<div class="col-md-2">
			<img src="<?php echo base_url(); ?>__statics/img/madrasah/<?php echo $madrasah->logo; ?>" alt="logo"  style="width:50px;padding-top:20px" />
			</div>
			<div class="col-md-9" style="padding-top:20px;">
			<b class="hidden-xs" style="font-size:12px;font-weight:bold;"> SELEKSI NASIONAL PESERTA DIDIK BARU <br> <?php echo strtoupper($madrasah->nama); ?> TAHUN PELAJARAN <?php echo $this->Acuan_model->ajaran();?>/<?php echo $this->Acuan_model->ajaran()+1;?> </b>
			
			<b class="hidden-lg " style="font-size:9px;font-weight:bold;"> SELEKSI NASIONAL PESERTA <br> DIDIK BARU  </b>
			</div>
			</a>
				   </a>
		</div>
		
		<div class="page-logo hidden-lg">
			<a href="<?php echo site_url("dashpendaftar"); ?>" style="text-decoration:none">
			
			<img src="<?php echo base_url(); ?>__statics/img/madrasah/<?php echo $madrasah->logo; ?>" alt="logo"  style="width:50px;padding-top:20px" />
			
			<b class="hidden-lg " style="font-size:9px;font-weight:bold;"> SELEKSI NASIONAL PESERTA DIDIK BARU  &nbsp;  &nbsp;</b>
			
			</a>
		
		</div>
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> 
		   </a>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		
	
		<!-- BEGIN PAGE TOP -->
		<div class="page-top">
		
			<div class="top-menu">
		          
			
				<ul class="nav navbar-nav pull-right translation-icons">
				<li>
					   
					    <img style="cursor:pointer;margin-top:5px" width="130px" src="<?php echo base_url(); ?>__statics/img/call.png" onClick="window.open('https://tawk.to/chat/5a25a93dd0795768aaf8d508/default/?$_tawk_popout=true','mywindow','toolbar=0,location=0,menubar=0,resizable=0,status=0,scrollbars=1,width=350,height=400'); selected(3);"/> 
					   
					   </li>
						   
                        
					<li class="separator hide">
					</li>
				
					
				
					<li class="dropdown dropdown-user dropdown-dark">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<span class="username username-hide-on-mobile">
						<?php echo $siswa->nama; ?> </span>
						       <?php 
								  $cf = $this->Acuan_model->query("select a.file,b.nama,a.folder from tr_persyaratansiswa a LEFT JOIN tr_persyaratan b ON a.trpersyaratan_id=b.id where a.tmsiswa_id='".$_SESSION['tmsiswa_id']."' and UPPER(b.nama) LIKE '%PAS%' order by tanggal desc")->row();
								   if(count($cf) >0){
									   
									   
									   $foto    = $cf->folder."/".$cf->file;
								   }else{
									   $foto    = "__statics/img/orang.png";
								   }
								?>
								
						<img alt="" class="img-circle" src="<?php echo base_url(); ?><?php echo $foto; ?>"/>
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="<?php echo site_url("dashpendaftar/akun"); ?>">
								<i class="icon-user"></i> Akun Saya </a>
							</li>
					
							<li>
								<a href="javascript:void(0)" id="keluar">
								<i class="icon-key"></i> Keluar  </a>
							</li>
						</ul>
					</li>
					<!-- END USER LOGIN DR
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
		<!-- END PAGE TOP -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
                        

<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
	
		<div class="page-sidebar md-shadow-z-2-i  navbar-collapse collapse">
		<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
		 <?php $segment = $this->uri->segment(1); ?>
		 <?php $segment2 = $this->uri->segment(2); ?>
				<li class="start <?php echo ($segment=="dashpendaftar" && $segment2=="") ? "active":""; ?> tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Dashboard Anda">
					<a href="<?php echo site_url("dashpendaftar"); ?>">
					<i class="icon-home"></i>
					<span class="title">Dashboard </span>
					</a>
				</li>
				
				<li class="start <?php echo ($segment2=="formulir") ? "active":""; ?> tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Silahkan Lengkapi Formulir Pendaftaran">
					<a href="<?php echo site_url("dashpendaftar/formulir"); ?>">
					<i class="icon-note "></i>
					<span class="title">Data Pendaftar</span>
					</a>
				</li>
				
				<li class="start <?php echo ($segment2=="jalur") ? "active":""; ?> tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Silahkan Pilih Jalur Pendaftaran">
					<a href="<?php echo site_url("dashpendaftar/jalur"); ?>">
					<i class="icon-cursor-move  "></i>
					<span class="title">Jalur Pendaftaran</span>
					</a>
				</li>
				
				<li class="start <?php echo ($segment2=="sekolahasal") ? "active":""; ?> tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Silahkan Lengkapi Sekolah Asal">
					<a href="<?php echo site_url("dashpendaftar/sekolahasal"); ?>">
					<i class="icon-graduation   "></i>
					<span class="title">Madrasah/Sekolah Asal</span>
					</a>
				</li>
				
				<li class="start <?php echo ($segment2=="pilihsekolah") ? "active":""; ?> tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Silahkan Pilih <?php echo $madrasah->singkatan; ?>">
					<a href="<?php echo site_url("dashpendaftar/pilihsekolah"); ?>">
					<i class="icon-target    "></i>
					<span class="title">Pilih <?php echo $madrasah->singkatan; ?></span>
					</a>
				</li>
				
				<li class="start <?php echo ($segment2=="rapor") ? "active":""; ?> tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Input Nilai Rapor">
					<a href="<?php echo site_url("dashpendaftar/rapor"); ?>">
					<i class="fa fa-book  "></i>
					<span class="title">Input Nilai Rapor</span>
					</a>
				</li>
				
				<?php 
				  if($siswa->tmjalur_id==0){
					   ?>
					    	<li class="start <?php echo ($segment2=="persyaratanfile") ? "active":""; ?> tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Pilih terlebih dahulu jalur pendaftaran">
								<a href="<?php echo site_url("dashpendaftar/persyaratanfile"); ?>">
								<i class="icon-doc    "></i>
								<span class="title">Upload Persyaratan</span>
								</a>
							</li>
					  
					  <?php 
					  
				  }else{
					  
				?>
		
				<li class=" <?php echo ($segment2=="persyaratan" or $segment2=="persyaratanrapor" or $segment2=="persyaratanprestasi") ? "active":""; ?> tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Silahkan Lengkapi Persyaratan">
					<a href="javascript:;">
					<i class=" icon-doc "></i>
					<span class="title">Upload Persyaratan</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
					
						<?php 
						  $acid= base64_decode($this->input->get_post("id",true));
						  
						  $persyaratan = $this->Acuan_model->query(" select * from tr_persyaratan where tmmadrasah_id='".$madrasah->id."' and tmjalur_id='".$siswa->tmjalur_id."' order by urutan asc")->result();
						    foreach($persyaratan as $row){
								if( strpos( strtoupper($row->nama), "RAPOR" ) !== false ) {
								?>
								
								<li class="<?php echo ($acid==$row->id) ? "active":""; ?>">
									<a href="<?php echo site_url("dashpendaftar/persyaratanrapor?dfd5dfgg67898fg7574fgg6dfgdfg54fhfghfghfghdsk6wsfsdf5657sdgdge56654534fsdfhkshuidytakshdlfhyueebslmdjdsgy7g86jsdndkznddfxvfgxvcxv=".base64_encode("1234567266255363767672672565165652637676375265651656526676376736735567767878")."&id=".base64_encode($row->id)."&syarat=".$row->nama.""); ?>">
									 <i class="fa fa-file-archive-o"></i>
									<?php echo $row->nama; ?> <?php echo ($row->status==1) ? " <small style='color:red'> (*)</small>":""; ?></a>
								</li>
								
								<?php
								
								}else if(strpos( strtoupper($row->nama), "PRESTASI" ) !== false){
									?>
								
								<li class="<?php echo ($acid==$row->id) ? "active":""; ?>">
									<a href="<?php echo site_url("dashpendaftar/persyaratanprestasi?dfd5dfgg67898fg7574fgg6dfgdfg54fhfghfghfghdsk6wsfsdf5657sdgdge56654534fsdfhkshuidytakshdlfhyueebslmdjdsgy7g86jsdndkznddfxvfgxvcxv=".base64_encode("1234567266255363767672672565165652637676375265651656526676376736735567767878")."&id=".base64_encode($row->id)."&syarat=".$row->nama.""); ?>">
									 <i class="fa fa-file-archive-o"></i>
									<?php echo $row->nama; ?> <?php echo ($row->status==1) ? " <small style='color:red'> (*)</small>":""; ?></a>
								</li>
								
								<?php
								
								}else{
									
									?>
								
									<li class="<?php echo ($acid==$row->id) ? "active":""; ?>">
										<a href="<?php echo site_url("dashpendaftar/persyaratan?dfd5dfgg67898fg7574fgg6dfgdfg54fhfghfghfghdsk6wsfsdf5657sdgdge56654534fsdfhkshuidytakshdlfhyueebslmdjdsgy7g86jsdndkznddfxvfgxvcxv=".base64_encode("1234567266255363767672672565165652637676375265651656526676376736735567767878")."&id=".base64_encode($row->id)."&syarat=".$row->nama.""); ?>">
										 <i class="fa fa-file-archive-o"></i>
										<?php echo $row->nama; ?> <?php echo ($row->status==1) ? " <small style='color:red'> (*)</small>":""; ?></a>
									</li>
									
									<?php
								
									
									
								}
							}
						?>
						
					</ul>
				</li>
				
				  <?php } ?>
				
				<li class="start <?php echo ($segment2=="lokasi") ? "active":""; ?> tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Silahkan Pilih Lokasi Tes">
					<a href="<?php echo site_url("dashpendaftar/lokasi"); ?>">
					<i class="fa fa-map-marker "></i>
					<span class="title">Tentukan Lokasi Tes</span>
					</a>
				</li>
				
				<li class="start tooltips <?php echo ($segment2=="kartupendaftaran") ? "active":""; ?>" data-container="body" data-placement="right" data-html="true" data-original-title="Cetak Kartu Pendaftaran">
					<a href="<?php echo site_url("dashpendaftar/kartupendaftaran"); ?>">
					<i class="icon-film  "></i>
					<span class="title">Bukti Pendaftaran</span>
					</a>
				</li>
				
				<li class="start tooltips <?php echo ($segment2=="hasilseleksi") ? "active":""; ?>" data-container="body" data-placement="right" data-html="true" data-original-title="Lihat Hasil Seleksi Anda">
					<a href="<?php echo site_url("dashpendaftar/hasilseleksi"); ?>">
					<i class="icon-layers  "></i>
					<span class="title">Pengumuman </span>
					</a>
				</li>
				
			<!--	<li class="start tooltips <?php echo ($segment2=="daftarulang") ? "active":""; ?>" data-container="body" data-placement="right" data-html="true" data-original-title="Pendaftaran Ulang">
					<a href="<?php echo site_url("dashpendaftar/daftarulang"); ?>">
					<i class="fa fa-cubes  "></i>
					<span class="title">Daftar Ulang </span>
					</a>
				</li> -->
				<li class=" <?php echo ($segment2=="persentase") ? "active":""; ?> tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Persentase Jumlah Pendaftar">
					<a href="javascript:;">
					<i class=" icon-graph"></i>
					<span class="title">Persentase Pendaftar</span>
					<span class="arrow "></span>
					</a>
					   <ul class="sub-menu">
					      <li class="">
										<a href="<?php echo site_url("dashpendaftar/persentase?data=permadrasah"); ?>">
										 <i class="icon-graph"></i> Permadrasah 
										</a>
						   </li>
						    <li class="">
										<a href="<?php echo site_url("dashpendaftar/persentase?data=perjalur"); ?>">
										 <i class="icon-graph"></i> Jalur Pendaftaran
										</a>
						   </li>
					   </ul>
				</li>
					
				
				<li class="start tooltips <?php echo ($segment2=="akun") ? "active":""; ?>" data-container="body" data-placement="right" data-html="true" data-original-title="Perbaharui Password Anda">
					<a href="<?php echo site_url("dashpendaftar/akun"); ?>">
					<i class="icon-user  "></i>
					<span class="title">Akun Anda </span>
					</a>
				</li>
				
				<li class="start tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="Keluar Dari Dashboard ">
					<a href="javascript:void(0)" id="keluar">
					<i class=" icon-power  "></i>
					<span class="title">Keluar  </span>
					</a>
				</li>
				
				
				 
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
					 <div class="page-content">
					
						   <div class="portlet light">
												
												<div class="portlet-body">
												   <div class="row">
														    <div id="overlay">
																<div id="progstat"></div>
																<div id="progress"></div>
															</div>
															 <div class="my-box"></div>
			
															 
																	<?php $this->load->view(isset($konten) ? $konten:"page_default"); ?>
												   </div> 
											 </div>
					   </div>
					   </div>
			</div>
</div>

	
	<!-- END CONTENT -->

<div class="page-footer">
	<div class="page-footer-inner">
		 <?php echo date("Y"); ?> &copy; DIREKTORAT KSKK MADRASAH 
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>




<script>

  $(document).on("click","#keluar",function(){
	  
	  var base_url = "<?php echo base_url(); ?>";
	  
	  alertify.confirm("Anda akan keluar dari aplikasi, apakah anda yakin?",function(){
		  
	  	       $.post("<?php echo site_url("ppdb/logout"); ?>",function(data){
				   
				   location.href= base_url+"ppdb/login/"+data;
				   
			   });
		});
	  
	  
	  
  })
  
  
  
  
        $(function(){
          function id(v){ return document.getElementById(v); }
          function loadbar() {
            var ovrl = id("overlay"),
                prog = id("progress"),
                stat = id("progstat"),
                img = document.images,
                c = 0,
                tot = img.length;
            if(tot == 0) return doneLoading();

            function imgLoaded(){
              c += 1;
              var perc = ((100/tot*c) << 0) +"%";
              prog.style.width = perc;
              stat.innerHTML = "Loading "+ perc;
              if(c===tot) return doneLoading();
            }
            function doneLoading(){
              ovrl.style.opacity = 0;
              setTimeout(function(){ 
                ovrl.style.display = "none";
              }, 1200);
            }
            for(var i=0; i<tot; i++) {
              var tImg     = new Image();
              tImg.onload  = imgLoaded;
              tImg.onerror = imgLoaded;
              tImg.src     = img[i].src;
            }    
          }
          document.addEventListener('DOMContentLoaded', loadbar, false);
        }());
    </script>

<script src="<?php echo base_url(); ?>__statics/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>__statics/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>__statics/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>__statics/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>__statics/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>__statics/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>__statics/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>__statics/admin/layout4/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>__statics/js/datepicker/datepicker.js" ></script>
<script src="<?php echo base_url(); ?>__statics/js/alertify/alertify.js"></script>



<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
  
});
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>