
		<div id="showform"> </div>

		<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
							     <i class="icon-grid"></i> Pengawas Ruang Ujian 
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
								
								
							</div>
							<div class="actions">
								
								<a class="btn btn-icon-only btn-default btn-sm fullscreen" href="javascript:;" data-original-title="" title="">
								</a>
								
							</div>
						</div>
						<div class="portlet-body">
						<div class="alert alert-info" style="font-weight:bold"> 

                          <h2> Petunjuk </h2>
                           
                            1 Ruang akan di awasi oleh 1 orang pengawas, untuk melakukan pengawasan silahkan buka alamat <u> https://snpdb-madrasah.kemenag.go.id/pengawas </u> melalui browser Google Chrome atau Mozzila Firefox   atau klik tombol dibawah ini <br> <br>
                            <center> <a href="https://snpdb-madrasah.kemenag.go.id/pengawas" target="_blank" class="btn btn-warning"><i class="fa fa-sign-in"></i> Masuk Pengawas </a> </center>
							<br>
                            Akun  login  Pengawas menggunakan Kode Ruang dan Password Peruang Ujian yang ada pada tabel dibawah ini 
						</div>
							
					
							

	
						   <div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="datatableTable" width="100%">

									<thead>
										<tr>
											<th width="5%" style="font-size:12px"> No </th>
										 	<th style="font-size:12px">Lokasi </th> 
											<th style="font-size:12px">Sesi </th>
										    <th style="font-size:12px">Ruang   </th>
										    <th style="font-size:12px">Kode Ruang  </th>
										    <th style="font-size:12px">Password   </th>
											
										 	<th style="font-size:12px">Jml Peserta  </th> 
										 
											
																	
									   </tr>
									  
									</thead>
									<tbody>
									</tbody>
								</table>
							  </div>
		                </div>
</div>


<script type="text/javascript">
  
	
       var dataTable = $('#datatableTable').DataTable( {
					"processing": true,
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[25, 50,100,200,300,500,1000, 800000000], [25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	{
							extend: 'excelHtml5',
							
							},
							
						
						
						'colvis'
					],
					
					"ajax":{
						url :"<?php echo site_url("dashpanitia/gridRuang"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						  data.keyword = $('#keyword').val();
						  data.csrf_ppdb_kemenag = $.cookie('csrf_kemenag_ppdb');
						  
						 
				
						
						
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
		
	
				

		

</script>	
