

<?php $attributes = array('class' => 'form-horizontal', 'role'=>'form','name' => 'simpan', 'id' => 'simpan', 'method' => "post", 'url' =>site_url("klokal/s_lokasi")); ?>
<?php echo form_open("javascript:void(0)", $attributes); ?>

<input type="hidden" name="id" id="id" class="form-control" value="<?php echo isset($dataform->id) ? $dataform->id:"";  ?>">
<div class="card" id="form1">
								   <div class="card-header">
									   <h3><i class="fa fa-institution"></i> Lokasi Tes Pelaksanaan KSM Tingkat Kabupaten/Kota</h3>
								   </div>

						<div class="card-body" id="headerawal">
						
						           <div class="row" style="display: none;" id="errorvalidation">
										<div class="col-md-12">
											<div class="note note-success note-bordered" id="messagevalidation"></div>
										</div>
									</div>
									
									 
									 <div class="form-group">
											 <label class=" control-label" style="text-align:left"> Nama Lokasi Tes  <small class="required">*</small></label>
											 <div class=" has-success">
												
													<input type="text"  name="f[nama]" id="nama" value="<?php echo isset($dataform->nama) ? $dataform->nama:"";  ?>" placeholder="contoh : MAN 1 Bandung " class="form-control"  >
																						
											</div>
									 </div>
									 
									 <div class="form-group">
											 <label class=" control-label" style="text-align:left"> Alamat Lokasi Tes  <small class="required">*</small></label>
											 <div class=" has-success">
												
													<textarea  name="f[alamat]" id="alamat"  placeholder="Masukkan Alamat Lengkap (Akan ditampilkan di kartu Tes) " class="form-control"  ><?php echo isset($dataform->alamat) ? $dataform->alamat:"";  ?></textarea>
																						
											</div>
									 </div>

									 <div class="form-group">
											 <label class="control-label" style="text-align:left"> Mana saja Jenjang Pendidikan yang akan tes dilokasi tersebut ?</label>
											 <?php 
											  $jenjang = array("1"=>"MI/SD","2"=>"MTs/SMP","3"=>"MA/SMA");
											  foreach($jenjang as $ijs=>$js){

												$selekted="";
												if(isset($dataform)){
													$jenjang = explode(",",$dataform->jenjang);

													 if(in_array($ijs,$jenjang)){
														 $selekted="checked";
														 
													 }

												}
												?>


												
											    <div class="form-check">
												 <input type="checkbox" class="form-check-input jenjangpendidikan" id="check<?php echo $ijs; ?>" name="jenjang[]" readonly <?php echo $selekted; ?> value="<?php echo $ijs; ?>" >
												 <label class="form-check-label" for="check<?php echo $ijs; ?>"><?php echo $js; ?></label>
												</div>
											 <?php 
											  }
											  ?>
												
									 </div>
									 
									 <div class="form-group" id="loadkecamatan">
										 <?php 
										   if(isset($dataform)){
											$data['jenjang'] = explode(",",$dataform->jenjang);
											$this->load->view("lokasi/loadkecamatan",$data);

										   }else{

											echo "(Pilih terlebih dahulu jenjang pendidikan)";
										   }
										   ?>
									      
												
									 </div>


								

									




									<div class="form-group">
											 <label class="control-label" style="text-align:left"> Berapa jumlah ketersediaan komputer dilokasi tersebut ?</label>
											 <div class=" has-success">
												
													<input type="number"  name="f[kuota]"  value="<?php echo isset($dataform->kuota) ? $dataform->kuota:"";  ?>"  class="form-control"  >
																						
											</div>
									 </div>
									 
									
									
										       <center>
												<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
												<button type="button" class="btn default" id="cancel"><i class="fa fa-repeat "></i> Tutup </button>
												</center>
										
								
								<div class="clear"><br></div>
								<div class="alert alert-success">
									<b> Keterangan </b>
									<ol type="square">
									  <li> Tanda (*) Wajib di isi</li>
									 </ol>
								</div>
						</div>
</div>
						
<?php echo form_close(); ?>

<script type="text/javascript">


$(document).ready(function() {
    //$('.cakupan').select2();
	 

	$('#checkall').change(function () {
    $('.pemetaan').prop('checked',this.checked);
	});

	$('.pemetaan').change(function () {
	if ($('.pemetaan:checked').length == $('.pemetaan').length){
	$('#checkall').prop('checked',true);
	}
	else {
	$('#checkall').prop('checked',false);
	}
	});

  $(document).on("click",".jenjangpendidikan",function(){
	var id       = $("#id").val();
	var selected = new Array();
	

	$("input:checkbox.jenjangpendidikan:checked").each(function(){
		selected.push($(this).val());
		
     });

	
	 $("#loadkecamatan").html("<center>Sedang Memuat kecamatan, mohon tunggu ..</center>");
	  $.post("<?php echo site_url("klokal/loadkecamatan"); ?>",{jenjang:selected,id:id},function(data){

		$("#loadkecamatan").html(data);

	  });

	})


});


		
 


</script>		
				
							
							