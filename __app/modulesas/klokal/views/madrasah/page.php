   <div id="showform"></div>        
                                 
					  <div class="card" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-institution"></i><?php echo $title; ?></h3>
                        </div>
                        <br>
                         
                            <div class="col-md-12">
							<div class="alert alert-info" style="font-size:10px">
							 Petunjuk ! <br>
							 Dibawah ini adalah data-data Madrasah/Sekolah di Kab/Kota Anda, Anda dapat melihat jumlah peserta yang di kirimkan oleh Madrasah/Sekolah tersebut , dan merubah Password Akun jika ada permintaan dari Madrasah/Sekolah.<br>
							</div>
								 
									<br>
			 
			 
			              </div>
						  
					 <div class="col-md-12">
					 <div class="row">
					                 

										 <div class="col-md-2">
												<div class="form-group ">
												<select id="jenis" class="form-control">
												<option value=""> Tampilkan Jenis  </option>
												<option value="1"> Madrasah </option>
												<option value="2"> Sekolah  </option>
												
												 
												
												</select>
										       </div>
										 </div>
										 <div class="col-md-2">
												<div class="form-group ">
												<select id="jenjang" class="form-control">
												<option value=""> Tampilkan Jenjang  </option>
												<option value="1"> MI/SD </option>
												<option value="2"> MTs/SMP  </option>
												<option value="3"> MA/SMA  </option>
												
												 
												
												</select>
										       </div>
										 </div>
                                           
										   
										  
										  <div class="col-md-2">
												<div class="form-group ">
												<select id="status" class="form-control">
												<option value=""> Tampilkan Status </option>
												<option value="1"> Negeri </option>
												<option value="2"> Swasta </option>
												
												 
												
												</select>
										       </div>
										 </div>
										  <div class="col-md-2">
												<div class="form-group ">
												<select id="kolom" class="form-control">
												<option value="nama"> Filter Nama </option>
												<option value="username"> Filter NPSN/NSM </option>
												<option value="delegasi_nama"> Filter Ketua Delegasi</option>
												
												 
												
												</select>
										       </div>
										 </div>
										
										 <div class="col-md-3">
												<div class="form-group ">
												<div class="form-group">
														<input type="text" size="30" name="keyword" id="keyword" class="form-control" placeholder="Cari disini berdasarkan Filter ">
														
													</div>
										       </div>
										 </div>
										 
										 <div class="col-md-1"><button type="submit" class="btn green tooltips" data-container="body" data-placement="bottom" title="Lakukan Pencarian" id="searchcustom"><span class="fa fa-search"></span></button>
								        </div>
							
					</div>
					</div>
			
		     
		<div class="col-md-12">   				
		   <div class="table-responsive">
			<table id="datatableTable" class="table  table-bordered table-hover table-striped" >
			  <thead>
				<tr>
				   <th class="center" width="3%" rowspan="2">No</th>
				   <th class="center" width="10%" rowspan="2">Aksi </th>	
				   <th class="center" rowspan="2">Jenis</th>				  
				   <th class="center" rowspan="2">Jenjang </th>				  
				   <th class="center" rowspan="2">Username </th>				  
				   				  
				   <th class="center" rowspan="2">Nama </th>
				   	  
				   <th class="center" rowspan="2">Telepon</th>				  
				   <th class="center" rowspan="2">Email</th>			   			  
				   <th class="center" colspan="4">Jumlah Peserta</th>	
				   <th class="center" rowspan="2">NIK Delegasi</th>				  
				   <th class="center" rowspan="2">Ketua Delegasi</th>				  
				   <th class="center" rowspan="2">Kontak Delegasi</th>				  
				   <th class="center" rowspan="2">Status</th>

				  		  
				
				  
				</tr>
				 <tr>
				   <th> Draft </th>
				   <th> Terkirim </th>
				   <th> Lulus </th>
				   <th> Tidak </th>
				 </tr>
			  </thead>
			  <tbody></tbody>
            </table>
			</div>
        </div>
        </div>
		

<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<b style="color:white;background:#33AFFF">Data sedang di tampilkan..</b>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data tidak atau belum tersedia, silahkan input data di menu Input Peserta",
						   "sLengthMenu": "Default menampilkan   _MENU_ Data <small>(Klik pada angkanya untuk memuat lebih banyak data sebelum mencetak ke format excel)</small>"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": true,
					"lengthMenu": [[5,10,25, 50,100,200,300,500,1000, 800000000], [5,10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							
							text:'<span class="fa fa-file-excel-o"></span> Cetak Excel',
							className :"btn btn-danger btn-sm"
							},
							
							
							{
							extend: 'copyHtml5',
							
							text:'<span class="fa fa-copy "></span> Copy Data ',
							className :"btn btn-danger btn-sm"
						},
							{
							extend: 'csvHtml5',
							
							text:'<span class="fa fa-file-archive-o "></span> Cetak CSV ',
							className :"btn btn-danger "
						},
						
							{
							extend: 'colvis',
							
							text:'<span class="fa fa-columns "></span> Setting Kolom ',
							className :"btn btn-danger "
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("klokal/g_madrasah"); ?>", 
						type: "post", 
						"data": function ( data ) {
						data.jenis = $('#jenis').val();
						data.jenjang = $('#jenjang').val();
						data.status = $('#status').val();
						data.kolom = $('#kolom').val();
						data.keyword = $('#keyword').val();
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
	
		
		$(document).on('input click change', '#keyword,#searchcustom,#status,#jenis,#jenjang', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
		
		$(document).on(' click ', '#cancelr', function (event, messages) {			
			   $("#showform").html("");
			   dataTable.ajax.reload(null,false);	        
		  
        });

		
		$(document).on(' click ', '#cancelr', function (event, messages) {			
			   $("#showform").html("");
			   dataTable.ajax.reload(null,false);	        
		  
        });
		
		
		
			$(document).on('input  change', '#kota,#kolom,#keyword,#searchcustom', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
		
		
		
 $(document).on("click",".kelaaktifkan",function(){
	 var id     = $(this).attr("datanya");
	 var urlnya = $(this).attr("urlnya");
	
	alertify.confirm("Anda akan mengaktifkan kembali akun Lembaga agar dapat login kembali ?",function(){
		 loading();
	 $.post(urlnya,{id:id},function(data){
		  	toastr.success("Yeay", "Sukses !", {
							"timeOut": "0",
							"extendedTImeout": "0",
							"closeButton": true,
							  "debug": false,
							  "positionClass": "toast-top-right",
							  "onclick": null,
							  "showDuration": "10000",
							  "hideDuration": "10000",
							  "timeOut": "50000",
							  "extendedTimeOut": "10000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
						});
		 dataTable.ajax.reload(null,false);
		 
			jQuery.unblockUI({ });
	 })
	 })
	
	 
 })
 
 
 		
 $(document).on("click",".kelanonaktifkan",function(){
	 var id     = $(this).attr("datanya");
	 var urlnya = $(this).attr("urlnya");
	
	alertify.confirm("Anda akan Menonaktifkan akun Lembaga, Lembaga tidak akan bisa login , Yakin ?",function(){
		 loading();
	 $.post(urlnya,{id:id},function(data){
		  	toastr.success("Yeay", "Sukses !", {
							"timeOut": "0",
							"extendedTImeout": "0",
							"closeButton": true,
							  "debug": false,
							  "positionClass": "toast-top-right",
							  "onclick": null,
							  "showDuration": "10000",
							  "hideDuration": "10000",
							  "timeOut": "50000",
							  "extendedTimeOut": "10000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
						});
		 dataTable.ajax.reload(null,false);
		 
			jQuery.unblockUI({ });
	 })
	 })
	
	 
 })
 
 

</script>
			
                    