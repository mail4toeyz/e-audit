   <div id="showform"></div>        
                                 
					  <div class="card form" id="form1">
                        <div class="card-header">
                           <?php 
						  $arje = array("1"=>"MI/SD Sederajat","2"=>"MTS/SMP Sederajat","3"=>"MA/SMA Sederajat");
						 ?>
                            <h3><i class="fa fa-users"></i> PESERTA TES <?php echo strtoupper($arje[$jenjang]); ?></h3>
                        </div>
                        <br>
                         
                            <div class="col-md-12">
							<div class="alert alert-info">
							 Petunjuk ! <br>
							Data Peserta Telah Anda Verifikasi Lulus dan Berhak Mengikuti KSM Tingkat Kabupaten/Kota.<br>
							Silahkan Download Kelengkapan Pelaksanaan Test Kompetisi Sains Madrasah Tingkat Kabupaten/Kota dibawah ini
							</div>
								
			 
			 
			              </div>
						  
						  <form>
							<div class="col-md-12">
					 					 <div class="row">
										 
										    <div class="col-md-2"> <b> PENCARIAN  : </b></div>
                                           
										   
										
										 
										  
										     <div class="col-md-3">
												<div class="form-group ">
													<select id="trkompetisi_id" class="form-control">
													<option value=""> - Cari kompetisi -</option>
													  <?php 
													    $ko = $this->db->get_where("tr_kompetisi",array("tmmadrasah_id"=>$jenjang))->result();
														  foreach($ko as $r){
															?> <option value="<?php echo $r->id; ?>" > <?php echo $r->nama; ?> </option><?php   
															  
														  }
														  
														?>
													  
													  
													
													</select>
												</div>
										  </div>
										  
										  
										   <div class="col-md-3">
													
													<div class="form-group">
															<input type="text" size="30" name="keyword" id="keyword" class="form-control" placeholder="Cari Nama Siswa ">
															<input type="hidden" id="jenjang" class="form-control" value="<?php echo $jenjang; ?>">
															
														
												   </div>
											 </div>
											 
										 <div class="col-md-1"><button type="button" class="btn btn-danger "  id="searchcustom"><span class="fa fa-search"></span> </button>
								      
										  
										 
										 </div>
									
							
					</div>
					</div>
			</form>
			
		           <hr>
		<div class="col-md-12">  
		     
		         <div style="float:right">
			           <a class="btn btn-danger btn-sm" href="<?php echo base_url(); ?>piagam/BA.docx" target="_blank"><span class="fa fa-briefcase"></span> BERITA ACARA </a>
			           <a class="btn btn-danger btn-sm"  href="<?php echo base_url(); ?>piagam/LAMPIRAN_SK.xlsx" target="_blank"><span class="fa fa-briefcase"></span> LAMPIRAN SK  </a>
			           <a class="btn btn-danger btn-sm"  href="<?php echo base_url(); ?>piagam/PAKTA_INTEGRITAS.docx" target="_blank"> <span class="fa fa-briefcase"></span>  FAKTA INTEGRITAS  </a>
			           <button  class="btn btn-danger btn-sm cetak" datajenis="kartumeja"  data-toggle="modal" data-target="#myModalgg"><span class="fa fa-briefcase"></span> KARTU MEJA</button>
	        					
					   <button  class="btn btn-danger btn-sm cetak" datajenis="absensipeserta"  class="btn red btn-sm" data-toggle="modal" data-target="#myModalgg"><span class="fa  fa-briefcase"></span> ABSENSI PESERTA </button>
					   <button  class="btn btn-danger btn-sm cetak" datajenis="daftarhadir"  class="btn red btn-sm" data-toggle="modal" data-target="#myModalgg"><span class="fa  fa-briefcase"></span> DATA PESERTA </button>
					   <button style="display:none"  class="btn btn-danger btn-sm cetak" datajenis="albumpeserta"  class="btn red btn-sm" data-toggle="modal" data-target="#myModalgg"><span class="fa  fa-briefcase"></span> ALBUM PESERTA </button>
					</div>
					 
			  <br>
			  <br>
		
		   <div class="table-responsive">
			<table id="datatableTable" class="  table-bordered table-hover table-striped" width="100%">
			  <thead>
				<tr>
				   <th class="center" width="3%">No</th>				   				  
				   <th class="center">Nomor Peserta </th>	
                   <th class="center">Nama Peserta </th>
                  
                    <th class="center">Gender</th>
                     <th class="center">NPSN/NSM </th>
				   <th class="center">Madrasah/Sekolah </th>
				   <th class="center">Kompetisi</th>
               
				  
				</tr>
			  </thead>
			  <tbody></tbody>
            </table>
			</div>
			
			
		
        </div>
        </div>
		
<div id="myModalgg" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
     
      <div class="modal-body" id="load_data">
        loading.. mohon tunggu 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
	

	
    });

  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<b style="color:white;background:#33AFFF">Data sedang di tampilkan..</b>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data tidak atau belum tersedia, silahkan input data",
						   "sLengthMenu": "Default menampilkan   _MENU_ Data <small>(Klik pada angkanya untuk memuat lebih banyak data sebelum mencetak excel)</small>"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": true,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							exportOptions: {
							  columns: [ 0,1,2,3,4,5,6]
							},
							text:'<span class="fa fa-file-excel-o"></span> Cetak Excel',
							className :"btn btn-danger btn-sm"
							},
							
							
							{
							extend: 'copyHtml5',
							exportOptions: {
							      columns: [ 0,1,2,3,4,5]
							},
							text:'<span class="fa fa-copy "></span> Copy Data ',
							className :"btn btn-danger btn-sm"
						},
							{
							extend: 'csvHtml5',
							exportOptions: {
							   columns: [ 0,1,2,3,4,5,6]
							},
							text:'<span class="fa fa-file-archive-o "></span> Cetak CSV ',
							className :"btn btn-danger "
						},
						
							{
							extend: 'colvis',
							
							text:'<span class="fa fa-columns "></span> Setting Kolom ',
							className :"btn btn-danger "
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("klokal/g_pesertates"); ?>", 
						type: "post", 
						"data": function ( data ) {
						 data.kota    = $('#kota').val();
						 data.jenjang = $('#jenjang').val();
						 data.madrasah = $('#madrasah').val();
						 data.trkompetisi_id = $('#trkompetisi_id').val();
						
						 data.keyword = $('#keyword').val();
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
	
	
		
			$(document).on('change', '#kota,#jenjang,#madrasah,#trkompetisi_id', function (event, messages) {			
			 
			     dataTable.ajax.reload(null,false);	        
		  
        });
		
			$(document).on('input', '#keyword', function (event, messages) {			
			 
			     dataTable.ajax.reload(null,false);	        
		  
        });
		
		
		 $(document).on("change",".onchange2",function(event){
				 event.preventDefault()
				 var kota    = $("#kota").val();
				 var jenjang    = $("#jenjang").val();
			
				 
				 loading();
				
				 $.post("<?php echo site_url("publik/madrasah"); ?>",{kota:kota,jenjang:jenjang},function(data){
					 
					 $("#madrasah").html(data).show();
				
					 
					   
						jQuery.unblockUI({ });
						
						
				 })
			  
			  
			  
			 })

			 
			  $(document).on("change","#jenjang",function(event){
				 event.preventDefault()
				 var tmmadrasah_id    = $(this).val();
				 
			
				 
				 loading();
				
				 $.post("<?php echo site_url("publik/kompetisi"); ?>",{tmmadrasah_id:tmmadrasah_id},function(data){
					 
					 $("#trkompetisi_id").html(data).show();
				
					 
					   
						jQuery.unblockUI({ });
						
						
				 })
			  
			  
			  
			 })
		
		
		  	 $(document).off('click', '.daftarhadir').on('click', '.daftarhadir', function (event, messages) {			
			 
			 var jenjang = $("#jenjang").val();
			 var kompetisi = $("#trkompetisi_id").val();
			  if(jenjang==""){
			      
			      alertify.alert("Jenjang Harus di pilih");
			      return false;
			  }
			  	  if(kompetisi==""){
			      
			      alertify.alert("kompetisi Harus di pilih");
			      return false;
			  }
			 location.href ="<?php echo site_url("klokal/daftarhadir"); ?>?jenjang="+jenjang+"&kompetisi="+kompetisi;
		  
        });

           $(document).off('click', '.absensipeserta').on('click', '.absensipeserta', function (event, messages) {			
			 
			 var jenjang = $("#jenjang").val();
			 var kompetisi = $("#trkompetisi_id").val();
			 alert(kompetisi)
			  if(jenjang==""){
			      
			      alertify.alert("Jenjang Harus di pilih");
			      return false;
			  }
			  	  if(kompetisi==""){
			      
			      alertify.alert("kompetisi Harus di pilih");
			      return false;
			  }
			 location.href ="<?php echo site_url("klokal/absensipeserta"); ?>?jenjang="+jenjang+"&kompetisi="+kompetisi;
		  
        });			
		
		
		$(document).off('click', '.kartumeja').on('click', '.kartumeja', function (event, messages) {			
			 
			 var jenjang = $("#jenjang").val();
			 var kompetisi = $("#trkompetisi_id").val();
			  if(jenjang==""){
			      
			      alertify.alert("Jenjang Harus di pilih");
			      return false;
			  }
			  	  if(kompetisi==""){
			      
			      alertify.alert("kompetisi Harus di pilih");
			      return false;
			  }
			 location.href ="<?php echo site_url("klokal/kartumeja"); ?>?jenjang="+jenjang+"&kompetisi="+kompetisi;
		  
        });		

        $(document).off('click', '.cetak').on('click', '.cetak', function (event, messages) {			
			 
			 var jenjang = $("#jenjang").val();
			 var kompetisi = $("#trkompetisi_id").val();
			 var jenis   = $(this).attr("datajenis");
			 
			 
			 $.post("<?php echo site_url("klokal/pencetakan"); ?>",{jenjang:jenjang,kompetisi:kompetisi,jenis:jenis},function(data){
				 
				 
				$("#load_data").html("<br><br>"+data);
				 
				 
			 })
			
		  
        });			
		

</script>
			
                    