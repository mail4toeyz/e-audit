<?php

class M_lokal extends CI_Model {
 
    public function __construct() {
        parent::__construct();
    }

    
	
	
	public function m_madrasah($paging){
               $jenis     = $_POST['jenis'];
               $jenjang   = $_POST['jenjang'];
               $status    = $_POST['status'];
               $kolom     = $_POST['kolom'];
        $keyword = $_POST['keyword'];
		$this->db->select("*");
        $this->db->from('tm_madrasah');
        $this->db->where('kota',$_SESSION['tmkota_id']);
        //$this->db->where('sts',0);
	    if(!empty($status)){ $this->db->where("status",$status); } 
	    if(!empty($jenis)){ $this->db->where("jenis",$jenis); } 
	    if(!empty($jenjang)){ $this->db->where("jenjang",$jenjang); } 
	    if(!empty($keyword)){ $this->db->where("UPPER(".$kolom.") LIKE '%".$keyword."%'"); } 
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("jenjang","ASC");
					 $this->db->order_by("nama","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
	public function i_peserta($imagenew){
		
		
		$kota   = $this->db->get_where("kota",array("id"=>$_SESSION['tmkota_id']))->row();
		$tmsiswa_id    = $this->Di->get_max_id("id","tm_siswa")+1;
		$m                     = $this->input->get_post("m");
		$f                     = $this->input->get_post("f");
		$this->db->insert("tm_madrasah",$m);
		$tmmadrasah_id    = $this->Di->get_max_id("id","tm_madrasah");
		
		
		$this->db->set("id",$tmsiswa_id);
		$this->db->set("tmmadrasah_id",$tmmadrasah_id);
		 $this->db->set("foto",$imagenew);
		$this->db->set("tgl_lahir",$this->Di->formattanggaldb($_POST['tgl_lahir']));
		$this->db->set('d_entry', date('Y-m-d H:i:s'));
	    $this->db->set('i_entry', $_SESSION['nama']);
	    $this->db->set('status', 1);
		
		$this->db->insert("tm_siswa",$f);
		
		
		        if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
				   
				} else {
						
					$this->db->trans_commit();
					
				}
		
	}
	
	
	public function u_peserta($id){
		$madrasah   = $this->db->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']))->row();
		$f                     = $this->input->get_post("f");
		$this->db->set("tgl_lahir",$this->Di->formattanggaldb($_POST['tgl_lahir']));
		$this->db->set('d_update', date('Y-m-d H:i:s'));
	    $this->db->set('i_update', $_SESSION['nama']);
		$this->db->set("jenjang",$madrasah->jenjang);
		$this->db->set("provinsi",$madrasah->provinsi);
		$this->db->set("kota",$madrasah->kota);
	    $this->db->where('id', $id);
		
		$this->db->update("tm_siswa",$f);
		
		
		        if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
				   
				} else {
						
					$this->db->trans_commit();
					
				}
		
	}
	
	
	public function m_peserta($paging){
     
     
        $kolom     = $_POST['kolom'];
        $status_siswa     = $_POST['status_siswa'];
        $jenjang     = $_POST['jenjang'];
       
        $trkompetisi_id   = $_POST['trkompetisi_id'];
        $status   = $this->input->post("status",true);
		$keyword   = $_POST['keyword'];
		
		$this->db->select("*");
        $this->db->from('v_siswa');
        $this->db->where("kota_madrasah",$_SESSION['tmkota_id']);
		$this->db->where("jenjang_madrasah",$jenjang);
	
		
		if(!empty($keyword)){ $this->db->where("UPPER({$kolom}) LIKE '%".strtoupper($keyword)."%'"); }
		if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 
		if(!empty($status_siswa)){ $this->db->where("status",$status_siswa); }else{ 	$this->db->where("status IN(0,1,3)"); }
		
		if(!empty($status)){

			if($status==0){
				$this->db->where("status < 1");
			}else{
			$this->db->where("status",$status);
			}
		   
		    
		} 

         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("status","DESC");
					 //$this->db->order_by("tmmadrasah_id","ASC");
					 $this->db->order_by("trkompetisi_id","ASC");
					 $this->db->order_by("lokasi_kabko","ASC");
					 $this->db->order_by("sesi_kabko","ASC");
					 $this->db->order_by("nama","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	public function m_hasilkabko($paging){
           
		$kolom     = $_POST['kolom'];
        $jenjang     = $_POST['jenjang'];
		$kota     = $_POST['kota'];
		$provinsi     = $_POST['provinsi'];
        $trkompetisi_id   = $_POST['trkompetisi_id'];
        $status   = $this->input->post("status",true);
		$keyword   = $_POST['keyword'];
		$d_p       = $_POST['d_p'];
		$ctt       = $_POST['ctt'];
		$skrn       = $_POST['skrn'];
		$medali       = $_POST['medali'];
		
		$this->db->select("foto,nik_siswa,medali,id,no_test,nama,tgl_lahir,skrn,provinsi_madrasah,kota_madrasah,trkompetisi_id,ketkabko,bnr_pg,bnr_isi,jml_benar,slh,ksg,nla,jenis,jenjang,username,madrasah");
        $this->db->from('v_siswa_kabkota');
		$this->db->where('nla is not null');
        $this->db->where('bnr_isi is not null');
        $this->db->where('bnr_pg is not null');
        $this->db->where('slh is not null');
        $this->db->where('ksg is not null');
    
		
       $this->db->where("jenjang_madrasah",$jenjang);
			
	   $this->db->where("kota_madrasah",$_SESSION['tmkota_id']);
   
	   
	   if(!empty($keyword)){ $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'"); }
	   if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 

	   if(!empty($d_p)){ $this->db->where("d_p",$d_p); } 
	  
	   if(!empty($skrn)){ $this->db->where("skrn",$skrn); } 

		
	   
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("trkompetisi_id","asc");
					 $this->db->order_by("nla","DESC");
					 $this->db->order_by("jml_benar","DESC");
					 $this->db->order_by("slh","ASC");
					 $this->db->order_by("tgl_lahir","DESC");
					 
		
			 }
      
		
		
		return $this->db->get();
		
	}

	public function m_hasilprov($paging){
           
		$kolom     = $_POST['kolom'];
        $jenjang     = $_POST['jenjang'];
		$kota     = $_POST['kota'];
		$provinsi     = $_POST['provinsi'];
        $trkompetisi_id   = $_POST['trkompetisi_id'];
        $status   = $this->input->post("status",true);
		$keyword   = $_POST['keyword'];
		$d_p       = $_POST['d_p'];
		$ctt       = $_POST['ctt'];
		$skrn       = $_POST['skrn'];
		$medali       = $_POST['medali'];
		
		$this->db->select("*");
        $this->db->from('v_siswa_provinsi');
        $this->db->where('nla is not null');
    
		
       $this->db->where("jenjang_madrasah",$jenjang);
			
	   $this->db->where("kota_madrasah",$_SESSION['tmkota_id']);
   
	   
	   if(!empty($keyword)){ $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'"); }
	   if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 

	   if(!empty($d_p)){ $this->db->where("peringkat_prov",$d_p); } 
	   if(!empty($medali)){ $this->db->where("medali",$medali); } 
	   if(!empty($ctt)){ $this->db->where("ctt",$ctt); } 
	   if(!empty($skrn)){ $this->db->where("skrn",$skrn); } 

		
	   
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("trkompetisi_id","asc");
					 $this->db->order_by("nla","DESC");
					 $this->db->order_by("jml_benar","DESC");
					 $this->db->order_by("slh","ASC");
					 $this->db->order_by("nilai_kabko","DESC");
					 $this->db->order_by("tgl_lahir","DESC");
					 
		
			 }
      
		
		
		return $this->db->get();
		
	}

	public function m_pesertates($paging){
           
        $jenjang   = $_POST['jenjang'];
       
        $trkompetisi_id   = $_POST['trkompetisi_id'];        
        $keyword   = $_POST['keyword'];
		
		$this->db->select("no_test,id,tgl_lahir,tmmadrasah_id,nama,gender,trkompetisi_id,kota,jenjang,nilai,benar,salah,kosong,d_p");
        $this->db->from('tm_siswa');
    
		$this->db->where("status",1);
	    $this->db->where("kota",$_SESSION['tmkota_id']);
       $this->db->where("jenjang",$jenjang);
			//$this->db->where("tmmadrasah_id in (select id from tm_madrasah where kota='".$_SESSION['tmkota_id']."') and jenjang ='".$jenjang."'");
	
		if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 
		if(!empty($keyword)){ $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'"); }
		
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("trkompetisi_id","asc");
					 $this->db->order_by("nama","asc");
					 
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	public function m_pesertateshasil($paging){
           
        $jenjang   = $_POST['jenjang'];
       
        $trkompetisi_id   = $_POST['trkompetisi_id'];        
        $keyword   = $_POST['keyword'];
		
		$this->db->select("no_test,id,tgl_lahir,tmmadrasah_id,nama,gender,trkompetisi_id,kota,jenjang,nilai,benar,salah,kosong,d_p");
        $this->db->from('tm_siswa');
    
		$this->db->where("status",1);
	    $this->db->where("kota",$_SESSION['tmkota_id']);
       $this->db->where("jenjang",$jenjang);
       $this->db->where("no_test !=''");
			//$this->db->where("tmmadrasah_id in (select id from tm_madrasah where kota='".$_SESSION['tmkota_id']."') and jenjang ='".$jenjang."'");
	
		if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 
		if(!empty($keyword)){ $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'"); }
		
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("trkompetisi_id","ASC");
					 $this->db->order_by("nilai","DESC");
					 $this->db->order_by("benar","DESC");
					 $this->db->order_by("salah","ASC");
					 $this->db->order_by("tgl_lahir","desc");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	public function m_pesertasertifikat($paging){
           
        $jenjang   = $_POST['jenjang'];
       
        $trkompetisi_id   = $_POST['trkompetisi_id'];        
        $keyword   = $_POST['keyword'];
		
		$this->db->select("status2,tgl_lahir,id,tmmadrasah_id,tgl_lahir,nama,gender,trkompetisi_id,kota,jenjang,nilai,benar,salah,kosong,d_p,no_test");
        $this->db->from('tm_siswa');
    
		$this->db->where("status",1);
	/*	$this->db->where("benar !=",0);
        $this->db->where("salah !=",0);
        $this->db->where("kosong !=",0);
		$this->db->where("nilai !=",0); */
		
		$this->db->where("(benar+salah+kosong) !=0");
	
	    $this->db->where("kota",$_SESSION['tmkota_id']);
        $this->db->where("jenjang",$jenjang);
			//$this->db->where("tmmadrasah_id in (select id from tm_madrasah where kota='".$_SESSION['tmkota_id']."') and jenjang ='".$jenjang."'");
	
		if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 
		if(!empty($keyword)){ $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'"); }
		
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("trkompetisi_id","ASC");
					 $this->db->order_by("nilai","DESC");
					 $this->db->order_by("benar","DESC");
					 $this->db->order_by("salah","ASC");
					 $this->db->order_by("tgl_lahir","desc");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	public function m_peserta_provinsi($paging){
           
        $jenjang   = $_POST['jenjang'];
       
        $trkompetisi_id   = $_POST['trkompetisi_id'];        
        $keyword   = $_POST['keyword'];
		
		$this->db->select("*");
        $this->db->from('v_siswa');
    
		$this->db->where("status2",1);
	
	    $this->db->where("kota_madrasah",$_SESSION['tmkota_id']);
        if(!empty($jenjang)){ $this->db->where("jenjang_madrasah",$jenjang); }
			
		if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 
		if(!empty($keyword)){ $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'"); }
		
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("jenjang","ASC");
					 $this->db->order_by("trkompetisi_id","ASC");
					 $this->db->order_by("d_p","ASC");
					 
		
			 }
      
		
		
		return $this->db->get();
		
	}

	public function m_peserta_nasional($paging){
           
        $jenjang   = $_POST['jenjang'];
       
        $trkompetisi_id   = $_POST['trkompetisi_id'];        
        $keyword   = $_POST['keyword'];
		
		$this->db->select("*");
        $this->db->from('v_siswa');
    
		$this->db->where("status3",1);
	
	    $this->db->where("kota_madrasah",$_SESSION['tmkota_id']);
        if(!empty($jenjang)){ $this->db->where("jenjang_madrasah",$jenjang); }
			
		if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 
		if(!empty($keyword)){ $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'"); }
		
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("jenjang","ASC");
					 $this->db->order_by("trkompetisi_id","ASC");
				
					 
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	public function g_pendamping($paging){
    
		
		$this->db->select("*");
        $this->db->from('tr_panitia');
		$this->db->where("kota",$_SESSION['tmkota_id']);

	
		
	 
			
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					
					 $this->db->order_by("nama","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	public function i_pendamping(){
		
		
		
		$f  = $this->input->get_post("f");
        $provinsi = $this->Di->get_kondisi(array("id"=>$_SESSION['tmkota_id']),"kota","provinsi_id");
		
		
		$this->db->set('kota',$_SESSION['tmkota_id']);
		$this->db->set('provinsi',$provinsi);
		$this->db->set('jenis',"kankemenag");
		
		
		$this->db->insert("tr_panitia",$f);
		
		
		        if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
				   
				} else {
						
					$this->db->trans_commit();
					
				}
		
	}
	
	public function u_pendamping($id){
		
		
		
		$f  = $this->input->get_post("f");

		$this->db->where('id',$id);
		
		
		$this->db->update("tr_panitia",$f);
		
		
		        if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
				   
				} else {
						
					$this->db->trans_commit();
					
				}
		
	}
	

	public function g_lokasi($paging){
	
		$keyword = $_POST['keyword'];
		$this->db->select("*");
		$this->db->from('tr_lokasites');
		$this->db->where('kota',$_SESSION['tmkota_id']);
		
		if(!empty($keyword)){ $this->db->where("UPPER(nama) LIKE '%".$keyword."%'"); } 
		
		
		if($paging==true){
					$this->db->limit($_REQUEST['length'],$_REQUEST['start']);					
					$this->db->order_by("nama","ASC");
		
			}

		
		
		return $this->db->get();
		
		}

	
	
}
