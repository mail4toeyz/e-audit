<?php
use setasign\Fpdi\Fpdi;

require_once(APPPATH.'libraries/fpdf/fpdf.php');
require_once(APPPATH.'libraries/fpdi/src/autoload.php');
defined('BASEPATH') OR exit('No direct script access allowed');
define('PHPWORD_BASE_DIR', realpath(__DIR__));
class Klokal extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		$this->load->model('M_lokal');
		 $this->load->helper('exportpdf_helper');  
		
	   
		
		 if(!$this->session->userdata("tmkota_id")){						
         $ajax = $this->input->get_post("ajax",true);		 
	     	if(!empty($ajax)){
			
				echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
				   exit();
			  }else{
				  redirect(site_url()."ksm/login");
			  }
		 
		}
		
	  }
     public function laporan(){
		
		
		
		
		
									
          $ajax = $this->input->get_post("ajax",true);	
		  $data['title']           = "Laporan Pendaftaran ";
		  $data['dt']              = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('laporan',$data);
		 }else{
			 
			
		     $data['konten'] = "laporan";
			 
			 $this->load->view('kdashboard/page_header',$data);
		 }
	
		
	}
	
	
	public function madrasah(){
		
		
		
		
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']           = "Data Madrasah/Sekolah Peserta KSM di Kab/Kota Anda ";
		  $data['dt']              = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('madrasah/page',$data);
		 }else{
			 
			
		     $data['konten'] = "madrasah/page";
			 
			 $this->load->view('kdashboard/page_header',$data);
		 }
	
		
	}
	
	
	public function g_madrasah(){
		
		  $iTotalRecords = $this->M_lokal->m_madrasah(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->M_lokal->m_madrasah(true)->result_array();
		   $arjenis = array("1"=>"Madrasah","2"=>"Sekolah");
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
						   
			$draft           = $this->db->query("select count(id) as jml from tm_siswa where tmmadrasah_id='{$val['id']}' and status=0")->row();;                
			$terkirim		 = $this->db->query("select count(id) as jml from tm_siswa where tmmadrasah_id='{$val['id']}' and status=1")->row();;                
			$lulus   		 = $this->db->query("select count(id) as jml from tm_siswa where tmmadrasah_id='{$val['id']}' and status=2")->row();;                
			$tidak_lulus     = $this->db->query("select count(id) as jml from tm_siswa where tmmadrasah_id='{$val['id']}' and status=3")->row();;                
				 
					
						   if($val['sts']=="1"){
					  
				
					     $tombol ='	<a class="btn btn-danger btn-sm  kelaaktifkan " disabled style="color:white;cursor:pointer" urlnya = "'.site_url("klokal/a_madrasah").'"  datanya="'.$val['id'].'" title="Aktifkan">
					           Nonaktif </a>';
					  
				  }else{
				
					    $tombol ='	<a class="btn btn-success btn-sm  kelanonaktifkan " disabled style="color:white;cursor:pointer" urlnya = "'.site_url("klokal/n_madrasah").'"  datanya="'.$val['id'].'" title="Nonaktifkan">
					Aktif</a>';
					  
				  }
				$no = $i++;
				$records["data"][] = array(
					$no,
					'
					<a class="btn btn-info  ubah" urlnya = "'.site_url("klokal/f_madrasah").'"   datanya="'.$val['id'].'" title="Ubah Data">
					<i class=" fa fa-pencil-square  " style="color:white"></i>
				
				 
					 ',		
					$arjenis[$val['jenis']],
					$this->Di->jenjangnama($val['jenjang']),
					$val['username'],
				//	$val['password'],
					$val['nama'],
					$val['telepon'],
					$val['email'],
					
					$draft->jml,
					$terkirim->jml,
					$lulus->jml,
					$tidak_lulus->jml,
				
					$val['delegasi_nik'],
					$val['delegasi_nama'],
					$val['delegasi_hp'],
					$tombol,
				
					
					
					
					
                   			
										
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	
	public function f_madrasah(){
		
		 $id = $this->input->get_post("id",TRUE);
		 
		 $data = array();
		    if(!empty($id)){
				
				$data['dataform'] = $this->Di->get_where("tm_madrasah",array("id"=>$id));
			}
	 
		 $data['title']  = "Form ";
		 $this->load->view('madrasah/form',$data);
	}
	
	
	public function s_madrasah(){
     
        $this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
        
        $this->form_validation->set_message('min_length', '{field} Minimal 6 Karakter .');
        $this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
        $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , mungkin anda sudah pernah melakukan pendaftaran, silahkan klik menu login .');
				 
		 
			
				$config = array(
				    array('field' => 'password', 'label' => 'Password Baru  ', 'rules' => 'trim|required'),
				  
				 
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			  
			  
			   $this->db->set("password",(($_POST['password'])));
			   $this->db->where("id",$_POST['id']);
			   $this->db->update("tm_madrasah");
			   $this->Di->log($_SESSION['nama']. "Merubah Password Madrasah","k");
			   echo "sukses";
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }	
	
	
	}
	
	
	
	public function n_madrasah(){
		
		$this->db->set("sts",1);
		$this->db->where("id",$_POST['id']);
		$this->db->update("tm_madrasah");
		echo "sukses";
		
	}
	public function a_madrasah(){
		
		$this->db->set("sts",0);
		$this->db->where("id",$_POST['id']);
		$this->db->update("tm_madrasah");
		echo "sukses";
		
	}
	public function lokasi(){
		
		
		
		 if(!$this->session->userdata("tmkota_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
               exit();
		  }
	   
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Lokasi Tes";
		 $data['dt']              = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('lokasi/page',$data);
		 }else{
			 
			
		     $data['konten'] = "lokasi/page";
			 
			 $this->load->view('kdashboard/page_header',$data);
		 }
	
		
	}


	public function g_lokasi(){
		
		$iTotalRecords = $this->M_lokal->g_lokasi(false)->num_rows();
		
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);
		
		$records = array();
		$records["data"] = array(); 

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;
		
		$datagrid = $this->M_lokal->g_lokasi(true)->result_array();
		$jenjangArr = array("1"=>"MI/SD","2"=>"MTs/SMP","3"=>"MA/SMA");
		 $i= ($iDisplayStart +1);
		 foreach($datagrid as $val) {
											
			  $pemetaan =  explode(",",$val['pemetaan']);
			   $vpemetaan ="<ol>";
			    foreach($pemetaan as $rp){
					$vpemetaan .="<li>".$this->Di->get_kondisi(array("id"=>$rp),"kecamatan","nama")."</li>";
				}	
				$vpemetaan .="</ol>";
				
				$jenjang =  explode(",",$val['jenjang']);
			   $vjenjang ="<ol>";
			    foreach($jenjang as $rp){
					$vjenjang .="<li>".$jenjangArr[$rp]."</li>";
				}	
				$vjenjang .="</ol>";
								 
			  $no = $i++;
			  $sesi1 = $this->db->query("SELECT count(id) as jml from tm_siswa where lokasi_kabko='".$val['id']."' and sesi_kabko=1")->row();
			  $sesi2 = $this->db->query("SELECT count(id) as jml from tm_siswa where lokasi_kabko='".$val['id']."' and sesi_kabko=2")->row();
			  $sesi3 = $this->db->query("SELECT count(id) as jml from tm_siswa where lokasi_kabko='".$val['id']."' and sesi_kabko=3")->row();
			  $sesi4 = $this->db->query("SELECT count(id) as jml from tm_siswa where lokasi_kabko='".$val['id']."' and sesi_kabko=4")->row();
			  $sesi5 = $this->db->query("SELECT count(id) as jml from tm_siswa where lokasi_kabko='".$val['id']."' and sesi_kabko=5")->row();
			  $sesi6 = $this->db->query("SELECT count(id) as jml from tm_siswa where lokasi_kabko='".$val['id']."' and sesi_kabko=6")->row();
			  $sesi7 = $this->db->query("SELECT count(id) as jml from tm_siswa where lokasi_kabko='".$val['id']."' and sesi_kabko=7")->row();
			  $sesi8 = $this->db->query("SELECT count(id) as jml from tm_siswa where lokasi_kabko='".$val['id']."' and sesi_kabko=8")->row();
			  $sesi9 = $this->db->query("SELECT count(id) as jml from tm_siswa where lokasi_kabko='".$val['id']."' and sesi_kabko=9")->row();
			  $sesi10 = $this->db->query("SELECT count(id) as jml from tm_siswa where lokasi_kabko='".$val['id']."' and sesi_kabko=10")->row();
			  $jmlPeserta = $this->db->query("SELECT count(id) as jml from tm_siswa where lokasi_kabko='".$val['id']."' ")->row();

			  $records["data"][] = array(
				  $no,
				  '<div class="btn-group" role="group" aria-label="Basic example">
				  <button type="button" class="btn btn-primary btn-sm ubah"  urlnya = "'.site_url("klokal/f_lokasi").'"  datanya="'.$val['id'].'" title="Ubah Data"><i class=" fa fa-pencil-square"></i> Update  </button>
				  <button type="button" class="btn btn-danger hapus btn-sm" style="display:none"  urlnya = "'.site_url("klokal/h_lokasi").'"  datanya="'.$val['id'].'" title="Hapus Data" ><i class=" fa fa-trash"></i>  Hapus </button>
				 
				  
			     </div>',
				  $val['nama'],
				  $val['alamat'],
				  $val['kuota'],
				 
				  $sesi1->jml,
				  $sesi2->jml,
				  $sesi3->jml,
				  $sesi4->jml,
				  $sesi5->jml,
				  $sesi6->jml,
				  $sesi7->jml,
				  $sesi8->jml,
				  $sesi9->jml,
				  $sesi10->jml,
				  $jmlPeserta->jml,
				  $vpemetaan,
				  $vjenjang
				 
				  
				  
									  
						  
			  
				 

				);
			}
	  
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		
		echo json_encode($records);
  }


	public function f_lokasi(){
		
		$id = $this->input->get_post("id",TRUE);
		
		$data = array();
		   if(!empty($id)){
			   
			   $data['dataform'] = $this->Di->get_where("tr_lokasites",array("id"=>$id));
		   }
	
		$data['title']  = "Form ";
		$this->load->view('lokasi/form',$data);
   }
	
	public function s_lokasi(){
     
        $this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
        
        $this->form_validation->set_message('min_length', '{field} Minimal 6 Karakter .');
        $this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
        $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , mungkin anda sudah pernah melakukan pendaftaran, silahkan klik menu login .');
				 
		 
			
				$config = array(
				    array('field' => 'f[nama]', 'label' => 'Nama Lokasi', 'rules' => 'trim|required'),
				    array('field' => 'f[alamat]', 'label' => 'Alamat Lokasi', 'rules' => 'trim|required'),
				    array('field' => 'pemetaan[]', 'label' => 'Pemetaaan Lokasi Tes', 'rules' => 'trim|required'),
				    array('field' => 'jenjang[]', 'label' => 'Jenjang Pendidikan', 'rules' => 'trim|required'),
				    array('field' => 'f[kuota]', 'label' => 'Jumlah Ketersediaan Komputer', 'rules' => 'trim|required'),
				    
				 
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			   $id = $this->input->get_post("id");
			   $f = $this->input->get_post("f");
			   $pemetaan = implode(",",$this->input->get_post("pemetaan"));
			   $jenjang  = implode(",",$this->input->get_post("jenjang"));
			   $kota     = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));

			   if(empty($id)){
				   $this->db->set("tingkat",1);
				   $this->db->set("provinsi",$kota->provinsi_id);
				   $this->db->set("kota",$kota->id);
				   $this->db->set("pemetaan",$pemetaan);
				   $this->db->set("jenjang",$jenjang);
				   $this->db->insert("tr_lokasites",$f);
				   $this->Di->log($_SESSION['nama']. "Menentukan Lokasi Tes","k");

			   }else{
					$this->db->set("tingkat",1);
					$this->db->set("provinsi",$kota->provinsi_id);
					$this->db->set("kota",$kota->id);
					$this->db->set("pemetaan",$pemetaan);
					$this->db->set("jenjang",$jenjang);
					$this->db->where("id",$id);
					$this->db->update("tr_lokasites",$f);

					$this->Di->log($_SESSION['nama']. "Merubah Lokasi Tes","k");
			   }
			  
			  
			   
			   
			   echo "sukses";
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }	
	
	
	}
	

	public function h_lokasi(){
		
		$this->db->delete("tr_lokasites",array("id"=>$_POST['id']));
		
		echo "sukses";
		
	}


	public function loadkecamatan(){
 
		$id = $this->input->get_post("id",TRUE);
		
		$data = array();
		   if(!empty($id)){
			   
			   $data['dataform'] = $this->Di->get_where("tr_lokasites",array("id"=>$id));
		   }
		$data['jenjang'] 		 = implode(",",$this->input->get_post("jenjang",true));
		
		$data['data']    = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
 
		$this->load->view("lokasi/loadkecamatan",$data);
		 


	}

	// Jadwal 


	public function jadwal(){
		
		
	   
								   
		$ajax = $this->input->get_post("ajax",true);	
		 $data['title']  = "Jadwal KSM Kabkota";
		$data['dt']              = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
		if(!empty($ajax)){
					   
			$this->load->view('lokasi/jadwal',$data);
		}else{
			
		   
			$data['konten'] = "lokasi/jadwal";
			
			$this->load->view('kdashboard/page_header',$data);
		}
   
	   
   }

	
	public function peserta(){
		
		
		
		 if(!$this->session->userdata("tmkota_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
               exit();
		  }
	   
		
									
           $ajax            = $this->input->get_post("ajax",true);	
           $data['jenjang'] = base64_decode($this->input->get_post("jenjang",true));	
		   $data['title']   = "Approval Peserta ";
		   $data['dt']              = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('page_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "page_peserta";
			 
			 $this->load->view('kdashboard/page_header',$data);
		 }
	
		
	}
	
	
	public function g_peserta(){
		$simulasi    = array("1"=>"Rabu, 5 Juli 2023","2"=>"Kamis, 6 Juli 2023","3"=>"Jumat, 7 Juli 2023");
		$pelaksanaan = array("1"=>"Sabtu, 8 Juli 2023","2"=>"Minggu, 9 Juli 2023","3"=>"Senin, 10 Juli 2023");


		  $iTotalRecords = $this->M_lokal->m_peserta(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  $kabko = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
		  $zona =  strtolower($this->Di->get_kondisi(array("id"=>$kabko->provinsi_id),"provinsi","zona"));
		  $datagrid = $this->M_lokal->m_peserta(true)->result_array();
		  $arjenis = array("1"=>"Madrasah","2"=>"Sekolah");
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
				                              
												 
				if($val['status']==0){
					
					$status ='<i class="badge badge-default"> Draft (belum dikirim) </i>';
					
				 }else if($val['status']==1){
					 
				    $status ='<button class="btn btn-primary   btn-sm ubah" urlnya = "'.site_url("klokal/verval").'"  datanya="'.$val['id'].'"  style"cursor:pointer"> <i class="fa fa-check-square"></i> Lakukan Verifikasi   </button>';
					 
				 }else if($val['status']==2){
					  $status ='<button class="btn btn-success btn-sm ubah" urlnya = "'.site_url("klokal/verval").'"  datanya="'.$val['id'].'"  style"cursor:pointer"> <i class="fa fa-check-square"></i> Lulus Verifikasi </button>';
					
					}else if($val['status']==3){
						$status ='<button class="btn btn-danger btn-sm ubah" urlnya = "'.site_url("klokal/verval").'"  datanya="'.$val['id'].'"  style"cursor:pointer"><i class="fa fa-times"></i> Tidak Lulus </button>';
					  
				   }

				$foto ="img/ksm.png";
				 if(!empty($val['foto'])){

					$foto ="upload/{$val['foto']}";
				 }
				 $img ='<img src="'.base_url().'__statics/'.$foto.'" class="img-responsive img-circle" style="width:40px">';
				 $notest = "<a href='".site_url("klokal/kartutes?id=".($val['id'])."")."' target='_blank' >".$val['no_test']."</a>";
				 
				 $sesi = $this->db->query("SELECT * from tm_sesi where id='".$val['sesi_kabko']."'")->row();
				
				$no = $i++;
				$records["data"][] = array(
					$no,
					//$notest,
					$status,
					$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
					$img,
					
					$val['nisn'],
					$val['nama'],
					//'<a href="#" class="ubah" urlnya = "'.site_url("klokal/f_peserta").'"  datanya="'.$val['id'].'">'.$val['nama'].'</a>',					
					$val['gender'],
					$val['tempat'].",".$this->Di->formattanggalstring($val['tgl_lahir']),
					$this->Di->get_kondisi(array("id"=>$val['lokasi_kabko']),"tr_lokasites","nama"),
					$simulasi[$sesi->hari],
					$pelaksanaan[$sesi->hari],
					$sesi->sesi,
					$val['ruang1'],
					$arjenis[$val['jenis']],
					$this->Di->jenjangnama($val['jenjang_madrasah']),
					$val['username'],
					$val['madrasah'],
					$this->Di->get_kondisi(array("id"=>$val['kecamatan']),"kecamatan","nama"),
					$this->Di->get_kondisi(array("id"=>$val['desa']),"desa","nama"),
					$val['delegasi_hp']
					

					
					
										
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}

	public function pesertaruang(){
		
		
		
		if(!$this->session->userdata("tmkota_id")){
		   echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
			  exit();
		 }
	  
	   
								   
		  $ajax              = $this->input->get_post("ajax",true);	
		  $data['kompetisi'] = ($this->input->get_post("kompetisi",true));	
		  $data['title']     = " Peserta  Peruangan";
		  $data['dt']              = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
		if(!empty($ajax)){
					   
			$this->load->view('page_pesertaruang',$data);
		}else{
			
		   
			$data['konten'] = "page_pesertaruang";
			
			$this->load->view('kdashboard/page_header',$data);
		}
   
	   
   }


	
	
	public function verval(){
		
		 $id = $this->input->get_post("id",TRUE);
		 
		 $data = array();
		
				
		 $data['dataform'] = $this->db->query("select * from v_siswa where id='".$id."'")->row();
			
	     $data['m']	             = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
	     $data['madrasah']	     = $this->Di->get_where("tm_madrasah",array("id"=>$data['dataform']->tmmadrasah_id));
		 $data['title']  = "Verifikasi Data ".$data['dataform']->nama;
		 $this->load->view('verifikasi',$data);
	}
	
	
	public function save_verval(){
		$nama_siswa = $this->Di->get_kondisi(array("id"=>$this->input->get_post("tmsiswa_id")),"tm_siswa","nama");
		$status     = $this->input->get_post("status");

		$this->db->set("status",$this->input->get_post("status"));
		$this->db->where("id",$this->input->get_post("tmsiswa_id"));
		$this->db->update("tm_siswa");
		$this->Di->log($_SESSION['nama']. "Melakukan Verifikasi {$nama_siswa} dengan hasil {$status}","k");
		echo "sukses";
		
	}
	
	public function save_keterangan(){
		
		$this->db->set("keterangan",$this->input->get_post("keterangan"));
		$this->db->where("id",$this->input->get_post("tmsiswa_id"));
		$this->db->update("tm_siswa");
		echo "sukses";
		
	}
	
	
	public function f_peserta(){
		
		 $id = $this->input->get_post("id",TRUE);
		 
		 $data = array();
		    if(!empty($id)){
				
				$data['dataform'] = $this->Di->get_where("tm_siswa",array("id"=>$id));
			}
	    $data['m']	  = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
		 $data['title']  = "Perbaiki Data Peserta KSM";
		 $this->load->view('form_peserta',$data);
	}
	
	public function simpanpeserta(){
		
		error_reporting(0);
		
		 $this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
        
    	 
			
				$config = array(
				  //  array('field' => 'f[nisn]', 'label' => 'NISN ', 'rules' => 'trim|required'),
				    array('field' => 'f[nama]', 'label' => 'Nama Siswa  ', 'rules' => 'trim|required'),
				    array('field' => 'f[gender]', 'label' => 'Gender  ', 'rules' => 'trim|required'),
				
				    array('field' => 'f[tempat]', 'label' => 'Tempat Lahir  ', 'rules' => 'trim|required'),
				    array('field' => 'f[tgl_lahir]', 'label' => 'Tanggal Lahir  ', 'rules' => 'trim|required'),
				  
				 
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
           if ($this->form_validation->run() == true) {
			  
		
			$id         = $this->input->get_post("id");
				
			$f  = $this->input->get_post("f");
				
			if(!empty($_FILES['file']['name'])){
					 $config['upload_path'] = './__statics/upload';
					 $folder   = '__statics/upload';
					 $config['allowed_types'] = "*"; 	
					 $config['overwrite'] = true; 	
					 $filenamepecah			 = explode(".",$_FILES['file']['name']);
					 $imagenew				 = $peserta_id.$persyaratan_id.time().".".pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
					 
					 $config['file_name'] = $imagenew;
					 
					 $this->load->library('upload', $config);
  
							 if ( ! $this->upload->do_upload('file'))
							 {
							  $this->db->where("id",$id);
							  $this->db->update("tm_siswa",$f);
								 
							 
							 }else{
							  $id = $this->input->get_post("id",true);
							  $siswanya = $this->db->get_where("v_siswa",array("id"=>$id))->row();
  
									  $madrasah = $this->db->get_where("tm_madrasah",array("id"=>$siswanya->tmmadrasah_id))->row();
  
			  
		  
									  $service = new Google_Drive();
									  $foldernama = $madrasah->username;
									  $foldernama = str_replace("/",DIRECTORY_SEPARATOR,$foldernama);
									  $folderId   = $madrasah->folder;
									  if(!$folderId){
										  $folderId = $service->getFileIdByName( BACKUP_FOLDER );
										  if( !$folderId ) {
											  $folderId = $service->createFolder( BACKUP_FOLDER );					
										  }
										  $folderId = $service->createMultiFolder($foldernama,$folderId);
										  $this->db->set("folder",$folderId);
										  
										  $this->db->where("id",$siswanya->tmmadrasah_id);
										  $this->db->update("tm_madrasah");
									  }
  
									 $fileId = $service->createFileFromPath( $_FILES["file"]['tmp_name'], $_FILES["file"]['name'], $folderId );
									 $service->setPublic($fileId);
  
									 if (empty($fileId)) {
									  header('Content-Type: application/json');
									  echo json_encode(array('error' => true, 'message' =>"Gagal"));
										 
									 }else{
  
										  $this->db->where("peserta_id",$id);
										  $this->db->where("persyaratan_id",1);
										  $this->db->set("file",$fileId);
										  $this->db->update("tr_persyaratan");

																	  
											  $this->db->where("id",$id);
											  $this->db->update("tm_siswa",$f);
  
											   $this->db->query("update tm_siswa set foto='".$imagenew."' where id='".$id."'");
  
  
  
									 }
  
  
  
  
  
							 }


						  }else{

							  $this->db->where("id",$id);
							  $this->db->update("tm_siswa",$f);


						  }


					

					 echo "sukses";
						
									
									
				  
						
						
			 } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
             }	
	
		
	}
	
	public function status_peserta(){
		
		$this->db->set("status",$_POST['status']);
		$this->db->where("id",$_POST['tmsiswa_id']);
		$this->db->update("tm_siswa");
		echo "sukses";
	}
	
	
	
	public function pesertates(){
		
		
		
		 if(!$this->session->userdata("tmkota_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
               exit();
		  }
	   
		
									
           $ajax = $this->input->get_post("ajax",true);	
		   $data['title']  = "Peserta Tes";
		   $data['dt']              = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
		    $data['jenjang'] = base64_decode($this->input->get_post("jenjang",true));
	     if(!empty($ajax)){
					    
			 $this->load->view('peserta/page_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "peserta/page_peserta";
			 
			 $this->load->view('kdashboard/page_header',$data);
		 }
	
		
	}
	
	
	public function g_pesertates(){
		
		  $iTotalRecords = $this->M_lokal->m_pesertates(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->M_lokal->m_pesertates(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
				$madrasah = $this->db->query("select npsn,nama from tm_madrasah where id='".$val['tmmadrasah_id']."'")->row();             
			    $notest = "<a href='".site_url("klokal/kartutes?id=".base64_encode($val['id'])."")."' target='_blank' >".$val['no_test']."</a>";	

				
				$no = $i++;
				$records["data"][] = array(
					$no,
				$notest,
					
					strtoupper($val['nama']),
					$val['gender'],
					$madrasah->npsn,
				
					
				    strtoupper($this->Di->get_kondisi(array("id"=>$val['tmmadrasah_id']),"tm_madrasah","nama")),
					$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama")
					
				
					
					
                    				
										
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
		
	public function hasiltes(){
		
		
		
		
		
									
          $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Hasil Tes  ";
		  $data['dt']              = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
		  $data['jenjang'] = base64_decode($this->input->get_post("jenjang",true));
	     if(!empty($ajax)){
					    
			 $this->load->view('hasiltes/page_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "hasiltes/page_peserta";
			 
			 $this->load->view('kdashboard/page_header',$data);
		 }
	
		
	}
	
	public function g_hasiltes(){
		error_reporting(0);
		$iTotalRecords = $this->M_lokal->m_hasilkabko(false)->num_rows();
		
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);
		
		$records = array();
		$records["data"] = array(); 

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;
		
		$datagrid = $this->M_lokal->m_hasilkabko(true)->result_array();
		$arjenis = array("1"=>"Madrasah","2"=>"Sekolah");
		$ketkabko = array("1"=>"Juara I","2"=>"Juara II","3"=>"Juara III","4"=>"Juara Harapan I","5"=>"Juara Harapan II","6"=>"Juara Harapan III");
		 $i= ($iDisplayStart +1);
		 foreach($datagrid as $val) {
											
											   
			$no = $i++;
			  $foto ="img/ksm.png";
			    if(!empty($val['foto'])){

			 	  $foto ="upload/{$val['foto']}";
			    }
			$nonya = "<a href='".site_url('klokal/sertifikatkota?q='.base64_encode($val['id']).'&nik='.base64_encode($val['nik_siswa']).'')."' target='_blank' >".$no."</a>";
			$piagam = "<a href='".site_url('klokal/sertifikatkota?q='.base64_encode($val['id']).'&nik='.base64_encode($val['nik_siswa']).'')."' target='_blank' class=' btn-sm btn btn-danger'><i class='fa fa-file'></i> Unduh Piagam</a>";
			   $notest = "<a href='https://ksm.kemenag.go.id/pengawas/ruangan/berita-acara?id=".$val['id']."' target='_blank' >".$val['no_test']."</a>";
			 $img ='<center><img src="https://ksm.kemenag.go.id/__statics/'.$foto.'" class="img-responsive img-thumbnail" style="width:80px; border-radius:50%"></center>';
			
											   
			  

			  $disabled="disabled";
			    if($val['ctt']==1){
					$disabled="";
				}

				$disabled="disabled";

				$checked="";
			    if($val['skrn']==1){
					$checked="checked";
				}
			 
			  $records["data"][] = array(
				  $nonya,
				  $piagam,
				  $img,
				  $notest,
				  $val['nama'],
				  $val['tgl_lahir'],
				  $this->Di->get_kondisi(array("id"=>$val['provinsi_madrasah']),"provinsi","nama"),
				  $this->Di->get_kondisi(array("id"=>$val['kota_madrasah']),"kota","nama"),				  		
				  $this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
				  (!empty($val['ketkabko'])) ? "<span class='badge badge-success'>".$val['ketkabko']."</span>" : "<span class='badge badge-danger'>Peserta </span>",
				  $val['bnr_pg'],
				  $val['bnr_isi'],
				  $val['jml_benar'],
				  $val['slh'],
				  $val['ksg'],
				  $val['nla'],
				  
				  $arjenis[$val['jenis']],
				  $this->Di->jenjangnama($val['jenjang']),
				  $val['username'],
				  $val['madrasah'],
				  $val['ctt'],
				
				  '<input type="checkbox" class="" '.$disabled.' '.$checked.' value="1" tmsiswa_id="'.$val['id'].'">'
			
				  
				  
				  
									  
						  
			  
				 

				);
			}
	  
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		
		echo json_encode($records);
  }


  public function sertifikatkota(){

	$tmsiswa_id = base64_decode($this->input->get("q")); 
	$nik       = base64_decode($this->input->get("nik")); 

   $datasiswa = $this->db->query("select d_p,nik_siswa,madrasah,provinsi,trkompetisi_id,id,nama,jenjang_madrasah,kota_madrasah from v_siswa_kabkota where nla IS NOT NULL AND id='".$tmsiswa_id."' ")->row();
	   
   if(count($datasiswa) >0){

	   $pdf = new Fpdi();

	   $this->load->library('ciqrcode'); //pemanggilan library QR CODE

	   $config['cacheable']	= true; 
	   $config['cachedir']		= './tmp/chache'; 
	   $config['errorlog']		= './tmp/'; 
	   $config['imagedir']		= './tmp/'; 
	   $config['quality']		= true; 
	   $config['size']			= '1024'; 
	   $config['black']		= array(224,255,255);
	   $config['white']		= array(70,130,180); 
	   $this->ciqrcode->initialize($config);

	   //$image_name=$datasiswa->id.'.png'; 
	   $image_name= $datasiswa->id.'.png'; 

	   $params['data'] = "https://ksm.kemenag.go.id/publik/validitas?q=".base64_encode($datasiswa->nik_siswa)."&s=".base64_encode($datasiswa->id).""; 
	   $params['level'] = 'H'; 
	   $params['size'] = 10;
	   $params['savename'] = FCPATH.$config['imagedir'].$image_name; 
	   $this->ciqrcode->generate($params);

	   $ketkabko = array("1"=>"Juara I","2"=>"Juara II","3"=>"Juara III","4"=>"Juara Harapan I","5"=>"Juara Harapan II","6"=>"Juara Harapan III");
	   $jenjangn = array("1"=>"Madrasah Ibtidaiyah/Sederajat ","2"=>"Madrasah Tsanawiyah/Sederajat","3"=>"Madrasah Aliyah/Sederajat");
	   $kompetisi = $this->Di->get_kondisi(array("id"=>$datasiswa->trkompetisi_id),"tr_kompetisi","nama");
	   $kota_madrasah = $this->Di->get_kondisi(array("id"=>$datasiswa->kota_madrasah),"kota","nama");
	   $wilayah = strtoupper($jenjangn[$datasiswa->jenjang_madrasah])." ".$kota_madrasah;


		 if($datasiswa->d_p <= 6){
   
		   $status =   $ketkabko[$datasiswa->d_p];
		  
		  
	   


		   $pdf->addPage('L');
		   
		   $pdf->setSourceFile('juara.pdf');
	   
		   $tplIdx = $pdf->importPage(1);
		   $pdf ->useTemplate($tplIdx, null, null, null, null,FALSE);
		   
		   $pdf->Ln(5);
		   $pdf->SetFont('Arial','B',26);
		   $pdf->SetTextColor(6, 0, 0);

		   $pdf->Cell(null,93,$datasiswa->nama,0,0,'C');

		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,$datasiswa->madrasah,0,0,'C');

		   $pdf->Ln(20);
		   $pdf->SetFont('Arial','B',26);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(271,93,$status,0,0,'C');


		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,"Bidang Studi ".$kompetisi,0,0,'C');

		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,"JENJANG ".$wilayah,0,0,'C');
		   
		   
		   //$pdf->Write(0, '');


		   $pdf->Image("tmp/".$image_name,260,170,30,30);


		   $pdf->Output('I', 'PIAGAM  '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf');

		   }else{
			   $status = " Peserta ";




			   $pdf->addPage('L');
		   
			   $pdf->setSourceFile('peserta.pdf');
		   
			   $tplIdx = $pdf->importPage(1);
			   $pdf ->useTemplate($tplIdx, null, null, null, null,FALSE);
			   
			   
			   $pdf->SetFont('Arial','B',26);
			   $pdf->SetTextColor(6, 0, 0);
   
			   $pdf->Cell(null,100,$datasiswa->nama,0,0,'C');
   
			   $pdf->Ln(15);
			   $pdf->SetFont('Arial','B',15);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(null,93,$datasiswa->madrasah,0,0,'C');
   
			   $pdf->Ln(20);
			   $pdf->SetFont('Arial','B',26);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(271,93,null,0,0,'C');
   
   
			   $pdf->Ln(10);
			   $pdf->SetFont('Arial','B',15);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(null,93,"Bidang Studi ".$kompetisi,0,0,'C');
   
			   $pdf->Ln(10);
			   $pdf->SetFont('Arial','B',15);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(null,93,"JENJANG ".$wilayah,0,0,'C');
			   
			   
			   //$pdf->Write(0, '');
   
   
			   $pdf->Image("tmp/".$image_name,260,170,30,30);
   
   
			   $pdf->Output('I', 'PIAGAM  '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf');


	   
		   }


   }else{

	   echo "404 Not Found";
   }
 }

	
 public function hasiltesprov(){
		
		
		
		
		
									
	$ajax = $this->input->get_post("ajax",true);	
	$data['title']  = "Hasil Tes  ";
	$data['dt']              = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
	$data['jenjang'] = base64_decode($this->input->get_post("jenjang",true));
   if(!empty($ajax)){
				  
	   $this->load->view('hasiltes/page_provinsi',$data);
   }else{
	   
	  
	   $data['konten'] = "hasiltes/page_provinsi";
	   
	   $this->load->view('kdashboard/page_header',$data);
   }

  
}

public function g_hasiltesprov(){
  error_reporting(0);
  $iTotalRecords = $this->M_lokal->m_hasilprov(false)->num_rows();
  
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $records = array();
  $records["data"] = array(); 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
  
  $datagrid = $this->M_lokal->m_hasilprov(true)->result_array();
  $arjenis = array("1"=>"Madrasah","2"=>"Sekolah");
  $ketkabko = array("1"=>"Juara I","2"=>"Juara II","3"=>"Juara III","4"=>"Juara Harapan I","5"=>"Juara Harapan II","6"=>"Juara Harapan III");
   $i= ($iDisplayStart +1);
   foreach($datagrid as $val) {
									  
										 
	  $no = $i++;
		$foto ="img/ksm.png";
		  if(!empty($val['foto'])){

			 $foto ="upload/{$val['foto']}";
		  }
	  $nonya = "<a href='".site_url('klokal/sertifikatprovinsi?q='.base64_encode($val['id']).'&nik='.base64_encode($val['nik_siswa']).'')."' target='_blank' >".$no."</a>";
	  $piagam = "<a href='".site_url('klokal/sertifikatprovinsi?q='.base64_encode($val['id']).'&nik='.base64_encode($val['nik_siswa']).'')."' target='_blank' class=' btn-sm btn btn-danger'><i class='fa fa-file'></i> Unduh Piagam</a>";
	 $notest = "<a href='https://ksm.kemenag.go.id/pengawas/ruangan/berita-acara?id=".$val['id']."' target='_blank' >".$val['no_test']."</a>";
	   $img ='<center><img src="https://ksm.kemenag.go.id/__statics/'.$foto.'" class="img-responsive img-thumbnail" style="width:80px; border-radius:50%"></center>';
	  
										 
		

		$disabled="disabled";
		  if($val['ctt']==1){
			  $disabled="";
		  }

		  $disabled="disabled";

		  $checked="";
		  if($val['skrn']==1){
			  $checked="checked";
		  }
	   
		$records["data"][] = array(
			$nonya,
			$piagam,
			$img,
			$notest,
			$val['nama'],
			$val['tgl_lahir'],
			$this->Di->get_kondisi(array("id"=>$val['provinsi_madrasah']),"provinsi","nama"),
			$this->Di->get_kondisi(array("id"=>$val['kota_madrasah']),"kota","nama"),				  		
			$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
			($val['peringkat_prov'] <= 6) ? "<span class='badge badge-success'>".$ketkabko[$val['peringkat_prov']]."</span>" : "<span class='badge badge-danger'>Peserta </span> ",
			$val['bnr_pg'],
			$val['bnr_isi'],
			$val['jml_benar'],
			$val['slh'],
			$val['ksg'],
			$val['nla'],
			
			$arjenis[$val['jenis']],
			$this->Di->jenjangnama($val['jenjang']),
			$val['username'],
			$val['madrasah'],
			$val['ctt'],
		  
			'<input type="checkbox" class="" '.$disabled.' '.$checked.' value="1" tmsiswa_id="'.$val['id'].'">'
	  
			
			
			
								
					
		
		   

		  );
	  }

  $records["draw"] = $sEcho;
  $records["recordsTotal"] = $iTotalRecords;
  $records["recordsFiltered"] = $iTotalRecords;
  
  echo json_encode($records);
}



public function sertifikatprovinsi(){

	$tmsiswa_id = base64_decode($this->input->get("q")); 
	$nik       = base64_decode($this->input->get("nik")); 

   $datasiswa = $this->db->query("select peringkat_prov,nik_siswa,madrasah,provinsi,trkompetisi_id,id,nama,jenjang_madrasah,provinsi_madrasah,tmmadrasah_id from v_siswa_provinsi where nla IS NOT NULL AND id='".$tmsiswa_id."'")->row();
	   
   if(count($datasiswa) >0){

	   $pdf = new Fpdi();

	   $this->load->library('ciqrcode'); //pemanggilan library QR CODE

	   $config['cacheable']	= true; 
	   $config['cachedir']		= './tmp/chache'; 
	   $config['errorlog']		= './tmp/'; 
	   $config['imagedir']		= './tmp/'; 
	   $config['quality']		= true; 
	   $config['size']			= '1024'; 
	   $config['black']		= array(224,255,255);
	   $config['white']		= array(70,130,180); 
	   $this->ciqrcode->initialize($config);

	   //$image_name=$datasiswa->id.'.png'; 
	   $image_name= $datasiswa->id.'prov.png'; 

	   $params['data'] = "https://ksm.kemenag.go.id/publik/validitasprovinsi?q=".base64_encode($datasiswa->tmmadrasah_id)."&s=".base64_encode($datasiswa->id).""; 
	   $params['level'] = 'H'; 
	   $params['size'] = 10;
	   $params['savename'] = FCPATH.$config['imagedir'].$image_name; 
	   $this->ciqrcode->generate($params);

	   $ketkabko = array("1"=>"Juara I","2"=>"Juara II","3"=>"Juara III","4"=>"Juara Harapan I","5"=>"Juara Harapan II","6"=>"Juara Harapan III");
	   $jenjangn = array("1"=>"Madrasah Ibtidaiyah/Sederajat ","2"=>"Madrasah Tsanawiyah/Sederajat","3"=>"Madrasah Aliyah/Sederajat");
	   $kompetisi = $this->Di->get_kondisi(array("id"=>$datasiswa->trkompetisi_id),"tr_kompetisi","nama");
	   $kota_madrasah = $this->Di->get_kondisi(array("id"=>$datasiswa->provinsi_madrasah),"provinsi","nama");
	   $wilayah = strtoupper($jenjangn[$datasiswa->jenjang_madrasah])." PROVINSI ".$kota_madrasah;


		 if($datasiswa->peringkat_prov <= 6){
   
		   $status =   $ketkabko[$datasiswa->peringkat_prov];
		  
		  
	   


		   $pdf->addPage('L');
		   
		   $pdf->setSourceFile('juaraprovinsi.pdf');
	   
		   $tplIdx = $pdf->importPage(1);
		   $pdf ->useTemplate($tplIdx, null, null, null, null,FALSE);
		   
		   
		   $pdf->SetFont('Arial','B',26);
		   $pdf->SetTextColor(6, 0, 0);

		   $pdf->Cell(null,93,$datasiswa->nama,0,0,'C');

		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,$datasiswa->madrasah,0,0,'C');

		   $pdf->Ln(20);
		   $pdf->SetFont('Arial','B',26);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(276,93,$status,0,0,'C');


		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,"Bidang Studi ".$kompetisi,0,0,'C');

		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,"JENJANG ".$wilayah,0,0,'C');
		   
		   
		   //$pdf->Write(0, '');


		   $pdf->Image("tmp/".$image_name,260,170,30,30);


		   $pdf->Output('I', 'PIAGAM  '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf');

		   }else{
			   $status = " Peserta ";




			   $pdf->addPage('L');
		   
			   $pdf->setSourceFile('pesertaprovinsi.pdf');
		   
			   $tplIdx = $pdf->importPage(1);
			   $pdf ->useTemplate($tplIdx, null, null, null, null,FALSE);
			   
			   
			   $pdf->SetFont('Arial','B',26);
			   $pdf->SetTextColor(6, 0, 0);
   
			   $pdf->Cell(null,100,$datasiswa->nama,0,0,'C');
   
			   $pdf->Ln(15);
			   $pdf->SetFont('Arial','B',15);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(null,93,$datasiswa->madrasah,0,0,'C');
   
			   $pdf->Ln(20);
			   $pdf->SetFont('Arial','B',26);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(271,93,null,0,0,'C');
   
   
			   $pdf->Ln(10);
			   $pdf->SetFont('Arial','B',15);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(null,93,"Bidang Studi ".$kompetisi,0,0,'C');
   
			   $pdf->Ln(10);
			   $pdf->SetFont('Arial','B',15);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(null,93,"JENJANG ".$wilayah,0,0,'C');
			   
			   
			   //$pdf->Write(0, '');
   
   
			   $pdf->Image("tmp/".$image_name,260,170,30,30);
   
   
			   $pdf->Output('I', 'PIAGAM  '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf');


	   
		   }


   }else{

	   echo "404 Not Found";
   }
 }
	
	public function sertifikat(){
		
		
		
		
		
									
          $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Sertifikat  ";
		  $data['dt']              = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
		  $data['jenjang'] = base64_decode($this->input->get_post("jenjang",true));
	     if(!empty($ajax)){
					    
			 $this->load->view('sertifikat/page_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "sertifikat/page_peserta";
			 
			 $this->load->view('kdashboard/page_header',$data);
		 }
	
		
	}
	
	public function g_sertifikat(){
		
		  $iTotalRecords = $this->M_lokal->m_pesertasertifikat(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->M_lokal->m_pesertasertifikat(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
				                            
			    $notest = $val['no_test'];	
				
				$no = $i++;
				$statusarray= array("1"=>"Juara 1","2"=>"Juara II","3"=>"Juara III","4"=>"Harapan I","5"=>"Harapan II","6"=>"Harapan III");
				    if($no < 7){
						 if($no <4){
							 $status = "<span class='btn btn-success btn-sm'>".$statusarray[$no]."</span>";
						 }else{
							 
							$status = "<span class='btn btn-warning btn-sm' style='color:white'>".$statusarray[$no]."</span>";
						 }
						 
						 
						
					}else{
						
						$status = "<span class='btn btn-danger btn-sm' style='color:white'>Peserta</span>";
					}
					
					
				$records["data"][] = array(
					$no,
					'<a class="btn btn-danger " href="'.site_url("klokal/cetaksertifikat?tmsiswa_id=".base64_encode($val['id'])."&urutan=".base64_encode($no)."&display=".base64_encode($iDisplayStart)."").'"> <i class="fa fa-file-word-o"></i> PIAGAM </a>',
					//''.
					$notest,
					($val['nama']),
					$status,
					$this->Di->formattanggalstring($val['tgl_lahir']),
                    $this->Di->get_kondisi(array("id"=>$val['tmmadrasah_id']),"tm_madrasah","nama"),					
					$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
					(!empty($val['benar'])) ? $val['benar'] : 0,
					(!empty($val['salah'])) ? $val['salah'] : 0,
					(!empty($val['kosong'])) ? $val['kosong'] : 0,	
					(!empty($val['nilai'])) ? $val['nilai'] : 0,
					 "Jml Input = ".($val['benar']+$val['salah']+$val['kosong'])." <br> Jml Soal = ".$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","soal")
				
					
				
				
					
					
                    				
										
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	
	public function cetaksertifikat(){
		header ("Content-type: text/html; charset=utf-8");
		
		$this->load->library('PHPWord');
		$this->load->library('ciqrcode');
        $this->load->helper('download');
		
		$PHPWord = new PHPWord();	
		$document 		   = $PHPWord->loadTemplate('piagam.docx');
		
		$tmsiswa_id	  			   = base64_decode($this->input->get_post("tmsiswa_id",TRUE));
		$urutan  	  			   = base64_decode($this->input->get_post("urutan",TRUE));
		$display  	  			   = base64_decode($this->input->get_post("display",TRUE));

		
		$datasiswa = $this->db->query("select tmmadrasah_id,kota,trkompetisi_id,id,nama from tm_siswa where id='".$tmsiswa_id."'")->row();
		
		  if(count($datasiswa) >0){
			  
			  
			  
			  
			  
		$sekolah   = $this->Di->get_kondisi(array("id"=>$datasiswa->tmmadrasah_id),"tm_madrasah","nama");
		$kabupaten = $this->Di->get_kondisi(array("id"=>$datasiswa->kota),"kota","nama");
		$kompetisi = $this->Di->get_kondisi(array("id"=>$datasiswa->trkompetisi_id),"tr_kompetisi","nama");
		$kepala = $this->Di->get_kondisi(array("id"=>$datasiswa->kota),"kota","kepala");
		$jabatan = $this->Di->get_kondisi(array("id"=>$datasiswa->kota),"kota","ketkepala");
		   
		   
		   
		   $this->load->library('ciqrcode'); //pemanggilan library QR CODE

		$config['cacheable']	= true; 
		$config['cachedir']		= './piagam/chache'; 
		$config['errorlog']		= './piagam/'; 
		$config['imagedir']		= './piagam/'; 
		$config['quality']		= true; 
		$config['size']			= '1024'; 
		$config['black']		= array(224,255,255);
		$config['white']		= array(70,130,180); 
		$this->ciqrcode->initialize($config);

		$image_name=$datasiswa->id.'.png'; 

		$params['data'] = "http://ksm.kemenag.go.id/publik/piagam?token=".base64_encode($datasiswa->id).""; 
		$params['level'] = 'H'; 
		$params['size'] = 10;
		$params['savename'] = FCPATH.$config['imagedir'].$image_name; 
		$this->ciqrcode->generate($params);
		
		
		
		   
		
		  if($urutan < 7){
			  if($display==0){
			  $ura = array("1"=>"JUARA I","2"=>"JUARA II","3"=>"JUARA III","4"=>"HARAPAN I","5"=>"HARAPAN II","6"=>"HARAPAN III");
			  
			  $urutan = $ura[$urutan];
			  }else{
				  
				 $urutan = "PESERTA ";  
			  }
		  }else{
			  $urutan = "PESERTA "; 
		  }
		
		$document->setValue('{NAMAPESERTA}', strtoupper($datasiswa->nama));
		$document->setValue('{NAMASEKOLAH}', strtoupper($sekolah));
		$document->setValue('{STATUSPIAGAM}', $urutan);
		
		$document->setValue('{KOMPETISI}', strtoupper($kompetisi));
		$document->setValue('{kabupaten2}', ucwords(strtolower($kabupaten)));
		$document->setValue('{kepala}', (strtoupper($kepala)));
		$document->setValue('{jabatan}', (($jabatan)));
		$document->setValue('{kabupaten}', $kabupaten);
		$document->setValue('{tgl}', str_replace(array("Kabupaten","Kota"),"",ucwords(strtolower($kabupaten))).", 20 Juli 2019");
		
	    $brc      = FCPATH."/piagam/".$image_name;
	    $document->setImageValue('image4.png', $brc);
		$namafile = str_replace(array(" ","-","'",",","."),"",$datasiswa->nama).'.docx';
		$tmp_file = 'piagam/'.$namafile.'';
		$document->save($tmp_file);
		 
    
       // $pth    =   file_get_contents($_SERVER["DOCUMENT_ROOT"]."/piagam/".$namafile."");
	
		//unlink(FCPATH."piagam/".$namafile);
		//force_download($namafile, $pth); 
		
		 force_download('piagam/'.$namafile.'',NULL); 
		
		  }
		
	}
	
	
		public function disposisi(){
		
		
		
		
		
									
          $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Diposisi Peserta  ";
		  $data['dt']              = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
		  $data['jenjang'] = base64_decode($this->input->get_post("jenjang",true));
	     if(!empty($ajax)){
					    
			 $this->load->view('disposisi/page_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "disposisi/page_peserta";
			 
			 $this->load->view('kdashboard/page_header',$data);
		 }
	
		
	}
	
	public function g_disposisi(){
		
		  $iTotalRecords = $this->M_lokal->m_pesertasertifikat(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->M_lokal->m_pesertasertifikat(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
				                            
			    
				 $no = $i++;
				$statusarray= array("1"=>"Juara 1","2"=>"Juara II","3"=>"Juara III","4"=>"Harapan I","5"=>"Harapan II","6"=>"Harapan III");
				    if($no < 7){
						 if($no <4){
							 $status = "<span class='btn btn-success btn-sm'>".$statusarray[$no]."</span>";
						 }else{
							 
							$status = "<span class='btn btn-warning btn-sm' style='color:white'>".$statusarray[$no]."</span>";
						 }
						 
						 $button ='<button class="btn btn-info   btn-sm disposisi" data-toggle="modal" data-target="#myModal"   datanya="'.$val['id'].'" status="'.$statusarray[$no].'"  style"cursor:pointer"> <span class="fa fa-plane"></span> Disposisi  </button>';
				   
						
					}else{
						
						$button ='<button class="btn btn-default   btn-sm " disabled  > Tidak Tersedia </button>';
				   
						$status = "<span class='btn btn-danger btn-sm' style='color:white'>Peserta</span>";
					}
					
					if($val['status2']==1){
						
						$button ='<button class="btn btn-danger   btn-sm " disabled  > Terdisposisi </button>';
				   
					}
				
				$records["data"][] = array(
					$no,
					
						$button,
					$val['no_test'],
					($val['nama']),
					 $status,
					$this->Di->get_kondisi(array("id"=>$val['tmmadrasah_id']),"tm_madrasah","nama"),
                    $this->Di->formattanggalstring($val['tgl_lahir']),			
					$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
					(!empty($val['benar'])) ? $val['benar'] : 0,
					(!empty($val['salah'])) ? $val['salah'] : 0,
					(!empty($val['kosong'])) ? $val['kosong'] : 0,	
					(!empty($val['nilai'])) ? $val['nilai'] : 0,
					 "Jml Input = ".($val['benar']+$val['salah']+$val['kosong'])." <br> Jml Soal = ".$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","soal")
				
					
				
					
					
                    				
										
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	public function formdisposisikan(){
		
		$id = $_POST['id'];
		$status = $_POST['status'];
		$data = $this->db->query("select id,nama,trkompetisi_id from tm_siswa where id='".$id."' ")->row();
		?>
		  
		  
		     <div class="row">
							
									<div class="col-md-12">
										
										<div class="alert alert-info">
										 Anda akan mendisposisikan Peserta Atas Nama <b style="color:yellow"><i> <?php echo $data->nama; ?> ( <?php echo $status; ?> <?php echo $this->Di->get_kondisi(array("id"=>$data->trkompetisi_id),"tr_kompetisi","nama"); ?> ) </i></b>
										 Untuk mengikuti KSM Tingkat Provinsi, Berikan Keterangan dibawah ini untuk dibaca oleh Komite Provinsi dan Komite Pusat, Alasan Anda mendisposisikan peserta ini </br>
										  <input type="hidden" id="id_disposisi" value="<?php echo $data->id; ?>">
										  <input type="hidden" id="statuskabko" value="<?php echo $status; ?>">

										</div>
										
										
										<div class="form-group">
											<label for="name">Keterangan  </label>
											<textarea class="form-control" id="ketkabko" cols="50" rows="3" placeholder="Contoh : Peserta ini adalah Juara 1 di Kota kami atau Juara 1 - 3 dari Madrasah yang sama sehingga kami mendisposisi Peserta ini "></textarea>
										</div>
										
										<div class="form-group" id="aksian">
										  <center>
										  <button class="btn btn-info   btn-sm" id="hayudisposisikan">  <span class="fa fa-plane"></span> Lakukan Disposisi  </button>
									      </center>
									   </div>
									
									</div>
									
									
			</div>
		
		
		
		
		
		
		<?php 
		
	}
	
	public function disposisikuy(){
		
		$id_disposisi = $_POST['id_disposisi'];
		$statuskabko  = $_POST['statuskabko'];
		$ketkabko     = $_POST['ketkabko'];
		$datasiswa    = $this->db->query("select trkompetisi_id,tmmadrasah_id,benar,salah,kosong from tm_siswa where id='".$id_disposisi."'")->row();
		
		$kompetisi    = $datasiswa->trkompetisi_id;
		$tmmadrasah_id= $datasiswa->tmmadrasah_id;
		 $jml_input    = ($datasiswa->benar+$datasiswa->salah+$datasiswa->kosong); 
		 $jml_soal     = $this->Di->get_kondisi(array("id"=>$kompetisi),"tr_kompetisi","soal"); 
		  //echo "Belum bisa disposisi, Tunggu 15 Menit"; exit();
		  if($jml_input == $jml_soal){
		   $cek  = $this->db->query("select count(id) as jml from tm_siswa where trkompetisi_id='".$kompetisi."' and tmmadrasah_id='".$tmmadrasah_id."' and status2='1'")->row();
		   $cek2 = $this->db->query("select count(id) as jml from tm_siswa where trkompetisi_id='".$kompetisi."' and status2='1' and tmmadrasah_id in (select id from tm_madrasah where kota='".$_SESSION['tmkota_id']."')")->row();
		     
			 // echo $cek2->jml; exit();
			  if($cek->jml > 1){
				  echo "Disposisi Tidak dapat dilakukan, dikarenakan sudah ada 2 perwakilan di Bidang Studi dari Lembaga ini, Silahkan Disposisi Peserta lain  ";
				  
				  
			  }else{
				  
				  if($cek2->jml > 2){
					  
					 echo "Disposisi Tidak dapat dilakukan, Anda Maksimal hanya dapat mengirimkan 3 Peserta Per Bidang Studi  ";
				 
				  }else{
				   $this->db->set("status2",1);
				   $this->db->set("statuskabko",$statuskabko);
				   $this->db->set("ketkabko",$ketkabko);
				   $this->db->where("id",$id_disposisi);
				   $this->db->update("tm_siswa");
				  
				  
				   echo "Proses Disposisi Berhasil, Peserta sudah dikirim ke Komite Provinsi untuk mengikuti KSM tingkat Provinsi , Anda dapat melihat data Peserta Tk Provinsi pada menu Peserta Tingkat Provinsi";
				  
				  }
			  }
		  }else{
			  
			   echo "Disposisi Tidak dapat dilakukan, dikarenakan Jumlah Nilai yang Anda input tidak sesuai dengan jumlah soal, silahkan perbaiki dulu jumlah nilai pada menu Hasil Tes Kabko ";
				  
		  }
	}
	public function peserta_provinsi(){
		
		
		
		
		
									
          $ajax = $this->input->get_post("ajax",true);	
		  $data['jenjang'] = base64_decode($this->input->get_post("jenjang",true));	
		  $data['title']  = "Peserta Tingkat Provinsi  ";
		  $data['dt']              = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
		     if(!empty($ajax)){
					    
			 $this->load->view('peserta/peserta_provinsi',$data);
		 }else{
			 
			
		     $data['konten'] = "peserta/peserta_provinsi";
			 
			 $this->load->view('kdashboard/page_header',$data);
		 }
	
		
	}
	
	public function g_pesertaprovinsi(){
		 error_reporting(0);
		  $iTotalRecords = $this->M_lokal->m_peserta_provinsi(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 
		  $arjenis = array("1"=>"Madrasah","2"=>"Sekolah");
		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  $cbt = $this->load->database("cbt",true);
		  $datagrid = $this->M_lokal->m_peserta_provinsi(true)->result_array();
		   $arje = array("1"=>"MI/SD","2"=>"MTS/SMP","3"=>"MA/SMA");
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
				           
			$no = $i++;
			$foto ="img/ksm.png";
			  if(!empty($val['foto'])){

				 $foto ="upload/{$val['foto']}";
			  }
	     //  $notest = "<a href='https://ksm.kemenag.go.id/pengawas/ruangan/berita-acara?id=".$val['id']."' target='_blank' >".$val['no_test']."</a>";
		   $img ='<center><img src="https://ksm.kemenag.go.id/__statics/'.$foto.'" class="img-responsive img-thumbnail" style="width:80px; border-radius:50%"></center>';
		  
											 
		   $status = $cbt->query("select sta  FROM t_pkujps where idPs ='{$val['id']}' and strt is not null")->row();
			   								
		   if($status->sta==1){
			   
			   $statusnya ='<i class="badge badge-danger"> Sedang Mengerjakan </i>';
			   
			}else if($status->sta==2){
				
				$statusnya ='<i class="badge badge-success"> Selesai Mengerjakan </i>';	
			}else{

				$statusnya ='<i class="badge badge-primary"> Belum Mengerjakan </i>';
			}


			$disabled="disabled";
			  if($val['ctt']==1){
				  $disabled="";
			  }

			  $disabled="disabled";

			  $checked="";
			  if($val['skrn']==1){
				  $checked="checked";
			  }
		   
			$records["data"][] = array(
				$no,
				$img,
				$val['no_test2'],
				$val['nama'],
			
						  		
				$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
				 $statusnya,
				
				$arjenis[$val['jenis']],
				$this->Di->jenjangnama($val['jenjang']),
				$val['username'],
				$val['madrasah'],
				$val['ctt']
			  
				
					
					
                    				
										
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	public function peserta_nasional(){
		
		
		
		
		
									
		$ajax = $this->input->get_post("ajax",true);	
		$data['jenjang'] = base64_decode($this->input->get_post("jenjang",true));	
		$data['title']  = "Peserta Tingkat Provinsi  ";
		$data['dt']              = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
		   if(!empty($ajax)){
					  
		   $this->load->view('peserta/peserta_nasional',$data);
	   }else{
		   
		  
		   $data['konten'] = "peserta/peserta_nasional";
		   
		   $this->load->view('kdashboard/page_header',$data);
	   }
  
	  
  }
  
  public function g_pesertanasional(){
	   error_reporting(0);
		$iTotalRecords = $this->M_lokal->m_peserta_nasional(false)->num_rows();
		
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);
		
		$records = array();
		$records["data"] = array(); 
		$arjenis = array("1"=>"Madrasah","2"=>"Sekolah");
		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;
		$cbt = $this->load->database("cbt",true);
		$datagrid = $this->M_lokal->m_peserta_nasional(true)->result_array();
		 $arje = array("1"=>"MI/SD","2"=>"MTS/SMP","3"=>"MA/SMA");
		 $i= ($iDisplayStart +1);
		 $ketkabko = array("1"=>"Juara I","2"=>"Juara II","3"=>"Juara III","4"=>"Juara Harapan I","5"=>"Juara Harapan II","6"=>"Juara Harapan III");
		 foreach($datagrid as $val) {
						 
		  $no = $i++;
		  $foto ="img/ksm.png";
			if(!empty($val['foto'])){

			   $foto ="upload/{$val['foto']}";
			}
	   //  $notest = "<a href='https://ksm.kemenag.go.id/pengawas/ruangan/berita-acara?id=".$val['id']."' target='_blank' >".$val['no_test']."</a>";
		 $img ='<center><img src="https://ksm.kemenag.go.id/__statics/'.$foto.'" class="img-responsive img-thumbnail" style="width:80px; border-radius:50%"></center>';
		
		

	
		 
		  $records["data"][] = array(
			  $no,
			  $img,
			 // $val['no_test3'],
			  "9 Okt 2021",
			  $val['nama'],	  
								
			  $this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
			  ($val['peringkat_prov'] <= 6) ? "<span class='badge badge-success'>".$ketkabko[$val['peringkat_prov']]."</span>" : "<span class='badge badge-danger'>Peserta </span> ",
			 
			  $arjenis[$val['jenis']],
			  $this->Di->jenjangnama($val['jenjang']),
			  $val['username'],
			  $val['madrasah']
			
			  
				  
				  
								  
									  
						  
			  
				 

				);
			}
	  
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		
		echo json_encode($records);
  }


	public function kartutes(){
	    
	    	 		 
				$ret = '';
				
				$download_pdf=TRUE;
				$link_download = ($download_pdf == TRUE)?'':anchor(base_url().'lokal/printkartutes/true', 'Download Pdf');
				
				
		 
					$data['breadcumb']  ="Kartu Tes";
				//	$data['m']   = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
					$data['data']      = $this->Di->get_where("v_siswa",array("id"=>($_GET['id'])));
					
				$pdf_filename = 'Kartutes'.str_replace(array("'",",","-"),"",$data['data']->nama).'.pdf';	 
				$data_header = array('title' => 'Kartu Tes',);
			
				
				$user_info = $this->load->view('peserta/kartutes', $data, true);
			
				$output = $user_info;
				
				
				if($download_pdf == TRUE)
				generate_pdf($output, $pdf_filename,TRUE);
				else
				echo $output;
	    
	}
	
	public function disposisikan(){
		
		$status = $this->input->get_post("status");
		$tmsiswa_id = $this->input->get_post("tmsiswa_id");
		
		$this->db->set("d_p",$status);
		$this->db->where("id",$tmsiswa_id);
		$this->db->update("tm_siswa");
		echo "sukses";
		
		
	}
	
	
		public function kartu(){
		
		
		
		 if(!$this->session->userdata("tmkota_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/loginkomite")."'>disini </a></center>";
               exit();
		  }
	   
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Kartu Peserta KSM ";
		 $data['dt']              = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('kartu/page_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "kartu/page_peserta";
			 
			 $this->load->view('kdashboard/page_header',$data);
		 }
	
		
	}
	
	public function perbaharui_p(){
		
		
		
		 if(!$this->session->userdata("tmkota_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/loginkomite")."'>disini </a></center>";
               exit();
		  }
	   
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Perbaharui Profil Anda";
		  $data['dt']              = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('page_profile',$data);
		 }else{
			 
			
		     $data['konten'] = "page_profile";
			 
			 $this->load->view('kdashboard/page_header',$data);
		 }
	
		
	}
	
	
	public function perbaharui_akun(){
		  $this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
        
        $this->form_validation->set_message('min_length', '{field} Minimal 6 Karakter .');
        $this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
        $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , mungkin anda sudah pernah melakukan pendaftaran, silahkan klik menu login .');
				 
		 
			
				$config = array(
					    array('field' => 'password', 'label' => 'Password  ', 'rules' => 'trim|required|min_length[6]'),
				        array('field' => 'passwordc', 'label' => 'Ulangi Password ', 'rules' => 'trim|required|matches[password]'),
			
				 
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			
			
		    $this->db->set("password",(($_POST['password'])));
		    $this->db->set("alias",$_POST['password']);
		    $this->db->where("id",$_SESSION['tmkota_id']);
		    $this->db->update("kota");
			echo "sukses";
		}else{
			
			 header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
			
		}
		
	
	}
	
	public function soal(){
		
		
	
	   
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Download Soal Tes";
		  $data['dt']              = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('soal/page_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "soal/page_peserta";
			 
			 $this->load->view('kdashboard/page_header',$data);
		 }
	
		
	}
	

	

public function g_pesertates2(){
		
		  $iTotalRecords = $this->M_lokal->m_pesertates2(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->M_lokal->m_pesertates2(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
				                                if($val['foto']==""){
													$file = "not.png";
												 }else{
													 $file = $val['foto'];
												 }
												 
				$status = ($val['status']==1) ? "checked" :"";
												 
				$no = $i++;
				$records["data"][] = array(
					$no,
				    $this->Di->get_kondisi(array("id"=>$val['tmmadrasah_id']),"tm_madrasah","nama"),
					'<img src="'.base_url().'__statics/img/peserta/'.$file.'" class="img-circle img-responsive" style="height:50px;width:50px">',
					
					$val['nama'],
					
					$val['gender'],
					$val['tempat'].",".$this->Di->formattanggalstring($val['tgl_lahir']),
					
					$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama")
					
					
                    				
										
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	
	public function pencetakan(){
		
		$jenjang   = $this->input->get_post("jenjang",true);
		$kompetisi = $this->input->get_post("kompetisi",true);
		$jenis     = $this->input->get_post("jenis",true);
		  
		  if($kompetisi==""){
			
			echo "<h4><center>Mohon untuk memilih kompetisi sebelum menampilkan jumlah ruang , silahkan klik tombol close kemudian pilih kompetisi </h4>"; 			
			  
		  }else{
		  $j = $this->db->query("select count(id) as js from tm_siswa where status='1' and trkompetisi_id='".$kompetisi."' and jenjang ='".$jenjang."' and kota ='".$_SESSION['tmkota_id']."' ")->row();
		  
		  $kota  = $this->db->query("select ruang_mi,ruang_mts,ruang_ma from kota where id='".$_SESSION['tmkota_id']."'")->row();
		  if($jenjang==1){
			  $kolom = "ruang_mi";
		  }else if($jenjang==2){
			  $kolom = "ruang_mts";
		  }else if($jenjang==3){
			  $kolom = "ruang_ma";
			  
		  }
		  $ruang  = ($kota->$kolom ==0) ? 20 : $kota->$kolom;
		  $arrje  = array("1"=>"MI/SD ","2"=>"MTS/SMP","3"=>"MA/SMA");
		  $jumlah =  ceil($j->js/$ruang);
		  ?><div class="alert alert-info"> 
		   <b> Jumlah Peserta peruangan untuk jenjang <i> <?php echo $arrje[$jenjang]; ?></i> di atur <i><?php echo $ruang; ?></i> Peruangan,<br>
		     Untuk mengatur jumlah peserta peruangan, silahkan tentukan pada menu <i> Lokasi Tes </i> <br>
		  
		        Silahkan klik pada setiap tombol dibawah ini untuk melakukan download <?php echo strtoupper($jenis); ?> Peserta Peruang Tes </div> 
				<?php 
		     for($a=1;$a<=$jumlah;$a++){
				 
				  ?>
				 
				  <a href="<?php echo base_url(); ?>klokal/<?php echo $jenis; ?>?jenjang=<?php echo $jenjang; ?>&kompetisi=<?php echo $kompetisi; ?>&page=<?php echo $a; ?>" target="_blank" class="btn btn-primary btn-sm"><span class="fa fa-institution"> RUANG  <?php echo ($a<10) ? "0".$a: $a; ?></span></a>
				 
				 <?php 
				 
				 
			 }
		  
		  
		  
		  
		  }
		    
		
	}
	public function daftarhadir(){
	    
	    	 
				$ret = '';
				
				$download_pdf=TRUE;
				$link_download = ($download_pdf == TRUE)?'':anchor(base_url().'klokal/daftarhadir/true', 'Download Pdf');
				
				
		           $kompetisi = $_GET['kompetisi'];
		   		   $jenjang   = $_GET['jenjang'];
		   		   $page      = $_GET['page'];
				   $awal =0;
				   
				    $kota  = $this->db->query("select ruang_mi,ruang_mts,ruang_ma from kota where id='".$_SESSION['tmkota_id']."'")->row();
					  if($jenjang==1){
						  $kolom = "ruang_mi";
					  }else if($jenjang==2){
						  $kolom = "ruang_mts";
					  }else if($jenjang==3){
						  $kolom = "ruang_ma";
						  
					  }
					  $ruang  = ($kota->$kolom ==0) ? 20 : $kota->$kolom;
					  
		  
				     if($page !=1){
						  $page = $page -1;
						  $awal = ($page * $ruang);
					  }
		   		   
					$data['breadcumb']   = "Kartu Tes";
					$data['awal']   = $awal;
					
		
				    
					$data['ssiswa']      = $this->Di->query("select * from tm_siswa where trkompetisi_id='".$kompetisi."' and status='1' and  jenjang='".$jenjang."' and kota='".$_SESSION['tmkota_id']."' order by nama asc limit $awal,$ruang")->result();
					
				$pdf_filename = 'Daftar_peserta_ruang_'.$_GET['page'].'.pdf';	 
				$data_header = array('title' => 'Daftar Peserta',);
			
				
				$user_info = $this->load->view('kartu/daftarpeserta', $data, true);
			
				$output = $user_info;
				
				
				if($download_pdf == TRUE)
				generate_pdf($output, $pdf_filename,TRUE);
				else
				echo $output;
			
	    
	}
	
	public function absensipeserta(){
	    
	    	 
				$ret = '';
				
				$download_pdf=TRUE;
				$link_download = ($download_pdf == TRUE)?'':anchor(base_url().'klokal/absensipeserta/true', 'Download Pdf');
				
				
		            $kompetisi = $_GET['kompetisi'];
		   		   $jenjang   = $_GET['jenjang'];
		   		   $page      = $_GET['page'];
				   $awal =0;
				    
					$kota  = $this->db->query("select ruang_mi,ruang_mts,ruang_ma from kota where id='".$_SESSION['tmkota_id']."'")->row();
					  if($jenjang==1){
						  $kolom = "ruang_mi";
					  }else if($jenjang==2){
						  $kolom = "ruang_mts";
					  }else if($jenjang==3){
						  $kolom = "ruang_ma";
						  
					  }
					 $ruang  = ($kota->$kolom ==0) ? 20 : $kota->$kolom;
					  
		  
				     if($page !=1){
						  $page = $page -1;
						  $awal = ($page * $ruang);
					  }
					  
					$data['breadcumb']   = "Kartu Tes";
					$data['awal']   = $awal;
				    
					$data['ssiswa']      = $this->Di->query("select * from tm_siswa where trkompetisi_id='".$kompetisi."' and status='1' and  jenjang='".$jenjang."' and kota='".$_SESSION['tmkota_id']."' order by nama asc limit $awal,$ruang")->result();
					
				$pdf_filename = 'Absensi_peserta_ruang_'.$_GET['page'].'.pdf';	
				$data_header = array('title' => 'Absensi Peserta ',);
			
				
				$user_info = $this->load->view('kartu/absensipeserta', $data, true);
			
				$output = $user_info;
				
				
				if($download_pdf == TRUE)
				generate_pdf($output, $pdf_filename,TRUE);
				else
				echo $output;
			
	    
	}
	
	public function albumpeserta(){
	    
	    	 
				$ret = '';
				
				$download_pdf=TRUE;
				$link_download = ($download_pdf == TRUE)?'':anchor(base_url().'klokal/absensipeserta/true', 'Download Pdf');
				
				
		            $kompetisi = $_GET['kompetisi'];
		   		   $jenjang   = $_GET['jenjang'];
		   		   $page      = $_GET['page'];
				   $awal =0;

                    $kota  = $this->db->query("select ruang_mi,ruang_mts,ruang_ma from kota where id='".$_SESSION['tmkota_id']."'")->row();
					  if($jenjang==1){
						  $kolom = "ruang_mi";
					  }else if($jenjang==2){
						  $kolom = "ruang_mts";
					  }else if($jenjang==3){
						  $kolom = "ruang_ma";
						  
					  }
					  $ruang  = ($kota->$kolom ==0) ? 20 : $kota->$kolom;
					  
		  
				     if($page !=1){
						  $page = $page -1;
						  $awal = ($page * $ruang);
					  }
		   		   
					$data['breadcumb']   = "Kartu Tes";
					$data['awal']   = $awal;
				    
					$data['ssiswa']      = $this->Di->query("select * from tm_siswa where trkompetisi_id='".$kompetisi."' and status='1' and  jenjang='".$jenjang."' and kota='".$_SESSION['tmkota_id']."' order by nama asc limit $awal,$ruang")->result();
					
				$pdf_filename = 'album_peserta_ruang_'.$_GET['page'].'.pdf';	
				$data_header = array('title' => 'Absensi Peserta ',);
			
				
				$user_info = $this->load->view('kartu/album', $data, true);
			
				$output = $user_info;
				
				
				if($download_pdf == TRUE)
				generate_pdf($output, $pdf_filename,TRUE);
				else
				echo $output;
			
	    
	}
	
	
	
	public function kartumeja(){
	    
	    	 
				$ret = '';
				
				$download_pdf=TRUE;
				$link_download = ($download_pdf == TRUE)?'':anchor(base_url().'klokal/kartumeja/true', 'Download Pdf');
				
				
		           $kompetisi = $_GET['kompetisi'];
		   		   $jenjang   = $_GET['jenjang'];
		   		   $page      = $_GET['page'];
				   $awal =0;
				    $kota  = $this->db->query("select ruang_mi,ruang_mts,ruang_ma from kota where id='".$_SESSION['tmkota_id']."'")->row();
					  if($jenjang==1){
						  $kolom = "ruang_mi";
					  }else if($jenjang==2){
						  $kolom = "ruang_mts";
					  }else if($jenjang==3){
						  $kolom = "ruang_ma";
						  
					  }
					  $ruang  = ($kota->$kolom ==0) ? 20 : $kota->$kolom;
					  
		  
				     if($page !=1){
						  $page = $page -1;
						  $awal = ($page * $ruang);
					  }
		   		   
					$data['breadcumb']   = "Kartu Tes";
					$data['awal']   = $awal;
				    
					$data['ssiswa']      = $this->Di->query("select * from tm_siswa where trkompetisi_id='".$kompetisi."' and status='1' and  jenjang='".$jenjang."' and kota='".$_SESSION['tmkota_id']."' order by nama asc limit $awal,$ruang")->result();
					
				$pdf_filename = 'Kartumeja_peserta_ruang_'.$_GET['page'].'.pdf';		 
				$data_header = array('title' => 'Absensi Peserta ',);
			
				
				$user_info = $this->load->view('kartu/kartumeja', $data, true);
			
				$output = $user_info;
				
				
				if($download_pdf == TRUE)
				generate_pdf($output, $pdf_filename,TRUE);
				else
				echo $output;
			
	    
	}
	
	
	public function lembar_download(){
		
		$this->load->helper('download');
   
        force_download('piagam/LJU.zip',NULL);   
		
	}
	
	public function soal_download(){
		
		$this->load->helper('download');
       $this->db->query("update kota set download ='1' where id='".$_SESSION['tmkota_id']."'");
        force_download('piagam/NASKAH_SOAL.zip',NULL);   
		
	}
	
	
		public function jawaban_download(){
		
		$this->load->helper('download');
       $this->db->query("update kota set jawaban ='1' where id='".$_SESSION['tmkota_id']."'");
        force_download('piagam/jawaban.zip',NULL);   
		
	}
	
	
		public function paktanyaapa(){
		
		
		
		
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']           = "Upload Pakta  ";
		  $data['dt']              = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('soal/pakta',$data);
		 }else{
			 
			
		     $data['konten'] = "soal/pakta";
			 
			 $this->load->view('kdashboard/page_header',$data);
		 }
	
		
	}
	
	public function save_pakta(){
		
		//error_reporting(0);
		$trpersyaratan_id    = $this->input->get_post("trpersyaratan_id");
		$namapersyaratan     = $this->input->get_post("namapersyaratan");
	
							
		$config['upload_path'] = './__statics/pakta';
		$folder   = '__statics/pakta';
	$config['allowed_types'] = "jpg|png|gif|jpeg"; 	
	    $config['overwrite'] = true; 	
		$filenamepecah			 = explode(".",$_FILES['file']['name']);
		$imagenew				 = "Fakta_".$_SESSION['tmkota_id'].".".pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
		
		//unlink(FCPATH.$folder."/".$imagenew);
		
		$config['file_name'] = $imagenew;
		
		$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload('file'))
				{
					 header('Content-Type: application/json');
				     echo json_encode(array('error' => true, 'message' => $this->upload->display_errors()));
							
					
				
				}
				else{
					
											
												  	
												  
					        
							
							    $this->db->set("fakta",$imagenew);	
								$this->db->where("id",$_SESSION['tmkota_id']);	
								$this->db->update("kota");
							
							
					
						
						
					
							 header('Content-Type: application/json');
						    echo json_encode(array('success' => true, 'message' => "Upload Sukses"));
								
				
				}
	
	}
	
	
	
	public function hapus_pakta(){
		
	
	    $this->db->set("fakta","");	
	    $this->db->where("id",$_SESSION['tmkota_id']);	
		$this->db->update("kota");
		echo "sukses";
		
		
	}
	
	 public function import(){
		
		
		
		
		
									
          $ajax = $this->input->get_post("ajax",true);	
		  $data['title']           = "Import Hasil Tes ";
		  $data['jenjang']           = $this->input->get_post("jenjang");
		  $data['dt']              = $this->Di->get_where("kota",array("id"=>$_SESSION['tmkota_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('hasiltes/form_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "hasiltes/form_peserta";
			 
			 $this->load->view('kdashboard/page_header',$data);
		 }
	
		
	}
	
	function import_excel()
		{
			error_reporting(0);
		$this->load->library('PHPExcel/IOFactory');
      
	
	   
	    $kota           = $_SESSION['tmkota_id'];
	    $kompetisi      = $_POST['kompetisi'];
	    $jenjang        = $_POST['jenjang'];
	    $sheetup   = 0;
	    $rowup     = 2;
		
		
		$fileName = time().".xlsx";
         
        $config['upload_path'] = './excel/'; 
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 1000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('file');
		
        $inputFileName = './excel/'.$fileName;
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet($sheetup);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
             $to=0;
             $jum=0;
			 
            for ($row = $rowup; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                 
				
			 	$no           =      $this->db->escape_str(ucwords(trim($rowData[0][0])));  
			 	$no_test      =      str_replace(array("-","."," ","_",""),"",trim($rowData[0][1]));  
			$eksplorasi   =      number_format($this->db->escape_str(ucwords(trim($rowData[0][4]))),2); 
				
		    $cek        = $this->db->query("SELECT id,no_test3,nas_nilai FROM tm_siswa where REPLACE(no_test3,'.','') ='".$no_test."' ")->row();
			//print_r($cek); exit();
			 $nilai_cbt  = (50 * $cek->nas_nilai) / 100;
			 $nilai_eksplorasi = (50 * $eksplorasi) / 100;
			 $nilai_akhir = $nilai_cbt + $nilai_eksplorasi;
			if(count($cek) >0 ){
		
			$jum++;
                 $data = array(
                    "eksplorasi "=> $eksplorasi,                   
                    "nilai_akhir "=> $nilai_akhir               
                    
                );
				
				
			
				
				$this->db->where("id",$cek->id);
				$this->db->update("tm_siswa",$data);
				
            }			
			
        }
		
		unlink("./excel/".$fileName);
		
	//	print_r($data); echo "sbntar"; exit();
			echo "<center><br><b>". $jum. "</b>  PESERTA  BERHASIL DIUPLOAD ";
			echo "<br> SILAHKAN PERIKSA KEMBALI HASIL UPLOAD APAKAH SUDAH SESUAI ? JIKA BELUM SESUAI SILAHKAN MASUKKAN NILAI DAN PERIKSA FILE EXCEL ANDA";
           echo '<br>
			                       <a href="'.site_url("klokal/import?jenjang=".$jenjang."").'" class="btn btn-danger btn-sm" target="_blank"> KLIK DISINI UNTUK KEMBALI KE HALAMAN IMPORT DATA </a>';
								
	}
	
	
	
	function import_excel_prov()
		{
			error_reporting(0);
		$this->load->library('PHPExcel/IOFactory');
      
	
	   
	   
	    $kompetisi      = $_POST['kompetisi'];
	   
	    $sheetup   = 0;
	    $rowup     = 2;
		
		
		$fileName = time().".xlsx";
         
        $config['upload_path'] = './excel/'; 
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 1000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('file');
		
        $inputFileName = './excel/'.$fileName;
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet($sheetup);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
             $to=0;
             $jum=0;
			 
            for ($row = $rowup; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                 $jum++;
				
			 	 $id                =      substr($rowData[0][0],1);  
			  	$password_ujian      =      $rowData[0][2];  
		
		
				$this->db->set("password_ujian",$password_ujian);
				
				$this->db->where("id",$id);
				$this->db->update("provinsi");
				
            			
			
        }
		
		unlink("./excel/".$fileName);
		
	//	print_r($data); echo "sbntar"; exit();
			echo "<center><br><b>". $jum. "</b>  PESERTA  BERHASIL DIUPLOAD ";
			echo "<br> SILAHKAN PERIKSA KEMBALI HASIL UPLOAD APAKAH SUDAH SESUAI ? JIKA BELUM SESUAI SILAHKAN MASUKKAN NILAI DAN PERIKSA FILE EXCEL ANDA";
           echo '<br>
			                       <a href="'.site_url("klokal/import?jenjang=".$jenjang."").'" class="btn btn-danger btn-sm" target="_blank"> KLIK DISINI UNTUK KEMBALI KE HALAMAN IMPORT DATA </a>';
								
	}
		
	
	
	public function test(){
		
		require_once(APPPATH.'libraries/kepdf/vendor/autoload.php');
		require_once(APPPATH.'system/helpers/dompdf/dompdf.php');
		
         $PdfPath = APPPATH.'system/helpers/dompdf';
		\PhpOffice\PhpWord\Settings::setPdfRendererPath($PdfPath);
		\PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
		$phpWord = \PhpOffice\PhpWord\IOFactory::load('piagam.docx');
		$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
		$xmlWriter->save('piagamdendi.pdf');
			
			 
			 
		
		
	}
	
	
	public function panitia(){
		
		
		
		 
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Input Pendamping ";
		 
	     if(!empty($ajax)){
					    
			 $this->load->view('official/page_pendamping',$data);
		 }else{
			 
			
		     $data['konten'] = "official/page_pendamping";
			 
			 $this->load->view('kdashboard/page_header',$data);
		 }
	
		
	}
	
	
	public function g_pendamping(){
		
		  $iTotalRecords = $this->M_lokal->g_pendamping(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->M_lokal->g_pendamping(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			//$nonya = "<a href='".site_url('klokal/sertifikatofficial?q='.base64_encode($val['id']).'')."' target='_blank' class='btn btn-danger btn-sm'><i class='fa fa-file'> Unduh Sertifikat </i></a>";
		                      
												 
				$no = $i++;
				$records["data"][] = array(
					$no,
					"Belum Tersedia",
					'<img src="'.base_url().'__statics/upload/'.$val['foto'].'" width="30px" class="img-responsive">',
				  	$val['nama'],
					  $val['jabatan'],
					$val['hp'],				
										
					$val['alamat'],
				
					
					
						 '
					<a class="btn btn-info  ubah" urlnya = "'.site_url("klokal/f_pendamping").'"  datanya="'.$val['id'].'" title="Ubah Data">
					<i class=" fa fa-pencil-square  " style="color:white"></i>
					</a>
					<a class="btn btn-info  hapus" urlnya = "'.site_url("klokal/h_pendamping").'"  datanya="'.$val['id'].'" title="Hapus Data">
					<i class=" fa fa-trash-o  " style="color:white"></i>
					</a>
					
					 '	
					
					
                			
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}

	public function sertifikatofficial(){
       // error_reporting(0);
	   
		$tmsiswa_id = base64_decode($this->input->get("q")); 
		//$nik       = base64_decode(str_replace(array())$this->input->get("nik")); 
	
	   $datasiswa = $this->db->query("select * from tr_panitia where id='".$tmsiswa_id."' ")->row();
		   
	   if(count($datasiswa) >0){
	
		   $pdf = new Fpdi();
	
		   $this->load->library('ciqrcode'); //pemanggilan library QR CODE
	
		   $config['cacheable']	= true; 
		   $config['cachedir']		= './tmp/chache'; 
		   $config['errorlog']		= './tmp/'; 
		   $config['imagedir']		= './tmp/'; 
		   $config['quality']		= true; 
		   $config['size']			= '1024'; 
		   $config['black']		= array(224,255,255);
		   $config['white']		= array(70,130,180); 
		   $this->ciqrcode->initialize($config);
	
		   //$image_name=$datasiswa->id.'.png'; 
		   $image_name= $datasiswa->id.'_official.png'; 
	
		   $params['data'] = "https://ksm.kemenag.go.id/publik/officialValiditasKabko?q=".base64_encode($datasiswa->id)."&s=".base64_encode(str_replace(array("'","-",":","."," "),"",$datasiswa->nama)).""; 
		   $params['level'] = 'H'; 
		   $params['size'] = 10;
		   $params['savename'] = FCPATH.$config['imagedir'].$image_name; 
		   $this->ciqrcode->generate($params);
	
		   $foto ="";
		   if($datasiswa->foto !=""){
		   $foto = $_SERVER["DOCUMENT_ROOT"].'/__statics/upload/'.$datasiswa->foto;
		   }
	
		   $kota_madrasah = $this->Di->get_kondisi(array("id"=>$_SESSION['tmkota_id']),"kota","nama");
		   $wilayah       = $kota_madrasah;
			
			  
			  
		   
	
	
			   $pdf->addPage('L');
			   
			   $pdf->setSourceFile('officialkabko.pdf');
		   
			   $tplIdx = $pdf->importPage(1);
			   $pdf ->useTemplate($tplIdx, null, null, null, null,FALSE);
			   
			   
			   $pdf->SetFont('Arial','B',26);
			   $pdf->SetTextColor(6, 0, 0);
	
			   $pdf->Cell(275,110,$datasiswa->nama,0,0,'C');
	
			   $pdf->Ln(16);
			   $pdf->SetFont('Arial','B',15);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(270,93,$datasiswa->jabatan,0,0,'C');

			   $pdf->Ln(7);
			$pdf->SetFont('Arial','B',12);
			$pdf->SetTextColor(6, 0, 0);
			$pdf->Cell(null,93,$wilayah,0,0,'C');
	
			   
			   //$pdf->Write(0, '');
	
	
			   $pdf->Image("tmp/".$image_name,260,175,30,30);
			//    if($foto !=""){
			//    $pdf->Image($foto,228,175,30,30);
			//    }
	
	
			   $pdf->Output('I', 'Sertifikat '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf');
	
	
	
	   }else{
	
		   echo "404 Not Found";
	   }
	 }



	
	public function f_pendamping(){
		
		 $id = $this->input->get_post("id",TRUE);
		 
		 $data = array();
		    if(!empty($id)){
				
				$data['dataform'] = $this->Di->get_where("tr_panitia",array("id"=>$id));
			}
	     
		 $data['title']  = "Form Pendamping";
		 $this->load->view('official/form_pendamping',$data);
	}
	
	public function simpanpendamping(){
		
		error_reporting(0);
		
		 $this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
        
    	 
			
				$config = array(
				  //  array('field' => 'f[nisn]', 'label' => 'NISN ', 'rules' => 'trim|required'),
				    
				    array('field' => 'f[hp]', 'label' => 'Hp  ', 'rules' => 'trim|required'),
				    array('field' => 'f[jabatan]', 'label' => 'Jabatan   ', 'rules' => 'trim|required'),
				   
				  
				  
				 
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
           if ($this->form_validation->run() == true) {
			  
		
		
				
								
							$id = $this->input->get_post("id",true);			
							$config['upload_path'] = './__statics/upload';		
							$config['allowed_types'] = 'jpg|png|jpeg|gif|'; 	
							$config['overwrite'] = true; 
                            $config['max_size'] = '1000';							
							$imagenew				 =str_replace(array("."," ","-","/","?","'"),"_",$f['nama']).time().".".pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
						
							$config['file_name'] = $imagenew;
							
							$this->load->library('upload', $config);

							
							
									if ( ! $this->upload->do_upload('file'))
									{
										if($id ==""){
											header('Content-Type: application/json');
                                            echo json_encode(array('error' => true, 'message' => "Periksa kembali foto Anda"));
											 
										}else{
											
											$this->M_lokal->u_pendamping($id);
											echo "sukses";
										}
											
									
									}
									else{
										
										
										
										  if($id ==""){
											  
											
													
												  $this->db->set("foto",$imagenew);
												  $this->M_lokal->i_pendamping();
												  echo "sukses";
												  
											
											  
										  }else{
											  $this->db->set("foto",$imagenew);
											  $this->M_lokal->u_pendamping($id);
											  echo "sukses";
										  }
										
									}
									
									
				  	
						
			 } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
             }	
	
		
	}
	public function h_pendamping(){
		
		$this->Di->delete("tr_panitia",array("id"=>$_POST['id']));
		echo "sukses";
		
	}


	// Helpdesk 
	public function helpdesk(){
		
		
		
		 
		
									
		$ajax = $this->input->get_post("ajax",true);	
		 $data['title']  = "Input Helpdesk ";
		
		if(!empty($ajax)){
					   
			$this->load->view('official/page_helpdesk',$data);
		}else{
			
		   
			$data['konten'] = "official/page_helpdesk";
			
			$this->load->view('kdashboard/page_header',$data);
		}
   
	   
   }
   
   
   public function g_helpdesk(){
	   
		 $iTotalRecords = $this->M_lokal->g_helpdesk(false)->num_rows();
		 
		 $iDisplayLength = intval($_REQUEST['length']);
		 $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		 $iDisplayStart = intval($_REQUEST['start']);
		 $sEcho = intval($_REQUEST['draw']);
		 
		 $records = array();
		 $records["data"] = array(); 

		 $end = $iDisplayStart + $iDisplayLength;
		 $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		 
		 $datagrid = $this->M_lokal->g_helpdesk(true)->result_array();
		  
		  $i= ($iDisplayStart +1);
		  foreach($datagrid as $val) {
		   $nonya = "<a href='".site_url('klokal/sertifikatofficial?q='.base64_encode($val['id']).'')."' target='_blank' class='btn btn-danger btn-sm'><i class='fa fa-file'> Unduh Sertifikat </i></a>";
							 
												
			   $no = $i++;
			   $records["data"][] = array(
				   $no,
				   $nonya,
				   '<img src="'.base_url().'__statics/upload/'.$val['foto'].'" width="30px" class="img-responsive">',
					 $val['nama'],
					 $val['jabatan'],
				   $val['hp'],				
									   
				   $val['alamat'],
			   
				   
				   
						'
				   <a class="btn btn-info  ubah" urlnya = "'.site_url("klokal/f_pendamping").'"  datanya="'.$val['id'].'" title="Ubah Data">
				   <i class=" fa fa-pencil-square  " style="color:white"></i>
				   </a>
				   <a class="btn btn-info  hapus" urlnya = "'.site_url("klokal/h_pendamping").'"  datanya="'.$val['id'].'" title="Hapus Data">
				   <i class=" fa fa-trash-o  " style="color:white"></i>
				   </a>
				   
					'	
				   
				   
						   
						   
			   
				  

				 );
			 }
	   
		 $records["draw"] = $sEcho;
		 $records["recordsTotal"] = $iTotalRecords;
		 $records["recordsFiltered"] = $iTotalRecords;
		 
		 echo json_encode($records);
   }

   public function sertifikatofficialhelpdesk(){
	  // error_reporting(0);
	   $tmsiswa_id = base64_decode($this->input->get("q")); 
	   //$nik       = base64_decode(str_replace(array())$this->input->get("nik")); 
   
	  $datasiswa = $this->db->query("select * from tr_panitia where id='".$tmsiswa_id."' ")->row();
		  
	  if(count($datasiswa) >0){
   
		  $pdf = new Fpdi();
   
		  $this->load->library('ciqrcode'); //pemanggilan library QR CODE
   
		  $config['cacheable']	= true; 
		  $config['cachedir']		= './tmp/chache'; 
		  $config['errorlog']		= './tmp/'; 
		  $config['imagedir']		= './tmp/'; 
		  $config['quality']		= true; 
		  $config['size']			= '1024'; 
		  $config['black']		= array(224,255,255);
		  $config['white']		= array(70,130,180); 
		  $this->ciqrcode->initialize($config);
   
		  //$image_name=$datasiswa->id.'.png'; 
		  $image_name= $datasiswa->id.'_official.png'; 
   
		  $params['data'] = "https://ksm.kemenag.go.id/publik/officialValiditas?q=".base64_encode($datasiswa->id)."&s=".base64_encode(str_replace(array("'","-",":","."," "),"",$datasiswa->nama)).""; 
		  $params['level'] = 'H'; 
		  $params['size'] = 10;
		  $params['savename'] = FCPATH.$config['imagedir'].$image_name; 
		  $this->ciqrcode->generate($params);
   
		  $foto ="";
		  if($datasiswa->foto !=""){
		  $foto = $_SERVER["DOCUMENT_ROOT"].'/__statics/upload/'.$datasiswa->foto;
		  }
   
		  $kota_madrasah = $this->Di->get_kondisi(array("id"=>$_SESSION['tmkota_id']),"kota","nama");
		  $wilayah       = $kota_madrasah;
		   
			 
			 
		  
   
   
			  $pdf->addPage('L');
			  
			  $pdf->setSourceFile('official.pdf');
		  
			  $tplIdx = $pdf->importPage(1);
			  $pdf ->useTemplate($tplIdx, null, null, null, null,FALSE);
			  
			  
			  $pdf->SetFont('Arial','B',26);
			  $pdf->SetTextColor(6, 0, 0);
   
			  $pdf->Cell(275,110,$datasiswa->nama,0,0,'C');
   
			  $pdf->Ln(35);
			  $pdf->SetFont('Arial','B',26);
			  $pdf->SetTextColor(6, 0, 0);
			  $pdf->Cell(270,93,$datasiswa->jabatan,0,0,'C');
			  $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',20);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,$wilayah,0,0,'C');
   
			  
			  //$pdf->Write(0, '');
   
   
			  $pdf->Image("tmp/".$image_name,260,175,30,30);
		   //    if($foto !=""){
		   //    $pdf->Image($foto,228,175,30,30);
		   //    }
   
   
			  $pdf->Output('I', 'Sertifikat '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf');
   
   
   
	  }else{
   
		  echo "404 Not Found";
	  }
	}



   
   public function f_helpdesk(){
	   
		$id = $this->input->get_post("id",TRUE);
		
		$data = array();
		   if(!empty($id)){
			   
			   $data['dataform'] = $this->Di->get_where("tr_panitia",array("id"=>$id));
		   }
		
		$data['title']  = "Form Pendamping";
		$this->load->view('official/form_pendamping',$data);
   }
   
   public function simpanhelpdesk(){
	   
	   error_reporting(0);
	   
		$this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
	   
		
		   
			   $config = array(
				 //  array('field' => 'f[nisn]', 'label' => 'NISN ', 'rules' => 'trim|required'),
				   
				   array('field' => 'f[hp]', 'label' => 'Hp  ', 'rules' => 'trim|required'),
				   array('field' => 'f[jabatan]', 'label' => 'Jabatan   ', 'rules' => 'trim|required'),
				  
				 
				 
				
   
				   
			   );
			   
			   $this->form_validation->set_rules($config);	
	   
		  if ($this->form_validation->run() == true) {
			 
	   
	   
			   
							   
						   $id = $this->input->get_post("id",true);			
						   $config['upload_path'] = './__statics/upload';		
						   $config['allowed_types'] = 'jpg|png|jpeg|gif|'; 	
						   $config['overwrite'] = true; 
						   $config['max_size'] = '1000';							
						   $imagenew				 =str_replace(array("."," ","-","/","?","'"),"_",$f['nama']).time().".".pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
					   
						   $config['file_name'] = $imagenew;
						   
						   $this->load->library('upload', $config);

						   
						   
								   if ( ! $this->upload->do_upload('file'))
								   {
									   if($id ==""){
										   header('Content-Type: application/json');
										   echo json_encode(array('error' => true, 'message' => "Periksa kembali foto Anda"));
											
									   }else{
										   
										   $this->M_lokal->u_pendamping($id);
										   echo "sukses";
									   }
										   
								   
								   }
								   else{
									   
									   
									   
										 if($id ==""){
											 
										   
												   
												 $this->db->set("foto",$imagenew);
												 $this->M_lokal->i_pendamping();
												 echo "sukses";
												 
										   
											 
										 }else{
											 $this->db->set("foto",$imagenew);
											 $this->M_lokal->u_pendamping($id);
											 echo "sukses";
										 }
									   
								   }
								   
								   
					 
					   
			} else {
			
		  
			   header('Content-Type: application/json');
			   echo json_encode(array('error' => true, 'message' => validation_errors()));
		   
			}	
   
	   
   }
   public function h_helpdesk(){
	   
	   $this->Di->delete("tr_panitia",array("id"=>$_POST['id']));
	   echo "sukses";
	   
   }
	
}
