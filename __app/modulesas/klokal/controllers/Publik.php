<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use setasign\Fpdi\Fpdi;

require_once(APPPATH.'libraries/fpdf/fpdf.php');
require_once(APPPATH.'libraries/fpdi/src/autoload.php');
ini_set('memory_limit','770M');
ini_set('max_execution_time', 0);
class Publik extends CI_Controller {

    public function __construct()
      {
        parent::__construct();
		
     
		
	  }

	  public function officialValiditas(){

		$tmsiswa_id = base64_decode($this->input->get("q")); 
		//$nik       = base64_decode(str_replace(array())$this->input->get("nik")); 
	
	   $datasiswa = $this->db->query("select * from tr_panitia where id='".$tmsiswa_id."' ")->row();
		   
	   if(count($datasiswa) >0){
	
		   $pdf = new Fpdi();
	
		   $this->load->library('ciqrcode'); //pemanggilan library QR CODE
	
		   $config['cacheable']	= true; 
		   $config['cachedir']		= './tmp/chache'; 
		   $config['errorlog']		= './tmp/'; 
		   $config['imagedir']		= './tmp/'; 
		   $config['quality']		= true; 
		   $config['size']			= '1024'; 
		   $config['black']		= array(224,255,255);
		   $config['white']		= array(70,130,180); 
		   $this->ciqrcode->initialize($config);
	
		   //$image_name=$datasiswa->id.'.png'; 
		   $image_name= $datasiswa->id.'_official.png'; 
	
		   $params['data'] = "https://ksm.kemenag.go.id/publik/officialValiditas?q=".base64_encode($datasiswa->id)."&s=".base64_encode(str_replace(array("'","-",":","."," "),"",$datasiswa->nama)).""; 
		   $params['level'] = 'H'; 
		   $params['size'] = 10;
		   $params['savename'] = FCPATH.$config['imagedir'].$image_name; 
		   $this->ciqrcode->generate($params);
		   $foto = $_SERVER["DOCUMENT_ROOT"].'/ksm/__statics/upload/'.$datasiswa->foto;
		  
	
	
			
			  
			  
		   
	
	
			   $pdf->addPage('L');
			   
			   $pdf->setSourceFile('official.pdf');
		   
			   $tplIdx = $pdf->importPage(1);
			   $pdf ->useTemplate($tplIdx, null, null, null, null,FALSE);
			   
			   
			   $pdf->SetFont('Arial','B',26);
			   $pdf->SetTextColor(6, 0, 0);
	
			   $pdf->Cell(null,110,$datasiswa->nama,0,0,'C');
	
			   $pdf->Ln(35);
			   $pdf->SetFont('Arial','B',26);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(275,93,$datasiswa->jabatan,0,0,'C');
	
			   
			   //$pdf->Write(0, '');
	
	
			   $pdf->Image("tmp/".$image_name,260,175,30,30);
			  // $pdf->Image($foto,228,175,30,30);
	
			   $pdf->Output('I', 'Sertifikat '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf');
	
	
	
	   }else{
	
		   echo "404 Not Found";
	   }
	 }


public function validitas(){

	$tmsiswa_id = base64_decode($this->input->get("s")); 
	$nik       = base64_decode($this->input->get("q")); 

   $datasiswa = $this->db->query("select d_p,nik_siswa,madrasah,provinsi,trkompetisi_id,id,nama,jenjang_madrasah,kota_madrasah from v_siswa_kabkota where nla IS NOT NULL AND id='".$tmsiswa_id."' ")->row();
	   
   if(count($datasiswa) >0){

	   $pdf = new Fpdi();

	   $this->load->library('ciqrcode'); //pemanggilan library QR CODE

	   $config['cacheable']	= true; 
	   $config['cachedir']		= './tmp/chache'; 
	   $config['errorlog']		= './tmp/'; 
	   $config['imagedir']		= './tmp/'; 
	   $config['quality']		= true; 
	   $config['size']			= '1024'; 
	   $config['black']		= array(224,255,255);
	   $config['white']		= array(70,130,180); 
	   $this->ciqrcode->initialize($config);

	   //$image_name=$datasiswa->id.'.png'; 
	   $image_name= $datasiswa->id.'.png'; 

	   $params['data'] = "https://ksm.kemenag.go.id/publik/validitas?q=".base64_encode($datasiswa->nik_siswa)."&s=".base64_encode($datasiswa->id).""; 
	   $params['level'] = 'H'; 
	   $params['size'] = 10;
	   $params['savename'] = FCPATH.$config['imagedir'].$image_name; 
	   $this->ciqrcode->generate($params);

	   $ketkabko = array("1"=>"Juara I","2"=>"Juara II","3"=>"Juara III","4"=>"Juara Harapan I","5"=>"Juara Harapan II","6"=>"Juara Harapan III");
	   $jenjangn = array("1"=>"Madrasah Ibtidaiyah/Sederajat ","2"=>"Madrasah Tsanawiyah/Sederajat","3"=>"Madrasah Aliyah/Sederajat");
	   $kompetisi = $this->Di->get_kondisi(array("id"=>$datasiswa->trkompetisi_id),"tr_kompetisi","nama");
	   $kota_madrasah = $this->Di->get_kondisi(array("id"=>$datasiswa->kota_madrasah),"kota","nama");
	   $wilayah = strtoupper($jenjangn[$datasiswa->jenjang_madrasah])." ".$kota_madrasah;


		 if($datasiswa->d_p <= 6){
   
		   $status =   $ketkabko[$datasiswa->d_p];
		  
		  
	   


		   $pdf->addPage('L');
		   
		   $pdf->setSourceFile('juara.pdf');
	   
		   $tplIdx = $pdf->importPage(1);
		   $pdf ->useTemplate($tplIdx, null, null, null, null,FALSE);
		   
		   
		   $pdf->SetFont('Arial','B',26);
		   $pdf->SetTextColor(6, 0, 0);

		   $pdf->Cell(null,93,$datasiswa->nama,0,0,'C');

		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,$datasiswa->madrasah,0,0,'C');

		   $pdf->Ln(20);
		   $pdf->SetFont('Arial','B',26);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(271,93,$status,0,0,'C');


		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,"Bidang Studi ".$kompetisi,0,0,'C');

		   $pdf->Ln(10);
		   $pdf->SetFont('Arial','B',15);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(null,93,"JENJANG ".$wilayah,0,0,'C');
		   
		   
		   //$pdf->Write(0, '');


		   $pdf->Image("tmp/".$image_name,260,170,30,30);


		   $pdf->Output('I', 'PIAGAM  '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf');

		   }else{
			   $status = " Peserta ";




			   $pdf->addPage('L');
		   
			   $pdf->setSourceFile('peserta.pdf');
		   
			   $tplIdx = $pdf->importPage(1);
			   $pdf ->useTemplate($tplIdx, null, null, null, null,FALSE);
			   
			   
			   $pdf->SetFont('Arial','B',26);
			   $pdf->SetTextColor(6, 0, 0);
   
			   $pdf->Cell(null,100,$datasiswa->nama,0,0,'C');
   
			   $pdf->Ln(15);
			   $pdf->SetFont('Arial','B',15);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(null,93,$datasiswa->madrasah,0,0,'C');
   
			   $pdf->Ln(20);
			   $pdf->SetFont('Arial','B',26);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(271,93,null,0,0,'C');
   
   
			   $pdf->Ln(10);
			   $pdf->SetFont('Arial','B',15);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(null,93,"Bidang Studi ".$kompetisi,0,0,'C');
   
			   $pdf->Ln(10);
			   $pdf->SetFont('Arial','B',15);
			   $pdf->SetTextColor(6, 0, 0);
			   $pdf->Cell(null,93,"JENJANG ".$wilayah,0,0,'C');
			   
			   
			   //$pdf->Write(0, '');
   
   
			   $pdf->Image("tmp/".$image_name,260,170,30,30);
   
   
			   $pdf->Output('I', 'PIAGAM  '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf');


	   
		   }


   }else{

	   echo "404 Not Found";
   }
 }
	
	   
		   public function generatenomortes(){
				
		 
	
		 
		$siswa = $this->db->query("select id,provinsi,jenjang,trkompetisi_id,juara_provinsi from tm_siswa where  no_test3='0' and  status3='1' order by jenjang asc, trkompetisi_id asc,nama asc ")->result();
		
		 //echo count($siswa); exit();
		   foreach($siswa as $row){
			     $maxid     = $this->db->query("select max(urutan) as pendaftar from tm_siswa")->row();
				 $pendaftar = $maxid->pendaftar+1;
				 $provinsi     = $row->provinsi;
				 $jenjang     = $row->jenjang;
				 $trkompetisi_id     = (strlen($row->trkompetisi_id)==1) ? "0".$row->trkompetisi_id : $row->trkompetisi_id;
				 $juara_provinsi     = (strlen($row->juara_provinsi)==1) ? "0".$row->juara_provinsi : $row->juara_provinsi;
				 
				 
				 if(strlen($pendaftar)==1){
					 $nopen = "0000".$pendaftar;
				 }else if(strlen($pendaftar)==2){
					 
					 $nopen = "000".$pendaftar;
				 }else if(strlen($pendaftar)==3){
					 
					  	 $nopen = "00".$pendaftar;
				 }
				 

				 
				  $no_test    = "0".$jenjang.".".$provinsi.".".$trkompetisi_id.".".$nopen; 
				 
				 $this->db->set("urutan",($pendaftar));
				 $this->db->set("no_test3",($no_test));
				 $this->db->where("id",$row->id);
				 $this->db->update("tm_siswa");
			   
			    
			  
		   }
		   echo "sukses".count($siswa); 
		  
		   
		   
		
		
	}
	
	
	public function piagam(){
		
		$token = base64_decode($this->input->get_post("token"));
		
		echo "<center> Disini nanti akan muncul data siswa dll, masih dibuat </center>";
		
		
	}
	
	public function kota(){
		
		$id = $this->input->get_post("id",TRUE);
		 $kota = $this->Di->query("select * from kota where provinsi_id='".$id."' order by nama asc")->result();
		                                                     ?><option value="">- Pilih Kabupaten/Kota -</option><?php
															    foreach($kota as $row){
																	?><option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option><?php 
																}
																
																
	}
	
    public function kecamatan(){
		
		$id = $this->input->get_post("id",TRUE);
		 $kota = $this->Di->query("select * from kecamatan where kota_id='".$id."' order by nama asc")->result();
															      ?><option value="">- Pilih Kecamatan -</option><?php
																foreach($kota as $row){
																	?><option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option><?php 
																}
																
																	?><option value="0">Lainnya </option><?php
	}
	
	  public function desa(){
		
		$id = $this->input->get_post("id",TRUE);
		 $kota = $this->Di->query("select * from desa where kecamatan_id='".$id."' order by nama asc")->result();
															      ?><option value="">- Pilih Kelurahan/Desa -</option><?php
																foreach($kota as $row){
																	?><option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option><?php 
																}
																
																	?><option value="0">Lainnya </option><?php
	}
	
	
	 public function madrasah(){
		
		$kota = $this->input->get_post("kota",TRUE);
		$jenjang = $this->input->get_post("jenjang",TRUE);
		  $where =" 1=1 ";
		    if($kota !=""){
				 $where .=" and kota ='".$kota."'";
			}
			
			 if($jenjang !=""){
				 $where .=" and jenjang ='".$jenjang."'";
			}
		 $kota = $this->Di->query("select * from tm_madrasah where $where  order by nama asc")->result();
															      ?><option value="">- Pilih Madrasah/Sekolah -</option><?php
																foreach($kota as $row){
																	?><option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option><?php 
																}
																
																	?><option value="">Tampilkan Semua </option><?php
	}
	 public function kompetisi(){
		
		$id = $this->input->get_post("tmmadrasah_id",TRUE);
		 $kota = $this->Di->query("select * from tr_kompetisi where tmmadrasah_id='".$id."' order by nama asc")->result();
															      ?><option value="">- Pilih Kompetisi  -</option><?php
																foreach($kota as $row){
																	?><option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option><?php 
																}
																
																	?><option value="">Tampilkan Semua </option><?php
	}
	 public function kompetisi2(){
		
		$id = $this->input->get_post("id",TRUE);
		 $kota = $this->Di->query("select * from tr_kompetisi where tmmadrasah_id='".$id."' order by nama asc")->result();
															      ?><option value="">- Pilih Kompetisi  -</option><?php
																foreach($kota as $row){
																	?><option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option><?php 
																}
																
																	?><option value="">Tampilkan Semua </option><?php
	}
	
  
	
	
}
