            
             <div class="alert alert-info alert-dismissible fade show" role="alert">
                <i class="bi bi-info-circle me-1"></i>
                <?php echo $kka->nama; ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              <br>
            <form class="row g-3" method="post" action="<?php echo site_url('kka/saveNotisi'); ?>">
                <div class="col-12">
                    <input type="hidden" name="satker_id" value="<?php echo $satker_id; ?>">
                    <input type="hidden" name="kka_id" value="<?php echo $kka->id; ?>">

                  <label for="inputNanme4" class="form-label">Kode Temuan </label>
                     <select class="form-control" name="f[kode_temuan]" id="select2Temuan"  required>
                        <option value="">- Pilih Temuan -</option>
                        <?php
                        $unit_kerja = $this->db->get_where("tm_temuan",array("kel !="=>""))->result();
                        foreach ($unit_kerja as $unit) {
                        ?>

                            <optgroup label="<?php echo $unit->deskripsi; ?>">
                            <?php
                            $satkerData = $this->db->query("SELECT * from tm_temuan where sub_kel='{$unit->id}'")->result();
                            foreach ($satkerData as $row) {
                            ?>
                                <option value="<?php echo $row->jenis; ?>" <?php if(isset($notisi)){ echo ($notisi->kode_temuan==$row->jenis) ? "selected":""; }  ?>><?php echo $row->jenis; ?> - <?php echo $row->deskripsi; ?></option>
                            <?php
                            }
                            ?>
                            </optgroup>

                        <?php
                        }
                        ?>
                        </select>
                </div>
                <div class="col-12">
                  <label for="inputEmail4" class="form-label">Rekomendasi </label>
                    <select class="form-control" name="f[kode_rekomendasi]" id="kode_rekomendasi" required>
                        <option value="">- Rekomendasi - </option>

                    </select>
                </div>
                <div class="col-12">
                  <label for="inputPassword4" class="form-label">Keterangan</label>
                  <textarea class="form-control" name="f[keterangan]" required><?php if(isset($notisi)){ echo $notisi->keterangan; }  ?></textarea>
                </div>
               
                <div class="text-center">
                  <button type="submit" class="btn btn-primary">Simpan Notisi</button>
                  <button type="reset" class="btn btn-secondary">Reset</button>
                </div>
              </form>


 <script type="text/javascript">
            //   $(document).ready(function() {
            //   $('.js-example-basic-single').select2({ width: '100%',  dropdownParent: $("#largeModalNotisi");  });
           
            //  });

			 $("#select2Temuan").select2({
				dropdownParent: $("#largeModalNotisi"),
                width: '100%'
			});

            $(document).on("change","#select2Temuan",function(){
				var id 		  = $(this).val();
			
				$.post("<?php echo site_url('kka/getRekomendasi'); ?>",{id:id},function(data){

					$("#kode_rekomendasi").html(data);

				})


			  })
         </script>


              