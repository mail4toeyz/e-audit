<script src="https://apis.google.com/js/platform.js"></script>
<div class="alert alert-primary">
    <h4><b>Petunjuk Kompetisi Sains Madrasah : </b></h4> 
      <ol start="1">
       <li>Peserta KSM Kabupaten/Kota diikuti oleh siswa terbaik tiap bidang
        studi yang dilombakan yang merupakan hasil tahapan seleksi KSM
        Satuan Pendidikan di wilayah kabupaten/kota setempat. </li>
        <li> Setiap Madrasah/Sekolah dapat mengirimkan maksimal 2 siswa
        terbaiknya tiap bidang studi yang dilombakan. </li>
        <li> Peserta yang Anda daftarkan akan melalui tahap verifikasi yang dilakukan oleh Komite Kabupaten/Kota </li>
        <li> Hanya Peserta yang sudah dilakukan verifikasi yang dapat mengikuti KSM tingkat Kabupaten/Kota </li>
        <li> Pengumuman Juara KSM Tingkat Kabupaten/Kota, Tingkat Provinsi dan Tingkat Nasional dapat diakses melalui menu pengumuman </li>

      </ol>


  </div>
  
   <div class="alert alert-warning">
    Official Youtube Kompetisi Sains Madrasah 
    <center><div class="g-ytsubscribe dropdown-item d-flex" data-channelid="UCyxXd5WX9Wz1aSgnkTUR7lQ" data-layout="default" data-count="hidden"></div></center>
                          

  </div>

  <div class="row">
  <?php 
                        $jumlah=0;
                        $status = array("0"=>"Draft (Belum dikirim)","1"=>"Belum diverifikasi","2"=>"Verifikasi Lulus","3"=>"Tidak Lulus");
                        $statusket = array("0"=>"Peserta  belum dikirim dan tidak dapat diverifikasi","1"=>"Belum diverifikasi oleh Komite Kabupaten/Kota","2"=>"Diverifikasi Lulus dan berhak mengikuti CBT KSM","3"=>"Diverifikasi Tidak Lulus dan Tidak berhak ikut KSM ");
                         foreach($status as $i=>$r){
                           $peserta = $this->db->query("select count(id) as jml from v_siswa where tmmadrasah_id='{$_SESSION['tmmadrasah_id']}' and status='{$i}'")->row();
                       
                           $jumlah  = $jumlah + $peserta->jml;
                       ?>
                      <div class="col-lg-3 col-md-6">
                      <div class="card">
                       <div class="card-body">
                        <div class="">
                          <div class="wrapper">
                            <h3 class="mb-0 font-weight-semibold"><i class="fa fa-copy"></i> &nbsp; <?php echo $this->Di->formatuang2($peserta->jml); ?></h3>
                            <h5 class="mb-0 font-weight-medium text-primary"> <?php echo $r; ?> </h5>
                            <small class="mb-0 text-muted"> <?php echo $statusket[$i]; ?> </small>
                          </div>
                         
                        </div>
                      </div>
                      </div>
                      </div>
                      <?php 
                         }
                      ?>
  </div>
  <div class="row" id="report4">
  

            <?php 
            foreach($this->Di->get_wherearray("tr_kompetisi",array("tmmadrasah_id"=>$m->jenjang)) as $r){
                $peserta = $this->db->query("select count(id) as jml from tm_siswa where tmmadrasah_id='".$m->id."' and trkompetisi_id='".$r->id."'")->row();

            ?>
                <div class="col-md-4">
                    <div class="card text-center social-bottom sb-fb rounded">
                        <i class="fa fa-edit" style="color:white;font-size:30px"></i>
                        <div><?php echo $peserta->jml; ?></div>
                        <p><?php echo $r->nama; ?></p>
                    </div>
                </div>
            <?php 
            }
            ?>


               
            </div>
