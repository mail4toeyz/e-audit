 <ul class="list-unstyled" style="color:black;font-weight:bold">
                <li class="active"> <a  class="" href="<?php echo site_url("dashboard"); ?>" title="Dashboard "><i class="fa fa-laptop"></i>Dashboard    </a></li>
                <li class=""> <a  class="menuklik" href="<?php echo site_url("lokal/data"); ?>" title="Data Lembaga "><i class="fa fa-institution"></i>Data  Lembaga   </a></li>
				
                <li class=""> <a class="menuklik" href="<?php echo site_url("lokal/delegasi"); ?>" title="Ketua Delegasi"><i class="fa fa-user"></i>Ketua Delegasi   </a></li>
                <!-- <li class=""> <a class="menuklik" href="<?php echo site_url("lokal/pendaftaran"); ?>" title="Pendaftaran Peserta KSM"><i class="fa fa-file-text-o" aria-hidden="true"></i>  Pendaftaran Peserta  </a></li> -->
                <li class=""> <a class="menuklik" href="<?php echo site_url("lokal/peserta/0"); ?>" title="Draft Peserta KSM"><i class="fa fa-copy" aria-hidden="true"></i>  Draft   </a></li>

				<li><a href="#pesertatesksm" aria-expanded="false" data-toggle="collapse" > <i class="fa fa-users"></i>Peserta KSM </a>
                    <ul id="pesertatesksm" class=" list-unstyled" >
                     
                        <li><a class="menuklik" href="<?php echo site_url("lokal/peserta/1"); ?>" title="Satuan Pendidikan "> Satuan Pendidikan </a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("lokal/peserta/2"); ?>" title="Kabupaten/Kota "> Tingkat Kabupaten/Kota </a></li> 
                       <!-- <li><a class="menuklik" href="<?php echo site_url("lokal/provinsi"); ?>" title="Provinsi"> Tingkat Provinsi </a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("lokal/nasional"); ?>" title="Nasional"> Tingkat Nasional </a></li> -->
                    </ul>
                </li>
                <li class=""> <a class="menuklik" href="<?php echo site_url("lokal/peserta/3"); ?>" title="Tidak Lulus Peserta KSM"><i class="fa fa-times" aria-hidden="true"></i>  Tidak Lulus Verifikasi   </a></li>
                <li class=""> <a class="menuklik" href="<?php echo site_url("lokal/pengumuman"); ?>" title="Pengumuman  KSM"><i class="fa fa-bullhorn" aria-hidden="true"></i>  Pengumuman   </a></li>
              <!--  <li class=""> <a  class="menuklik" href="<?php echo site_url("lokal/pengumuman"); ?>" title="pengumuman "><i class="fa fa-bullhorn"></i> Pengumuman    </a></li>-->
            <!--   <li><a href="#pengumumanprov" aria-expanded="false" data-toggle="collapse" > <i class="fa fa-bullhorn"></i>  </a>
                    <ul id="pengumumanprov" class="collapse list-unstyled" >
                     
                        <li><a class="menuklik" href="<?php echo site_url("lokal/pengumuman"); ?>" title="Pengumuman Kabupaten/Kota  "> Pengumuman Kabupaten/Kota </a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("lokal/pengumumanprov"); ?>" title="Kabupaten/Kota "> Pengumuman Provinsi  </a></li> 
                        <li><a class="menuklik" href="<?php echo site_url("lokal/kartunasional"); ?>" title="Kartu Nasional "> Kartu Peserta Nasional  </a></li> 
                         <li><a class="menuklik" href="<?php echo site_url("lokal/pengumumannas"); ?>" title="Pengumuman KSM Nasional"> Pengumuman Nasional  </a></li> 
                        
                    </ul>
                </li>  -->

           			
				<li class=""> <a  class="menuklik" href="<?php echo site_url("lokal/perbaharui_p"); ?>" title="Profile"><i class="fa fa-edit "></i>Perbaharui Akun</a></li>
               
                <li > 	<a href="javascript:void(0)" id="keluar"><i class="fa fa-unlock-alt"></i> Keluar </a></li>
                <br>
                
               
</ul>

				
					<script type="text/javascript">	
						 $(document).on("click","#keluar",function(){
					  
					      var base_url = "<?php echo base_url(); ?>ksm/login";
					  
					      alertify.confirm("Anda akan keluar dari aplikasi, apakah anda yakin?",function(){
						  
							   $.post("<?php echo site_url("ksm/logout"); ?>",function(data){
								   
								   location.href= base_url;
								   
							   });
						 });
	  
	  
	  
                         });
		</script>
		