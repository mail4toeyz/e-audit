
<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">

    <title><?php echo $title; ?> </title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>__statics/img/kemenag.png">
    <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/admin/css/font-icon-style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/admin/css/style.default.css" id="theme-stylesheet">

    <!-- Core stylesheets -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/admin/css/ui-elements/card.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/admin/css/style.css">
    <link rel="stylesheet" href="https://www.bootstrapdash.com/demo/star-admin-pro/src/assets/css/demo_1/style.css">
	
	    <link href="<?php echo base_url(); ?>__statics/js/datepicker/datepicker.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>__statics/js/datatable/button.css" rel="stylesheet" type="text/css"/>
        <link href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
		<link href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    
		<link href="<?php echo base_url(); ?>__statics/js/alert/alert.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>__statics/js/alertify/css/alertify.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/bootstrap-toastr/toastr.min.css"/>
		 <script src="<?php echo base_url(); ?>__statics/admin/js/jquery.min.js"></script>

		<script src="<?php echo base_url(); ?>__statics/js/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>__statics/admin/js/popper/popper.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" type="text/javascript"></script>
		<script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
		<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
		<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript"></script>
		<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js" type="text/javascript"></script>
		<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js" type="text/javascript"></script>
		<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.colVis.min.js" type="text/javascript"></script>
    
    <script src="<?php echo base_url(); ?>__statics/admin/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>__statics/js/bootstrap-toastr/toastr.min.js"></script>
        <script src="<?php echo base_url(); ?>__statics/js/proses.js" ></script>
        <script src="<?php echo base_url(); ?>__statics/js/datepicker/datepicker.js" ></script>
        <script src="<?php echo base_url(); ?>__statics/js/alert/alert.js"></script>
        <link href="<?php echo base_url(); ?>__statics/js/upload/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
  <script src="<?php echo base_url(); ?>__statics/js/upload/js/fileinput.js" type="text/javascript"></script>
		
  
			
<style>
.alert-info{
	
	background:#1536ED;font-weight:bold;color:white;
}
.btn-info{
	background:#1536ED;font-weight:bold;color:white;
	
}
.btn-info:hover{
	background:#1536ED;font-weight:bold;color:white;
	
}
.btn-info:active{
	background:#1536ED;font-weight:bold;color:white;
	
}


#overlay{
  position:fixed;
  z-index:99999;
  top:0;
  left:0;
  bottom:0;
  right:0;
  background:rgba(0,0,0,0.9);
  transition: 1s 0.4s;
}
#progress{
  height:1px;
  background:#fff;
  position:absolute;
  width:0;
  top:50%;
  transition: 1s;
}
#progstat{
  font-size:0.7em;
  letter-spacing: 3px;
  position:absolute;
  top:50%;
  margin-top:-40px;
  width:100%;
  text-align:center;
  color:#fff;
}

</style>
</head>

<body> 
       
    <header class="header">
        <nav class="navbar navbar-expand-lg ">
            <div class="search-box">
                <button class="dismiss"><i class="icon-close"></i></button>
                <form id="searchForm" action="#" role="search">
                    <input type="search" placeholder="Search Now" class="form-control">
                </form>
            </div>
            <div class="container-fluid ">
                <div class="navbar-holder d-flex align-items-center justify-content-between">
                    <div class="navbar-header">
                        <a href="index.html" class="navbar-brand">
                            <div class="brand-text brand-big hidden-lg-down">
							<img src="<?php echo base_url(); ?>__statics/img/ksm.png" class="img-responsive" style="height:50px">
							KOMPETISI SAINS MADRASAH</div>
                            <div class="brand-text brand-small"></div>
                        </a>
                        <a id="toggle-btn" href="#" class="menu-btn active">
                            <span></span>
                            <span></span>
                            <span></span>
                        </a>
                    </div>
                </div>
                <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                     
                    <li class="nav-item dropdown"><a id="profile" class="nav-link logout" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url(); ?>__statics/img/kemenag.png" alt="..." class="img-fluid rounded-circle" style="height: 30px; width: 30px;"></a>
                        <ul aria-labelledby="profile" class="dropdown-menu profile">
                         
                            <li>
                                <a rel="nofollow" href="#" class="dropdown-item d-flex">
                                    <div class="msg-profile"> <img src="<?php echo base_url(); ?>__statics/img/kemenag.png" alt="..." class="img-fluid rounded-circle"></div>
                                    <div class="msg-body">
                                        <h3 class="h5"><?php echo $_SESSION['nama']; ?></h3><span><?php echo strtoupper($_SESSION['status']); ?></span>
                                    </div>
                                </a>
                                <hr>
                            </li>
							
                            <li>
                                <a rel="nofollow" class="menuklik dropdown-item" href="<?php echo site_url("lokal/perbaharui_p"); ?>" title="Profile" >
                                    <div class="notification">
                                        <div class="notification-content"><i class="fa fa-user "></i>Perbaharui Akun</div>
                                    </div>
                                </a>
                            </li>
                          
                            <li>
                                <a rel="nofollow"  href="javascript:void(0)" id="keluar" class="dropdown-item">
                                    <div class="notification">
                                        <div class="notification-content"><i class="fa fa-power-off"></i>Logout</div>
                                    </div>
                                </a> 
                            </li>
                        </ul>
                    </li>
                
                </ul> 
            </div>
        </nav>
    </header>

<!--====================================================
                        PAGE CONTENT
======================================================-->
    <div class="page-content d-flex align-items-stretch">

        <!--***** SIDE NAVBAR *****-->
        <nav class="side-navbar">
            <div class="sidebar-header d-flex align-items-center">
                
                <div class="title">
                    <h6 class="h6"><b><?php echo $_SESSION['nama']; ?></b></h6>
                </div>
            </div>
           
            <!-- Sidebar Navidation Menus-->
           
           <?php $this->load->view("page_menu"); ?>
        </nav>

        <div class="content-inner chart-cont">
                                                             <div id="overlay">
																<div id="progstat"></div>
																<div id="progress"></div>
															</div>
															 <div class="my-box"></div>
               
                   <div id="loadeusi"> <?php $this->load->view(isset($konten) ? $konten:"page_default"); ?></div>

         </div>
    </div> 

    <!--Global Javascript -->
   
    <script src="<?php echo base_url(); ?>__statics/admin/js/tether.min.js"></script>
    
    <script src="<?php echo base_url(); ?>__statics/admin/js/jquery.cookie.js"></script>
    <script src="<?php echo base_url(); ?>__statics/admin/js/jquery.validate.min.js"></script> 
    <script src="<?php echo base_url(); ?>__statics/admin/js/chart.min.js"></script> 
    <script src="<?php echo base_url(); ?>__statics/admin/js/front.js"></script> 

<script src="<?php echo base_url(); ?>__statics/js/jquery.blockui.min.js"></script>
		
           <script src="<?php echo base_url(); ?>__statics/js/alertify/alertify.js"></script>
	
	<script type="text/javascript">
		  $(document).off("click",".menuklik").on("click",".menuklik",function (event, messages) {
	           event.preventDefault()
			   var url = $(this).attr("href");
			   var title = $(this).attr("title");
			  
			 
				   
				   
			   
			    $(this).parent().addClass('active').siblings().removeClass('active');
				
			  	  
			   $("#loadeusi").html("<center><img src='<?php echo base_url(); ?>__statics/img/loading.gif' class='img-responsive'></center>")
		
			  $.post(url,{ajax:"yes"},function(data){
				  
				
				  history.replaceState(title, title, url);
				  $('title').html(title);
				  $("#loadeusi").html(data);
				 
				 jQuery.unblockUI({ });
			  })
		  })
		$(function(){
          function id(v){ return document.getElementById(v); }
          function loadbar() {
            var ovrl = id("overlay"),
                prog = id("progress"),
                stat = id("progstat"),
                img = document.images,
                c = 0,
                tot = img.length;
            if(tot == 0) return doneLoading();

            function imgLoaded(){
              c += 1;
              var perc = ((100/tot*c) << 0) +"%";
              prog.style.width = perc;
              stat.innerHTML = "Loading "+ perc;
              if(c===tot) return doneLoading();
            }
            function doneLoading(){
              ovrl.style.opacity = 0;
              setTimeout(function(){ 
                ovrl.style.display = "none";
              }, 1200);
            }
            for(var i=0; i<tot; i++) {
              var tImg     = new Image();
              tImg.onload  = imgLoaded;
              tImg.onerror = imgLoaded;
              tImg.src     = img[i].src;
            }    
          }
          document.addEventListener('DOMContentLoaded', loadbar, false);
        }());
        
        
        
  
   
   function sukses2(param){
		  let timerInterval;
		  Swal.fire({
			type: 'success',
			title: 'Berhasil ',
			showConfirmButton: false,
			 html: 'Berhasil disimpan',
   
			timer: 1000,
			onBeforeOpen: () => {
			  Swal.showLoading();
			  timerInterval = setInterval(() => {
				Swal.getContent().querySelector('strong')
				  .textContent = Swal.getTimerLeft()
			  }, 100)
			},
			onClose: () => {
			  clearInterval(timerInterval)
			}
		  }).then((result) => {
			if (
			  /* Read more about handling dismissals below */
			  result.dismiss === Swal.DismissReason.timer
			  
			) {
		   
			 location.href = base_url+param;
			}
		  })
   }
   
   function gagal(param){
		  let timerInterval;
		  Swal.fire({
			type: 'warning',
			title: 'Gagal, Perhatikan ! ',
			showConfirmButton: true,
			 html: param
   
		  })
   }


		</script>
</body>


</html>