<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		$this->load->model('Auth_model');

		if(!$this->session->userdata("tmmadrasah_id")){						
			$ajax = $this->input->get_post("ajax",true);		 
				if(!empty($ajax)){
			   
				   echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
					  exit();
				 }else{
					 redirect(site_url()."ksm/login");
				 }
			
		   }
     
		
	  }

	
	public function index(){
		
		
		
		
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Data Madrasah/Sekolah";
		  $data['m']	  = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('dashboard/page_default',$data);
		 }else{
			 
			
		     $data['konten'] = "dashboard/page_default";
			 
			 $this->load->view('dashboard/page_header',$data);
		 }
		
	}
	
	public function do_login(){
		  //error_reporting(0);
		  $username = trim($this->input->get_post("username"));
		  $password = trim($this->input->get_post("password"));
		  
		 
		  
		  $username   = $this->db->escape_str($username);
		  $password   = $this->db->escape_str($password);
		  
          $pegawai = $this->db->get_where("tm_pegawai",array("username"=>$username,"password"=>sha1(md5($password))))->row();
		  
			   if(count($pegawai) >0){
						  
						  
						$session = array(
						'tmbagian_id'        => $pegawai->tmbagian_id,
						'tmpegawai_id'        => $pegawai->id,
						'tmpegawai_nama'          => $pegawai->nama,
						'tmgroup_id'   => $pegawai->tmgroup_id);
						
						$this->session->sess_expiration = '1000000';
						$this->session->set_userdata($session);
					
						echo "yes";
						 
			   }else{
				   header('Content-Type: application/json');
                   echo json_encode(array('error' => true, 'message' => "username atau password yang anda masukkan salah"));
				   
				   
				   
			   }

		
	}

	
	 
	  public function cek(){
		 
		 echo count($this->Acuan_model->get_wherearray("tm_notif","status='0'"));
		 
		 
	 }
	 
	 
	 public function get_pemberitahuan(){
		 
		 $this->db->update("tm_notif",array("status"=>1));
	
		  $pemberitahuan =  $this->Acuan_model->getlimit(array("table"=>"tm_notif","order"=>"tanggal","by"=>"desc","limit"=>20),null)->result();
		    if(count($pemberitahuan) >0){
				  foreach($pemberitahuan as $row){
					?>
		                          <li>
									<a href="javascript:void(0)">
									
									<span class="subject">
									
									<span class="time"><?php echo $this->Acuan_model->timeAgo($row->tanggal); ?> </span>
									</span>
									<span class="pesan">
									 <?php echo $row->keterangan; ?> </span>
									</a>
								</li>
					<?php
				  }					
			}else{
				
					?>
		                         <li>
									<a href="#">
									
									<span class="subject">
									
									<span class="time"> </span>
									</span>
									<span class="pesan">
									 Tidak ada pemberitahuan </span>
									</a>
								</li>
					<?php 
				
			}
	 }
	 
	  public function tvinformasi(){
		  
		  $this->load->view("tvinformasi");
	 }
	 
	 	public function statistik(){
		
		
		
		 if(!$this->session->userdata("tmpegawai_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url()."'>disini </a></center>";
               exit();
		  }
	   
		 	$data = array();  
	   
						
							
								$grid = array();
								$data['categorie_xAxis'] = "";
								$data['json_anggota']    = "";
								$pie   					 = "";
								$data['title']           = "Persentase Jumlah Permadrasah ";
								$bagian  = $this->Dendi->query("select * from tm_bagian order by nama asc")->result();
									
									 foreach($bagian as $index=>$row){
									   
									   
										$pm = $this->Dendi->jumlahdata("tm_kegiatan",array("tmbagian_id"=>$row->id));
										
										$data['json_anggota'] .=",".$pm;
										$data["categorie_xAxis"] .=",'".$row->kode."";
										$tempo = array("INDEXES"=>$row->kode,"nama"=>$row->nama,"Jumlah"=>$pm,"rata"=>$pm/count($bagian));
										
										$pie          .=",['".$row->nama."',".(($pm/count($bagian))*100)."]";
										
										
									   $grid[] = $tempo;
									   
									 }
								
								
									 $data['statistik'] = " Grafik Kegiatan  ";
									 $data['header']    = $data['statistik'];
									 $data['categorie_xAxis'] = " Grafik Kegiatan  ";

									$data['json_pie_chart']  =  substr($pie,1);
									$data['json_anggota']    = substr($data['json_anggota'], 1);
									
									$data['grid'] = $grid;

									
         $ajax = $this->input->get_post("ajax",true);	
		 
	     if(!empty($ajax)){
			 $data['title']  = "Statistik Kegiatan KSKK";		    
			 $this->load->view('page_statistik',$data);
		 }else{
			 
			 $data['title']  = "Statistik Kegiatan KSKK";
		     $data['konten'] = "page_statistik";
			 
			 $this->load->view('home/page_header',$data);
		 }
	
		
	}
	 public function logout(){
		     $this->session->sess_destroy();
			echo "sukses";
		 
	 }
	 
	 
	 
	 public function dendikota(){
		 
		 $data = $this->db->query("select * from kota")->result();
		   foreach($data as $d){
			   
			   $username = "KABKO-".$d->id;
			   $this->db->set("username",$username);
			   $this->db->set("password",'123456');
			   $this->db->where("id",$d->id);
			   $this->db->update("kota");
			   
		   }
		 
	 }
	 
	 public function dendiprov(){
		 
		 $data = $this->db->query("select * from provinsi")->result();
		   foreach($data as $d){
			   
			   $username = "PROV-".$d->id;
			   $this->db->set("username",$username);
			 $this->db->set("password",sha1(md5(123456)));
			   $this->db->where("id",$d->id);
			   $this->db->update("provinsi");
			   
		   }
		 
	 }
	
}
