<?php
ini_set('upload_max_filesize', '1000M');
ini_set('post_max_size', '1000M');
ini_set('max_input_time', 3000000);
ini_set('max_execution_time', 3000000);

use setasign\Fpdi\Fpdi;

require_once(APPPATH.'libraries/fpdf/fpdf.php');
require_once(APPPATH.'libraries/fpdi/src/autoload.php');


defined('BASEPATH') OR exit('No direct script access allowed');
define('PHPWORD_BASE_DIR', realpath(__DIR__));
class Puslokal extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		$this->load->model('M_lokal');
		 $this->load->helper('exportpdf_helper');  
		 if(!$this->session->userdata("tmadmin_id")){						
			$ajax = $this->input->get_post("ajax",true);		 
				if(!empty($ajax)){
			   
				   echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
					  exit();
				 }else{
					 redirect(site_url()."ksm/login");
				 }
			
		   }
	   
		
	  }
     public function laporan(){
		
		
		
		
		
									
          $ajax = $this->input->get_post("ajax",true);	
		  $data['title']           = "Laporan Pendaftaran ";
		  $data['dt']              = $this->Di->get_where("provinsi",array("id"=>$_SESSION['tmadmin_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('laporan',$data);
		 }else{
			 
			
		     $data['konten'] = "laporan";
			 
			 $this->load->view('pusdashboard/page_header',$data);
		 }
	
		
	}

	public function jadwal(){
		
		
		
		
		
									
		$ajax = $this->input->get_post("ajax",true);	
		$data['title']           = "Jadwal KSM  ";
		
	   if(!empty($ajax)){
					  
		   $this->load->view('jadwal',$data);
	   }else{
		   
		  
		   $data['konten'] = "jadwal";
		   
		   $this->load->view('pusdashboard/page_header',$data);
	   }
  
	  
  }

  public function lokasi(){
		
		
	
   
							   
	$ajax = $this->input->get_post("ajax",true);	
	 $data['title']  = "Lokasi Tes";
	
	if(!empty($ajax)){
				   
		$this->load->view('lokasi/page',$data);
	}else{
		
	   
		$data['konten'] = "lokasi/page";
		
		$this->load->view('pusdashboard/page_header',$data);
	}

   
}


public function g_lokasi(){
   
   $iTotalRecords = $this->M_lokal->g_lokasi(false)->num_rows();
   
   $iDisplayLength = intval($_REQUEST['length']);
   $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
   $iDisplayStart = intval($_REQUEST['start']);
   $sEcho = intval($_REQUEST['draw']);
   
   $records = array();
   $records["data"] = array(); 

   $end = $iDisplayStart + $iDisplayLength;
   $end = $end > $iTotalRecords ? $iTotalRecords : $end;
   
   $datagrid = $this->M_lokal->g_lokasi(true)->result_array();
   $jenjangArr = array("1"=>"MI/SD","2"=>"MTs/SMP","3"=>"MA/SMA");
	$i= ($iDisplayStart +1);
	foreach($datagrid as $val) {
									   
		 $pemetaan =  explode(",",$val['pemetaan']);
		  $vpemetaan ="<ol>";
		   foreach($pemetaan as $rp){
			   $vpemetaan .="<li>".$this->Di->get_kondisi(array("id"=>$rp),"kecamatan","nama")."</li>";
		   }	
		   $vpemetaan .="</ol>";
		   
		   $jenjang =  explode(",",$val['jenjang']);
		  $vjenjang ="<ol>";
		   foreach($jenjang as $rp){
			   $vjenjang .="<li>".$jenjangArr[$rp]."</li>";
		   }	
		   $vjenjang .="</ol>";

		 $jmlPeserta = $this->db->query("SELECT count(id) as jml from tm_siswa where  lokasi_kabko='".$val['id']."'")->row();
							
		 $no = $i++;
		 $records["data"][] = array(
			 $no,
			
			 $this->Di->get_kondisi(array("id"=>$val['provinsi']),"provinsi","nama"),
			 $this->Di->get_kondisi(array("id"=>$val['kota']),"kota","nama"),
			
			 $val['nama'],
			 $val['alamat'],
			 $val['kuota'],
			 $vpemetaan,
			 $vjenjang,
			 $jmlPeserta->jml
			
			 
			 
								 
					 
		 
			

		   );
	   }
 
   $records["draw"] = $sEcho;
   $records["recordsTotal"] = $iTotalRecords;
   $records["recordsFiltered"] = $iTotalRecords;
   
   echo json_encode($records);
}
	
	
	public function madrasah(){
		
		
		
		
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']           = "Data Madrasah/Sekolah Peserta KSM di Provinsi Anda ";
		  $data['dt']              = $this->Di->get_where("provinsi",array("id"=>$_SESSION['tmadmin_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('madrasah/page',$data);
		 }else{
			 
			
		     $data['konten'] = "madrasah/page";
			 
			 $this->load->view('pusdashboard/page_header',$data);
		 }
	
		
	}
	
	
	public function g_madrasah(){
		
		  $iTotalRecords = $this->M_lokal->m_madrasah(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->M_lokal->m_madrasah(true)->result_array();
		   $arjenis = array("1"=>"Madrasah","2"=>"Sekolah");
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
						   
			$draft           = $this->db->query("select count(id) as jml from tm_siswa where tmmadrasah_id='{$val['id']}' and status=0")->row();;                
			$terkirim		 = $this->db->query("select count(id) as jml from tm_siswa where tmmadrasah_id='{$val['id']}' and status=1")->row();;                
			$lulus   		 = $this->db->query("select count(id) as jml from tm_siswa where tmmadrasah_id='{$val['id']}' and status=2")->row();;                
			$tidak_lulus     = $this->db->query("select count(id) as jml from tm_siswa where tmmadrasah_id='{$val['id']}' and status=3")->row();;                
				 
					
						   if($val['sts']=="1"){
					  
				
					     $tombol ='	<a class="btn btn-danger btn-sm  kelaaktifkan " disabled style="color:white;cursor:pointer" urlnya = "'.site_url("puslokal/a_madrasah").'"  datanya="'.$val['id'].'" title="Aktifkan">
					           Nonaktif </a>';
					  
				  }else{
				
					    $tombol ='	<a class="btn btn-success btn-sm  kelanonaktifkan " disabled style="color:white;cursor:pointer" urlnya = "'.site_url("puslokal/n_madrasah").'"  datanya="'.$val['id'].'" title="Nonaktifkan">
					Aktif</a>';
					  
				  }
				$no = $i++;
				$records["data"][] = array(
					$no,
					'
					<a class="btn btn-info  ubah" urlnya = "'.site_url("puslokal/f_madrasah").'"   datanya="'.$val['id'].'" title="Ubah Data">
					<i class=" fa fa-pencil-square  " style="color:white"></i>
				
				 
					 ',		
					$arjenis[$val['jenis']],
					$this->Di->jenjangnama($val['jenjang']),
					$val['username'],
					$val['password'],
					$val['nama'],
					$this->Di->get_kondisi(array("id"=>$val['kota']),"kota","nama"),
					$val['telepon'],
					$val['email'],
					
					$draft->jml,
					$terkirim->jml,
					$lulus->jml,
					$tidak_lulus->jml,
				
					$val['delegasi_nik'],
					$val['delegasi_nama'],
					$val['delegasi_hp'],
					$tombol,
				
					
					
					
					
                   			
										
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	
	public function f_madrasah(){
		
		 $id = $this->input->get_post("id",TRUE);
		 
		 $data = array();
		    if(!empty($id)){
				
				$data['dataform'] = $this->Di->get_where("tm_madrasah",array("id"=>$id));
			}
	 
		 $data['title']  = "Form ";
		 $this->load->view('madrasah/form',$data);
	}
	
	
	public function s_madrasah(){
     
        $this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
        
        $this->form_validation->set_message('min_length', '{field} Minimal 6 Karakter .');
        $this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
        $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , mungkin anda sudah pernah melakukan pendaftaran, silahkan klik menu login .');
				 
		 
			
				$config = array(
				    array('field' => 'password', 'label' => 'Password Baru  ', 'rules' => 'trim|required'),
				  
				 
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			  
			  
			   $this->db->set("password",(($_POST['password'])));
			   $this->db->where("id",$_POST['id']);
			   $this->db->update("tm_madrasah");
			   $this->Di->log($_SESSION['nama']. "Merubah Password Madrasah","k");
			   echo "sukses";
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }	
	
	
	}
	
	
	
	public function n_madrasah(){
		
		$this->db->set("sts",1);
		$this->db->where("id",$_POST['id']);
		$this->db->update("tm_madrasah");
		echo "sukses";
		
	}
	public function a_madrasah(){
		
		$this->db->set("sts",0);
		$this->db->where("id",$_POST['id']);
		$this->db->update("tm_madrasah");
		echo "sukses";
		
	}
	
	
	public function peserta(){
		
		
		
		 if(!$this->session->userdata("tmadmin_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
               exit();
		  }
	   
		  $arje = array("1"=>"MI/SD Sederajat","2"=>"MTS/SMP Sederajat","3"=>"MA/SMA Sederajat");
									
           $ajax            = $this->input->get_post("ajax",true);	
           $data['jenjang'] = base64_decode($this->input->get_post("jenjang",true));	
		   $data['title']   = "Peserta KSM";
		   $data['dt']              = $this->Di->get_where("provinsi",array("id"=>$_SESSION['tmadmin_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('page_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "page_peserta";
			 
			 $this->load->view('pusdashboard/page_header',$data);
		 }
	
		
	}
	
	
	public function g_peserta(){
		  error_reporting(0);
		  $iTotalRecords = $this->M_lokal->m_peserta(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->M_lokal->m_peserta(true)->result_array();
		  $arjenis = array("1"=>"Madrasah","2"=>"Sekolah");
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
				                              
												 
				if($val['status']==0){
					
					$status ='<i class="badge badge-default"> Draft (belum dikirim) </i>';
					
				 }else if($val['status']==1){
					 
				    $status ='<button class="btn btn-primary   btn-sm ubah" urlnya = "'.site_url("puslokal/verval").'"  datanya="'.$val['id'].'"  style"cursor:pointer"> <i class="fa fa-check-square"></i> Belum Diverifikasi   </button>';
					 
				 }else if($val['status']==2){
					  $status ='<button class="btn btn-success btn-sm ubah" urlnya = "'.site_url("puslokal/verval").'"  datanya="'.$val['id'].'"  style"cursor:pointer"> <i class="fa fa-check-square"></i> Lulus  </button>';
					
					}else if($val['status']==3){
						$status ='<button class="btn btn-danger btn-sm ubah" urlnya = "'.site_url("puslokal/verval").'"  datanya="'.$val['id'].'"  style"cursor:pointer"><i class="fa fa-times"></i> Tidak Lulus </button>';
					  
				   }

				$foto ="img/ksm.png";
				 if(!empty($val['foto'])){

					$foto ="upload/{$val['foto']}";
				 }
				 $img ='<img src="https://ksm.kemenag.go.id/__statics/'.$foto.'" class="img-responsive img-circle" style="width:40px">';
												 
				$no = $i++;
				$notest = "<a href='".site_url("puslokal/kartutes?id=".($val['id'])."")."' target='_blank' >".$val['no_test']."</a>";
				$records["data"][] = array(
					$no,
					$this->Di->get_kondisi(array("id"=>$val['provinsi_madrasah']),"provinsi","nama"),
					$this->Di->get_kondisi(array("id"=>$val['kota_madrasah']),"kota","nama"),
					$notest,
					$status,
					$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
					//$img,
					
					$val['nisn'],
					'<a href="#" urlnya = "'.site_url("puslokal/f_peserta").'" class="" datanya="'.$val['id'].'">'.$val['nama'].'</a>',					
					$val['gender'],
					$val['tempat'].",".$this->Di->formattanggalstring($val['tgl_lahir']),
					
					$arjenis[$val['jenis']],
					$this->Di->jenjangnama($val['jenjang']),
					$val['username'],
					$val['madrasah'],
					$val['delegasi_hp'],
					
					
					
										
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	
	public function verval(){
		
		 $id = $this->input->get_post("id",TRUE);
		 
		 $data = array();
		
				
		 $data['dataform'] = $this->db->query("select * from v_siswa where id='".$id."'")->row();
			
	     $data['m']	             = $this->Di->get_where("provinsi",array("id"=>$_SESSION['tmadmin_id']));
	     $data['madrasah']	     = $this->Di->get_where("tm_madrasah",array("id"=>$data['dataform']->tmmadrasah_id));
		 $data['title']  = "Verifikasi Data ".$data['dataform']->nama;
		 $this->load->view('verifikasi',$data);
	}
	
	
	public function save_verval(){
		$nama_siswa = $this->Di->get_kondisi(array("id"=>$this->input->get_post("tmsiswa_id")),"tm_siswa","nama");
		$status     = $this->input->get_post("status");

		$this->db->set("status",$this->input->get_post("status"));
		$this->db->where("id",$this->input->get_post("tmsiswa_id"));
		$this->db->update("tm_siswa");
		$this->Di->log($_SESSION['nama']. "Melakukan Verifikasi {$nama_siswa} dengan hasil {$status}","k");
		echo "sukses";
		
	}
	
	public function save_keterangan(){
		
		$this->db->set("keterangan",$this->input->get_post("keterangan"));
		$this->db->where("id",$this->input->get_post("tmsiswa_id"));
		$this->db->update("tm_siswa");
		echo "sukses";
		
	}
	
	
	public function f_peserta(){
		
		$id = $this->input->get_post("id",TRUE);
		
		$data = array();
		   if(!empty($id)){
			   
			   $data['dataform'] = $this->Di->get_where("tm_siswa",array("id"=>$id));
		   }
	  
		$data['title']  = "Perbaiki Data Peserta KSM";
		$this->load->view('form_peserta',$data);
   }
   
//    public function simpanpeserta(){
	   
// 	   error_reporting(0);
	   
// 		$this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
	   
		
		   
// 			   $config = array(
// 				 //  array('field' => 'f[nisn]', 'label' => 'NISN ', 'rules' => 'trim|required'),
// 				   array('field' => 'f[nama]', 'label' => 'Nama Siswa  ', 'rules' => 'trim|required'),
// 				   array('field' => 'f[gender]', 'label' => 'Gender  ', 'rules' => 'trim|required'),
			   
// 				   array('field' => 'f[tempat]', 'label' => 'Tempat Lahir  ', 'rules' => 'trim|required'),
// 				   array('field' => 'f[tgl_lahir]', 'label' => 'Tanggal Lahir  ', 'rules' => 'trim|required'),
				 
				
   
				   
// 			   );
			   
// 			   $this->form_validation->set_rules($config);	
	   
// 		  if ($this->form_validation->run() == true) {
			 
	   
	   
			   
// 				 $f  = $this->input->get_post("f");
				
// 			      if(!empty($_FILES['file']['name'])){
// 						   $config['upload_path'] = './__statics/upload';
// 						   $folder   = '__statics/upload';
// 						   $config['allowed_types'] = "*"; 	
// 						   $config['overwrite'] = true; 	
// 						   $filenamepecah			 = explode(".",$_FILES['file']['name']);
// 						   $imagenew				 = $peserta_id.$persyaratan_id.".".pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
						   
// 						   $config['file_name'] = $imagenew;
						   
// 						   $this->load->library('upload', $config);
		
// 								   if ( ! $this->upload->do_upload('file'))
// 								   {
// 									$this->db->where("id",$id);
// 									$this->db->update("tm_siswa",$f);
									   
								   
// 								   }else{
// 									$id = $this->input->get_post("id",true);
// 									$siswanya = $this->db->get_where("v_siswa",array("id"=>$id))->row();
		
// 											$madrasah = $this->db->get_where("tm_madrasah",array("id"=>$siswanya->tmmadrasah_id))->row();
		
					
				
// 											$service = new Google_Drive();
// 											$foldernama = $madrasah->username;
// 											$foldernama = str_replace("/",DIRECTORY_SEPARATOR,$foldernama);
// 											$folderId   = $madrasah->folder;
// 											if(!$folderId){
// 												$folderId = $service->getFileIdByName( BACKUP_FOLDER );
// 												if( !$folderId ) {
// 													$folderId = $service->createFolder( BACKUP_FOLDER );					
// 												}
// 												$folderId = $service->createMultiFolder($foldernama,$folderId);
// 												$this->db->set("folder",$folderId);
												
// 												$this->db->where("id",$siswanya->tmmadrasah_id);
// 												$this->db->update("tm_madrasah");
// 											}
		
// 										   $fileId = $service->createFileFromPath( $_FILES["file"]['tmp_name'], $_FILES["file"]['name'], $folderId );
// 										   $service->setPublic($fileId);
		
// 										   if (empty($fileId)) {
// 											header('Content-Type: application/json');
// 											echo json_encode(array('error' => true, 'message' =>"Gagal"));
											   
// 										   }else{
		
		
																			
// 													$this->db->where("id",$id);
// 													$this->db->update("tm_siswa",$f);
		
// 												     $this->db->query("update tm_siswa set foto='".$imagenew."' where id='".$id."'");
		
		
		
// 										   }
		
		
		
		
		
// 								   }


// 								}else{

// 									$this->db->where("id",$id);
// 									$this->db->update("tm_siswa",$f);


// 								}


						  

// 						   echo "sukses";
					   
								   
								   
				 
					   
					   
// 			} else {
			
		  
// 			   header('Content-Type: application/json');
// 			   echo json_encode(array('error' => true, 'message' => validation_errors()));
		   
// 			}	
   
	   
//    }

   public function mutasipeserta(){
	   
	error_reporting(0);
	
	 $this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
	
	 
		
			$config = array(
			  //  array('field' => 'f[nisn]', 'label' => 'NISN ', 'rules' => 'trim|required'),
				array('field' => 'kota_tujuan', 'label' => 'Kabkota Tujuan  ', 'rules' => 'trim|required'),
				
			 

				
			);
			
			$this->form_validation->set_rules($config);	
	
	   if ($this->form_validation->run() == true) {
		  
						$kota_tujuan = $this->input->get_post("kota_tujuan");
	
			
						$siswa  = $this->db->get_where("v_siswa",array("id"=>$_POST['tmsiswa_id']))->row();

						$this->db->set("tmsiswa_id",$siswa->id);
						$this->db->set("kota_asal",$siswa->kota_madrasah);
						$this->db->set("kota_tujuan",$kota_tujuan);						
						$this->db->insert("mutasi");


			 
						$this->db->set("kota",$kota_tujuan);
						$this->db->where("id",$siswa->tmmadrasah_id);
						$this->db->update("tm_madrasah");
							
												
						$this->db->set("kota",$kota_tujuan);
						$this->db->where("id",$siswa->id);
						$this->db->update("tm_siswa");

						echo "sukses";
					
								
								
			  
					
					
		 } else {
		 
	   
			header('Content-Type: application/json');
			echo json_encode(array('error' => true, 'message' => validation_errors()));
		
		 }	

	
}
	
	public function status_peserta(){
		
		$this->db->set("status",$_POST['status']);
		$this->db->where("id",$_POST['tmsiswa_id']);
		$this->db->update("tm_siswa");
		echo "sukses";
	}
	
	
	
	public function pesertates(){
		
		
		
		 if(!$this->session->userdata("tmadmin_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
               exit();
		  }
	   
		
									
           $ajax = $this->input->get_post("ajax",true);	
		   $data['title']  = "Peserta Tes";
		   $data['dt']              = $this->Di->get_where("provinsi",array("id"=>$_SESSION['tmadmin_id']));
		    $data['jenjang'] = base64_decode($this->input->get_post("jenjang",true));
	     if(!empty($ajax)){
					    
			 $this->load->view('peserta/page_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "peserta/page_peserta";
			 
			 $this->load->view('pusdashboard/page_header',$data);
		 }
	
		
	}
	
	
	public function g_pesertates(){
		
		  $iTotalRecords = $this->M_lokal->m_pesertates(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->M_lokal->m_pesertates(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
				$madrasah = $this->db->query("select npsn,nama from tm_madrasah where id='".$val['tmmadrasah_id']."'")->row();             
			    $notest = "<a href='".site_url("puslokal/kartutes?id=".base64_encode($val['id'])."")."' target='_blank' >".$val['no_test']."</a>";	

				
				$no = $i++;
				$records["data"][] = array(
					$no,
				$notest,
					
					strtoupper($val['nama']),
					$val['gender'],
					$madrasah->npsn,
				
					
				    strtoupper($this->Di->get_kondisi(array("id"=>$val['tmmadrasah_id']),"tm_madrasah","nama")),
					$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama")
					
				
					
					
                    				
										
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
		
	public function hasiltes(){
		
		
		
		
		
									
          $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Hasil Tes  ";
		  $data['dt']              = $this->Di->get_where("provinsi",array("id"=>$_SESSION['tmadmin_id']));
		  $data['jenjang'] = base64_decode($this->input->get_post("jenjang",true));
	     if(!empty($ajax)){
					    
			 $this->load->view('hasiltes/page_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "hasiltes/page_peserta";
			 
			 $this->load->view('pusdashboard/page_header',$data);
		 }
	
		
	}
	
	public function g_hasiltes(){
		
		  $iTotalRecords = $this->M_lokal->m_pesertateshasil(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->M_lokal->m_pesertateshasil(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
				                            
			    
				 $notest = $val['no_test'];	
				$no = $i++;
				$records["data"][] = array(
					$no,
					$notest,
					strtoupper($val['nama']),					
					$this->Di->formattanggalstring($val['tgl_lahir']),					
					$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
					'<input type="text" class="form-control nilai" status="benar" tmsiswa_id="'.$val['id'].'" style="width:30px" value="'.$val['benar'].'">',
					'<input type="text" class="form-control nilai" status="salah" tmsiswa_id="'.$val['id'].'" style="width:30px" value="'.$val['salah'].'">',
					'<input type="text" class="form-control nilai" status="kosong" tmsiswa_id="'.$val['id'].'" style="width:30px" value="'.$val['kosong'].'">',
					'<span id="nilaiakhir'.$val['id'].'">'.$val['nilai'].'</span>',
					 "Jml Input = ".($val['benar']+$val['salah']+$val['kosong'])." <br> Jml Soal = ".$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","soal")
				
					
				
					
					
                    				
										
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	public function nilaites(){
		$tmsiswa_id = $this->input->get_post("tmsiswa_id");
		$jumlah     = $this->input->get_post("jumlah");
		$status     = $this->input->get_post("status");
		  
		    $this->db->set($status,$jumlah);
		    $this->db->where("id",$tmsiswa_id);
		    $this->db->update("tm_siswa");
		   
		   $datanya = $this->db->query("select id,benar,salah,kosong from  tm_siswa where id ='".$tmsiswa_id."'")->row();
		   $benar = ($datanya->benar * 5);
		   $salah  = ($datanya->salah * -2);
		   $kosong  = ($datanya->kosong * -1);
		   
		    $nilai   = ($benar + $salah +$kosong);
		    $this->db->set("nilai",$nilai);
		    $this->db->where("id",$tmsiswa_id);
		    $this->db->update("tm_siswa");
			
			echo $nilai;
		   
		  
		
		
	}
	
	public function sertifikat(){
		
		
		
		
		
									
          $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Sertifikat  ";
		  $data['dt']              = $this->Di->get_where("provinsi",array("id"=>$_SESSION['tmadmin_id']));
		  $data['jenjang'] = base64_decode($this->input->get_post("jenjang",true));
	     if(!empty($ajax)){
					    
			 $this->load->view('sertifikat/page_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "sertifikat/page_peserta";
			 
			 $this->load->view('pusdashboard/page_header',$data);
		 }
	
		
	}
	
	public function g_sertifikat(){
		
		  $iTotalRecords = $this->M_lokal->m_pesertasertifikat(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->M_lokal->m_pesertasertifikat(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
				                            
			    $notest = $val['no_test'];	
				
				$no = $i++;
				$statusarray= array("1"=>"Juara 1","2"=>"Juara II","3"=>"Juara III","4"=>"Harapan I","5"=>"Harapan II","6"=>"Harapan III");
				    if($no < 7){
						 if($no <4){
							 $status = "<span class='btn btn-success btn-sm'>".$statusarray[$no]."</span>";
						 }else{
							 
							$status = "<span class='btn btn-warning btn-sm' style='color:white'>".$statusarray[$no]."</span>";
						 }
						 
						 
						
					}else{
						
						$status = "<span class='btn btn-danger btn-sm' style='color:white'>Peserta</span>";
					}
					
					
				$records["data"][] = array(
					$no,
					'<a class="btn btn-danger " href="'.site_url("puslokal/cetaksertifikat?tmsiswa_id=".base64_encode($val['id'])."&urutan=".base64_encode($no)."&display=".base64_encode($iDisplayStart)."").'"> <i class="fa fa-file-word-o"></i> PIAGAM </a>',
					//''.
					$notest,
					($val['nama']),
					$status,
					$this->Di->formattanggalstring($val['tgl_lahir']),
                    $this->Di->get_kondisi(array("id"=>$val['tmmadrasah_id']),"tm_madrasah","nama"),					
					$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
					(!empty($val['benar'])) ? $val['benar'] : 0,
					(!empty($val['salah'])) ? $val['salah'] : 0,
					(!empty($val['kosong'])) ? $val['kosong'] : 0,	
					(!empty($val['nilai'])) ? $val['nilai'] : 0,
					 "Jml Input = ".($val['benar']+$val['salah']+$val['kosong'])." <br> Jml Soal = ".$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","soal")
				
					
				
				
					
					
                    				
										
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	
	public function cetaksertifikat(){
		header ("Content-type: text/html; charset=utf-8");
		
		$this->load->library('PHPWord');
		$this->load->library('ciqrcode');
        $this->load->helper('download');
		
		$PHPWord = new PHPWord();	
		$document 		   = $PHPWord->loadTemplate('piagam.docx');
		
		$tmsiswa_id	  			   = base64_decode($this->input->get_post("tmsiswa_id",TRUE));
		$urutan  	  			   = base64_decode($this->input->get_post("urutan",TRUE));
		$display  	  			   = base64_decode($this->input->get_post("display",TRUE));

		
		$datasiswa = $this->db->query("select tmmadrasah_id,provinsi,trkompetisi_id,id,nama from tm_siswa where id='".$tmsiswa_id."'")->row();
		
		  if(count($datasiswa) >0){
			  
			  
			  
			  
			  
		$sekolah   = $this->Di->get_kondisi(array("id"=>$datasiswa->tmmadrasah_id),"tm_madrasah","nama");
		$kabupaten = $this->Di->get_kondisi(array("id"=>$datasiswa->provinsi),"provinsi","nama");
		$kompetisi = $this->Di->get_kondisi(array("id"=>$datasiswa->trkompetisi_id),"tr_kompetisi","nama");
		$kepala = $this->Di->get_kondisi(array("id"=>$datasiswa->provinsi),"provinsi","kepala");
		$jabatan = $this->Di->get_kondisi(array("id"=>$datasiswa->provinsi),"provinsi","ketkepala");
		   
		   
		   
		   $this->load->library('ciqrcode'); //pemanggilan library QR CODE

		$config['cacheable']	= true; 
		$config['cachedir']		= './piagam/chache'; 
		$config['errorlog']		= './piagam/'; 
		$config['imagedir']		= './piagam/'; 
		$config['quality']		= true; 
		$config['size']			= '1024'; 
		$config['black']		= array(224,255,255);
		$config['white']		= array(70,130,180); 
		$this->ciqrcode->initialize($config);

		$image_name=$datasiswa->id.'.png'; 

		$params['data'] = "http://ksm.kemenag.go.id/publik/piagam?token=".base64_encode($datasiswa->id).""; 
		$params['level'] = 'H'; 
		$params['size'] = 10;
		$params['savename'] = FCPATH.$config['imagedir'].$image_name; 
		$this->ciqrcode->generate($params);
		
		
		
		   
		
		  if($urutan < 7){
			  if($display==0){
			  $ura = array("1"=>"JUARA I","2"=>"JUARA II","3"=>"JUARA III","4"=>"HARAPAN I","5"=>"HARAPAN II","6"=>"HARAPAN III");
			  
			  $urutan = $ura[$urutan];
			  }else{
				  
				 $urutan = "PESERTA ";  
			  }
		  }else{
			  $urutan = "PESERTA "; 
		  }
		
		$document->setValue('{NAMAPESERTA}', strtoupper($datasiswa->nama));
		$document->setValue('{NAMASEKOLAH}', strtoupper($sekolah));
		$document->setValue('{STATUSPIAGAM}', $urutan);
		
		$document->setValue('{KOMPETISI}', strtoupper($kompetisi));
		$document->setValue('{kabupaten2}', ucwords(strtolower($kabupaten)));
		$document->setValue('{kepala}', (strtoupper($kepala)));
		$document->setValue('{jabatan}', (($jabatan)));
		$document->setValue('{kabupaten}', $kabupaten);
		$document->setValue('{tgl}', str_replace(array("Kabupaten","provinsi"),"",ucwords(strtolower($kabupaten))).", 20 Juli 2019");
		
	    $brc      = FCPATH."/piagam/".$image_name;
	    $document->setImageValue('image4.png', $brc);
		$namafile = str_replace(array(" ","-","'",",","."),"",$datasiswa->nama).'.docx';
		$tmp_file = 'piagam/'.$namafile.'';
		$document->save($tmp_file);
		 
    
       // $pth    =   file_get_contents($_SERVER["DOCUMENT_ROOT"]."/piagam/".$namafile."");
	
		//unlink(FCPATH."piagam/".$namafile);
		//force_download($namafile, $pth); 
		
		 force_download('piagam/'.$namafile.'',NULL); 
		
		  }
		
	}
	
	
		public function disposisi(){
		
		
		
		
		
									
          $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Diposisi Peserta  ";
		  $data['dt']              = $this->Di->get_where("provinsi",array("id"=>$_SESSION['tmadmin_id']));
		  $data['jenjang'] = base64_decode($this->input->get_post("jenjang",true));
	     if(!empty($ajax)){
					    
			 $this->load->view('disposisi/page_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "disposisi/page_peserta";
			 
			 $this->load->view('pusdashboard/page_header',$data);
		 }
	
		
	}
	
	public function g_disposisi(){
		
		  $iTotalRecords = $this->M_lokal->m_pesertasertifikat(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->M_lokal->m_pesertasertifikat(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
				                            
			    
				 $no = $i++;
				$statusarray= array("1"=>"Juara 1","2"=>"Juara II","3"=>"Juara III","4"=>"Harapan I","5"=>"Harapan II","6"=>"Harapan III");
				    if($no < 7){
						 if($no <4){
							 $status = "<span class='btn btn-success btn-sm'>".$statusarray[$no]."</span>";
						 }else{
							 
							$status = "<span class='btn btn-warning btn-sm' style='color:white'>".$statusarray[$no]."</span>";
						 }
						 
						 $button ='<button class="btn btn-info   btn-sm disposisi" data-toggle="modal" data-target="#myModal"   datanya="'.$val['id'].'" status="'.$statusarray[$no].'"  style"cursor:pointer"> <span class="fa fa-plane"></span> Disposisi  </button>';
				   
						
					}else{
						
						$button ='<button class="btn btn-default   btn-sm " disabled  > Tidak Tersedia </button>';
				   
						$status = "<span class='btn btn-danger btn-sm' style='color:white'>Peserta</span>";
					}
					
					if($val['status2']==1){
						
						$button ='<button class="btn btn-danger   btn-sm " disabled  > Terdisposisi </button>';
				   
					}
				
				$records["data"][] = array(
					$no,
					
						$button,
					$val['no_test'],
					($val['nama']),
					 $status,
					$this->Di->get_kondisi(array("id"=>$val['tmmadrasah_id']),"tm_madrasah","nama"),
                    $this->Di->formattanggalstring($val['tgl_lahir']),			
					$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
					(!empty($val['benar'])) ? $val['benar'] : 0,
					(!empty($val['salah'])) ? $val['salah'] : 0,
					(!empty($val['kosong'])) ? $val['kosong'] : 0,	
					(!empty($val['nilai'])) ? $val['nilai'] : 0,
					 "Jml Input = ".($val['benar']+$val['salah']+$val['kosong'])." <br> Jml Soal = ".$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","soal")
				
					
				
					
					
                    				
										
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	public function formdisposisikan(){
		
		$id = $_POST['id'];
		$status = $_POST['status'];
		$data = $this->db->query("select id,nama,trkompetisi_id from tm_siswa where id='".$id."' ")->row();
		?>
		  
		  
		     <div class="row">
							
									<div class="col-md-12">
										
										<div class="alert alert-info">
										 Anda akan mendisposisikan Peserta Atas Nama <b style="color:yellow"><i> <?php echo $data->nama; ?> ( <?php echo $status; ?> <?php echo $this->Di->get_kondisi(array("id"=>$data->trkompetisi_id),"tr_kompetisi","nama"); ?> ) </i></b>
										 Untuk mengikuti KSM Tingkat Provinsi, Berikan Keterangan dibawah ini untuk dibaca oleh Komite Provinsi dan Komite Pusat, Alasan Anda mendisposisikan peserta ini </br>
										  <input type="hidden" id="id_disposisi" value="<?php echo $data->id; ?>">
										  <input type="hidden" id="statuskabko" value="<?php echo $status; ?>">

										</div>
										
										
										<div class="form-group">
											<label for="name">Keterangan  </label>
											<textarea class="form-control" id="ketkabko" cols="50" rows="3" placeholder="Contoh : Peserta ini adalah Juara 1 di provinsi kami atau Juara 1 - 3 dari Madrasah yang sama sehingga kami mendisposisi Peserta ini "></textarea>
										</div>
										
										<div class="form-group" id="aksian">
										  <center>
										  <button class="btn btn-info   btn-sm" id="hayudisposisikan">  <span class="fa fa-plane"></span> Lakukan Disposisi  </button>
									      </center>
									   </div>
									
									</div>
									
									
			</div>
		
		
		
		
		
		
		<?php 
		
	}
	
	public function disposisikuy(){
		
		$id_disposisi = $_POST['id_disposisi'];
		$statuskabko  = $_POST['statuskabko'];
		$ketkabko     = $_POST['ketkabko'];
		$datasiswa    = $this->db->query("select trkompetisi_id,tmmadrasah_id,benar,salah,kosong from tm_siswa where id='".$id_disposisi."'")->row();
		
		$kompetisi    = $datasiswa->trkompetisi_id;
		$tmmadrasah_id= $datasiswa->tmmadrasah_id;
		 $jml_input    = ($datasiswa->benar+$datasiswa->salah+$datasiswa->kosong); 
		 $jml_soal     = $this->Di->get_kondisi(array("id"=>$kompetisi),"tr_kompetisi","soal"); 
		  //echo "Belum bisa disposisi, Tunggu 15 Menit"; exit();
		  if($jml_input == $jml_soal){
		   $cek  = $this->db->query("select count(id) as jml from tm_siswa where trkompetisi_id='".$kompetisi."' and tmmadrasah_id='".$tmmadrasah_id."' and status2='1'")->row();
		   $cek2 = $this->db->query("select count(id) as jml from tm_siswa where trkompetisi_id='".$kompetisi."' and status2='1' and tmmadrasah_id in (select id from tm_madrasah where provinsi='".$_SESSION['tmadmin_id']."')")->row();
		     
			 // echo $cek2->jml; exit();
			  if($cek->jml > 1){
				  echo "Disposisi Tidak dapat dilakukan, dikarenakan sudah ada 2 perwakilan di Bidang Studi dari Lembaga ini, Silahkan Disposisi Peserta lain  ";
				  
				  
			  }else{
				  
				  if($cek2->jml > 2){
					  
					 echo "Disposisi Tidak dapat dilakukan, Anda Maksimal hanya dapat mengirimkan 3 Peserta Per Bidang Studi  ";
				 
				  }else{
				   $this->db->set("status2",1);
				   $this->db->set("statuskabko",$statuskabko);
				   $this->db->set("ketkabko",$ketkabko);
				   $this->db->where("id",$id_disposisi);
				   $this->db->update("tm_siswa");
				  
				  
				   echo "Proses Disposisi Berhasil, Peserta sudah dikirim ke Komite Provinsi untuk mengikuti KSM tingkat Provinsi , Anda dapat melihat data Peserta Tk Provinsi pada menu Peserta Tingkat Provinsi";
				  
				  }
			  }
		  }else{
			  
			   echo "Disposisi Tidak dapat dilakukan, dikarenakan Jumlah Nilai yang Anda input tidak sesuai dengan jumlah soal, silahkan perbaiki dulu jumlah nilai pada menu Hasil Tes Kabko ";
				  
		  }
	}
	public function peserta_provinsi(){
		
		
		
		
		
									
          $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Peserta Tingkat Provinsi  ";
		  $data['dt']              = $this->Di->get_where("provinsi",array("id"=>$_SESSION['tmadmin_id']));
		     if(!empty($ajax)){
					    
			 $this->load->view('peserta/peserta_provinsi',$data);
		 }else{
			 
			
		     $data['konten'] = "peserta/peserta_provinsi";
			 
			 $this->load->view('pusdashboard/page_header',$data);
		 }
	
		
	}
	
	public function g_pesertaprovinsi(){
		
		  $iTotalRecords = $this->M_lokal->m_peserta_provinsi(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->M_lokal->m_peserta_provinsi(true)->result_array();
		   $arje = array("1"=>"MI/SD","2"=>"MTS/SMP","3"=>"MA/SMA");
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
				           
              if(empty($val['no_test2'])){
				  
			     //$notest ='<center><small style="color:blue">Blm digenerate</small><br> <button style="cursor:pointer" class="btn btn-danger   btn-sm batalkan_disposisi"    datanya="'.$val['id'].'"  style"cursor:pointer"> Batalkan Disposisi   </button></center>';
				// $notest  = '<small style="color:blue">Belum digenerate</small>';
			  }else{
				  
				  $notest = $val['no_test2'];
				  
				  
			  }
				 $no = $i++;
				
				$records["data"][] = array(
					$no,
					'<img src="'.base_url().'__statics/img/peserta/'.$val['foto'].'" class="img-thumbnail img-responsive" style="height:40px;width:40px" alt="loading.." id="blah" >',
							
					 $notest,
					($val['nama']),
					 $val['gender'],					
                    $this->Di->formattanggaldb($val['tgl_lahir']),			
					
					
				    $arje[$val['jenjang']],
					$this->Di->get_kondisi(array("id"=>$val['tmmadrasah_id']),"tm_madrasah","nama"),
					$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
					
					($val['statuskabko']),
					$val['ketkabko']
				
					
					
                    				
										
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	public function disposisi_batalkan(){
		
		 $datanya = $_POST['datanya'];
		 
		 $this->db->set("status2",0);
		 $this->db->where("id",$datanya);
		 $this->db->update("tm_siswa");
		 
		 echo "sukses";
		
	}
	public function kartutes(){
	    
	    	 		 
				$ret = '';
				
				$download_pdf=TRUE;
				$link_download = ($download_pdf == TRUE)?'':anchor(base_url().'lokal/printkartutes/true', 'Download Pdf');
				
				
		 
					$data['breadcumb']  ="Kartu Tes";
				//	$data['m']   = $this->Di->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
					$data['data']      = $this->Di->get_where("v_siswa",array("id"=>($_GET['id'])));
					
				$pdf_filename = 'Kartutes'.str_replace(array("'",",","-"),"",$data['data']->nama).'.pdf';	 
				$data_header = array('title' => 'Kartu Tes',);
			
				
				$user_info = $this->load->view('peserta/kartutes', $data, true);
			
				$output = $user_info;
				
				
				if($download_pdf == TRUE)
				generate_pdf($output, $pdf_filename,TRUE);
				else
				echo $output;
	    
	}
	
	public function disposisikan(){
		
		$status = $this->input->get_post("status");
		$tmsiswa_id = $this->input->get_post("tmsiswa_id");
		
		$this->db->set("d_p",$status);
		$this->db->where("id",$tmsiswa_id);
		$this->db->update("tm_siswa");
		echo "sukses";
		
		
	}
	
	
		public function kartu(){
		
		
		
		 if(!$this->session->userdata("tmadmin_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/loginkomite")."'>disini </a></center>";
               exit();
		  }
	   
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Kartu Peserta KSM ";
		 $data['dt']              = $this->Di->get_where("provinsi",array("id"=>$_SESSION['tmadmin_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('kartu/page_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "kartu/page_peserta";
			 
			 $this->load->view('pusdashboard/page_header',$data);
		 }
	
		
	}
	
	public function perbaharui_p(){
		
		
		
		 if(!$this->session->userdata("tmadmin_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/loginkomite")."'>disini </a></center>";
               exit();
		  }
	   
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Perbaharui Profil Anda";
		  $data['dt']              = $this->Di->get_where("provinsi",array("id"=>$_SESSION['tmadmin_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('page_profile',$data);
		 }else{
			 
			
		     $data['konten'] = "page_profile";
			 
			 $this->load->view('pusdashboard/page_header',$data);
		 }
	
		
	}
	
	
	public function perbaharui_akun(){
		  $this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
        
        $this->form_validation->set_message('min_length', '{field} Minimal 6 Karakter .');
        $this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
        $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , mungkin anda sudah pernah melakukan pendaftaran, silahkan klik menu login .');
				 
		 
			
				$config = array(
					    array('field' => 'password', 'label' => 'Password  ', 'rules' => 'trim|required|min_length[6]'),
				        array('field' => 'passwordc', 'label' => 'Ulangi Password ', 'rules' => 'trim|required|matches[password]'),
			
				 
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			
			
		    $this->db->set("password",(($_POST['password'])));
		    $this->db->set("alias",$_POST['password']);
		    $this->db->where("id",$_SESSION['tmadmin_id']);
		    $this->db->update("provinsi");
			echo "sukses";
		}else{
			
			 header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
			
		}
		
	
	}
	
	public function soal(){
		
		
	
	   
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']  = "Download Soal Tes";
		  $data['dt']              = $this->Di->get_where("provinsi",array("id"=>$_SESSION['tmadmin_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('soal/page_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "soal/page_peserta";
			 
			 $this->load->view('pusdashboard/page_header',$data);
		 }
	
		
	}
	

	

public function g_pesertates2(){
		
		  $iTotalRecords = $this->M_lokal->m_pesertates2(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->M_lokal->m_pesertates2(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
				                                if($val['foto']==""){
													$file = "not.png";
												 }else{
													 $file = $val['foto'];
												 }
												 
				$status = ($val['status']==1) ? "checked" :"";
												 
				$no = $i++;
				$records["data"][] = array(
					$no,
				    $this->Di->get_kondisi(array("id"=>$val['tmmadrasah_id']),"tm_madrasah","nama"),
					'<img src="'.base_url().'__statics/img/peserta/'.$file.'" class="img-circle img-responsive" style="height:50px;width:50px">',
					
					$val['nama'],
					
					$val['gender'],
					$val['tempat'].",".$this->Di->formattanggalstring($val['tgl_lahir']),
					
					$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama")
					
					
                    				
										
							
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	
	public function pencetakan(){
		
		$jenjang   = $this->input->get_post("jenjang",true);
		$kompetisi = $this->input->get_post("kompetisi",true);
		$jenis     = $this->input->get_post("jenis",true);
		  
		  if($kompetisi==""){
			
			echo "<h4><center>Mohon untuk memilih kompetisi sebelum menampilkan jumlah ruang , silahkan klik tombol close kemudian pilih kompetisi </h4>"; 			
			  
		  }else{
		  $j = $this->db->query("select count(id) as js from tm_siswa where status='1' and trkompetisi_id='".$kompetisi."' and jenjang ='".$jenjang."' and provinsi ='".$_SESSION['tmadmin_id']."' ")->row();
		  
		  $provinsi  = $this->db->query("select ruang_mi,ruang_mts,ruang_ma from provinsi where id='".$_SESSION['tmadmin_id']."'")->row();
		  if($jenjang==1){
			  $kolom = "ruang_mi";
		  }else if($jenjang==2){
			  $kolom = "ruang_mts";
		  }else if($jenjang==3){
			  $kolom = "ruang_ma";
			  
		  }
		  $ruang  = ($provinsi->$kolom ==0) ? 20 : $provinsi->$kolom;
		  $arrje  = array("1"=>"MI/SD ","2"=>"MTS/SMP","3"=>"MA/SMA");
		  $jumlah =  ceil($j->js/$ruang);
		  ?><div class="alert alert-info"> 
		   <b> Jumlah Peserta peruangan untuk jenjang <i> <?php echo $arrje[$jenjang]; ?></i> di atur <i><?php echo $ruang; ?></i> Peruangan,<br>
		     Untuk mengatur jumlah peserta peruangan, silahkan tentukan pada menu <i> Lokasi Tes </i> <br>
		  
		        Silahkan klik pada setiap tombol dibawah ini untuk melakukan download <?php echo strtoupper($jenis); ?> Peserta Peruang Tes </div> 
				<?php 
		     for($a=1;$a<=$jumlah;$a++){
				 
				  ?>
				 
				  <a href="<?php echo base_url(); ?>puslokal/<?php echo $jenis; ?>?jenjang=<?php echo $jenjang; ?>&kompetisi=<?php echo $kompetisi; ?>&page=<?php echo $a; ?>" target="_blank" class="btn btn-primary btn-sm"><span class="fa fa-institution"> RUANG  <?php echo ($a<10) ? "0".$a: $a; ?></span></a>
				 
				 <?php 
				 
				 
			 }
		  
		  
		  
		  
		  }
		    
		
	}
	public function daftarhadir(){
	    
	    	 
				$ret = '';
				
				$download_pdf=TRUE;
				$link_download = ($download_pdf == TRUE)?'':anchor(base_url().'puslokal/daftarhadir/true', 'Download Pdf');
				
				
		           $kompetisi = $_GET['kompetisi'];
		   		   $jenjang   = $_GET['jenjang'];
		   		   $page      = $_GET['page'];
				   $awal =0;
				   
				    $provinsi  = $this->db->query("select ruang_mi,ruang_mts,ruang_ma from provinsi where id='".$_SESSION['tmadmin_id']."'")->row();
					  if($jenjang==1){
						  $kolom = "ruang_mi";
					  }else if($jenjang==2){
						  $kolom = "ruang_mts";
					  }else if($jenjang==3){
						  $kolom = "ruang_ma";
						  
					  }
					  $ruang  = ($provinsi->$kolom ==0) ? 20 : $provinsi->$kolom;
					  
		  
				     if($page !=1){
						  $page = $page -1;
						  $awal = ($page * $ruang);
					  }
		   		   
					$data['breadcumb']   = "Kartu Tes";
					$data['awal']   = $awal;
					
		
				    
					$data['ssiswa']      = $this->Di->query("select * from tm_siswa where trkompetisi_id='".$kompetisi."' and status='1' and  jenjang='".$jenjang."' and provinsi='".$_SESSION['tmadmin_id']."' order by nama asc limit $awal,$ruang")->result();
					
				$pdf_filename = 'Daftar_peserta_ruang_'.$_GET['page'].'.pdf';	 
				$data_header = array('title' => 'Daftar Peserta',);
			
				
				$user_info = $this->load->view('kartu/daftarpeserta', $data, true);
			
				$output = $user_info;
				
				
				if($download_pdf == TRUE)
				generate_pdf($output, $pdf_filename,TRUE);
				else
				echo $output;
			
	    
	}
	
	public function absensipeserta(){
	    
	    	 
				$ret = '';
				
				$download_pdf=TRUE;
				$link_download = ($download_pdf == TRUE)?'':anchor(base_url().'puslokal/absensipeserta/true', 'Download Pdf');
				
				
		            $kompetisi = $_GET['kompetisi'];
		   		   $jenjang   = $_GET['jenjang'];
		   		   $page      = $_GET['page'];
				   $awal =0;
				    
					$provinsi  = $this->db->query("select ruang_mi,ruang_mts,ruang_ma from provinsi where id='".$_SESSION['tmadmin_id']."'")->row();
					  if($jenjang==1){
						  $kolom = "ruang_mi";
					  }else if($jenjang==2){
						  $kolom = "ruang_mts";
					  }else if($jenjang==3){
						  $kolom = "ruang_ma";
						  
					  }
					 $ruang  = ($provinsi->$kolom ==0) ? 20 : $provinsi->$kolom;
					  
		  
				     if($page !=1){
						  $page = $page -1;
						  $awal = ($page * $ruang);
					  }
					  
					$data['breadcumb']   = "Kartu Tes";
					$data['awal']   = $awal;
				    
					$data['ssiswa']      = $this->Di->query("select * from tm_siswa where trkompetisi_id='".$kompetisi."' and status='1' and  jenjang='".$jenjang."' and provinsi='".$_SESSION['tmadmin_id']."' order by nama asc limit $awal,$ruang")->result();
					
				$pdf_filename = 'Absensi_peserta_ruang_'.$_GET['page'].'.pdf';	
				$data_header = array('title' => 'Absensi Peserta ',);
			
				
				$user_info = $this->load->view('kartu/absensipeserta', $data, true);
			
				$output = $user_info;
				
				
				if($download_pdf == TRUE)
				generate_pdf($output, $pdf_filename,TRUE);
				else
				echo $output;
			
	    
	}
	
	public function albumpeserta(){
	    
	    	 
				$ret = '';
				
				$download_pdf=TRUE;
				$link_download = ($download_pdf == TRUE)?'':anchor(base_url().'puslokal/absensipeserta/true', 'Download Pdf');
				
				
		            $kompetisi = $_GET['kompetisi'];
		   		   $jenjang   = $_GET['jenjang'];
		   		   $page      = $_GET['page'];
				   $awal =0;

                    $provinsi  = $this->db->query("select ruang_mi,ruang_mts,ruang_ma from provinsi where id='".$_SESSION['tmadmin_id']."'")->row();
					  if($jenjang==1){
						  $kolom = "ruang_mi";
					  }else if($jenjang==2){
						  $kolom = "ruang_mts";
					  }else if($jenjang==3){
						  $kolom = "ruang_ma";
						  
					  }
					  $ruang  = ($provinsi->$kolom ==0) ? 20 : $provinsi->$kolom;
					  
		  
				     if($page !=1){
						  $page = $page -1;
						  $awal = ($page * $ruang);
					  }
		   		   
					$data['breadcumb']   = "Kartu Tes";
					$data['awal']   = $awal;
				    
					$data['ssiswa']      = $this->Di->query("select * from tm_siswa where trkompetisi_id='".$kompetisi."' and status='1' and  jenjang='".$jenjang."' and provinsi='".$_SESSION['tmadmin_id']."' order by nama asc limit $awal,$ruang")->result();
					
				$pdf_filename = 'album_peserta_ruang_'.$_GET['page'].'.pdf';	
				$data_header = array('title' => 'Absensi Peserta ',);
			
				
				$user_info = $this->load->view('kartu/album', $data, true);
			
				$output = $user_info;
				
				
				if($download_pdf == TRUE)
				generate_pdf($output, $pdf_filename,TRUE);
				else
				echo $output;
			
	    
	}
	
	
	
	public function kartumeja(){
	    
	    	 
				$ret = '';
				
				$download_pdf=TRUE;
				$link_download = ($download_pdf == TRUE)?'':anchor(base_url().'puslokal/kartumeja/true', 'Download Pdf');
				
				
		           $kompetisi = $_GET['kompetisi'];
		   		   $jenjang   = $_GET['jenjang'];
		   		   $page      = $_GET['page'];
				   $awal =0;
				    $provinsi  = $this->db->query("select ruang_mi,ruang_mts,ruang_ma from provinsi where id='".$_SESSION['tmadmin_id']."'")->row();
					  if($jenjang==1){
						  $kolom = "ruang_mi";
					  }else if($jenjang==2){
						  $kolom = "ruang_mts";
					  }else if($jenjang==3){
						  $kolom = "ruang_ma";
						  
					  }
					  $ruang  = ($provinsi->$kolom ==0) ? 20 : $provinsi->$kolom;
					  
		  
				     if($page !=1){
						  $page = $page -1;
						  $awal = ($page * $ruang);
					  }
		   		   
					$data['breadcumb']   = "Kartu Tes";
					$data['awal']   = $awal;
				    
					$data['ssiswa']      = $this->Di->query("select * from tm_siswa where trkompetisi_id='".$kompetisi."' and status='1' and  jenjang='".$jenjang."' and provinsi='".$_SESSION['tmadmin_id']."' order by nama asc limit $awal,$ruang")->result();
					
				$pdf_filename = 'Kartumeja_peserta_ruang_'.$_GET['page'].'.pdf';		 
				$data_header = array('title' => 'Absensi Peserta ',);
			
				
				$user_info = $this->load->view('kartu/kartumeja', $data, true);
			
				$output = $user_info;
				
				
				if($download_pdf == TRUE)
				generate_pdf($output, $pdf_filename,TRUE);
				else
				echo $output;
			
	    
	}
	
	
	public function lembar_download(){
		
		$this->load->helper('download');
   
        force_download('piagam/LJU.zip',NULL);   
		
	}
	
	public function soal_download(){
		
		$this->load->helper('download');
       $this->db->query("update provinsi set download ='1' where id='".$_SESSION['tmadmin_id']."'");
        force_download('piagam/NASKAH_SOAL.zip',NULL);   
		
	}
	
	
		public function jawaban_download(){
		
		$this->load->helper('download');
       $this->db->query("update provinsi set jawaban ='1' where id='".$_SESSION['tmadmin_id']."'");
        force_download('piagam/jawaban.zip',NULL);   
		
	}
	
	
		public function paktanyaapa(){
		
		
		
		
		
									
         $ajax = $this->input->get_post("ajax",true);	
		  $data['title']           = "Upload Pakta  ";
		  $data['dt']              = $this->Di->get_where("provinsi",array("id"=>$_SESSION['tmadmin_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('soal/pakta',$data);
		 }else{
			 
			
		     $data['konten'] = "soal/pakta";
			 
			 $this->load->view('pusdashboard/page_header',$data);
		 }
	
		
	}
	
	public function save_pakta(){
		
		//error_reporting(0);
		$trpersyaratan_id    = $this->input->get_post("trpersyaratan_id");
		$namapersyaratan     = $this->input->get_post("namapersyaratan");
	
							
		$config['upload_path'] = './__statics/pakta';
		$folder   = '__statics/pakta';
	$config['allowed_types'] = "jpg|png|gif|jpeg"; 	
	    $config['overwrite'] = true; 	
		$filenamepecah			 = explode(".",$_FILES['file']['name']);
		$imagenew				 = "Fakta_".$_SESSION['tmadmin_id'].".".pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
		
		//unlink(FCPATH.$folder."/".$imagenew);
		
		$config['file_name'] = $imagenew;
		
		$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload('file'))
				{
					 header('Content-Type: application/json');
				     echo json_encode(array('error' => true, 'message' => $this->upload->display_errors()));
							
					
				
				}
				else{
					
											
												  	
												  
					        
							
							    $this->db->set("fakta",$imagenew);	
								$this->db->where("id",$_SESSION['tmadmin_id']);	
								$this->db->update("provinsi");
							
							
					
						
						
					
							 header('Content-Type: application/json');
						    echo json_encode(array('success' => true, 'message' => "Upload Sukses"));
								
				
				}
	
	}
	
	
	
	public function hapus_pakta(){
		
	
	    $this->db->set("fakta","");	
	    $this->db->where("id",$_SESSION['tmadmin_id']);	
		$this->db->update("provinsi");
		echo "sukses";
		
		
	}
	
	 public function import(){
		
		
		
		
		
									
          $ajax = $this->input->get_post("ajax",true);	
		  $data['title']           = "Import Hasil Tes ";
		  $data['jenjang']           = $this->input->get_post("jenjang");
		  $data['dt']              = $this->Di->get_where("provinsi",array("id"=>$_SESSION['tmadmin_id']));
	     if(!empty($ajax)){
					    
			 $this->load->view('hasiltes/form_peserta',$data);
		 }else{
			 
			
		     $data['konten'] = "hasiltes/form_peserta";
			 
			 $this->load->view('pusdashboard/page_header',$data);
		 }
	
		
	}
	
	function import_excel()
		{
			error_reporting(0);
		$this->load->library('PHPExcel/IOFactory');
      
	
	   
	    $provinsi           = $_SESSION['tmadmin_id'];
	    $kompetisi      = $_POST['kompetisi'];
	    $jenjang        = $_POST['jenjang'];
	    $sheetup   = 0;
	    $rowup     = 2;
		
		
		$fileName = time().".xlsx";
         
        $config['upload_path'] = './excel/'; 
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 1000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('file');
		
        $inputFileName = './excel/'.$fileName;
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet($sheetup);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
             $to=0;
             $jum=0;
			 
            for ($row = $rowup; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                 
				
			 	$no           =      $this->db->escape_str(ucwords(trim($rowData[0][0])));  
			 	$no_test      =      str_replace(array("-","."," ","_",""),"",trim($rowData[0][1]));  
			$eksplorasi   =      number_format($this->db->escape_str(ucwords(trim($rowData[0][4]))),2); 
				
		    $cek        = $this->db->query("SELECT id,no_test3,nas_nilai FROM tm_siswa where REPLACE(no_test3,'.','') ='".$no_test."' ")->row();
			//print_r($cek); exit();
			 $nilai_cbt  = (50 * $cek->nas_nilai) / 100;
			 $nilai_eksplorasi = (50 * $eksplorasi) / 100;
			 $nilai_akhir = $nilai_cbt + $nilai_eksplorasi;
			if(count($cek) >0 ){
		
			$jum++;
                 $data = array(
                    "eksplorasi "=> $eksplorasi,                   
                    "nilai_akhir "=> $nilai_akhir               
                    
                );
				
				
			
				
				$this->db->where("id",$cek->id);
				$this->db->update("tm_siswa",$data);
				
            }			
			
        }
		
		unlink("./excel/".$fileName);
		
	//	print_r($data); echo "sbntar"; exit();
			echo "<center><br><b>". $jum. "</b>  PESERTA  BERHASIL DIUPLOAD ";
			echo "<br> SILAHKAN PERIKSA KEMBALI HASIL UPLOAD APAKAH SUDAH SESUAI ? JIKA BELUM SESUAI SILAHKAN MASUKKAN NILAI DAN PERIKSA FILE EXCEL ANDA";
           echo '<br>
			                       <a href="'.site_url("puslokal/import?jenjang=".$jenjang."").'" class="btn btn-danger btn-sm" target="_blank"> KLIK DISINI UNTUK KEMBALI KE HALAMAN IMPORT DATA </a>';
								
	}
	
	
	
	
	
	public function test(){
		
		require_once(APPPATH.'libraries/kepdf/vendor/autoload.php');
		require_once(APPPATH.'system/helpers/dompdf/dompdf.php');
		
         $PdfPath = APPPATH.'system/helpers/dompdf';
		\PhpOffice\PhpWord\Settings::setPdfRendererPath($PdfPath);
		\PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
		$phpWord = \PhpOffice\PhpWord\IOFactory::load('piagam.docx');
		$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
		$xmlWriter->save('piagamdendi.pdf');
			
			 
			 
		
		
	}
	
	
	public function panitia(){
		
		
		
		 
		
									
         $ajax = $this->input->get_post("ajax",true);	
         $id = $this->input->get_post("id",true);	
		 $data['title']  = "Input Pendamping ";
		 $data['id']  = $id;
		 
	     if(!empty($ajax)){
					    
			 $this->load->view('official/page_pendamping',$data);
		 }else{
			 
			
		     $data['konten'] = "official/page_pendamping";
			 
			 $this->load->view('pusdashboard/page_header',$data);
		 }
	
		
	}
	
	
	public function g_pendamping(){
		error_reporting(0);
		  $iTotalRecords = $this->M_lokal->g_pendamping(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->M_lokal->g_pendamping(true)->result_array();
		  $tingkat = array("0"=>"KSM Provinsi","1"=>"KSM Nasional");
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
				                        
		
			$status="";					
		  if($val['tingkat']==1){
			$status ="<br><span class='badge badge-primary'>".$val['status']."</span>";

			$aksi ='<div class="btn-group">
			<a href="'.site_url('plokal/idCard?q='.base64_encode($val['id']).'').'" disabled target="_blank" class="btn btn-danger btn-sm"><i class="fa fa-file"> ID Card </i></a>
			<a class="btn btn-info  ubah" urlnya = "'.site_url("plokal/f_pendamping").'"  datanya="'.$val['id'].'" title="Ubah Data">
			<i class=" fa fa-pencil-square  " style="color:white"></i>
			</a>
			<a class="btn btn-info  hapus" urlnya = "'.site_url("plokal/h_pendamping").'"  datanya="'.$val['id'].'" title="Hapus Data">
			<i class=" fa fa-trash-o  " style="color:white"></i>
			</a>
		</div> ';
		   }else{
			$aksi ='<div class="btn-group">
				<a href="'.site_url('plokal/sertifikatofficial?q='.base64_encode($val['id']).'').'" disabled target="_blank" class="btn btn-danger btn-sm"><i class="fa fa-file"> Sertifikat </i></a>
				<a class="btn btn-info  ubah" urlnya = "'.site_url("plokal/f_pendamping").'"  datanya="'.$val['id'].'" title="Ubah Data">
				<i class=" fa fa-pencil-square  " style="color:white"></i>
				</a>
				<a class="btn btn-info  hapus" urlnya = "'.site_url("plokal/h_pendamping").'"  datanya="'.$val['id'].'" title="Hapus Data">
				<i class=" fa fa-trash-o  " style="color:white"></i>
				</a>
			</div> ';

		   }


			$no = $i++;
			$records["data"][] = array(
				$no,
				$aksi,
				$this->Di->get_kondisi(array("id"=>$val['provinsi']),"provinsi","nama"),
				$tingkat[$val['tingkat']].$status,
				'<img src="'.base_url().'__statics/upload/'.$val['foto'].'" width="30px" class="img-responsive">',
				  $val['nama'],
				$val['jabatan_struktural'],	
				$val['jabatan'],	
				$val['hp'],				
								
				$val['alamat']
			
				
						
			
			   

			  );
		  }
	
	  $records["draw"] = $sEcho;
	  $records["recordsTotal"] = $iTotalRecords;
	  $records["recordsFiltered"] = $iTotalRecords;
	  
	  echo json_encode($records);
	}
	
	public function f_pendamping(){
		
		 $id = $this->input->get_post("id",TRUE);
		 
		 $data = array();
		    if(!empty($id)){
				
				$data['dataform'] = $this->Di->get_where("tr_panitia",array("id"=>$id));
			}
	     
		 $data['title']  = "Form Pendamping";
		 $this->load->view('official/form_pendamping',$data);
	}
	
	
	public function h_pendamping(){
		
		$this->Di->delete("tr_panitia",array("id"=>$_POST['id']));
		echo "sukses";
		
	}

// Kabkota 

	public function kabkota(){
			
			
			
			
		
			
										
		$ajax = $this->input->get_post("ajax",true);	
		$data['title']           = "Data Kabupaten/kota di Provinsi Anda";
		$data['dt']              = $this->Di->get_where("provinsi",array("id"=>$_SESSION['tmadmin_id']));
		if(!empty($ajax)){
					
			$this->load->view('kabkota/page',$data);
		}else{
			
		
			$data['konten'] = "kabkota/page";
			
			$this->load->view('pusdashboard/page_header',$data);
		}

	
	} 



	public function g_kabkota(){
	
		$iTotalRecords = $this->M_lokal->m_kabkota(false)->num_rows();
		
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);
		
		$records = array();
		$records["data"] = array(); 

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;
		
		$datagrid = $this->M_lokal->m_kabkota(true)->result_array();
		
		$i= ($iDisplayStart +1);
		foreach($datagrid as $val) {
										
			$no = $i++;
			$records["data"][] = array(
				$no,
				$val['nama'],
				
				$val['username'],
				$val['alias']
			
									
						
			
				

				);
			}
	
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		
		echo json_encode($records);
	}


	public function f_kabkota(){
	
		$id = $this->input->get_post("id",TRUE);
		
		$data = array();
		if(!empty($id)){
			
			$data['dataform'] = $this->Di->get_where("tm_madrasah",array("id"=>$id));
		}

		$data['title']  = "Form ";
		$this->load->view('kabkota/form',$data);
	}


	public function s_kabkota(){

	$this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
	
	$this->form_validation->set_message('min_length', '{field} Minimal 6 Karakter .');
	$this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
	$this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , mungkin anda sudah pernah melakukan pendaftaran, silahkan klik menu login .');
				
		
		
			$config = array(
				array('field' => 'password', 'label' => 'Password Baru  ', 'rules' => 'trim|required'),
				
				

				
			);
			
			$this->form_validation->set_rules($config);	
	
	if ($this->form_validation->run() == true) {
			
			
			
			$this->db->set("password",sha1(md5($_POST['password'])));
			$this->db->set("alias",$_POST['password']);
			$this->db->where("id",$_POST['id']);
			$this->db->update("kota");
			$this->Di->log($_SESSION['nama']. "Merubah Password Kota","p");
			echo "sukses";
												
		} else {
			
		
			header('Content-Type: application/json');
			echo json_encode(array('error' => true, 'message' => validation_errors()));
		
	}	


	}


	public function komite(){
			
			
			
			
		
			
										
		$ajax = $this->input->get_post("ajax",true);	
		$data['title']           = "Data Komite Kabupaten/kota di Provinsi Anda";
		$data['dt']              = $this->Di->get_where("provinsi",array("id"=>$_SESSION['tmadmin_id']));
		if(!empty($ajax)){
					
			$this->load->view('kabkota/komite',$data);
		}else{
			
		
			$data['konten'] = "kabkota/komite";
			
			$this->load->view('pusdashboard/page_header',$data);
		}

	
	} 



	public function g_komite(){
	
		$iTotalRecords = $this->M_lokal->m_komite(false)->num_rows();
		
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);
		
		$records = array();
		$records["data"] = array(); 

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;
		
		$datagrid = $this->M_lokal->m_komite(true)->result_array();
		
		$i= ($iDisplayStart +1);
		foreach($datagrid as $val) {
										
			$no = $i++;
			$records["data"][] = array(
				$no,
				$val['kota'],
				'<img src="'.base_url().'__statics/upload/'.$val['foto'].'" width="30px" class="img-responsive">',
				  $val['nama'],
				$val['hp'],				
				$val['jabatan'],					
				$val['alamat']
				
				
									
						
			
				

				);
			}
	
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		
		echo json_encode($records);
	}
	

	// Generate Nomor Tes 

	public function generatekabko(){
				
		 
	
		 
		$siswa = $this->db->query("select id,urutan,trkompetisi_id,kota_madrasah from v_siswa where  status=2 AND no_test='' order by kota_madrasah asc,lokasi_kabko asc,trkompetisi_id asc")->result();
		
		   foreach($siswa as $row){
			     $maxid     = $this->db->query("select max(urutan) as pendaftar from tm_siswa")->row();
				 $pendaftar = $maxid->pendaftar+1;

				 $kota      = $row->kota_madrasah;
				
				 $trkompetisi_id     = (strlen($row->trkompetisi_id)==1) ? "0".$row->trkompetisi_id : $row->trkompetisi_id;
				 
				 if(strlen($pendaftar)==1){
					 $nopen = "00000".$pendaftar;
				 }else if(strlen($pendaftar)==2){
					 
					 $nopen = "0000".$pendaftar;
				 }else if(strlen($pendaftar)==3){
					 
					  	 $nopen = "000".$pendaftar;
				 }else if(strlen($pendaftar)==4){
					 
							$nopen = "00".$pendaftar;
				  }else if(strlen($pendaftar)==5){
					 
							$nopen = "0".$pendaftar;
				  }else if(strlen($pendaftar)==6){
					 
							$nopen = $pendaftar;
				  }
				 

				 
				$no_test    = $trkompetisi_id.$kota.$nopen; 
				 
				 $this->db->set("urutan",($pendaftar));
				 $this->db->set("no_test",($no_test));
				 $this->db->where("id",$row->id);
				 $this->db->update("tm_siswa");
			   
			    
			  

		   }
		   echo "sukses".count($siswa); 
		  
		   
		   
		
		
	}

	 public function generateLokasi(){

		
	 }


	public function generateprovinsi(){
				
		 
	
		 
		$siswa = $this->db->query("select id,urutan,trkompetisi_id,kota_madrasah from v_siswa where  status2=1 AND no_test2='' order by kota_madrasah asc,trkompetisi_id asc")->result();
		
		   foreach($siswa as $row){
			     $maxid     = $this->db->query("select max(urutan) as pendaftar from tm_siswa")->row();
				 $pendaftar = $maxid->pendaftar+1;

				 $kota      = $row->kota_madrasah;
				
				 $trkompetisi_id     = (strlen($row->trkompetisi_id)==1) ? "0".$row->trkompetisi_id : $row->trkompetisi_id;
				 
				 if(strlen($pendaftar)==1){
					 $nopen = "00000".$pendaftar;
				 }else if(strlen($pendaftar)==2){
					 
					 $nopen = "0000".$pendaftar;
				 }else if(strlen($pendaftar)==3){
					 
					  	 $nopen = "000".$pendaftar;
				 }else if(strlen($pendaftar)==4){
					 
							$nopen = "00".$pendaftar;
				  }else if(strlen($pendaftar)==5){
					 
							$nopen = "0".$pendaftar;
				  }else if(strlen($pendaftar)==6){
					 
							$nopen = $pendaftar;
				  }
				 

				 
				$no_test    = $trkompetisi_id.$kota.$nopen; 
				 
				 $this->db->set("urutan",($pendaftar));
				 $this->db->set("no_test2",($no_test));
				 $this->db->where("id",$row->id);
				 $this->db->update("tm_siswa");
			   
			    
			  

		   }
		   echo "sukses".count($siswa); 
		  
		   
		   
		
		
	}

	public function generatenasional(){
				
		 
	
		 
		$siswa = $this->db->query("select id,urutan,trkompetisi_id,kota_madrasah from v_siswa where  status3=1 AND no_test3='' order by provinsi_madrasah asc,trkompetisi_id asc")->result();
		
		   foreach($siswa as $row){
			     $maxid     = $this->db->query("select max(urutan) as pendaftar from tm_siswa")->row();
				 $pendaftar = $maxid->pendaftar+1;

				 $kota      = $row->kota_madrasah;
				
				 $trkompetisi_id     = (strlen($row->trkompetisi_id)==1) ? "0".$row->trkompetisi_id : $row->trkompetisi_id;
				 
				 if(strlen($pendaftar)==1){
					$nopen = "00000".$pendaftar;
				}else if(strlen($pendaftar)==2){
					
					$nopen = "0000".$pendaftar;
				}else if(strlen($pendaftar)==3){
					
						  $nopen = "000".$pendaftar;
				}else if(strlen($pendaftar)==4){
					
						   $nopen = "00".$pendaftar;
				 }else if(strlen($pendaftar)==5){
					
						   $nopen = "0".$pendaftar;
				 }else if(strlen($pendaftar)==6){
					
						   $nopen = $pendaftar;
				 }
				
				 

				 
				$no_test    = $trkompetisi_id.$kota.$nopen; 
				 
				 $this->db->set("urutan",($pendaftar));
				 $this->db->set("no_test3",($no_test));
				 $this->db->where("id",$row->id);
				 $this->db->update("tm_siswa");
			   
			    
			  

		   }
		   echo "sukses".count($siswa); 
		  
		   
		   
		
		
	}
	

	public function generate_ruang(){

       $kota = $this->db->get_where("tr_lokasites",array("tingkat"=>1))->result();
	      foreach($kota as $rkota){

			  $kompetisi = $this->db->get("tr_kompetisi")->result();

			    foreach($kompetisi as $rkom){

					$jml       = $this->db->query("select count(id) as jml from v_siswa where lokasi_kabko='{$rkota->id}'  and status=2 and trkompetisi_id='{$rkom->id}'")->row();
					$ruang     = ceil($jml->jml / 40);
					$trkompetisi_id     = (strlen($rkom->id)==1) ? "0".$rkom->id : $rkom->id;

						$nop =1; 
						for($r=1;$r <= $ruang; $r++){
							$ruangnya     = (strlen($r)==1) ? "0".$r : $r;
							$username = $trkompetisi_id.$rkota->id.$ruangnya;

							$limit = 0; 
                                     if($r !=1){
                                            $batasan = $r-1;
                                            $limit = $batasan * 40;
                                     }
									 $password = $this->Di->randomangka(6);
									 $this->db->set("provinsi_id",$rkota->provinsi);
									 $this->db->set("kota_id",$rkota->kota);
									 $this->db->set("lokasi_id",$rkota->id);
									 $this->db->set("trkompetisi_id",$rkom->id);
									 $this->db->set("ruang",$r);
									 $this->db->set("kode",$username);
									 $this->db->set("password",$password);
									 $this->db->set("tingkat","kabko");
									 $this->db->insert("tm_ruang");
									 $id_ruang = $this->db->insert_id();
                                                          
                                        $peserta   = $this->db->query("select id from v_siswa where lokasi_kabko='{$rkota->id}' and status=2 and trkompetisi_id='{$rkom->id}' limit $limit,40")->result();
                                                 foreach($peserta as $rpes){

                                
													$this->db->set("id_ruang1",$id_ruang);													
													$this->db->set("ruang1",$r);													
													$this->db->where("id",$rpes->id);													
													$this->db->update("tm_siswa");


												 }




						}


				}




		  }

	}



	public function generate_provinsi(){

		$kota = $this->db->get("provinsi")->result();
		   foreach($kota as $rkota){
 
			   $kompetisi = $this->db->get("tr_kompetisi")->result();
 
				 foreach($kompetisi as $rkom){
 
					 $jml       = $this->db->query("select count(id) as jml from v_siswa where provinsi_madrasah='{$rkota->id}' and status2=1 and trkompetisi_id='{$rkom->id}'")->row();
					 $ruang     = ceil($jml->jml / 40);
					 $trkompetisi_id     = (strlen($rkom->id)==1) ? "0".$rkom->id : $rkom->id;
 
						 $nop =1; 
						 for($r=1;$r <= $ruang; $r++){
							 $ruangnya     = (strlen($r)==1) ? "0".$r : $r;
							 $username = $trkompetisi_id.$rkota->id.$ruangnya;
 
							 $limit = 0; 
									  if($r !=1){
											 $batasan = $r-1;
											 $limit = $batasan * 40;
									  }
									  $password = $this->Di->randomangka(6);
									  $this->db->set("provinsi_id",$rkota->id);
									  $this->db->set("trkompetisi_id",$rkom->id);
									  $this->db->set("ruang",$r);
									  $this->db->set("kode",$username);
									  $this->db->set("password",$password);
									  $this->db->set("tingkat","provinsi");
									  $this->db->insert("tm_ruang");
									  $id_ruang = $this->db->insert_id();
														   
										 $peserta   = $this->db->query("select id from v_siswa where provinsi_madrasah='{$rkota->id}' and status2=1 and trkompetisi_id='{$rkom->id}' limit $limit,40")->result();
												  foreach($peserta as $rpes){
 
								 
													 $this->db->set("id_ruang2",$id_ruang);													
													 $this->db->set("ruang2",$r);													
													 $this->db->where("id",$rpes->id);													
													 $this->db->update("tm_siswa");
 
 
												  }
 
 
 
 
						 }
 
 
				 }
 
 
 
 
		   }
 
	 }
 


	public function monitorpeserta(){
		
		
		
		if(!$this->session->userdata("tmadmin_id")){
		   echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
			  exit();
		 }
	  
	   
								   
		  $ajax            = $this->input->get_post("ajax",true);	
		  $data['jenjang'] = base64_decode($this->input->get_post("jenjang",true));	
		  $data['title']   = "Monitor  Peserta ";
		  $data['dt']              = $this->Di->get_where("provinsi",array("id"=>$_SESSION['tmadmin_id']));
		if(!empty($ajax)){
					   
			$this->load->view('monitorpeserta',$data);
		}else{
			
		   
			$data['konten'] = "monitorpeserta";
			
			$this->load->view('pusdashboard/page_header',$data);
		}
   
	   
   }
   
   
   public function g_monitorpeserta(){
	     error_reporting(0);
		 $iTotalRecords = $this->M_lokal->m_monitorpeserta(false)->num_rows();
		 $cbt = $this->load->database("cbt",true);
		 $iDisplayLength = intval($_REQUEST['length']);
		 $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		 $iDisplayStart = intval($_REQUEST['start']);
		 $sEcho = intval($_REQUEST['draw']);
		 
		 $records = array();
		 $records["data"] = array(); 

		 $end = $iDisplayStart + $iDisplayLength;
		 $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		 
		 $datagrid = $this->M_lokal->m_monitorpeserta(true)->result_array();
		 $arjenis = array("1"=>"Madrasah","2"=>"Sekolah");
		  $i= ($iDisplayStart +1);
		  foreach($datagrid as $val) {
											 
			   $status = $cbt->query("select sta  FROM t_pkujps where idPs ='{$val['id']}' and strt is not null")->row();
			   								
			   if($status->sta==1){
				   
				   $statusnya ='<i class="badge badge-danger"> Sedang Mengerjakan </i>';
				   
				}else if($status->sta==2){
					
				    $statusnya ='<i class="badge badge-success"> Selesai Mengerjakan </i>';	
				}else{

					$statusnya ='<i class="badge badge-default"> Belum Mengerjakan </i>';
				}

			  		
			   $no = $i++;

			   $peserta   = $this->db->query("select * from v_siswa where id='{$val['id']}'")->row(); 
			   $kompetisi = $this->db->query("select * from tr_kompetisi where id='{$val['idJlr']}'")->row(); 

			   $foto ="img/ksm.png";
			   if(!empty($peserta->foto)){

				  $foto ="upload/{$peserta->foto}";
			   }
			   $img ='<img src="https://ksm.kemenag.go.id/__statics/'.$foto.'" class="img-responsive img-circle" style="width:40px">';
								   
			  
			   $records["data"][] = array(
				   $no,
				   $this->Di->get_kondisi(array("id"=>$peserta->provinsi_madrasah),"provinsi","nama"),
				   $this->Di->get_kondisi(array("id"=>$peserta->kota_madrasah),"kota","nama"),
				   $val['kdr'],
				   "<center>".$img."<br>".$val['nama']."</center>",
				   $this->Di->jenjangnama($kompetisi->tmmadrasah_id)." ".$kompetisi->nama,
				   "Ruang ".$peserta->ruang1." <br> Sesi ".$val['sesi'],
				   $statusnya
				   
				   
				   
									   
						   
			   
				  

				 );
			 }
	   
		 $records["draw"] = $sEcho;
		 $records["recordsTotal"] = $iTotalRecords;
		 $records["recordsFiltered"] = $iTotalRecords;
		 
		 echo json_encode($records);
   }
	

   // Hasil Kabkota


	
	public function g_hasilkabko(){
		error_reporting(0);
		$iTotalRecords = $this->M_lokal->m_hasilkabko(false)->num_rows();
		
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);
		
		$records = array();
		$records["data"] = array(); 

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;
		
		$datagrid = $this->M_lokal->m_hasilkabko(true)->result_array();
		$arjenis = array("1"=>"Madrasah","2"=>"Sekolah");
		$ketkabko = array("1"=>"Juara I","2"=>"Juara II","3"=>"Juara III","4"=>"Juara Harapan I","5"=>"Juara Harapan II","6"=>"Juara Harapan III");
		 $i= ($iDisplayStart +1);
		 foreach($datagrid as $val) {
											
											   
			$no = $i++;
			//   $foto ="img/ksm.png";
			//    if(!empty($val['foto'])){

			// 	  $foto ="upload/{$val['foto']}";
			//    }
			   $nonya = "<a href='".site_url('puslokal/sertifikatkota?q='.base64_encode($val['id']).'&nik='.base64_encode($val['nik_siswa']).'')."' target='_blank' >".$no."</a>";
			   $notest = "<a href='https://ksm.kemenag.go.id/pengawas/ruangan/berita-acara?id=".$val['id']."' target='_blank' >".$val['no_test']."</a>";
			  // $img ='<center><img src="https://ksm.kemenag.go.id/__statics/'.$foto.'" class="img-responsive img-thumbnail" style="width:80px; border-radius:50%">'.$notest."<br>".$val['nama'].'<br>'.$val['tgl_lahir'].'</center>';
			    $img ='<center>'.$val['nama']."<br>".$notest.'<br>'.$val['tgl_lahir'].'</center>';
											   
			  

			//   $disabled="disabled";
			//     if($val['ctt']==1){
			// 		$disabled="";
			// 	}

				$disabled="disabled";

				$checked="";
			    if($val['skrn']==1){
					$checked="checked";
				}
			 
			  $records["data"][] = array(
				  $nonya,
				  $img,	
				  $this->Di->get_kondisi(array("id"=>$val['provinsi_madrasah']),"provinsi","nama"),
				  $this->Di->get_kondisi(array("id"=>$val['kota_madrasah']),"kota","nama"),				  		
				  $this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
				  (!empty($val['ketkabko'])) ? "<span class='badge badge-success'>".$val['ketkabko']."</span>" : "<span class='badge badge-danger'>Peserta </span>",
				  $val['bnr_pg'],
				  $val['bnr_isi'],
				  $val['jml_benar'],
				  $val['slh'],
				  $val['ksg'],
				  $val['nla'],
				  
				  $arjenis[$val['jenis']],
				  $this->Di->jenjangnama($val['jenjang']),
				  $val['username'],
				  $val['madrasah'],
				  $val['textTegur'],
				
				  '<input type="checkbox" class="konfirm" '.$disabled.' '.$checked.' value="1" tmsiswa_id="'.$val['id'].'">'
			
				  
				  
				  
									  
						  
			  
				 

				);
			}
	  
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		
		echo json_encode($records);
  }

  public function sertifikatkota(){

 	$tmsiswa_id = base64_decode($this->input->get("q")); 
 	//$nik       = base64_decode(str_replace(array())$this->input->get("nik")); 

	$datasiswa = $this->db->query("select d_p,nik_siswa,madrasah,provinsi,trkompetisi_id,id,nama,jenjang_madrasah,kota_madrasah from v_siswa_kabkota where nla IS NOT NULL AND id='".$tmsiswa_id."' ")->row();
		
	if(count($datasiswa) >0){

		$pdf = new Fpdi();

		$this->load->library('ciqrcode'); //pemanggilan library QR CODE

		$config['cacheable']	= true; 
		$config['cachedir']		= './tmp/chache'; 
		$config['errorlog']		= './tmp/'; 
		$config['imagedir']		= './tmp/'; 
		$config['quality']		= true; 
		$config['size']			= '1024'; 
		$config['black']		= array(224,255,255);
		$config['white']		= array(70,130,180); 
		$this->ciqrcode->initialize($config);

		//$image_name=$datasiswa->id.'.png'; 
		$image_name= $datasiswa->id.'.png'; 

		$params['data'] = "https://ksm.kemenag.go.id/publik/validitas?q=".base64_encode($datasiswa->nik_siswa)."&s=".base64_encode($datasiswa->id).""; 
		$params['level'] = 'H'; 
		$params['size'] = 10;
		$params['savename'] = FCPATH.$config['imagedir'].$image_name; 
		$this->ciqrcode->generate($params);

		$ketkabko = array("1"=>"Juara I","2"=>"Juara II","3"=>"Juara III","4"=>"Juara Harapan I","5"=>"Juara Harapan II","6"=>"Juara Harapan III");
		$jenjangn = array("1"=>"Madrasah Ibtidaiyah/Sederajat ","2"=>"Madrasah Tsanawiyah/Sederajat","3"=>"Madrasah Aliyah/Sederajat");
		$kompetisi = $this->Di->get_kondisi(array("id"=>$datasiswa->trkompetisi_id),"tr_kompetisi","nama");
		$kota_madrasah = $this->Di->get_kondisi(array("id"=>$datasiswa->kota_madrasah),"kota","nama");
		$wilayah = strtoupper($jenjangn[$datasiswa->jenjang_madrasah])." ".$kota_madrasah;


		  if($datasiswa->d_p <= 6){
	
			$status =   $ketkabko[$datasiswa->d_p];
		   
		   
		


            $pdf->addPage('L');
			
			$pdf->setSourceFile('juara.pdf');
		
			$tplIdx = $pdf->importPage(1);
			$pdf ->useTemplate($tplIdx, null, null, null, null,FALSE);
			
			
			$pdf->SetFont('Arial','B',26);
            $pdf->SetTextColor(6, 0, 0);

            $pdf->Cell(null,93,$datasiswa->nama,0,0,'C');

			$pdf->Ln(10);
			$pdf->SetFont('Arial','B',15);
            $pdf->SetTextColor(6, 0, 0);
            $pdf->Cell(null,93,$datasiswa->madrasah,0,0,'C');

			$pdf->Ln(20);
			$pdf->SetFont('Arial','B',26);
            $pdf->SetTextColor(6, 0, 0);
            $pdf->Cell(271,93,$status,0,0,'C');


			$pdf->Ln(10);
			$pdf->SetFont('Arial','B',15);
            $pdf->SetTextColor(6, 0, 0);
            $pdf->Cell(null,93,"Bidang Studi ".$kompetisi,0,0,'C');

			$pdf->Ln(10);
			$pdf->SetFont('Arial','B',15);
            $pdf->SetTextColor(6, 0, 0);
            $pdf->Cell(null,93,"JENJANG ".$wilayah,0,0,'C');
			
			
			//$pdf->Write(0, '');


			$pdf->Image("tmp/".$image_name,260,170,30,30);


			$pdf->Output('I', 'PIAGAM  '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf');

			}else{
				$status = " Peserta ";




				$pdf->addPage('L');
			
				$pdf->setSourceFile('peserta.pdf');
			
				$tplIdx = $pdf->importPage(1);
				$pdf ->useTemplate($tplIdx, null, null, null, null,FALSE);
				
				
				$pdf->SetFont('Arial','B',26);
				$pdf->SetTextColor(6, 0, 0);
	
				$pdf->Cell(null,100,$datasiswa->nama,0,0,'C');
	
				$pdf->Ln(15);
				$pdf->SetFont('Arial','B',15);
				$pdf->SetTextColor(6, 0, 0);
				$pdf->Cell(null,93,$datasiswa->madrasah,0,0,'C');
	
				$pdf->Ln(20);
				$pdf->SetFont('Arial','B',26);
				$pdf->SetTextColor(6, 0, 0);
				$pdf->Cell(271,93,null,0,0,'C');
	
	
				$pdf->Ln(10);
				$pdf->SetFont('Arial','B',15);
				$pdf->SetTextColor(6, 0, 0);
				$pdf->Cell(null,93,"Bidang Studi ".$kompetisi,0,0,'C');
	
				$pdf->Ln(10);
				$pdf->SetFont('Arial','B',15);
				$pdf->SetTextColor(6, 0, 0);
				$pdf->Cell(null,93,"JENJANG ".$wilayah,0,0,'C');
				
				
				//$pdf->Write(0, '');
	
	
				$pdf->Image("tmp/".$image_name,260,170,30,30);
	
	
				$pdf->Output('I', 'PIAGAM  '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf');


		
			}


	}else{

		echo "404 Not Found";
	}
  }



  public function sertifikatofficial(){

	$tmsiswa_id = base64_decode($this->input->get("q")); 
	//$nik       = base64_decode(str_replace(array())$this->input->get("nik")); 

   $datasiswa = $this->db->query("select * from tr_panitia where id='".$tmsiswa_id."' ")->row();
	   
   if(count($datasiswa) >0){

	   $pdf = new Fpdi();

	   $this->load->library('ciqrcode'); //pemanggilan library QR CODE

	   $config['cacheable']	= true; 
	   $config['cachedir']		= './tmp/chache'; 
	   $config['errorlog']		= './tmp/'; 
	   $config['imagedir']		= './tmp/'; 
	   $config['quality']		= true; 
	   $config['size']			= '1024'; 
	   $config['black']		= array(224,255,255);
	   $config['white']		= array(70,130,180); 
	   $this->ciqrcode->initialize($config);

	   //$image_name=$datasiswa->id.'.png'; 
	   $image_name= $datasiswa->id.'_official.png'; 

	   $params['data'] = "https://ksm.kemenag.go.id/publik/officialValiditas?q=".base64_encode($datasiswa->id)."&s=".base64_encode(str_replace(array("'","-",":","."," "),"",$datasiswa->nama)).""; 
	   $params['level'] = 'H'; 
	   $params['size'] = 10;
	   $params['savename'] = FCPATH.$config['imagedir'].$image_name; 
	   $this->ciqrcode->generate($params);
	   $foto ="";
	   if($datasiswa->foto !=""){
	   $foto = $_SERVER["DOCUMENT_ROOT"].'/__statics/upload/'.$datasiswa->foto;
	   }
	  


		
		  
		  
	   


		   $pdf->addPage('L');
		   
		   $pdf->setSourceFile('official.pdf');
	   
		   $tplIdx = $pdf->importPage(1);
		   $pdf ->useTemplate($tplIdx, null, null, null, null,FALSE);
		   
		   
		   $pdf->SetFont('Arial','B',26);
		   $pdf->SetTextColor(6, 0, 0);

		   $pdf->Cell(275,110,$datasiswa->nama,0,0,'C');

		   $pdf->Ln(35);
		   $pdf->SetFont('Arial','B',26);
		   $pdf->SetTextColor(6, 0, 0);
		   $pdf->Cell(275,93,$datasiswa->jabatan,0,0,'C');

		   
		   //$pdf->Write(0, '');


		   $pdf->Image("tmp/".$image_name,260,175,30,30);
		//    if($foto !=""){
		// 	$pdf->Image($foto,228,175,30,30);
		// 	}

		   $pdf->Output('I', 'Sertifikat '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf');



   }else{

	   echo "404 Not Found";
   }
 }



  public function hasilkonfirm(){

	  $tmsiswa_id = $this->input->get_post("tmsiswa_id");
	  $status     = $this->input->get_post("status");
	  $nilai      = $this->input->get_post("nilai");

	  $peserta = $this->db->query("select nla from v_siswa_kabkota where id='".$tmsiswa_id."'")->row();

	 
     $nla = $peserta->nla + $nilai;
	 $this->db->set("skrn",$status);
	 $this->db->where("id",$tmsiswa_id);
	 $this->db->update("tm_siswa");

	 $this->db->set("nla",$nla);
	 $this->db->where("idPs",$tmsiswa_id);
	 $this->db->update("nilai_kabkota");

	 echo "sukses";


  }
  

  public function generateNilaiKabkota(){

	 $data = $this->db->get("nilai_kabkota")->result();
	  foreach($data as $r){
		  $nilai = ($r->bnr_pg *4) + ($r->bnr_isi * 5) + ($r->slh * -1);

		  $this->db->set("nla",$nilai);
		  $this->db->where("idPs",$r->idPs);
		  $this->db->update("nilai_kabkota");



	  }
	  echo "sukses";

  }
  // Generate 

  public function generate_kabkota(){

	$kota = $this->db->get("kota")->result();
	$ketkabko = array("1"=>"Juara I","2"=>"Juara II","3"=>"Juara III","4"=>"Juara Harapan I","5"=>"Juara Harapan II","6"=>"Juara Harapan III");
	$this->db->set("d_p",0);		
	$this->db->set("ketkabko","");
	//$this->db->set("skrn",0);	
	$this->db->update("tm_siswa");

	foreach($kota as $rkota){

		$kompetisi = $this->db->get("tr_kompetisi")->result();
         
		  foreach($kompetisi as $rkom){

            $no=0;
			$peserta   = $this->db->query("select id from v_siswa_kabkota where kota_madrasah='{$rkota->id}' and nla IS NOT NULL and trkompetisi_id='{$rkom->id}' and sta !=0 order by nla DESC,jml_benar DESC,slh ASC,tgl_lahir DESC")->result();
				foreach($peserta as $rpes){
					$no++;
              

					$this->db->set("d_p",$no);		
					if($no <=6){											
					$this->db->set("ketkabko",$ketkabko[$no]);	
					}	
					if($no <=3){											
						$this->db->set("status2",1);	
					}												
					$this->db->where("id",$rpes->id);													
					$this->db->update("tm_siswa");


				}


		  }
	}
  echo "sukses";
  }


  // Peserta Provinsi 


	public function pesertaprov(){
		
		
		
		if(!$this->session->userdata("tmadmin_id")){
		   echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
			  exit();
		 }
	  
	   
								   
		  $ajax            = $this->input->get_post("ajax",true);	
		  $data['jenjang'] = base64_decode($this->input->get_post("jenjang",true));	
		  $data['title']   = "Provinsi Peserta ";
		  $data['dt']              = $this->Di->get_where("provinsi",array("id"=>$_SESSION['tmadmin_id']));
		if(!empty($ajax)){
					   
			$this->load->view('pesertaprov/page_peserta',$data);
		}else{
			
		   
			$data['konten'] = "pesertaprov/page_peserta";
			
			$this->load->view('pusdashboard/page_header',$data);
		}
   
	   
   }
   
   
   public function g_pesertaprov(){
		 error_reporting(0);
		 $iTotalRecords = $this->M_lokal->m_pesertaprov(false)->num_rows();
		 
		 $iDisplayLength = intval($_REQUEST['length']);
		 $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		 $iDisplayStart = intval($_REQUEST['start']);
		 $sEcho = intval($_REQUEST['draw']);
		 
		 $records = array();
		 $records["data"] = array(); 

		 $end = $iDisplayStart + $iDisplayLength;
		 $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		 
		 $datagrid = $this->M_lokal->m_pesertaprov(true)->result_array();
		 $arjenis = array("1"=>"Madrasah","2"=>"Sekolah");
		  $i= ($iDisplayStart +1);
		  foreach($datagrid as $val) {
											 
												
			   if($val['status']==0){
				   
				   $status ='<i class="badge badge-default"> Draft (belum dikirim) </i>';
				   
				}else if($val['status']==1){
					
				   $status ='<button class="btn btn-primary   btn-sm ubah" urlnya = "'.site_url("puslokal/verval").'"  datanya="'.$val['id'].'"  style"cursor:pointer"> <i class="fa fa-check-square"></i> Belum Diverifikasi   </button>';
					
				}else if($val['status']==2){
					 $status ='<button class="btn btn-success btn-sm ubah" urlnya = "'.site_url("puslokal/verval").'"  datanya="'.$val['id'].'"  style"cursor:pointer"> <i class="fa fa-check-square"></i> Lulus  </button>';
				   
				   }else if($val['status']==3){
					   $status ='<button class="btn btn-danger btn-sm ubah" urlnya = "'.site_url("puslokal/verval").'"  datanya="'.$val['id'].'"  style"cursor:pointer"><i class="fa fa-times"></i> Tidak Lulus </button>';
					 
				  }

			   $foto ="img/ksm.png";
				if(!empty($val['foto'])){

				   $foto ="upload/{$val['foto']}";
				}
				$img ='<img src="https://ksm.kemenag.go.id/__statics/'.$foto.'" class="img-responsive img-circle" style="width:40px">';
												
			   $no = $i++;
			   $notest = "<a href='".site_url("puslokal/kartutes?id=".($val['id'])."")."' target='_blank' >".$val['no_test2']."</a>";
			   $records["data"][] = array(
				   $no,
				   $this->Di->get_kondisi(array("id"=>$val['provinsi_madrasah']),"provinsi","nama"),
				   $this->Di->get_kondisi(array("id"=>$val['kota_madrasah']),"kota","nama"),
				   $notest,
				   $status,
				   $this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
				   $img,
				   
				   $val['nisn'],
				   '<a href="#" urlnya = "'.site_url("puslokal/f_peserta").'" class="" datanya="'.$val['id'].'">'.$val['nama'].'</a>',					
				   $val['gender'],
				   $val['tempat'].",".$this->Di->formattanggalstring($val['tgl_lahir']),
				   
				   $arjenis[$val['jenis']],
				   $this->Di->jenjangnama($val['jenjang']),
				   $val['username'],
				   $val['madrasah'],
				   $val['delegasi_hp'],
				   
				   
				   
									   
						   
			   
				  

				 );
			 }
	   
		 $records["draw"] = $sEcho;
		 $records["recordsTotal"] = $iTotalRecords;
		 $records["recordsFiltered"] = $iTotalRecords;
		 
		 echo json_encode($records);
   }


	public function hasiltesprov(){
		
		
		
		
		
									
		$ajax = $this->input->get_post("ajax",true);	
		$data['title']  = "Hasil Tes  ";
		$data['dt']              = $this->Di->get_where("provinsi",array("id"=>$_SESSION['tmadmin_id']));
		$data['jenjang'] = base64_decode($this->input->get_post("jenjang",true));
	   if(!empty($ajax)){
					  
		   $this->load->view('hasiltesprov/page_peserta',$data);
	   }else{
		   
		  
		   $data['konten'] = "hasiltesprov/page_peserta";
		   
		   $this->load->view('pusdashboard/page_header',$data);
	   }
  
	  
  }
  
 
  public function g_hasilprov(){
	error_reporting(0);
	$iTotalRecords = $this->M_lokal->m_hasilprov_jml(false);
	
	$iDisplayLength = intval($_REQUEST['length']);
	$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
	$iDisplayStart = intval($_REQUEST['start']);
	$sEcho = intval($_REQUEST['draw']);
	
	$records = array();
	$records["data"] = array(); 

	$end = $iDisplayStart + $iDisplayLength;
	$end = $end > $iTotalRecords ? $iTotalRecords : $end;
	
	$datagrid = $this->M_lokal->m_hasilprov(true)->result_array();
	$arjenis = array("1"=>"Madrasah","2"=>"Sekolah");
	$ketkabko = array("1"=>"Juara I","2"=>"Juara II","3"=>"Juara III","4"=>"Juara Harapan I","5"=>"Juara Harapan II","6"=>"Juara Harapan III");
	 $i= ($iDisplayStart +1);
	 foreach($datagrid as $val) {
										
										   
		$no = $i++;
	
		   $nonya = "<a href='".site_url('puslokal/sertifikatkota?q='.base64_encode($val['id']).'&nik='.base64_encode($val['nik_siswa']).'')."' target='_blank' >".$no."</a>";
		   $notest = "<a href='https://ksm.kemenag.go.id/pengawas/ruangan/berita-acara?id=".$val['id']."' target='_blank' >".$val['no_test2']."</a>";
		  // $img ='<center><img src="https://ksm.kemenag.go.id/__statics/'.$foto.'" class="img-responsive img-thumbnail" style="width:80px; border-radius:50%">'.$notest."<br>".$val['nama'].'<br>'.$val['tgl_lahir'].'</center>';
			$img ='<center>'.$val['nama']."<br>".$notest.'<br>'.$val['tgl_lahir'].'</center>';
										   
		  

		  $disabled="disabled";
			if($val['ctt']==1){
				$disabled="";
			}

			//$disabled="";

			$checked="";
			if($val['konf_prov']==1){
				$checked="checked";
			}
		 
		  $records["data"][] = array(
			  $nonya,
			  $img,	
			  $this->Di->get_kondisi(array("id"=>$val['provinsi_madrasah']),"provinsi","nama"),
			  $this->Di->get_kondisi(array("id"=>$val['kota_madrasah']),"kota","nama"),				  		
			  $this->Di->jenjangnama($this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","tmmadrasah_id"))."  ".$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
			  //($val['peringkat_prov'] <= 6) ? "<span class='badge badge-success'>".$ketkabko[$val['peringkat_prov']]."</span> Sebelumnya :".$ketkabko[$val['peringkatprov_seb']] : "<span class='badge badge-danger'>Peserta </span> Sebelumnya :".$ketkabko[$val['peringkatprov_seb']],
			  ($val['peringkat_prov'] <= 6) ? "<span class='badge badge-success'>".$ketkabko[$val['peringkat_prov']]."</span> ": "<span class='badge badge-danger'>Peserta </span>",
			  $val['nla'], 
			  $val['bnr_pg'],
			  $val['bnr_isi'],
			  $val['jml_benar'],
			  $val['slh'],
			  $val['ksg'],
			  $val['nilai_kabko'],
			 
			  
			  $arjenis[$val['jenis']],
			  $this->Di->jenjangnama($val['jenjang']),
			  $val['username'],
			  $val['madrasah'],
			  $val['ctt'],
			
			  '<input type="checkbox" class="konfirm" '.$disabled.' '.$checked.' value="1" tmsiswa_id="'.$val['id'].'">'
		
			  
			  
			  
								  
					  
		  
			 

			);
		}
  
	$records["draw"] = $sEcho;
	$records["recordsTotal"] = $iTotalRecords;
	$records["recordsFiltered"] = $iTotalRecords;
	
	echo json_encode($records);
}
  


public function hasilkonfirmprov(){

	$tmsiswa_id = $this->input->get_post("tmsiswa_id");
	$status     = $this->input->get_post("status");
	$nilai      = $this->input->get_post("nilai");

	$peserta = $this->db->query("select nla from v_siswa_provinsi where id='".$tmsiswa_id."'")->row();

   
   $nla = $peserta->nla + $nilai;
   $this->db->set("konf_prov",$status);
   $this->db->where("id",$tmsiswa_id);
   $this->db->update("tm_siswa");

   $this->db->set("nla",$nla);
   $this->db->where("idPs",$tmsiswa_id);
   $this->db->update("nilai_provinsi");

   echo "sukses";


}


public function generate_provinsijuara(){

	$kota = $this->db->get("provinsi")->result();
	$ketkabko = array("1"=>"Juara I","2"=>"Juara II","3"=>"Juara III","4"=>"Juara Harapan I","5"=>"Juara Harapan II","6"=>"Juara Harapan III");
	$this->db->set("status3",0);		
	$this->db->set("peringkat_prov",0);	
	$this->db->update("tm_siswa");

	foreach($kota as $rkota){

		$kompetisi = $this->db->get("tr_kompetisi")->result();
         
		  foreach($kompetisi as $rkom){

            $no=0;
			$peserta   = $this->db->query("select id from v_siswa_provinsi where provinsi_madrasah='{$rkota->id}' and nla IS NOT NULL and trkompetisi_id='{$rkom->id}' order by nla DESC,jml_benar DESC,slh ASC,nilai_kabko DESC, tgl_lahir DESC")->result();
				foreach($peserta as $rpes){
					$no++;
              

					$this->db->set("peringkat_prov",$no);		
					

					if($no <=1){											
						$this->db->set("status3",1);	
					}												
					$this->db->where("id",$rpes->id);													
					$this->db->update("tm_siswa");


				}


		  }
	}
  echo "sukses";
  }

  // Peserta Nasional


	public function pesertanas(){
		
		
		
		if(!$this->session->userdata("tmadmin_id")){
		   echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
			  exit();
		 }
	  
	   
								   
		  $ajax            = $this->input->get_post("ajax",true);	
		  $data['jenjang'] = base64_decode($this->input->get_post("jenjang",true));	
		  $data['title']   = "Nasional Peserta ";
		  
		if(!empty($ajax)){
					   
			$this->load->view('pesertanas/page_peserta',$data);
		}else{
			
		   
			$data['konten'] = "pesertanas/page_peserta";
			
			$this->load->view('pusdashboard/page_header',$data);
		}
   
	   
   }
   
   
   public function g_pesertanas(){
		 error_reporting(0);
		 $iTotalRecords = $this->M_lokal->m_pesertanas(false)->num_rows();
		 $ketkabko = array("1"=>"Juara I","2"=>"Juara II","3"=>"Juara III","4"=>"Juara Harapan I","5"=>"Juara Harapan II","6"=>"Juara Harapan III");
		 $iDisplayLength = intval($_REQUEST['length']);
		 $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		 $iDisplayStart = intval($_REQUEST['start']);
		 $sEcho = intval($_REQUEST['draw']);
		 
		 $records = array();
		 $records["data"] = array(); 

		 $end = $iDisplayStart + $iDisplayLength;
		 $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		 
		 $datagrid = $this->M_lokal->m_pesertanas(true)->result_array();
		 $arjenis = array("1"=>"Madrasah","2"=>"Sekolah");
		  $i= ($iDisplayStart +1);
		  foreach($datagrid as $val) {
			$no = $i++;						 
												
			   if($val['status']==0){
				   
				   $status ='<i class="badge badge-default"> Draft (belum dikirim) </i>';
				   
				}else if($val['status']==1){
					
				   $status ='<button class="btn btn-primary   btn-sm ubah" urlnya = "'.site_url("puslokal/verval").'"  datanya="'.$val['id'].'"  style"cursor:pointer"> <i class="fa fa-check-square"></i> '.$ketabko[$val['peringkat_prov']].' Prov   </button>';
					
				}else if($val['status']==2){
					 $status ='<button class="btn btn-success btn-sm ubah" urlnya = "'.site_url("puslokal/verval").'"  datanya="'.$val['id'].'"  style"cursor:pointer"> <i class="fa fa-check-square"></i> '.$ketkabko[$val['peringkat_prov']].' Prov  </button>';
				   
				   }else if($val['status']==3){
					   $status ='<button class="btn btn-danger btn-sm ubah" urlnya = "'.site_url("puslokal/verval").'"  datanya="'.$val['id'].'"  style"cursor:pointer"><i class="fa fa-times"></i> '.$ketkabko[$val['peringkat_prov']].' Prov </button>';
					 
				  }

				  $status = '<a class="btn btn-primary   btn-sm " href = "'.site_url("puslokal/idPeserta?id=".$val['id']."&no=".$no."").'"    style"cursor:pointer"> <i class="fa fa-check-square"></i> ID Peserta  </button>';

			   $foto ="img/ksm.png";
				if(!empty($val['foto'])){

				   $foto ="upload/{$val['foto']}";
				}
				$img ='<img src="https://ksm.kemenag.go.id/__statics/'.$foto.'" class="img-responsive img-circle" style="width:40px">';
												
			  
			   $notest = "<a href='".site_url("puslokal/kartutes?id=".($val['id'])."")."' target='_blank' >".$val['no_test3']."</a>";
			   $records["data"][] = array(
				   $no,
				   $this->Di->get_kondisi(array("id"=>$val['provinsi_madrasah']),"provinsi","nama"),
				   $this->Di->get_kondisi(array("id"=>$val['kota_madrasah']),"kota","nama"),
				   $status,
				   $notest,
				   
				   $this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
				   $img,
				   
				   $val['nisn'],
				   '<a href="#" urlnya = "'.site_url("puslokal/verval").'" class="ubah" datanya="'.$val['id'].'">'.$val['nama'].'</a>',					
				   $val['gender'],
				   $val['tempat'].",".$this->Di->formattanggalstring($val['tgl_lahir']),
				   
				   $arjenis[$val['jenis']],
				   $this->Di->jenjangnama($val['jenjang']),
				   $val['username'],
				   $val['madrasah'],
				   $val['delegasi_hp'],
				   
				   
				   
									   
						   
			   
				  

				 );
			 }
	   
		 $records["draw"] = $sEcho;
		 $records["recordsTotal"] = $iTotalRecords;
		 $records["recordsFiltered"] = $iTotalRecords;
		 
		 echo json_encode($records);
   }


   public function generateMadrasahProv(){

	  $madrasah = $this->db->get("tm_madrasah")->result();
	     foreach($madrasah as $rm){
           $juara1 = $this->db->query("select count(id) as jml from v_siswa_provinsi where peringkat_prov=1 AND tmmadrasah_id='{$rm->id}'")->row();
           $juara2 = $this->db->query("select count(id) as jml from v_siswa_provinsi where peringkat_prov=2 AND tmmadrasah_id='{$rm->id}'")->row();
           $juara3 = $this->db->query("select count(id) as jml from v_siswa_provinsi where peringkat_prov=3 AND tmmadrasah_id='{$rm->id}'")->row();
           $harapan1 = $this->db->query("select count(id) as jml from v_siswa_provinsi where peringkat_prov=4 AND tmmadrasah_id='{$rm->id}'")->row();
           $harapan2 = $this->db->query("select count(id) as jml from v_siswa_provinsi where peringkat_prov=5 AND tmmadrasah_id='{$rm->id}'")->row();
           $harapan3 = $this->db->query("select count(id) as jml from v_siswa_provinsi where peringkat_prov=6 AND tmmadrasah_id='{$rm->id}'")->row();

		   $this->db->set("juara1_prov",$juara1->jml);
		   $this->db->set("juara2_prov",$juara2->jml);
		   $this->db->set("juara3_prov",$juara3->jml);
		   $this->db->set("harapan1_prov",$harapan1->jml);
		   $this->db->set("harapan2_prov",$harapan2->jml);
		   $this->db->set("harapan3_prov",$harapan3->jml);
		   $this->db->where("id",$rm->id);
		   $this->db->update("tm_madrasah");

		 }

		 echo "sukses";
   }

   public function generateMadrasahNas(){
	$this->db->set("juara1_nas",0);
	$this->db->set("juara2_nas",0);
	$this->db->set("juara3_nas",0);
	$this->db->update("tm_madrasah");


	$madrasah = $this->db->get("tm_madrasah")->result();
	   foreach($madrasah as $rm){
		 $juara1 = $this->db->query("select count(id) as jml from v_siswa_nasional where nasional_juara='emas' AND tmmadrasah_id='{$rm->id}'")->row();
		 $juara2 = $this->db->query("select count(id) as jml from v_siswa_nasional where nasional_juara='perak' AND tmmadrasah_id='{$rm->id}'")->row();
		 $juara3 = $this->db->query("select count(id) as jml from v_siswa_nasional where nasional_juara='perunggu' AND tmmadrasah_id='{$rm->id}'")->row();
		

		 $this->db->set("juara1_nas",$juara1->jml);
		 $this->db->set("juara2_nas",$juara2->jml);
		 $this->db->set("juara3_nas",$juara3->jml);

		 $this->db->where("id",$rm->id);
		 $this->db->update("tm_madrasah");

	   }

	   redirect(site_url("puslokal/statistikNasional"));
 }




	public function pemenang(){
		
		
		
		if(!$this->session->userdata("tmadmin_id")){
		   echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
			  exit();
		 }
	  
	   
								   
		  $ajax            = $this->input->get_post("ajax",true);	
		  $data['jenjang'] = base64_decode($this->input->get_post("jenjang",true));	
		  $data['title']   = "Nasional Pemenang ";
		  
		if(!empty($ajax)){
					   
			$this->load->view('nasional/hasiltes',$data);
		}else{
			
		   
			$data['konten'] = "nasional/hasiltes";
			
			$this->load->view('pusdashboard/page_header',$data);
		}
   
	   
   }
   
   
   public function g_pemenangNas(){
		 error_reporting(0);
		 $iTotalRecords = $this->M_lokal->m_hasilNasional(false)->num_rows();
		 $jenjang = array("1"=>"MI/SD","2"=>"MTs/SMP","3"=>"MA/SMA");
		 $iDisplayLength = intval($_REQUEST['length']);
		 $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		 $iDisplayStart = intval($_REQUEST['start']);
		 $sEcho = intval($_REQUEST['draw']);
		 
		 $records = array();
		 $records["data"] = array(); 

		 $end = $iDisplayStart + $iDisplayLength;
		 $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		 
		 $datagrid = $this->M_lokal->m_hasilNasional(true)->result_array();
		 $arjenis = array("1"=>"Madrasah","2"=>"Sekolah");
		  $i= ($iDisplayStart +1);
		  foreach($datagrid as $val) {
			  $cbtNasional = $this->db->get_where("nilai_nasional",array("idPs"=>$val['id']))->row();							 
		

			   $foto ="img/ksm.png";
				if(!empty($val['foto'])){

				   $foto ="upload/{$val['foto']}";
				}
				$img ='<img src="https://ksm.kemenag.go.id/__statics/'.$foto.'" class="img-responsive circle" style="width:80px;height:80px;border-radius: 30%;">';
												
			   $no = $i++;
				$nasional_juara="-";
			   if($val['nasional_juara']=="emas"){

				   $nasional_juara = '<span class="badge badge-warning"> Emas </span>';
			   }else if($val['nasional_juara']=="perak"){

			    	$nasional_juara = '<span class="badge badge-info"> Perak </span>';
			   }else if($val['nasional_juara']=="perunggu"){
				$nasional_juara = '<span class="badge badge-dark"> Perunggu </span>';
			   }
			
			   $records["data"][] = array(
				   $val['nasional_peringkat'],
				   $img,
				   $val['no_test3'],
				   $val['nama'],
				   $this->Di->get_kondisi(array("id"=>$val['provinsi_madrasah']),"provinsi","nama"),
				   $val['madrasah'],
				   $this->Di->jenjangnama($val['jenjang'])." ".$this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),
				   $nasional_juara,
				   $val['nasional_nilai_akhir'],
				   $val['nasional_cbt'],
				   $val['nasional_eksplorasi'],
				   
				   $cbtNasional->bnr_pg,
				   $cbtNasional->bnr_isi,
				   $cbtNasional->bnr,
				   $cbtNasional->slh,
				   $cbtNasional->ksg,
				   $val['nilai_provinsi'],
				   $val['jml_benar_provinsi'],
				   $val['slh_provinsi'],
				   $val['ksg_provinsi'],

				   $val['nilai_kabko'],
				   $val['jml_benar_kabko'],
				   $val['slh_kabko'],
				   $val['ksg_kabko'],
				   $val['nik_siswa'],
				   $val['nisn'],
				   $val['tempat'],
				   $this->Di->formattanggaldb($val['tgl_lahir']),
				   $val['nik_ayah'],
				   $val['nama_ayah'],
				   $val['nik_ibu'],
				   $val['nama_ibu'],		   
				
				   $this->Di->get_kondisi(array("id"=>$val['kota']),"kota","nama"),
				   $this->Di->get_kondisi(array("id"=>$val['kecamatan']),"kecamatan","nama"),
				   $this->Di->get_kondisi(array("id"=>$val['desa']),"desa","nama"),
				  
				   
				   $arjenis[$val['jenis']],
				   $this->Di->jenjangnama($val['jenjang']),
				   $val['username'],
				   $val['madrasah'],
				   $val['delegasi_nik'],
				   $val['delegasi_nama'],
				  
				   $val['delegasi_hp'],

				   $val['bnr_pg'],
				   $val['bnr_isi'],
				   $val['slh'],
				   $val['ksg'],
				   $val['nla'],
				   $this->Di->get_kondisi(array("tmsiswa_id"=>$val['id']),"h_ujian","list_jawaban"),
				   $val['nilai_provinsi'],
				   $val['nilai_kabko']
				   
				   
									   
						   
			   
				  

				 );
			 }
	   
		 $records["draw"] = $sEcho;
		 $records["recordsTotal"] = $iTotalRecords;
		 $records["recordsFiltered"] = $iTotalRecords;
		 
		 echo json_encode($records);
   }


     public function generateNilaiNasional(){

		$siswa = $this->db->query("select * from v_siswa_nasional")->result();

		   foreach($siswa as $row){

				$eks = $this->Di->get_kondisi(array("tmsiswa_id"=>$row->id),"h_ujian","nilai");
				$nasional_nilai_akhir = $eks + $row->nla;

				$this->db->set("nasional_cbt",$row->nla);
				$this->db->set("nasional_eksplorasi",$eks);
				$this->db->set("nasional_nilai_akhir",$nasional_nilai_akhir);
				$this->db->where("id",$row->id);
				$this->db->update("tm_siswa");

		   }

		   redirect(site_url("puslokal/statistikNasional"));
	 }


	public function statistikNasional(){

		if(!$this->session->userdata("tmadmin_id")){
			echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("ksm/login")."'>disini </a></center>";
			   exit();
		  }
	  					
		   $ajax            = $this->input->get_post("ajax",true);	
		   $data['jenjang'] = base64_decode($this->input->get_post("jenjang",true));	
		   $data['title']   = "Statistik Nasional ";
		   $pie   					 = "";
		   $grid = array();
		   $kategori = array("1"=>"Madrasah Negeri","2"=>"Madrasah Swasta","3"=>"Sekolah Negeri","4"=>"Sekolah Swasta");
		   $status   = array("1"=>"jenis='1' and status_madrasah='1'","2"=>"jenis='1' and status_madrasah='2'","3"=>"jenis='2' and status_madrasah='1'","4"=>"jenis='2' and status_madrasah='2'");
			foreach($kategori as $index=>$row){
			  
			  $emas = $this->db->query("select count(id) as jml from v_siswa_nasional where ".$status[$index]." and nasional_juara='emas' and jenjang_madrasah=3")->row();
			  $perak = $this->db->query("select count(id) as jml from v_siswa_nasional where ".$status[$index]." and nasional_juara='perak' and jenjang_madrasah=3")->row();
			  $perunggu = $this->db->query("select count(id) as jml from v_siswa_nasional where ".$status[$index]." and nasional_juara='perunggu' and jenjang_madrasah=3")->row();

					 $pm = $emas->jml+$perak->jml+$perunggu->jml;
					 $tempo = array("kategori"=>$row,"emas"=>$emas->jml,"perak"=>$perak->jml,"perunggu"=>$perunggu->jml);
					 $pie          .=",['".$row."',".(($pm/count($status))*100)."]";
					 $grid[] = $tempo;
									   
			 }

			 $data['json_pie_chart']  =  substr($pie,1);
			 $data['grid'] = $grid;

			 $data['provinsiName']    = "";
			 $data['provinsiNameData']    = "";
			 $data['emas']    = "";
			 $data['perak']    = "";
			 $data['perunggu']    = "";
			 $provinsi = $this->db->query("select * from provinsi order by emas desc,perak desc,perunggu desc, nama ASC")->result();
			 	  foreach($provinsi as $r){

							$data["provinsiName"] .=",'".$r->nama."'";
							$data["emas"] .=",".$r->emas."";
							$data["perak"] .=",".$r->perak."";
							$data["perunggu"] .=",".$r->perunggu."";
				   }

			 $data['provinsiNameData']    = substr($data['provinsiName'], 1);
			 
			 $data['emasData']    = substr($data['emas'], 1);
			 $data['perakData']    = substr($data['perak'], 1);
			 $data['perungguData']    = substr($data['perunggu'], 1);

		   
		 if(!empty($ajax)){
						
			 $this->load->view('nasional/statistik',$data);
		 }else{
			 
			
			 $data['konten'] = "nasional/statistik";
			 
			 $this->load->view('pusdashboard/page_header',$data);
		 }
	
	}


public function generate_Nasionaljuara(){

    $this->db->set("nasional_peringkat",0);
    $this->db->set("nasional_juara","");
	$this->db->update("tm_siswa");

	$medali = array("1"=>"emas",
				"2"=>"emas",
				"3"=>"emas",
				"4"=>"perak",
				"5"=>"perak",
				"6"=>"perak",
				"7"=>"perak",
				"8"=>"perak",
				"9"=>"perunggu",
				"10"=>"perunggu",
				"11"=>"perunggu",
				"12"=>"perunggu",
				"13"=>"perunggu",
				"14"=>"perunggu",
				"15"=>"perunggu");


		$kompetisi = $this->db->get("tr_kompetisi")->result();
         
		  foreach($kompetisi as $rkom){

            $no=0;
			$peserta   = $this->db->query("select id from v_siswa_nasional where  trkompetisi_id='{$rkom->id}' order by nasional_nilai_akhir DESC,nasional_cbt DESC,nasional_eksplorasi DESC, jml_benar DESC,slh ASC,nilai_provinsi DESC,jml_benar_provinsi DESC,slh_provinsi ASC,nilai_kabko DESC,jml_benar_kabko DESC,slh_kabko ASC,tgl_lahir DESC")->result();
				foreach($peserta as $rpes){
					$no++;
              

					$this->db->set("nasional_peringkat",$no);		
					

					if($no <=15){											
						$this->db->set("nasional_juara",$medali[$no]);
					}												
					$this->db->where("id",$rpes->id);													
					$this->db->update("tm_siswa");


				}


		  }
	
		  redirect(site_url("puslokal/statistikNasional"));
  }


  public function raihanmedali(){
	$this->db->set("emas",0);
	$this->db->set("perak",0);
	$this->db->set("perunggu",0);
	$this->db->update("provinsi");


	$p = $this->db->get("provinsi")->result();
	  foreach($p as $r){
		  
		  $emas = $this->db->query("select count(id) as jml from v_siswa_nasional where provinsi_madrasah='".$r->id."' and nasional_juara='emas'")->row();
		  $perak = $this->db->query("select count(id) as jml from v_siswa_nasional where provinsi_madrasah='".$r->id."' and nasional_juara='perak'")->row();
		  $perunggu = $this->db->query("select count(id) as jml from v_siswa_nasional where provinsi_madrasah='".$r->id."' and nasional_juara='perunggu'")->row();
		  
		  $this->db->set("emas",$emas->jml);
		  $this->db->set("perak",$perak->jml);
		  $this->db->set("perunggu",$perunggu->jml);
		  $this->db->where("id",$r->id);
		  $this->db->update("provinsi");
		
		  
		  
	  }
	  redirect(site_url("puslokal/statistikNasional"));
	
}

public function tabulasiMedaliProvinsi(){
		
		
		
		
		
									
	$ajax = $this->input->get_post("ajax",true);	
	$data['title']  = "Tabulasi Medali Provinsi   ";
	
	  if(!empty($ajax)){
					 
		  $this->load->view('tabulasi_medali',$data);
	  }else{
		  
		 
		  $data['konten'] = "tabulasi_medali";
		  
		  $this->load->view('pusdashboard/page_header',$data);
	  }

	}


	public function peringkatmadrasahnas(){
		
		
		
		
		
									
		$ajax = $this->input->get_post("ajax",true);	
		$data['title']  = "Tabulasi Madrasah Juara   ";
		
		  if(!empty($ajax)){
						 
			  $this->load->view('nasional/peringkat_madrasah',$data);
		  }else{
			  
			 
			  $data['konten'] = "nasional/peringkat_madrasah";
			  
			  $this->load->view('pusdashboard/page_header',$data);
		  }
	
		}


	public function g_MadrasahNasPeringkat(){
		error_reporting(0);
		$iTotalRecords = $this->M_lokal->m_madrasahNas(false)->num_rows();
		
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);
		
		$records = array();
		$records["data"] = array(); 

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;
		
		$datagrid = $this->M_lokal->m_madrasahNas(true)->result_array();
	
			 $i= ($iDisplayStart +1);
		 foreach($datagrid as $val) {

			 
							
			$no = $i++; 
			$this->db->set("rangking",$no);
			$this->db->where("id",$val['id']);
			$this->db->update("tm_madrasah");

			  $records["data"][] = array(
				  $val['rangking'],	
				  $val['nama'],			
				  $this->Di->get_kondisi(array("id"=>$val['provinsi']),"provinsi","nama"),
				  $this->Di->get_kondisi(array("id"=>$val['kota']),"kota","nama"),				  		
				
				  $val['juara1_nas'],
				  $val['juara2_nas'],
				  $val['juara3_nas']
				
				 
				  
									  
						  
			  
				 

				);
			}
	  
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		
		echo json_encode($records);
  }




  // Akun 

  public function provinsi(){
		
		
		
		 
		
									
	$ajax = $this->input->get_post("ajax",true);	
	$id = $this->input->get_post("id",true);	
	$data['title']  = "Akun Kanwil ";
	$data['id']  = $id;
	
	if(!empty($ajax)){
				   
		$this->load->view('official/page_provinsi',$data);
	}else{
		
	   
		$data['konten'] = "official/page_provinsi";
		
		$this->load->view('pusdashboard/page_header',$data);
	}

   
}


public function g_provinsi(){
   error_reporting(0);
	 $iTotalRecords = $this->M_lokal->g_provinsi(false)->num_rows();
	 
	 $iDisplayLength = intval($_REQUEST['length']);
	 $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
	 $iDisplayStart = intval($_REQUEST['start']);
	 $sEcho = intval($_REQUEST['draw']);
	 
	 $records = array();
	 $records["data"] = array(); 

	 $end = $iDisplayStart + $iDisplayLength;
	 $end = $end > $iTotalRecords ? $iTotalRecords : $end;
	 
	 $datagrid = $this->M_lokal->g_provinsi(true)->result_array();
	  
	  $i= ($iDisplayStart +1);
	  foreach($datagrid as $val) {
								   
	  	   $no = $i++;
		   $records["data"][] = array(
			   $no,
			  
			   $val['nama'],
			   $val['username'],				
			   $val['password']
			   
			   	   
					   
		   
			  

			 );
		 }
   
	 $records["draw"] = $sEcho;
	 $records["recordsTotal"] = $iTotalRecords;
	 $records["recordsFiltered"] = $iTotalRecords;
	 
	 echo json_encode($records);
}

public function generateAkunProvinsi(){

	$data = $this->db->get("provinsi")->result();
	  foreach($data as $rdata){

		$password = $this->Di->randomangka(6);
		$this->db->set("password",$password);
		$this->db->where("id",$rdata->id);
		$this->db->update("provinsi");
	  }

	
}

// Kabko

public function kabko(){
		
		
		
		 
		
									
	$ajax = $this->input->get_post("ajax",true);	
	$id = $this->input->get_post("id",true);	
	$data['title']  = "Akun Kabkota ";
	
	
	if(!empty($ajax)){
				   
		$this->load->view('official/page_kabko',$data);
	}else{
		
	   
		$data['konten'] = "official/page_kabko";
		
		$this->load->view('pusdashboard/page_header',$data);
	}

   
}


public function g_kabko(){
   error_reporting(0);
	 $iTotalRecords = $this->M_lokal->g_kabko(false)->num_rows();
	 
	 $iDisplayLength = intval($_REQUEST['length']);
	 $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
	 $iDisplayStart = intval($_REQUEST['start']);
	 $sEcho = intval($_REQUEST['draw']);
	 
	 $records = array();
	 $records["data"] = array(); 

	 $end = $iDisplayStart + $iDisplayLength;
	 $end = $end > $iTotalRecords ? $iTotalRecords : $end;
	 
	 $datagrid = $this->M_lokal->g_kabko(true)->result_array();
	  
	  $i= ($iDisplayStart +1);
	  foreach($datagrid as $val) {
		  $jmlPeserta = $this->db->query("SELECT count(id) as jml from v_siswa where status=2 and kota_madrasah='".$val['id']."' and lokasi_kabko=0")->row();	
		  
		  
	  	   $no = $i++;
		   $records["data"][] = array(
			   $no,
			  
			   $this->Di->get_kondisi(array("id"=>$val['provinsi_id']),"provinsi","nama"),
			   $val['nama'],
			   $val['username'],				
			   $val['password'],
			   $jmlPeserta->jml
			   
			   	   
					   
		   
			  

			 );
		 }
   
	 $records["draw"] = $sEcho;
	 $records["recordsTotal"] = $iTotalRecords;
	 $records["recordsFiltered"] = $iTotalRecords;
	 
	 echo json_encode($records);
}

public function generateAkunKabko(){

	$data = $this->db->get("kota")->result();
	  foreach($data as $rdata){

		$password = $this->Di->randomangka(6);
		$this->db->set("password",$password);
		$this->db->where("id",$rdata->id);
		$this->db->update("kota");
	  }

	
}

// Ruang 



public function ruang(){
		
		
		
		 
		
									
	$ajax = $this->input->get_post("ajax",true);	
	$id = $this->input->get_post("id",true);	
	$data['title']  = "Ruang Kabkota ";
	
	
	if(!empty($ajax)){
				   
		$this->load->view('official/page_ruang',$data);
	}else{
		
	   
		$data['konten'] = "official/page_ruang";
		
		$this->load->view('pusdashboard/page_header',$data);
	}

   
}


public function g_ruang(){
   error_reporting(0);
	 $iTotalRecords = $this->M_lokal->g_ruang(false)->num_rows();
	 
	 $iDisplayLength = intval($_REQUEST['length']);
	 $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
	 $iDisplayStart = intval($_REQUEST['start']);
	 $sEcho = intval($_REQUEST['draw']);
	 
	 $records = array();
	 $records["data"] = array(); 

	 $end = $iDisplayStart + $iDisplayLength;
	 $end = $end > $iTotalRecords ? $iTotalRecords : $end;
	 
	 $datagrid = $this->M_lokal->g_ruang(true)->result_array();
	  
	  $i= ($iDisplayStart +1);
	  foreach($datagrid as $val) {
			//$jmlPeserta = $this->db->query("SELECT COUNT(id) as jml from tm_siswa where id_ruang1='{$val['id']}'")->row();			   
	  	   $no = $i++;
		   $records["data"][] = array(
			   $no,
			   $val['id'],	
			   $val['ruang'],	
			   $this->Di->get_kondisi(array("id"=>$val['provinsi_id']),"provinsi","nama"),
			   $this->Di->get_kondisi(array("id"=>$val['kota_id']),"kota","nama"),
			   
			   $val['provinsi_id'],
			   $val['kota_id'],	
			   $this->Di->get_kondisi(array("id"=>$val['trkompetisi_id']),"tr_kompetisi","nama"),			
			   $val['kode'],
			   $val['password'],
			   "0",
			  // $jmlPeserta->jml
			   
			   	   
					   
		   
			  

			 );
		 }
   
	 $records["draw"] = $sEcho;
	 $records["recordsTotal"] = $iTotalRecords;
	 $records["recordsFiltered"] = $iTotalRecords;
	 
	 echo json_encode($records);
}


public function pesertakabko(){
		
		
		
		 
		
									
	$ajax = $this->input->get_post("ajax",true);	
	$id = $this->input->get_post("id",true);	
	$data['title']  = "Ruang Kabkota ";
	
	
	if(!empty($ajax)){
				   
		$this->load->view('official/pesertakabko',$data);
	}else{
		
	   
		$data['konten'] = "official/pesertakabko";
		
		$this->load->view('pusdashboard/page_header',$data);
	}

   
}


public function g_pesertakabko(){
   //error_reporting(0);
	 $iTotalRecords = $this->M_lokal->g_pesertakabko(false)->num_rows();
	 
	 $iDisplayLength = intval($_REQUEST['length']);
	 $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
	 $iDisplayStart = intval($_REQUEST['start']);
	 $sEcho = intval($_REQUEST['draw']);
	 
	 $records = array();
	 $records["data"] = array(); 
	 $jenjangArr = array("1"=>"MI/SD","2"=>"MTs/SMP","3"=>"MA/SMA");
	 $end = $iDisplayStart + $iDisplayLength;
	 $end = $end > $iTotalRecords ? $iTotalRecords : $end;
	 
	 $datagrid = $this->M_lokal->g_pesertakabko(true)->result_array();
	  
	  $i= ($iDisplayStart +1);
	  foreach($datagrid as $val) {
						   
	  	   $no = $i++;
		   $records["data"][] = array(
			   $no,
			   $val['id'],	
			   $val['no_test'],	
			   $val['nama'],	
			   $val['tgl_lahir'],	
			   
			   $val['trkompetisi_id'],
			 
			   $val['id_sesi'],	
			   $val['hari'],	
			   $val['sesi'],	
			   $val['id_ruang'],	
			   $val['ruang'],	
			   $val['foto_dir'],	
			   $val['foto_drive'],	
			   $val['kartupelajar'],	
			  
			   
			   $val['provinsi'],
			   $val['kota']
			 
			   	   
					   
		   
			  

			 );
		 }
   
	 $records["draw"] = $sEcho;
	 $records["recordsTotal"] = $iTotalRecords;
	 $records["recordsFiltered"] = $iTotalRecords;
	 
	 echo json_encode($records);
}


		public function idPeserta(){

			
			$id = $this->input->get_post("id");
			$no = $this->input->get_post("no");
			$datasiswaArray = $this->db->query("select * from v_siswa where id='".$id."' order by provinsi_madrasah ASC, trkompetisi_id ASC ")->result();
			
		
			$pdf = new Fpdi();
			foreach($datasiswaArray as $datasiswa){
		
			
				$kompetisi = $this->Di->get_kondisi(array("id"=>$datasiswa->trkompetisi_id),"tr_kompetisi","nama");
				$provinsi = $this->Di->get_kondisi(array("id"=>$datasiswa->provinsi_madrasah),"provinsi","nama");
				$jenjangn = array("1"=>"MI/SD ","2"=>"MTs/SMP","3"=>"MA/SMA");
				$jenjang  = strtoupper($jenjangn[$datasiswa->jenjang_madrasah]);
				$kompetisi = $jenjang." ".strtoupper($kompetisi);
			
		
		
					$pdf->addPage('P');
					
					$pdf->setSourceFile('idPeserta.pdf');
				
					$tplIdx = $pdf->importPage(1);
					$pdf ->useTemplate($tplIdx, null, null, null, null,TRUE);
		
					$pdf->Ln(76);
					$pdf->SetFont('Arial','B',10);
					$pdf->SetTextColor(6, 0, 0);
					$pdf->Cell(null,0,$datasiswa->nama,0,0,'C');
		
					$pdf->Ln(4);
					$pdf->SetFont('Arial','B',9);
					$pdf->SetTextColor(6, 0, 0);
					$pdf->Cell(null,0,$kompetisi,0,0,'C');
		
					$pdf->Ln(4);
					$pdf->SetFont('Arial','B',5);
					$pdf->SetTextColor(6, 0, 0);
					$pdf->Cell(null,0,$datasiswa->madrasah." - ".$provinsi,0,0,'C');
		
				// $pdf->Image($datasiswa->file_foto_dir."",30,40,30,30);
					$pdf->Image("__statics/upload/".$datasiswa->foto,30,40,30,30);
					//echo $no."-".$provinsi.'-idPeserta  '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf';
				$pdf->Output('D',$no."-".$provinsi.'-idPeserta  '.str_replace(array("'","-",":","."),"",$datasiswa->nama).'.pdf',TRUE);
		
					
				}
		}
	


		public function daftarPeserta(){
	    
	    	 		 
			$ret = '';
			
			$download_pdf=TRUE;
			$j = array("1"=>"MI/SD","2"=>"MTs/SMP","3"=>"MA/SMA");
			$kompetisi = $this->db->get_where("tr_kompetisi",array("id"=>$_GET['kompetisi']))->row();
			
			$pdf_filename = 'Peserta '.$j[$kompetisi->tmmadrasah_id]."-".$kompetisi->nama.'.pdf';	 
			$data_header = array('title' => 'Kartu Tes',);
			$data = array();
			
			$user_info = $this->load->view('kartu/absensipeserta',$data, true);
		
			 $output = $user_info;
			
			
			
			generate_pdf($output, $pdf_filename,TRUE);
			
	
}


}
