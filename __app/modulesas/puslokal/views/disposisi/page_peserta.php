
   <div id="showform"></div>        
                                 
					  <div class="card form" id="form1">
                        <div class="card-header">
                           <?php 
						  $arje = array("1"=>"MI/SD Sederajat","2"=>"MTS/SMP Sederajat","3"=>"MA/SMA Sederajat");
						 ?>
                            <h3><i class="fa fa-users"></i> Disposisi Peserta   <?php echo $arje[$jenjang]; ?> Ke Tkt Provinsi </h3>
                        </div>
                        <br>
                         
                            <div class="col-md-12">
							<div class="alert alert-info">
							 PETUNJUK ! <br>
							 SILAHKAN LAKUKAN DISPOSISI  PESERTA  KE TINGKAT PROVINSI UNTUK PESERTA RANGKING  1 - 3 JUARA KSM TINGKAT KABUPATEN/KOTA <br>
							 PASTIKAN JUMLAH HASIL INPUT NILAI TES SESUAI DENGAN JUMLAH SOAL, JIKA TIDAK SESUAI, MAKA PESERTA TIDAK DAPAT DIKIRIM KE TINGKAT PROVINSI SEBELUM NILAINYA DIPERBAIKI 
						     SILAHKAN CEK PADA KOLOM KETERANGAN DI TABEL DIBAWAH INI. <br>
			                 STATUS JUARA MENGIKUTI HASIL NILAI YANG ANDA INPUT 
			 
			              </div>
						  
						  <form>
							<div class="col-md-12">
					 					 <div class="row">
										 
										    <div class="col-md-2"> <b> PENCARIAN  : </b></div>
                                           
										   
										
										 
										  
										     <div class="col-md-3">
												<div class="form-group ">
													<select id="trkompetisi_id" class="form-control">
													<option value=""> - Cari kompetisi -</option>
													  <?php 
													    $ko = $this->db->get_where("tr_kompetisi",array("tmmadrasah_id"=>$jenjang))->result();
														  foreach($ko as $r){
															?> <option value="<?php echo $r->id; ?>" > <?php echo $r->nama; ?> </option><?php   
															  
														  }
														  
														?>
													  
													  
													
													</select>
												</div>
										  </div>
										  
										  
										   <div class="col-md-3">
													
													<div class="form-group">
															<input type="text" size="30" name="keyword" id="keyword" class="form-control" placeholder="Cari Nama Siswa ">
															<input type="hidden" id="jenjang" class="form-control" value="<?php echo $jenjang; ?>">
															
														
												   </div>
											 </div>
										
										  
										 
										 </div>
									
							
					</div>
					</div>
			</form>
			
		           <hr>
		<div class="col-md-12">  
		          
					 
					    
		

				      <div style="width: 100%;overflow-x: auto; white-space: nowrap;">
			<table id="datatableTable" class="  table-bordered table-hover table-striped" width="100%">
			  <thead>
				<tr>
				   <th class="center" width="3%">No</th>				   				  
				   	
				   <th class="center">Disposisi  </th>
				  
                   <th class="center">Nomor  </th>

                   <th class="center">Nama Peserta </th>
                   <th class="center">Rangking   </th>				   
                   <th class="center">Lembaga</th> 
				   <th class="center">Tgl Lahir </th>					   
				   <th class="center">Kompetisi</th>
				   <th class="center">Benar</th>
				   <th class="center">Salah</th>
				   <th class="center">Kosong</th>
				   <th class="center">Nilai</th>
				   <th class="center">Ket</th>
               
				  
				</tr>
			  </thead>
			  <tbody></tbody>
            </table>
			</div>
			
			
		
        </div>
        </div>
		
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> </h4>
      </div>
      <div class="modal-body" id="load_data">
        loading.. mohon tunggu 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">

$(document).on("click",".disposisi",function(){
    
    
    
    
    alertify.alert("Akses untuk disposisi ke tingkat provinsi akan dibuka setelah semua nilai peserta dari seluruh Indonesia sudah masuk ke database pusat");
})
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<b style="color:white;background:#33AFFF">Data sedang di tampilkan..</b>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data tidak atau belum tersedia",
						   "sLengthMenu": "Default menampilkan   _MENU_ Data <small>(Klik pada angkanya untuk memuat lebih banyak data sebelum mencetak excel)</small>"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": true,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	 {
							extend: 'excelHtml5',
							exportOptions: {
							  columns: [ 0,2,3,4,5,6,7,8,9,10,11]
							},
							text:'<span class="fa fa-file-excel-o"></span> Cetak Excel',
							className :"btn btn-danger btn-sm"
							},
							
						
						  
						
							{
							extend: 'colvis',
							
							text:'<span class="fa fa-columns "></span> Setting Kolom ',
							className :"btn btn-danger "
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("klokal/g_disposisi"); ?>", 
						type: "post", 
						"data": function ( data ) {
						 data.kota    = $('#kota').val();
						 data.jenjang = $('#jenjang').val();
						 data.madrasah = $('#madrasah').val();
						 data.trkompetisi_id = $('#trkompetisi_id').val();
						
						 data.keyword = $('#keyword').val();
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
	
	
		
			$(document).on('change', '#kota,#jenjang,#madrasah,#trkompetisi_id', function (event, messages) {			
			 
			     dataTable.ajax.reload(null,false);	        
		  
        });
		
			$(document).on('input', '#keyword', function (event, messages) {			
			 
			     dataTable.ajax.reload(null,false);	        
		  
        });
		
		
		 $(document).on("change",".onchange2",function(event){
				 event.preventDefault()
				 var kota    = $("#kota").val();
				 var jenjang    = $("#jenjang").val();
			
				 
				 loading();
				
				 $.post("<?php echo site_url("publik/madrasah"); ?>",{kota:kota,jenjang:jenjang},function(data){
					 
					 $("#madrasah").html(data).show();
				
					 
					   
						jQuery.unblockUI({ });
						
						
				 })
			  
			  
			  
			 })

			 
			  $(document).on("change","#jenjang",function(event){
				 event.preventDefault()
				 var tmmadrasah_id    = $(this).val();
				 
			
				 
				 loading();
				
				 $.post("<?php echo site_url("publik/kompetisi"); ?>",{tmmadrasah_id:tmmadrasah_id},function(data){
					 
					 $("#trkompetisi_id").html(data).show();
				
					 
					   
						jQuery.unblockUI({ });
						
						
				 })
			  
			  
			  
			 })
		
		
		$(document).off("click",".disposisi").on("click",".disposisi",function(){
			var id = $(this).attr("datanya");
			var status = $(this).attr("status");
		     $("#load_data").html("Loading, Mohon Tunggu..");
			
			$.post("<?php echo site_url("klokal/formdisposisikan"); ?>",{id:id,status:status},function(data){
				
				
				$("#load_data").html("Sudah ditutup");
			
				
			})
			
		})
  
  
   
       $(document).off("click","#hayudisposisikan").on("click","#hayudisposisikan",function(){
			var id_disposisi = $("#id_disposisi").val();
			var statuskabko = $("#statuskabko").val();
			var ketkabko = $("#ketkabko").val();
			  if(ketkabko==""){ alertify.alert("Isi Keterangan Alasan Anda mendisposisi Peserta ini ");
			  return false;
			  }
		
			
		     alertify.confirm("Apakah Anda Yakin akan mendisposisi Peserta ini ? ",function(){
				 
				    $("#aksian").html('<center><i class="fa fa-spinner fa-pulse"></i> Mohon Tunggu, Sistem sedang melakukan disposisi</center>');
				  	$.post("<?php echo site_url("klokal/disposisikuy"); ?>",{id_disposisi:id_disposisi,statuskabko:statuskabko,ketkabko:ketkabko},function(data){
				
				                alertify.alert(data);
				               dataTable.ajax.reload(null,false);
				             
			                  $("#aksian").html('');
				  	
			        })
				 
				 
			
			
		
			
			 })
			
		})
        

</script>
			
                    