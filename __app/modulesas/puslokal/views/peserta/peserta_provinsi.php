   <div id="showform"></div>        
                                 
					  <div class="card form" id="form1">
                        <div class="card-header">
                          
                            <h3><i class="fa fa-users"></i> PESERTA TES TINGKAT PROVINSI </h3>
                        </div>
                        <br>
                         
                            <div class="col-md-12">
							<div class="alert alert-info">
							 PETUNJUK ! <br>
						     DATA DIBAWAH INI ADALAH DATA PESERTA JUARA DI KABUPATEN/KOTA ANDA YANG SUDAH ANDA DISPOSISI DAN BERHAK MENGIKUTI KOMPETISI SAINS MADRASAH TINGKAT PROVINSI 
							</div>
								
			 
			 
			              </div>
						  
						  <form>
							<div class="col-md-12">
					 					 <div class="row">
										 
										    <div class="col-md-2"> <b> PENCARIAN  : </b></div>
                                           
										   
										   <div class="col-md-3">
												<div class="form-group ">
													<select id="trkompetisi_id" class="form-control">
													<option value=""> - CARI JENJANG -</option>
													  <?php 
													    $ko = $this->Di->jenjang();
														  foreach($ko as $i=>$r){
															?> <option value="<?php echo $i; ?>" > <?php echo $r; ?> </option><?php   
															  
														  }
														  
														?>
													  
													  
													
													</select>
												</div>
										  </div>
										 
										  
										     <div class="col-md-3">
												<div class="form-group ">
													<select id="trkompetisi_id" class="form-control">
													<option value=""> - CARI KOMPETISI -</option>
													<option value=""> Pilih jenjang dulu </option>

													  
													  
													
													</select>
												</div>
										  </div>
										  
										  
										   <div class="col-md-3">
													
													<div class="form-group">
															<input type="text" size="30" name="keyword" id="keyword" class="form-control" placeholder="Cari Nama Siswa ">
															<input type="hidden" id="jenjang" class="form-control" value="<?php echo $jenjang; ?>">
															
														
												   </div>
											 </div>
											 
										 <div class="col-md-1"><button type="button" class="btn btn-danger "  id="searchcustom"><span class="fa fa-search"></span> </button>
								      
										  
										 
										 </div>
									
							
					</div>
					</div>
			</form>
			
		           <hr>
		<div class="col-md-12">  
		     
		        
					 
			  <br>
			  <br>
		
		   <div style="width: 100%;overflow-x: auto; white-space: nowrap;">
			<table id="datatableTable" class="  table-bordered table-hover table-striped" width="100%">
			  <thead>
				<tr>
				   <th class="center" width="3%">No</th>				   				  
				 
				   <th class="center">Foto  </th>				   
				   <th class="center">Nomor Tes </th>				   
                   <th class="center">Nama  </th>
				   <th class="center">Gender</th>
                   <th class="center">Tgl Lahir</th>
                   <th class="center">Jenjang </th>
				   <th class="center">Lembaga  </th>
				   <th class="center">Kompetisi</th>
				   <th class="center">Status </th>	                   
                   			   
				   
				   <th class="center">Keterangan</th>
               
				  
				</tr>
			  </thead>
			  <tbody></tbody>
            </table>
			</div>
			
			
		
        </div>
        </div>
		
<div id="myModalgg" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
     
      <div class="modal-body" id="load_data">
        loading.. mohon tunggu 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
	

	
    });

  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<b style="color:white;background:#33AFFF">Data sedang di tampilkan..</b>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data belum tersedia",
						   "sLengthMenu": "Default menampilkan   _MENU_ Data <small>(Klik pada angkanya untuk memuat lebih banyak data sebelum mencetak excel)</small>"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": true,
					"lengthMenu": [[5,10,25,50,100], [5,10,25,50,100]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							exportOptions: {
							  columns: [ 0,2,3,4,5,6,7,8,9,10]
							},
							text:'<span class="fa fa-file-excel-o"></span> Cetak Excel',
							className :"btn btn-danger btn-sm"
							},
							
							
							
							{
							extend: 'colvis',
							
							text:'<span class="fa fa-columns "></span> Setting Kolom ',
							className :"btn btn-danger "
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("klokal/g_pesertaprovinsi"); ?>", 
						type: "post", 
						"data": function ( data ) {
						 data.kota    = $('#kota').val();
						 data.jenjang = $('#jenjang').val();
						
						 data.trkompetisi_id = $('#trkompetisi_id').val();
						
						 data.keyword = $('#keyword').val();
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
	
	
		
			$(document).on('change', '#kota,#jenjang,#madrasah,#trkompetisi_id', function (event, messages) {			
			 
			     dataTable.ajax.reload(null,false);	        
		  
        });
		
			$(document).on('input', '#keyword', function (event, messages) {			
			 
			     dataTable.ajax.reload(null,false);	        
		  
        });
		
		
			

        $(document).off('click', '.batalkan_disposisi').on('click', '.batalkan_disposisi', function (event, messages) {			
			
			 var datanya   = $(this).attr("datanya");
			 
			 alertify.confirm("Jika dibatalkan, Data Peserta ini akan hilang dari Peserta Tingkat Provinsi dan akan  di kembalikan ke menu Disposisi Peserta, Apakah Anda yakin ? ",function(){
				 
				 
				  loading();
		
					 $.post("<?php echo site_url("klokal/disposisi_batalkan"); ?>",{datanya:datanya},function(data){
						 
						 jQuery.unblockUI({ });
						alertify.alert("Oke sipp, pembatalan Disposisi Berhasil, Data telah dikembalikan ke Menu Disposisi");
						  dataTable.ajax.reload(null,false);	
						 
					 })
			 
			 		 
			 })
			
		  
        });			
		

</script>
			
                    