			  <div class="card form" id="form1">
                        <div class="card-header">
                          <?php 
						  $arje = array("1"=>"MI/SD Sederajat","2"=>"MTS/SMP Sederajat","3"=>"MA/SMA Sederajat");
						 ?>
                            <h3><i class="fa fa-users"></i>IMPORT EXCEL JENJANG <?php echo $arje[$jenjang]; ?></h3>
                        </div>
                        <br>
						
							<div class="alert alert-info">
							  
							    Keterangan : Untuk fitur Import Data Nilai dari Excel , Anda harus menggunakan format excel yang telah di sediakan aplikasi , silahkan download template excel terlebih dahulu 
							   <br><center><a href="<?php echo base_url(); ?>__statics/upload/template.xlsx" target="_blank"  class="btn btn-danger ">Download Template Excel </a></center>
							
							   <br>
							   PASTIKAN FORMAT YANG DI UPLOAD SAMA DENGAN TEMPLATE DARI APLIKASI , DAN NOMOR TES DI TEMPLATE EXCEL SESUAI DENGAN YANG ADA DI APLIKASI  <br>
							   
							</div>
							
							
							  <?php $attributes = array('class' => 'form-horizontal', 'role'=>'form','name' => 'thisForm', 'id' => '', 'method' => "post", 'enctype' => "multipart/form-data"); ?>
                             <?php echo form_open(site_url("klokal/import_excel"), $attributes); ?>
							 
							 
							 
							 
							 
							 
							 		    <div class="form-group">
											   <label class="col-md-4 control-label" style="text-align:right">Pilih Kompetisi</label>
												<div class="col-md-7">
												 <select id="kompetisi" class="form-control">
													<option value=""> - Pilih Kompetisi -</option>
													  <?php 
													      foreach($kompetisi as $r){
															?> <option value="<?php echo $r->id; ?>" > <?php echo $r->nama; ?> </option><?php   
															  
														  }
														  
														?>
													  
													  
													
													</select>
												
												</div>
										 </div>
									 
									      <div class="form-group">
											   <label class="col-md-4 control-label" style="text-align:right">Pilih File Excel (MAX : 1MB) </label>
												<div class="col-md-7">
												 <input type="file" name="file" required>
												
												</div>
										 </div>
										 
										
										
									 <div class="form-actions">
										<div class="row">
											<div class="col-md-12">
												<center><button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-save"></i>  Import Data </button>
												<button type="button" id="cancel" class="btn btn-sm btn-default"><i class="fa fa-history"></i> Tutup Form</button>
											
												</center>
											</div>
										</div>
									</div>
									
									 <?php echo form_close(); ?>
									 
								</div>
								</div>

			             