
 <div class="row">
                <div class="col-md-12">                      
					  <div class="card form" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-user"></i> Rekap Pendaftar di Provinsi  Anda</h3>
                        </div>
                        <br>
				 <div class="table-responsive">
                            <button  class="btn btn-info btn-sm"  onclick="fnExcelReport();" target="_blank" style="float:right"> <span class="fa fa-file-archive"> </span> Download  Excel </button>				
		
                            <table   id="headerTable" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="3%" rowspan="2"> NO </th>
                                        <th rowspan="2"> Kabupaten/Kota </th>
                                         <th  colspan="4"> <center> JUMLAH   </center> </th>
                                        <th colspan="4" align="center"><center> STATUS </center>   </th>
                                    </tr>
									
									 <tr>
                                       <th> MI/SD </th>
                                       <th> MTS/SMP </th>
                                       <th> MA/SMA </th>
                                       <th> TOTAL </th>
									   
									   
                                       <th> DRAFT </th>
                                       <th> BLM DIVERIFIKASI </th>
                                       <th> LULUS </th>
                                       <th> TIDAK LULUS  </th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php 
									   $data = $this->db->query("select id,nama from kota where provinsi_id='".$_SESSION['tmprovinsi_id']."' order by nama asc")->result();
									     $no=1;
									       foreach($data as $r){
											   
											     $mi = $this->db->query("select count(id) as jml from v_siswa where  kota_madrasah='".$r->id."' and jenjang_madrasah='1'")->row();
											     $mts = $this->db->query("select count(id) as jml from v_siswa where kota_madrasah='".$r->id."' and jenjang_madrasah='2'")->row();
											     $ma = $this->db->query("select count(id) as jml from v_siswa where  kota_madrasah='".$r->id."' and jenjang_madrasah='3'")->row();
												 $jml = $mi->jml+$mts->jml+$ma->jml;
											
											
											
											     $draft = $this->db->query("select count(id) as jml from v_siswa where status='0' and kota_madrasah='".$r->id."'")->row();
											     $blm = $this->db->query("select count(id) as jml from v_siswa where status='1' and kota_madrasah='".$r->id."'")->row();
											     $lulus = $this->db->query("select count(id) as jml from v_siswa where  status='2' and  kota_madrasah='".$r->id."'")->row();
                                                 $tl = $this->db->query("select count(id) as jml from v_siswa where  status='3' and  kota_madrasah='".$r->id."'")->row();
                                                 
                                                 $style="";
                                                   if($blm->jml >0){
                                                       $style="background-color:red";
                                                   }
											    ?>
												<tr style="<?php echo $style; ?>">
												  <td> <?php echo $no++; ?></td>
												  <td> <?php echo $r->nama; ?></td>										  
												  
												  <td> <?php echo $mi->jml; ?></td>
												  <td> <?php echo $mts->jml; ?></td>
												  <td> <?php echo $ma->jml; ?></td>
												 
												  <td> <?php echo $jml; ?></td>
												  
												  <td> <?php echo $draft->jml; ?></td>
												  <td> <?php echo $blm->jml; ?></td>
												  <td> <?php echo $lulus->jml; ?></td>
												  <td> <?php echo $tl->jml; ?></td>
												  
												
												</tr>
												<?php
											   
											   
											   
										   }
									             $mi    = $this->db->query("select count(id) as jml from tm_siswa where tmmadrasah_id in(select id from tm_madrasah where provinsi='".$_SESSION['tmprovinsi_id']."' and jenjang='1')")->row();
									              $mts   = $this->db->query("select count(id) as jml from tm_siswa where tmmadrasah_id in(select id from tm_madrasah where provinsi='".$_SESSION['tmprovinsi_id']."' and jenjang='2')")->row();
									             $ma   = $this->db->query("select count(id) as jml from tm_siswa where tmmadrasah_id in(select id from tm_madrasah where provinsi='".$_SESSION['tmprovinsi_id']."' and jenjang='3')")->row();
												 $jml =  $mi->jml+$mts->jml+$ma->jml;
												 
									    

                                                 $draft = $this->db->query("select count(id) as jml from tm_siswa where status='0' and tmmadrasah_id in(select id from tm_madrasah where provinsi='".$_SESSION['tmprovinsi_id']."')")->row();
                                                 $blm = $this->db->query("select count(id) as jml from tm_siswa where status='1' and tmmadrasah_id in(select id from tm_madrasah where provinsi='".$_SESSION['tmprovinsi_id']."')")->row();
											     $lulus = $this->db->query("select count(id) as jml from tm_siswa where  status='2' and tmmadrasah_id in(select id from tm_madrasah where provinsi='".$_SESSION['tmprovinsi_id']."')")->row();
											     $tl = $this->db->query("select count(id) as jml from tm_siswa where  status='3' and tmmadrasah_id in(select id from tm_madrasah where provinsi='".$_SESSION['tmprovinsi_id']."')")->row();
											  
									 ?>
									 
									 	<tr style="font-weight:bold">
												  <td colspan="2"> Jumlah </td>
												  <td> <?php echo $mi->jml; ?></td>
												  <td> <?php echo $mts->jml; ?></td>
												  <td> <?php echo $ma->jml; ?></td>
												 
												  <td> <?php echo $jml; ?></td>
												  
												  <td> <?php echo $draft->jml; ?></td>
												  <td> <?php echo $blm->jml; ?></td>
												  <td> <?php echo $lulus->jml; ?></td>
												  <td> <?php echo $tl->jml; ?></td>
												  
												
												</tr>
                                </tbody>
                            </table>
                        </div>
                        </div>
                        </div>
                        </div>
												<iframe id="txtArea1" style="display:none"></iframe>
	<script>
	function fnExcelReport()
{
    var tab_text=" <center><h4><b>LAPORAN PROSES VERIFIKASI   <br> PESERTA KOMPETISI SAINS MADRASAH <br>  TAHUN 2019 </b></h4></center> <br><br> <br><br><table border='1px'><tr>";
    var textRange; var j=0;
    tab = document.getElementById('headerTable'); // id of table

    for(j = 0 ; j < tab.rows.length ; j++) 
    {     
        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text=tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE "); 

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus(); 
        sa=txtArea1.document.execCommand("SaveAs",true,"laporan.xls");
    }  
    else                 //other browser not tested on IE 11
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

    return (sa);
}
</script>				
