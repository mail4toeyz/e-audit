
       
                                 
								 <div class="card" id="form1">
								   <div class="card-header">
									   <h3><i class="fa fa-calendar"></i> Jadwal Pelaksanaan KSM </h3>
								   </div>
								   
					   
						
				   <div class="col-md-12">  
					  <br>	
					  <div class="table-responsive">
					  <table class="table table-hover table-bordered table-striped">
								<thead>
								<tr> 
									<th rowspan="2"> NO </th>
									
									<th rowspan="2"> MAPEL </th>
									<th rowspan="2"> SESI </th>
									<th rowspan="2"> SIMULASI </th>
									<th colspan="3"> <center>ZONA (SIMULASI) </center></th>  
									<th rowspan="2"> PELAKSANAAN </th>
									<th colspan="3"> <center>ZONA (PELAKSANAAN) </center></th>  
									                   
									
									
									<th rowspan="2"> TOTAL PESERTA </th>
									</tr>

									<tr>
									<th> WIB </th>
									<th> WITA </th>
									<th> WIT </th>

									<th> WIB </th>
									<th> WITA </th>
									<th> WIT </th>
									</tr>
								</thead>

								<tbody>
								<?php 
								
								 $jenjang = array("1"=>"MI/SD","2"=>"MTs/SMP","3"=>"MA/SMA");
									$sesi = $this->db->get_where("tm_sesi",array("tingkat"=>"kabko"))->result();
									$simulasi    = array("1"=>"Rabu, 5 Juli 2023","2"=>"Kamis, 6 Juli 2023","3"=>"Jumat, 7 Juli 2023");
									$pelaksanaan = array("1"=>"Sabtu, 8 Juli 2023","2"=>"Minggu, 9 Juli 2023","3"=>"Senin, 10 Juli 2023");
									$jml=0;
									$jmlPeserta=0;
									
									foreach($sesi as $rs){

									

									
									// $kebutuhan =  ceil($jml_peserta->jml/40);
									

									// $mapel ="";
									// $kebutuhan=0;
									// 	$mex = explode(",",$rs->kompetisi);
									// 		foreach($mex as $rk){
									// 		// $jml_pesertamapel = $this->db->query("select count(id) as jml from v_siswa where hari2='{$rs->hari}' and sesi2='{$rs->sesi}' AND   kota_madrasah='{$_SESSION['tmkota_id']}' AND trkompetisi_id={$rk}  ")->row();
									// 		// $pengawasden  =  ceil($jml_pesertamapel->jml/40);
									// 		// $mapel      .="<br>".$this->Di->get_kondisi(array("id"=>$rk),"tr_kompetisi","nama")." (Pengawas : ".$pengawasden." )";
									// 		// $kebutuhan =  $kebutuhan+ $pengawasden;
											
									// 		}
									// 		$jmlkeb  = $jmlkeb +$kebutuhan;

									//$jml_peserta = $this->db->query("select count(id) as jml from v_siswa where status=2 and trkompetisi_id IN(".$rs->kompetisi.") and provinsi_madrasah IN(select id from provinsi where zona='') ")->row();

									$jml_pesertawib = $this->db->query("select count(id) as jml from v_siswa where status=2 and trkompetisi_id IN(".$rs->kompetisi.") and provinsi_madrasah IN(select id from provinsi where zona='WIB') and sesi_kabko='{$rs->id}' ")->row();
									$jml_pesertawita = $this->db->query("select count(id) as jml from v_siswa where status=2 and trkompetisi_id IN(".$rs->kompetisi.") and provinsi_madrasah IN(select id from provinsi where zona='WITA') and sesi_kabko='{$rs->id}'")->row();
									$jml_pesertawit = $this->db->query("select count(id) as jml from v_siswa where status=2 and trkompetisi_id IN(".$rs->kompetisi.") and provinsi_madrasah IN(select id from provinsi where zona='WIT') and sesi_kabko='{$rs->id}'")->row();
											
									
									

									
									?>
										<tr>
										<td><?php echo $rs->id; ?></td>
										
										<td> <?php echo $rs->keterangan; ?>  (<?php echo $jenjang[$rs->jenjang]; ?>)    </td>
										<td> <?php echo $rs->sesi; ?> </td>
										<td> <?php echo $simulasi[$rs->hari]; ?> </td>
										<td> <?php echo $rs->wib_ujicoba; ?> </td>
										<td> <?php echo $rs->wita_ujicoba; ?> </td>
										<td> <?php echo $rs->wit_ujicoba; ?> </td>

										<td> <?php echo $pelaksanaan[$rs->hari]; ?> </td>
										
										
										<td> <?php echo $rs->wib; ?> </td>
										<td> <?php echo $rs->wita; ?> </td>
										<td> <?php echo $rs->wit; ?> </td>
									
										
										
										<td>-</td>
										

										</tr>

										<tr>
										<td colspan="4" align="right"><b> Jumlah Peserta </b> </td>
										<?php 
										$jmlPeserta=0;
										  if(!empty($rs->wib)){

											$wib = $jml_pesertawib->jml;
											$jmlPeserta = $jmlPeserta+$wib;
											}else{

											$wib = "-";
											} 

											if(!empty($rs->wita)){

												$wita = $jml_pesertawita->jml;
												$jmlPeserta = $jmlPeserta+$wita;
												}else{
	
												$wita = "-";
												} 

											if(!empty($rs->wit)){

													$wit = $jml_pesertawit->jml;
													$jmlPeserta = $jmlPeserta+$wit;
													}else{
		
													$wit = "-";
												} 

												$jml = $jml + $jmlPeserta;


												
										 ?>

										<td><?php echo ($wib !="-") ?  $this->Di->formatuang2($wib) :"-"; ?>  </td>
										<td><?php echo ($wita !="-") ?  $this->Di->formatuang2($wita) :"-"; ?>  </td>
										<td><?php echo ($wit !="-") ?  $this->Di->formatuang2($wit) :"-"; ?>  </td>
										<td colspan="1" align="right"><b> Jumlah Peserta </b> </td>
										<td><?php echo ($wib !="-") ?  $this->Di->formatuang2($wib) :"-"; ?>  </td>
										<td><?php echo ($wita !="-") ?  $this->Di->formatuang2($wita) :"-"; ?>  </td>
										<td><?php echo ($wit !="-") ?  $this->Di->formatuang2($wit) :"-"; ?>  </td>
										
								
									
										
										
										<td><?php echo $this->Di->formatuang2($jmlPeserta); ?></td>
										

										</tr>




									<?php 


									}
									?>

										<tr>
										<td colspan="11">Total Keseluruhan Peserta </td>
										
										<td><?php echo $this->Di->formatuang2($jml); ?></td>
									

										</tr>


									


								</tbody>

							</table>


					   </div>
				   </div>
				   </div>
				   
		   
		 
							   