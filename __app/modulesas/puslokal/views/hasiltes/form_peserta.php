			  <div class="card form" id="form1">
                        <div class="card-header">
                          <?php 
						  $arje = array("1"=>"MI/SD Sederajat","2"=>"MTS/SMP Sederajat","3"=>"MA/SMA Sederajat");
						 ?>
                            <h3><i class="fa fa-users"></i>   IMPORT EXCEL JENJANG <?php echo $arje[$jenjang]; ?></h3>
                        </div>
                        <br>
						
							<div class="alert alert-info">
							  
							    KETERANGAN : UNTUK FITUR IMPORT DATA NILAI DARI EXCEL , ANDA HARUS MENGGUNAKAN FORMAT EXCEL YANG TELAH DI SEDIAKAN APLIKASI , SILAHKAN DOWNLOAD TEMPLATE EXCEL TERLEBIH DAHULU 
							   <br><center><a href="<?php echo base_url(); ?>excel/template.xlsx" target="_blank"  class="btn btn-danger ">Download Template Excel </a></center>
							
							   <br>
							   PASTIKAN FORMAT YANG DI UPLOAD SAMA DENGAN TEMPLATE DARI APLIKASI , DAN NOMOR TES DI TEMPLATE EXCEL SESUAI DENGAN YANG ADA DI APLIKASI,MAKSIMAL UPLOAD ADALAH 1 MB  <br>
							   
							</div>
							
							
							  <?php $attributes = array('class' => 'form-horizontal', 'role'=>'form','name' => 'thisForm', 'id' => '', 'method' => "post", 'enctype' => "multipart/form-data"); ?>
                             <?php echo form_open(site_url("klokal/import_excel"), $attributes); ?>
							 
							 
						
			<input type="hidden" name="jenjang" class="form-control" value="<?php echo $jenjang;  ?>">

							 
							  
							 
							 		    <div class="form-group">
												<div class="col-md-12">
												 <select name="kompetisi" class="form-control" >
													<option value=""> - Pilih Kompetisi -</option>
													  <?php 
													    $kompetisi = $this->db->get_where("tr_kompetisi",array("tmmadrasah_id"=>$jenjang))->result();
													      foreach($kompetisi as $r){
															?> <option value="<?php echo $r->id; ?>" > <?php echo $r->nama; ?> </option><?php   
															  
														  }
														  
														?>
													  
													  
													
													</select>
												
												</div>
										 </div>
									 
									      <div class="form-group">
												<div class="col-md-12">
												 <input type="file" name="file" required>
												
												</div>
										 </div>
										 
										
										
									 <div class="form-actions">
										<div class="row">
											<div class="col-md-12">
												<center><button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-save"></i>  Import Data </button>
												<button type="button" id="cancel" class="btn btn-sm btn-default"><i class="fa fa-history"></i> Tutup Form</button>
											
												</center>
											</div>
										</div>
									</div>
									
									 <?php echo form_close(); ?>
									 
								</div>
								
							

			             