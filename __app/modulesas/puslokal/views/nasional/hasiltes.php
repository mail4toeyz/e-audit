<style> 
table.dataTable.table-striped.DTFC_Cloned tbody tr:nth-of-type(odd) {
        background: #F3F3F3;
		top:0;
}

table.dataTable.table-striped.DTFC_Cloned tbody tr:nth-of-type(even) {
        background: white;
}

table.DTFC_Cloned thead,
table.DTFC_Cloned tfoot {
  background-color: #F3F3F3;
}

div.DTFC_Blocker {
  background-color: #F3F3F3;
}

table.dataTable.display tbody tr.DTFC_NoData {
  background-color: transparent;
}

th,
td {
  white-space: nowrap;
}



table.DTFC_Cloned thead,
thead {
  background-color: #F3F3F3
}
</style>

  <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.3.2/js/dataTables.fixedColumns.min.js"></script>

<div id="showform"></div>        
  						 <?php 
						$status_siswa = $this->input->get_post("status");
					   ?>                       
					  <div class="card " id="form1">
                        <div class="card-header">
						<?php 
						  $arje = array("1"=>"MI Sederajat","2"=>"MTs Sederajat","3"=>"MA Sederajat");
						 ?>
                            <h3><i class="fa fa-users"></i> Hasil KSM Nasional  <?php echo $arje[$jenjang]; ?></h3>
                        </div>
                        <br>
                        <br>
						
                          
						<hr>
							<div class="col-md-12">
					 <div class="row">
					  
					 
					                   <div class="col-md-2">
												<div class="form-group ">
													<select id="provinsi" class="form-control onchange"  target="kota" urlnya="<?php echo site_url('publik/kota'); ?>"">
													<option value=""> - Provinsi  -</option>
													  <?php 
													    $ko = $this->db->get("provinsi")->result();
														  foreach($ko as $r){
															?> <option value="<?php echo $r->id; ?>"> <?php echo $r->nama; ?> </option><?php   
															  
														  }
														  
														?>
													  
													  
													
													</select>
												</div>
										  </div>

										 <div class="col-md-2">
												<div class="form-group ">
													<select id="kota" class="form-control kota ">
													<option value=""> - Kabupaten/Kota -</option>
													  
													
													</select>
												</div>
										  </div>

										     <div class="col-md-2">
												<div class="form-group ">
													<select id="trkompetisi_id" class="form-control">
													<option value=""> - Cari kompetisi -</option>
													  <?php 
													    $ko = $this->db->get_where("tr_kompetisi",array("tmmadrasah_id"=>$jenjang))->result();
														  foreach($ko as $r){
															?> <option value="<?php echo $r->id; ?>"> <?php echo $r->nama; ?> </option><?php   
															  
														  }
														  
														?>
													  
													  
													
													</select>
												</div>
										  </div>
										  
									
										  <div class="col-md-2">
												<div class="form-group ">
												<select id="kolom" class="form-control">
												<option value="nisn">  NISN </option>
												<option value="nama" selected>  Nama Peserta </option>
												<option value="username">  NSM/NPSN Lembaga </option>
												<option value="madrasah"> Nama Lembaga </option>
												
												 
												
												</select>
										       </div>
										 </div>
										  
										   <div class="col-md-2">
													
													<div class="form-group">
															<input type="text" size="30" name="keyword" id="keyword" class="form-control" placeholder="Keyword  ">
															<input type="hidden" id="jenjang" class="form-control" value="<?php echo $jenjang; ?>">
															
														
												   </div>
											 </div>
											 
										 <div class="col-md-1"><button type="button" class="btn btn-danger "  id="searchcustom"><span class="fa fa-search"></span> </button>
								       
										  
										 
										 </div>
									
							
					</div>
					</div>
			
			
		           <hr>
<div class="col-md-12">   
		<button onclick="btn_export()" class="link" >
    Download 
  </button>				
		   <div class="table-responsive">
			<table id="datatableTable"  class="table table-striped table-bordered  " cellspacing="0" width="100%">
     
			  <thead>
				<tr>
				   <th class="center" width="3%" rowspan="2">NO</th>
                   <th class="center" rowspan="2">FOTO </th>
				   <th class="center" rowspan="2">NO PESERTA</th> 
				   <th class="center" rowspan="2">NAMA </th> 
				   <th class="center" rowspan="2">PROVINSI</th> 
				   <th class="center" rowspan="2">LEMBAGA </th>                  
                   <th class="center" rowspan="2">KOMPETISI</th>
                  	
                   <th class="center" rowspan="2">MEDALI </th>	
				   <th class="center" colspan="3">NILAI  </th>
				   <th class="center" colspan="5">RINCIAN NILAI KSM NASIONAL  </th>
				   <th class="center" colspan="4">RINCIAN NILAI KSM PROVINSI  </th>
				   <th class="center" colspan="4">RINCIAN NILAI KSM KABUPATEN/KOTA  </th>
				   <th class="center" rowspan="2">NIK</th>
				   <th class="center" rowspan="2">NISN</th>
				   <th class="center"  rowspan="2">LAHIR</th>
				   <th class="center"  rowspan="2">TGL LAHIR</th>
				  
				  
				   <th class="center"  rowspan="2">NIK AYAH</th>					  
				   <th class="center"  rowspan="2">NAMA AYAH</th>					  
				   <th class="center"  rowspan="2">NIK IBU</th>					  
				   <th class="center"  rowspan="2">NAMA IBU</th>	
				   
				   <th class="center" rowspan="2">KABUPATEN/KOTA </th> 
				   <th class="center" rowspan="2">KECAMATAN </th> 
				   <th class="center" rowspan="2">DESA </th> 			  
				   <th class="center" colspan="5">DETAIL LEMBAGA </th>
				   
				</tr>
				<tr>
				  <th> NILAI  </th>
                  <th> CBT </th>
                  <th> EKSPLORASI </th>
                

				  <th> BNR PG </th>
                  <th> BNR ESSAY </th>
                  <th> JML BNR </th>
                  <th> SLH </th>
                  <th> KSG </th>

				  <th> NILAI </th>
                  <th> JML BNR </th>
                  <th> SLH </th>
                  <th> KSG </th>

				  <th> NILAI </th>
                  <th> JML BNR </th>
                  <th> SLH </th>
                  <th> KSG </th>

				  <th> JENIS </th>
				  <th> JENJANG </th>
				  <th> NSM/NPSN </th>
				  <th> LEMBAGA  </th>
				  <th> NIK KEPALA  </th>
				  <th> NAMA KEPALA  </th>
				  <th> HP KEPALA </th>

				
				</tr>
			  </thead>
			  <tbody></tbody>
            </table>
			</div>
        </div>
        </div>
		
<script src="<?php echo base_url(); ?>__statics/js/excel/xlsx.extendscript.js"></script>
<script src="<?php echo base_url(); ?>__statics/js/excel/xlsx-style/xlsx.full.min.js"></script>

<script src="<?php echo base_url(); ?>__statics/js/excel/export.js"></script>
	
<script type="text/javascript">

function btn_export() {
        var table1 = document.querySelector("#datatableTable");
        var opt = {
            rowIndex: 4
        }; //开头空4行
        var sheet = XLSX2.utils.table_to_sheet(table1, opt);

  
     

        openDownloadDialog(sheet2blob(sheet), ' .Pemenang KSM.xlsx');
    }

  var dataTable = $('#datatableTable').DataTable( {
	"fixedHeader": true,
	
	"scrollX": true,
    "fixedColumns": {
      leftColumns: 4
    },
						"processing": true,
						"language": {
						"processing": 'Mohon Tunggu..',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data tidak atau belum tersedia",
						   "sLengthMenu": "   _MENU_  "
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": true,
					"lengthMenu": [[34, 50,100,200,300,500,1000, 800000000], [34, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							
							text:'<span class="fa fa-file-excel-o"></span> Cetak Excel',
							className :"btn btn-danger btn-sm"
							},
						
							{
							extend: 'colvis',
							
							text:'<span class="fa fa-columns "></span> Setting Kolom ',
							className :"btn btn-danger "
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("puslokal/g_pemenangNas"); ?>", 
						type: "post", 
						"data": function ( data ) {
							
						 data.kota    = $('#kota').val();
						 data.kolom    = $('#kolom').val();
						 data.jenjang = $('#jenjang').val();
						 data.madrasah = $('#madrasah').val();
						 data.trkompetisi_id = $('#trkompetisi_id').val();
						 data.status = $('#status').val();
						 data.keyword = $('#keyword').val();
						 data.status_siswa = $('#status_siswa').val();
						 data.kota = $('#kota').val();
						 data.provinsi = $('#provinsi').val();
						
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
	
		
		$(document).on('input click change', '#keyword,#searchcustom', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
		$(document).on('input', '#keyword', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
		
	
		$(document).on('change', '#kota,#jenjang,#provinsi,#trkompetisi_id,#status,#kota', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
			
	
				
				
		$(document).off("click",'input[name="status"]').on("click",'input[name="status"]',function(){
			
			var status= $('input[name="status"]:checked').val();
			var tmsiswa_id= $(this).attr("tmsiswa_id");
			  
			  loading();
				
				 $.post("<?php echo site_url("klokal/save_verval"); ?>",{tmsiswa_id:tmsiswa_id,status:status},function(data){
					   
					    alertify.success("Verifikasi Berhasil dilakukan");
						dataTable.ajax.reload(null,false);	
						jQuery.unblockUI({ });
						
						
				 })
			
		})
		
		$(document).off('input', '#keterangan').on('input', '#keterangan', function (event, messages) {			
			 
			   var tmsiswa_id= $(this).attr("tmsiswa_id"); 
			   
               var keterangan = $(this).val();
			  
			   
			   $.post("<?php echo site_url("klokal/save_keterangan"); ?>",{tmsiswa_id:tmsiswa_id,keterangan:keterangan},function(data){
					   
					    
						
				 })
		  
        });
		
		
		 $(document).on("change",".onchange2",function(event){
				 event.preventDefault()
				 var kota    = $("#kota").val();
				 var jenjang    = $("#jenjang").val();
			
				 
				 loading();
				
				 $.post("<?php echo site_url("publik/madrasah"); ?>",{kota:kota,jenjang:jenjang},function(data){
					 
					 $("#madrasah").html(data).show();
				
					 
					   
						jQuery.unblockUI({ });
						
						
				 })
			  
			  
			  
			 })

			 
			  $(document).on("change","#jenjang",function(event){
				 event.preventDefault()
				 var tmmadrasah_id    = $(this).val();
				 
			
				 
				 loading();
				
				 $.post("<?php echo site_url("publik/kompetisi"); ?>",{tmmadrasah_id:tmmadrasah_id},function(data){
					 
					 $("#trkompetisi_id").html(data).show();
				
					 
					   
						jQuery.unblockUI({ });
						
						
				 })
			  
			  
			  
			 })


$(function() {
    // Check the initial Poistion of the Sticky Header
 var stickyHeaderTop = $('#showform').offset().top;
 $(window).scroll(function() {
if ($(window).scrollTop() > stickyHeaderTop) {
  $('.dataTables_scrollHead, .DTFC_LeftHeadWrapper').css('transform', 'translateY(0%)');
  $('.DTFC_LeftHeadWrapper').css({position: 'fixed',top: '0px',zIndex: '1',left: 'auto'});
  $('.dataTables_scrollHead').css({position: 'fixed',top: '0px', zIndex: '1' });
  $('.DTFC_ScrollWrapper').css({height: ''});
 
 }
 else {
  $('.DTFC_LeftHeadWrapper, .DTFC_LeftHeadWrapper').css({position: 'relative',top: '0px'});
  $('.dataTables_scrollHead').css({position: 'relative', top: '0px'});
  $('.dataTables_scrollHead').css('transform', 'translateY(0%)');
      }

    });
  });

</script>
			
                    