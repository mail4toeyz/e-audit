<div class="row">
                <div class="col-md-12">                      
					  <div class="card form" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-user"></i> <?php echo $title; ?>  </h3>
                        </div>
                        <br>
                        
                            <div class="row">
                               <div class="col-md-8"> 
                                <br>

                                  <div class="table-responsive">
                                  <?php $attributes = array('class' => 'form-horizontal', 'role'=>'form','name' => 'simpanNilai', 'id' => 'simpanNilai', 'method' => "post","enctype"=>"multipart/form-data", 'url' =>site_url("juri/simpanNilai")); ?>
			                             <?php echo form_open("javascript:void(0)", $attributes); ?>
                              
                                      <input type="hidden" name="tmsiswa_id" value="<?php echo $dataform->id; ?>">

                                      <table class="table table-hover table-bordered table-striped">
                                            <thead> 
                                                  <tr>
                                                    <th width="2px"> NO </th>
                                                    <th> FILE </th>
                                                    <th> NILAI </th>
                                                 </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                  $no=0;
                                                  $cekFile = $this->db->query("select * from h_ujian where tmsiswa_id='{$dataform->id}'")->row();
                                                  $jawabanList = explode(",",$cekFile->list_jawaban);
                                                  $nomor=0;
                                                  foreach($listSoal as $is=>$ls){
                                                     $cek = explode(".",$ls);
                                                       if($cek[1]=="pdf"){
                                                          if($ls !=""){
                                                          $no++;
                                                          ?>

                                                        <tr>
                                                            <th width="2px"> <?php echo $no; ?> </th>
                                                            <th> <a href="<?php echo site_url("juri/unduhFile?file=".$ls.""); ?>" class="btn btn-danger btn-sm" target="_blank"><span class="fa fa-file"></span>  Unduh Jawaban </a> </th>
                                                            <th> <input type="number" name="nilai[]" class="form-control" value="<?php echo $jawabanList[$nomor]; ?>" ><input type="hidden" name="soal[]" value="<?php echo $ls; ?>" class="form-control" > </th>
                                                        </tr>
                                                 <?php 
 $nomor++;
                                                      }
                                                     
                                                    } else { 
													$no++; 
												?>
													
                                                <?php 
													}
                                                  }
                                                ?>


                                            </tbody>
                                     </table>

                                    <center> <button type="button" class="btn btn-default " id="cancel">Tutup Form </button> 
                                   <button type="submit" class="btn btn-primary btn-white"><span class="fa fa-save"></span> Simpan Penilaian </button> 
                                                </center>
                                 </div>



                               </div>

                               <div class="col-md-4">
                              
                               <br>
                     

                                   <br>
										
									 	
                                              <b> Nomor Peserta : </b>
                                              <?php echo $dataform->no_test3; ?>
                                              <br>

                                              <b> Nama  Peserta : </b>
                                              <?php echo $dataform->nama; ?>
                                              <br>


                                      
									
										
										<div class="form-group">
										   <?php 
										     if(isset($dataform)){
												 if($dataform->foto==""){
													$file = "ksm.png";
												 }else{
													 $file = $dataform->foto;
												 }
												 
											 }else{
												 
												    $file = "ksm.png";
											 }
											 
											 ?>
											<img src="<?php echo base_url(); ?>__statics/upload/<?php echo $file; ?>" class="img-thumbnail img-responsive" style="height:200px;" id="blah" >
										</div>

                  


                            </div>

                           


                        </div>
                     

                       <?php echo form_close(); ?>
                            <br>
                  </div>
                  </div>
    </div>

    <script type="text/javascript">

$(document).off('submit', 'form#simpanNilai').on('submit', 'form#simpanNilai', function (event, messages) {
	 event.preventDefault()
       var form   = $(this);
       var urlnya = $(this).attr("url");
	
	   var formData = new FormData(this);
   
	  loading();
        $.ajax({
            type: "POST",
            url: urlnya,
            
			data: formData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,    
			success: function (response, status, xhr) {
                var ct = xhr.getResponseHeader("content-type") || "";
                if (ct == "application/json") {
                    
                    toastr.error(response.message, "Gagal  , perhatikan !  ", {
							"timeOut": "0",
							"extendedTImeout": "0",
							"closeButton": true,
							  "debug": false,
							  "positionClass": "toast-top-right",
							  "onclick": null,
							  "showDuration": "10000",
							  "hideDuration": "10000",
							  "timeOut": "50000",
							  "extendedTimeOut": "10000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
						});
                } else {
					
                  toastr.success("Data Berhasil disimpan", "Sukses !", {
							"timeOut": "0",
							"extendedTImeout": "0",
							"closeButton": true,
							  "debug": false,
							  "positionClass": "toast-top-right",
							  "onclick": null,
							  "showDuration": "10000",
							  "hideDuration": "10000",
							  "timeOut": "50000",
							  "extendedTimeOut": "10000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
						});
				   $("#showform").html("");
				   dataTable.ajax.reload(null,false);
					
				   
				    
				   
                }
				
				jQuery.unblockUI({ });
            }
        });

        return false;
    });	
    </script>
