           
					  <div class="card">
                        <div class="card-header">
						<?php 
						  $arje = array("1"=>"MI/SD Sederajat","2"=>"MTS/SMP Sederajat","3"=>"MA/SMA Sederajat");
						 ?>
                            <h3> Statistik KSM Nasional </h3>
                        </div>

						<div class="card-body">
						<div class="text-right"> 
						<div class="buttons btn-group">
							<a class="btn btn-success btn-sm" href="<?php echo site_url('puslokal/generateNilaiNasional'); ?>"> Hitung Skor </a>
							<a class="btn btn-info btn-sm" href="<?php echo site_url('puslokal/generate_Nasionaljuara'); ?>"> Generate Pemenang</a>
							<a class="btn btn-primary btn-sm" href="<?php echo site_url('puslokal/raihanmedali'); ?>"> Generate Peringkat Provinsi</a>
							<a class="btn btn-success btn-sm" href="<?php echo site_url('puslokal/generateMadrasahNas'); ?>"> Generate Peringkat Madrasah</a>
						</div>
						</div>
						<hr>
							 <div class="row">
								 <div class="col-md-7">
									 <div class="table-responsive">
									 <table id="datatableTable" class=" table table-bordered table-hover table-striped" width="100%">
										<thead>
												<tr>
												
												<th class="center" rowspan="2">KATEGORI </th> 												
												<th class="center" colspan="3"><center>RAIHAN MEDALI</center> </th> 
												<th class="center" rowspan="2">JUMLAH </th> 
												
												
												</tr>

												<tr>
																					
																					<td align="center"><img src="<?php echo base_url(); ?>__statics/img/medali/emas.png" width="50px" height="50px">  </td>
																					<td align="center"><img src="<?php echo base_url(); ?>__statics/img/medali/perak.png" width="50px" height="50px">   </td>
																					
																					<td align="center"><img src="<?php echo base_url(); ?>__statics/img/medali/perunggu.png" width="50px" height="50px">    </td>
																					
																					</tr>

												</thead>
												<tbody>
													<?php 
													 $emas=0;
													 $perak=0;
													 $perunggu=0;
													  foreach ($grid as $val){
														$emas= $emas +$val['emas']; 
														$perak= $perak +$val['perak']; 
														$perunggu= $perunggu +$val['perunggu']; 
														?>

													  
													<tr>
														<td> <?php echo $val['kategori']; ?> </td>
														<td> <?php echo $val['emas']; ?> </td>
														<td> <?php echo $val['perak']; ?> </td>
														<td> <?php echo $val['perunggu']; ?> </td>
														<td> <?php echo $val['emas']+$val['perak']+$val['perunggu']; ?> </td>
													
													
													</tr>

													<?php 
													  }
													  ?>

													<tr style="font-weight:bold">
														<td> Total  </td>
														<td> <?php echo $emas; ?> </td>
														<td> <?php echo $perak; ?> </td>
														<td> <?php echo $perunggu; ?> </td>
														<td> <?php echo $emas+$perak+$perunggu; ?> </td>
													
													
													</tr>
													
												
												</tbody>
												
										</table>

									 </div>

								 </div>
								 <div class="col-md-5">
									 <div id="chartKategori"></div>
								 </div>
							 </div>


							 <!-- Provinsi -->

							 <div class="row">
								 <div class="col-md-12">
									 <div id="grafikProvinsi"></div>
								 </div>
							 </div>


						</div>
					</div>

                        <br>
                         
<script>document
// Data retrieved from https://netmarketshare.com/
// Make monochrome colors
var pieColors = (function () {
  var colors = [],
    base = Highcharts.getOptions().colors[0],
    i;

  for (i = 0; i < 10; i += 1) {
    // Start out with a darkened base color (negative brighten), and end
    // up with a much brighter color
    colors.push(Highcharts.color(base).brighten((i - 3) / 7).get());
  }
  return colors;
}());

// Build the chart
Highcharts.chart('chartKategori', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: '<b>Raihan medali berdasarkan kategori lembaga</b>'
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  accessibility: {
    point: {
      valueSuffix: '%'
    }
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      colors: pieColors,
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
        distance: -50,
        filter: {
          property: 'percentage',
          operator: '>',
          value: 4
        }
      }
    }
  },
  credits: {
        enabled: false
    },
  series: [{
    name: 'Share',
    data: [
		<?php echo $json_pie_chart; ?>
 
    ]
  }]
});

Highcharts.chart('grafikProvinsi', {
    chart: {
        type: 'bar',
		height:1700
    },

    title: {
        text: 'JUARA UMUM '
    },
    subtitle: {
        text: 'Raihan Medali Berdasarkan Provinsi dalam Pelaksanaan Kompetisi Sains Madrasah'
    },
    xAxis: {
        categories: [<?php echo $provinsiNameData; ?>],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Siswa ',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' Siswa'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
	
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Medali Emas',
        data: [<?php echo $emasData; ?>]
    }, {
        name: 'Medali Perak',
        data: [<?php echo $perakData; ?>]
    }, {
        name: 'Medali Perunggu',
        data: [<?php echo $perungguData; ?>]
    }]
});

</script>
                                