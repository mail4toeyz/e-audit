<div id="showform"></div>        
  						 <?php 
						$status_siswa = $this->input->get_post("status");
					   ?>                       
					  <div class="card " id="form1">
                        <div class="card-header">
						<?php 
						  $arje = array("1"=>"MI/SD Sederajat","2"=>"MTS/SMP Sederajat","3"=>"MA/SMA Sederajat");
						 ?>
                            <h3><i class="fa fa-users"></i> Madrasah Juara Umum KSM Nasional </h3>
                        </div>
                        <br>
                         
                            <div class="col-md-12">
							<div class="alert alert-info" style="font-size:10px">
							 Petunjuk ! <br>
							  Dibawah ini adalah Peringkat Madrasah peraih juara KSM Nasional
						      <br>
							</div>
								
			 
			 
			              </div>
						  
						  <form>
							<div class="col-md-12">
					
                            <div class="row">
					
						 
					  
					 
                    <div class="col-md-2">
                             <div class="form-group ">
                                 <select id="provinsi" class="form-control">
                                 <option value=""> - Provinsi  -</option>
                                   <?php 
                                     $ko = $this->db->get("provinsi")->result();
                                       foreach($ko as $r){
                                         ?> <option value="<?php echo $r->id; ?>"> <?php echo $r->nama; ?> </option><?php   
                                           
                                       }
                                       
                                     ?>
                                   
                                   
                                 
                                 </select>
                             </div>
                       </div>

                 

                        
                     
                      
                       
                        <div class="col-md-6">
                                 
                                 <div class="form-group">
                                         <input type="text" size="30" name="keyword" id="keyword" class="form-control" placeholder="NSM/NPSN atau Nama Lembaga  ">
                                         
                                         
                                     
                                </div>
                          </div>
                          
                      <div class="col-md-1"><button type="button" class="btn btn-danger "  id="searchcustom"><span class="fa fa-search"></span> </button>
                    
                       
                      
                      </div>

     
 </div>


<div class="table-responsive">
<table id="datatableTable" class=" table table-bordered table-hover table-striped" width="100%">
<thead>
 <tr>
   <th class="center" width="3%" rowspan="2" >RANGKING</th>
   <th class="center" rowspan="2">LEMBAGA </th> 
   <th class="center" rowspan="2">PROVINSI</th> 
   <th class="center" rowspan="2">KABKOTA</th> 
   <th class="center" colspan="3">RAIHAN MEDALI </th> 
   
   
   
 </tr>

 <tr style="font-weight:bold">
								     
								      <td align="center"><img src="<?php echo base_url(); ?>__statics/img/medali/emas.png" width="50px" height="50px">  </td>
								      <td align="center"><img src="<?php echo base_url(); ?>__statics/img/medali/perak.png" width="50px" height="50px">   </td>
								      
								      <td align="center"><img src="<?php echo base_url(); ?>__statics/img/medali/perunggu.png" width="50px" height="50px">    </td>
								      
								    </tr>

</thead>
<tbody></tbody>
</table>
</div>

        </div>
        </div>
		

<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<b style="color:white;background:#33AFFF">Data sedang di tampilkan..</b>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data tidak atau belum tersedia",
						   "sLengthMenu": "Default menampilkan   _MENU_ Data <small>(Klik pada angkanya untuk memuat lebih banyak data sebelum mencetak excel)</small>"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": true,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							
							text:'<span class="fa fa-file-excel-o"></span> Cetak Excel',
							className :"btn btn-danger btn-sm"
							},
						
							{
							extend: 'colvis',
							
							text:'<span class="fa fa-columns "></span> Setting Kolom ',
							className :"btn btn-danger "
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("puslokal/g_MadrasahNasPeringkat"); ?>", 
						type: "post", 
						"data": function ( data ) {
							
					
						 data.keyword = $('#keyword').val();
						
						 data.provinsi = $('#provinsi').val();
						
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
	
		
		$(document).on('input click change', '#keyword,#searchcustom', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
		$(document).on('input', '#keyword', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
		
	
		$(document).on('change', '#kota,#jenjang,#provinsi,#trkompetisi_id,#status,#kota', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
			
	
				
				
		$(document).off("click",'input[name="status"]').on("click",'input[name="status"]',function(){
			
			var status= $('input[name="status"]:checked').val();
			var tmsiswa_id= $(this).attr("tmsiswa_id");
			  
			  loading();
				
				 $.post("<?php echo site_url("klokal/save_verval"); ?>",{tmsiswa_id:tmsiswa_id,status:status},function(data){
					   
					    alertify.success("Verifikasi Berhasil dilakukan");
						dataTable.ajax.reload(null,false);	
						jQuery.unblockUI({ });
						
						
				 })
			
		})
		
		$(document).off('input', '#keterangan').on('input', '#keterangan', function (event, messages) {			
			 
			   var tmsiswa_id= $(this).attr("tmsiswa_id"); 
			   
               var keterangan = $(this).val();
			  
			   
			   $.post("<?php echo site_url("klokal/save_keterangan"); ?>",{tmsiswa_id:tmsiswa_id,keterangan:keterangan},function(data){
					   
					    
						
				 })
		  
        });
		
		
		 $(document).on("change",".onchange2",function(event){
				 event.preventDefault()
				 var kota    = $("#kota").val();
				 var jenjang    = $("#jenjang").val();
			
				 
				 loading();
				
				 $.post("<?php echo site_url("publik/madrasah"); ?>",{kota:kota,jenjang:jenjang},function(data){
					 
					 $("#madrasah").html(data).show();
				
					 
					   
						jQuery.unblockUI({ });
						
						
				 })
			  
			  
			  
			 })

			 
			  $(document).on("change","#jenjang",function(event){
				 event.preventDefault()
				 var tmmadrasah_id    = $(this).val();
				 
			
				 
				 loading();
				
				 $.post("<?php echo site_url("publik/kompetisi"); ?>",{tmmadrasah_id:tmmadrasah_id},function(data){
					 
					 $("#trkompetisi_id").html(data).show();
				
					 
					   
						jQuery.unblockUI({ });
						
						
				 })
			  
			  
			  
			 })

</script>
			
                    