<div id="showform"></div>        
  						 <?php 
						$status_siswa = $this->input->get_post("status");
					   ?>                       
					  <div class="card " id="form1">
                        <div class="card-header">
						<?php 
						  $arje = array("1"=>"MI Sederajat","2"=>"MTs Sederajat","3"=>"MA Sederajat");
						 ?>
                            <h3><i class="fa fa-users"></i> Hasil KSM Nasional  <?php echo $arje[$jenjang]; ?></h3>
                        </div>
                        <br>
                        <br>
						
                          
						<hr>
							<div class="col-md-12">
					 <div class="row">
					  
					 
					                   <div class="col-md-2">
												<div class="form-group ">
													<select id="provinsi" class="form-control onchange"  target="kota" urlnya="<?php echo site_url('publik/kota'); ?>"">
													<option value=""> - Provinsi  -</option>
													  <?php 
													    $ko = $this->db->get("provinsi")->result();
														  foreach($ko as $r){
															?> <option value="<?php echo $r->id; ?>"> <?php echo $r->nama; ?> </option><?php   
															  
														  }
														  
														?>
													  
													  
													
													</select>
												</div>
										  </div>

										 <div class="col-md-2">
												<div class="form-group ">
													<select id="kota" class="form-control kota ">
													<option value=""> - Kabupaten/Kota -</option>
													  
													
													</select>
												</div>
										  </div>

										     <div class="col-md-2">
												<div class="form-group ">
													<select id="trkompetisi_id" class="form-control">
													<option value=""> - Cari kompetisi -</option>
													  <?php 
													    $ko = $this->db->get_where("tr_kompetisi",array("tmmadrasah_id"=>$jenjang))->result();
														  foreach($ko as $r){
															?> <option value="<?php echo $r->id; ?>"> <?php echo $r->nama; ?> </option><?php   
															  
														  }
														  
														?>
													  
													  
													
													</select>
												</div>
										  </div>
										  
									
										  <div class="col-md-2">
												<div class="form-group ">
												<select id="kolom" class="form-control">
												<option value="nisn">  NISN </option>
												<option value="nama" selected>  Nama Peserta </option>
												<option value="username">  NSM/NPSN Lembaga </option>
												<option value="madrasah"> Nama Lembaga </option>
												
												 
												
												</select>
										       </div>
										 </div>
										  
										   <div class="col-md-2">
													
													<div class="form-group">
															<input type="text" size="30" name="keyword" id="keyword" class="form-control" placeholder="Keyword  ">
															<input type="hidden" id="jenjang" class="form-control" value="<?php echo $jenjang; ?>">
															
														
												   </div>
											 </div>
											 
										 <div class="col-md-1"><button type="button" class="btn btn-danger "  id="searchcustom"><span class="fa fa-search"></span> </button>
								       
										  
										 
										 </div>
									
							
					</div>
					</div>
			
			
		           <hr>
		<div class="col-md-12">   				
		   <div class="table-responsive">
			<table id="datatableTable" class=" table table-bordered table-hover table-striped" width="100%">
			  <thead>
				<tr>
				   <th class="center" width="3%" rowspan="2">NO</th>
                   <th class="center" rowspan="2">FOTO </th>
                   <th class="center" rowspan="2">JENJANG</th>  
                   <th class="center" rowspan="2">KOMPETISI</th>  
                   <th class="center" rowspan="2">NOPESERTA</th> 
                   <th class="center" rowspan="2">NIK</th> 
                   <th class="center" rowspan="2">NAMA </th>	
                   <th class="center" rowspan="2">PERINGKAT </th>	
                   <th class="center" rowspan="2">MEDALI </th>	
				   <th class="center" rowspan="2">PROVINSI</th> 
				   <th class="center" rowspan="2">KAB/KOTA </th>   
				   <th class="center" rowspan="2">KECAMATAN </th>   
				   <th class="center" rowspan="2">DESA </th>   
				  
				   <th class="center" rowspan="2">ALAMAT </th>   
				   <th class="center"  rowspan="2">TEMPAT</th>					  
				  
				   <th class="center"  rowspan="2">TGLLAHIR</th>					  
				   <th class="center"  rowspan="2">NIK_AYAH</th>					  
				   <th class="center"  rowspan="2">NAMA_AYAH</th>					  
				   <th class="center"  rowspan="2">NIK_IBU</th>					  
				   <th class="center"  rowspan="2">NAMA_IBU</th>					  
				   				  
				   <th class="center" colspan="3"> NILAI </th>
				   <th class="center" colspan="5">ASAL LEMBAGA </th>
				   <th class="center" colspan="8">RINCIAN NILAI  </th>
				</tr>
				<tr>
                  <th> CBT </th>
                  <th> EKSPLORASI </th>
                  <th> NILAIAKHIR </th>

				  <th> JENIS </th>
				  <th> JENJANG </th>
				  <th> NSM/NPSN </th>
				  <th> LEMBAGA  </th>
				  <th> KONTAK </th>

				  <th> BNR_PG </th>
				  <th> BNR_ESSAY </th>
				  <th> SALAH </th>
				  <th> KSG </th>
				  <th> NILAI_CBT </th>
				  <th> EKSPLORASI </th>
				  <th> NILAI_PROVINSI </th>
				  <th> NILAI_KABKOTA </th>
				</tr>
			  </thead>
			  <tbody></tbody>
            </table>
			</div>
        </div>
        </div>
		

<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<b style="color:white;background:#33AFFF">Data sedang di tampilkan..</b>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data tidak atau belum tersedia",
						   "sLengthMenu": "Default menampilkan   _MENU_ Data <small>(Klik pada angkanya untuk memuat lebih banyak data sebelum mencetak excel)</small>"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": true,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							
							text:'<span class="fa fa-file-excel-o"></span> Cetak Excel',
							className :"btn btn-danger btn-sm"
							},
						
							{
							extend: 'colvis',
							
							text:'<span class="fa fa-columns "></span> Setting Kolom ',
							className :"btn btn-danger "
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("puslokal/g_pemenangNas"); ?>", 
						type: "post", 
						"data": function ( data ) {
							
						 data.kota    = $('#kota').val();
						 data.kolom    = $('#kolom').val();
						 data.jenjang = $('#jenjang').val();
						 data.madrasah = $('#madrasah').val();
						 data.trkompetisi_id = $('#trkompetisi_id').val();
						 data.status = $('#status').val();
						 data.keyword = $('#keyword').val();
						 data.status_siswa = $('#status_siswa').val();
						 data.kota = $('#kota').val();
						 data.provinsi = $('#provinsi').val();
						
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
	
		
		$(document).on('input click change', '#keyword,#searchcustom', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
		$(document).on('input', '#keyword', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
		
	
		$(document).on('change', '#kota,#jenjang,#provinsi,#trkompetisi_id,#status,#kota', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
			
	
				
				
		$(document).off("click",'input[name="status"]').on("click",'input[name="status"]',function(){
			
			var status= $('input[name="status"]:checked').val();
			var tmsiswa_id= $(this).attr("tmsiswa_id");
			  
			  loading();
				
				 $.post("<?php echo site_url("klokal/save_verval"); ?>",{tmsiswa_id:tmsiswa_id,status:status},function(data){
					   
					    alertify.success("Verifikasi Berhasil dilakukan");
						dataTable.ajax.reload(null,false);	
						jQuery.unblockUI({ });
						
						
				 })
			
		})
		
		$(document).off('input', '#keterangan').on('input', '#keterangan', function (event, messages) {			
			 
			   var tmsiswa_id= $(this).attr("tmsiswa_id"); 
			   
               var keterangan = $(this).val();
			  
			   
			   $.post("<?php echo site_url("klokal/save_keterangan"); ?>",{tmsiswa_id:tmsiswa_id,keterangan:keterangan},function(data){
					   
					    
						
				 })
		  
        });
		
		
		 $(document).on("change",".onchange2",function(event){
				 event.preventDefault()
				 var kota    = $("#kota").val();
				 var jenjang    = $("#jenjang").val();
			
				 
				 loading();
				
				 $.post("<?php echo site_url("publik/madrasah"); ?>",{kota:kota,jenjang:jenjang},function(data){
					 
					 $("#madrasah").html(data).show();
				
					 
					   
						jQuery.unblockUI({ });
						
						
				 })
			  
			  
			  
			 })

			 
			  $(document).on("change","#jenjang",function(event){
				 event.preventDefault()
				 var tmmadrasah_id    = $(this).val();
				 
			
				 
				 loading();
				
				 $.post("<?php echo site_url("publik/kompetisi"); ?>",{tmmadrasah_id:tmmadrasah_id},function(data){
					 
					 $("#trkompetisi_id").html(data).show();
				
					 
					   
						jQuery.unblockUI({ });
						
						
				 })
			  
			  
			  
			 })

</script>
			
                    