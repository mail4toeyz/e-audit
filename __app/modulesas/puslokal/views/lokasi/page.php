
<div id="showform"></div>        
                                 
								 <div class="card" id="form1">
								   <div class="card-header">
									   <h3><i class="fa fa-institution"></i><?php echo $title; ?></h3>
								   </div>
								   <br>
									
									   <div class="col-md-12">

									   <div class="alert alert-info" style="font-size:14px">
										Petunjuk ! <br>
										Lokasi Pelaksanaan Kompetisi Sains Madrasah Tahun  Tingkat Kabupaten/Kota ditentukan oleh komite Kabupaten/Kota, <br>
									
										<br>
									   </div>
											
											   
									
									
									 </div>
									 
								<div class="col-md-12">
								<div class="row">
												
								<div class="col-md-2">
												<div class="form-group ">
													<select id="provinsi" class="form-control onchange"  target="kota" urlnya="<?php echo site_url('publik/kota'); ?>"">
													<option value=""> - Provinsi  -</option>
													  <?php 
													    $ko = $this->db->get("provinsi")->result();
														  foreach($ko as $r){
															?> <option value="<?php echo $r->id; ?>"> <?php echo $r->nama; ?> </option><?php   
															  
														  }
														  
														?>
													  
													  
													
													</select>
												</div>
										  </div>

										 <div class="col-md-2">
												<div class="form-group ">
													<select id="kota" class="form-control kota ">
													<option value=""> - Kabupaten/Kota -</option>
													  
													
													</select>
												</div>
										  </div>
													
												   
													<div class="col-md-3">
														   <div class="form-group ">
														   <div class="form-group">
																   <input type="text" size="30" name="keyword" id="keyword" class="form-control" placeholder=" Ketik disini.. ">
																   
															   </div>
														  </div>
													</div>
													
													<div class="col-md-1"><button type="submit" class="btn green tooltips" data-container="body" data-placement="bottom" title="Lakukan Pencarian" id="searchcustom"><span class="fa fa-search"></span></button>
												   </div>
									   
							   </div>
							   </div>
					   
						
				   <div class="col-md-12">   				
					  <div class="table-responsive">
					   <table id="datatableTable" class="table  table-bordered table-hover table-striped" >
						 <thead>
						   <tr>
							  <th class="center" width="3%" >No</th>
							  <th class="center" width="10%">Provinsi </th>	
							  <th class="center" width="10%">Kab/kota </th>	
							  <th class="center">Lokasi </th>				  
							  <th class="center">Alamat </th>				  
							  <th class="center">Komputer </th>				  
							  <th class="center">Cakupan Wilayah </th>		  
							  <th class="center">Jenjang </th>		  
							  <th class="center">Jml Peserta </th>		  
							 
						   </tr>
							
						 </thead>
						 <tbody></tbody>
					   </table>
					   </div>
				   </div>
				   </div>
				   
		   
		  <script type="text/javascript">
			 var dataTable = $('#datatableTable').DataTable( {
								   "processing": true,
								   "language": {
								   "processing": '<b style="color:white;background:#33AFFF">Data sedang di tampilkan..</b>',
									 "oPaginate": {
									   "sFirst": "Halaman Pertama",
									   "sLast": "Halaman Terakhir",
										"sNext": "Selanjutnya",
										"sPrevious": "Sebelumnya"
										},
								   "sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
									"sInfoEmpty": "Tidak ada data yang di tampilkan",
									  "sZeroRecords": "Data belum tersedia, silahkan input ",
									  "sLengthMenu": "Default menampilkan   _MENU_ Data <small>(Klik pada angkanya untuk memuat lebih banyak data sebelum mencetak ke format excel)</small>"
							   },
							   
							   "serverSide": true,
							   "searching": false,
							   "responsive": true,
							   "lengthMenu": [[5,10,25, 50,100,200,300,500,1000, 800000000], [5,10,25, 50,100,200,300,500,1000,"All"]],
								"dom": 'Blfrtip',
								"sPaginationType": "full_numbers",
							   "buttons": [
							   
										
									   {
									   extend: 'excelHtml5',
									   
									   text:'<span class="fa fa-file-excel-o"></span> Cetak Excel',
									   className :"btn btn-danger btn-sm"
									   },
									   
									   
									   {
									   extend: 'copyHtml5',
									   
									   text:'<span class="fa fa-copy "></span> Copy Data ',
									   className :"btn btn-danger btn-sm"
								   },
									   {
									   extend: 'csvHtml5',
									   
									   text:'<span class="fa fa-file-archive-o "></span> Cetak CSV ',
									   className :"btn btn-danger "
								   },
								   
									   {
									   extend: 'colvis',
									   
									   text:'<span class="fa fa-columns "></span> Setting Kolom ',
									   className :"btn btn-danger "
								   }
							   ],
							   
							   "ajax":{
								   url :"<?php echo site_url("puslokal/g_lokasi"); ?>", 
								   type: "post", 
								   "data": function ( data ) {
								   data.provinsi = $('#provinsi').val();
								   data.kota = $('#kota').val();
								   data.status = $('#status').val();
								   data.kolom = $('#kolom').val();
								   data.keyword = $('#keyword').val();
						   
							   }
								   
							   },
							   "rowCallback": function( row, data ) {
								   
								   
							   }
						   } );
						   
			   
				   
				   $(document).on('input click change', '#keyword,#searchcustom,#status,#jenis,#jenjang,#kota,#provinsi', function (event, messages) {			
						
						  dataTable.ajax.reload(null,false);	        
					 
				   });
				   
				   $(document).on(' click ', '#cancelr', function (event, messages) {			
						  $("#showform").html("");
						  dataTable.ajax.reload(null,false);	        
					 
				   });
		   
				   
				   $(document).on(' click ', '#cancelr', function (event, messages) {			
						  $("#showform").html("");
						  dataTable.ajax.reload(null,false);	        
					 
				   });
				   
				   
				   
					   $(document).on('input  change', '#kota,#kolom,#keyword,#searchcustom', function (event, messages) {			
						
						  dataTable.ajax.reload(null,false);	        
					 
				   });
				   
				   
				   
			
			
		   
		   </script>
		
							   