   <div id="showform"></div>        
                                 
					  <div class="card form" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-user"></i>Pendaftaran Official KSM Provinsi Anda </h3>
                        </div>
                        <br>
                         
                            <div class="col-md-12">
							<div class="alert alert-info">
								 Petunjuk ! <br>
								Silahkan Input official /Panitia Provinsi (Proktor,Pengawas dll) agar mendapatkan piagam
								</div>
						
							   
								    <button class="btn  btn-primary" id="tambahdata"  urlnya="<?php echo site_url("plokal/f_official"); ?>">
									  <i class="fa  fa-plus-square "></i>
											 Tambah Data 
									</button>
								 
									<br>
			
		
			              </div>
			
		           <hr>
		<div class="col-md-12">   				
		   <div style="width: 100%;overflow-x: auto; white-space: nowrap;">
			<table id="datatableTable" class="  table-bordered table-hover table-striped" width="100%">
			  <thead>
			  <tr>
				   <th class="center" width="3%">No</th>
				   <th class="center">Aksi  </th>			  
				   <th class="center">Provinsi  </th>				  
				   <th class="center">Tingkat  </th>				  
				   <th class="center">Tingkat  </th>				  
				   <th class="center">Foto  </th>				  
				   <th class="center">Nama </th>				  
				   <th class="center">Jabatan Struktural</th>				  
				   <th class="center">Jabatan KSM</th>				  
				   <th class="center">Kontak  </th>				  
				   <th class="center">Alamat </th>		
				   		  
				  		  
				   				  		  
				  
				</tr>
			  </thead>
			  <tbody></tbody>
            </table>
			</div>
        </div>
        </div>
		

<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<b style="color:white;background:#33AFFF">Data sedang di tampilkan..</b>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data tidak atau belum tersedia, silahkan input data",
						   "sLengthMenu": "Default menampilkan   _MENU_ Data <small>(Klik pada angkanya untuk memuat lebih banyak data)</small>"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[5,10,25, 50,100,200,300,500,1000, 800000000], [5,10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							exportOptions: {
							  columns: [ 0,1,2,3,4,5,6]
							},
							text:'<span class="fa fa-file-excel-o"></span> Cetak Excel',
							className :"btn btn-danger btn-sm"
							},
							
							
							{
							extend: 'copyHtml5',
							exportOptions: {
							       columns: [ 0,1,2,3,4,5,6]
							},
							text:'<span class="fa fa-copy "></span> Copy Data ',
							className :"btn btn-danger btn-sm"
						},
							{
							extend: 'csvHtml5',
							exportOptions: {
							   columns: [ 0,1,2,3,4,5,6]
							},
							text:'<span class="fa fa-file-archive-o "></span> Cetak CSV ',
							className :"btn btn-danger "
						},
						
							{
							extend: 'colvis',
							
							text:'<span class="fa fa-columns "></span> Setting Kolom ',
							className :"btn btn-danger "
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("plokal/g_official"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
	
		
		$(document).on('input click change', '#keyword,#searchcustom', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
		
		

</script>
			
                    