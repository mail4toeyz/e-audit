           <div class="row">
                <div class="col-md-12">                      
					  <div class="card form" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-user"></i> <?php echo $title; ?> </h3>
                        </div>
                        <br>
                          <?php $attributes = array('class' => 'form-horizontal', 'role'=>'form','name' => 'simpangambar', 'id' => 'simpangambar', 'method' => "post","enctype"=>"multipart/form-data", 'url' =>site_url("puslokal/simpanpendamping")); ?>
			                     <?php echo form_open("javascript:void(0)", $attributes); ?>
                            <div class="col-md-12">
							<div class="alert alert-info">
							 Petunjuk ! <br>
							 Silahkan isi semua kolom bertanda (*) 
							 </div>
                            <div class="row">
							
									<div class="col-md-3">
										<div class="form-group">
											<label for="name">Upload Foto (3x4) </label>
											<input type="file" class="form-control" name="file" id="image">
										</div>
										
										<div class="form-group">
										   <?php 
										     if(isset($dataform)){
												 if($dataform->foto==""){
													$file = "";
												 }else{
													 $file = $dataform->foto;
												 }
												 
											 }else{
												 
												    $file = "";
											 }
											 
											 ?>
											<img src="<?php echo base_url(); ?>__statics/upload/<?php echo $file; ?>" class="img-thumbnail img-responsive" style="height:200px" alt="foto" id="blah" >
										</div>
										
									</div>
									
									<div class="col-md-9">
										<div class="form-group">
											<label for="name">Nama (*)   </label>
											<input type="hidden" class="form-control" name="id" value="<?php echo isset($dataform) ?  $dataform->id :""; ?>">
											<input type="text" class="form-control" name="f[nama]" value="<?php echo isset($dataform) ?  $dataform->nama :""; ?>">
										</div>
										
										<div class="form-group">
											<label for="name">Nomor Hp</label>
											<input type="text" class="form-control" name="f[hp]" value="<?php echo isset($dataform) ?  $dataform->hp :""; ?>">
										</div>
										<div class="form-group">
											<label for="name">Jabatan </label>
											<input type="text" class="form-control" name="f[jabatan]" value="<?php echo isset($dataform) ?  $dataform->jabatan :""; ?>" placeholder="Contoh : Ketua Komite ">
										</div>
										
										<div class="form-group">
											<label for="name">Alamat Lengkap </label>
											<textarea type="text" class="form-control" name="f[alamat]" value=""><?php echo isset($dataform) ?  $dataform->alamat :""; ?></textarea>
										</div>
									</div>
									
									
					

										
										
										
									
									
									  <div class="col-md-12">
										
									  </div>
									
									
									
							</div>
							
							
							
                             

							<center>
                            <button type="submit" class="btn btn-general btn-white">Simpan Data </button>  
                            <button type="reset" class="btn btn-general btn-blue">Reset </button> 
                            <button type="button" class="btn btn-general btn-white" id="cancel">Tutup Form </button> 
							<center>
							<br>
                        <?php echo form_close(); ?>
                    </div>
                    </div>
                    </div>
					
<script>
					function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blah').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#image").change(function() {
  readURL(this);
});
</script>