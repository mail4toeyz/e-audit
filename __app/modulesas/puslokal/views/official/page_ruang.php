   <div id="showform"></div>        
                                 
					  <div class="card form" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-user"></i>Ruang  Kabkota </h3>
                        </div>
                        <br>
                         
                            <div class="col-md-12">
								<br>
							 <!-- <a class="btn  btn-primary"   href="<?php echo site_url("puslokal/generateAkunkabko"); ?>">
									  <i class="fa  fa-plus-square "></i>
											 Generate Akun
							</a> -->
			
		
			              </div>
			
		           <hr>
		<div class="col-md-12">   				
		   <div style="width: 100%;overflow-x: auto; white-space: nowrap;">
			<table id="datatableTable" class=" table table-bordered table-hover table-striped" width="100%">
			  <thead>
				<tr>
				   <th class="center" width="3%">No</th>
				  				  
				   <th class="center">ID Ruang  </th>				  
				   <th class="center">Ruang  </th>				  
				   <th class="center">Provinsi  </th>				  
				   <th class="center">KabKota  </th>  
				   <th class="center">KD PROV  </th>				  
				   <th class="center">KD KABKO  </th> 			  
				   <th class="center">Kompetisi  </th>					  
				   <th class="center">Username </th>				  
				   <th class="center">Password</th>				  
				   <th class="center">Peserta</th>				  
				   
				  
				</tr>
			  </thead>
			  <tbody></tbody>
            </table>
			</div>
        </div>
        </div>
		

<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<b style="color:white;background:#33AFFF">Data sedang di tampilkan..</b>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data tidak atau belum tersedia, silahkan input data",
						   "sLengthMenu": "Default menampilkan   _MENU_ Data <small>(Klik pada angkanya untuk memuat lebih banyak data)</small>"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[20,10,25, 50,100,200,300,500,1000, 800000000], [10,10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							
							text:'<span class="fa fa-file-excel-o"></span> Cetak Excel',
							className :"btn btn-danger btn-sm"
							},
							
							
							{
							extend: 'copyHtml5',
							
							text:'<span class="fa fa-copy "></span> Copy Data ',
							className :"btn btn-danger btn-sm"
						},
							{
							extend: 'csvHtml5',
						
							text:'<span class="fa fa-file-archive-o "></span> Cetak CSV ',
							className :"btn btn-danger "
						},
						
							{
							extend: 'colvis',
							
							text:'<span class="fa fa-columns "></span> Setting Kolom ',
							className :"btn btn-danger "
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("puslokal/g_ruang"); ?>", 
						type: "post", 
						"data": function ( data ) {
						  
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
	
		
		$(document).on('input click change', '#keyword,#searchcustom', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
		
		

</script>
			
                    