   <div id="showform"></div>        
                                 
					  <div class="card form" id="form1">
                        <div class="card-header">
                            <h3><i class="fa fa-institution"></i><?php echo $title; ?></h3>
                        </div>
                        <br>
                         
                            <div class="col-md-12">
							<div class="alert alert-info" style="font-size:12px">
							 Petunjuk ! <br>
							 Dibawah ini adalah Panitia Komite Kabupaten/Kota yang sudah didaftarkan  <br>
								</div>
								 
									<br>
			 
			 
			              </div>
			
		           <hr>
		<div class="col-md-12">   				
		   <div class="table-responsive">
			<table id="datatableTable" class="table  table-bordered table-hover table-striped" >
			  <thead>
				<tr>
				   <th class="center" width="3%">No</th>
				   <th class="center">Kabupaten/Kota </th>				  
				   <th class="center">Foto  </th>				  
				   <th class="center">Nama </th>				  
				   <th class="center">Jabatan</th>				  
				   <th class="center">Kontak  </th>				  
				   <th class="center">Alamat </th>				  
				  
				 		  
				  
				  
				</tr>
			  </thead>
			  <tbody></tbody>
            </table>
			</div>
        </div>
        </div>
		

<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<b style="color:white;background:#33AFFF">Data sedang di tampilkan..</b>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data tidak atau belum tersedia",
						   "sLengthMenu": "Default menampilkan   _MENU_ Data <small>(Klik pada angkanya untuk memuat lebih banyak data sebelum anda mencetak Excel)</small>"
				    },
					
					"serverSide": true,
					"searching": true,
					"responsive": true,
					"lengthMenu": [[5,10,25, 50,100,200,300,500,1000, 800000000], [5,10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							
							text:'<span class="fa fa-file-excel-o"></span> Cetak Excel',
							className :"btn btn-danger btn-sm"
							},
							
							
							{
							extend: 'copyHtml5',
						
							text:'<span class="fa fa-copy "></span> Copy Data ',
							className :"btn btn-danger btn-sm"
						},
							{
							extend: 'csvHtml5',
							exportOptions: {
							   columns: [ 0,1,2,3]
							},
							text:'<span class="fa fa-file-archive-o "></span> Cetak CSV ',
							className :"btn btn-danger "
						},
						
							{
							extend: 'colvis',
							
							text:'<span class="fa fa-columns "></span> Setting Kolom ',
							className :"btn btn-danger "
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("plokal/g_komite"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
	
		
		$(document).on('input click change', '#keyword,#searchcustom', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
		
		$(document).on(' click ', '#cancelr', function (event, messages) {			
			   $("#showform").html("");
			   dataTable.ajax.reload(null,false);	        
		  
        });
		

</script>
			
                    