<div id="showform"></div>        
  						 
                       
					  <div class="card " id="form1">
                        <div class="card-header">
						
                            <h3><i class="fa fa-users"></i> Monitor Peserta </h3>
                        </div>
                        <br>
                         
                            <div class="col-md-12">
							<div class="alert alert-info" style="font-size:10px">
							   Dibawah ini adalah peserta KSM tingkat Kabupaten/Kota yang sedang melaksanakan Ujian 
						      <br>
							</div>
								
			 
			 
			              </div>
						  
                           
							<div class="col-md-12">
					 <div class="row">
					  
					              
										     <div class="col-md-2">
												<div class="form-group ">
													<select id="trkompetisi_id" class="form-control trkompetisi_id" target="kota">
													<option value=""> - Cari kompetisi -</option>
													  <?php 
													    $ko = $this->db->get("tr_kompetisi")->result();
														  foreach($ko as $r){
															?> <option value="<?php echo $r->id; ?>"> <?php echo $r->nama; ?> - <?php echo $this->Di->jenjangnama($r->tmmadrasah_id); ?> </option><?php   
															  
														  }
														  
														?>
													  
													  
													
													</select>
												</div>
										  </div>
										  
										

                                          <div class="col-md-2">
												<div class="form-group ">
													<select id="sesi" class="form-control">
													<option value=""> - Cari Sesi  -</option>
													  <?php 
													    $ko = $this->db->get("tm_sesi")->result();
														  foreach($ko as $r){
															?> <option value="<?php echo $r->sesi; ?>"> <?php echo $r->sesi; ?>  </option><?php   
															  
														  }
														  
														?>
													  
													  
													
													</select>
												</div>
										  </div>

                                          <div class="col-md-2">
												<div class="form-group ">
													<select id="status" class="form-control">
													<option value=""> - Status Ujian -</option>
													<option value="1"> Sedang Mengerjakan </option>
													<option value="2"> Selesai Mengerjakan </option>
													 
													  
													
													</select>
												</div>
										  </div>
										  


                                          <div class="col-md-2">
                                          <div class="form-group ">
                                          <input type="text" size="30" name="keyword" id="keyword" class="form-control" placeholder="Cari Nama Peserta ">
                                                        </div>            
                                         </div>
										
										   
											 
										 <div class="col-md-1"><button type="button" class="btn btn-danger "  id="searchcustom"><span class="fa fa-search"></span> </button>
								       
										  
										 
										 </div>
									
							
					</div>
					</div>
			</form>
			
		           <hr>
		<div class="col-md-12">   				
		   <div class="table-responsive">
			<table id="datatableTable" class=" table table-bordered table-hover table-striped" width="100%">
			  <thead>
				<tr>
				   <th class="center" width="3%" >NO</th>
                   <th class="center">PROVINSI </th>
                   <th class="center">KABUPATEN/KOTA </th>
				   <th class="center">NOPESERTA</th> 
                   <th class="center">NAMA </th>
                   <th class="center">KOMPETISI</th>				   	
				   <th class="center">RUANG </th>   
				   <th class="center">STATUS </th>   
                </tr>
			  </thead>
			  <tbody></tbody>
            </table>
			</div>
        </div>
        </div>
		

<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<b style="color:white;background:#33AFFF">Data sedang di tampilkan..</b>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data tidak atau belum tersedia",
						   "sLengthMenu": "Default menampilkan   _MENU_ Data <small>(Klik pada angkanya untuk memuat lebih banyak data sebelum mencetak excel)</small>"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": true,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							
							text:'<span class="fa fa-file-excel-o"></span> Cetak Excel',
							className :"btn btn-danger btn-sm"
							},
						
							{
							extend: 'colvis',
							
							text:'<span class="fa fa-columns "></span> Setting Kolom ',
							className :"btn btn-danger "
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("puslokal/g_monitorpeserta"); ?>", 
						type: "post", 
						"data": function ( data ) {
							
						 data.kota    = $('#kota').val();
						 data.kolom    = $('#kolom').val();
						 data.jenjang = $('#jenjang').val();
						 data.madrasah = $('#madrasah').val();
						 data.trkompetisi_id = $('#trkompetisi_id').val();
						 data.status = $('#status').val();
						 data.keyword = $('#keyword').val();
						 data.status_siswa = $('#status_siswa').val();
						 data.kota = $('#kota').val();
						 data.sesi = $('#sesi').val();
						
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
	
		
		$(document).on('input click change', '#keyword,#searchcustom', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
		$(document).on('input', '#keyword', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
		
		
		
		$(document).on('change', '#kota,#jenjang,#madrasah,#trkompetisi_id,#status,#kota,#sesi', function (event, messages) {			
			 
			   dataTable.ajax.reload(null,false);	        
		  
        });
			
	
				
				
		$(document).off("click",'input[name="status"]').on("click",'input[name="status"]',function(){
			
			var status= $('input[name="status"]:checked').val();
			var tmsiswa_id= $(this).attr("tmsiswa_id");
			  
			  loading();
				
				 $.post("<?php echo site_url("klokal/save_verval"); ?>",{tmsiswa_id:tmsiswa_id,status:status},function(data){
					   
					    alertify.success("Verifikasi Berhasil dilakukan");
						dataTable.ajax.reload(null,false);	
						jQuery.unblockUI({ });
						
						
				 })
			
		})
		
		$(document).off('input', '#keterangan').on('input', '#keterangan', function (event, messages) {			
			 
			   var tmsiswa_id= $(this).attr("tmsiswa_id"); 
			   
               var keterangan = $(this).val();
			  
			   
			   $.post("<?php echo site_url("klokal/save_keterangan"); ?>",{tmsiswa_id:tmsiswa_id,keterangan:keterangan},function(data){
					   
					    
						
				 })
		  
        });
		
		
		 $(document).on("change",".onchange2",function(event){
				 event.preventDefault()
				 var kota    = $("#kota").val();
				 var jenjang    = $("#jenjang").val();
			
				 
				 loading();
				
				 $.post("<?php echo site_url("publik/madrasah"); ?>",{kota:kota,jenjang:jenjang},function(data){
					 
					 $("#madrasah").html(data).show();
				
					 
					   
						jQuery.unblockUI({ });
						
						
				 })
			  
			  
			  
			 })

			 
			  $(document).on("change","#jenjang",function(event){
				 event.preventDefault()
				 var tmmadrasah_id    = $(this).val();
				 
			
				 
				 loading();
				
				 $.post("<?php echo site_url("publik/kompetisi"); ?>",{tmmadrasah_id:tmmadrasah_id},function(data){
					 
					 $("#trkompetisi_id").html(data).show();
				
					 
					   
						jQuery.unblockUI({ });
						
						
				 })
			  
			  
			  
			 })

</script>
			
                    