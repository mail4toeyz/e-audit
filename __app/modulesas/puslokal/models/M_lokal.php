<?php

class M_lokal extends CI_Model {
 
    public function __construct() {
        parent::__construct();
    }

    
	public function m_monitorpeserta($paging){
		$cbt = $this->load->database("cbt",true);
        $key = $this->input->get_post("keyword");
        $status = $this->input->get_post("status");
        $sesi = $this->input->get_post("sesi");
        $provinsi = $this->input->get_post("provinsi");
        $kota     = $this->input->get_post("kota");

		$cbt->select("*");
        $cbt->from('d_psrt');
       
	    if(!empty($key)){ $cbt->where("UPPER(nama) LIKE '%".$key."%'"); } 
	    if(!empty($sesi)){ $cbt->where("sesi",$sesi); } 
		
		if(!empty($status)){

			
				$cbt->where("id IN(select idPS  FROM t_pkujps where sta='{$status}' and strt is not null)");
				
			

		}

		
		
         if($paging==true){
				     $cbt->limit($_REQUEST['length'],$_REQUEST['start']);
					 $cbt->order_by("kdr","ASC");
					 $cbt->order_by("idJlr","ASC");
		
			 }
      
		
		
		return $cbt->get();
		
	}


	
	public function m_kabkota($paging){
        $key = $_REQUEST['search']['value'];
		$this->db->select("*");
        $this->db->from('kota');
       
	    if(!empty($key)){ $this->db->where("UPPER(nama) LIKE '%".$key."%'"); } 
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("id","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}

	public function g_ruang($paging){
        $key = $_REQUEST['search']['value'];
		$this->db->select("*");
        $this->db->from('tm_ruang');
        $this->db->where('tingkat','kabko');
       
	    if(!empty($key)){ $this->db->where("UPPER(nama) LIKE '%".$key."%'"); } 
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("provinsi_id","ASC");
					 $this->db->order_by("kota_id","ASC");
					 $this->db->order_by("trkompetisi_id","ASC");
					 $this->db->order_by("ruang","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	

	public function m_madrasah($paging){
               $jenis     = $_POST['jenis'];
               $jenjang   = $_POST['jenjang'];
               $status    = $_POST['status'];
               $kolom     = $_POST['kolom'];
               $kota     = $_POST['kota'];
        $keyword = $_POST['keyword'];
		$this->db->select("*");
        $this->db->from('tm_madrasah');
     
        //$this->db->where('sts',0);
	    if(!empty($status)){ $this->db->where("status",$status); } 
	    if(!empty($kota)){ $this->db->where("kota",$kota); } 
	    if(!empty($jenis)){ $this->db->where("jenis",$jenis); } 
	    if(!empty($jenjang)){ $this->db->where("jenjang",$jenjang); } 
	    if(!empty($keyword)){ $this->db->where("UPPER(".$kolom.") LIKE '%".$keyword."%'"); } 
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("kota","ASC");
					 $this->db->order_by("jenjang","ASC");
					 $this->db->order_by("nama","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
	
	
	public function m_peserta($paging){
     
     
        $kolom     = $_POST['kolom'];
        $status_siswa     = $_POST['status_siswa'];
        $jenjang     = $_POST['jenjang'];
		$kota     = $_POST['kota'];
		$provinsi     = $_POST['provinsi'];
        $trkompetisi_id   = $_POST['trkompetisi_id'];
        $status   = $this->input->post("status",true);
		$keyword   = $_POST['keyword'];
		
		$this->db->select("*");
        $this->db->from('v_siswa');
     
		$this->db->where("jenjang_madrasah",$jenjang);
		if(!empty($kota)){ $this->db->where("kota_madrasah",$kota); } 
		if(!empty($provinsi)){ $this->db->where("provinsi_madrasah",$provinsi); } 
	
		
		if(!empty($keyword)){ $this->db->where("UPPER({$kolom}) LIKE '%".strtoupper($keyword)."%'"); }
		if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 
		
		
		if(!empty($status)){

			if($status==0){

				$this->db->where("status < 1");
			}else{
			$this->db->where("status",$status);
			}
		   
		    
		} 

         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("provinsi_madrasah","ASC");
					 $this->db->order_by("kota_madrasah","ASC");
					 $this->db->order_by("tmmadrasah_id","ASC");
					 $this->db->order_by("trkompetisi_id","ASC");
					 $this->db->order_by("nama","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}

	public function m_pesertaprov($paging){
     
     
        $kolom     = $_POST['kolom'];
        $status_siswa     = $_POST['status_siswa'];
        $jenjang     = $_POST['jenjang'];
		$kota     = $_POST['kota'];
		$provinsi     = $_POST['provinsi'];
        $trkompetisi_id   = $_POST['trkompetisi_id'];
        $status   = $this->input->post("status",true);
		$keyword   = $_POST['keyword'];
		
		$this->db->select("*");
        $this->db->from('v_siswa');
        $this->db->where('status2',1);
     
		$this->db->where("jenjang_madrasah",$jenjang);
		if(!empty($kota)){ $this->db->where("kota_madrasah",$kota); } 
		if(!empty($provinsi)){ $this->db->where("provinsi_madrasah",$provinsi); } 
	
		
		if(!empty($keyword)){ $this->db->where("UPPER({$kolom}) LIKE '%".strtoupper($keyword)."%'"); }
		if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 
		
		
		

         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("provinsi_madrasah","ASC");
					 $this->db->order_by("kota_madrasah","ASC");
					 $this->db->order_by("tmmadrasah_id","ASC");
					 $this->db->order_by("trkompetisi_id","ASC");
					 $this->db->order_by("nama","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}

	public function m_pesertanas($paging){
     
     
        $kolom     = $_POST['kolom'];
        $status_siswa     = $_POST['status_siswa'];
        $jenjang     = $_POST['jenjang'];
		$kota     = $_POST['kota'];
		$provinsi     = $_POST['provinsi'];
        $trkompetisi_id   = $_POST['trkompetisi_id'];
        $status   = $this->input->post("status",true);
		$keyword   = $_POST['keyword'];
		
		$this->db->select("*");
        $this->db->from('v_siswa');
        $this->db->where('status3',1);
     
		$this->db->where("jenjang_madrasah",$jenjang);
		if(!empty($kota)){ $this->db->where("kota_madrasah",$kota); } 
		if(!empty($provinsi)){ $this->db->where("provinsi_madrasah",$provinsi); } 
	
		
		if(!empty($keyword)){ $this->db->where("UPPER({$kolom}) LIKE '%".strtoupper($keyword)."%'"); }
		if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 
		
		
		

         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("provinsi_madrasah","ASC");
					 $this->db->order_by("kota_madrasah","ASC");
					 $this->db->order_by("tmmadrasah_id","ASC");
					 $this->db->order_by("trkompetisi_id","ASC");
					 $this->db->order_by("nama","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	public function m_pesertates($paging){
           
        $jenjang   = $_POST['jenjang'];
       
        $trkompetisi_id   = $_POST['trkompetisi_id'];        
        $keyword   = $_POST['keyword'];
		
		$this->db->select("no_test,id,tgl_lahir,tmmadrasah_id,nama,gender,trkompetisi_id,kota,jenjang,nilai,benar,salah,kosong,d_p");
        $this->db->from('tm_siswa');
    
		$this->db->where("status",1);
		
       $this->db->where("jenjang",$jenjang);
			//$this->db->where("tmmadrasah_id in (select id from tm_madrasah where kota='".$_SESSION['tmkota_id']."') and jenjang ='".$jenjang."'");
	
		if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 
		if(!empty($keyword)){ $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'"); }
		
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("trkompetisi_id","asc");
					 $this->db->order_by("nama","asc");
					 
		
			 }
      
		
		
		return $this->db->get();
		
	}

	public function m_hasilkabko($paging){
           
		$kolom     = $_POST['kolom'];
        $jenjang     = $_POST['jenjang'];
		$kota     = $_POST['kota'];
		$provinsi     = $_POST['provinsi'];
        $trkompetisi_id   = $_POST['trkompetisi_id'];
        $status   = $this->input->post("status",true);
		$keyword   = $_POST['keyword'];
		$d_p       = $_POST['d_p'];
		$ctt       = $_POST['ctt'];
		$skrn       = $_POST['skrn'];
		$medali       = $_POST['medali'];
		
		$this->db->select("nik_siswa,medali,id,no_test,nama,tgl_lahir,skrn,provinsi_madrasah,kota_madrasah,trkompetisi_id,ketkabko,bnr_pg,bnr_isi,jml_benar,slh,ksg,nla,jenis,jenjang,username,madrasah,isTegur,textTegur");
        $this->db->from('v_siswa_kabkota');
        $this->db->where('nla is not null');
        $this->db->where('bnr_isi is not null');
        $this->db->where('bnr_pg is not null');
        $this->db->where('slh is not null');
        $this->db->where('ksg is not null');
    
		
       $this->db->where("jenjang_madrasah",$jenjang);
			
	   if(!empty($kota)){ $this->db->where("kota_madrasah",$kota); } 
	   if(!empty($provinsi)){ $this->db->where("provinsi_madrasah",$provinsi); } 
   
	   
	   if(!empty($keyword)){ $this->db->where("UPPER({$kolom}) LIKE '%".strtoupper($keyword)."%'"); }
	   if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 

	   if(!empty($d_p)){ $this->db->where("d_p",$d_p); } 
	   if(!empty($medali)){ $this->db->where("medali",$medali); } 
	   if(!empty($ctt)){ 
		   if($ctt ==1){
			$this->db->where("ctt > 0");
		   }else{
			$this->db->where("ctt < 0");  
		   }
		   
		 } 
	   if(!empty($skrn)){ $this->db->where("skrn",$skrn); } 

		
	   
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("provinsi_madrasah","asc");
					 $this->db->order_by("kota_madrasah","asc");
					 $this->db->order_by("trkompetisi_id","asc");
					 $this->db->order_by("trkompetisi_id","asc");
					 $this->db->order_by("nla","DESC");
					 $this->db->order_by("jml_benar","DESC");
					 $this->db->order_by("slh","ASC");
					 $this->db->order_by("tgl_lahir","DESC");
					 
		
			 }
      
		
		
		return $this->db->get();
		
	}

	public function m_hasilkabko_jml($paging){
           
		$kolom     = $_POST['kolom'];
        $jenjang     = $_POST['jenjang'];
		$kota     = $_POST['kota'];
		$provinsi     = $_POST['provinsi'];
        $trkompetisi_id   = $_POST['trkompetisi_id'];
        $status   = $this->input->post("status",true);
		$keyword   = $_POST['keyword'];
		$d_p       = $_POST['d_p'];
		$ctt       = $_POST['ctt'];
		$skrn       = $_POST['skrn'];
		
		$this->db->select("count(id) as jml");
        $this->db->from('v_siswa_kabkota');
        $this->db->where('nla is not null');
    
		
       $this->db->where("jenjang_madrasah",$jenjang);
			
	   if(!empty($kota)){ $this->db->where("kota_madrasah",$kota); } 
	   if(!empty($provinsi)){ $this->db->where("provinsi_madrasah",$provinsi); } 
   
	   
	   if(!empty($keyword)){ $this->db->where("UPPER({$kolom}) LIKE '%".strtoupper($keyword)."%'"); }
	   if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 

	   if(!empty($d_p)){ $this->db->where("d_p",$d_p); } 
	   if(!empty($ctt)){ $this->db->where("ctt",$ctt); } 
	   if(!empty($skrn)){ $this->db->where("skrn",$skrn); } 

		
	   
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("trkompetisi_id","asc");
					 $this->db->order_by("nla","DESC");
					 $this->db->order_by("jml_benar","DESC");
					 $this->db->order_by("slh","ASC");
					 $this->db->order_by("tgl_lahir","DESC");
					 
		
			 }
      
		
		
		$d=  $this->db->get()->row();
		return $d->jml;
		
	}

	public function m_hasilprov($paging){
           
		$kolom     = $_POST['kolom'];
        $jenjang     = $_POST['jenjang'];
		$kota     = $_POST['kota'];
		$provinsi     = $_POST['provinsi'];
        $trkompetisi_id   = $_POST['trkompetisi_id'];
        $status   = $this->input->post("status",true);
		$keyword   = $_POST['keyword'];
		$d_p       = $_POST['d_p'];
		$ctt       = $_POST['ctt'];
		$skrn       = $_POST['skrn'];
		$medali       = $_POST['medali'];
		
		$this->db->select("nik_siswa,medali,id,no_test2,nama,tgl_lahir,ctt,skrn,provinsi_madrasah,kota_madrasah,trkompetisi_id,ketkabko,bnr_pg,bnr_isi,jml_benar,slh,ksg,nla,jenis,jenjang,username,madrasah,nilai_kabko,status_provinsi,p_bnr,konf_prov,peringkat_prov,peringkatprov_seb");
        $this->db->from('v_siswa_provinsi');
      //  $this->db->where('nla is not null');
    
	  if(!empty($jenjang)){
       $this->db->where("jenjang_madrasah",$jenjang);
	  }
			
	   if(!empty($kota)){ $this->db->where("kota_madrasah",$kota); } 
	   if(!empty($provinsi)){ $this->db->where("provinsi_madrasah",$provinsi); } 
   
	   
	   if(!empty($keyword)){ $this->db->where("UPPER({$kolom}) LIKE '%".strtoupper($keyword)."%'"); }
	   if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 

	   if(!empty($d_p)){ $this->db->where("peringkat_prov",$d_p); } 
	   if(!empty($medali)){ $this->db->where("peringkatprov_seb",$medali); } 
	   if(!empty($ctt)){ 
		     if($ctt==1){
				$this->db->where("ctt > ",$ctt);
			 }else{
				$this->db->where("ctt",0);
			 }
		  
		 } 
	   if(!empty($skrn)){ $this->db->where("konf_prov",$skrn); } 

		
	   
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("provinsi_madrasah","ASC");
					 $this->db->order_by("trkompetisi_id","asc");
					 $this->db->order_by("nla","DESC");
					 $this->db->order_by("jml_benar","DESC");
					 $this->db->order_by("slh","ASC");
					 $this->db->order_by("nilai_kabko","DESC");
					 $this->db->order_by("tgl_lahir","DESC");
					 
		
			 }
      
		
		
		return $this->db->get();
		
	}

	public function m_hasilprov_jml($paging){
           
		$kolom     = $_POST['kolom'];
        $jenjang     = $_POST['jenjang'];
		$kota     = $_POST['kota'];
		$provinsi     = $_POST['provinsi'];
        $trkompetisi_id   = $_POST['trkompetisi_id'];
        $status   = $this->input->post("status",true);
		$keyword   = $_POST['keyword'];
		$d_p       = $_POST['d_p'];
		$ctt       = $_POST['ctt'];
		$skrn       = $_POST['skrn'];
		
		$this->db->select("count(id) as jml");
        $this->db->from('v_siswa_provinsi');
        $this->db->where('nla is not null');
    
		
		if(!empty($jenjang)){
			$this->db->where("jenjang_madrasah",$jenjang);
		   }
			
	   if(!empty($kota)){ $this->db->where("kota_madrasah",$kota); } 
	   if(!empty($provinsi)){ $this->db->where("provinsi_madrasah",$provinsi); } 
   
	   
	   if(!empty($keyword)){ $this->db->where("UPPER({$kolom}) LIKE '%".strtoupper($keyword)."%'"); }
	   if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 

	   if(!empty($d_p)){ $this->db->where("d_p",$d_p); } 
	   if(!empty($ctt)){ $this->db->where("ctt",$ctt); } 
	   if(!empty($skrn)){ $this->db->where("skrn",$skrn); } 

		
	   
		
		$d=  $this->db->get()->row();
		return $d->jml;
		
	}
	
	
	public function m_pesertateshasil($paging){
           
        $jenjang   = $_POST['jenjang'];
       
        $trkompetisi_id   = $_POST['trkompetisi_id'];        
        $keyword   = $_POST['keyword'];
		
		$this->db->select("no_test,id,tgl_lahir,tmmadrasah_id,nama,gender,trkompetisi_id,kota,jenjang,nilai,benar,salah,kosong,d_p");
        $this->db->from('tm_siswa');
    
		$this->db->where("status",1);
		
       $this->db->where("jenjang",$jenjang);
       $this->db->where("no_test !=''");
			//$this->db->where("tmmadrasah_id in (select id from tm_madrasah where kota='".$_SESSION['tmkota_id']."') and jenjang ='".$jenjang."'");
	
		if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 
		if(!empty($keyword)){ $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'"); }
		
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					
					 $this->db->order_by("trkompetisi_id","ASC");
					 $this->db->order_by("nilai","DESC");
					 $this->db->order_by("benar","DESC");
					 $this->db->order_by("salah","ASC");
					 $this->db->order_by("tgl_lahir","desc");
		
			 }
      
		
		
		return $this->db->get();
		
	}


	public function m_pesertateshasilprov($paging){
           
        $jenjang   = $_POST['jenjang'];
       
        $trkompetisi_id   = $_POST['trkompetisi_id'];        
        $keyword   = $_POST['keyword'];
		
		$this->db->select("no_test2,id,tgl_lahir,tmmadrasah_id,nama,gender,trkompetisi_id,kota,jenjang,nilai,benar,salah,kosong,d_p");
        $this->db->from('tm_siswa');
    
		$this->db->where("status",1);
		
       $this->db->where("jenjang",$jenjang);
       $this->db->where("no_test2 !=''");
			//$this->db->where("tmmadrasah_id in (select id from tm_madrasah where kota='".$_SESSION['tmkota_id']."') and jenjang ='".$jenjang."'");
	
		if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 
		if(!empty($keyword)){ $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'"); }
		
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("trkompetisi_id","ASC");
					 $this->db->order_by("nilai","DESC");
					 $this->db->order_by("benar","DESC");
					 $this->db->order_by("salah","ASC");
					 $this->db->order_by("tgl_lahir","desc");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	public function m_pesertasertifikat($paging){
           
        $jenjang   = $_POST['jenjang'];
       
        $trkompetisi_id   = $_POST['trkompetisi_id'];        
        $keyword   = $_POST['keyword'];
		
		$this->db->select("status2,tgl_lahir,id,tmmadrasah_id,tgl_lahir,nama,gender,trkompetisi_id,kota,jenjang,nilai,benar,salah,kosong,d_p,no_test");
        $this->db->from('tm_siswa');
    
		$this->db->where("status",1);
	/*	$this->db->where("benar !=",0);
        $this->db->where("salah !=",0);
        $this->db->where("kosong !=",0);
		$this->db->where("nilai !=",0); */
		
		$this->db->where("(benar+salah+kosong) !=0");
	
		$this->db->where('provinsi',$_SESSION['tmprovinsi_id']);
        $this->db->where("jenjang",$jenjang);
			//$this->db->where("tmmadrasah_id in (select id from tm_madrasah where kota='".$_SESSION['tmkota_id']."') and jenjang ='".$jenjang."'");
	
		if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 
		if(!empty($keyword)){ $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'"); }
		
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("trkompetisi_id","ASC");
					 $this->db->order_by("nilai","DESC");
					 $this->db->order_by("benar","DESC");
					 $this->db->order_by("salah","ASC");
					 $this->db->order_by("tgl_lahir","desc");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	public function m_peserta_provinsi($paging){
           
        $jenjang   = $_POST['jenjang'];
       
        $trkompetisi_id   = $_POST['trkompetisi_id'];        
        $keyword   = $_POST['keyword'];
		
		$this->db->select("foto,statuskabko,ketkabko,tgl_lahir,id,tmmadrasah_id,tgl_lahir,nama,gender,trkompetisi_id,kota,jenjang,no_test2");
        $this->db->from('tm_siswa');
    
		$this->db->where("status2",1);
	
		$this->db->where('provinsi',$_SESSION['tmprovinsi_id']);
        if(!empty($jenjang)){ $this->db->where("jenjang",$jenjang); }
			
		if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 
		if(!empty($keyword)){ $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'"); }
		
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("jenjang","ASC");
					 $this->db->order_by("trkompetisi_id","ASC");
					 $this->db->order_by("nilai","DESC");
					 $this->db->order_by("benar","DESC");
					 $this->db->order_by("salah","ASC");
					 $this->db->order_by("tgl_lahir","desc");
		
			 }
      
		
		
		return $this->db->get();
		
	}

	public function m_komite($paging){
    
		
		$this->db->select("*");
        $this->db->from('tr_panitia');
		$this->db->where('provinsi',$_SESSION['tmprovinsi_id']);
		$this->db->where('jenis',"kankemenag");

	
		
	 
			
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					
					 $this->db->order_by("nama","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	public function g_pendamping($paging){
    
		$status = $_POST['status'];
		$this->db->select("*");
        $this->db->from('tr_panitia');
		
		$arjenis = array("1"=>"pusat","2"=>"kanwil","3"=>"kankemenag");
		$this->db->where('tingkat',1);

	
		
	 
			
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					
					 $this->db->order_by("provinsi","ASC");
					 $this->db->order_by("kota","ASC");
					 $this->db->order_by("nama","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	public function i_pendamping(){
		
		
		
		$f  = $this->input->get_post("f");
        
		
		
		
		$this->db->set('jenis',"pusat");
		
		$this->db->insert("tr_panitia",$f);
		
		
		        if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
				   
				} else {
						
					$this->db->trans_commit();
					
				}
		
	}
	
	public function u_pendamping($id){
		
		
		
		$f  = $this->input->get_post("f");

		$this->db->where('id',$id);
		
		
		$this->db->update("tr_panitia",$f);
		
		
		        if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
				   
				} else {
						
					$this->db->trans_commit();
					
				}
		
	}


	public function m_hasilNasional($paging){
           
		$kolom     = $_POST['kolom'];
        $jenjang     = $_POST['jenjang'];
		$kota     = $_POST['kota'];
		$provinsi     = $_POST['provinsi'];
        $trkompetisi_id   = $_POST['trkompetisi_id'];
        $status   = $this->input->post("status",true);
		$keyword   = $_POST['keyword'];
		
		
		$this->db->select("*");
        $this->db->from('v_siswa_nasional');
        $this->db->where('nasional_peringkat IS NOT NULL');
       // $this->db->where('nasional_juara !=""');
    
		
       $this->db->where("jenjang_madrasah",$jenjang);
			
	   if(!empty($kota)){ $this->db->where("kota_madrasah",$kota); } 
	   if(!empty($provinsi)){ $this->db->where("provinsi_madrasah",$provinsi); } 
   
	   
	   if(!empty($keyword)){ $this->db->where("UPPER({$kolom}) LIKE '%".strtoupper($keyword)."%'"); }
	   if(!empty($trkompetisi_id)){ $this->db->where("trkompetisi_id",$trkompetisi_id); } 

	  

		
	   
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("trkompetisi_id","asc");
					 $this->db->order_by("nasional_peringkat","ASC");
				
					 
		
			 }
      
		
		
		return $this->db->get();
		
	}
	

	public function m_madrasahNas($paging){
           
		
		//$kota     = $_POST['kota'];
		$provinsi     = $_POST['provinsi'];       
      
        $keyword   = $this->input->post("keyword",true);
		
		
		$this->db->select("*");
        $this->db->from('tm_madrasah');
        $this->db->where('id IN(select tmmadrasah_id from tm_siswa where status3=1) ');
        //$this->db->where('jenjang',1);
        $this->db->where('jenis',2);
       
    
			
	   if(!empty($kota)){ $this->db->where("kota",$kota); } 
	   if(!empty($provinsi)){ $this->db->where("provinsi",$provinsi); } 
   
	   
	   if(!empty($keyword)){ $this->db->where("(UPPER(username) LIKE '%".strtoupper($keyword)."%' or UPPER(nama) LIKE '%".strtoupper($keyword)."%')"); }
	     
		
	   
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("juara1_nas","DESC");					
					 $this->db->order_by("juara2_nas","DESC");					
					 $this->db->order_by("juara3_nas","DESC");					
					 $this->db->order_by("harapan1_nas","DESC");					
					 $this->db->order_by("harapan2_nas","DESC");					
					 $this->db->order_by("harapan3_nas","DESC");					
					 $this->db->order_by("jenis","ASC");					
					
					
					 
		
			 }
      
		
		
		return $this->db->get();
		
	}
	

	public function g_provinsi($paging){
    
		
		$this->db->select("*");
        $this->db->from('provinsi');
		
		
	
		
	 
			
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					
					 $this->db->order_by("nama","ASC");
					 
		
			 }
      
		
		
		return $this->db->get();
		
	}

	public function g_kabko($paging){
    
		
		$this->db->select("*");
        $this->db->from('kota');
		//$this->db->where('id IN(select kota_madrasah from v_siswa where status=2 and lokasi_kabko=0)');
		
	
		
	 
			
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					
					 $this->db->order_by("provinsi_id","ASC");
					 $this->db->order_by("nama","ASC");
					 
		
			 }
      
		
		
		return $this->db->get();
		
	}


	public function g_lokasi($paging){
	
		$keyword = $_POST['keyword'];
		$kota     = $_POST['kota'];
		$provinsi     = $_POST['provinsi'];
 
			
	 
   


		$this->db->select("*");
		$this->db->from('tr_lokasites');
		$this->db->where('tingkat',1);
	
		if(!empty($kota)){ $this->db->where("kota",$kota); } 
		if(!empty($provinsi)){ $this->db->where("provinsi",$provinsi); } 
		if(!empty($keyword)){ $this->db->where("UPPER(nama) LIKE '%".$keyword."%'"); } 
		
		
		if($paging==true){
					$this->db->limit($_REQUEST['length'],$_REQUEST['start']);					
					$this->db->order_by("provinsi","ASC");
					$this->db->order_by("kota","ASC");
		
			}

		
		
		return $this->db->get();
		
		}


		public function g_pesertakabko($paging){
	
		
				
		 
	   
	
	
			$this->db->select("id,no_test,nama,tgl_lahir,trkompetisi_id,id_sesi,hari,sesi,id_ruang,ruang,foto_dir,foto_drive,kartupelajar,provinsi,kota");
			$this->db->from('pesertakabko');
		
			
			
			if($paging==true){
						$this->db->limit($_REQUEST['length'],$_REQUEST['start']);					
						
						$this->db->order_by("kota","ASC");
						$this->db->order_by("id_ruang","ASC");
						$this->db->order_by("nama","ASC");
						$this->db->order_by("nama","ASC");
			
				}
	
			
			
			return $this->db->get();
			
			}
	
}
