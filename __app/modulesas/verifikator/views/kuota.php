<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Post-->
<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
	<div id="kt_content_container" class="container-xxl">
	
					

<div class="col-lg-12 col-xxl-12">

  <div class="row">
                
<div class="col-md-12">
  <div class="card card-custom gutter-b card-stretch">
											<!--begin::Header-->
											<div class="card-header border-0">
                        <h3 class="card-title font-weight-bolder text-dark"> Kuota BKBA Perprovinsi </h3>
                        
     
                        </div>
                            <div class="card-body">
                              <div class="table-responsive">
                              
                                  <table class="table table-bordered  " id="datatableTable" >
                                      <thead>
                                        <tr>
                                            <th rowspan="2"> NO </th>
                                            <th rowspan="2"> PROVINSI </th>
                                            <th rowspan="2"> MADRASAH  </th>
                                            <th colspan="2"> KUOTA  </th>
                                           
                                            <th colspan="4"> LONGLIST </th>
                                            <th colspan="4"> SHORTLIST  KINERJA </th>
                                            <th colspan="4"> SHORTLIST  AFIRMASI  </th>
                                      </tr>

                                      <tr>
                                            <th> KINERJA </th>
                                            <th> AFIRMASI  </th>


                                            <th> MI </th>
                                            <th> MTs </th>
                                            <th> MA  </th>
                                            <th> JML  </th>

                                            <th> MI </th>
                                            <th> MTs </th>
                                            <th> MA  </th>
                                            <th> JML  </th>

                                            <th> MI </th>
                                            <th> MTs </th>
                                            <th> MA  </th>
                                            <th> JML  </th>
                                         
                                            
                                      </tr>

                                      </thead>

                                      <?php 
                                      $jenjang = $this->db->get_where("provinsi",array("active"=>1))->result();
                                    
                                      $jml =0;
                                      $kuotak =0;
                                      $kuotaf =0;
                                      $jmllonglistmi =0;
                                      $jmllonglistmts =0;
                                      $jmllonglistma =0;
                                      $jmllonglist =0;


                                      $jmlshortlistkinerjami =0;
                                      $jmlshortlistkinerjamts =0;
                                      $jmlshortlistkinerjama =0;
                                      $jmlshortlistkinerja =0;


                                      $jmlshortlistafirmasimi   =0;
                                      $jmlshortlistafirmasimts  =0;
                                      $jmlshortlistafirmasima   =0;
                                      $jmlshortlistafirmasi     =0;
                                      $tahun   = $this->Reff->tahun();
                                      foreach($jenjang as $r){
                                        $jmldata      = $this->db->query("select count(id) as jml from madrasahedm where  provinsi_id='".$r->kode."' and tahun='{$tahun}'")->row();
                                        $longlistmi   = $this->db->query("select count(id) as jml from madrasahedm where jenjang='mi' and  longlist=1 AND provinsi_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                        $longlistmts  = $this->db->query("select count(id) as jml from madrasahedm where  jenjang='mts' and longlist=1 AND provinsi_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                        $longlistma   = $this->db->query("select count(id) as jml from madrasahedm where  jenjang='ma' and longlist=1 AND provinsi_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                        $longlist     = $this->db->query("select count(id) as jml from madrasahedm where  longlist=1 AND provinsi_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                        
                                        
                                        $shortlistkinerjami   = $this->db->query("select count(id) as jml from madrasahedm where jenjang='mi' and  bantuan=1 and  shortlist=1  AND provinsi_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                        $shortlistkinerjamts  = $this->db->query("select count(id) as jml from madrasahedm where  jenjang='mts' and bantuan=1 and  shortlist=1 AND provinsi_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                        $shortlistkinerjama   = $this->db->query("select count(id) as jml from madrasahedm where  jenjang='ma' and bantuan=1 and  shortlist=1  AND provinsi_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                        $shortlistkinerja     = $this->db->query("select count(id) as jml from madrasahedm where bantuan=1 and  shortlist=1 AND provinsi_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                     

                                        $shortlistmi   = $this->db->query("select count(id) as jml from madrasahedm where jenjang='mi' and  bantuan=2 and  shortlist=1  AND provinsi_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                        $shortlistmts  = $this->db->query("select count(id) as jml from madrasahedm where  jenjang='mts' and bantuan=2 and  shortlist=1 AND provinsi_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                        $shortlistma   = $this->db->query("select count(id) as jml from madrasahedm where  jenjang='ma' and bantuan=2 and  shortlist=1  AND provinsi_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                 
                                        $shortlist  = $this->db->query("select count(id) as jml from madrasahedm where bantuan=2 and  shortlist=1 AND provinsi_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                          
                                        // $jml = $jml + $jmldata->jml;
                                        // $kuotak = $kuotak + $r->kuota_kinerja;
                                        // $kuotaf = $kuotaf + $r->kuota_afirmasi;


                                        // $jmllonglistmi = $jmllonglistmi + $longlistmi->jml;
                                        // $jmllonglistmts = $jmllonglistmts + $longlistmts->jml;
                                        // $jmllonglistma = $jmllonglistma + $longlistma->jml;
                                        // $jmllonglist = $jmllonglist + $longlist->jml;

                                        // $jmlshortlistkinerjami = $jmlshortlistkinerjami + $shortlistkinerja->jml;
                                        // $jmlshortlistkinerjamts = $jmlshortlistkinerjamts + $shortlistkinerja->jml;
                                        // $jmlshortlistkinerjama  = $jmlshortlistkinerjama + $shortlistkinerja->jml;
                                        // $jmlshortlistkinerja = $jmlshortlistkinerja + $shortlistkinerja->jml;



                                        // $jmlshortlistafirmasimi   = $jmlshortlistafirmasimi + $shortlistmi->jml;
                                        // $jmlshortlistafirmasimts  = $jmlshortlistafirmasimts + $shortlistmts->jml;
                                        // $jmlshortlistafirmasima   = $jmlshortlistafirmasima + $shortlistma->jml;
                                        // $jmlshortlistafirmasi     = $jmlshortlistafirmasi + $shortlist->jml;


                                       
                                        $style="";
                                        if($shortlist->jml < $r->kuota_afirmasi){
 
                                         $style="background-color:yellow";
                                        }
                                        
                                        ?>
                                      <tr style="<?php echo $style; ?>"">
                                        <td> # </td>
                                        <td> <a href="#" class="detailnya" data-id="<?php echo $r->id; ?>"> <b style="color:black"> <?php echo ($r->nama); ?> </b> </a></td>
                                        <td>  <?php echo $this->Reff->formatuang2($jmldata->jml); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($r->kuota_kinerja); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($r->kuota_afirmasi); ?> </td>
                                        
                                        <td>  <?php echo  $this->Reff->formatuang2($longlistmi->jml); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($longlistmts->jml); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($longlistma->jml); ?> </td>
                                        <td style="background:green;color:white">  <?php echo  $this->Reff->formatuang2($longlist->jml); ?> </td>


                                        <td>  <?php echo  $this->Reff->formatuang2($shortlistkinerjami->jml); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($shortlistkinerjamts->jml); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($shortlistkinerjama->jml); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($shortlistkinerja->jml); ?> </td>


                                        <td>  <?php echo  $this->Reff->formatuang2($shortlistmi->jml); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($shortlistmts->jml); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($shortlistma->jml); ?> </td>
                                        <td style="background:green;color:white">  <?php echo  $this->Reff->formatuang2($shortlist->jml); ?> </td>
                                        
                                      </tr>

                                       <?php 
                                       $kota = $this->db->query("select * from kota where provinsi_id='{$r->id}' and kuota !=0")->result();
                                       $no=1;
                                       foreach($kota as $r){
                                        $jmldata      = $this->db->query("select count(id) as jml from madrasahedm where  kota_id='".$r->kode."' and tahun='{$tahun}'")->row();
                                        $longlistmi   = $this->db->query("select count(id) as jml from madrasahedm where jenjang='mi' and  longlist=1 AND kota_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                        $longlistmts  = $this->db->query("select count(id) as jml from madrasahedm where  jenjang='mts' and longlist=1 AND kota_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                        $longlistma   = $this->db->query("select count(id) as jml from madrasahedm where  jenjang='ma' and longlist=1 AND kota_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                        $longlist     = $this->db->query("select count(id) as jml from madrasahedm where  longlist=1 AND kota_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                        
                                        
                                        $shortlistkinerjami   = $this->db->query("select count(id) as jml from madrasahedm where jenjang='mi' and  bantuan=1 and  shortlist=1  AND kota_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                        $shortlistkinerjamts  = $this->db->query("select count(id) as jml from madrasahedm where  jenjang='mts' and bantuan=1 and  shortlist=1 AND kota_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                        $shortlistkinerjama   = $this->db->query("select count(id) as jml from madrasahedm where  jenjang='ma' and bantuan=1 and  shortlist=1  AND kota_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                        $shortlistkinerja     = $this->db->query("select count(id) as jml from madrasahedm where bantuan=1 and  shortlist=1 AND kota_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                     

                                        $shortlistmi   = $this->db->query("select count(id) as jml from madrasahedm where jenjang='mi' and  bantuan=2 and  shortlist=1  AND kota_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                        $shortlistmts  = $this->db->query("select count(id) as jml from madrasahedm where  jenjang='mts' and bantuan=2 and  shortlist=1 AND kota_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                        $shortlistma   = $this->db->query("select count(id) as jml from madrasahedm where  jenjang='ma' and bantuan=2 and  shortlist=1  AND kota_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                 
                                        $shortlist  = $this->db->query("select count(id) as jml from madrasahedm where bantuan=2 and  shortlist=1 AND kota_id='".$r->kode."'  and tahun='{$tahun}'")->row();
                                          
                                        $jml = $jml + $jmldata->jml;
                                       // $kuotak = $kuotak + $r->kuota_kinerja;
                                        $kuotak = 0;
                                        $kuotaf = $kuotaf + $r->kuota;


                                        $jmllonglistmi = $jmllonglistmi + $longlistmi->jml;
                                        $jmllonglistmts = $jmllonglistmts + $longlistmts->jml;
                                        $jmllonglistma = $jmllonglistma + $longlistma->jml;
                                        $jmllonglist = $jmllonglist + $longlist->jml;

                                        $jmlshortlistkinerjami = $jmlshortlistkinerjami + $shortlistkinerja->jml;
                                        $jmlshortlistkinerjamts = $jmlshortlistkinerjamts + $shortlistkinerja->jml;
                                        $jmlshortlistkinerjama  = $jmlshortlistkinerjama + $shortlistkinerja->jml;
                                        $jmlshortlistkinerja = $jmlshortlistkinerja + $shortlistkinerja->jml;



                                        $jmlshortlistafirmasimi   = $jmlshortlistafirmasimi + $shortlistmi->jml;
                                        $jmlshortlistafirmasimts  = $jmlshortlistafirmasimts + $shortlistmts->jml;
                                        $jmlshortlistafirmasima   = $jmlshortlistafirmasima + $shortlistma->jml;
                                        $jmlshortlistafirmasi     = $jmlshortlistafirmasi + $shortlist->jml;
                                        $style="";
                                       if($shortlist->jml < $r->kuota){

                                        $style="background-color:red";
                                       }
                                       

                                        
                                        ?>
                                      <tr style="<?php echo $style; ?>;display:none" class="kabkotanya<?php echo $r->provinsi_id; ?>">
                                        <td> <?php echo $no++; ?> </td>
                                        <td> <?php echo ($r->kode); ?> - <?php echo ($r->nama); ?> </td>
                                        <td>  <?php echo $this->Reff->formatuang2($jmldata->jml); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2(0); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($r->kuota); ?> </td>
                                        
                                        <td>  <?php echo  $this->Reff->formatuang2($longlistmi->jml); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($longlistmts->jml); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($longlistma->jml); ?> </td>
                                        <td style="background:green;color:white">  <?php echo  $this->Reff->formatuang2($longlist->jml); ?> </td>


                                        <td>  <?php echo  $this->Reff->formatuang2($shortlistkinerjami->jml); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($shortlistkinerjamts->jml); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($shortlistkinerjama->jml); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($shortlistkinerja->jml); ?> </td>


                                        <td>  <?php echo  $this->Reff->formatuang2($shortlistmi->jml); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($shortlistmts->jml); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($shortlistma->jml); ?> </td>
                                        <td style="background:green;color:white">  <?php echo  $this->Reff->formatuang2($shortlist->jml); ?> </td>
                                        
                                      </tr>

                                      <?php 
                                       }
                                       ?>






                                      <?php 


                                      }
                                    ?>

                                     <tr>
                                        <td colspan="2"> Total  </td>
                                        
                                      
                                        <td>  <?php echo  $this->Reff->formatuang2($jml); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($kuotak); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($kuotaf); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($jmllonglistmi); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($jmllonglistmts); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($jmllonglistma); ?> </td>
                                        <td style="background:green;color:white">  <?php echo  $this->Reff->formatuang2($jmllonglist); ?> </td>
                                        
                                        <td>  <?php echo  $this->Reff->formatuang2($jmlshortlistkinerjami); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($jmlshortlistkinerjamts); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($jmlshortlistkinerjama); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($jmlshortlistkinerja); ?> </td>


                                        <td>  <?php echo  $this->Reff->formatuang2($jmlshortlistafirmasimi); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($jmlshortlistafirmasimts); ?> </td>
                                        <td>  <?php echo  $this->Reff->formatuang2($jmlshortlistafirmasima); ?> </td>
                                        <td style="background:green;color:white">  <?php echo  $this->Reff->formatuang2($jmlshortlistafirmasi); ?> </td>
                                        
                                      </tr>

                                  </table>
                          
                          
                          </div>
                          </div>
</div>
</div>





</div>

              
</div>
 </div>


</div>
</div>


<script type="text/javascript">
  
   $(document).off("click",".detailnya").on("click",".detailnya",function(){

    var id = $(this).data("id");

    $(".kabkotanya"+id).toggle();

   })

</script>
