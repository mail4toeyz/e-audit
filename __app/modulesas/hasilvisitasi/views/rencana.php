<div class="table-responsive">
                                          <table class="table table-hover table-bordered table-striped">
                                              <thead>
                                              <tr class="fw-bolder  bg-light" style="font-weight:bold">
                                                   <th>NO</th>
                                                   <th>TAHUN</th>
                                                   <th>JENIS</th>
                                                   <th>KEGIATAN</th>
                                                   <th>VOLUME</th>
                                                   <th>SATUAN</th>
                                                   <th>HARGA SATUAN</th>
                                                   <th>JUMLAH BIAYA</th>
                                                  
                                                </tr>
                                              </thead>

                                              <thead>
                                               
                                              </thead>
                                              <tbody id="loadbodyrencana">

                                                 <?php 
                                                   $kegiatan = $this->db->query("select * from visitasi_kegiatan where madrasah_id='".$data->id."' order by tahun ASC")->result();
                                                   $no=1;   
                                                   $total = 0;
                                                   foreach($kegiatan as $rk){
                                                      $total = $total + $rk->biaya;

                                                         ?>

                                                                <tr>
                                                                  <td><?php echo $no++; ?></td>
                                                                  <td><?php echo $rk->tahun; ?></td>
                                                                  <td><?php echo $this->Reff->get_kondisi(array("id"=>$rk->kegiatan_id),"visitasi_tatapmuka","jenis"); ?></td>
                                                                  <td><?php echo $rk->kegiatan; ?></td>
                                                                  <td><?php echo $rk->volume; ?></td>
                                                                  <td><?php echo $rk->satuan; ?></td>
                                                                  <td><?php echo $this->Reff->formatuang2($rk->harga); ?></td>
                                                                  <td><?php echo $this->Reff->formatuang2($rk->biaya); ?></td>
                                                                  
                                                                 </tr>




                                                         <?php



                                                      }
                                                   ?>

                                                   <tr style="font-weight:bold">
                                                      <td colspan="7"> Total Kegiatan </td>
                                                      <td> <?php echo $this->Reff->formatuang2($total); ?> </td>
                                                      <td> - </td>
                                                   </tr>


                                                   <tr style="font-weight:bold">
                                                      <td colspan="7"> Rencana Tahun 2021 </td>
                                                      <td> 
                                                        
                                                      <?php 
                                                      $saldo = $this->db->query("select sum(biaya) as total from visitasi_kegiatan where tahun='2021' and madrasah_id='".$data->id."'")->row();
                                                      echo $this->Reff->formatuang2($saldo->total); ?>
                                                    
                                                    </td>
                                                    
                                                   </tr>

                                                   <tr style="font-weight:bold">
                                                      <td colspan="7"> Rencana Tahun 2022 </td>
                                                      <td> <?php 
                                                      $saldo = $this->db->query("select sum(biaya) as total from visitasi_kegiatan where tahun='2022' and madrasah_id='".$data->id."'")->row();
                                                      echo $this->Reff->formatuang2($saldo->total); ?> </td>
                                                     
                                                   </tr>


                                             </tbody>



                                        </table>
                                    </div>