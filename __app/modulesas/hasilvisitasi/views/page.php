
		<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Post-->
<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
	<div id="kt_content_container" class="container-xxl">
				       <div class="card">
						
					  
						 <div class="card-body">
						
						 <div class="row">
													
													
						                       

													<div class="col-md-2">
													  <select class="form-control" id="statusVisit">
														   <option value="">- Status Visitasi    - </option>
														   <option value="1" > On Progress </option>
														   <option value="3" selected> Selesai </option>
														   <option value="2"> Belum </option>
														  
														  
													   
													   </select>
												   
													</div>

													
													<div class="col-md-2">
												
													  <select class="form-control onchange " id="provinsi" target="kota" urlnya="<?php echo site_url('publik/kota'); ?>">
														   <option value="">- Provinsi - </option>
														   <?php 
															   $provinsi = $this->db->query("SELECT * from provinsi where (kuota_kinerja+kuota_afirmasi) !=0")->result();
															   foreach($provinsi as $row){
																   ?><option value="<?php echo strtoupper($row->id); ?>"> <?php echo ($row->nama); ?> </option><?php 
																   
															   }
															  ?>
													   
													   </select>
												   
													</div>

													<div class="col-md-2">
												
													  <select class="form-control kota " id="kota" target="kota" urlnya="<?php echo site_url('publik/kota'); ?>">
														   <option value="">- Kabupaten/Kota - </option>
														  
													   
													   </select>
												   
													</div>
													
													
													<div class="col-md-3">
													  <input class="form-control" id="keyword"  placeholder="Apa yang Anda cari ?">
													  <input class="form-control" type="hidden" id="bantuan" value="<?php echo isset($_GET['bantuan']) ? $_GET['bantuan']:''; ?>">
													
												   
													</div>
													<div class="col-md-1">
													  <button class="btn btn-sm btn-primary" id="tampilkan" > <i class="fa fa-search"></i> </button> 
												   
													</div>
											   
											</div>
											<br>

							<div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
									<tr class="fw-bolder  bg-light" style="font-weight:bold">
                                            <th width="2px" rowspan="2">NO</th>                                           
                                           
											<th rowspan="2">VISITASI </th>                                            
                                            <th width="2px" rowspan="2">BANTUAN</th>                                            
                                            <th rowspan="2">NSM </th>
                                            <th rowspan="2">NAMA </th>
                                            <th rowspan="2">PROVINSI </th>
                                            <th rowspan="2">KABKOTA </th>                                                                                   
                                        
                                            <th colspan="9">HASIL VISITASI </th>
                                            <th colspan="4">AMAN </th>
											
											
                                            
                                        </tr>

										<tr class="fw-bolder  bg-light" style="font-weight:bold">

										
										 

										  <th> EDM </th>
										  <th> SISWA  </th>
									      <th> ROMBEL </th>
										  <th> GURU </th>
										  <th> RUANGBELAJAR </th>
										  <th> TOILET </th>
										 
										  <th> DOKUMENTASI  </th>
										 
										  <th> NIK  </th>
										  <th> NAMA  </th>
										  <th> MULAI  </th>
										  <th> SELESAI  </th>
										  

										 

										</tr>

                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                         </div>
                         </div>
                         </div>
                         </div>
                         </div>
                 
 
 
              
        
<div id="defaultModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body" id="loadform">
        <p>Loading...</p>
      </div>
      
    </div>

  </div>
</div>
	
<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"scrollY": 500,
                        "scrollX": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[ 50,100,200,300,500,1000, 800000000], [ 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					
                     buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
					
					"ajax":{
						url :"<?php echo site_url("hasilvisitasi/grid"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						data.status = $("#status").val();
						data.skor_visitasi = $("#skor_visitasi").val();
						data.provinsi = $("#provinsi").val();
						data.kota = $("#kota").val();
						data.keyword = $("#keyword").val();
						data.bantuan = $("#bantuan").val();
						data.statusVisit = $("#statusVisit").val();
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				

				$(document).on("change","#status,#skor_visitasi,#provinsi,#kota,#statusVisit",function(){


					dataTable.ajax.reload(null,false);	
				});

				$(document).on("input","#keyword",function(){


				   dataTable.ajax.reload(null,false);	
				});
				
				
	


</script>
				