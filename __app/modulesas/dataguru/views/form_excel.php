<style>

.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
	background:red;
	color:white;
}
</style>

</head>
						
						 <form action="<?php echo site_url("dataguru/import_excel"); ?>" method="post" enctype="multipart/form-data">
						
                                
							
								

								<div class="row clearfix">
                                 <div class="col-sm-12">
								 
								   <div class="alert alert-danger">
								  <center> Keterangan : <br>
									 File excel yang dapat diupload hanya file excel yang sudah kami sediakan templatenya,
									 silahkan dowload template file Excel , jangan merubah urutan kolom dan isi dengan data yang valid 
									 <br>
									 <a href="<?php echo base_url(); ?>__statics/excel/template_siswa.xlsx"  class="btn btn-sm btn-success">
									 Download Template Excel 
									 </a> 
								   </center>
								    </div>
									
							    
								
								 
                                    <div class="form-group">
									<center>
									<label for="file-upload" class="custom-file-upload">
											<i class="fa fa-cloud-upload"></i>  File Excel
										</label>
										<input id="file-upload" type="file" required name="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" >

																			
									</center>
                                    </div>
                                    
                                 </div>
                                </div>
								
								 <div class=" " id="aksimodal">
						       <center>
								   <button type="submit" class="btn btn-success btn-sm ">
										<i class="fa fa-save"></i>
										<span>Import Excel </span>
                                    </button>
								    
									<button type="button" class="btn btn-success btn-sm " data-dismiss="modal">
										
										<span>Close   </span>
                                    </button>
									
								   </center>
								
								 </div>
								 
								  <div class="row clearfix" id="loadingmodal" style="display:none">
								  <center><div class="preloader pl-size-md"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div> <br> Data sedang disimpan, Mohon Tunggu ...</center>
								 
								 </div>
							  
					</form>	    
