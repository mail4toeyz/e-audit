<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pka extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		if(!$this->session->userdata("idAuditor")){
			    
			echo $this->Reff->sessionhabis();
			exit();
		
	  }
		  $this->load->model('M_ruang','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('auditor/page_header',$data);	
	}
		
	public function data()
	{  
	   
	    		
         $ajax            = $this->input->get_post("ajax",true);	
         $kode            = $this->input->get_post("kode",true);	
		 $data['satker']  = $this->db->get_where("satker",array("kode"=>$kode))->row();
		 $data['kegiatan']  = $this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
		 $data['title']   = "Program Kerja Audit | ".$data['satker']->nama;
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}

	public function save(){

		$satker_id   = $this->input->get_post("satker_id");
		$kegiatan_id = $_SESSION['kegiatan_id'];
		$penyusun    = $_SESSION['idAuditor'];

		$this->db->where("satker_id",$satker_id);
		$this->db->where("kegiatan_id",$kegiatan_id);
		$this->db->where("penyusun",$penyusun);
		
		$this->db->delete("tr_pka");
		
		$kategoriPKA = $this->db->get("tm_kategori_pka")->result();
		foreach($kategoriPKA as $rkat){
			$uraian = $this->db->query("SELECT * from tm_uraian_pka where kategori_id='{$rkat->id}' order by urutan ASC")->result();
			$no=1;

			$kategori_pka    = $rkat->id;
			$parameter       = $this->input->get_post("parameter".$kategori_pka);
			$tao	         = $this->input->get_post("tao".$kategori_pka);
			foreach($uraian as $urow){

				 $uraian_id			    = $urow->id;
				 $uraian 				=$this->input->get_post("uraian".$kategori_pka.$uraian_id);
				 $dikerjakanRencana 	=$this->input->get_post("dikerjakanRencana".$kategori_pka.$uraian_id);
				 $dikerjakanRealisasi 	=$this->input->get_post("dikerjakanRealisasi".$kategori_pka.$uraian_id);
				 $waktuRencana       	=$this->input->get_post("waktuRencana".$kategori_pka.$uraian_id);
				 $waktuRealisasi       	=$this->input->get_post("waktuRealisasi".$kategori_pka.$uraian_id);
				 $no_kk			       	=$this->input->get_post("no_kk".$kategori_pka.$uraian_id);
				 $catatan		       	=$this->input->get_post("catatan".$kategori_pka.$uraian_id);


				 $this->db->set("satker_id",$satker_id);
				 $this->db->set("kegiatan_id",$kegiatan_id);
				 $this->db->set("penyusun",$penyusun);
				 $this->db->set("kategori_pka",$kategori_pka);
				 $this->db->set("parameter",$parameter);
				 $this->db->set("tao",$tao);
				 $this->db->set("uraian_id",$uraian_id);
				 $this->db->set("uraian",$uraian);
				 $this->db->set("dikerjakanRencana",$dikerjakanRencana);
				 $this->db->set("dikerjakanRealisasi",$dikerjakanRealisasi);
				 $this->db->set("waktuRencana",$waktuRencana);
				 $this->db->set("waktuRealisasi",$waktuRealisasi);
				 $this->db->set("no_kk",$no_kk);
				 $this->db->set("catatan",$catatan);
				 $this->db->insert("tr_pka");

				    

			}


		}

	}
	
	
	 
}
