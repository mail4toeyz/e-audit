<aside id="sidebar" class="sidebar">

  <ul class="sidebar-nav" id="sidebar-nav">

    <li class="nav-item">
      <a class="nav-link " href="<?php echo site_url('admin'); ?>">
        <i class="bi bi-grid"></i>
        <span>Dashboard</span>
      </a>
    </li><!-- End Dashboard Nav -->


   
        <li>

          <a class="nav-link collapsed" data-bs-target="#master-navTes" data-bs-toggle="collapse" href="#">
            <i class="bi bi-circle"></i><span>Data Pegawai </span> <i class="bi bi-chevron-down ms-auto" style="font-size:12px"></i>
          </a>

          <ul id="master-navTes" class="nav-content collapse " data-bs-parent="#sidebar-navTes">



            <?php
            $unit_kerja = $this->db->get("unit_kerja")->result();
            foreach ($unit_kerja as $row) {
            ?>
              <li style="padding-left:10px">
                <a href="<?php echo site_url("unitkerja/pegawai/" . base64_encode($row->kode) . ""); ?>" title="<?php echo $row->nama; ?> " class="menuajax">
                  <i class="bi bi-arrow-right"></i><span> <?php echo $row->nama; ?> </span>
                </a>
              </li>
            <?php

            }
            ?>
          </ul>


        </li>

        <li>

          <a class="nav-link collapsed" data-bs-target="#master-navTesSatuanKerja" data-bs-toggle="collapse" href="#">
            <i class="bi bi-circle"></i><span>Satuan Kerja </span><i class="bi bi-chevron-down ms-auto" style="font-size:12px"></i>
          </a>

          <ul id="master-navTesSatuanKerja" class="nav-content collapse " data-bs-parent="#sidebar-navTesSatuanKerja">



            <?php
            $satker_kategori = $this->db->get("satker_kategori")->result();
            foreach ($satker_kategori as $row) {
            ?>
              <li>
                <a href="<?php echo site_url("satuankerja/data/" . base64_encode($row->id) . ""); ?>" title="<?php echo $row->nama; ?> " class="menuajax">
                  <i class="bi bi-arrow-right"></i><span> <?php echo $row->nama; ?> </span>
                </a>
              </li>
            <?php

            }
            ?>
          </ul>


        </li>

        <li>

        <a class="nav-link collapsed" data-bs-target="#master-SBM" data-bs-toggle="collapse" href="#">
          <i class="bi bi-circle"></i><span>Master SBM</span><i class="bi bi-chevron-down ms-auto" style="font-size:12px"></i>
        </a>

        <ul id="master-SBM" class="nav-content collapse " data-bs-parent="#sidebar-SBM">



            <li>
                <a href="<?php echo site_url("sbmtransport/data"); ?>" title="SBM Transport" class="menuajax">
                <i class="bi bi-arrow-arrow"></i><span>SBM Transport </span>
              </a>
                
            </li>
            <li>
                <a href="<?php echo site_url("sbmhotel/data"); ?>" title="SBM Transport" class="menuajax">
                <i class="bi bi-arrow-arrow"></i><span>SBM Hotel </span>
              </a>
                
            </li>
            <li>
                <a href="<?php echo site_url("sbmuh/data"); ?>" title="SBM Transport" class="menuajax">
                <i class="bi bi-arrow-arrow"></i><span>SBM Uang Harian </span>
              </a>
                
            </li>
         
        </ul>


        </li>

     <li class="nav-item">
      <a class="nav-link collapsed " href="<?php echo site_url('masterpka/data'); ?>">
        <i class="bi bi-circle"></i>
        <span>Master PKA</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link collapsed" href="<?php echo site_url('masterkka/data'); ?>">
        <i class="bi bi-circle"></i>
        <span>Master KKA </span>
      </a>
    </li>

     <li class="nav-item">
      <a class="nav-link collapsed " href="<?php echo site_url('admin/kegiatan'); ?>">
        <i class="bi bi-calendar"></i>
        <span>Kegiatan</span>
      </a>
    </li>



    <li class="nav-item">
      <a class="nav-link collapsed" data-bs-target="#users-nav" data-bs-toggle="collapse" href="#">
        <i class="bi bi-file-person-fill"></i><span>Pengguna Aplikasi </span> <i class="bi bi-chevron-down ms-auto"></i>
      </a>
      <ul id="users-nav" class="nav-content collapse show" data-bs-parent="#sidebar-nav">
        <?php
        $groups = $this->db->query("SELECT * from `groups` order by urutan ASC")->result();
        foreach ($groups as $row) {
        ?>
          <li>
            <a href="<?php echo site_url("users/data/" . base64_encode($row->id) . ""); ?>" title="<?php echo $row->nama; ?> " class="menuajax">
              <i class="bi bi-circle"></i><span> <?php echo $row->nama; ?> </span>
            </a>
          </li>
        <?php

        }
        ?>





      </ul>
    </li><!-- End Components Nav -->

   

    <li class="nav-item">
      <a class="nav-link collapsed" id="keluar">
        <i class="bi bi-file-earmark"></i>
        <span>Logout</span>
      </a>
    </li><!-- End Blank Page Nav -->
  </ul>

</aside><!-- End Sidebar-->