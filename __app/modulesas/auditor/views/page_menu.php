<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

      <li class="nav-item">
        <a class="nav-link " href="<?php echo site_url('auditor'); ?>">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->
     <?php
     $kegiatan		   = $this->db->get_where("kegiatan",array("id"=>$_SESSION['kegiatan_id']))->row();
     if(!is_null($kegiatan)){
      ?>
      <li class="nav-heading"> Additional </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo site_url("dokumen"); ?>">
          <i class="bi bi-plus-circle"></i>
          <span> Permintaan Dokumen </span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo site_url("vicon"); ?>">
        <i class="bi bi-menu-button-wide"></i>
          <span> Video Conference</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo site_url("dataauditi"); ?>">
          <i class="bi bi-building"></i>
          <span> Data Auditi </span>
        </a>
      </li>

      <?php 
       if($_SESSION['group_id']=="ketua"){

        ?>
         <li class="nav-heading"> Proses Audit </li>
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#forms-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-journal-text"></i><span>Program Kerja Audit </span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="forms-nav" class="nav-content collapse show " data-bs-parent="#sidebar-nav">
          <?php 
          $satker = $this->db->query("SELECT id,kode,nama from satker where id IN(".$kegiatan->satker_id.")")->result();
          foreach($satker as $rsat){
          ?>
          <li>
            <a href="<?php echo site_url("pka/data?kode=".$rsat->kode); ?>">
              <i class="bi bi-circle"></i><span><?php echo $rsat->nama; ?></span>
            </a>
          </li>
          <?php 
          }
          ?>
        
        </ul>
      </li>

      <li class="nav-item">
       <a class="nav-link collapsed" data-bs-target="#forms-nav" data-bs-toggle="collapse" href="#">
         <i class="bi bi-journal-text"></i><span>Hasil Audit </span><i class="bi bi-chevron-down ms-auto"></i>
       </a>
       <ul id="forms-nav" class="nav-content collapse  " data-bs-parent="#sidebar-nav">
         <?php 
         $satker = $this->db->query("SELECT id,kode,nama from satker where id IN(".$kegiatan->satker_id.")")->result();
         foreach($satker as $rsat){
         ?>
         <li>
           <a href="<?php echo site_url("kka/data?kode=".$rsat->kode); ?>">
             <i class="bi bi-circle"></i><span><?php echo $rsat->nama; ?></span>
           </a>
         </li>
         <?php 
         }
         ?>
       
       </ul>
     </li>
      <?php


       }else if($_SESSION['group_id']=="anggota"){
        ?>
      <li class="nav-heading"> Proses Audit </li>
     <li class="nav-item">
       <a class="nav-link collapsed" data-bs-target="#forms-nav" data-bs-toggle="collapse" href="#">
         <i class="bi bi-journal-text"></i><span>Program Kerja Audit </span><i class="bi bi-chevron-down ms-auto"></i>
       </a>
       <ul id="forms-nav" class="nav-content collapse show " data-bs-parent="#sidebar-nav">
         <?php 
         $satker = $this->db->query("SELECT id,kode,nama from satker where id IN(".$kegiatan->satker_id.")")->result();
         foreach($satker as $rsat){
         ?>
         <li>
           <a href="<?php echo site_url("pka/data?kode=".$rsat->kode); ?>">
             <i class="bi bi-circle"></i><span><?php echo $rsat->nama; ?></span>
           </a>
         </li>
         <?php 
         }
         ?>
       
       </ul>
     </li>

     <li class="nav-item">
       <a class="nav-link collapsed" data-bs-target="#forms-nav" data-bs-toggle="collapse" href="#">
         <i class="bi bi-journal-text"></i><span>Kertas Kerja Audit </span><i class="bi bi-chevron-down ms-auto"></i>
       </a>
       <ul id="forms-nav" class="nav-content collapse show " data-bs-parent="#sidebar-nav">
         <?php 
         $satker = $this->db->query("SELECT id,kode,nama from satker where id IN(".$kegiatan->satker_id.")")->result();
         foreach($satker as $rsat){
         ?>
         <li>
           <a href="<?php echo site_url("kka/data?kode=".$rsat->kode); ?>">
             <i class="bi bi-circle"></i><span><?php echo $rsat->nama; ?></span>
           </a>
         </li>
         <?php 
         }
         ?>
       
       </ul>
     </li>


     <?php

       }
     
     ?>

<li class="nav-item">
       <a class="nav-link collapsed" data-bs-target="#forms-nav" data-bs-toggle="collapse" href="#">
         <i class="bi bi-journal-text"></i><span>Notisi  </span><i class="bi bi-chevron-down ms-auto"></i>
       </a>
       <ul id="forms-nav" class="nav-content collapse show " data-bs-parent="#sidebar-nav">
         <?php 
         $satker = $this->db->query("SELECT id,kode,nama from satker where id IN(".$kegiatan->satker_id.")")->result();
         foreach($satker as $rsat){
         ?>
         <li>
           <a href="<?php echo site_url("notisi/data?kode=".$rsat->kode); ?>">
             <i class="bi bi-circle"></i><span><?php echo $rsat->nama; ?></span>
           </a>
         </li>
         <?php 
         }
         ?>
       
       </ul>
     </li>


      <?php 
       if($_SESSION['group_id']=="ketua"){

        ?>
      <li class="nav-heading">Laporan </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo site_url("auditor/laporan"); ?>">
        <i class="fa fa-file"></i>
          <span> Laporan Hasil Audit</span>
        </a>
      </li>
      <?php 
       }
       ?>

    <?php 
     }
     ?>
     <!-- <li class="nav-heading">e-Palap  </li>
      <li class="nav-item">
      <a class="nav-link collapsed" href="<?php echo site_url("auditor/laporan"); ?>">
      <i class="bi bi-send-plus-fill"></i>
        <span> Penugasan </span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?php echo site_url("auditor/laporan"); ?>">
      <i class="bi bi-car-front"></i>
        <span> Pengeluaran ril </span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?php echo site_url("auditor/laporan"); ?>">
      <i class="bi bi-cash-coin"></i>
        <span>Saldo  </span>
      </a>
    </li>

    -->
      

      <li class="nav-heading">Pengaturan </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#">
          <i class="bi bi-dash-circle"></i>
          <span>Profile Anda</span>
        </a>
      </li><!-- End Error 404 Page Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo site_url("login"); ?>">
          <i class="bi bi-file-earmark"></i>
          <span>Logout</span>
        </a>
      </li><!-- End Blank Page Nav -->

    </ul>

  </aside><!-- End Sidebar-->