<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datasiswa extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 if(!$this->session->userdata("status")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_siswa','m');
		  $this->load->helper('exportpdf_helper');  
		
	  }
	  
   function _template($data)
	{
		
		if($_SESSION['status'] =="madrasah"){
			$this->load->view('madrasah/page_header',$data);	
		}else{
	      $this->load->view('admin/page_header',$data);	
		}	
		
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Data Siswa ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			

			$statusLogin ="<span class='badge badge-sm bg-gradient-secondary'> Belum Login </span>";
			$login       ="";
			if($val['login']==1){
			 $statusLogin ="<span class='badge badge-sm bg-gradient-success'> Sudah Login </span>";
			 $login ="<a href='#' class='btn btn-danger btn-sm resetlogin' tmsiswa_id='".$val['id']."'> Reset  </a>";

			}else if($val['login']==2){
			 $statusLogin ="<span class='badge badge-sm bg-gradient-warning'> Sudah Logout </span>";
			 $login ="<a href='#' class='btn btn-danger btn-sm resetlogin' tmsiswa_id='".$val['id']."'> Reset  </a>";
			}
			
		  

			
			
			$fotoawal ="";
			 if($val['foto_awal'] !=''){
				
			   $fotoawal ="<a href='#' class='btn btn-success btn-sm camerates' tmsiswa_id='".$val['id']."' data-toggle='modal' data-target='#pelaksanaanmodal'><span class='fa fa-camera'></span> </a>"; 
			}



				$no = $i++;
				$records["data"][] = array(
					$no,
					'
					<div class="d-flex px-2 py-1">
                          <div>
                            <img src="https://appmadrasah.kemenag.go.id/seleksiptk/file_upload/peserta/'.$val['nik'].'/'.$val['foto'].'" class="avatar avatar-sm me-3 border-radius-lg" alt="user1" onError="imageError(this)">
                          </div>
                          <div class="d-flex flex-column justify-content-center">
                            <h6 class="mb-0 text-sm">'.$val['nama'].'</h6>
                            <p class="text-xs text-secondary mb-0">'.$val['no_test'].'</p>
                          </div>
                        </div>
					',
					
					$val['no_test'],
					$val['token'],
					
					
				//	$this->Reff->get_kondisi(array("id"=>$val['kategori']),"tm_kategori","nama"),
					$this->Reff->get_kondisi(array("id_admin"=>$val['madrasah_peminatan']),"madrasah_peminatan","owner"),
									
					$val['sesi'],
					$statusLogin,
					
										
				    ' 
				
					<div class="btn-group">

					<a href="'.site_url("datasiswa/kartutes?id=".$val['id']."").'" class="btn btn-success btn-sm" title="Kartu Ujian"> <i class="fa fa-file"></i></a>
					<button type="button" class="btn btn-warning btn-sm ubahmodal" datanya="'.$val['id'].'" urlnya="'.site_url("datasiswa/form").'" title="Ubah Data" target="#loadform" data-toggle="modal" data-target="#defaultModal"> <i class="fa fa-pencil"></i></button>
					<button type="button" class="btn btn-danger btn-sm  hapus" datanya="'.$val['id'].'" urlnya="'.site_url("datasiswa/hapus").'" title="Hapus Data"> <i class="fa fa-trash"></i></button>
				    '.$login.'
					
					</div> 
					
					 
					  '
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
			public function kartutes()
			{
				
			

			$data['breadcumb']  ="Kartu Tes";
				
			$data['siswa']      = $this->db->get_where("tm_siswa",array("id"=>$_GET['id']))->row();
				
			$pdf_filename = 'Kartutes'.str_replace(array("'",",","-"),"",$data['siswa']->nama).'.pdf';	 
			$data_header = array('title' => 'Kartu Tes',);


			$user_info = $this->load->view('kartutes', $data, true);

			$output = $user_info;

			
			generate_pdf($output, $pdf_filename,TRUE);
			

			
			}


	public function hasil()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Data Siswa ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page_hasil',$data);
		
		 }else{
			 
		     $data['konten'] = "page_hasil";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid_hasil(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			
				$simulasi = $this->db->query("select nilai from h_ujian where kategori=0 and tmsiswa_id='".$val['id']."'")->row();
				

				$no = $i++;
				$records["data"][] = array(
					$no,
					
					$val['no_test'],
					
					$val['nama'],
					$simulasi->nilai
					
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	


	public function resetujian(){
		
		$id = $this->input->get_post("id");

		$this->db->query("update tm_siswa set login='0' where id='".$id."'");
		$this->db->query("delete from h_ujian where tmsiswa_id='".$id."'");
		echo "sukses";
		
		
	}

	public function form(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("tm_siswa",array("id"=>$id));
			   
		   }
		$this->load->view("form",$data);
		
	}
	
	
	
	public function save(){
     
         $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
		 $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
    
		 
			
				$config = array(
				    
				   
				    array('field' => 'f[nama]', 'label' => 'Nama Guru  ', 'rules' => 'trim|required'),
				  
				   
			
				   
				  
				   
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
				
								
				$config['upload_path']      = './__statics/upload/';
				$folder   					= '__statics/upload/';
		        $config['allowed_types']    = "jpg|jpeg|png"; 	
				$config['overwrite']        = true; 
                $config['max_size']         = '8200';				
				$filenamepecah			    = explode(".",$_FILES['file']['name']);
				$imagenew				    = "dp".time().".".pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
				$config['file_name'] = $imagenew;
				
				$id = $this->input->get_post("id",true);
				
				   $this->load->library('upload', $config);

						if ( ! $this->upload->do_upload('file'))
						{
							
							
								 
							$this->m->update($id); 
							echo "Data Berhasil disimpan";	
									
							
							
						
						}
						else{
				 
				
							 if(empty($id)){
								
								   $this->db->set("foto",$imagenew);
								   $this->m->insert();
								   echo "Data Berhasil disimpan";	
								 
								
								
							 }else{

								$this->db->set("foto",$imagenew);
								$this->m->update($id); 
								echo "Data Berhasil disimpan";	
								 
							 }


							 
							}
							
						
		
		
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}
	

	
	public function form_excel(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("tm_siswa",array("id"=>$id));
			   
		   }
		$this->load->view("form_excel",$data);
		
	}
	
	public function import_excel()
		{
	  
		 	$this->load->library('PHPExcel/IOFactory');
       // error_reporting(0);
	   $pengaturan_id  = $this->Reff->set();
	   $kategori       = $this->input->get_post("kategori");

	    $sheetup   = 0;
	    $rowup     = 2;
		
		
	
		$fileName = time().".xlsx";
         
        $config['upload_path'] = './__statics/excel/tmp/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = '*';
        $config['max_size'] = 100000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') ){
			$jupload=0;
              echo $this->upload->display_errors();

			  
		}else{
             
        $media = $this->upload->data('file');
		
        $inputFileName = './__statics/excel/tmp/'.$fileName;
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet($sheetup);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
        
             
			 $jupload=0;
            for ($row = $rowup; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
				
       
			 	
			 		 
			 	$no_test	          =      (trim($rowData[0][1]));			 
			 	$nisn	              =      (trim($rowData[0][2]));			 
			 	$nama                 =      (trim($rowData[0][3]));	
				$gender	              =      (trim($rowData[0][4]));		 
			 	$tempat               =      (trim($rowData[0][5]));			 
			 	$tgl_lahir            =      (trim($rowData[0][6]));			 
			 				 
			 		 		 
			
					 
				
					
				
					$jupload++;
						$data = array(							                 
							"pengaturan_id"=> $pengaturan_id,                   
							"kategori"=> $kategori,                   
							"no_test"=> $no_test,                   
							"nisn"=> $nisn,                   
							"nama"=> $nama,                   
							"gender"=> $gender,                   
							"tempat"=> $tempat,                                 
							"tgl_lahir"=> $tgl_lahir,  
							"sesi"=> 1,  							  
							"token"=> $this->Reff->randomangka(6)              
								
						);
				  
				 
				  
				 
                  
                   $this->db->insert("tm_siswa",$data);
				
				   
				   
				
           
		   
			}
			
			
		
			echo "<br><br><center>Data yg di upload : ". $jupload ." Data <br> <a href='".site_url("datasiswa")."' > Kembali ke Aplikasi </a>";
        
        }
   }
	
	
	public function hapus(){
		
		$id = $this->input->get_post("id",true);
		
		$this->db->delete("tm_siswa",array("id"=>$id));
		echo "sukses";
	}
	// sESI 

	public function sesi()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['tmujian_id']  = $this->input->get_post("id");
		 $data['title']   = "Pemilihan Sesi Siswa ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page_sesi',$data);
		
		 }else{
			 
		     $data['konten'] = "page_sesi";
			 
			 $this->_template($data);
		 }
	

	}

	public function sesi_ujian()
	{  

		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['tmjadwal_id']  = $this->input->get_post("id");
		 $data['title']   = "Pelaksanaan Ujian  ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page_sesi_ujian',$data);
		
		 }else{
			 
		     $data['konten'] = "page_sesi_ujian";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid_sesi(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			   $disabled="";
			   if($_SESSION['status'] !="madrasah"){
				$disabled="disabled";
			   }

			$selek ="<select class='form-control sesi' datanya='".$val['id']."' ".$disabled.">";
			$selek .="<option value=''>- Pilih Sesi - </option>";
			  $sesi = $this->db->query("select * from tm_sesi")->result();
			    foreach($sesi as $r){
					 $selected="";
					   if($r->sesi==$val['sesi']){
						  $selected="selected";
					   }
					$selek .="<option value='".$r->sesi."' ".$selected."> Sesi ".$r->sesi." - ".$r->hari." pukul ".$r->pukul."</option>";

				}
				
					
					
		    $selek .="</select>";
			
			 


				$no = $i++;
				$records["data"][] = array(
					$no,
					
					$val['madrasah'],
					$val['no_aksi']."",
					$val['nama'],
					$val['tempat'],
					$val['tgl_lahir'],
					$val['namakelas'],
					$selek
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	

	public function pilihsesi(){

	 $sesi = $_POST['sesi'];
	 $id    = $_POST['id'];
	 if($sesi !=""){
	   $cek = $this->db->query("select count(id) as jml from tm_siswa where sesi='".$sesi."'")->row();
	   $jml = $cek->jml;
	 }else{
		$jml = 0;

	 }
	     if($jml >= 3000){

             echo 1;
		 }else{

			$this->db->set("sesi",$sesi);
			$this->db->where("id",$id);
			$this->db->update("tm_siswa");
			echo "sukses";


		 }

	}

	public function infosesi()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Informasi Sesi Siswa ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page_sesi_info',$data);
		
		 }else{
			 
		     $data['konten'] = "page_sesi_info";
			 
			 $this->_template($data);
		 }
	

	}

	public function generate_no(){


		 
		$siswa = $this->db->query("select * from tm_siswa where no_test='' order by tmmadrasah_id asc,nama asc")->result();
		 $arraysekolah = array("0"=>"1",
		                       "1"=>"2",
							   "2"=>"3",
							   "3"=>"4"); 
							   
			
		 
		   foreach($siswa as $row){
			     $maxid     = $this->db->query("select max(urutan) as pendaftar from tm_siswa ")->row();
				 $pendaftar = $maxid->pendaftar+1;
				 //$jenis = $row->jenis_sekolah;
				 
				
			   
			   
				 if(strlen($pendaftar)==1){
					 $nopen = "0000".$pendaftar;
				 }else if(strlen($pendaftar)==2){
					 
					 $nopen = "000".$pendaftar;
				 }else if(strlen($pendaftar)==3){
					 
					 $nopen = "00".$pendaftar;
				 }else if(strlen($pendaftar)==4){
					 
					 $nopen = "0".$pendaftar;
				 }else if(strlen($pendaftar)==5){
					 
					 $nopen = $pendaftar;
				 }
				 
				 $kategori = $this->Reff->get_kondisi(array("id"=>$row->kategori),"tm_kategori","kode");
				 $sesi     = strlen($row->sesi==1) ? "0".$row->sesi : $row->sesi; 
				 //$no_test    = $sesi.".".$kategori.".".$nopen; 

				 $no_test  ="10-07-503-".$nopen;
				 
				 $this->db->set("urutan",($pendaftar));
				 $this->db->set("no_test",($no_test));
				 $this->db->set("token",$this->Reff->randomangka(6));
				 $this->db->where("id",$row->id);
				 $this->db->update("tm_siswa");
			   
			    
			   
		   }
		   
		   $jumlahsiswa = count($siswa); 
		   if($jumlahsiswa==0){
			    echo  "Generate Gagal , Seluruh siswa sudah di generate sebelumnya";
		   }else{
		    echo  $jumlahsiswa." Nomor Test Calon siswa berhasil di generate";
		   }


	}

	public function resetlogin(){



		$this->db->set("login",0);
		$this->db->set("locked",0);
		$this->db->where("id",$_POST['tmsiswa_id']);
		$this->db->update("tm_siswa");
		echo "sukses";
	}


	public function camera(){
		
		$tmujian_id = $this->input->get_post("tmujian_id");
		$data = array();
		  
			  $data['data']  = $this->Reff->get_where("h_ujian",array("id"=>$tmujian_id));
			   
		   
		$this->load->view("camera",$data);
		
	}
	
	public function camerates(){
		
		$tmsiswa_id = $this->input->get_post("tmsiswa_id");
		$data = array();
		  
			  $data['data']  = $this->Reff->get_where("tm_siswa",array("id"=>$tmsiswa_id));
			   
		   
		$this->load->view("camerates",$data);
		
	}


public function bantuan(){

	  $data['data'] = $this->db->get_where("tm_siswa",array("id"=>$_POST['tmsiswa_id']))->row();
	  $data['pertanyaan'] = $this->db->query("select * from bantuan where tmsiswa_id='".$_POST['tmsiswa_id']."' AND jawaban ='' order by tgl_pertanyaan DESC limit 1")->row();

	  $this->load->view("bantuan",$data);


}

public function bantuan_save(){


   $this->db->set("tgl_jawaban",date("Y-m-d H:i:s"));
   $this->db->set("jawaban",$_POST['pertanyaan']);
   $this->db->where("id",$_POST['id']);
   $this->db->update("bantuan");
   echo "sukses";
}
   
	 
}
