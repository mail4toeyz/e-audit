<?php
ini_set('memory_limit', '-1');

defined('BASEPATH') OR exit('No direct script access allowed');

class Penawaran extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		if(!$this->session->userdata("isKantor")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_guru','m');
		
	  }
	  
   function _template($data)
	{
		$this->load->view(''.$_SESSION['isKantor'].'/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Penawaran Madrasah ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		  error_reporting(0);
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		  $arbantuan = array("1"=>"Kinerja","2"=>"Afirmasi");
		  
		   $i= ($iDisplayStart +1);
		   $status= $_POST['status'];
		   
		   
		   foreach($datagrid as $val) {
			    
				$no = $i++;
				$pernyataan ="";
				 $statusPernyataan = $this->db->query("SELECT * from conf_bkba where nsm='".$val['nsm']."'")->row();

				 if(!is_null($statusPernyataan)){
				  if($statusPernyataan->bersedia_menerima=="Ya"){
					$pernyataan ='<span class="badge badge-light-success"> Menerima </span><br>';
					$pernyataan .='Tgl Konfirmasi : '.$this->Reff->formattimestamp($statusPernyataan->tanggal_konfirmasi).'<br>';
					$pernyataan .='File Surat : <a href="'.$statusPernyataan->path_surat_pernyataan.'" target="_blank"> '.$statusPernyataan->path_surat_pernyataan.' </a>';
				  }else{

					$pernyataan ='<span class="badge badge-light-danger"> Menolak </span><br>';
					$pernyataan .='Tgl Konfirmasi : '.$this->Reff->formattimestamp($statusPernyataan->tanggal_konfirmasi).'<br>';
					$pernyataan .='Alasan : '.$statusPernyataan->alasan_tidak_bersedia.'';
				  }
				}else{
					$pernyataan ='<span class="badge badge-default" style="color:black"> Belum Konfirmasi </span>';

				}
			
				$records["data"][] = array(
					$no,
					$pernyataan,
					
					strtoupper($val['jenjang']),
					$val['nsm'],
					$val['nama'],
					$val['akreditasi'],
					$val['provinsi'],
					$val['kota'],				
					$arbantuan[$val['bantuan']],
					number_format($val['skor_akhir'],1),
					number_format($val['skor_edm'],1),
					number_format($val['skor_pip'],1),
					number_format($val['skor_rombel'],1),
					number_format($val['skor_toilet'],1),
					
					$val['jml_siswa'],
					$val['jml_guru'],
					$val['siswa_laki'],
					$val['siswa_perempuan'],
					$val['rombel'],
					$val['rombel_ideal'],
					number_format($val['rombel_persen'],1),
					$val['toilet_jumlah'],
					$val['toilet_ideal'],
					number_format($val['toilet_persen'],1),

					$val['jml_guru'],
					$val['siswa_pip'],
					number_format($val['persen_pip'],1)." %",
					$val['skor_pip'],
					sprintf('%0.2f',$val['nilai_kedisiplinan']),
					sprintf('%0.2f',$val['nilai_pengembangan_diri']),
					sprintf('%0.2f',$val['nilai_proses_pembelajaran']),
					sprintf('%0.2f',$val['nilai_sarana_prasarana']),
					sprintf('%0.2f',$val['nilai_pembiayaan']),
					number_format($val['skor_edm'],2),
					number_format($val['nilai_skpm'],2),
					$this->Reff->formatuang2($val['rencana_pendapatan']),
					$this->Reff->formatuang2($val['total_rencana_kegiatan'])
					
					
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	public function form(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("tm_guru",array("id"=>$id));
			   
		   }
		$this->load->view("form",$data);
		
	}
	
	
	
	 
}
