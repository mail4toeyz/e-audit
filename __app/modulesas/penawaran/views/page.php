
		<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Post-->
<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
	<div id="kt_content_container" class="container-xxl">
				       <div class="card">
						<div class="card-header card-header-success">
						  <h4 class="card-title"> Penawaran Bantuan </h4>
						  <p class="card-category">
						  
						  </p>
						</div>
					  
						 <div class="card-body">
						 <div class="row">
							  
													
													
							  <div class="col-md-2">
									<select class="form-control" id="status">
										 <option value="">- Tampilkan Status   - </option>
										 <option value="1"> Menerima </option>
										 <option value="2"> Menolak </option>
										 <option value="3"> Belum Konfirmasi </option>
										
										
									 
									 </select>
								 
								  </div>

								  <div class="col-md-2">
									<select class="form-control" id="jenjang">
										 <option value="">- Jenjang - </option>
										 <?php 
										   $jenjang = array("mi","mts","ma");
											 foreach($jenjang as $r){
												 ?><option value="<?php echo $r; ?>"  > <?php echo strtoupper($r); ?> </option><?php 
												 
											 }
											?>
										
									 
									 </select>
								 
								  </div>
								  
								  <div class="col-md-2">
												
												<select class="form-control onchange " id="provinsi" target="kota" urlnya="<?php echo site_url('publik/kota'); ?>">
													 <option value="">- Provinsi - </option>
													 <?php 
														 $provinsi = $this->db->get("provinsi")->result();
														 foreach($provinsi as $row){
															 ?><option value="<?php echo strtoupper($row->id); ?>"> <?php echo ($row->nama); ?> </option><?php 
															 
														 }
														?>
												 
												 </select>
											 
											  </div>

											  <div class="col-md-2">
										  
												<select class="form-control kota " id="kota" target="kota" urlnya="<?php echo site_url('publik/kota'); ?>">
													 <option value="">- Kabupaten/Kota - </option>
													
												 
												 </select>
											 
											  </div>
								  
								  <div class="col-md-3">
									<input class="form-control" id="keyword"  placeholder="Apa yang Anda cari ?">
									<input class="form-control" type="hidden" id="bantuan" value="<?php echo isset($_GET['bantuan']) ? $_GET['bantuan']:''; ?>">
								  
								 
								  </div>
								  <div class="col-md-1">
									<button class="btn btn-sm btn-primary" id="tampilkan" > <i class="fa fa-search"></i> </button> 
								 
								  </div>
							 
						  </div>

							 <br>
							<div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
									<tr class="fw-bolder text-muted bg-light">
                                            <th width="2px" rowspan="2">NO</th>
                                            <th width="2px" rowspan="2">KONFIRMASI</th>
                                            
                                            <th rowspan="2">JENJANG </th>
                                            <th rowspan="2">NSM </th>
                                            <th rowspan="2">NAMA </th>
                                            <th rowspan="2">AKREDITASI </th>
                                            <th rowspan="2">PROVINSI </th>
                                            <th rowspan="2">KABKOTA </th>
                                             <th rowspan="2">BANTUAN </th>
                                            <th rowspan="2">SKOR </th>
                                            <th colspan="4">RINCIAN SKOR </th>
											<th colspan="11">EMIS </th>
											<th colspan="3">PIP </th>
                                            <th colspan="7">SKOR HASIL EDM </th>
                                            <th colspan="2">eRKAM </th>
											
											
                                            
                                        </tr>

										<tr class="fw-bolder text-muted bg-light">

										  <th> EDM </th>
										  <th> PIP </th>
										  <th> Ruang Belajar </th>
										  <th> Toilet </th>

										  <th> Jml Siswa </th>
										  <th> Jml Guru  </th>
										  <th> Siswa (L) </th>
										  <th> Siswa (P) </th>
										  <th> Jml Ruang Belajar </th>
										  <th> Ruang Belajar(Ideal) </th>
										  <th> Ruang Belajar(%) </th>
										  <th> Jml Toilet </th>
										  <th> Toilet (Ideal) </th>
										  <th> Toilet (%) </th>
										  <th> Jml Guru </th>


										  <th> Siswa Penerima PIP</th>
										  <th> Persen </th>
										  <th> Skor </th>
										  <th> Kedisiplinan</th>
										  <th> Pengembangan Diri</th>
										  <th> Pembelajaran</th>
										  <th> Sarana & Prasarana</th>
										  <th> Pembiayaan</th>
										  <th> Skor EDM </th>
										  <th> SKPM </th>
										  <th> Rencana Pendapatan </th>											
										  <th> Total Rencana Kegiatan </th>

										 

										</tr>

                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                         </div>
                         </div>
                         </div>
                         </div>
                         </div>
                 
 
 
              
        
<div id="defaultModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body" id="loadform">
        <p>Loading...</p>
      </div>
      
    </div>

  </div>
</div>
	
<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"scrollY": 500,
                        "scrollX": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,30, 50,100,200,300,500,1000, 800000000], [10,30, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					
                     buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
					
					"ajax":{
						url :"<?php echo site_url("penawaran/grid"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						data.status = $("#status").val();
						data.jenjang = $("#jenjang").val();
						data.provinsi = $("#provinsi").val();
						data.kota = $("#kota").val();
						data.keyword = $("#keyword").val();
						data.bantuan = $("#bantuan").val();
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				

				$(document).on("change","#status,#jenjang,#provinsi,#kota",function(){


					dataTable.ajax.reload(null,false);	
				});

				$(document).on("input","#keyword",function(){


				   dataTable.ajax.reload(null,false);	
				});
				
				
	


</script>
				