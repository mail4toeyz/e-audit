<style>

.table-scroll {
    position: relative;
    width:100%;
    z-index: 1;
    margin: auto;
    overflow: scroll;
    height: 80vh;
}
.table-scroll table {
    width: 100%;
    min-width: 100px;
    margin: auto;
    border-collapse: separate;
    border-spacing: 0;
}
.table-wrap {
    position: relative;
}
.table-scroll th,
.table-scroll td {
    padding: 5px 10px;
    border: 1px solid #000;
    
    vertical-align: top;
    text-align: center;
}
.table-scroll thead th {
    background: #333;
    color: #fff;
    position: -webkit-sticky;
    position: sticky;
    top: 0;
}
th.sticky {
    position: -webkit-sticky;
    position: sticky;
    left: 0;
    z-index: 2;
    background: #ccc;
}
thead th.sticky {
    z-index: 5;
}
.table-scroll thead tr:nth-child(2) th {
    top: 30px;
}

						</style>

<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Post-->
<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
	<div id="kt_content_container" class="container-xxl">
     <center> <a href="<?php echo site_url('pusat/get_pernyataan'); ?>" target="_blank" class="btn btn-danger btn-sm"> Tarik Data </a> </center> <br>
  <?php 
  error_reporting(0);
  $tahun   = $this->Reff->tahun();
                             
                              $shortlist  = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and tahun='{$tahun}'")->row();
                              $pernyataan  = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and tahun='{$tahun}' and nsm IN(SELECT nsm FROM conf_bkba where bersedia_menerima ='Ya')")->row();
                              $pernyataanmenolak  = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and tahun='{$tahun}' and nsm IN(SELECT nsm FROM conf_bkba where bersedia_menerima ='Tidak')")->row();

                             
                            
                              $presentasi_pernyataan = number_format((($pernyataan->jml / $shortlist->jml) * 100),2);

                              $presentasi_pernyataanmenolak = number_format((($pernyataanmenolak->jml / $shortlist->jml) * 100),2);

                              $belumkonfirm = $shortlist->jml - ($pernyataan->jml+$pernyataanmenolak->jml);
                              $presentasi_pernyataanall = number_format((($belumkonfirm / $shortlist->jml) * 100),2);
   ?>
 
  <div class="row">
                  
                  <div class="col-xl-3">
										<!--begin: Stats Widget 19-->
										<div class="card card-custom bg-primary card-stretch gutter-b rounded-xl ">
											<!--begin::Body-->
											<div class="card-body my-3">
                      <span class="fa fa-check-square-o " style="color:white;font-size:40px"> <?php echo $shortlist->jml; ?>   </span>  <br><br>
												<a href="#"  style="color:white;font-size:16px;font-weight:bold">Shortlist  BKBA </a>
												
											
											</div>
											<!--end:: Body-->
										</div>
										<!--end: Stats:Widget 19-->
									</div>

                  <div class="col-xl-3">
										<!--begin: Stats Widget 19-->
										<div class="card card-custom bg-success card-stretch gutter-b rounded-xl ">
											<!--begin::Body-->
											<div class="card-body my-3">
                      <span class="fa fa-credit-card " style="color:white;font-size:40px"> <?php echo $pernyataan->jml; ?>   </span>  <br><br>
												<a href="#"  style="color:white;font-size:15px;font-weight:bold">Bersedia Menerima  (<?php echo $presentasi_pernyataan; ?>%) </a>
												
											
											</div>
											<!--end:: Body-->
										</div>
										<!--end: Stats:Widget 19-->
									</div>



                  <div class="col-xl-3">
										<!--begin: Stats Widget 19-->
										<div class="card card-custom bg-danger card-stretch gutter-b rounded-xl ">
											<!--begin::Body-->
											<div class="card-body my-3">
                      <span class="fa fa-credit-card " style="color:white;font-size:40px"> <?php echo $pernyataanmenolak->jml; ?>   </span>  <br><br>
												<a href="#"  style="color:white;font-size:16px;font-weight:bold"> Menolak  (<?php echo $presentasi_pernyataanmenolak; ?>%) </a>
												
											
											</div>
											<!--end:: Body-->
										</div>
										<!--end: Stats:Widget 19-->
									</div>


                  <div class="col-xl-3">
										<!--begin: Stats Widget 19-->
										<div class="card card-custom bg-warning card-stretch gutter-b rounded-xl ">
											<!--begin::Body-->
											<div class="card-body my-3">
                      <span class="fa fa-credit-card " style="color:white;font-size:40px"> <?php echo $belumkonfirm; ?>   </span>  <br><br>
												<a href="#"  style="color:white;font-size:16px;font-weight:bold"> Belum Konfirmasi  (<?php echo $presentasi_pernyataanall; ?>%) </a>
												
											
											</div>
											<!--end:: Body-->
										</div>
										<!--end: Stats:Widget 19-->
									</div>






													
	</div>

  <br>

  <div class="table-responsive table-scroll ">
                              
															<table class="table table-condensed table-bordered table-hover  " id="datatableTable" >
																<thead>
																	<tr>
																		<th rowspan="2"> NO </th>
																		
																		<th rowspan="2"> PROVINSI </th>
																		
																		<th colspan="4">  STATUS  </th>
																		
																		
																</tr>
																<tr>

																  <th> SHORTLIST </th>
																  <th> MENERIMA </th>
																  <th> MENOLAK </th>
																  <th> BELUM KONFIRMASI </th>
																  

																</tr>
													</thead>
														<tbody>

															<?php 
                                                            $provinsi = $this->db->query(" select * from provinsi where (kuota_kinerja+kuota_afirmasi) !=0")->result();
															$no_prov=1;
															$tahun    = $this->Reff->tahun();

															$totalshortlist=0;
															$totalmenerima=0;
															$totalmenolak=0;
															$totalbelumkonfirm=0;
														
															 foreach($provinsi as $prov){
															

																 $shortlist = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and provinsi_id='{$prov->id}' and tahun='{$tahun}'")->row();
																 $shortlistpernyataan = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and provinsi_id='{$prov->id}' and tahun='{$tahun}' and nsm IN(SELECT nsm FROM conf_bkba where bersedia_menerima ='Ya')")->row();
																 $shortlistpernyataanmenolak = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and provinsi_id='{$prov->id}' and tahun='{$tahun}' and nsm IN(SELECT nsm FROM conf_bkba where bersedia_menerima ='Tidak')")->row();
																
                                 $belumkonfirm = $shortlist->jml - ($shortlistpernyataan->jml+$shortlistpernyataanmenolak->jml);

																 $totalshortlist= $totalshortlist + $shortlist->jml;
																 $totalmenerima= $totalmenerima + $shortlistpernyataan->jml;
																 $totalmenolak= $totalmenolak + $shortlistpernyataanmenolak->jml;
																 $totalbelumkonfirm= $totalbelumkonfirm + $belumkonfirm;
															
																?>

																<tr style="font-weight:bold">
																		<th> <?php echo $no_prov++; ?> </th>
																		
																		<th> <?php echo $prov->nama; ?> </th>
																		<th> <?php echo $shortlist->jml; ?>  </th>
																		<th> <?php echo $shortlistpernyataan->jml; ?>  </th>
																		<th> <?php echo $shortlistpernyataanmenolak->jml; ?>  </th>
																		<th> <?php echo $belumkonfirm; ?>  </th>
																
																		
																</tr>

																

															   <?php 

															
														}


															?>


															

														</tbody>
														   <tr>
																	   <th> # </th>
																	
																	   <th>Total</th>
																	   <th><?php echo $totalshortlist; ?></th>
																	   <th><?php echo $totalmenerima; ?></th>
																	   <th><?php echo $totalmenolak; ?></th>
																	   <th><?php echo $totalbelumkonfirm; ?></th>
																	 
																	 
																	   
																	  
															   </tr>

															</table>

													</div>
              
              
              
              
              
              
              
              
              
              </div>


</div>