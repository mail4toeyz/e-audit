

<div class="">
						<!--begin::Post-->
<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
	<div id="kt_content_container" class="container-custom">
   
  <?php 
  error_reporting(0);

   $where ="";
     if($_SESSION['status']=="verifikator"){

      $where =" AND provinsi_id='".$_SESSION['admin_id']."'";

     }
  $tahun   = $this->Reff->tahun();
                              $simsarpras  = $this->db->query("select count(id) as jml from madrasah_penerima where anggaran='2023' ")->row();
                              $pupr        = $this->db->query("SELECT COUNT(nsm) as jml FROM `penerima_bantuan` where anggaran LIKE '%2023%' and kategori='PUPR' ")->row();
                              $sbsn        = $this->db->query("SELECT COUNT(nsm) as jml FROM `penerima_bantuan` where anggaran LIKE '%2023%' and kategori='SBSN Madrasah' ")->row();
                              $afiliasi  = $this->db->query("SELECT count(id) as jml FROM `madrasahedm` where tahun='2023' and nsm  in(SELECT nsm from madrasahedm where shortlist=1 and pernyataan=0 and tahun='2021' and nsm  IN(SELECT nsm FROM conf_bkba where bersedia_menerima ='Ya')) ")->row();
                             
                              $madrasah  = $this->db->query("select count(id) as jml from madrasahedm where tahun='{$tahun}' $where ")->row();
                              $longlist  = $this->db->query("select count(id) as jml from madrasahedm where longlist=1 AND tahun='{$tahun}' $where")->row();
                              $shortlist  = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and tahun='{$tahun}'  $where")->row();
                              $pernyataan  = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and tahun='{$tahun}' and nsm IN(SELECT nsm FROM conf_bkba where bersedia_menerima ='Ya') $where")->row();

                              $sudah_menerima_bkba  = $this->db->query("select count(id) as jml from madrasahedm where tahun='{$tahun}' and sudah_menerima_bkba =1 $where")->row();
                              $sudah_menerima_simsarpras  = $this->db->query("select count(id) as jml from madrasahedm where tahun='{$tahun}' and simsarpras=1 $where")->row();
                              $sudah_menerima_pupr  = $this->db->query("select count(id) as jml from madrasahedm where tahun='{$tahun}' and pupr=1 $where")->row();
                              $sudah_menerima_sbsn  = $this->db->query("select count(id) as jml from madrasahedm where tahun='{$tahun}' and sbsn=1 $where")->row();
                              $calonPenerima   = $this->db->query("select count(id) as jml from madrasahedm where tahun='{$tahun}' and blacklist=0 and sudah_menerima_bkba=0 and simsarpras=0 and provinsi_id  IN(select id from provinsi where kuota_kinerja+kuota_afirmasi !=0) $where")->row();
                              $madrasahInk   = $this->db->query("select count(id) as jml from madrasahedm where tahun='{$tahun}' and blacklist=0 and inklusi=1 $where ")->row();
                              //$calonPenerima   = $madrasah->jml - $sudah_menerima_bkba->jml - $sudah_menerima_simsarpras->jml;
                              $pernyataanmenolak  = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and tahun='{$tahun}' and nsm IN(SELECT nsm FROM conf_bkba where bersedia_menerima ='Tidak') $where")->row();

                              $presentasi_longlist   = round(($longlist->jml / $calonPenerima->jml) * 100);
                              $presentasi_shortlist  = round(($shortlist->jml / $longlist->jml) * 100);
                              $presentasi_pernyataan = round(($pernyataan->jml / $shortlist->jml) * 100);
                              $presentasi_menolak = round(($pernyataanmenolak->jml / $shortlist->jml) * 100);

                              $selisih  = $this->db->query("select AVG(skor_visitasi) as jml from v_madrasah_visitasi where shortlist=1 and tahun='{$tahun}' and nsm IN(SELECT nsm FROM conf_bkba where bersedia_menerima ='Ya') $where")->row();
                          
   ?>
  <div class="row">
                  <div class="col-xl-12">
                  <div class="card course-box"> 
                      <div class="card-body"> 
                        <div class="course-widget"> 
                          <div class="course-icon" style="width:70px"> 
                              <i class="fa fa-check-square-o" style="color:white;"></i>
                          </div>
                          <div> 
                            <h4 class="mb-0"><?php echo $this->Reff->formatuang2($madrasah->jml); ?></h4><span class="f-light"> Data EDM - eRKAM </span> <br>
                            <small> Data  Madrasah  sudah menyelesaikan EDM dan eRKAM  dan tidak pernah ikut dalam seleksi BKBA piloting, Batch 1 dan Batch 2</small>
                            
                          </div>
                        </div>
                      </div>
                      <ul class="square-group">
                        <li class="square-1 warning"></li>
                        <li class="square-1 primary"></li>
                        <li class="square-2 warning1"></li>
                        <li class="square-3 danger"></li>
                        <li class="square-4 light"></li>
                        <li class="square-5 warning"></li>
                        <li class="square-6 success"></li>
                        <li class="square-7 success"></li>
                      </ul>
                    </div>
              </div>

              <div class="col-xl-3">
                  <div class="card course-box"> 
                      <div class="card-body"> 
                        <div class="course-widget"> 
                          <div class="course-icon" style="width:50px"> 
                              <i class="fa fa-check-square-o" style="color:white;"></i>
                          </div>
                          <div> 
                            <h4 class="mb-0"><?php echo $this->Reff->formatuang2("5512"); ?></h4><span class="f-light"> Sudah Menerima BKBA </span> <br>
                            <small> 33 Madrasah Penerima BKBA  2021 <br> 2.302 Madrasah Penerima BKBA 2022 Batch 1 <br>  3.177 Madrasah Penerima 2023 Batch 2 </small>
                            
                          </div>
                        </div>
                      </div>
                      <ul class="square-group">
                        <li class="square-1 warning"></li>
                        <li class="square-1 primary"></li>
                        <li class="square-2 warning1"></li>
                        <li class="square-3 danger"></li>
                        <li class="square-4 light"></li>
                        <li class="square-5 warning"></li>
                        <li class="square-6 success"></li>
                        <li class="square-7 success"></li>
                      </ul>
                    </div>
              </div>

              <div class="col-xl-3">
                  <div class="card course-box"> 
                      <div class="card-body"> 
                        <div class="course-widget"> 
                          <div class="course-icon" style="width:70px"> 
                              <i class="fa fa-check-square-o" style="color:white;"></i>
                          </div>
                          <div> 
                            <h4 class="mb-0"><?php echo $this->Reff->formatuang2($sudah_menerima_simsarpras->jml); ?></h4><span class="f-light"> Penerima SIMSARPRAS </span> <br>
                            <small> <?php echo $this->Reff->formatuang2($simsarpras->jml); ?> Madrasah Penerima 2023 <br>  <?php echo $this->Reff->formatuang2($sudah_menerima_simsarpras->jml); ?> Madrasah Penerima SIMSARPRAS merupakan Sasaran EDM eRKAM </small>
                            
                          </div>
                        </div>
                      </div>
                      <ul class="square-group">
                        <li class="square-1 warning"></li>
                        <li class="square-1 primary"></li>
                        <li class="square-2 warning1"></li>
                        <li class="square-3 danger"></li>
                        <li class="square-4 light"></li>
                        <li class="square-5 warning"></li>
                        <li class="square-6 success"></li>
                        <li class="square-7 success"></li>
                      </ul>
                    </div>
              </div>
              <div class="col-xl-3">
                  <div class="card course-box"> 
                      <div class="card-body"> 
                        <div class="course-widget"> 
                          <div class="course-icon" style="width:70px"> 
                              <i class="fa fa-check-square-o" style="color:white;"></i>
                          </div>
                          <div> 
                            <h4 class="mb-0"><?php echo $this->Reff->formatuang2($sudah_menerima_pupr->jml); ?></h4><span class="f-light"> Penerima PUPR </span> <br>
                            <small> <?php echo $this->Reff->formatuang2($pupr->jml); ?> Madrasah menerima Bantuan PUPR 2023 <br>  <?php echo $this->Reff->formatuang2($sudah_menerima_pupr->jml); ?> Madrasah  merupakan Sasaran EDM eRKAM </small>
                            
                          </div>
                        </div>
                      </div>
                      <ul class="square-group">
                        <li class="square-1 warning"></li>
                        <li class="square-1 primary"></li>
                        <li class="square-2 warning1"></li>
                        <li class="square-3 danger"></li>
                        <li class="square-4 light"></li>
                        <li class="square-5 warning"></li>
                        <li class="square-6 success"></li>
                        <li class="square-7 success"></li>
                      </ul>
                    </div>
              </div>

              <div class="col-xl-3">
                  <div class="card course-box"> 
                      <div class="card-body"> 
                        <div class="course-widget"> 
                          <div class="course-icon" style="width:70px"> 
                              <i class="fa fa-check-square-o" style="color:white;"></i>
                          </div>
                          <div> 
                            <h4 class="mb-0"><?php echo $this->Reff->formatuang2($sudah_menerima_sbsn->jml); ?></h4><span class="f-light"> Penerima SBSN Madrasah </span> <br>
                            <small> <?php echo $this->Reff->formatuang2($sbsn->jml); ?> Madrasah menerima  SBSN Madrasah 2023 <br>  <?php echo $this->Reff->formatuang2($sudah_menerima_sbsn->jml); ?> Madrasah  merupakan Sasaran EDM eRKAM </small>
                            
                          </div>
                        </div>
                      </div>
                      <ul class="square-group">
                        <li class="square-1 warning"></li>
                        <li class="square-1 primary"></li>
                        <li class="square-2 warning1"></li>
                        <li class="square-3 danger"></li>
                        <li class="square-4 light"></li>
                        <li class="square-5 warning"></li>
                        <li class="square-6 success"></li>
                        <li class="square-7 success"></li>
                      </ul>
                    </div>
              </div>

              <div class="col-xl-12">
                  <div class="card course-box"> 
                      <div class="card-body"> 
                        <div class="course-widget"> 
                          <div class="course-icon"> 
                              <i class="fa fa-check-square-o" style="color:white;"></i>
                          </div>
                          <div> 
                            <h4 class="mb-0"><?php echo $this->Reff->formatuang2($calonPenerima->jml); ?></h4><span class="f-light"> Calon Penerima </span> <br>
                            <small> Madrasah calon penerima BKBA yang akan diterapkan kriteria Longlist berdasarkan hasil dari total Madrasah EDM eRKAM dan belum mendapatkan  Bantuan lainnya </small>
                            
                          </div>
                        </div>
                      </div>
                      <ul class="square-group">
                        <li class="square-1 warning"></li>
                        <li class="square-1 primary"></li>
                        <li class="square-2 warning1"></li>
                        <li class="square-3 danger"></li>
                        <li class="square-4 light"></li>
                        <li class="square-5 warning"></li>
                        <li class="square-6 success"></li>
                        <li class="square-7 success"></li>
                      </ul>
                    </div>
              </div>

              <div class="col-xl-6">
                  <div class="card course-box"> 
                      <div class="card-body"> 
                        <div class="course-widget"> 
                          <div class="course-icon" > 
                              <i class="fa fa-check-square-o" style="color:white;"></i>
                          </div>
                          <div> 
                            <h4 class="mb-0"><?php echo $this->Reff->formatuang2($longlist->jml); ?></h4><span class="f-light"> Longlist  </span> <br>
                            <small> Total Madrasah yang masuk dalam kriteria longlist dan <br> akan diterapkan kriteria shortlist  </small>
                            
                          </div>
                        </div>
                      </div>
                      <ul class="square-group">
                        <li class="square-1 warning"></li>
                        <li class="square-1 primary"></li>
                        <li class="square-2 warning1"></li>
                        <li class="square-3 danger"></li>
                        <li class="square-4 light"></li>
                        <li class="square-5 warning"></li>
                        <li class="square-6 success"></li>
                        <li class="square-7 success"></li>
                      </ul>
                    </div>
              </div>

              <div class="col-xl-6">
                  <div class="card course-box"> 
                      <div class="card-body"> 
                        <div class="course-widget"> 
                          <div class="course-icon" > 
                              <i class="fa fa-check-square-o" style="color:white;"></i>
                          </div>
                          <div> 
                            <h4 class="mb-0"><?php echo $this->Reff->formatuang2($shortlist->jml); ?></h4><span class="f-light"> Shortlist  </span> <br>
                            <small> Total Madrasah yang masuk dalam kriteria shortlist dan akan ditetapkan <br> didalam  SK sebagai penerima BKBA setelah tahapan konfirmasi </small>
                            
                          </div>
                        </div>
                      </div>
                      <ul class="square-group">
                        <li class="square-1 warning"></li>
                        <li class="square-1 primary"></li>
                        <li class="square-2 warning1"></li>
                        <li class="square-3 danger"></li>
                        <li class="square-4 light"></li>
                        <li class="square-5 warning"></li>
                        <li class="square-6 success"></li>
                        <li class="square-7 success"></li>
                      </ul>
                    </div>
              </div>

            <!--   <div class="col-xl-12">
                  <div class="card course-box"> 
                      <div class="card-body"> 
                        <div class="course-widget"> 
                          <div class="course-icon" > 
                              <i class="fa fa-check-square-o" style="color:white;"></i>
                          </div>
                          <div> 
                            <h4 class="mb-0"><?php echo number_format($selisih->jml,2); ?> %</h4><span class="f-light">    </span> <br>
                            <small> Rata Rata Selisih Hasil Visitasi  </small>
                            
                          </div>
                        </div>
                      </div>
                      <ul class="square-group">
                        <li class="square-1 warning"></li>
                        <li class="square-1 primary"></li>
                        <li class="square-2 warning1"></li>
                        <li class="square-3 danger"></li>
                        <li class="square-4 light"></li>
                        <li class="square-5 warning"></li>
                        <li class="square-6 success"></li>
                        <li class="square-7 success"></li>
                      </ul>
                    </div>
              </div> -->

              

                          
   
  <figure class="highcharts-figure">
    <div id="container"></div>
  
</figure>  



<div class="row">
  <div class="col-lg-12 col-md-12">
  <div class="card">
    <div class="card-header card-header-danger">
      <h4 class="card-title">Histori Login   </h4>
     
    </div>
    <div class="card-body table-responsive">
    
        <div class="table-responsive">
                    <table class="table table-bordered table-striped  " id="datatableTable" width="99%">
                        <thead class="bg-blue">
                            <tr>
                                <th width="2px">NO</th>
                                
                                <th>ROLE  </th>
                                <th>KETERANGAN </th>
                                <th>WAKTU </th>
                                
                                
                            </tr>
                        </thead>
                       
                        <tbody>
                            
                            
                        </tbody>
                    </table>
                </div>
                
                
      
    </div>
  </div>
</div>
</div>


    
<script type="text/javascript">
var dataTable = $('#datatableTable').DataTable( {
            "processing": true,
            "language": {
            "processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
              "oPaginate": {
                "sFirst": "Halaman Pertama",
                "sLast": "Halaman Terakhir",
                 "sNext": "Selanjutnya",
                 "sPrevious": "Sebelumnya"
                 },
            "sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
             "sInfoEmpty": "Tidak ada data yang di tampilkan",
               "sZeroRecords": "Data kosong",
               "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
        },
        
        "serverSide": true,
        "searching": false,
        "responsive": false,
        "lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
         
         "sPaginationType": "full_numbers",
         "dom": 'Blfrtip',
        "buttons": [
        
                 
                {
                extend: 'excelHtml5',
                exportOptions: {
                  columns: [ 0,1,2,3]
                },
               
                
                }
        ],
        
        "ajax":{
            url :"<?php echo site_url("pusat/grid"); ?>", 
            type: "post", 
            "data": function ( data ) {
            
            
        
    
        }
            
        },
        "rowCallback": function( row, data ) {
            
            
        }
    } );
    
    



    



</script>	
              
              
         
              
              
              

<?php 
                  
$mi1  = $this->db->query("select count(id) as jml from madrasahedm where jenjang='mi'")->row();
$mts1  = $this->db->query("select count(id) as jml from madrasahedm where jenjang='mts'")->row();
$ma1  = $this->db->query("select count(id) as jml from madrasahedm where jenjang='ma'")->row();


             
$mi2  = $this->db->query("select count(id) as jml from madrasahedm where jenjang='mi' and longlist=1")->row();
$mts2  = $this->db->query("select count(id) as jml from madrasahedm where jenjang='mts' and longlist=1")->row();
$ma2  = $this->db->query("select count(id) as jml from madrasahedm where jenjang='ma' and longlist=1")->row();

             
$mi3  = $this->db->query("select count(id) as jml from madrasahedm where jenjang='mi' and shortlist=1")->row();
$mts3  = $this->db->query("select count(id) as jml from madrasahedm where jenjang='mts' and shortlist=1")->row();
$ma3  = $this->db->query("select count(id) as jml from madrasahedm where jenjang='ma' and shortlist=1")->row();

?>
              
<script type="text/javascript">
Highcharts.chart('container', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Jenjang Pendidikan '
    },
    subtitle: {
        text: 'Calon Penerima Bantuan berdasarkan Jenjang Pendidikan'
    },
    xAxis: {
        categories: ['MI', 'MTs', 'MA'],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah ',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' millions'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: 0,
       
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Calon Penerima',
        data: [<?php echo $mi1->jml; ?>, <?php echo $mts1->jml; ?>,<?php echo $ma1->jml; ?>]
    }, {
        name: 'Daftar Panjang (Longlist)',
        data: [<?php echo $mi2->jml; ?>, <?php echo $mts2->jml; ?>,<?php echo $ma2->jml; ?>]
    }, {
        name: 'Daftar Pendek (Shortlist)',
        data: [<?php echo $mi3->jml; ?>, <?php echo $mts3->jml; ?>,<?php echo $ma3->jml; ?>]
    }]
});
</script>
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              </div>


</div>