<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head>
		<title><?php echo $title; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="shortcut icon" href="<?php echo base_url(); ?>__statics/img/logo.png">	
		
       	
		<link href="<?php echo base_url(); ?>__statics/tema/versi2/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>__statics/tema/versi2/style.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>__statics/tema/css/custom.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>__statics/fa/css/font-awesome.min.css">	
		<link href="<?php echo base_url(); ?>__statics/js/alertify/css/alertify.css" rel="stylesheet">
		<link href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
		<link href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
		<link href="<?php echo base_url(); ?>__statics/js/alertify/css/alertify.css" rel="stylesheet">
		<script src="<?php echo base_url(); ?>__statics/tema/plugins/global/plugins.bundle1894.js?v=7.1.9"></script>

		<script src="<?php echo base_url(); ?>__statics/js/proses.js"></script>
		<script src="<?php echo base_url(); ?>__statics/js/alert.js"></script>
		<script src="<?php echo base_url(); ?>__statics/js/alertify/alertify.js"></script>
		<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" type="text/javascript"></script>
		<script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
		<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
		<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript"></script>
		<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js" type="text/javascript"></script>
		<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js" type="text/javascript"></script>
		<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.colVis.min.js" type="text/javascript"></script>

		<script src="https://code.highcharts.com/highcharts.js"></script>
		<script src="https://code.highcharts.com/highcharts-more.js"></script>
		<script src="https://code.highcharts.com/modules/exporting.js"></script>
		<script src="https://code.highcharts.com/modules/export-data.js"></script>
		<script src="https://code.highcharts.com/modules/accessibility.js"></script>

		
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed aside-enabled aside-fixed">
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="page d-flex flex-row flex-column-fluid">
				<!--begin::Aside-->
				<?php $this->load->view("page_menu"); ?>
				<!--end::Aside-->
				<!--begin::Wrapper-->
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					<!--begin::Header-->
					<?php $this->load->view("navbar"); ?>
					<!--end::Header-->
					<!--begin::Content-->
					<div id="kontendefault"><?php $this->load->view($konten); ?></div>
					<!--end::Content-->
					<!--begin::Footer-->
					
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Root-->
		<!--begin::Drawers-->
		<!--begin::Activities drawer-->
		
		<!--end::Modals-->
		<!--begin::Scrolltop-->
		<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
			<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
			<span class="svg-icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
					<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
				</svg>
			</span>
			<!--end::Svg Icon-->
		</div>
		<!--end::Scrolltop-->
		<script src="<?php echo base_url(); ?>__statics/tema/plugins/custom/prismjs/prismjs.bundle1894.js?v=7.1.9"></script>
		<script src="<?php echo base_url(); ?>__statics/tema/js/pages/widgets1894.js?v=7.1.9"></script>
		<script src="<?php echo base_url(); ?>__statics/tema/versi2/scripts.bundle.js"></script>

		


		<script>
   
		
		$(document).on("click","#keluar",function(){
	  
				  var base_url = "<?php echo base_url(); ?>";
				  
				//  alertify.confirm("Anda akan keluar dari aplikasi, apakah anda yakin?",function(){
					  
						   $.post("<?php echo site_url("pusat/logout"); ?>",function(data){
							   
							   location.href= base_url;
							   
						   });
				//	});
				  
				  
				  
			  })
			  
		$(document).off("click",".menuajax").on("click",".menuajax",function (event, messages) {
	           event.preventDefault()
			   var url = $(this).attr("href");
			   var title = $(this).attr("title");
			  
			 
				   
				   
			    $("li").siblings().removeClass('active');
			    $(this).parent().addClass('active');
			    
				
			  	  
			   $("#kontendefault").html('....');
		   
			  $.post(url,{ajax:"yes"},function(data){
				  
				
				  history.replaceState(title, title, url);
				  $('title').html(title);
				
				  $("#kontendefault").html(data);
				 
				 
				 
			  })
		  })
  </script>
		
	</body>
	<!--end::Body-->
</html>