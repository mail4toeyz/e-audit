
<style>

.table-scroll {
    position: relative;
    width:100%;
    z-index: 1;
    margin: auto;
    overflow: scroll;
    height: 90vh;
}
.table-scroll table {
    width: 100%;
    min-width: 100px;
    margin: auto;
    border-collapse: separate;
    border-spacing: 0;
}
.table-wrap {
    position: relative;
}
.table-scroll th,
.table-scroll td {
    padding: 5px 10px;
    border: 1px solid #000;
    
    vertical-align: top;
    text-align: center;
}
.table-scroll thead th {
    background: #333;
    color: #fff;
    position: -webkit-sticky;
    position: sticky;
    top: 0;
}
th.sticky {
    position: -webkit-sticky;
    position: sticky;
    left: 0;
    z-index: 2;
    background: #ccc;
}
thead th.sticky {
    z-index: 5;
}
.table-scroll thead tr:nth-child(2) th {
    top: 30px;
}

						</style>
<div class="table-responsive table-scroll ">
                              
                              <table class="table table-condensed table-bordered table-hover  " id="datatableTable" >
                                  <thead>
                                      <tr>
                                          <th rowspan="2"> NO </th>
                                          <th rowspan="2"> # </th>
                                          <th rowspan="2"> PROVINSI/KABKOTA </th>
                                          <th colspan="2"> Longlist </th>
                                          <th colspan="2">  Bantuan Kinerja  </th>
                                          <th colspan="2">  Bantuan 	Afirmasi  </th>
                                          <th rowspan="2"> Total </th>
                                          
                                  </tr>
                                  <tr>

                                    <th> Calon </th>
                                    <th> Longlist </th>

                                    <th> Kuota </th>
                                    <th> Shortlist </th>


                                  
                                    <th> Kuota </th>
                                    <th> Shortlist </th>

                                  </tr>
                      </thead>
                          <tbody>

                              <?php 
                              $provinsi = $this->db->query(" select * from provinsi where (kuota_kinerja+kuota_afirmasi) >0")->result();
                              $no_prov=1;
                              $tahun    = $this->Reff->tahun();

                              $jml=0;
                              $totalcalon=0;
                              $totallonglist=0;
                              $kinerjakuota=0;
                              $kinerjashort=0;
                              $afirmasikuota=0;
                              $afirmasishort=0;
                               foreach($provinsi as $prov){
                                   

                                   $calon    = $this->db->query("select count(id) as jml from madrasahedm where blacklist=0 and provinsi_id='{$prov->id}' and tahun='{$tahun}'")->row();
                                   $longlist    = $this->db->query("select count(id) as jml from madrasahedm where longlist=1 and provinsi_id='{$prov->id}' and tahun='{$tahun}'")->row();
                                   $kinerjashortlist = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and bantuan=1 and provinsi_id='{$prov->id}' and tahun='{$tahun}'")->row();
                                   $afirmasishortlist = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and bantuan=2 and provinsi_id='{$prov->id}' and tahun='{$tahun}'")->row();
                                   $totalcalon = $totalcalon+$calon->jml;
                                   $totallonglist = $totallonglist+$longlist->jml;
                                   $kinerjakuota = $kinerjakuota+$prov->kuota_kinerja;
                                   $kinerjashort = $kinerjashort+$kinerjashortlist->jml;
                                   $afirmasikuota = $afirmasikuota+$prov->kuota_afirmasi;
                                   $afirmasishort = $afirmasishort+$afirmasishortlist->jml;
                                   $jml = $jml + ($prov->kuota_kinerja+$prov->kuota_afirmasi); 
                                  ?>

                                  <tr style="font-weight:bold">
                                          <th> <?php echo $no_prov++; ?> </th>
                                          <th> # </th>
                                          <th> <?php echo $prov->nama; ?> </th>
                                          <th> <?php echo $calon->jml; ?>  </th>
                                          <th> <?php echo $longlist->jml; ?>  </th>
                                          <th> <?php echo $prov->kuota_kinerja; ?>  </th>
                                          <th> <?php echo $kinerjashortlist->jml; ?>  </th>
                                          <th> <?php echo $prov->kuota_afirmasi; ?>  </th>
                                          <th> <?php echo $afirmasishortlist->jml; ?>   </th>
                                          <th> <?php echo $prov->kuota_kinerja+$prov->kuota_afirmasi; ?>   </th>
                                          
                                  </tr>

                                  <?php 

                               
                              
                          }


                              ?>


                              

                          </tbody>
                             <tr>
                                         <th> # </th>
                                         <th>#</th>
                                         <th>Total</th>
                                         <th><?php echo $totalcalon; ?></th>
                                         <th><?php echo $totallonglist; ?></th>
                                         <th><?php echo $kinerjakuota; ?></th>
                                         <th><?php echo $kinerjashort; ?></th>
                                         <th><?php echo $afirmasikuota; ?></th>
                                         <th><?php echo $afirmasishort; ?></th>
                                         <th><?php echo $jml; ?></th>
                                       
                                         
                                        
                                 </tr>

                              </table>

                      </div>