
<div id="showform"></div>


<div class="col-md-12">
		
				       <div class="card">
						<div class="card-header card-header-success">
						  <h4 class="card-title">Bank Soal Ujian </h4>
						  <p class="card-category"> Dibawah ini adalah bank soal untuk pelaksanaan Ujian   </p>
						</div>
					  
						 <div class="card-body">
						 <button type="button" target="#loadform" url="<?php echo site_url("banksoal/form"); ?>"  data-toggle="modal" data-target="#defaultModal" data-color="cyan" class="btn btn-success btn-sm addmodal"><span class="fa fa-plus"></span> TAMBAH BANK SOAL </button>

					    
						
							<div class="table-responsive">
                                <table class="table table-bordered table-striped  " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
                                        <tr>
                                            <th width="2px">NO</th>
                                            
                                         
                                            <th>NAMA UJIAN   </th>
                                            <th>JUMLAH    </th>
                                            <th>WAKTU    </th>
                                            <th>AKTIVASI    </th>
                                            
                                            <th>AKSI </th>
                                           
                                           
                                            
                                        </tr>
                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                         </div>
                         </div>
                 
 </div>
   <style>
   .modal-lg {
    max-width: 95% !important;
    }

</style>
  <div id="defaultModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog ">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body" id="loadform" style="color:black">
        <p>Loading...</p>
      </div>
      
    </div>

  </div>
</div>

      
<div id="analisismodal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body" id="loadanalisis" style="color:black">
        <p>Loading...</p>
      </div>
      
    </div>

  </div>
</div>
	
	
<script type="text/javascript">

$(document).off("click",".aktivasi").on("click",".aktivasi",function(){
	            
				
				var tmujian_id = $(this).attr("tmujian_id");
				if($(this).is(":checked")){
							 var status =1;
							 
						}else{
	   
							var status =0;
							
				 }
  
				alertify.confirm("Anda akan mengaktifkan ujian ini ? ",function(){
  
			   
	   
				  $.post("<?php echo site_url('banksoal/aktivasi'); ?>",{tmujian_id:tmujian_id,status:status},function(){
	   
					alertify.success("Berhasil dilakukan perubahan aktivasi");
					location.reload();
				  })
  
  
				})
				   
				 
	 });



  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
						
							text:'<font color:"black">Cetak Excel</font>',
							
							},
							
							
							
							
						
							{
							extend: 'colvis',
							
							text:' Pengaturan Kolom ',
							
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("banksoal/grid"); ?>", 
						type: "post", 
						"data": function ( data ) {
					     	data.keyword = $("#keyword").val();
						  
						
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
				
			
			 
			  $(document).off("click",".truncate").on("click",".truncate",function(){
	   
					var id = $(this).attr("tmujian_id");
					alertify.confirm("Apakah Anda yakin akan menghapus seluruh Soal ?",function(){
						$.post("<?php echo site_url("banksoal/truncate"); ?>",{id:id},function(data){
							
							dataTable.ajax.reload(null,false);	
						})

						})
						
					
					});


					$(document).off("click",".buatujian").on("click",".buatujian",function(){
						
						var id = $(this).attr("tmujian_id");
							$.post("<?php echo site_url("teacherujian/create"); ?>",{id:id},function(data){
								
								$("#showform").html(data);
								
							})
							
						
						});


			  $(document).on("click","#cancelll",function(){
				$("#showform").html("");
					dataTable.ajax.reload(null,false);	
					
				});

			  
				
	


</script>
				