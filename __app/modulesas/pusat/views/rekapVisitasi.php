
<style>

.table-scroll {
    position: relative;
    width:100%;
    z-index: 1;
    margin: auto;
    overflow: scroll;
    height: 90vh;
}
.table-scroll table {
    width: 100%;
    min-width: 100px;
    margin: auto;
    border-collapse: separate;
    border-spacing: 0;
}
.table-wrap {
    position: relative;
}
.table-scroll th,
.table-scroll td {
    padding: 5px 10px;
    border: 1px solid #000;
    
    vertical-align: top;
    text-align: center;
}
.table-scroll thead th {
    background: #333;
    color: #fff;
    position: -webkit-sticky;
    position: sticky;
    top: 0;
}
th.sticky {
    position: -webkit-sticky;
    position: sticky;
    left: 0;
    z-index: 2;
    background: #ccc;
}
thead th.sticky {
    z-index: 5;
}
.table-scroll thead tr:nth-child(2) th {
    top: 30px;
}

						</style>

<button onclick="btn_export()" class="btn btn-default">
    Cetak Excel 
  </button>
<div class="table-responsive table-scroll ">
                              
                              <table class="table table-condensed table-bordered table-hover  " id="datatableTable" >
                                  <thead>
                                      <tr>
                                          <th rowspan="2"> Kode </th>
                                          <th rowspan="2"> # </th>
                                          <th rowspan="2"> PROVINSI/KABKOTA </th>
                                        
                                          <th colspan="3"> Status Visitasi </th>
                                          <th colspan="2">  Bantuan Kinerja  </th>
                                          <th colspan="2">  Bantuan 	Afirmasi  </th>
                                          <th rowspan="2"> ASESOR AKTIVASI </th>
                                          
                                  </tr>
                                  <tr>

                                    <th> Sudah Visitasi </th>
                                    <th> Belum Visitasi </th>
                                    <th> Persentase </th>

                                    <th> Kuota </th>
                                    <th> Shortlist </th>


                                  
                                    <th> Kuota </th>
                                    <th> Shortlist </th>

                                  </tr>
                      </thead>
                          <tbody>

                              <?php 
                              $kinerjakuota=0;
                              $afirmasikuota=0;
                              $provinsi = $this->db->query("select * from provinsi  where (kuota_kinerja+kuota_afirmasi) !=0")->result();
                              $no_prov=1;
                              $tahun    = $this->Reff->tahun();

                              $totalcalon=0;
                              $totallonglist=0;
                              
                              $kinerjashort=0;
                             
                              $afirmasishort=0;
                               foreach($provinsi as $prov){
                                $kinerjakuota = $kinerjakuota + $prov->kuota_kinerja;
                                $afirmasikuota = $afirmasikuota + $prov->kuota_afirmasi;

                                   $asesor           = $this->db->query("select count(id) as jml from asesor where provinsi_id='{$prov->id}'")->row();
                                   $sudahVisitasi    = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and provinsi_id='{$prov->id}' and tahun='{$tahun}' and nsm IN(SELECT nsm FROM conf_bkba where bersedia_menerima ='Ya') and id IN(select madrasah_id from tr_persyaratan where persyaratan_id=5)")->row();
                                   $kinerjashortlist = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and bantuan=1 and provinsi_id='{$prov->id}' and tahun='{$tahun}' and nsm IN(SELECT nsm FROM conf_bkba where bersedia_menerima ='Ya')")->row();
                                   $afirmasishortlist = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and bantuan=2 and provinsi_id='{$prov->id}' and tahun='{$tahun}' and nsm IN(SELECT nsm FROM conf_bkba where bersedia_menerima ='Ya')")->row();
                                   $jmlShortlist      = $kinerjashortlist->jml+$afirmasishortlist->jml;


                                   $jmlShortlist      = $kinerjashortlist->jml+$afirmasishortlist->jml;

                                

                                   $totalshortlist    = $kinerjashortlist->jml + $afirmasishortlist->jml;
                                  
                                  
                                 

                                 $totalcalon = $totalcalon+$sudahVisitasi->jml;
                                 $totallonglist = $totallonglist+ ($jmlShortlist-$sudahVisitasi->jml);
                                
                                 $kinerjashort = $kinerjashort+$kinerjashortlist->jml;
                                
                                
                                 $afirmasishort = $afirmasishort+$afirmasishortlist->jml;
                                 $persentase    = ($sudahVisitasi->jml/ ($jmlShortlist)) * 100;
                                 
                                 ?>

                                  <tr style="font-weight:bold">
                                          <th> <?php echo $prov->id; ?> </th>
                                          <th> Kode </th>
                                          <th> <?php echo $prov->nama; ?> </th>
                                          
                                          <th> <?php echo $sudahVisitasi->jml; ?>  </th>
                                          <th> <?php echo $jmlShortlist-$sudahVisitasi->jml; ?>  </th>
                                          <th> <?php echo number_format($persentase,2); ?> %  </th>

                                          <th> <?php echo $prov->kuota_kinerja; ?>  </th>
                                          <th> <?php echo $kinerjashortlist->jml; ?>  </th>
                                          <th> <?php echo $prov->kuota_afirmasi; ?>   </th>
                                          <th> <?php echo $afirmasishortlist->jml; ?>   </th>
                                          <th> <?php echo $asesor->jml; ?> </th>
                                  </tr>

                                  <?php 
                                  $jenis = $this->input->get_post("jenis",true);
                                if(empty($jenis)){
                               
                              $kota = $this->db->query(" select * from kota where  provinsi_id='{$prov->id}'")->result();
                          
                              
                               $nokot=1;
                               foreach($kota as $rkot){
                                  $kinerjashortlist = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and bantuan=1 and kota_id='{$rkot->id}' and tahun='{$tahun}' and nsm IN(SELECT nsm FROM conf_bkba where bersedia_menerima ='Ya')")->row();
                                   $afirmasishortlist = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and bantuan=2 and kota_id='{$rkot->id}' and tahun='{$tahun}' and nsm IN(SELECT nsm FROM conf_bkba where bersedia_menerima ='Ya')")->row();
                                  
                                   $sudahVisitasi    = $this->db->query("select count(id) as jml from madrasahedm where shortlist=1 and kota_id='{$rkot->id}' and tahun='{$tahun}' and nsm IN(SELECT nsm FROM conf_bkba where bersedia_menerima ='Ya') and id IN(select madrasah_id from tr_persyaratan where persyaratan_id=5)")->row();
                                 
                                   $jmlShortlist      = $kinerjashortlist->jml+$afirmasishortlist->jml;

                                

                                   $totalshortlist    = $kinerjashortlist->jml + $afirmasishortlist->jml;
                                  
                                  
                                 

                                 $totalcalon = $totalcalon+$sudahVisitasi->jml;
                                 $totallonglist = $totallonglist+ ($jmlShortlist-$sudahVisitasi->jml);
                                
                                 $kinerjashort = $kinerjashort+$kinerjashortlist->jml;
                                
                                
                                 $afirmasishort = $afirmasishort+$afirmasishortlist->jml;
                                 $persentase    = ($sudahVisitasi->jml/ ($jmlShortlist)) * 100;
                               ?>

                                 <tr>
                                         <th> # </th>
                                         <th> <?php echo $rkot->id; ?> </th>
                                         <th> <?php echo $rkot->nama; ?> </th>
                                         <th> <?php echo $sudahVisitasi->jml; ?>  </th>
                                         <th> <?php echo $jmlShortlist-$sudahVisitasi->jml; ?>  </th>
                                         <th> <?php echo number_format($persentase,2); ?> %  </th>
                                         <th> <?php echo $rkot->shortlist_kinerja; ?>  </th>
                                         <th> <?php echo $kinerjashortlist->jml; ?>  </th>
                                         <th> <?php echo $rkot->shortlist_afirmasi; ?>  </th>
                                         <th> <?php echo $afirmasishortlist->jml; ?>   </th>
                                         <th>  -  </th>
                                         
                                        
                                 </tr>

                                 <?php 

                              }
                          }

                        }

                              ?>


                              

                          </tbody>
                             <tr>
                                         <th> # </th>
                                         <th>#</th>
                                         <th>Total</th>
                                         <th><?php echo $totalcalon; ?></th>
                                         <th><?php echo $totallonglist; ?></th>
                                         <th><?php echo number_format((($totalcalon/($kinerjashort+$afirmasishort)) * 100),2); ?>%</th>
                                         <th><?php echo $kinerjakuota; ?></th>
                                         <th><?php echo $kinerjashort; ?></th>
                                         <th><?php echo $afirmasikuota; ?></th>
                                         <th><?php echo $afirmasishort; ?></th>
                                         <th> - </th>
                                       
                                         
                                        
                                 </tr>

                              </table>

                      </div>


		
				   
	
<script src="<?php echo base_url(); ?>__statics/js/excel/xlsx.extendscript.js"></script>
<script src="<?php echo base_url(); ?>__statics/js/excel/xlsx-style/xlsx.full.min.js"></script>

<script src="<?php echo base_url(); ?>__statics/js/excel/export.js"></script>
	
<script type="text/javascript">

function btn_export() {
        var table1 = document.querySelector("#datatableTable");
        var opt = {
            rowIndex: 4
        }; //开头空4行
        var sheet = XLSX2.utils.table_to_sheet(table1, opt);

  
     

        openDownloadDialog(sheet2blob(sheet), ' .Hasil Visitasi.xlsx');
    }

    </script>