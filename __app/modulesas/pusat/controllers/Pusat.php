<?php
ini_set('memory_limit', '-1');
defined('BASEPATH') OR exit('No direct script access allowed');

class Pusat extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		  if(!$this->session->userdata("admin_id")){
			    
				echo $this->Reff->sessionhabis();
				exit();
			
		  }
		  $this->load->model('M_dashboard','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('pusat/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['title']   = "Dashboard BKBA ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page_default',$data);
		
		 }else{
			 
			
		     $data['konten'] = "page_default";
			 
			 $this->_template($data);
		 }
	

	}

	public function infografis()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['title']   = "Infografis BKBA ";
	     if(!empty($ajax)){
					    
			 $this->load->view('infografis',$data);
		
		 }else{
			 
			
		     $data['konten'] = "infografis";
			 
			 $this->_template($data);
		 }
	

	}
	
	 
	 public function grid(){
		  error_reporting(0);
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		  $role = array("1"=>"Administrator","2"=>"Asesor");
		  
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			    
			   
				$no = $i++;
				$records["data"][] = array(
					$no,
			
              	
					
					$role[$val['status']],
					$val['keterangan'],
					$this->Reff->timeAgo($val['tanggal']),
					
		
				   
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	
 
	
	public function logout(){
		     $this->session->sess_destroy();
			 
		
			echo "yes";
		 
	 }

	 public function getDataEDM(){

		

		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://hasura.34.120.175.35.nip.io/v1/graphql',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS =>'{"query":"query long_list_afirmasi_2022($limit: Int!, $offset: Int!) \\n{\\n  rekap: mv_afirmasi_2022_aggregate(where: {persen: {_gte: \\"99\\"}}) {\\n    total: aggregate {\\n      count(columns: nsm)\\n    }\\n  }\\n  v_afirmasi_2022: mv_afirmasi_2022(where: {persen: {_gte: \\"99\\"}}, limit: $limit, offset: $offset) {\\n    tahun\\n    nama_madrasah\\n    nsm\\n    nama_provinsi\\n    kode_provinsi\\n    kode_kabkota\\n    nama_kabkota\\n    nilai_kedisiplinan\\n    skor_kedisiplinan\\n    nilai_pembiayaan\\n    skor_pembiayaan\\n    nilai_pengembangan_diri\\n    skor_pengembangan_diri\\n    nilai_proses_pembelajaran\\n    skor_proses_pembelajaran\\n    nilai_sarana_prasarana\\n    skor_sarana_prasarana\\n    nilai_skpm\\n    total_rencana_kegiatan\\n    total_rencana_pendapatan\\n    persen\\n  }\\n}","variables":{"limit":100000,"offset":0}}',
		CURLOPT_HTTPHEADER => array(
			'x-hasura-admin-secret: myadminsecretkeyPa55',
			'Content-Type: application/json'
		),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		

			

				$data = json_decode($response,true);
				//echo  "<pre>".print_r($data['data'])."<pre>"; exit();
				//print("<pre>".print_r($data,true)."</pre>"); exit();
             $no=0;
				//print_r($data['data']['v_afirmasi_2022']); exit();
			 $jenjang = array("101"=>"ra","111"=>"mi","121"=>"mts","131"=>"ma");

				  foreach($data['data']['v_afirmasi_2022'] as $row){

					$cek = $this->db->query("select count(id) as jml from madrasahedm where tahun='2022' and nsm='{$row['nsm']}'")->row();
					  if($cek->jml == 0){

								//echo $jenjang[substr($row['nsm'],0,3)]; exit();
								if(!empty($row['nilai_skpm'])){
								$no++;
										$this->db->set("tahap","bkba2022");
										$this->db->set("tahun","2022");
										$this->db->set("nama",$row['nama_madrasah']);
										$this->db->set("nsm",$row['nsm']);
										$this->db->set("npsn",$row['npsn']);
										$this->db->set("jenjang",$jenjang[substr($row['nsm'],0,3)]);
										$this->db->set("provinsi",$row['nama_provinsi']);
										$this->db->set("kota",$row['nama_kabkota']);
										$this->db->set("provinsi_id",$row['kode_provinsi']);
										$this->db->set("kota_id",str_replace(".","",$row['kode_kabkota']));
										$this->db->set("nilai_kedisiplinan",$row['nilai_kedisiplinan']);
										$this->db->set("skor_kedisiplinan",$row['skor_kedisiplinan']);

										$this->db->set("nilai_pembiayaan",$row['nilai_pembiayaan']);
										$this->db->set("skor_nilai_pembiayaan",$row['skor_pembiayaan']);

										$this->db->set("nilai_pengembangan_diri",$row['nilai_pengembangan_diri']);
										$this->db->set("skor_pengembangan_diri",$row['skor_pengembangan_diri']);

										

										$this->db->set("nilai_proses_pembelajaran",$row['nilai_proses_pembelajaran']);
										$this->db->set("skor_proses_pembelajaran",$row['skor_proses_pembelajaran']);

										$this->db->set("nilai_sarana_prasarana",$row['nilai_sarana_prasarana']);
										$this->db->set("skor_sarana_prasarana",$row['skor_sarana_prasarana']);

										$this->db->set("nilai_skpm",$row['nilai_skpm']);
										$this->db->set("total_rencana_kegiatan",$row['total_rencana_kegiatan']);
										$this->db->set("rencana_pendapatan ",$row['total_rencana_pendapatan']);
										$this->db->set("persen",$row['persen']);

										$this->db->insert("madrasahedm");
								}

					  }
					
					
				  }

				  echo $no."sukses";


	 }


	 public function getDataEDMKurang(){

		
		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://erkam-graphql.devel.nusatechstudio.com/v1/graphql',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS =>'{"query":"query long_list_afirmasi_2021 {\\n  v_afirmasi_2021(where: {persen: {_gte: \\"99\\"}}) {\\n    nama_madrasah\\n    nsm\\n  npsn\\n    nama_provinsi\\n    kode_provinsi\\n    kode_kabkota\\n    nama_kabkota\\n    nilai_kedisiplinan\\n    skor_kedisiplinan\\n    nilai_pembiayaan\\n    skor_pembiayaan\\n    nilai_pengembangan_diri\\n    skor_pengembangan_diri\\n    nilai_proses_pembelajaran\\n    skor_proses_pembelajaran\\n    nilai_sarana_prasarana\\n    skor_sarana_prasarana\\n    nilai_skpm\\n    total_rencana_kegiatan\\n    total_rencana_pendapatan\\n    persen\\n  }\\n}","variables":{}}',
		CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json',
			'x-hasura-admin-secret: myadminsecretkeyPa55'
		),
		));

		$response = curl_exec($curl);

		curl_close($curl);

		$data = json_decode($response,true);
        $kode_kabkota = array("11.02","11.09","11.11","11.13","33.14","35.73","51.03","73.26");
		
	 $no=0;
	
	 $jenjang = array("101"=>"ra","111"=>"mi","121"=>"mts","131"=>"ma");

		  foreach($data['data']['v_afirmasi_2021'] as $row){

			if (in_array($row['kode_kabkota'], $kode_kabkota))
			{

				
				$cek = $this->db->query("select count(id) as jml from madrasahedm where tahun='2021' and nsm='{$row['nsm']}'")->row();
				if($cek->jml == 0){

						//echo $jenjang[substr($row['nsm'],0,3)]; exit();
						if(!empty($row['nilai_skpm'])){
						$no++;
								$this->db->set("tahap","bkba2021");
								$this->db->set("tahun","2021");
								$this->db->set("nama",$row['nama_madrasah']);
								$this->db->set("nsm",$row['nsm']);
							//	$this->db->set("npsn",$row['npsn']);
								$this->db->set("jenjang",$jenjang[substr($row['nsm'],0,3)]);
								$this->db->set("provinsi",$row['nama_provinsi']);
								$this->db->set("kota",$row['nama_kabkota']);
								$this->db->set("provinsi_id",$row['kode_provinsi']);
								$this->db->set("kota_id",str_replace(".","",$row['kode_kabkota']));
								$this->db->set("nilai_kedisiplinan",$row['nilai_kedisiplinan']);
								$this->db->set("skor_kedisiplinan",$row['skor_kedisiplinan']);

								$this->db->set("nilai_pembiayaan",$row['nilai_pembiayaan']);
								$this->db->set("skor_nilai_pembiayaan",$row['skor_pembiayaan']);

								$this->db->set("nilai_pengembangan_diri",$row['nilai_pengembangan_diri']);
								$this->db->set("skor_pengembangan_diri",$row['skor_pengembangan_diri']);

								

								$this->db->set("nilai_proses_pembelajaran",$row['nilai_proses_pembelajaran']);
								$this->db->set("skor_proses_pembelajaran",$row['skor_proses_pembelajaran']);

								$this->db->set("nilai_sarana_prasarana",$row['nilai_sarana_prasarana']);
								$this->db->set("skor_sarana_prasarana",$row['skor_sarana_prasarana']);

								$this->db->set("nilai_skpm",$row['nilai_skpm']);
								$this->db->set("total_rencana_kegiatan",$row['total_rencana_kegiatan']);
								$this->db->set("rencana_pendapatan ",$row['total_rencana_pendapatan']);
								$this->db->set("persen",$row['persen']);

								$this->db->insert("madrasahedm");
						}

			  }
			}
			
			
		  }

		  echo $no."sukses";


}

	 // KUOTA BANTUAN 
	 public function kuota()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['title']   = "Kuota Bantuan BKBA ";
	     if(!empty($ajax)){
					    
			 $this->load->view('kuota',$data);
		
		 }else{
			 
			
		     $data['konten'] = "kuota";
			 
			 $this->_template($data);
		 }
	

	}

	public function rekapVisitasi()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['title']   = "Rekap Visitasi Bantuan BKBA ";
	     if(!empty($ajax)){
					    
			 $this->load->view('rekapVisitasi',$data);
		
		 }else{
			 
			
		     $data['konten'] = "rekapVisitasi";
			 
			 $this->_template($data);
		 }
	

	}


	public function get_pernyataan(){

		
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://madrasahkeren.com/v1/bkba/report-bkba-madrasah?tahun=2023&filter=1',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvbWFkcmFzYWgtYm9zLWJlLnRlc3RcL3YxXC9hdXRoXC9sb2dpbiIsImlhdCI6MTYwOTgxODAwMCwiZXhwIjoxNjA5ODIxNjAwLCJuYmYiOjE2MDk4MTgwMDAsImp0aSI6IkxHZHVLaGVHc2s3VTdIQ0YiLCJzdWIiOjMsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.P6WcoyCR61VMQ4i_uvVslsWzZ-okzwjgdCFtvN2hxZU'
		  ),
		));
		
		$response = curl_exec($curl);
		
		curl_close($curl);
		$data = json_decode($response,true);
		  foreach($data['data']['madrasah'] as $i=>$row){

			  if(!empty($row['bersedia_menerima'])){

				 	$cekData = $this->db->query("SELECT nsm from conf_bkba WHERE nsm='{$row['nsm']}'")->num_rows();
					 if($cekData==0){

						$this->db->set("nsm",$row['nsm']);
						$this->db->set("nama_lembaga",$row['nama_lembaga']);
						$this->db->set("tahun",$row['tahun']);
						$this->db->set("tanggal_konfirmasi",$row['tanggal_konfirmasi']);
						$this->db->set("bersedia_menerima",$row['bersedia_menerima']);
						$this->db->set("path_surat_pernyataan",$row['path_surat_pernyataan']);
						$this->db->set("jenis_bantuan",$row['jenis_bantuan']);
						$this->db->set("alasan_tidak_bersedia",$row['alasan_tidak_bersedia']);
						$this->db->insert("conf_bkba");

						
						echo $row['nsm']."-".$row['nama_lembaga']."-".$row['bersedia_menerima']." Data Baru <br>"; 


					 }else{

						
						$this->db->set("nama_lembaga",$row['nama_lembaga']);
						$this->db->set("tahun",$row['tahun']);
						$this->db->set("tanggal_konfirmasi",$row['tanggal_konfirmasi']);
						$this->db->set("bersedia_menerima",$row['bersedia_menerima']);
						$this->db->set("path_surat_pernyataan",$row['path_surat_pernyataan']);
						$this->db->set("jenis_bantuan",$row['jenis_bantuan']);
						$this->db->set("alasan_tidak_bersedia",$row['alasan_tidak_bersedia']);
						$this->db->where("nsm",$row['nsm']);
						$this->db->update("conf_bkba");

						echo $row['nsm']."-".$row['nama_lembaga']."-".$row['bersedia_menerima']." Data Update <br>"; 



					 }
			

			  }
				
		  }
		
	}

	public function pindahDataEDM(){

		$data = $this->db->query("SELECT * from edmerkamseptember")->result_array();
		foreach($data as $row){



			$cek = $this->db->query("select count(id) as jml from madrasahedm where  nsm='{$row['nsm']}'")->row();
			  if($cek->jml == 0){

						//echo $jenjang[substr($row['nsm'],0,3)]; exit();
						//if(!empty($row['nilai_skpm'])){
						
								$this->db->set("tahap","bkba2023");
								$this->db->set("tahun","2023");
								$this->db->set("nama",$row['nama']);
								$this->db->set("nsm",$row['nsm']);
								$this->db->set("npsn",$this->Reff->get_kondisi(array("nsm"=>$val['nsm']),"datamadrasahemis_csv","npsn"));
								$this->db->set("jenjang",$row['jenjang']);
								$this->db->set("provinsi",$row['nama_provinsi']);
								$this->db->set("kota",$row['nama_kabkota']);
								
								$this->db->set("provinsi_id",$row['kode_provinsi_transaksi']);
								$this->db->set("kota_id",$row['kode_kabkota_emis']);
								
								 $this->db->set("nilai_kedisiplinan",str_replace(",",".",$row['skor_kedisiplinan']));
								 $this->db->set("skor_kedisiplinan",str_replace(",",".",$row['skor_kedisiplinan']));
								

								$this->db->set("nilai_pembiayaan",str_replace(",",".",$row['nilai_pembiayaan']));
								$this->db->set("skor_nilai_pembiayaan",str_replace(",",".",$row['nilai_pembiayaan']));
							

								$this->db->set("nilai_pengembangan_diri",str_replace(",",".",$row['skor_pengembangan_diri']));
								$this->db->set("skor_pengembangan_diri",str_replace(",",".",$row['skor_pengembangan_diri']));
							
								$this->db->set("nilai_proses_pembelajaran",str_replace(",",".",$row['skor_proses_pembelajaran']));
								$this->db->set("skor_proses_pembelajaran",str_replace(",",".",$row['skor_proses_pembelajaran']));
							
								$this->db->set("nilai_sarana_prasarana",str_replace(",",".",$row['nilai_sarana_prasarana']));
								$this->db->set("skor_sarana_prasarana",str_replace(",",".",$row['nilai_sarana_prasarana']));

								$this->db->set("nilai_skpm",str_replace(",",".",$row['nilai_skpm']));

								$this->db->set("total_rencana_kegiatan",str_replace(",",".",str_replace(",",".",$row['rencana_pendapatan'])));
								$this->db->set("rencana_pendapatan",str_replace(",",".",str_replace(",",".",$row['belanja_pendapatan'])));
									//$this->db->set("persen",$row['persen']);

								$this->db->insert("madrasahedm");
						//}

			  }
			
			
		  }
	}
	 
}
