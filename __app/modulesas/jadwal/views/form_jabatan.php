<input type="hidden" id="kegiatan_id" value="<?php echo $kegiatan_id; ?>">

<div class="row p-2 ">

  <div class="col-md-4">
    <label for="inputName5" class="form-label"> Penanggung Jawab</label>
    <select class="form-control jabatanPilihan" name="penanggung_jawab" id="penanggung_jawab" data-search="true">
      <option>- Pilih Penanggung Jawab -</option>
      <?php
      $unit_kerja1 = $this->db->get("unit_kerja")->result();
      foreach ($unit_kerja1 as $unit1) {
      ?>

        <optgroup label="<?php echo $unit1->nama; ?>">
          <?php
          $penanggungjawab = $this->db->query("SELECT * from pegawai_simpeg where KODE_SATKER_3='{$unit1->kode}' order by NAMA_LENGKAP  ASC")->result();
          foreach ($penanggungjawab as $row) {
          ?>
            <option value="<?php echo $row->id; ?>" <?php if (isset($data)) {
                                                      echo ($data->penanggung_jawab == $row->id) ? "selected" : "";
                                                    }  ?>><?php echo $row->NIP_BARU; ?> - <?php echo $row->NAMA_LENGKAP; ?></option>
          <?php
          }
          ?>
        </optgroup>

      <?php
      }
      ?>
      <!-- </datalist> -->
    </select>
  </div>


  <div class="col-md-4">
    <label for="inputName5" class="form-label"> Pengendali Mutu</label>
    <select class="form-control jabatanPilihan" name="pengendali_mutu" id="pengendali_mutu" data-search="true">
      <option>- Pilih Pengendali Mutu -</option>
      <?php
      $unit_kerja2 = $this->db->get("unit_kerja")->result();
      foreach ($unit_kerja2 as $unit2) {
      ?>

        <optgroup label="<?php echo $unit2->nama; ?>">
          <?php
          $pengendalimutu = $this->db->query("SELECT * from pegawai_simpeg where KODE_SATKER_3='{$unit2->kode}'  order by NAMA_LENGKAP  ASC")->result();
          foreach ($pengendalimutu as $row) {
          ?>
            <option value="<?php echo $row->id; ?>" <?php if (isset($data)) {
                                                      echo ($data->pengendali_mutu == $row->id) ? "selected" : "";
                                                    }  ?>><?php echo $row->NIP_BARU; ?> - <?php echo $row->NAMA_LENGKAP; ?></option>
          <?php
          }
          ?>
        </optgroup>

      <?php
      }
      ?>
    </select>
  </div>

  <div class="col-md-4">
    <label for="inputName5" class="form-label"> Pengendali Teknis</label>
    <select class="form-control jabatanPilihan" name="pengendali_teknis" id="pengendali_teknis" data-search="true">
      <option>- Pilih Pengendali Teknis -</option>
      <?php
      $unit_kerja3 = $this->db->get("unit_kerja")->result();
      foreach ($unit_kerja3 as $unit3) {
      ?>

        <optgroup label="<?php echo $unit3->nama; ?>">
          <?php
          $pegendaliteknis = $this->db->query("SELECT * from pegawai_simpeg where KODE_SATKER_3='{$unit3->kode}'  order by NAMA_LENGKAP  ASC")->result();
          foreach ($pegendaliteknis as $row) {
          ?>
            <option value="<?php echo $row->id; ?>" <?php if (isset($data)) {
                                                      echo ($data->pengendali_teknis == $row->id) ? "selected" : "";
                                                    }  ?>><?php echo $row->NIP_BARU; ?> - <?php echo $row->NAMA_LENGKAP; ?></option>
          <?php
          }
          ?>
        </optgroup>

      <?php
      }
      ?>
    </select>
  </div>


</div>

<div class="row p-2 ">
  <div class="col-md-12">
    <label for="inputName5" class="form-label">Ketua Tim </label>
    <select class="form-control jabatanPilihan" name="ketua_tim" id="ketua_tim" data-search="true">
      <option>- Pilih Ketua Tim -</option>
      <?php
      $unit_kerja4 = $this->db->get("unit_kerja")->result();
      foreach ($unit_kerja4 as $unit4) {
      ?>

        <optgroup label="<?php echo $unit4->nama; ?>">
          <?php
          $ketuatim = $this->db->query("SELECT * from pegawai_simpeg where KODE_SATKER_3='{$unit4->kode}'  order by NAMA_LENGKAP  ASC")->result();
          foreach ($ketuatim as $row) {
          ?>
            <option value="<?php echo $row->id; ?>" <?php if (isset($data)) {
                                                      echo ($data->ketua == $row->id) ? "selected" : "";
                                                    }  ?>><?php echo $row->NIP_BARU; ?> - <?php echo $row->NAMA_LENGKAP; ?></option>
          <?php
          }
          ?>
        </optgroup>

      <?php
      }
      ?>
    </select>
  </div>
</div>


<div class="row p-2 ">
  <div class="col-md-12">
    <div class="form-group">
      <label class="col-sm-3 control-label no-padding-top" for="duallist"> Anggota </label>

      <div class="col-sm-12">
        <select multiple="multiple" size="10" name="auditor[]" id="duallist">
          <option value="" selected disabled> Dibawah ini adalah auditor yang terpilih sebagai anggota </option>

          <?php

          $auditor = $this->db->query("select * from pegawai_simpeg order by KODE_SATKER_3 asc")->result();

          foreach ($auditor as $r) {

            $sel = "";
            if (isset($data)) {
              $anggota = explode(",",$data->anggota);

              if (in_array($r->id, $anggota)) {
                $sel = "selected";
              }
            }

          ?>
            <option value="<?php echo $r->id; ?>" <?php echo $sel; ?>> <?php echo $r->NIP_BARU; ?> - <?php echo $r->NAMA_LENGKAP; ?> - <?php echo  $r->SATKER_3; ?></option>
          <?php
          }
          ?>

        </select>

        <div class="hr hr-16 hr-dotted"></div>
      </div>
    </div>


  </div>




</div>





<script>
  $(document).ready(function() {
    $('.jabatanPilihan').select2({
      width: '100%',
      cache: false
    });

    // $('#penanggung_jawab').selectstyle({
    //   width: 400,
    //   height: 300,
    //   theme: 'light',
    //   onchange: function(val) {
    //     val = jQuery('option:selected', this).text();
    //     self.next().text(val);
    //     alert(val);
    //   }
    // });
    // $('#pengendali_teknis').selectstyle({
    //   width: 400,
    //   height: 300,
    //   theme: 'light',
    //   onchange: function(val) {
    //     $('#pengendali_teknis').val(val);
    //     alert('aa');
    //   }
    // });
    // $('#pengendali_mutu').selectstyle({
    //   width: 400,
    //   height: 300,
    //   theme: 'light',
    //   onchange: function(val) {
    //     $('#pengendali_mutu').val(val);
    //   }
    // });
    // $('#ketua_tim').selectstyle({
    //   width: 400,
    //   height: 300,
    //   theme: 'light',
    //   onchange: function(val) {
    //     $('#ketua_tim').val(val);
    //   }
    // });

    $("#name").on('input', function() {
      var val = this.value;
      if ($('#allNames option').filter(function() {
          return this.value.toUpperCase() === val.toUpperCase();
        }).length) {
        //send ajax request
        t_val = this.value.split('-');
        t_id = $(this).attr('xid');
        $('#' + t_id).val(t_val[0]);
        alert(t_val[0]);

      }
    });

    var demo1 = $('select[name="auditor[]"]').bootstrapDualListbox({
      infoTextFiltered: '<span class="label label-purple label-lg">Terpilih</span>'
    });
    var container1 = demo1.bootstrapDualListbox('getContainer');
    container1.find('.btn').addClass('bxxxtn-white btn-info btn-bold');
  });
</script>