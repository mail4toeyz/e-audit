<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jadwal extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata("is_login")) {

			echo $this->Reff->sessionhabis();
			exit();
		}
		$this->load->model('M_jadwal', 'm');
	}

	function _template($data)
	{
		$this->load->view('dashboard/page_header', $data);
	}

	public function index()
	{




		$ajax            = $this->input->get_post("ajax", true);

		$data['title']   = "Data Jadwal Kegiatan ";
		if (!empty($ajax)) {

			$this->load->view('page', $data);
		} else {


			$data['konten'] = "page";

			$this->_template($data);
		}
	}

	public function grid()
	{


		$iTotalRecords = $this->m->grid(false)->num_rows();

		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);

		$records = array();
		$records["data"] = array();

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;

		$datagrid = $this->m->grid(true)->result_array();


		$i = ($iDisplayStart + 1);
		foreach ($datagrid as $val) {

			$anggota = "<ol>";
			$dataAnggota = explode(",",$val['anggota']);
			foreach ($dataAnggota as $r) {
				$anggota .= "<li>" . $this->Reff->get_kondisi(array("id" => $r), "pegawai_simpeg", "nama") . "</li>";
			}

			$anggota .= "</ol>";

			$satkerData = "<ol>";
			$satker =  explode(",", $val['satker_id']);
			foreach ($satker as $r) {
				$satkerData .= "<li>" . $this->Reff->get_kondisi(array("id" => $r), "satker", "nama") . "</li>";
			}

			$satkerData .= "</ol>";


			if ($val['status'] == 0) {
				$approval = '<span class="badge bg-danger"><i class="bi bi-exclamation-octagon me-1"></i> Draft </span>';
			} else {
				$approval = '<span class="badge bg-info"><i class="bi bi-exclamation-octagon me-1"></i> Belum disetujui</span>';
			}

			// if ($val['status_approval'] == 0) {
			// 	$approval = '<span class="badge bg-info"><i class="bi bi-exclamation-octagon me-1"></i> Belum disetujui</span>';
			// } 

			if ($val['status_approval'] == 1) {
				$approval = '<span class="badge bg-success"><i class="bi bi-check-circle me-"></i>  Disetujui</span>';
			}

			if ($val['status_approval'] == 2) {
				$approval = '<span class="badge bg-danger"><i class="bi bi-exclamation-octagon me-1"></i> Ditolak</span>';
			}

			if ($val['status_approval'] == 2 && $val['groups_id'] == 16) {
				$approval = '<span class="badge bg-warning"><i class="bi bi-exclamation-octagon me-1"></i> Revisi Sekretaris</span>';
			}




			$no = $i++;
			$records["data"][] = array(
				$no,


				$approval,
				$this->Reff->get_kondisi(array("id" => $val['jenis']), "jenis_sub", "nama"),

				$val['judul'],
				$this->Reff->get_kondisi(array("id" => $val['provinsi_id']), "provinsi", "nama"),
				$this->Reff->get_kondisi(array("id" => $val['kota_id']), "kota", "nama"),
				$satkerData,
				$this->Reff->formattanggalstring($val['tgl_mulai']) . " - " . $this->Reff->formattanggalstring($val['tgl_selesai']),

				$this->Reff->get_kondisi(array("id" => $val['penanggung_jawab']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['penanggung_jawab']), "pegawai_simpeg", "nama"),
				$this->Reff->get_kondisi(array("id" => $val['pengendali_mutu']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['pengendali_mutu']), "pegawai_simpeg", "nama"),
				$this->Reff->get_kondisi(array("id" => $val['pengendali_teknis']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['pengendali_teknis']), "pegawai_simpeg", "nama"),

				$this->Reff->get_kondisi(array("id" => $val['ketua']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['ketua']), "pegawai_simpeg", "nama"),
				$anggota,
				' 
					<div class="btn-group" role="group">
					<a class="btn btn-outline-primary btn-sm " datanya="' . $val['id'] . '" href="' . site_url("jadwal/create?kegiatan=" . $val['id'] . "") . '" >
                                    <i class="fa fa-pencil"></i>
                      </a>
					  <button type="button"  class="btn btn-outline-danger btn-sm hapus" datanya="' . $val['id'] . '" urlnya="' . site_url("jadwal/hapus") . '">
                                    <i class="fa fa-trash"></i>
					  </button>
					  </div>
					 
					  '






			);
		}

		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;

		echo json_encode($records);
	}

	public function create()
	{




		$ajax                = $this->input->get_post("ajax", true);
		$kegiatan            = $this->input->get_post("kegiatan", true);

		$data['title']   = "Buat Jadwal Kegiatan ";
		if (!empty($kegiatan)) {
			$data['data'] = $this->db->get_where("kegiatan", array("id" => $kegiatan))->row();
		}
		if (!empty($ajax)) {

			$this->load->view('page_create', $data);
		} else {


			$data['konten'] = "page_create";

			$this->_template($data);
		}
	}

	public function getSatker()
	{
		$provinsi_id  = $this->input->get_post("provinsi_id");
		$kota_id      = $this->input->get_post("kota_id");
		$provinsiKode = $this->Reff->get_kondisi(array("id" => $provinsi_id), "provinsi", "kode");
		$kotaKode 	  = $this->Reff->get_kondisi(array("id" => $kota_id), "kota", "kode");
		$where = " AND 1=1";
		if (!empty($kotaKode)) {
			$where .= " AND kota_id='{$kotaKode}' ";
		}
		$satker_kategori = $this->db->query("SELECT * from satker_kategori where id IN(SELECT kategori from satker where provinsi_id='{$provinsiKode}' $where )")->result();
		foreach ($satker_kategori as $satker) {
?>

			<optgroup label="<?php echo $satker->nama; ?>">
				<?php
				$satkerData = $this->db->query("SELECT * from satker where kategori='{$satker->id}' and provinsi_id='{$provinsiKode}' $where order by nama ASC")->result();
				foreach ($satkerData as $row) {
				?>
					<option value="<?php echo $row->id; ?>"><?php echo $row->kode; ?> - <?php echo $row->nama; ?></option>
				<?php
				}
				?>
			</optgroup>

<?php
		}
	}

	public function stepJabatan()
	{

		$this->form_validation->set_message('required', '{field} Wajib diisi. ');
		$this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
		$this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
		$this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
		$config = array(
			array('field' => 'f[jenis]', 'label' => 'Jenis Kegiatan  ', 'rules' => 'trim|required'),
			array('field' => 'f[judul]', 'label' => 'Judul   ', 'rules' => 'trim|required'),
			array('field' => 'f[provinsi_id]', 'label' => 'Provinsi   ', 'rules' => 'trim|required'),
			array('field' => 'f[tgl_mulai]', 'label' => 'Tanggal Mulai   ', 'rules' => 'trim|required'),
			array('field' => 'f[tgl_selesai]', 'label' => 'Tanggal Selesai   ', 'rules' => 'trim|required'),
			array('field' => 'satuankerja[]', 'label' => 'Satuan Kerja    ', 'rules' => 'trim|required'),
		);

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == true) {

			$f 			= $this->input->post("f");
			$satuankerja = implode(",", $this->input->post("satuankerja"));


			$this->db->where("jenis", $f['jenis']);
			$this->db->where("judul", $f['judul']);
			$this->db->where("provinsi_id", $f['provinsi_id']);
			$this->db->where("kota_id", $f['kota_id']);
			$this->db->where("satker_id", $satuankerja);
			$cek = $this->db->get("kegiatan")->row();
			if (is_null($cek)) {


				$this->db->set("anggaran", date("Y"));
				$this->db->set("satker_id", $satuankerja);
				$this->db->insert("kegiatan", $f);
				$data['kegiatan_id'] = $this->db->insert_id();
				$data['data'] = $this->db->get_where("kegiatan", array("id" => $data['kegiatan_id']))->row();
				$this->load->view("form_jabatan", $data);
			} else {

				$this->db->where("id", $cek->id);
				$this->db->set("satker_id", $satuankerja);
				$this->db->update("kegiatan", $f);
				$data['kegiatan_id'] = $cek->id;
				$data['data'] = $this->db->get_where("kegiatan", array("id" => $data['kegiatan_id']))->row();
				$this->load->view("form_jabatan", $data);
			}
		} else {

			echo  '<div class="alert alert-warning">' . validation_errors() . '</div>';
		}
	}

	public function stepPeriode()
	{
		$kegiatan_id        = $this->input->get_post('kegiatan_id');
		$penanggung_jawab  = $this->input->get_post('penanggung_jawab');
		$pengendali_mutu   = $this->input->get_post('pengendali_mutu');
		$pengendali_teknis  = $this->input->get_post('pengendali_teknis');
		$ketua_tim  = $this->input->get_post('ketua_tim');
		$auditor            = implode(",",$this->input->get_post('auditor'));

		// $trk = $this->db->query("SELECT * from kegiatan where id='{$kegiatan_id}' 
		// and penanggung_jawab = '{$penanggung_jawab}' 
		// and pengendali_mutu = '{$pengendali_mutu}' 
		// and pengendali_teknis = '{$pengendali_teknis}' 
		// and ketua = '{$ketua_tim}' 
		// and anggota = '{$auditor}' 
		// ")->result();

		// if (empty($trk)) {
		// 	$this->db->query("DELETE  FROM tr_kegiatanJabatan where kegiatan_id='{$kegiatan_id}'");
		// }

		// echo $penanggung_jawab . " - " . $pengendali_mutu . " - " . $pengendali_teknis;
		// die();

		$this->db->where("id", $kegiatan_id);
		$this->db->set("penanggung_jawab", $penanggung_jawab);
		$this->db->set("pengendali_mutu", $pengendali_mutu);
		$this->db->set("pengendali_teknis", $pengendali_teknis);
		$this->db->set("ketua", $ketua_tim);
		$this->db->set("anggota", $auditor);
		$this->db->update("kegiatan");

		// $trk = $this->db->query("SELECT * from tr_kegiatanJabatan where kegiatan_id='{$kegiatan_id}'")->result();
		// if (!empty($trk)) {
		// 	foreach ($trk as $row) {
		// 		if ($row->waktu == 0) {
		// 			$this->db->where("id", $row->id);
		// 			$this->db->set("waktu", 0);
		// 			$this->db->set("anggaran", 0);
		// 			$this->db->set("bst", 0);
		// 			$this->db->set("transport", 0);
		// 			$this->db->set("hotel", 0);
		// 			$this->db->set("uh", 0);
		// 			$this->db->set("representatif", 0);
		// 			$this->db->update("tr_kegiatanJabatan");
		// 		}
		// 	}
		// } else {
			$this->db->query("DELETE  FROM tr_kegiatanJabatan where kegiatan_id='{$kegiatan_id}'");
			if (!empty($penanggung_jawab)) {

				$this->db->set("kegiatan_id", $kegiatan_id);
				$this->db->set("jabatan", 1);
				$this->db->set("pejabat_id", $penanggung_jawab);
				$this->db->set("waktu", 0);
				$this->db->insert("tr_kegiatanJabatan");
			}

			if (!empty($pengendali_mutu)) {

				$this->db->set("kegiatan_id", $kegiatan_id);
				$this->db->set("jabatan", 3);
				$this->db->set("pejabat_id", $pengendali_mutu);
				$this->db->set("waktu", 0);
				$this->db->insert("tr_kegiatanJabatan");
			}

			if (!empty($pengendali_teknis)) {

				$this->db->set("kegiatan_id", $kegiatan_id);
				$this->db->set("jabatan", 4);
				$this->db->set("pejabat_id", $pengendali_teknis);
				$this->db->set("waktu", 0);
				$this->db->insert("tr_kegiatanJabatan");
			}

			if (!empty($ketua_tim)) {

				$this->db->set("kegiatan_id", $kegiatan_id);
				$this->db->set("jabatan", 2);
				$this->db->set("pejabat_id", $ketua_tim);
				$this->db->set("waktu", 0);
				$this->db->insert("tr_kegiatanJabatan");
			}

			$auditor  = $this->input->get_post('auditor');
			if (count($auditor) > 0) {

				foreach ($auditor as $ra) {
					$this->db->set("kegiatan_id", $kegiatan_id);
					$this->db->set("jabatan", 5);
					$this->db->set("pejabat_id", $ra);
					$this->db->set("waktu", 0);
					$this->db->insert("tr_kegiatanJabatan");
				}
			}
		//}


		$data["kegiatan_id"] =  $kegiatan_id;
		$data['kegiatan'] = $this->db->query("SELECT * from kegiatan where id='{$kegiatan_id}'")->row();
		$this->load->view("form_periode", $data);
	}

	public function savePeriode()
	{

		$kegiatan_id        = $this->input->get_post('kegiatan_id');
		$id                 = $this->input->get_post('id');
		$hari               = $this->input->get_post('hari');
		$bst               = $this->input->get_post('bst');
		$transport               = $this->input->get_post('transport');
		$hotel               = $this->input->get_post('hotel');
		$uh               = $this->input->get_post('uh');
		$rep               = $this->input->get_post('rep');

		$totalHotel = $hari * $hotel;
		$totaluh = $hari * $uh;
		$totalAnggaran = $bst + $transport + $totalHotel + $totaluh + $rep;

		// echo $totalAnggaran;
		// die();

		if ($hari == 0) {
			$totalAnggaran = 0;
		}

		$this->db->where("id", $id);
		$this->db->set("waktu", $hari);
		$this->db->set("anggaran", $totalAnggaran);
		$this->db->set("bst", $bst);
		$this->db->set("transport", $transport);
		$this->db->set("hotel", $hotel);
		$this->db->set("uh", $uh);
		$this->db->set("representatif", $rep);
		$this->db->update("tr_kegiatanJabatan");
		// echo $this->db->last_query();
		// die();

		$data["kegiatan_id"] =  $kegiatan_id;
		$data['kegiatan'] = $this->db->query("SELECT * from kegiatan where id='{$kegiatan_id}'")->row();
		$this->load->view("load_sbm", $data);
	}

	public function saveAll()
	{


		$kegiatan_id        = $this->input->get_post('kegiatan_id');
		$tgl_mulai          = $this->input->get_post('tgl_mulai');
		$tgl_selesai   = $this->input->get_post('tgl_selesai');


		$this->db->where("id", $kegiatan_id);
		$this->db->set("status", 1);
		$this->db->update("kegiatan");

		// baypass approval Irjen
		$bypass = $this->db->query("SELECT * from tr_status where kegiatan_id='{$kegiatan_id}' and groups_id =16 and status = 2 ")->result();
		if (!empty($bypass)) {
			$this->db->set("kegiatan_id", $kegiatan_id);
			$this->db->set("groups_id", 14);
			$this->db->set("status", 1);
			$this->db->set("notes", 'Otomatis Approve');
			$this->db->insert("tr_status");
		} else {
			//set status
			$this->db->set("kegiatan_id", $kegiatan_id);
			$this->db->set("groups_id", $_SESSION['group_id']);
			$this->db->set("status", 0);
			$this->db->insert("tr_status");
		}
	}

	public function hapus()
	{

		$id = $this->input->get_post("id", true);

		$this->db->delete("kegiatan", array("id" => $id));
		$this->db->delete("tr_kegiatanJabatan", array("kegiatan_id" => $id));
		echo "sukses";
	}

	public function berkas_upload()
	{

		header('Content-Type: application/json');
		echo json_encode(array('success' => true, 'message' => "Upload Sukses"));
	}
}
