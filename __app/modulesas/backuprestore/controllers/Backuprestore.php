<?php
ini_set("memory_limit","700M");
ini_set('max_execution_time', 90000);
defined('BASEPATH') OR exit('No direct script access allowed');

class Backuprestore extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 if(!$this->session->userdata("tmmadrasah_id")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_madrasah','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('school/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['data']	  = $this->Reff->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
		 $data['title']   = "Backup dan Restore Database ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	public function backup(){
		
		$this->load->helper('download');
		$tanggal=date('YmdHis');
		$namaFile="BackupElearningMadrasah".$tanggal.'.sql.zip';
		$prefs = array(
        
        'ignore'        => array('provinsi','kota','kecamatan','desa','migrations'),                     // List of tables to omit from the backup
        'format'        => 'zip',                       // gzip, zip, txt
        'filename'      => 'BackupElearningMadrasah'.$tanggal.'.sql',              // File name - NEEDED ONLY WITH ZIP FILES
        'add_drop'      => TRUE,                        // Whether to add DROP TABLE statements to backup file
        'add_insert'    => TRUE,                        // Whether to add INSERT data to backup file
        'newline'       => "\n"                         // Newline character used in backup file
        );


		$this->load->dbutil();
		$backup=& $this->dbutil->backup($prefs);
		force_download($namaFile, $backup);
		
		
	}
	
	public function restore(){
		
	
        
          $config['upload_path'] = '__statics/upload/'; 
          $config['allowed_types'] = '*'; 
          
          $config['file_name'] = $_FILES['file']['name'];

          // Load upload library 
          $this->load->library('upload',$config); 

          // File upload
          if($this->upload->do_upload('file')){
			  
			$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
 
            if($ext !="sql"){
		
				echo "Format yg Anda upload bukan database hasil backup (.sql)"; exit();

			}				
			  
			  
$db_debug = $this->db->db_debug; //save setting

$this->db->db_debug = FALSE;			  
             
			               $sql_contents = file_get_contents('__statics/upload/'.$config['file_name']);
							$sql_contents = explode(";", $sql_contents);
							$dendi=0;
							foreach($sql_contents as $query)
							{

								$pos = strpos($query,'ci_sessions');
								
								if($pos == false)
								{
									$dendi++;
									$result = $this->db->query($query);
									
									echo $dendi." Data berhasil direstore, mohon tunggu.. <br>";
								}
								else
								{
									continue;
								}

							}
							
							
             
				
				echo "Proses restore berhasil dilakukan"; exit();
            

         }else{ 
            echo $this->upload->display_errors();
         } 
      

		
	}
	
	
	 
}
