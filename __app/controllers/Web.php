<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		require APPPATH.'libraries/phpmailer/src/Exception.php';
		require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
		require APPPATH.'libraries/phpmailer/src/SMTP.php';


		$this->load->model("M_web","mf");
		date_default_timezone_set("Asia/Jakarta");
	}


    public function detail_auditi(){

		$data['data'] = $this->db->get_where("madrasah",array("nsm"=>$this->input->get_post("nsm")))->row();
		$this->load->view("detail_auditi",$data);
	}
	 
	
}
