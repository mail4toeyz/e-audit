<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumen extends CI_Controller {

    public function __construct()
      {
        parent::__construct();
		
          $this->load->helper('exportpdf_helper'); 
		
	  }
	
	
	public function pm1()
				{
					
				$id = base64_decode($this->input->get_post("id"));
		        
			    $data['yayasan']	  = $this->Reff->get_where("tm_yayasan",array("id"=>$id));
		        $data['madrasah']     = $this->Reff->get_where("tm_madrasah",array("tmyayasan_id"=>$id));
		
				$pdf_filename = 'PMS01-Tanda Terima Dokumen.pdf';	 
				
				$user_info = $this->load->view('pdf/pm-01', $data, true);
			
				
				$output = $user_info;
				
				generate_pdf($output, $pdf_filename);
				
				 
    }
	
	public function pm2()
				{
					
				$id = base64_decode($this->input->get_post("id"));
		        
			    $data['yayasan']	  = $this->Reff->get_where("tm_yayasan",array("id"=>$id));
		        $data['madrasah']     = $this->Reff->get_where("tm_madrasah",array("tmyayasan_id"=>$id));
		
				$pdf_filename = 'PMS02-Surat Tugas Verifikasi Dokumen Administratif.pdf';	 
				
				$user_info = $this->load->view('pdf/pm-02', $data, true);
			
				
				$output = $user_info;
				
				generate_pdf($output, $pdf_filename);
				
				 
    }
	
	public function pm3()
				{
					
				$id = base64_decode($this->input->get_post("id"));
		        
			    $data['yayasan']	  = $this->Reff->get_where("tm_yayasan",array("id"=>$id));
		        $data['madrasah']     = $this->Reff->get_where("tm_madrasah",array("tmyayasan_id"=>$id));
		
				$pdf_filename = 'PMS03- Berita Acara Verifikasi Administratif.pdf';	 
				
				$user_info = $this->load->view('pdf/pm-03', $data, true);
			
				
				$output = $user_info;
				
				generate_pdf($output, $pdf_filename);
				
				 
    }
	
	public function pm4()
				{
					
				$id = base64_decode($this->input->get_post("id"));
		        
			    $data['yayasan']	  = $this->Reff->get_where("tm_yayasan",array("id"=>$id));
		        $data['madrasah']     = $this->Reff->get_where("tm_madrasah",array("tmyayasan_id"=>$id));
		
				$pdf_filename = 'PMS04- Surat Tugas Verifikasi Lapangan.pdf';	 
				
				$user_info = $this->load->view('pdf/pm-04', $data, true);
			
				
				$output = $user_info;
				
				generate_pdf($output, $pdf_filename);
				
				 
    }
	
	public function pm5()
				{
					
				$id = base64_decode($this->input->get_post("id"));
		        
			    $data['yayasan']	  = $this->Reff->get_where("tm_yayasan",array("id"=>$id));
		        $data['madrasah']     = $this->Reff->get_where("tm_madrasah",array("tmyayasan_id"=>$id));
		
				$pdf_filename = 'PMS05- Berita Acara Verifikasi Lapangan.pdf';	 
				
				$user_info = $this->load->view('pdf/pm-05', $data, true);
			
				
				$output = $user_info;
				
				generate_pdf($output, $pdf_filename);
				
				 
    }
	
	public function pm6()
				{
					
				$id = base64_decode($this->input->get_post("id"));
		        
			    $data['yayasan']	  = $this->Reff->get_where("tm_yayasan",array("id"=>$id));
		        $data['madrasah']     = $this->Reff->get_where("tm_madrasah",array("tmyayasan_id"=>$id));
		
				$pdf_filename = 'PMS06- Surat Rekomendasi.pdf';	 
				
				$user_info = $this->load->view('pdf/pm-06', $data, true);
			
				
				$output = $user_info;
				
				generate_pdf($output, $pdf_filename);
				
				 
    }
	
	public function pm7()
				{
					
				$id = base64_decode($this->input->get_post("id"));
		        
			    $data['yayasan']	  = $this->Reff->get_where("tm_yayasan",array("id"=>$id));
		        $data['madrasah']     = $this->Reff->get_where("tm_madrasah",array("tmyayasan_id"=>$id));
		
				$pdf_filename = 'PMS07- Tanda Terima Dokumen dan Data Permohonan .pdf';	 
				
				$user_info = $this->load->view('pdf/pm-07', $data, true);
			
				
				$output = $user_info;
				
				generate_pdf($output, $pdf_filename);
				
				 
    }
	
	public function pm8()
				{
					
				$id = base64_decode($this->input->get_post("id"));
		        
			    $data['yayasan']	  = $this->Reff->get_where("tm_yayasan",array("id"=>$id));
		        $data['madrasah']     = $this->Reff->get_where("tm_madrasah",array("tmyayasan_id"=>$id));
		
				$pdf_filename = 'PMS08- Berita Acara Hasil Rapat Pertimbangan .pdf';	 
				
				$user_info = $this->load->view('pdf/pm-08', $data, true);
			
				
				$output = $user_info;
				
				generate_pdf($output, $pdf_filename);
				
				 
    }
	
		public function pm9()
				{
					
				$id = base64_decode($this->input->get_post("id"));
		        
			    $data['yayasan']	  = $this->Reff->get_where("tm_yayasan",array("id"=>$id));
		        $data['madrasah']     = $this->Reff->get_where("tm_madrasah",array("tmyayasan_id"=>$id));
		
				$pdf_filename = 'PMS09- NOTA DINAS.pdf';	 
				
				$user_info = $this->load->view('pdf/pm-09', $data, true);
			
				
				$output = $user_info;
				
				generate_pdf($output, $pdf_filename);
				
				 
    }
	
	public function pm10()
				{
					
				$id = base64_decode($this->input->get_post("id"));
		        
			    $data['yayasan']	  = $this->Reff->get_where("tm_yayasan",array("id"=>$id));
		        $data['madrasah']     = $this->Reff->get_where("tm_madrasah",array("tmyayasan_id"=>$id));
		
				$pdf_filename = 'PM10-SK IZIN OPERASIONAL.pdf';	 
				
				$user_info = $this->load->view('pdf/pm-10', $data, true);
			
				
				$output = $user_info;
				
				generate_pdf($output, $pdf_filename);
				
				 
    }
	
	public function pm11()
				{
					
				
		       header ("Content-type: text/html; charset=utf-8");	
				$this->load->library('PHPWord');		
                $this->load->helper('download');
				$id = base64_decode($this->input->get_post("id"));
		        
			  
		        $madrasah      = $this->Reff->get_where("tm_madrasah",array("id"=>$id));
		      
				
				$PHPWord = new PHPWord();	
		
		
                 
					 
				$document 		   = $PHPWord->loadTemplate('word/piagam.docx');
				
		
			    $madrasahnama = $this->Reff->namajenjang($madrasah->jenjang)." ".$madrasah->nama;
			   
						
				$document->setValue('{jenjang}', strtoupper($this->Reff->namajenjang($madrasah->jenjang)));
				
				$document->setValue('{nama_madrasah}', $madrasahnama);
				
				$document->setValue('{alamat}', ($madrasah->alamat));
				$document->setValue('{desa}', ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->desa_id),"desa","nama"))));
				$document->setValue('{kecamatan}', ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->kecamatan_id),"kecamatan","nama"))));
				$document->setValue('{kabko}', ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))));
				$document->setValue('{provinsi}', ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->provinsi_id),"provinsi","nama"))));
				
				list($n1,$n2,$n3,$n4,$n5,$n6,$n7,$n8,$n9,$na,$nb,$nc) = str_split($madrasah->nsm);
				
		        
				$document->setValue('{n1}', $n1); 
				$document->setValue('{n2}', $n2); 
				$document->setValue('{n3}', $n3); 
				$document->setValue('{n4}', $n4); 
				$document->setValue('{n5}', $n5); 
				$document->setValue('{n6}', $n6); 
				$document->setValue('{n7}', $n7); 
				$document->setValue('{n8}', $n8); 
				$document->setValue('{n9}', $n9); 
				$document->setValue('{n10}', $na); 
				$document->setValue('{n11}', $nb); 
				$document->setValue('{n12}', $nc); 
				
				
				
				$document->setValue('{tanggal}', $this->Reff->formattanggalstring($this->Reff->get_kondisi(array("tmyayasan_id"=>$madrasah->tmyayasan_id),"tr_piagam","tanggal")));
				
				
				
				$namafile = 'PMN_10_PIAGAM_PENEGERIAN.docx';	 
				$tmp_file = 'word/trash/'.$namafile.'';
		        $document->save($tmp_file);
		 
  
		
		      force_download($tmp_file,NULL); 
				
				 
    }
	
	public function sertifikat()
				{
					
				$id = base64_decode($this->input->get_post("id"));
		        
			    $data['yayasan']	  = $this->Reff->get_where("tm_yayasan",array("id"=>$id));
		        $data['madrasah']     = $this->Reff->get_where("tm_madrasah",array("tmyayasan_id"=>$id));
		
				$pdf_filename = 'SERTIFIKAT IZIN OPERASIONAL .pdf';	 
				
				$user_info = $this->load->view('pdf/sertifikat', $data, true);
			
				
				 $output = $user_info;
				
				generate_pdf($output, $pdf_filename);
				
				 
    }
}
