<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'login';
$route['404_override'] = 'login/notdendi';
$route['translate_uri_dashes'] = FALSE;
