<?php

class M_jadwal extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}



	// public function grid($paging)
	// {


	// 	$this->db->select("*");
	// 	$this->db->from('kegiatan k');
	// 	$this->db->join('tr_status ts', 'ts.kegiatan_id = k.id', 'left');


	// 	if ($paging == true) {
	// 		$this->db->limit($_REQUEST['length'], $_REQUEST['start']);
	// 		$this->db->order_by("k.id", "ASC");
	// 	}



	// 	return $this->db->get();
	// }

	public function grid($paging)
	{
		$query = 'select * from (
			select * from kegiatan k
			left join ( 
			SELECT kegiatan_id, groups_id, status as status_approval, notes, tanggal_approve
			FROM tr_status AS a
			WHERE tanggal_approve = (
				SELECT MAX(tanggal_approve)
				FROM tr_status AS b
				WHERE a.kegiatan_id = b.kegiatan_id
			)
			) s
			on s.kegiatan_id = k.id
			) x  ';

		if ($paging == true) {
			$query .= 'order by k.id asc
			LIMIT ' . $_REQUEST['length'] . ' OFFSET ' . $_REQUEST['start'];
		}
		return $this->db->query($query);
	}
}
