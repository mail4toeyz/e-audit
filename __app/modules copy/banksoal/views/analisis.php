
<div id="showform"></div>


<div class="col-md-12">
		
				       <div class="card">
					   <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
						<div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
							<h6 class="text-white text-capitalize ps-3">Analisis Butir  Soal Ujian <br>Dibawah ini adalah Analisis soal untuk pelaksanaan Ujian </h6>
							
						</div>
						</div>
						
					  
						 <div class="card-body">
							 
					
             <button onclick="btn_export()" class="btn btn-default">
						Cetak Excel with Header
					</button>
						
							<div class="table-responsive">
                                
                                <table class="table table-bordered table-striped  " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
                                        <tr>
                                            <th width="2px" rowspan="2">NO</th>
                                            
                                            <th rowspan="2">PAKET UJIAN   </th>
                                            <?php 
                                            $soal = $this->db->get_where("tr_soal",array("paket"=>$id))->result();
                                            $no=1;
                                              foreach($soal as $rs){
                                                ?> <th colspan="9"> SOAL NOMOR <?php echo $no++; ?>   </th><?php 

                                              }


                                            ?>
                                            
                                            
                                           
                                           
                                            
                                        </tr>

                                        <tr>
                                        <?php 
                                            $soal = $this->db->get_where("tr_soal",array("paket"=>$id))->result();
                                              foreach($soal as $rs){
                                                ?>
                                                <th> A </th>
                                                <th> B </th>
                                                <th> C </th>
                                                <th> D </th>
                                                <th> E </th>
                                                <th> JAWABAN BENAR </th>
                                                <th> MENJAWAB BENAR  </th>
                                                <th> PERSENTASE  </th>
                                                <th> TINGKAT KESULITAN </th>
                                          <?php 
                                              }
                                              ?>

                                        </tr>
                                    </thead>
                                   
                                    <tbody>
                                      <?php 
                                        $paket = $this->db->get_where("ref_ujian",array("id"=>$id))->result();
                                        $no=1;
                                         foreach($paket as $p){
                                           ?>
                                           <tr>
                                              <td><?php echo $no++; ?></td>
                                              <td><a href="<?php echo site_url("banksoal/cetak?id=".$p->id.""); ?>"><?php echo $p->nama; ?></a></td>

                                              <?php 
                                            $soal = $this->db->get_where("tr_soal",array("paket"=>$id))->result();
                                             $jwbBenar =0;
                                              foreach($soal as $rs){
                                                 $a = $this->db->query("SELECT count(id) as jml FROM `h_ujian` where list_jawaban LIKE '%".$rs->id.":A%'")->row();
                                                 $b = $this->db->query("SELECT count(id) as jml FROM `h_ujian` where list_jawaban LIKE '%".$rs->id.":B%'")->row();
                                                 $c = $this->db->query("SELECT count(id) as jml FROM `h_ujian` where list_jawaban LIKE '%".$rs->id.":C%'")->row();
                                                 $d = $this->db->query("SELECT count(id) as jml FROM `h_ujian` where list_jawaban LIKE '%".$rs->id.":D%'")->row();
                                                 $e = $this->db->query("SELECT count(id) as jml FROM `h_ujian` where list_jawaban LIKE '%".$rs->id.":E%'")->row();
                                                  if(strtoupper($rs->jawaban)=="A"){
                                                      $jwbBenar = $a->jml;
                                                   }else if(strtoupper($rs->jawaban)=="B"){
                                                    $jwbBenar = $b->jml;
                                                  }else if(strtoupper($rs->jawaban)=="C"){
                                                    $jwbBenar = $c->jml;
                                                  }else if(strtoupper($rs->jawaban)=="D"){
                                                    $jwbBenar = $d->jml;
                                                  }else if(strtoupper($rs->jawaban)=="E"){
                                                    $jwbBenar = $e->jml;
                                                   }



                                                   $persentase = ($jwbBenar/36) * 100;

                                                  $tingkatkesulitan="";
                                                     if($persentase > 80){
                                                      $tingkatkesulitan="Sangat Mudah";

                                                     }else if($persentase > 60 && $persentase < 79){
                                                      $tingkatkesulitan="Mudah";
                                                    }else if($persentase > 40 && $persentase < 59){
                                                      $tingkatkesulitan="Sedang";
                                                    }else if($persentase > 20 && $persentase < 39){
                                                      $tingkatkesulitan="Sulit";
                                                    }else if($persentase < 19){
                                                      $tingkatkesulitan="Sangat Sulit";
                                                     }

                                                ?>
                                                  <td> <?php echo $a->jml; ?> </td>
                                                  <td> <?php echo $b->jml; ?> </td>
                                                  <td> <?php echo $c->jml; ?> </td>
                                                  <td> <?php echo $d->jml; ?> </td>
                                                  <td> <?php echo $e->jml; ?> </td>
                                                  
                                                  <td> <?php echo $rs->jawaban; ?> </td>
                                                  <td> <?php echo $jwbBenar; ?> PESERTA   </td>
                                                  <td><?php echo number_format($persentase,2); ?> % </td>
                                                  <td> <?php echo $tingkatkesulitan; ?> </td>
                                          <?php 
                                              }
                                              ?>

                                           </tr>
                                           <?php 



                                         }
                                      ?>  
                                        
                                    </tbody>
                                </table>
                            </div>
                         </div>
                         </div>
                 
 </div>
  

      
<div id="analisismodal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body" id="loadanalisis" style="color:black">
        <p>Loading...</p>
      </div>
      
    </div>

  </div>
</div>
	
	
<script src="<?php echo base_url(); ?>__statics/js/excel/xlsx.extendscript.js"></script>
<script src="<?php echo base_url(); ?>__statics/js/excel/xlsx-style/xlsx.full.min.js"></script>

<script src="<?php echo base_url(); ?>__statics/js/excel/export.js"></script>
	
<script type="text/javascript">

function btn_export() {
        var table1 = document.querySelector("#datatableTable");
        var opt = {
            rowIndex: 4
        }; //开头空4行
        var sheet = XLSX2.utils.table_to_sheet(table1, opt);

  
     

        openDownloadDialog(sheet2blob(sheet), ' .Hasil CBT .xlsx');
    }
	

			  

</script>
				