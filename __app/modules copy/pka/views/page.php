<?php 
     if($_SESSION['group_id'] !="ketua"){

        ?>
<style>
 
        
    select,textarea,input{
    pointer-events: none;
	background-color: transparent;
   
        }
        .required{

          color:red;
        }
       
		
 </style>

 <?php 
	 }
?>


<div id="showform"></div>

<main id="main" class="main">

<div class="pagetitle">
  <h1><?php echo $title; ?></h1>
  <nav>
	<ol class="breadcrumb">
	  <li class="breadcrumb-item"><a href="#">e-Audit</a></li>
	
	  <li class="breadcrumb-item active"><?php echo $title; ?></li>
	</ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
	<div class="col-lg-12">

	  <div class="card">
		<div class="card-body">
		<form action="javascript:void(0)" method="post" id="simpanpka" name="simpanpka" url="<?php echo site_url("pka/save"); ?>">
		<input type="hidden" name="satker_id" value="<?php echo $satker->id; ?>">
		  <h5 class="card-title"> 
			<div style="float:right">
			<?php 
			if($_SESSION['group_id'] =="ketua"){

				?>
		      <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#verticalycentered"><i class="fa fa-cog"></i> Pengaturan </button>
                <button type="submit" class="btn btn-warning"><i class="fa fa-plus"></i> Add</button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                <button type="button" class="btn btn-danger"><i class="fa fa-file-pdf"></i> Export</button>
              </div>
			  <?php 
			}
			?>
			</div>
			<br>
			<br>
			<hr>
			 <table class="table  table-bordered table-striped">
				 <tr>
					 <td> Nama Auditi </td>
					 <td>   
					 <button type="button" class="btn btn-outline-primary detailLembaga" nsm="<?php echo $satker->kode; ?>" data-bs-toggle="modal" data-bs-target="#fullscreenModal">
					 <?php echo $satker->nama; ?> 
             		 </button></td>
				 </tr>
				 <tr>
					 <td> Tahun Anggaran </td>
					 <td> <?php echo $kegiatan->anggaran; ?> </td>
				 </tr>

			</table>
		  </h5>

		  <ul class="nav nav-tabs d-flex" id="myTabjustified" role="tablist">
			  <?php 
			   $kategoriPKA = $this->db->get("tm_kategori_pka")->result();
			     foreach($kategoriPKA as $rkat){
					$active = ($rkat->id==1) ? "active" :"";
			  ?>
                <li class="nav-item flex-fill" role="presentation">
                  <button class="nav-link w-100 <?php echo $active; ?>" id="home-tab" data-bs-toggle="tab" data-bs-target="#home-justified<?php echo $rkat->id; ?>" type="button" role="tab" aria-controls="home" aria-selected="true"><?php echo $rkat->slug; ?></button>
                </li>
			 <?php 
				 }
				?>
               
              </ul>
              <div class="tab-content pt-2" id="myTabjustifiedContent">
			  <?php 
			   $kategoriPKA = $this->db->get("tm_kategori_pka")->result();
			     foreach($kategoriPKA as $rkat){
					$active = ($rkat->id==1) ? "active" :"";
					
					$satker_id   = $satker->id;
					$kegiatan_id = $_SESSION['kegiatan_id'];
					$penyusun    = $_SESSION['idAuditor'];
					$this->db->where("kategori_pka",$rkat->id);
					$this->db->where("satker_id",$satker_id);
					$this->db->where("kegiatan_id",$kegiatan_id);
					//$this->db->where("penyusun",$penyusun);
					
					$datakategoriPKA  = $this->db->get("tr_pka")->row();

					if(is_null($datakategoriPKA) && $_SESSION['group_id'] =="anggota"){

						?>
						<div class="alert alert-danger bg-danger text-light border-0 alert-dismissible fade show" role="alert">
									Ketua TIM belum menyusun Program Kerja Audit   <?php echo $rkat->nama; ?>
						</div>

						<?php

					}else{
					
			  ?>
                <div class="tab-pane fade show <?php echo $active; ?>" id="home-justified<?php echo $rkat->id; ?>" role="tabpanel" aria-labelledby="home-tab">
					<center><h6 style="font-weight:bold">Program Kerja Audit <br> <?php echo $rkat->nama; ?></h6></center>
					
						<div class="row mb-3">
						<label for="inputEmail3" class="col-sm-2 col-form-label">PARAMETER</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="parameter<?php echo $rkat->id; ?>" value="<?php echo  isset($datakategoriPKA->parameter) ? $datakategoriPKA->parameter : $rkat->parameter; ?>">
							
						</div>
						</div>
						<div class="row mb-3">
						<label for="inputEmail3" class="col-sm-2 col-form-label">TAO</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="tao<?php echo $rkat->id; ?>" value="<?php echo  isset($datakategoriPKA->tao) ? $datakategoriPKA->tao : $rkat->tao; ?>">
						</div>
						</div>

						<div class="table-responsive">
					  	 <table class="table  table-bordered table-striped">
							<thead>
								<tr>
									<th rowspan="2">No</th>
									<th rowspan="2" width="35%">Uraian</th>
									<th colspan="2">Dikerjakan Oleh</th>
									<th colspan="2">Waktu yang diperlukan</th>
									<th rowspan="2">No KKA</th>
									<th rowspan="2">Catatan</th>
									 <th rowspan="2">Aksi</th> 
				 				</tr>
								<tr>
									<th> Rencana</th>
									<th> Realisasi</th>
									<th> Rencana</th>
									<th> Realisasi</th>
				 				</tr>
				 			</thead>
							<tbody>
								 <?php 
								  $uraian = $this->db->query("SELECT * from tm_uraian_pka where kategori_id='{$rkat->id}' order by urutan ASC")->result();
								  	$no=1;
								    foreach($uraian as $urow){

										$this->db->where("uraian_id",$urow->id);
										$this->db->where("kategori_pka",$rkat->id);
										$this->db->where("satker_id",$satker_id);
										$this->db->where("kegiatan_id",$kegiatan_id);
										//$this->db->where("penyusun",$penyusun);
										
										$dataUraianPKA  = $this->db->get("tr_pka")->row();

										?>
										 <tr id="<?php echo $rkat->id; ?><?php echo $urow->id; ?>">
											<td><?php echo $no++; ?></td>
											<td><input type="hidden" name="uraian<?php echo $rkat->id; ?><?php echo $urow->id; ?>" class="form-control" value="<?php echo  isset($dataUraianPKA->uraian) ? $dataUraianPKA->uraian : $urow->nama; ?>"><?php echo  isset($dataUraianPKA->uraian) ? $dataUraianPKA->uraian : $urow->nama; ?></td>
											<td>
											<select class="dikerjakanolehRencana form-control" name="dikerjakanRencana<?php echo $rkat->id; ?><?php echo $urow->id; ?>">
											<option value="0">- Pilih -</option>
												<?php 
												   $anggota = $this->db->query("SELECT id,NAMA_LENGKAP from pegawai_simpeg where id IN(".$kegiatan->anggota.")")->result();
												   foreach($anggota as $rang){
													?><option value="<?php echo $rang->id; ?>" <?php if(isset($dataUraianPKA->dikerjakanRencana)){  echo ($rang->id == $dataUraianPKA->dikerjakanRencana) ? "selected" : ""; } ?>><?php echo $rang->NAMA_LENGKAP; ?> </option>
													<?php
												   }
												?>
											</select>
											</td>
											<td>
											<select class="dikerjakanolehRealisasi form-control" name="dikerjakanRealisasi<?php echo $rkat->id; ?><?php echo $urow->id; ?>">
											<option value="0">- Pilih -</option>
												<?php 
												   $anggota = $this->db->query("SELECT id,NAMA_LENGKAP from pegawai_simpeg where id IN(".$kegiatan->anggota.")")->result();
												   foreach($anggota as $rang){
													?><option value="<?php echo $rang->id; ?>" <?php if(isset($dataUraianPKA->dikerjakanRealisasi)){  echo ($rang->id == $dataUraianPKA->dikerjakanRealisasi) ? "selected" : ""; } ?> ><?php echo $rang->NAMA_LENGKAP; ?> </option>
													<?php
												   }
												?>
											</select>
											</td>
											<td>
											<select class="waktuRencana form-control" name="waktuRencana<?php echo $rkat->id; ?><?php echo $urow->id; ?>">
												<option value="0">- Pilih -</option>
												<?php
												for ($a = 1; $a <= 10; $a++) {

												?><option value="<?php echo $a; ?>"  <?php if(isset($dataUraianPKA->waktuRencana)){  echo ($a == $dataUraianPKA->waktuRencana) ? "selected" : ""; } ?>><?php echo $a; ?> Hari </option>
												<?php
													}
												?>


											</select>
											</td>
											<td>
												<select class="waktuRealisasi form-control" name="waktuRealisasi<?php echo $rkat->id; ?><?php echo $urow->id; ?>">
													<option value="0">- Pilih -</option>
													<?php
													for ($a = 1; $a <= 10; $a++) {

													?><option value="<?php echo $a; ?>" <?php if(isset($dataUraianPKA->waktuRealisasi)){  echo ($a == $dataUraianPKA->waktuRealisasi) ? "selected" : ""; } ?>><?php echo $a; ?> Hari </option>
													<?php
														}
													?>


												</select>
											</td>
											<td><input type="text" class="form-control" name="no_kk<?php echo $rkat->id; ?><?php echo $urow->id; ?>" value="<?php echo  isset($dataUraianPKA->no_kk) ? $dataUraianPKA->no_kk : ""; ?>"></td>
											<td><textarea class="form-control" name="catatan<?php echo $rkat->id; ?><?php echo $urow->id; ?>"><?php echo  isset($dataUraianPKA->catatan) ? $dataUraianPKA->catatan : ""; ?> </textarea></td>
											<td>
													<div class="btn-group" role="group" aria-label="Basic mixed styles example">
														<button type="button" class="btn btn-primary"><i class="fa fa-pencil-square"></i> </button>
														<button type="button" class="btn btn-danger hapus" data-id="<?php echo $rkat->id; ?><?php echo $urow->id; ?>"><i class="fa fa-trash"></i> </button>
														
													</div>
											</td> 
										</tr>


										<?php 


									}
								 ?>

				 			</tbody>
				   		 </table>

				 		</div>
						
					
                 
                </div>
				<?php 
				 }
				}
				 ?>
               
              </div>
		 

		  <!-- Default Table -->
		</form>
		 
		</div>
	  </div>


	</div>

  </div>
</section>

</main>


<div class="modal fade modal-lg" id="verticalycentered" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Pengaturan </h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
					<div class="alert alert-warning alert-dismissible fade show" role="alert">
						<i class="bi bi-exclamation-triangle me-1"></i>
						 Anda dapat mengatur seluruh Uraian dan Ketepatan melalui fitur ini
						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
					</div>

				
						<div class="row mb-3">
						<label for="inputEmail3" class="col-sm-4 col-form-label">Dikerjakan Oleh (Rencana) </label>
						<div class="col-sm-8">
										<select class="form-control" id="dikerjakanolehRencana">
											<option value="0">- Pilih -</option>
												<?php 
												   $anggota = $this->db->query("SELECT id,NAMA_LENGKAP from pegawai_simpeg where id IN(".$kegiatan->anggota.")")->result();
												   foreach($anggota as $rang){
													?><option value="<?php echo $rang->id; ?>" ><?php echo $rang->NAMA_LENGKAP; ?> </option>
													<?php
												   }
												?>
											</select>
						</div>
						</div>
						<div class="row mb-3">
						<label for="inputEmail3" class="col-sm-4 col-form-label">Dikerjakan Oleh (Realisasi) </label>
						<div class="col-sm-8">
								<select class="form-control" id="dikerjakanolehRealisasi">
											<option value="0">- Pilih -</option>
												<?php 
												   $anggota = $this->db->query("SELECT id,NAMA_LENGKAP from pegawai_simpeg where id IN(".$kegiatan->anggota.")")->result();
												   foreach($anggota as $rang){
													?><option value="<?php echo $rang->id; ?>"><?php echo $rang->NAMA_LENGKAP; ?> </option>
													<?php
												   }
												?>
											</select>
						</div>
						</div>

						<div class="row mb-3">
						<label for="inputEmail3" class="col-sm-4 col-form-label">Waktu yang diperlukan (Rencana) </label>
						<div class="col-sm-8">
								
											<select class="form-control"  id="waktuRencana">
													<option value="0">- Pilih -</option>
													<?php
													for ($a = 1; $a <= 10; $a++) {

													?><option value="<?php echo $a; ?>" ><?php echo $a; ?> Hari </option>
													<?php
														}
													?>


												</select>
						</div>
						</div>

						<div class="row mb-3">
						<label for="inputEmail3" class="col-sm-4 col-form-label">Waktu yang diperlukan (Realisasi) </label>
						<div class="col-sm-8">
								
											<select class="form-control"  id="waktuRealisasi">
													<option value="0">- Pilih -</option>
													<?php
													for ($a = 1; $a <= 10; $a++) {

													?><option value="<?php echo $a; ?>" ><?php echo $a; ?> Hari </option>
													<?php
														}
													?>


												</select>
						</div>
						</div>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary" data-bs-dismiss="modal"><i class="fa fa-cog"></i> Terapkan Pengaturan</button>
                    </div>
                  </div>
                </div>
 </div>

	
<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					
					
					"ajax":{
						url :"<?php echo site_url("dokumen/grid"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						data.trkelas_id = $("#trkelas_id").val();
						data.keyword = $("#keyword").val();
						data.ajaran = $("#ajaran").val();
						
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
				
				$(document).on("input","#keyword",function(){
	  
				  dataTable.ajax.reload(null,false);	
				  
			  });
			  $(document).on("change","#trkelas_id,#ajaran",function(){
	  
				  dataTable.ajax.reload(null,false);	
				  
			  });
	
			  $(document).on("change","#dikerjakanolehRencana",function(){
				var nilai = $(this).val();
			 	 $(".dikerjakanolehRencana").val(nilai);

			  });

			  $(document).on("change","#dikerjakanolehRealisasi",function(){
				var nilai = $(this).val();
			 	 $(".dikerjakanolehRealisasi").val(nilai);

			  });
			  $(document).on("change","#waktuRencana",function(){
				var nilai = $(this).val();
			 	 $(".waktuRencana").val(nilai);

			  });
			  $(document).on("change","#waktuRealisasi",function(){
				var nilai = $(this).val();
			 	 $(".waktuRealisasi").val(nilai);

			  });

			  $(document).on('submit', 'form#simpanpka', function (event, messages) {
				event.preventDefault()
				var form   = $(this);
				var urlnya = $(this).attr("url");
				loading();
					$.ajax({
						type: "POST",
						url: urlnya,
						data: form.serialize(),
						success: function (response, status, xhr) {
							var ct = xhr.getResponseHeader("content-type") || "";
							if (ct == "application/json") {
						
							
							
								toastr.error(response.message, "Gagal  , perhatikan !  ", {
										"timeOut": "0",
										"extendedTImeout": "0",
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-top-right",
										"onclick": null,
										"showDuration": "10000",
										"hideDuration": "10000",
										"timeOut": "50000",
										"extendedTimeOut": "10000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut"
									});
								
							} else {
								
								toastr.success("Data Berhasil disimpan", "Sukses !", {
										"timeOut": "0",
										"extendedTImeout": "0",
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-top-right",
										"onclick": null,
										"showDuration": "10000",
										"hideDuration": "10000",
										"timeOut": "50000",
										"extendedTimeOut": "10000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut"
									});
								
							
							
							
							}
							
							jQuery.unblockUI({ });
						}
					});

					return false;
				});


</script>

			<div class="modal fade" id="fullscreenModal" tabindex="-1">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Detail Profile Auditi </h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body" id="loadBodyAuditi">
					 Mohon Tunggu ...
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                      
                    </div>
                  </div>
                </div>
</div>

<script>

	$(document).off("click",".detailLembaga").on("click",".detailLembaga",function(){

		var nsm = $(this).attr("nsm");
		 $.post("<?php echo site_url('web/detail_auditi'); ?>",{nsm:nsm},function(data){

			$("#loadBodyAuditi").html(data);

		 })

	});
  
</script>
				