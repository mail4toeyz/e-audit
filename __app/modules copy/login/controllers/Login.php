<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		$this->load->model('M_model',"mp");        
		$this->load->helper('exportpdf_helper'); 
	  }
		

	public function index()
	{  
	  
	   
	       $data 			= array();
           	   
		   $this->load->view('page_login',$data);
	

	}
	
	public function peserta()
	{  
	 
	   
		$data 			= array();           	   
		$this->load->view('page_peserta',$data);
	

	}
	

	
	
		
	public function do_login(){
     
        $this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
        
        $this->form_validation->set_message('min_length', '{field} Minimal 6 Karakter .');
        $this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
        $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , mungkin anda sudah pernah melakukan pendaftaran, silahkan klik menu login .');
				 
		 
			
				$config = array(
				    array('field' => 'username', 'label' => 'Username   ', 'rules' => 'trim|required'),
				  
				    array('field' => 'password', 'label' => 'Password ', 'rules' => 'trim|required|min_length[3]'),
				  
				 
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			  
			   $data = $this->mp->login();
			    if($data=="gagal"){
					header('Content-Type: application/json');
					echo json_encode(array('error' => true, 'message' => "Akun Anda tidak ditemukan "));
			    
                    
				}else{
					
					echo $data;
				}
												 
	     } else {
	         
           
			header('Content-Type: application/json');
			echo json_encode(array('error' => true, 'message' => "Username dan Password wajib diisi"));
			    
            
        }	
	
	
	}


	public function do_login_peserta(){
     
        $this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
        
        $this->form_validation->set_message('min_length', '{field} Minimal 6 Karakter .');
        $this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
        $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , mungkin anda sudah pernah melakukan pendaftaran, silahkan klik menu login .');
				 
		 
			
				$config = array(
				    array('field' => 'username', 'label' => 'Nomor Tes   ', 'rules' => 'trim|required'),
				  
				    array('field' => 'password', 'label' => 'Token Ujian ', 'rules' => 'trim|required|min_length[3]'),
				  
				 
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			  
			   $data = $this->mp->login_peserta();
			    if($data=="gagal"){
					header('Content-Type: application/json');
					echo json_encode(array('error' => true, 'message' => "Akun Anda tidak ditemukan, Mohon periksa Nomor Tes dan Tanggal Lahir yang Anda masukkan, Pastikan sama dengan kartu tes "));
			    				
					 
				}else if($data=="sesibuka"){

					header('Content-Type: application/json');
					echo json_encode(array('error' => true, 'message' => "Akun Anda ditemukan namun Anda hanya dapat login sesuai dengan jadwal pada kartu tes Anda, silahkan cek pada kartu tes "));
				
				
				}else if($data=="dikeluarkan"){
					
					header('Content-Type: application/json');
					echo json_encode(array('error' => true, 'message' => "Anda melakukan pelanggaran Ujian sehingga Panitia mengunci  akun Anda, silahkan hubungi Panitia "));
				
				
			
						
						

				}else if($data=="sudahlogin"){
					
						header('Content-Type: application/json');
						echo json_encode(array('error' => true, 'message' => "Anda sudah login dari perangkat lain, silahkan hubungi panitia untuk mereset login Anda"));
					
				
						

				}else{
					
					echo $data;
				}
												 
	     } else {
	         
			header('Content-Type: application/json');
			echo json_encode(array('error' => true, 'message' => "Nomor Tes dan Tanggal Lahir Wajib diisi"));
		
            
        }	
	
	
	}
	
	
	public function logout(){
		     $this->session->sess_destroy();
			 
		
			echo "yes";
		 
	 }
	 
   
   public function notdendi(){
	   
	   
	   $this->load->view("notdendi");
   }

 
	
	
}
