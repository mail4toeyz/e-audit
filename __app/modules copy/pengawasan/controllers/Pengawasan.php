<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pengawasan extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();

		if(!$this->session->userdata("status")){						
			$ajax = $this->input->get_post("ajax",true);		 
				if(!empty($ajax)){
			   
				   echo "<center>Session anda habis, silahkan login kembali klik <a href='".site_url("login")."'>disini </a></center>";
					  exit();
				 }else{
					 redirect(site_url()."login");
				 }
			
		   }


		
		  $this->load->model('M_siswa','m');
		  $this->load->helper('exportpdf_helper');  
		
	  }
	  
   function _template($data)
	{
		
		
	      $this->load->view('admin/page_header',$data);	
		
		
	}
		


	 public function ujian()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
         $id              = $this->input->get_post("id",true);	
         $kategori        = $this->input->get_post("kategori",true);	
		
		 $data['title']   	 = "Data Ujian  ";
		 $data['id']   		 =  $id; 
		 $data['kategori']   =  $kategori;
	     if(!empty($ajax)){
					    
			 $this->load->view('page_ujian',$data);
		
		 }else{
			 
		     $data['konten'] = "page_ujian";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid_ujian(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  $tmujian_id = $this->input->get_post("tmujian_id");
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			

			
			
			$bantuan   = $this->db->query("select id from bantuan where tmsiswa_id='".$val['id']."' AND jawaban =''")->row();
				$pertanyaan="";
				$stop ="";

				if(!is_null($bantuan)){
				$pertanyaan ="<a href='#' class='btn btn-warning btn-sm bantuan' tmsiswa_id='".$val['id']."' data-toggle='modal' data-target='#pelaksanaanmodal'><span class='fa fa-reply'></span> Pertanyaan </a>";
			

				}
				$waktu ="105 Menit";
                $hasilUjian = $this->db->query("SELECT * from h_ujian where tmsiswa_id='{$val['id']}' and tmujian_id='".$tmujian_id."'")->row();
				$status = "<span class='badge badge-sm bg-gradient-secondary'> Belum Mengerjakan </span>";
				if(!is_null($hasilUjian)){
				if($hasilUjian->status=="Y"){

				  $status = "<span class='badge badge-sm bg-gradient-warning'>Sedang mengerjakan </span>";				

				  $stop   ="<a href='javascript:void(0)' class='btn btn-danger btn-sm forcestop' tmsiswa_id='".$val['id']."' tmujian_id='".$hasilUjian->id."'><span class='fa fa-stop'></span>  Force stop </a>";
				

				}else if($hasilUjian->status=="N"){

				  $status = "<span class='badge badge-sm bg-gradient-success'>Selesai mengerjakan </span>";
				  $stop   ="<a href='javascript:void(0)' class='btn btn-danger btn-sm resetujian' tmsiswa_id='".$val['id']."' tmujian_id='".$hasilUjian->id."'><span class='fa fa-check'></span> Reset Ujian </a>";

				}

				$waktu    = round($hasilUjian->waktu/60)." Menit"; 

			  }


				$no = $i++;
				$records["data"][] = array(
					$no,
					'
					<div class="d-flex px-2 py-1">
                          <div>
                            <img src="https://appmadrasah.kemenag.go.id/seleksiptk/file_upload/peserta/'.$val['nik'].'/'.$val['foto'].'" class="avatar avatar-sm me-3 border-radius-lg" alt="user1" onError="imageError(this)">
                          </div>
                          <div class="d-flex flex-column justify-content-center">
                            <h6 class="mb-0 text-sm">'.$val['nama'].'</h6>
                            <p class="text-xs text-secondary mb-0">'.$val['no_test'].'</p>
                          </div>
                        </div>
					',
					
					$val['no_test'],
					$val['token'],
					
					
				//	$this->Reff->get_kondisi(array("id"=>$val['kategori']),"tm_kategori","nama"),
					$this->Reff->get_kondisi(array("id_admin"=>$val['madrasah_peminatan']),"madrasah_peminatan","owner"),
								
				
					$status,
					$waktu,
										
				    ' 
				
					<div class="btn-group">

					
					<a type="button" class="btn btn-primary btn-sm" href="'.site_url("pengawasan/beritaAcara?id=".$tmujian_id."&peserta_id=".$val['id']."").'" title="Berita Acara" > <i class="fa fa-laptop"></i> Berita Acara </a>
					'.$pertanyaan.'
					'.$stop.'
				 
					
					</div> 
					
					 
					  '
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}

public function beritaAcara(){

	$ajax            = $this->input->get_post("ajax",true);	
	$id              = $this->input->get_post("id",true);	
	$tmsiswa_id              = $this->input->get_post("peserta_id",true);	
	
   
	$data['title']   	 = "Berita Acara   ";
	$data['id']   		 =  $id; 
	$data['data']	     = $this->db->query("SELECT * from h_ujian where tmujian_id='{$id}' and tmsiswa_id='".$tmsiswa_id."'")->row();
	$data['peserta']	 = $this->db->query("SELECT * from tm_siswa where id='".$tmsiswa_id."'")->row();
	if(!empty($ajax)){
				   
		$this->load->view('beritaacara',$data);
   
	}else{
		
		$data['konten'] = "beritaacara";
		
		$this->_template($data);
	}


}


public function teguran(){

	 $this->db->set("teguran",$_POST['teguran']);
	 $this->db->set("tmsiswa_id",$_POST['tmsiswa_id']);
	 $this->db->set("ujian_id",$_POST['tmujian_id']);
	 $this->db->set("status",1);
	 $this->db->insert("teguran");

}


public function resetujian(){
	


	$id_tes = $this->input->get_post('tmujian_id', true);
		$id_tes = ($id_tes);
	

		$d_update = [
			
			'status'		=> 'Y'
		];
          
		$this->db->update('h_ujian', $d_update,array("id"=>$id_tes));
		print_r($d_update);


	
}


public function forcestop(){
	$tmsiswa_id = $this->input->get_post('tmsiswa_id', true);
	$this->db->set("locked",1);
	$this->db->where("id",$tmsiswa_id);
	$this->db->update("tm_siswa");


	$id_tes = $this->input->get_post('tmujian_id', true);
		$id_tes = ($id_tes);
		$list_jawaban = $this->m->getJawaban($id_tes);
		
		// Pecah Jawaban
		$pc_jawaban = explode(",", $list_jawaban);
		
		$jumlah_benar 	= 0;
		$jumlah_salah 	= 0;
		$jumlah_ragu  	= 0;
		$nilai_bobot 	= 0;
		$total_bobot	= 0;
		$jumlah_soal	= sizeof($pc_jawaban);

		foreach ($pc_jawaban as $jwb) {
			$pc_dt 		= explode(":", $jwb);
			$id_soal 	= $pc_dt[0];
			$jawaban 	= $pc_dt[1];
			$ragu 		= $pc_dt[2];

			$cek_jwb 	 = $this->m->getSoalById($id_soal);
			
			if($cek_jwb->jenis==1){

				if($cek_jwb->jawaban==$jawaban){
					$jumlah_benar++;
					$nilai_bobot = $nilai_bobot + $cek_jwb->bobot;
					$total_bobot = $total_bobot + $cek_jwb->bobot;
				}else{
					$jumlah_salah++;
				} 

			}else if($cek_jwb->jenis==9){
				$arr_nilai = array("A"=>"1","B"=>"2","C"=>"3","D"=>"4","E"=>"5");
				$nilai_bobot = $nilai_bobot + $arr_nilai[$jawaban];
				$total_bobot = $total_bobot + $cek_jwb->bobot;
				
			}
			
		}

		$nilai 			= ($jumlah_benar / ($jumlah_soal))  * 100;
		$nilai_bobot 	= ($total_bobot / ($jumlah_soal))  * 100;

		$d_update = [
			'jml_benar'		=> $jumlah_benar,
			//'tgl_selesai'	=> date("Y-m-d H:i:s"),
			'nilai'			=> $nilai,
			'nilai_bobot'	=> number_format(floor($nilai_bobot), 0),
			'status'		=> 'N'
		];
          
		$this->db->update('h_ujian', $d_update,array("id"=>$id_tes));
		print_r($d_update);


	
}
  public function selesaikan(){


	$id_tes = $this->input->get_post('id', true);
		$id_tes = ($id_tes);
		$list_jawaban = $this->m->getJawaban($id_tes);
		
		// Pecah Jawaban
		$pc_jawaban = explode(",", $list_jawaban);
		
		$jumlah_benar 	= 0;
		$jumlah_salah 	= 0;
		$jumlah_ragu  	= 0;
		$nilai_bobot 	= 0;
		$total_bobot	= 0;
		$jumlah_soal	= sizeof($pc_jawaban);

		foreach ($pc_jawaban as $jwb) {
			$pc_dt 		= explode(":", $jwb);
			$id_soal 	= $pc_dt[0];
			$jawaban 	= $pc_dt[1];
			$ragu 		= $pc_dt[2];

			$cek_jwb 	 = $this->m->getSoalById($id_soal);
			
			if($cek_jwb->jenis==1){

				if($cek_jwb->jawaban==$jawaban){
					$jumlah_benar++;
					$nilai_bobot = $nilai_bobot + $cek_jwb->bobot;
					$total_bobot = $total_bobot + $cek_jwb->bobot;
				}else{
					$jumlah_salah++;
				} 

			}else if($cek_jwb->jenis==9){
				$arr_nilai = array("A"=>"1","B"=>"2","C"=>"3","D"=>"4","E"=>"5");
				$nilai_bobot = $nilai_bobot + $arr_nilai[$jawaban];
				$total_bobot = $total_bobot + $cek_jwb->bobot;
				
			}
			
		}

		$nilai 			= ($jumlah_benar / ($jumlah_soal))  * 100;
		$nilai_bobot 	= ($total_bobot / ($jumlah_soal))  * 100;

		$d_update = [
			'jml_benar'		=> $jumlah_benar,
			//'tgl_selesai'	=> date("Y-m-d H:i:s"),
			'nilai'			=> $nilai,
			'nilai_bobot'	=> number_format(floor($nilai_bobot), 0),
			'status'		=> 'N'
		];
          
		$this->db->update('h_ujian', $d_update,array("id"=>$id_tes));
		print_r($d_update);


  }

  
 public function cetak(){
	    
	$data['peserta']   = $this->db->query("select * from tm_siswa where id='".$_GET['id']."'")->row();
		
	$data['soal']      = $this->db->query("select * from   tm_ujian where id='".$_GET['tmujian_id']."'")->row();
	$data['id_ujian']  = $_GET['id_ujian'];
		
	$pdf_filename = "Jawaban ".str_replace(array(".","'"),"",$data['peserta']->nama).str_replace(",","_",$data['soal']->nama).".pdf";	 

	$user_info = $this->load->view('cetak', $data, true);

	 $output = $user_info;
	
	 //echo $output;

	generate_pdf($output, $pdf_filename,TRUE);
	


}

}
