<link href="<?php echo base_url(); ?>__statics/js/gal/css/lightgallery.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>__statics/js/gal/js/lightgallery-all.min.js"></script>

<div class="col-md-12">
		
				       <div class="card">
					   <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
						<div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
							<h6 class="text-white text-capitalize ps-3"><?php echo  $this->Reff->get_kondisi(array("id"=>$kategori),"tm_kategori","nama"); ?> <br>
							Pengawasan Ujian Peserta  </h6>
						</div>
						</div>
						
					
						 <div class="card-body">
						
						 		
					   <div class="row" >
						 
					        

					   		
					   		<input type="hidden" id="kategori_ujian" value="<?php echo $id; ?>">
					   		<input type="hidden" id="tmujian_id" value="<?php echo $kategori; ?>">
							   <div class="col-md-3">
								
								<select class="form-control "    id="kategori" >
													 <option value=""> Filter Kategori </option>
													 <?php 
												  $pengaturan_id  = $this->Reff->set();
												  $kelas = $this->db->query("SELECT * from madrasah_peminatan where id_admin IN(select madrasah_peminatan from tm_siswa)")->result();
													 foreach($kelas as $i=>$r){
														 
														?><option value="<?php echo $r->id_admin; ?>" > <?php echo ($r->owner); ?></option><?php  
														 
													 }
												?>
								</select>
							</div>
		

						
						  <div class="col-md-2">
								
								<select class="form-control"    id="status" >
								<option value=""> Status Ujian  </option>
								<option value="1"> Belum Mengerjakan  </option>
								<option value="Y"> Sedang Mengerjakan  </option>
								<option value="N"> Selesai Mengerjakan  </option>
											
													 
													
								</select>
			        	  </div>


						        		
								<div class="col-md-4">
								
									<div class="input-group">
									<input type="text" class="form-control" id="keyword"  placeholder="Cari  Nomor Tes atau Nama disini..">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="button">CARI</button>
									</div>
									</div>
								</div>
					   </div>
					   
					   <br>


							<div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
									<tr>
                                            <th width="2px">NO</th>
                                            
                                            <th>PESERTA </th>
											<th>NO TEST</th>
                                            <th>TGL LAHIR </th>                                        
                                           
                                             <th>PEMINATAN </th>
                                             <th>STATUS </th>
                                             <th>SISA WAKTU </th>
                                             <th>TINDAKAN </th>

                                            
                                        </tr>

										
                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                         </div>
                         </div>
                         </div>
                 
 </div> 
      <div id="defaultModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	     Lembar Jawaban 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body" id="loadform">
        <p>Loading...</p>
      </div>
      
    </div>

  </div>
</div>


<div id="pelaksanaanmodal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body" id="loadbody">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
	
<script type="text/javascript">


$(document).off("click",".camera").on("click",".camera",function(){
	            
				var tmujian_id = $(this).attr("tmujian_id");
			 
				  $.post("<?php echo site_url('datasiswa/camera'); ?>",{tmujian_id:tmujian_id},function(data){
	   
				   $("#loadbody").html(data);
  
				  })
				   
				 
			   });
			   
			   $(document).off("click",".camerates").on("click",".camerates",function(){
				  
				var tmsiswa_id = $(this).attr("tmsiswa_id");
			 
				  $.post("<?php echo site_url('datasiswa/camerates'); ?>",{tmsiswa_id:tmsiswa_id},function(data){
	   
				   $("#loadbody").html(data);
  
				  })
				   
				 
			   });
			   $(document).off("click",".bantuan").on("click",".bantuan",function(){
				  
				var tmsiswa_id = $(this).attr("tmsiswa_id");
			 
				  $.post("<?php echo site_url('datasiswa/bantuan'); ?>",{tmsiswa_id:tmsiswa_id},function(data){
	   
				   $("#loadbody").html(data);
  
				  })
				   
				 
			   });

			  
			   
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							
							text:'Cetak Excel',
						
							},
							
							
							
							
						
							{
							extend: 'colvis',
							
							text:' Pengaturan Kolom ',
							
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("pengawasan/grid_ujian"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
							data.keyword 		= $("#keyword").val();
						
							data.kategori 		= $("#kategori").val();
							data.kategori_ujian = $("#kategori_ujian").val();
							data.provinsi = $("#provinsi").val();
							data.kota = $("#kota").val();
							data.tmujian_id = $("#tmujian_id").val();
							data.status = $("#status").val();
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
				
				
	
				$(document).on("input","#keyword",function(){
	  
					dataTable.ajax.reload(null,false);	
					
				});

				$(document).on("change","#tmmadrasah_id,#kategori,#provinsi,#kota,#unsur,#status",function(){
						
						dataTable.ajax.reload(null,false);	
						
					});

			

			$(document).off("click",".forcestop").on("click",".forcestop",function(){
				  
				  var tmsiswa_id = $(this).attr("tmsiswa_id");
				  var tmujian_id = $(this).attr("tmujian_id");

				  Swal.fire({
					title: 'Apakah Anda yakin  ?',
					text: "Peserta akan dikeluarkan dari pelaksanaan ujian dan tidak dapat melanjutkan ujian",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Keluarkan Peserta ',
					cancelButtonText: 'Tidak '
					}).then((result) => {
					if (result.value) {
						

						 $.post("<?php echo site_url('pengawasan/forcestop'); ?>",{tmsiswa_id:tmsiswa_id,tmujian_id:tmujian_id},function(data){
		 
							Swal.fire(
							'Keterangan !',
							'Peserta sudah dikeluarkan dari pelaksnaan CBT, Jika ingin memberikan akses kembali kepada peserta silahkan klik tombol reset.',
							'success'
							);
							dataTable.ajax.reload(null,false);	

							})

						
						
						
					
						
						
						
					}
					})


			   
				
					 
				   
				 });
			

				 
				 $(document).off("click",".resetujian").on("click",".resetujian",function(){
				  
				  var tmujian_id = $(this).attr("tmujian_id");
				

				  Swal.fire({
					title: 'Apakah Anda yakin  ?',
					text: "Anda akan melakukan reset ujian peserta ini. Jangan Khawatir, Reset ini hanya akan mengaktifkan kembali paket ujian agar dapat dilanjutkan dan  hasil jawaban Peserta sebelumnya tidak akan hilang, ",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Reset Ujian ',
					cancelButtonText: 'Tidak '
					}).then((result) => {
					if (result.value) {
						

						 $.post("<?php echo site_url('pengawasan/resetujian'); ?>",{tmujian_id:tmujian_id},function(data){
		 
							Swal.fire(
							'Keterangan !',
							'Reset Ujian berhasil dilakukan',
							'success'
							);
							dataTable.ajax.reload(null,false);	

							})

						
						
						
					
						
						
						
					}
					})


			   
				
					 
				   
				 });

</script>
				