<script src="<?php echo base_url(); ?>__statics/js/jquery.maskedinput.min.js"></script>

<div class="content">
        <div class="container-fluid">
          <div class="card">
            <div class="card-header card-header-primary">
              <h3 class="card-title">Aplikasi Wawancara <?php echo $this->Reff->owner(); ?> </h3>
              <p class="card-category"> Selamat Datang <?php echo $_SESSION['nama_aksi']; ?>, Silahkan mulai lakukan wawancara  dengan memasukkan nomor tes peserta yang akan diwawancara 



                
              </p>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">

                    <div class="input-group">
                    <input type="text" class="form-control input-mask-notes" id="no_test"  placeholder="Masukkan Nomor Tes">
                    <div class="input-group-append">
                      <button class="btn btn-success btn-sm" type="button" id="caripeserta"><span class="fa fa-search"></span> Cari Peserta </button>
                    </div>
                    </div>
            
                 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


      <script type="text/javascript">

var base_url="<?php echo base_url(); ?>";
jQuery(function($) {
                $.mask.definitions['~']='[+-]';
				
				$('.input-mask-notes').mask('99.99.99999');

});

$(document).off("click","#caripeserta").on("click","#caripeserta",function(){

    var no_test = $("#no_test").val();
    
       $.post("<?php echo site_url('wawancara/caripeserta'); ?>",{no_test:no_test},function(data){

          if(data==0){
              alertify.alert("Nomor Tes tidak ditemukan");

          }else{
         
           location.href = base_url+"wawancara/mulai?id="+data;

          }

       })


})

</script>