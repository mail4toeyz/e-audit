<?php 
     if($_SESSION['group_id'] =="ketua"){

        ?>
<style>
 
        
    select,textarea,input{
    pointer-events: none;
	background-color: transparent;
   
        }
        .required{

          color:red;
        }
       
		
 </style>

 <?php 
	 }
?>

<link href="<?php echo base_url(); ?>__statics/js/upload/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
 <script src="<?php echo base_url(); ?>__statics/js/upload/js/fileinput.js" type="text/javascript"></script>

<div id="showform"></div>

<main id="main" class="main">

<div class="pagetitle">
  <h1><?php echo $title; ?></h1>
  <nav>
	<ol class="breadcrumb">
	  <li class="breadcrumb-item"><a href="#">e-Audit</a></li>
	
	  <li class="breadcrumb-item active"><?php echo $title; ?></li>
	</ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
	<div class="col-lg-12">

	  <div class="card">
		<div class="card-body">
		<form action="javascript:void(0)" method="post" id="simpanpka" name="simpanpka" url="<?php echo site_url("kka/save"); ?>">
		<input type="hidden" name="satker_id" value="<?php echo $satker->id; ?>">
		  <h5 class="card-title"> 
			<div style="float:right">
		    <div class="btn-group" role="group" aria-label="Basic mixed styles example">
               <?php 
			    if($_SESSION['group_id'] !="ketua"){
				?>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<?php 
				}
				?>
                <button type="button" class="btn btn-danger"><i class="fa fa-file-pdf"></i> Export</button>
              </div>
			</div>
			<br>
			<br>
			<hr>
			 <table class="table  table-bordered table-striped">
				 <tr>
					 <td> Nama Auditi </td>
					 <td>   
					 <button type="button" class="btn btn-outline-primary detailLembaga" nsm="<?php echo $satker->kode; ?>" data-bs-toggle="modal" data-bs-target="#fullscreenModal">
					 <?php echo $satker->nama; ?> 
             		 </button></td>
				 </tr>
				 <tr>
					 <td> Tahun Anggaran </td>
					 <td> <?php echo $kegiatan->anggaran; ?> </td>
				 </tr>

			</table>
		  </h5>

		  <ul class="nav nav-tabs d-flex" id="myTabjustified" role="tablist">
			  <?php 
			   $kategoriPKA = $this->db->get("tm_kategori_pka")->result();
			     foreach($kategoriPKA as $rkat){
					$active = ($rkat->id==1) ? "active" :"";
			  ?>
                <li class="nav-item flex-fill" role="presentation">
                  <button class="nav-link w-100 <?php echo $active; ?>" id="home-tab" data-bs-toggle="tab" data-bs-target="#home-justified<?php echo $rkat->id; ?>" type="button" role="tab" aria-controls="home" aria-selected="true"><?php echo $rkat->slug; ?></button>
                </li>
			 <?php 
				 }
				?>
               
              </ul>
              <div class="tab-content pt-2" id="myTabjustifiedContent">
			  <?php 
			   $kategoriPKA = $this->db->get("tm_kategori_pka")->result();
			     foreach($kategoriPKA as $rkat){
					$active = ($rkat->id==1) ? "active" :"";
					
					$satker_id   = $satker->id;
					$kegiatan_id = $_SESSION['kegiatan_id'];
					$penyusun    = $_SESSION['idAuditor'];
					$this->db->where("kategori_pka",$rkat->id);
					$this->db->where("satker_id",$satker_id);
					$this->db->where("kegiatan_id",$kegiatan_id);
					//$this->db->where("penyusun",$penyusun);
					
					$datakategoriPKA  = $this->db->get("tr_kka")->row();
					$dataInduk = $this->db->query("SELECT SUM(nilai) as nilai, SUM(bobot) as bobot, sum(capaian) as capaian from tr_kka where uraian_id IN(SELECT id from tm_kka where kategori_id='".$rkat->id."' and jenis !=0)")->row();


					
					
			  ?>
                <div class="tab-pane fade show <?php echo $active; ?>" id="home-justified<?php echo $rkat->id; ?>" role="tabpanel" aria-labelledby="home-tab">
					<center><h5 style="font-weight:bold">Kertas Kerja Audit <br> Capaian Kinerja <?php echo $rkat->slug; ?></h5></center>
					
						

						<div class="col-md-4">
						<table class="table  table-bordered table-striped" width="100%">
							<thead>
								<tr>
									<th colspan="2"> <center> Kondisi</center> </th>
									<th rowspan="2"> Nilai Capaian </th>
				 				</tr>
								 <tr>
									<th> Realisasi Nilai </th>
									<th> Bobot(%)  </th>
				 				</tr>

				 			</thead>
							 <tbody>

							    <tr>
									<td> <?php echo number_format($dataInduk->nilai,3,",","."); ?> </td>
									<td> <?php if($rkat->id==5){ echo number_format(ceil($dataInduk->bobot),2,",","."); }else{ echo number_format(ceil($dataInduk->bobot),2,",","."); } ?> </td>
									<td> <?php echo number_format($dataInduk->capaian,3,",","."); ?> </td>
									
				 				</tr>
								

				 			</tbody>
				 		</table>
				 		</div>
						<div class="table-responsive">
						

					  	 <table class="table  table-bordered ">
							<thead>
								<tr>
									<th>No</th>
									<th>Key Performance Indicator (KPI) </th>
									<th>Nilai</th>
									<th>Bobot</th>
									<th>Capaian</th>
									<th>Dokumen</th>
									<th>Kondisi</th>
									<th>Sebab</th>
									<th>Akibat</th>
									<th>Rekomendasi</th>
									<!-- <th>Kode Temuan</th>
									<th>Kode Rekomendasi</th> -->
									<th>Aksi </th>
									
									
									<!-- <th rowspan="2">Aksi</th> -->
				 				</tr>
								
				 			</thead>
							<tbody>
								 <?php 
								  $uraian = $this->db->query("SELECT * from tm_kka where kategori_id='{$rkat->id}' ")->result();
								  	$no=1;
								    foreach($uraian as $urow){

										$this->db->where("uraian_id",$urow->id);
										$this->db->where("kategori_pka",$rkat->id);
										$this->db->where("satker_id",$satker_id);
										$this->db->where("kegiatan_id",$kegiatan_id);
										
										$dataUraianPKA  = $this->db->get("tr_kka")->row();

										$style="";
										$readonly="";
										if($urow->jenis==0){
											$style="style='color:white;background-color:green;'";
											$readonly ="readonly ";

										
											?>
										 <tr <?php echo $style; ?>>
											<td><?php echo $no++; ?></td>
											<td><input type="hidden" name="uraian<?php echo $rkat->id; ?><?php echo $urow->id; ?>" class="form-control" value="<?php echo  isset($dataUraianPKA->uraian) ? $dataUraianPKA->uraian : $urow->nama; ?>"><?php echo  isset($dataUraianPKA->uraian) ? $dataUraianPKA->uraian : $urow->nama; ?></td>
											
											<td></td>

											<td><?php echo number_format($urow->bobot,3,",",".");  ?></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											
										

											
										</tr>


										<?php 
										}else{

										$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
										$this->db->where("satker_id",$satker_id);
										$this->db->where("kka_id",$urow->id);
									    $kegiatanPersyaratan = $this->db->get("tr_kegiatan_persyaratan")->row();
										if(!is_null($kegiatanPersyaratan)){
											$status ='<button class="btn btn-primary uploadDokumen" data-id="'.$urow->id.'" satker_id="'.$satker_id.'" type="button" data-bs-toggle="modal" data-bs-target="#largeModal"><i class="fa fa-file"></i> Buka  ('.count($kegiatanPersyaratan).')</button>';
										}else{
											$status ='<span class="badge bg-danger"><i class="bi bi-exclamation-octagon me-1"></i> Belum diupload</span>';
										}


										$this->db->where("satker_id",$satker_id);
										$this->db->where("kka_id",$urow->id);
										$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
										$notisi = $this->db->get("tr_notisi")->row();
										$dataNotisi = "";
											if(!is_null($notisi)){
												
												$dataNotisi = '<span class="badge border-danger border-1 text-danger">Kode Temuan : '.$notisi->kode_temuan.' </span>';
												$dataNotisi .= '<br><span class="badge border-primary border-1 text-danger">Kode Rekomendasi : '.$notisi->kode_rekomendasi.' </span> ';
											
												$dataNotisi .= '<br><span class="badge border-secondary border-1 text-secondary">'. $notisi->keterangan."</span>";

												$tanggapan = $this->db->get_where("tr_tanggapan",array("notisi_id"=>$notisi->id))->row();
													
												if(!is_null($tanggapan)){
												  $dataNotisi .= '<br><span class="badge border-success border-1 text-success" style="text-align:left">Tanggapan : <br> '. $tanggapan->keterangan."</span>";
										  

												  }
											}

											if($dataUraianPKA->nilai < 1 ){


											?>
											<tr>
											   <td><?php echo $no++; ?></td>
											   <td><input type="hidden" name="uraian<?php echo $rkat->id; ?><?php echo $urow->id; ?>" class="form-control" value="<?php echo  isset($dataUraianPKA->uraian) ? $dataUraianPKA->uraian : $urow->nama; ?>"><?php echo  isset($dataUraianPKA->uraian) ? $dataUraianPKA->uraian : $urow->nama; ?></td>
											   
											   <td><input type="text"  class="form-control skor" name="nilai<?php echo $rkat->id; ?><?php echo $urow->id; ?>" data-id="<?php echo $rkat->id; ?><?php echo $urow->id; ?>" value="<?php echo  isset($dataUraianPKA->nilai) ? $dataUraianPKA->nilai : ""; ?>"></td>
   
											   <td><input type="hidden" class="form-control" id="bobot<?php echo $rkat->id; ?><?php echo $urow->id; ?>" value="<?php echo ($urow->bobot);  ?>"> <?php echo number_format($urow->bobot,3,",",".");  ?></td>
											   <td><input type="text" class="form-control"  id="capaian<?php echo $rkat->id; ?><?php echo $urow->id; ?>" name="capaian<?php echo $rkat->id; ?><?php echo $urow->id; ?>" value="<?php echo  isset($dataUraianPKA->capaian) ? number_format($dataUraianPKA->capaian,3,",",".") : ""; ?>" readonly></td>
											   <td><?php echo $status; ?></td>
											   <!-- <td><?php echo $dataNotisi; ?></td> -->

											   <td></td>
											   <td></td>
											   <td></td>
											   <td></td>
										
											   <td> 
												<?php 
												if($_SESSION['group_id'] !="ketua"){
												?>	
											   <button type="button" class="btn btn-danger rounded-pill notisi" type="button" data-bs-toggle="modal" data-bs-target="#largeModalNotisi" data-id="<?php echo $urow->id; ?>" > Notisi </button>
											   <?php 
												}
												?>
											   </td>
   
											   
										   </tr>
   
   
										   <?php 
											}


										}

										


									}
								 ?>

				 			</tbody>
				   		 </table>

				 		</div>
						
					
                 
                </div>
				<?php 
				 }
				
				 ?>
               
              </div>
		 

		  <!-- Default Table -->
		</form>
		 
		</div>
	  </div>


	</div>

  </div>
</section>

</main>

<div class="modal fade" id="largeModal" tabindex="-1">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Detail Dokumen</h5>
                      
                    </div>
                    <div class="modal-body" id="load-body">
				 		Mohon Tunggu ..
                     
                    </div>
					<div class="modal-footer">
						<button type="button"  class="btn btn-default" data-bs-dismiss="modal">Tutup</button>
					</div>
                   
                  </div>
                </div>
 </div>

 <div class="modal fade" id="largeModalNotisi">
                <div class="modal-dialog modal-xl">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Notisi </h5>
                      
                    </div>
                    <div class="modal-body" id="load-bodyNotisi">
					Mohon Tunggu ..
                    </div>
					<div class="modal-footer">
						<button type="button"  class="btn btn-default" data-bs-dismiss="modal">Tutup</button>
					</div>
                   
                  </div>
                </div>
 </div>
	


<script type="text/javascript">
	 $(document).on("click",".uploadDokumen",function(){
				var id 		  = $(this).data("id");
				var satker_id = "<?php echo $satker->id; ?>";

				$.post("<?php echo site_url('kka/berkas'); ?>",{id:id,satker_id:satker_id},function(data){

					$("#load-body").html(data);

				})


			  });

			  $(document).on("click",".notisi",function(){
				var id 		  = $(this).data("id");
				var satker_id = "<?php echo $satker->id; ?>";

				$.post("<?php echo site_url('notisi/notisi'); ?>",{id:id,satker_id:satker_id},function(data){

					$("#load-bodyNotisi").html(data);

				})


			  })


  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					
					
					"ajax":{
						url :"<?php echo site_url("dokumen/grid"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						data.trkelas_id = $("#trkelas_id").val();
						data.keyword = $("#keyword").val();
						data.ajaran = $("#ajaran").val();
						
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
				
				$(document).on("input","#keyword",function(){
	  
				  dataTable.ajax.reload(null,false);	
				  
			  });
			  $(document).on("change","#trkelas_id,#ajaran",function(){
	  
				  dataTable.ajax.reload(null,false);	
				  
			  });
	
			  $(document).on("change","#dikerjakanolehRencana",function(){
				var nilai = $(this).val();
			 	 $(".dikerjakanolehRencana").val(nilai);

			  });

			  $(document).on("change","#dikerjakanolehRealisasi",function(){
				var nilai = $(this).val();
			 	 $(".dikerjakanolehRealisasi").val(nilai);

			  });
			  $(document).on("change","#waktuRencana",function(){
				var nilai = $(this).val();
			 	 $(".waktuRencana").val(nilai);

			  });
			  $(document).on("change","#waktuRealisasi",function(){
				var nilai = $(this).val();
			 	 $(".waktuRealisasi").val(nilai);

			  });

			  $(document).on("input",".skor",function(){
				var nilai 	 = $(this).val();
				var id   	 = $(this).data("id");
				var bobot    = $("#bobot"+id).val();

				if(!isNaN(nilai)){

					if(nilai <= 1){
						var capaian = nilai * bobot;
						
				  		$("#capaian"+id).val(capaian);

					}else{
						alertify.warning("Tidak melebihi angka 1");	
					}
				   

				}else{
					alertify.warning("Masukkan berupa angka, jika ada komma, masukkan dengan tanda titik. Contoh : 0.750");

				}
			 	 

			  });

			  $(document).on('submit', 'form#simpanpka', function (event, messages) {
				event.preventDefault()
				var form   = $(this);
				var urlnya = $(this).attr("url");
				loading();
					$.ajax({
						type: "POST",
						url: urlnya,
						data: form.serialize(),
						success: function (response, status, xhr) {
							var ct = xhr.getResponseHeader("content-type") || "";
							if (ct == "application/json") {
						
							
							
								toastr.error(response.message, "Gagal  , perhatikan !  ", {
										"timeOut": "0",
										"extendedTImeout": "0",
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-top-right",
										"onclick": null,
										"showDuration": "10000",
										"hideDuration": "10000",
										"timeOut": "50000",
										"extendedTimeOut": "10000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut"
									});
								
							} else {
								
								toastr.success("Data Berhasil disimpan", "Sukses !", {
										"timeOut": "0",
										"extendedTImeout": "0",
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-top-right",
										"onclick": null,
										"showDuration": "10000",
										"hideDuration": "10000",
										"timeOut": "50000",
										"extendedTimeOut": "10000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut"
									});
									location.reload();
								
							
							
							
							}
							
							jQuery.unblockUI({ });
						}
					});

					return false;
				});

				


</script>

<div class="modal fade" id="fullscreenModal" tabindex="-1">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Detail Profile Auditi </h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body" id="loadBodyAuditi">
					 Mohon Tunggu ...
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                      
                    </div>
                  </div>
                </div>
</div>

<script>

	$(document).off("click",".detailLembaga").on("click",".detailLembaga",function(){

		var nsm = $(this).attr("nsm");
		 $.post("<?php echo site_url('web/detail_auditi'); ?>",{nsm:nsm},function(data){

			$("#loadBodyAuditi").html(data);

		 })

	});
  
</script>
				
				