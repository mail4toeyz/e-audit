<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title><?php echo $title; ?></title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo base_url(); ?>__statics/img/logo.png" rel="icon">

  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/fa/css/all.min.css">
  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/simple-datatables/style.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>__statics/js/datepicker/datepicker.css" rel="stylesheet" type="text/css">
  <!-- Template Main CSS File -->
  <script src="<?php echo base_url(); ?>__statics/js/jquery.min.js"></script>
	<script> var base_url="<?php echo base_url(); ?>"; </script>
  <script src="<?php echo base_url(); ?>__statics/js/proses.js"></script>
  <link href="<?php echo base_url(); ?>__statics/admin/assets/css/style.css" rel="stylesheet">
  <!-- Styles -->
  <link href="<?php echo base_url(); ?>__statics/js/wizard/css/smart_wizard_all.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="<?php echo base_url(); ?>__statics/js/wizard/js/jquery.smartWizard.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>
  <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/js/dualisme/bootstrap-duallistbox.min.css" />
  <script src="<?php echo base_url(); ?>__statics/js/dualisme/jquery.bootstrap-duallistbox.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/js/datepicker/datepicker.js" ></script>

		<script src="<?php echo base_url(); ?>__statics/js/datatable/button.js" type="text/javascript"></script>
	  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/bootstrap-toastr/toastr.min.css"/>
		
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.13.1/af-2.5.1/b-2.3.3/b-colvis-2.3.3/b-html5-2.3.3/b-print-2.3.3/cr-1.6.1/date-1.2.0/fc-4.2.1/fh-3.3.1/kt-2.8.0/r-2.4.0/rg-1.3.0/rr-1.3.1/sc-2.0.7/sb-1.4.0/sp-2.1.0/sl-1.5.0/sr-1.2.0/datatables.min.css"/>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.13.1/af-2.5.1/b-2.3.3/b-colvis-2.3.3/b-html5-2.3.3/b-print-2.3.3/cr-1.6.1/date-1.2.0/fc-4.2.1/fh-3.3.1/kt-2.8.0/r-2.4.0/rg-1.3.0/rr-1.3.1/sc-2.0.7/sb-1.4.0/sp-2.1.0/sl-1.5.0/sr-1.2.0/datatables.min.js"></script>

    <link href="<?php echo base_url(); ?>__statics/js/alertify/css/alertify.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/js/datetimepicker/jquery.datetimepicker.css">
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top d-flex align-items-center">

    <div class="d-flex align-items-center justify-content-between">
      <a href="<?php echo base_url(); ?>" class="logo d-flex align-items-center">
        <img src="<?php echo base_url(); ?>__statics/img/logo.png" alt="">
        <span class="d-none d-lg-block" style="font-size:14px">Sistem Informasi Pengawasan Internal Terintegrasi</span>
      </a>
      <i class="bi bi-list toggle-sidebar-btn"></i>
    </div><!-- End Logo -->

    <div class="search-bar">
      <form class="search-form d-flex align-items-center" method="POST" action="#">
          <select class="form-control selectKegiatan" >
            <option value="">- Pilih Kegiatan -</option>
            <?php 
              $kegiatan = $this->db->query("SELECT * from kegiatan where (ketua=".$_SESSION['idAuditor']." or anggota LIKE '%".$_SESSION['idAuditor'].",%') and approval=1")->result();
                foreach($kegiatan as $r){
       
                  ?><option value="<?php echo $r->id; ?>" <?php echo ($_SESSION['kegiatan_id']==$r->id) ? "selected":""; ?>><?php echo $r->anggaran; ?> - <?php echo $r->judul; ?></option><?php 

                }
              ?>
          </select>
        <button type="submit" title="Search"><i class="bi bi-search"></i></button>
      </form>
    </div><!-- End Search Bar -->

    <nav class="header-nav ms-auto">
      <ul class="d-flex align-items-center">

        <li class="nav-item d-block d-lg-none">
          <a class="nav-link nav-icon search-bar-toggle " href="#">
            <i class="bi bi-search"></i>
          </a>
        </li><!-- End Search Icon-->

        <li class="nav-item dropdown">

          <a class="nav-link nav-icon" href="#" data-bs-toggle="dropdown">
            <i class="bi bi-bell"></i>
            <span class="badge bg-primary badge-number">4</span>
          </a><!-- End Notification Icon -->

          <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow notifications">
            <li class="dropdown-header">
              You have 4 new notifications
              <a href="#"><span class="badge rounded-pill bg-primary p-2 ms-2">View all</span></a>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>

            <li class="notification-item">
              <i class="bi bi-exclamation-circle text-warning"></i>
              <div>
                <h4>Lorem Ipsum</h4>
                <p>Quae dolorem earum veritatis oditseno</p>
                <p>30 min. ago</p>
              </div>
            </li>

            <li>
              <hr class="dropdown-divider">
            </li>

            <li class="notification-item">
              <i class="bi bi-x-circle text-danger"></i>
              <div>
                <h4>Atque rerum nesciunt</h4>
                <p>Quae dolorem earum veritatis oditseno</p>
                <p>1 hr. ago</p>
              </div>
            </li>

            <li>
              <hr class="dropdown-divider">
            </li>

            <li class="notification-item">
              <i class="bi bi-check-circle text-success"></i>
              <div>
                <h4>Sit rerum fuga</h4>
                <p>Quae dolorem earum veritatis oditseno</p>
                <p>2 hrs. ago</p>
              </div>
            </li>

            <li>
              <hr class="dropdown-divider">
            </li>

            <li class="notification-item">
              <i class="bi bi-info-circle text-primary"></i>
              <div>
                <h4>Dicta reprehenderit</h4>
                <p>Quae dolorem earum veritatis oditseno</p>
                <p>4 hrs. ago</p>
              </div>
            </li>

            <li>
              <hr class="dropdown-divider">
            </li>
            <li class="dropdown-footer">
              <a href="#">Show all notifications</a>
            </li>

          </ul><!-- End Notification Dropdown Items -->

        </li><!-- End Notification Nav -->

       

        <li class="nav-item dropdown pe-3">

          <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
          <img src="<?php echo base_url(); ?>__statics/img/logo.png" alt="Profile" class="rounded-circle">
            <span class="d-none d-md-block dropdown-toggle ps-2"><?php echo $_SESSION['nama']; ?></span>
          </a><!-- End Profile Iamge Icon -->

          <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
            <li class="dropdown-header">
              <h6>Kevin Anderson</h6>
              <span>Web Designer</span>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>

            <li>
              <a class="dropdown-item d-flex align-items-center" href="users-profile.html">
                <i class="bi bi-person"></i>
                <span>My Profile</span>
              </a>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>

            <li>
              <a class="dropdown-item d-flex align-items-center" href="users-profile.html">
                <i class="bi bi-gear"></i>
                <span>Account Settings</span>
              </a>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>

            <li>
              <a class="dropdown-item d-flex align-items-center" href="pages-faq.html">
                <i class="bi bi-question-circle"></i>
                <span>Need Help?</span>
              </a>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>

            <li>
              <a class="dropdown-item d-flex align-items-center" href="#">
                <i class="bi bi-box-arrow-right"></i>
                <span>Sign Out</span>
              </a>
            </li>

          </ul><!-- End Profile Dropdown Items -->
        </li><!-- End Profile Nav -->

      </ul>
    </nav><!-- End Icons Navigation -->

  </header><!-- End Header -->

  <!-- ======= Sidebar ======= -->
   <?php $this->load->view("auditor/page_menu"); ?>

   <?php $this->load->view(isset($konten) ? $konten : "page_default"); ?>

 

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?php echo base_url(); ?>__statics/js/datetimepicker/jquery.datetimepicker.full.js"></script>
  <script src="<?php echo base_url(); ?>__statics/js/alertify/alertify.js"></script>
  <script src="<?php echo base_url(); ?>__statics/js/bootstrap-toastr/toastr.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/apexcharts/apexcharts.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/chart.js/chart.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/echarts/echarts.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/quill/quill.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/simple-datatables/simple-datatables.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/tinymce/tinymce.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/php-email-form/validate.js"></script>
  <script src="<?php echo base_url(); ?>__statics/js/jquery.blockui.min.js"></script>
  <!-- Template Main JS File -->
  <script src="<?php echo base_url(); ?>__statics/admin/assets/js/main.js"></script>
  <script>
 $(document).off("change",".selectKegiatan").on("change",".selectKegiatan",function(){
       
        var kegiatan_id  = $(this).val();
          $.post("<?php echo site_url("auditor/setKegiatan"); ?>",{kegiatan_id:kegiatan_id},function(data){
            location.reload();
          
          })
      

      });
$(document).off("click",".pilihKegiatan").on("click",".pilihKegiatan",function(){
       
       var kegiatan_id  = $(this).attr("kegiatan_id");
         $.post("<?php echo site_url("auditor/setKegiatan"); ?>",{kegiatan_id:kegiatan_id},function(data){
           location.reload();
         
         })
     

   });
</script>

</body>

</html>