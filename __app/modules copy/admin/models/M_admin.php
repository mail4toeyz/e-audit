<?php

class M_admin extends CI_Model {
 
    public function __construct() {
        parent::__construct();
    }


	public function getUjianById($id)
    {
        $this->db->select('*');
        $this->db->from('tm_ujian a');
      
        $this->db->where('a.id', $id);
        return $this->db->get()->row();
    }
	
	 public function getSoal($id)
    {
        $ujian = $this->getUjianById($id);
        $order = $ujian->jenis==="acak" ? 'rand()' : 'urutan';

        $this->db->select('id as id_soal, soal,opsi_a, opsi_b, opsi_c, opsi_d, opsi_e, jawaban,jenis');
        $this->db->from('tr_soal');
  
        $this->db->where('tmujian_id', $id);
        $this->db->order_by($order);
        
        return $this->db->get()->result();
    }
	
	 public function HslUjian($id, $tmsiswa_id)
    {
        $this->db->select('*, UNIX_TIMESTAMP(tgl_selesai) as waktu_habis');
        $this->db->from('h_ujian');
        $this->db->where('tmujian_id', $id);
        $this->db->where('tmsiswa_id', $tmsiswa_id);
        return $this->db->get();
    }
	
	
	  public function ambilSoal($pc_urut_soal1, $pc_urut_soal_arr)
    {
        $this->db->select("*,id as id_soal, {$pc_urut_soal1} AS jawaban");
        $this->db->from('tr_soal');
        $this->db->where('id', $pc_urut_soal_arr);
         $data =  $this->db->get()->row();
         if(!is_null($data)){
            return '';
         }else{

            return $data;
         }
    }
	
	  public function getJawaban($id_tes)
    {
        $this->db->select('list_jawaban');
        $this->db->from('h_ujian');
        $this->db->where('id', $id_tes);
        return $this->db->get()->row()->list_jawaban;
    }
	
	 public function getSoalById($id)
    {
        return $this->db->get_where('tr_soal', ['id' => $id])->row();
    }
	

	
    
	public function jml($table,$p=null){
		
		      $pengaturan_id  = $this->Reff->set();
			 $cek = $this->db->query("select count(id) as jml from $table where pengaturan_id='{$pengaturan_id}'")->row();
		 
			 return $cek->jml;
			  
			 
		 
		
		
		
	}
	
	public function grid($paging){
      
	   
		$this->db->select("*");
        $this->db->from('log');
        $this->db->where("UPPER(keterangan) LIKE '%LOGIN%' ");
    
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("tanggal","DESC");
					 
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
	
	
	
}
