<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumen extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		if(!$this->session->userdata("is_login")){
			    
			echo $this->Reff->sessionhabis();
			exit();
		
	  }
		  $this->load->model('M_ruang','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('auditor/page_header',$data);	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['title']   = "Permintaan Dokumen ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			    
			   
				$no = $i++;
				$records["data"][] = array(
					$no,
			
              	
					$val['nama'],
					$val['bentuk'],
					
					
					
				    '
					<div class="btn-group" role="group">
					 <button type="button" class="btn btn-primary btn-sm  ubahmodal" datanya="'.$val['id'].'" urlnya="'.site_url("dokumen/form").'" target="#loadform" data-toggle="modal" data-target="#defaultModal">
                                    <i class="fa fa-pencil"></i>
                      </button>
					  <button type="button" class="btn btn-danger btn-sm  hapus" datanya="'.$val['id'].'" urlnya="'.site_url("dokumen/hapus").'">
                                    <i class="fa fa-trash"></i>
                      </button>
					  </div>
					  '
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	public function form(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("tr_persyaratan",array("id"=>$id));
			   
		   }
		$this->load->view("form",$data);
		
	}
	
	
	
	
	public function save(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NIP lain.');
         $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
         $this->form_validation->set_message('numeric', '%s: Harus terdiri dari Angka');
				 
		 
			
				$config = array(
				    
				    array('field' => 'f[bentuk]', 'label' => 'Bentuk Dokumen    ', 'rules' => 'trim|required'),
					
				    array('field' => 'f[nama]', 'label' => 'Nama Dokumen ', 'rules' => 'trim|required'),
					
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
				
				             if(empty($id)){
								 $this->db->set("kegiatan_id",$_SESSION['kegiatan_id']);
								 $this->db->insert("tr_persyaratan",$f);
							 }else{
								$this->m->where("id",$id); 
								$this->m->update("tr_persyaratan"); 
								 
							 }
							 
						
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}
	
	
	public function hapus(){
		
		$id = $this->input->get_post("id",true);
		
		$this->db->delete("tr_persyaratan",array("id"=>$id));
		echo "sukses";
	}
	
	
	// Data Siswa 
	
	public function datasiswa()
	{  
	    
		  
		  
	    		
             $trkelas_id            = $this->input->get_post("id",true);	
		     $data['trkelas_id']    = $trkelas_id;
			 $this->load->view('datasiswa',$data);
		
		
	

	}
	
	 
}
