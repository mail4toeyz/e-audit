<div class="pagetitle">
  <h1><?php echo $title; ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.html">e-Audit</a></li>

      <li class="breadcrumb-item active"><?php echo $title; ?></li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="card">
        <div class="card-body">

          <!-- Bordered Tabs Justified -->
          <ul class="nav nav-tabs nav-tabs-bordered d-flex" id="borderedTabJustified" role="tablist">
            <li class="nav-item flex-fill" role="presentation">
              <button class="nav-link w-100 active" id="home-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-home" type="button" role="tab" aria-controls="home" aria-selected="true">Data Usulan Kegiatan</button>
            </li>
            <li class="nav-item flex-fill" role="presentation">
              <button class="nav-link w-100" id="profile-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Realisasi Keuangan </button>
            </li>
            <!-- <li class="nav-item flex-fill" role="presentation">
              <button class="nav-link w-100" id="contact-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Hasil Pengecekan Anggaran</button>
            </li> -->
          </ul>
          <div class="tab-content pt-2" id="borderedTabJustifiedContent">
            <div class="tab-pane fade show active" id="bordered-justified-home" role="tabpanel" aria-labelledby="home-tab">

              <div class="row p-2 ">
                <div class="col-md-6">
                  <table class="table">

                    <tr>
                      <td> Jenis Kegiatan </td>
                      <td>:</td>
                      <td> <?php echo $this->Reff->get_kondisi(array("id" => $data->jenis), "jenis_sub", "nama"); ?> </td>
                    </tr>

                    <tr>
                      <td> Nama Kegiatan </td>
                      <td>:</td>
                      <td> <?php echo $data->judul; ?> </td>
                    </tr>

                    <tr>
                      <td> Wilayah </td>
                      <td>:</td>
                      <td> <?php echo $this->Reff->get_kondisi(array("id" => $data->provinsi_id), "provinsi", "nama"); ?> - <?php echo $this->Reff->get_kondisi(array("id" => $data->kota_id), "kota", "nama"); ?></td>
                    </tr>



                  </table>

                </div>

                <div class="col-md-6">
                  <table class="table">
                    <tr>
                      <td> Periode </td>
                      <td>:</td>
                      <td> <?php echo $this->Reff->formattanggalstring($data->tgl_mulai); ?> - <?php echo $this->Reff->formattanggalstring($data->tgl_selesai); ?></td>
                    </tr>
                    <tr>
                      <td> Satuan Kerja </td>
                      <td>:</td>
                      <td>
                        <ol start="1">
                          <?php
                          $satker_id = explode(",", $data->satker_id);
                          foreach ($satker_id as $satker) {

                          ?><li><?php echo $this->Reff->get_kondisi(array("id" => $satker), "satker", "nama"); ?> - <?php echo $this->Reff->get_kondisi(array("id" => $satker), "satker", "kode"); ?></li><?php
                                                                                                                                                                                                        }
                                                                                                                                                                                                          ?>

                        </ol>


                      </td>
                    </tr>


                  </table>

                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="bordered-justified-profile" role="tabpanel" aria-labelledby="profile-tab">

              <div class="table-responsive" id="load_sbm">
                <table class="table table-hover table-striped table-bordered">
                  <thead>
                    <tr>
                      <th rowspan="2"> # </th>
                      <th rowspan="2"> Nama </th>

                      <th rowspan="2"> Gol </th>
                      <th rowspan="2"> Jabatan </th>
                      <th rowspan="2"> HP </th>
                      <th colspan="5"> Perhitungan SBM </th>
                      <th rowspan="2"> Anggaran </th>
                    </tr>

                    <tr>
                      <th>BST</th>
                      <th>Transport</th>
                      <th>Hotel</th>
                      <th>UH</th>
                      <th>Representatif</th>
                      <th>Transfer</th>
                      <th>Realisasi</th>
                      <th>Sisa</th>
                    </tr>

                  </thead>
                  <tbody>

                    <?php
                    $pegawaiJabatan = $this->db->query("SELECT * from tr_kegiatanJabatan where kegiatan_id='" . $data->id . "'")->result();
                    $no = 1;
                    $total = 0;
                    $bst = 512000;
                    $trans = 0;
                    foreach ($pegawaiJabatan as $r) {
                      $dataPegawai = $this->db->get_where("pegawai_simpeg", array("id" => $r->pejabat_id))->row();
                      $total = $total + $r->anggaran;
                      $trans = $trans + $r->uh * 0.75
                    ?>
                      <tr>
                        <td> <?php echo $no++; ?></td>
                        <td> <?php echo $dataPegawai->NAMA_LENGKAP; ?></td>

                        <td> <?php echo $dataPegawai->GOL_RUANG; ?></td>
                        <td> <?php echo $this->Reff->get_kondisi(array("id" => $r->jabatan), "jabatan", "nama"); ?></td>
                        <td>

                          <?php echo $r->waktu; ?>

                        </td>

                        <td>
                          <?php echo $this->Reff->formatuang2($r->bst); ?>
                        </td>

                        <td>

                          <?php echo $this->Reff->formatuang2($r->transport); ?>

                        </td>

                        <td>
                          <?php
                          echo  $this->Reff->formatuang2($r->hotel);
                          ?>
                        </td>

                        <td>
                          <?php
                          echo  $this->Reff->formatuang2($r->uh);
                          ?>
                        </td>
                        <td>
                          <?php echo $this->Reff->formatuang2($r->representatif); ?>
                        </td>
                        <td>
                          <?php echo $this->Reff->formatuang2($r->anggaran); ?>
                        </td>
                        <td>
                          <?php echo $this->Reff->formatuang2($r->uh * 0.75); ?>
                        </td>
                        <td>
                          <input type="text" class="form-control keychange" value="" id="rea<?php echo $r->id; ?>" onkeypress="return FormatCurrency(this)" data_id="<?php echo $r->id; ?>">
                        </td>
                        <td><?php echo $this->Reff->formatuang2($r->anggaran - ($r->uh * 0.75)); ?> </td>
                      </tr>



                    <?php
                    }
                    ?>

                    <tr style="font-weight:bold">
                      <td colspan="10"> Total Anggaran </th>
                      <td> <?php echo $this->Reff->formatuang2($total); ?> </th>
                      <td> <?php echo $this->Reff->formatuang2($trans); ?> </th>
                    </tr>

                  </tbody>
                </table>
              </div>

              <div class="alert alert-warning">
                <form method="post" action="" enctype="multipart/form-data" id="myform">

                  <div>
                    <input type="file" id="file" name="file" />
                    <input type="button" class="button" value="Bukti Realisasi" id="but_upload">
                  </div>
                </form>
              </div>


            </div>



          </div><!-- End Bordered Tabs Justified -->

        </div>
      </div>


    </div>
  </div>
</section>


<script>
  $(document).off("change", ".hari").on("change", ".hari", function() {

    var kegiatan_id = $("#kegiatan_id").val();
    var id = $(this).attr("data_id");
    var hari = $(this).val();
    var bst = HapusTitik($("#bst" + id).val());
    var transport = HapusTitik($("#transport" + id).val());
    var hotel = HapusTitik($("#hotel" + id).val());
    var uh = HapusTitik($("#uh" + id).val());
    var rep = HapusTitik($("#rep" + id).val());

    loading();
    $.post("<?php echo site_url("jadwal/savePeriode"); ?>", {
      id: id,
      hari: hari,
      kegiatan_id: kegiatan_id,
      bst: bst,
      transport: transport,
      hotel: hotel,
      uh: uh,
      rep: rep
    }, function(data) {

      $("#load_sbm").html(data);
      jQuery.unblockUI({});
    })


  });

  $(document).off("change", ".keychange").on("change", ".keychange", function(e) {

    //  if (e.key === 'Enter' || e.keyCode === 13) {
    var kegiatan_id = $("#kegiatan_id").val();
    var id = $(this).attr("data_id");

    var hari = HapusTitik($("#hari" + id).val());
    var bst = HapusTitik($("#bst" + id).val());
    var transport = HapusTitik($("#transport" + id).val());
    var hotel = HapusTitik($("#hotel" + id).val());
    var uh = HapusTitik($("#uh" + id).val());
    var rep = HapusTitik($("#rep" + id).val());

    loading();
    $.post("<?php echo site_url("jadwal/savePeriode"); ?>", {
      id: id,
      hari: hari,
      kegiatan_id: kegiatan_id,
      bst: bst,
      transport: transport,
      hotel: hotel,
      uh: uh,
      rep: rep
    }, function(data) {

      $("#load_sbm").html(data);
      jQuery.unblockUI({});
    })


    // }


  });
</script>