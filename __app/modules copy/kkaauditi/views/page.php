<?php 
     if($_SESSION['group_id'] =="ketua"){

        ?>
<style>
 
        
    select,textarea,input{
    pointer-events: none;
	background-color: transparent;
   
        }
        .required{

          color:red;
        }
        .btn{
          display:none;
        }
		
 </style>

 <?php 
	 }
?>


<div id="showform"></div>

<main id="main" class="main">

<div class="pagetitle">
  <h1><?php echo $title; ?></h1>
  <nav>
	<ol class="breadcrumb">
	  <li class="breadcrumb-item"><a href="index.html">e-Audit</a></li>
	
	  <li class="breadcrumb-item active"><?php echo $title; ?></li>
	</ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
	<div class="col-lg-12">

	  <div class="card">
		<div class="card-body">
		<form action="javascript:void(0)" method="post" id="simpanpka" name="simpanpka" url="<?php echo site_url("kka/save"); ?>">
		<input type="hidden" name="satker_id" value="<?php echo $satker->id; ?>">
		  <h5 class="card-title"> 
			<div style="float:right">
		    <div class="btn-group" role="group" aria-label="Basic mixed styles example">
               
                
              </div>
			</div>
			<br>
			<br>
			<hr>
			 <table class="table  table-bordered table-striped">
				 <tr>
					 <td> Nama Auditi </td>
					 <td> <?php echo $satker->nama; ?> </td>
				 </tr>
				 <tr>
					 <td> Tahun Anggaran </td>
					 <td> <?php echo $kegiatan->anggaran; ?> </td>
				 </tr>

			</table>
		  </h5>

		  <ul class="nav nav-tabs d-flex" id="myTabjustified" role="tablist">
			  <?php 
			   $kategoriPKA = $this->db->get("tm_kategori_pka")->result();
			     foreach($kategoriPKA as $rkat){
					$active = ($rkat->id==1) ? "active" :"";
			  ?>
                <li class="nav-item flex-fill" role="presentation">
                  <button class="nav-link w-100 <?php echo $active; ?>" id="home-tab" data-bs-toggle="tab" data-bs-target="#home-justified<?php echo $rkat->id; ?>" type="button" role="tab" aria-controls="home" aria-selected="true"><?php echo $rkat->slug; ?></button>
                </li>
			 <?php 
				 }
				?>
               
              </ul>
              <div class="tab-content pt-2" id="myTabjustifiedContent">
			  <?php 
			   $kategoriPKA = $this->db->get("tm_kategori_pka")->result();
			     foreach($kategoriPKA as $rkat){
					$active = ($rkat->id==1) ? "active" :"";
					
					$satker_id   = $satker->id;
					$kegiatan_id = $_SESSION['kegiatan_id'];
					$penyusun    = $_SESSION['idAuditor'];
					$this->db->where("kategori_pka",$rkat->id);
					$this->db->where("satker_id",$satker_id);
					$this->db->where("kegiatan_id",$kegiatan_id);
					//$this->db->where("penyusun",$penyusun);
					
					$datakategoriPKA  = $this->db->get("tr_kka")->row();

					if(is_null($datakategoriPKA) && $_SESSION['group_id'] =="anggota"){

						?>
						<div class="alert alert-danger bg-danger text-light border-0 alert-dismissible fade show" role="alert">
									Ketua TIM belum menyusun Program Kerja Audit   <?php echo $rkat->nama; ?>
						</div>

						<?php

					}else{
					
			  ?>
                <div class="tab-pane fade show <?php echo $active; ?>" id="home-justified<?php echo $rkat->id; ?>" role="tabpanel" aria-labelledby="home-tab">
					<center><h5 style="font-weight:bold"> <?php echo $rkat->slug; ?></h5></center>
					
						

						<div class="table-responsive">
					  	 <table class="table  table-bordered table-striped">
							<thead>
								<tr>
									<th>No</th>
									<th>Key Performance Indicator (KPI)</th>
									<th>Status </th>
									<th>Upload </th>
									<th>Notisi </th>
									
									<!-- <th rowspan="2">Aksi</th> -->
				 				</tr>
								
				 			</thead>
							<tbody>
								 <?php 
								  $uraian = $this->db->query("SELECT * from tm_kka where kategori_id='{$rkat->id}' and jenis !=0 ")->result();
								  	$no=1;
								    foreach($uraian as $urow){
										$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
										$this->db->where("satker_id",$_SESSION['satker_id']);
										$this->db->where("kka_id",$urow->id);
									    $kegiatanPersyaratan = $this->db->get("tr_kegiatan_persyaratan")->row();
										if(!is_null($kegiatanPersyaratan)){
											$status ='<span class="badge bg-primary"><i class="bi bi-check-circle-fill me-1"></i> Sudah diupload</span>';
										}else{
											$status ='<span class="badge bg-danger"><i class="bi bi-exclamation-octagon me-1"></i> Belum diupload</span>';
										}

										$this->db->where("satker_id",$_SESSION['satker_id']);
										$this->db->where("kka_id",$urow->id);
										$this->db->where("kegiatan_id",$_SESSION['kegiatan_id']);
										$notisi = $this->db->get("tr_notisi")->row();
										$dataNotisi = "";
											if(!is_null($notisi)){
												
												$dataNotisi = '<span class="badge border-danger border-1 text-danger" style="text-align:left"> Temuan : <br>'.$this->Reff->get_kondisi(array("jenis" => $notisi->kode_temuan), "tm_temuan", "deskripsi").' </span>';
												$dataNotisi .= '<br><span class="badge border-primary border-1 text-danger" style="text-align:left"> Rekomendasi : <br> '.$this->Reff->get_kondisi(array("id" => $notisi->kode_rekomendasi), "tm_rekomendasi", "nama").' </span> ';
											
												$dataNotisi .= '<br><span class="badge border-secondary border-1 text-secondary">'. $notisi->keterangan."</span>";
												$dataNotisi .= '<br><button type="button" class="btn btn-danger btn-sm rounded-pill tanggapan" type="button" data-bs-toggle="modal" data-bs-target="#largeModalNotisi" data-id="'.$urow->id.'" >Tanggapan</button>';
											
									
												   
											}
										

										?>
										 <tr id="<?php echo $rkat->id; ?><?php echo $urow->id; ?>">
											<td><?php echo $no++; ?></td>
											<td><input type="hidden" name="uraian<?php echo $rkat->id; ?><?php echo $urow->id; ?>" class="form-control" value="<?php echo  isset($dataUraianPKA->uraian) ? $dataUraianPKA->uraian : $urow->nama; ?>"><?php echo  isset($dataUraianPKA->uraian) ? $dataUraianPKA->uraian : $urow->nama; ?></td>
											<td><?php echo $status; ?></td>
											<td><button class="btn btn-danger uploadDokumen" data-id="<?php echo $urow->id; ?>" type="button" data-bs-toggle="modal" data-bs-target="#largeModal"><i class="fa fa-upload"></i> </button></td>
											<td><?php echo $dataNotisi; ?></td>
											
											
										</tr>


										<?php 


									}
								 ?>

				 			</tbody>
				   		 </table>

				 		</div>
						
					
                 
                </div>
				<?php 
				 }
				}
				 ?>
               
              </div>
		 

		  <!-- Default Table -->
		</form>
		 
		</div>
	  </div>


	</div>

  </div>
</section>

</main>


<div class="modal fade" id="largeModal" tabindex="-1">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Upload Dokumen</h5>
                      
                    </div>
                    <div class="modal-body" id="load-body">
                     
                    </div>
					<div class="modal-footer">
						<button type="button" id="simpanBerkas" class="btn btn-primary" data-dismiss="modal">Simpan</button>
					</div>
                   
                  </div>
                </div>
              </div>


<div class="modal fade" id="largeModalNotisi">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Tanggapan </h5>
                      
                    </div>
                    <div class="modal-body" id="load-bodyNotisi">
					Mohon Tunggu ..
                    </div>
					<div class="modal-footer">
						<button type="button"  class="btn btn-default" data-bs-dismiss="modal">Tutup</button>
					</div>
                   
                  </div>
                </div>
 </div>
	
<script type="text/javascript">

$(document).on("click",".tanggapan",function(){
				var id 		  = $(this).data("id");
				var satker_id = "<?php echo $_SESSION['satker_id']; ?>";

				$.post("<?php echo site_url('kkaauditi/notisi'); ?>",{id:id,satker_id:satker_id},function(data){

					$("#load-bodyNotisi").html(data);

				})


});


  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					
					
					"ajax":{
						url :"<?php echo site_url("dokumen/grid"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						data.trkelas_id = $("#trkelas_id").val();
						data.keyword = $("#keyword").val();
						data.ajaran = $("#ajaran").val();
						
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
				
				$(document).on("input","#keyword",function(){
	  
				  dataTable.ajax.reload(null,false);	
				  
			  });
			  $(document).on("change","#trkelas_id,#ajaran",function(){
	  
				  dataTable.ajax.reload(null,false);	
				  
			  });
	
			  $(document).on("change","#dikerjakanolehRencana",function(){
				var nilai = $(this).val();
			 	 $(".dikerjakanolehRencana").val(nilai);

			  });

			  $(document).on("change","#dikerjakanolehRealisasi",function(){
				var nilai = $(this).val();
			 	 $(".dikerjakanolehRealisasi").val(nilai);

			  });
			  $(document).on("change","#waktuRencana",function(){
				var nilai = $(this).val();
			 	 $(".waktuRencana").val(nilai);

			  });
			  $(document).on("change","#waktuRealisasi",function(){
				var nilai = $(this).val();
			 	 $(".waktuRealisasi").val(nilai);

			  });

			  $(document).on("input",".skor",function(){
				var nilai 	 = $(this).val();
				var id   	 = $(this).data("id");
				var bobot    = $("#bobot"+id).val();

				if(!isNaN(nilai)){

					if(nilai <= 1){
						var capaian = nilai * bobot;
				  		$("#capaian"+id).val(capaian);

					}else{
						alertify.warning("Tidak melebihi angka 1");	
					}
				   

				}else{
					alertify.warning("Masukkan berupa angka, jika ada komma, masukkan dengan tanda titik. Contoh : 0.750");

				}
			 	 

			  });

			  $(document).on("click",".uploadDokumen",function(){
				var id = $(this).data("id");

				$.post("<?php echo site_url('kkaauditi/berkas'); ?>",{id:id},function(data){

					$("#load-body").html(data);

				})


			  })

			  $(document).on('submit', 'form#simpanpka', function (event, messages) {
				event.preventDefault()
				var form   = $(this);
				var urlnya = $(this).attr("url");
				loading();
					$.ajax({
						type: "POST",
						url: urlnya,
						data: form.serialize(),
						success: function (response, status, xhr) {
							var ct = xhr.getResponseHeader("content-type") || "";
							if (ct == "application/json") {
						
							
							
								toastr.error(response.message, "Gagal  , perhatikan !  ", {
										"timeOut": "0",
										"extendedTImeout": "0",
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-top-right",
										"onclick": null,
										"showDuration": "10000",
										"hideDuration": "10000",
										"timeOut": "50000",
										"extendedTimeOut": "10000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut"
									});
								
							} else {
								
								toastr.success("Data Berhasil disimpan", "Sukses !", {
										"timeOut": "0",
										"extendedTImeout": "0",
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-top-right",
										"onclick": null,
										"showDuration": "10000",
										"hideDuration": "10000",
										"timeOut": "50000",
										"extendedTimeOut": "10000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut"
									});
								
							
							
							
							}
							
							jQuery.unblockUI({ });
						}
					});

					return false;
				});

				$(document).on("click","#simpanBerkas",function(){
						location.reload();


					})


</script>
				