  <div class="row">
  <div class="col-md-4">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Tahun Pelajaran dan Semester</h4>
                  <p class="card-category"> jika masuk tahun pelajaran dan semester baru, silahkan atur disini  </p>
                </div>
                <div class="card-body">
                  <form  method="post" action="<?php echo site_url("schoolmadrasah/saves"); ?>">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <select class="form-control" name="f[ajaran]" style="color:black">
						    <?php 
							 
							  
							    for($a=2019; $a<=2025; $a++){
									
									?><option value="<?php echo $a; ?>" <?php echo ($this->Reff->ajaran()==$a) ? "selected" : ""; ?>>Tahun Pelajaran <?php echo $a; ?>/<?php echo $a+1; ?></option><?php 
									
								}
								
								?>
								</select>
						  
						  
						  
                        </div>
                      </div>
                     
                      
                    </div>
					
					<div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <select class="form-control" name="f[semester]" style="color:black">
						    <?php 
							  $semester = array("1"=>"Semester Ganjil","2"=>"Semester Genap");
							  
							    foreach($semester as $i=>$r){
									
									?><option value="<?php echo $i; ?>" <?php echo ($this->Reff->semester()==$i) ? "selected" : ""; ?>> <?php echo $r; ?></option><?php 
									
								}
								
								?>
								</select>
						  
						  
						  
                        </div>
                      </div>
                     
                      
                    </div>
                    
				
                  
                    <button type="submit" class="btn btn-success pull-right">Simpan Pengaturan</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
			
			
			<div class="col-md-4">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Notification Realtime</h4>
                  <p class="card-category"> Jika Aplikasi terasa berat, silahkan nonaktifkan Notifikasi realtime untuk meningkatkan performa aplikasi </p>
				  
				    
					
                </div>
                <div class="card-body">
                   <?php 
				     if($data->verifikasi==1){
						
							
						$tombol ="Aktifkan Notifikasi Realtime";
						$nilai  ="0";
					 }else{
						 
						$tombol ="Nonaktifkan Notifikasi Realtime";
						$nilai  ="1";
					 }
				  ?>
                     <center>
                    <button type="button" id="notifikasireal" datanya="<?php echo $nilai; ?>" class="btn btn-success pull-right"> <?php echo $tombol; ?> </button>
					
					<button type="button" id="resetlogin" class="btn btn-success pull-right"> Reset semua Login (Offline) </button>
					</center>
				
                   
                </div>
              </div>
            </div>
			
			<div class="col-md-4">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Hapus Advance </h4>
                  <p class="card-category"> Hati-hati dengan tombol ini, setelah dihapus data tidak dapat dikembalikan, sebelum menghapus pastikan sudah melakukan backup database, kami tidak bertanggung jawab atas kehilangan data</p>
                </div>
                <div class="card-body">
                 
                    
				
                   <center>
                    <button type="button" id="hapusdataguru" class="btn btn-success pull-right"> Hapus semua data guru</button>
					 
                   </center>
                </div>
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Hapus Advance </h4>
                  <p class="card-category"> Hati-hati dengan tombol ini, setelah dihapus data tidak dapat dikembalikan, sebelum menghapus pastikan sudah melakukan backup database, kami tidak bertanggung jawab atas kehilangan data </p>
                </div>
                <div class="card-body">
                 
                    <?php 
					  $kelasrow = $this->Reff->kelas();
					   $ajaran = $this->Reff->ajaran();
						 foreach($kelasrow as $i=>$r){
							 $ruangkelas = $this->db->get_where("tr_ruangkelas",array("trkelas_id"=>$r->id,"ajaran"=>$ajaran))->result();
							  foreach($ruangkelas as $rk){
                      ?>
					 <button type="button" class="hapusdatasiswa btn btn-danger btn-sm" kelas="<?php echo $r->id; ?>" rombel="<?php echo $rk->id; ?>" title="Kelas <?php echo $r->nama; ?> Rombel <?php echo $rk->nama; ?>" class=" "> Hapus data siswa Kelas <?php echo $r->nama; ?> Rombel <?php echo $rk->nama; ?></button>
					 
						 <?php
							  }
						 }
						 ?>
                   </center>
                </div>
              </div>
           </div>
		   
		   <div class="col-md-6">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Pengaturan Zona Wilayah Indonesia  </h4>
                  <p class="card-category"> Untuk mendapatkan tanggal yang sesuai dengan Wilayah Anda, silahkan atur dibawah ini  </p>
				  
				  <b> 
				  <?php 
					
					  echo $this->Reff->formattimestamp(date("Y-m-d :H:i:s"));
					
					?>
				</b>
                </div>
                <div class="card-body" style="text-align:left">
                 
                     <center>
					 <div class="custom-control custom-radio">
					  <input type="radio" <?php echo ($data->sk=="Jakarta") ? "checked":""; ?> class="custom-control-input zonasiwaktu" id="wib" value="Jakarta" name="zonasi">
					  <label class="custom-control-label" for="wib">Waktu Indonesia Barat (WIB) </label>
					</div>
					
					 <div class="custom-control custom-radio">
					  <input type="radio" <?php echo ($data->sk=="Ujung_Pandang") ? "checked":""; ?> class="custom-control-input zonasiwaktu" id="wita" value="Ujung_Pandang" name="zonasi">
					  <label class="custom-control-label" for="wita">Waktu Indonesia Tengah (WITA) </label>
					</div>
					
					 <div class="custom-control custom-radio">
					  <input type="radio" <?php echo ($data->sk=="Jayapura") ? "checked":""; ?> class="custom-control-input zonasiwaktu" id="wit" value="Jayapura" name="zonasi">
					  <label class="custom-control-label" for="wit">Waktu Indonesia Timur (WIT) </label>
					</div>
					
					
                   </center>
                </div>
              </div>
           </div>
		   
		   
     </div>
          
			
<script>

     $(document).off("click",".zonasiwaktu").on("click",".zonasiwaktu",function(){
	  var datanya = $(this).val();
	  
	 
		  loading();
		  
		  $.post("<?php echo site_url("schoolmadrasah/zonasiwaktu"); ?>",{datanya:datanya},function(){
			  alertify.alert("Perubahan berhasil dilakukan");
			 jQuery.unblockUI({ }); 
			 
			 location.reload();
			  
		  })
		  
		  
		
	  
	  
	  
  })
  
  
   $(document).off("click","#notifikasireal").on("click","#notifikasireal",function(){
	  var datanya = $(this).attr("datanya");
	  alertify.confirm("Apakah Anda yakin ?",function(){
		  loading();
		  
		  $.post("<?php echo site_url("schoolmadrasah/notifikasireal"); ?>",{datanya:datanya},function(){
			  alertify.alert("Perubahan berhasil dilakukan");
			 jQuery.unblockUI({ }); 
			 
			 location.reload();
			  
		  })
		  
		  
		  
		  
	  })
	  
	  
	  
  })
  
  
  $(document).off("click","#hapusdataguru").on("click","#hapusdataguru",function(){
	  
	  alertify.confirm("Kami tidak bertanggung jawab atas kehilangan data, pastikan Anda sudah melakukan backup database, apakah Anda yakin akan menghapus data guru ?",function(){
		  loading();
		  $.post("<?php echo site_url("schoolmadrasah/hapusguru"); ?>",function(){
			  alertify.alert("Seluruh data guru berhasil dihapus");
			 jQuery.unblockUI({ }); 
			  
		  })
		  
		  
		  
		  
	  })
	  
	  
	  
  })
  
   $(document).off("click",".hapusdatasiswa").on("click",".hapusdatasiswa",function(){
	  
	   var kelas = $(this).attr("kelas");
	   var rombel = $(this).attr("rombel");
	   var title  = $(this).attr("title");
	  alertify.confirm("Kami tidak bertanggung jawab atas kehilangan data, pastikan Anda sudah melakukan backup database, apakah Anda yakin akan menghapus "+title+" ?",function(){
		  loading();
		  $.post("<?php echo site_url("schoolmadrasah/hapusdatasiswa"); ?>",{kelas:kelas,rombel:rombel},function(){
			  alertify.alert(title+" Berhasil dihapus");
			 jQuery.unblockUI({ }); 
			  
		  })
		  
		  
		  
		  
	  })
	  
	  
	  
  })
  
   $(document).off("click","#resetlogin").on("click","#resetlogin",function(){
	  
	 
	  alertify.confirm("Apakah Anda yakin akan mereset semua login , menjadikan semua user offline jika lupa melogout aplikasi ?",function(){
		  loading();
		  $.post("<?php echo site_url("schoolmadrasah/resetlogin"); ?>",function(){
			  alertify.alert(" Semua login berhasil direset");
			 jQuery.unblockUI({ }); 
			  
		  })
		  
		  
		  
		  
	  })
	  
	  
	  
  })

</script>
           