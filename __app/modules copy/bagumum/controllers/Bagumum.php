<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bagumum extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('exportpdf_helper');
		if (!$this->session->userdata("is_login")) {

			echo $this->Reff->sessionhabis();
			exit();
		}
		$this->load->model('M_dashboard', 'm');
	}

	function _template($data)
	{
		$this->load->view('bagumum/page_header', $data);
	}

	public function index()
	{


		$ajax            = $this->input->get_post("ajax", true);

		$data['title']   = "Dashboard Tata Usaha  ";

		$data['categorie_xAxis'] = "";
		$data['json_anggota']    = "";
		$pie   					 = "";
		$data['title']           = "Persentase Jumlah Permadrasah ";
		$jenis = $this->db->get("jabatan")->result();

		foreach ($jenis as $row) {

			$jml = $this->db->query("SELECT count(id) as jml from pegawai where jabatan_id='{$row->id}'")->row();
			$pm = $jml->jml;
			$data['json_anggota'] .= "," . $pm;
			$data["categorie_xAxis"] .= ",'" . $row->nama . "";
			$tempo = array("INDEXES" => $row->nama, "Jumlah" => $pm);

			$pie          .= ",['" . $row->nama . "'," . (($pm / count($jenis)) * 100) . "]";


			$grid[] = $tempo;
		}


		$data['statistik'] = " Pendaftar SNPDB  ";
		$data['header']    = $data['statistik'];
		$data['categorie_xAxis'] = "Jumlah Pendaftar ";

		$data['json_pie_chart']  =  substr($pie, 1);
		$data['json_anggota']    = substr($data['json_anggota'], 1);

		$data['grid'] = $grid;
		if (!empty($ajax)) {

			$this->load->view('page_default', $data);
		} else {


			$data['konten'] = "page_default";

			$this->_template($data);
		}
	}



	public function penugasan()
	{




		$ajax            = $this->input->get_post("ajax", true);

		$data['title']   = "Data Usulan Penugasan ";
		if (!empty($ajax)) {

			$this->load->view('penugasan/page', $data);
		} else {


			$data['konten'] = "penugasan/page";

			$this->_template($data);
		}
	}

	public function gridPenugasan()
	{


		$iTotalRecords = $this->m->grid(false)->num_rows();

		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);

		$records = array();
		$records["data"] = array();

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;

		$datagrid = $this->m->grid(true)->result_array();


		$i = ($iDisplayStart + 1);
		foreach ($datagrid as $val) {

			$anggota = "<ol>";
			$dataAnggota = json_decode($val['anggota'], true);
			foreach ($dataAnggota as $r) {
				$anggota .= "<li>" . $this->Reff->get_kondisi(array("id" => $r), "pegawai_simpeg", "nama") . "</li>";
			}

			$anggota .= "</ol>";

			$satkerData = "<ol>";
			$satker =  explode(",", $val['satker_id']);
			foreach ($satker as $r) {
				$satkerData .= "<li>" . $this->Reff->get_kondisi(array("id" => $r), "satker", "nama") . "</li>";
			}

			$satkerData .= "</ol>";


			$approval = '<div class="btn-group">';
			$approval .= '<button type="button" class="btn btn-primary ubah" datanya="' . $val['id'] . '" urlnya="' . site_url("bagumum/approval") . '" ><i class="bi bi-folder"></i></button> ';
			$approval .= '<a class="btn btn-danger " href="' . site_url("bagumum/surattugas?id=" . base64_encode($val['id']) . "") . '" ><i class="bi bi-file-earmark-pdf-fill"></i></a> ';
			$approval .= '<button type="button" class="btn btn-success ubah" datanya="' . $val['id'] . '" urlnya="' . site_url("bagumum/upload") . '" >  <i class="bi bi-cloud-upload-fill"></i></button> ';
			$approval .= '</div>';

			if ($val['approval'] == 0) {
				// $approval ='<button type="button" class="btn btn-danger ubah" datanya="'.$val['id'].'" urlnya="'.site_url("irjen/approval").'" ><i class="bi bi-folder"></i></button> ';
			}





			$no = $i++;
			$records["data"][] = array(
				$no,


				$approval,
				$this->Reff->get_kondisi(array("id" => $val['jenis']), "tm_jenis", "nama"),

				$val['judul'],
				$this->Reff->get_kondisi(array("id" => $val['provinsi_id']), "provinsi", "nama"),
				$this->Reff->get_kondisi(array("id" => $val['kota_id']), "kota", "nama"),
				$satkerData,
				$this->Reff->formattanggalstring($val['tgl_mulai']) . " - " . $this->Reff->formattanggalstring($val['tgl_selesai']),

				$this->Reff->get_kondisi(array("id" => $val['penanggung_jawab']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['penanggung_jawab']), "pegawai_simpeg", "nama"),
				$this->Reff->get_kondisi(array("id" => $val['pengendali_mutu']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['pengendali_mutu']), "pegawai_simpeg", "nama"),
				$this->Reff->get_kondisi(array("id" => $val['pengendali_teknis']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['pengendali_teknis']), "pegawai_simpeg", "nama"),

				$this->Reff->get_kondisi(array("id" => $val['ketua']), "pegawai_simpeg", "nip_baru") . "-" . $this->Reff->get_kondisi(array("id" => $val['ketua']), "pegawai_simpeg", "nama"),
				$anggota







			);
		}

		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;

		echo json_encode($records);
	}


	public function approval()
	{

		$id = $this->input->get_post("id");
		$data = array();
		if (!empty($id)) {

			$data['data']  = $this->Reff->get_where("kegiatan", array("id" => $id));
			$data['title']  =  "Approval Detail " . $data['data']->judul;
		}
		$this->load->view("penugasan/detail", $data);
	}

	public function upload()
	{

		$id = $this->input->get_post("id");
		$data = array();
		if (!empty($id)) {

			$data['data']  = $this->Reff->get_where("kegiatan", array("id" => $id));
			$data['title']  =  "Upload Surat Tugas  " . $data['data']->judul;
		}
		$this->load->view("penugasan/upload", $data);
	}

	public function save()
	{

		$this->form_validation->set_message('required', '{field} Wajib diisi. ');
		$this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
		$this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
		$this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');



		$config = array(


			array('field' => 'f[no_surat]', 'label' => 'Nomor Surat ', 'rules' => 'trim|required'),








		);

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == true) {

			$id = $this->input->get_post("id", true);
			$f = $this->input->get_post("f", true);
			$menimbang  = ($this->input->get_post("menimbang", true));
			$dasar  = ($this->input->get_post("dasar", true));
			$waktu  = ($this->input->get_post("waktu", true));

			$this->db->where("id", $id);
			$this->db->set("menimbang", $menimbang);
			$this->db->set("dasar", $dasar);
			$this->db->set("waktu", $waktu);
			$this->db->update("kegiatan", $f);
			echo "Data Berhasil disimpan";
		} else {


			header('Content-Type: application/json');
			echo json_encode(array('error' => true, 'message' => validation_errors()));
		}
	}

	public function surattugas()
	{

		$id = base64_decode($this->input->get_post("id"));

		$data['data']     = $this->Reff->get_where("kegiatan", array("id" => $id));

		$pdf_filename = 'Surat Tugas ' . $data['data']->judul . ' .pdf';

		$user_info = $this->load->view('penugasan/surattugas', $data, true);


		$output = $user_info;

		generate_pdf($output, $pdf_filename);
	}
}
