<div class="pagetitle">
  <h1><?php echo $title; ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.html">e-Audit</a></li>

      <li class="breadcrumb-item active"><?php echo $title; ?></li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="card">
        <div class="card-body">

          <!-- Bordered Tabs Justified -->
          <ul class="nav nav-tabs nav-tabs-bordered d-flex" id="borderedTabJustified" role="tablist">
            <li class="nav-item flex-fill" role="presentation">
              <button class="nav-link w-100 active" id="home-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-home" type="button" role="tab" aria-controls="home" aria-selected="true">Data Usulan Kegiatan</button>
            </li>
            <li class="nav-item flex-fill" role="presentation">
              <button class="nav-link w-100" id="profile-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Usulan Tim Kegiatan </button>
            </li>
            <li class="nav-item flex-fill" role="presentation">
              <button class="nav-link w-100" id="contact-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Approval Kegiatan</button>
            </li>
          </ul>
          <div class="tab-content pt-2" id="borderedTabJustifiedContent">
            <div class="tab-pane fade show active" id="bordered-justified-home" role="tabpanel" aria-labelledby="home-tab">

              <div class="row p-2 ">
                <div class="col-md-6">
                  <table class="table">

                    <tr>
                      <td> Jenis Kegiatan </td>
                      <td>:</td>
                      <td> <?php echo $this->Reff->get_kondisi(array("id" => $data->jenis), "jenis_sub", "nama"); ?> </td>
                    </tr>

                    <tr>
                      <td> Nama Kegiatan </td>
                      <td>:</td>
                      <td> <?php echo $data->judul; ?> </td>
                    </tr>

                    <tr>
                      <td> Wilayah </td>
                      <td>:</td>
                      <td> <?php echo $this->Reff->get_kondisi(array("id" => $data->provinsi_id), "provinsi", "nama"); ?> - <?php echo $this->Reff->get_kondisi(array("id" => $data->kota_id), "kota", "nama"); ?></td>
                    </tr>



                  </table>

                </div>

                <div class="col-md-6">
                  <table class="table">
                    <tr>
                      <td> Periode </td>
                      <td>:</td>
                      <td> <?php echo $this->Reff->formattanggalstring($data->tgl_mulai); ?> - <?php echo $this->Reff->formattanggalstring($data->tgl_selesai); ?></td>
                    </tr>
                    <tr>
                      <td> Satuan Kerja </td>
                      <td>:</td>
                      <td>
                        <ol start="1">
                          <?php
                          $satker_id = explode(",", $data->satker_id);
                          foreach ($satker_id as $satker) {

                          ?><li><?php echo $this->Reff->get_kondisi(array("id" => $satker), "satker", "nama"); ?> - <?php echo $this->Reff->get_kondisi(array("id" => $satker), "satker", "kode"); ?></li><?php
                                                                                                                                                                                                        }
                                                                                                                                                                                                          ?>

                        </ol>


                      </td>
                    </tr>


                  </table>

                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="bordered-justified-profile" role="tabpanel" aria-labelledby="profile-tab">

              <table class="table table-hover table-striped table-bordered">
                <thead>
                  <tr>
                    <th rowspan="2"> # </th>
                    <th rowspan="2"> Nama </th>

                    <th rowspan="2"> Gol </th>
                    <th rowspan="2"> Jabatan </th>
                    <th rowspan="2"> HP </th>
                    <th colspan="4"> Perhitungan SBM </th>
                    <th rowspan="2"> Anggaran </th>
                  </tr>

                  <tr>
                    <th>BST</th>
                    <th>Transport</th>
                    <th>Hotel</th>
                    <th>UH</th>
                    <th>Representatif</th>
                  </tr>

                </thead>
                <tbody>

                  <?php
                  $pegawaiJabatan = $this->db->query("SELECT * from tr_kegiatanJabatan where kegiatan_id='" . $data->id . "'")->result();
                  $no = 1;
                  $total = 0;
                  $bst = 512000;
                  foreach ($pegawaiJabatan as $r) {
                    $dataPegawai = $this->db->get_where("pegawai_simpeg", array("id" => $r->pejabat_id))->row();
                    $total = $total + $r->anggaran;
                  ?>
                    <tr>
                      <td> <?php echo $no++; ?></td>
                      <td> <?php echo $dataPegawai->NAMA_LENGKAP; ?></td>

                      <td> <?php echo $dataPegawai->GOL_RUANG; ?></td>
                      <td> <?php echo $dataPegawai->SATKER_3; //$this->Reff->get_kondisi(array("id"=>$r->jabatan),"jabatan","nama"); 
                            ?></td>
                      <td>

                        <?php echo $r->waktu; ?>

                      </td>

                      <td>
                        <?php echo $this->Reff->formatuang2($r->bst); ?>
                      </td>

                      <td>

                        <?php echo $this->Reff->formatuang2($r->transport); ?>

                      </td>

                      <td>
                        <?php
                        echo  $this->Reff->formatuang2($r->hotel);
                        ?>
                      </td>

                      <td>
                        <?php
                        echo  $this->Reff->formatuang2($r->uh);
                        ?>
                      </td>

                      <td>
                        <?php echo $this->Reff->formatuang2($r->representatif); ?>
                      </td>
                      <td>
                        <?php echo $this->Reff->formatuang2($r->anggaran); ?>
                      </td>

                    </tr>


                  <?php


                  }
                  ?>

                  <tr style="font-weight:bold">
                    <td colspan="10"> Total Anggaran </th>
                    <td> <?php echo $this->Reff->formatuang2($total); ?> </th>
                  </tr>

                </tbody>
              </table>

            </div>
            <div class="tab-pane fade" id="bordered-justified-contact" role="tabpanel" aria-labelledby="contact-tab">

              <div class="alert alert-warning  alert-dismissible fade show" role="alert">
                <h4 class="alert-heading">Petunjuk !!! </h4>
                <p> Kegiatan yang Anda setujui akan dikirim ke Perencanaan dan Bagian umum untuk penerbitan Surat Tugas <br>
                  Kegiatan yang Anda tolak akan dikembalikan kebagian Tata Usaha Wilayah </p>
                <hr>
                <p class="mb-0">Silahkan pilih status Approval Anda dibawah ini, apabila ada catatan untuk usulan kegiatan ini silahkan isi pada form yang tersedia </p>

              </div>

              <?php

              ?>

              <form class="row g-3" action="javascript:void(0)" method="post" id="simpangambar" name="simpangambarmodal" url="<?php echo site_url("sekretaris/save"); ?>">
                <input type="hidden" class="form-control" name='id' value="<?php echo (isset($data)) ? $data->id : ""; ?>">
                <div class="col-12">
                  <label for="inputNanme4" class="form-label">Status Approval </label>
                  <select class="form-select" aria-label="Default select example" name="f[approval]">

                    <option>Pilih Status Approval</option>
                    <option value="1" <?php echo ($data->status_approval == 1 && $data->groups_id == 16) ? "selected" : ""; ?>>Setujui</option>
                    <option value="2" <?php echo ($data->status_approval == 2 && $data->groups_id == 16) ? "selected" : ""; ?>>Tolak Usulan</option>

                  </select>
                </div>
                <div class="col-12">
                  <label for="inputEmail4" class="form-label">Catatan (opsional)</label>
                  <textarea class="form-control" id="catatan" name="f[approval_catatan]"><?php echo ($data->groups_id == 16) ? $data->notes : ""; ?></textarea>
                </div>

                <div class="text-center">
                  <button type="submit" class="btn btn-outline-primary">Simpan </button>
                  <button type="button" class="btn btn-outline-primary" id="cancel"> <i class="bi bi-box-arrow-in-down-right"></i> Kembali </button>

                </div>
              </form><!-- Vertical Form -->



            </div>


          </div><!-- End Bordered Tabs Justified -->

        </div>
      </div>


    </div>
  </div>
</section>