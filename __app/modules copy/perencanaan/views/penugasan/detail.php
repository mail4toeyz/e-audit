<div class="pagetitle">
  <h1><?php echo $title; ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.html">e-Audit</a></li>

      <li class="breadcrumb-item active"><?php echo $title; ?></li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="card">
        <div class="card-body">

          <!-- Bordered Tabs Justified -->
          <ul class="nav nav-tabs nav-tabs-bordered d-flex" id="borderedTabJustified" role="tablist">
            <li class="nav-item flex-fill" role="presentation">
              <button class="nav-link w-100 active" id="home-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-home" type="button" role="tab" aria-controls="home" aria-selected="true">Data Usulan Kegiatan</button>
            </li>
            <li class="nav-item flex-fill" role="presentation">
              <button class="nav-link w-100" id="profile-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Usulan Tim Kegiatan </button>
            </li>
            <li class="nav-item flex-fill" role="presentation">
              <button class="nav-link w-100" id="contact-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Hasil Pengecekan Anggaran</button>
            </li>
          </ul>
          <div class="tab-content pt-2" id="borderedTabJustifiedContent">
            <div class="tab-pane fade show active" id="bordered-justified-home" role="tabpanel" aria-labelledby="home-tab">

              <div class="row p-2 ">
                <div class="col-md-6">
                  <table class="table">

                    <tr>
                      <td> Jenis Kegiatan </td>
                      <td>:</td>
                      <td> <?php echo $this->Reff->get_kondisi(array("id" => $data->jenis), "jenis_sub", "nama"); ?> </td>
                    </tr>

                    <tr>
                      <td> Nama Kegiatan </td>
                      <td>:</td>
                      <td> <?php echo $data->judul; ?> </td>
                    </tr>

                    <tr>
                      <td> Wilayah </td>
                      <td>:</td>
                      <td> <?php echo $this->Reff->get_kondisi(array("id" => $data->provinsi_id), "provinsi", "nama"); ?> - <?php echo $this->Reff->get_kondisi(array("id" => $data->kota_id), "kota", "nama"); ?></td>
                    </tr>



                  </table>

                </div>

                <div class="col-md-6">
                  <table class="table">
                    <tr>
                      <td> Periode </td>
                      <td>:</td>
                      <td> <?php echo $this->Reff->formattanggalstring($data->tgl_mulai); ?> - <?php echo $this->Reff->formattanggalstring($data->tgl_selesai); ?></td>
                    </tr>
                    <tr>
                      <td> Satuan Kerja </td>
                      <td>:</td>
                      <td>
                        <ol start="1">
                          <?php
                          $satker_id = explode(",", $data->satker_id);
                          foreach ($satker_id as $satker) {

                          ?><li><?php echo $this->Reff->get_kondisi(array("id" => $satker), "satker", "nama"); ?> - <?php echo $this->Reff->get_kondisi(array("id" => $satker), "satker", "kode"); ?></li><?php
                                                                                                                                                                                                        }
                                                                                                                                                                                                          ?>

                        </ol>


                      </td>
                    </tr>


                  </table>

                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="bordered-justified-profile" role="tabpanel" aria-labelledby="profile-tab">

              <div class="table-responsive" id="load_sbm">
                <table class="table table-hover table-striped table-bordered">
                  <thead>
                    <tr>
                      <th rowspan="2"> # </th>
                      <th rowspan="2"> Nama </th>

                      <th rowspan="2"> Gol </th>
                      <th rowspan="2"> Jabatan </th>
                      <th rowspan="2"> HP </th>
                      <th colspan="5"> Perhitungan SBM </th>
                      <th rowspan="2"> Anggaran </th>
                    </tr>

                    <tr>
                      <th>BST</th>
                      <th>Transport</th>
                      <th>Hotel</th>
                      <th>UH</th>
                      <th>Representatif</th>
                    </tr>

                  </thead>
                  <tbody>

                    <?php
                    $pegawaiJabatan = $this->db->query("SELECT * from tr_kegiatanJabatan where kegiatan_id='" . $data->id . "'")->result();
                    $no = 1;
                    $total = 0;
                    $bst = 512000;
                    foreach ($pegawaiJabatan as $r) {
                      $dataPegawai = $this->db->get_where("pegawai_simpeg", array("id" => $r->pejabat_id))->row();
                      $total = $total + $r->anggaran;
                    ?>
                      <tr>
                        <td> <?php echo $no++; ?></td>
                        <td> <?php echo $dataPegawai->NAMA_LENGKAP; ?></td>

                        <td> <?php echo $dataPegawai->GOL_RUANG; ?></td>
                        <td> <?php echo $dataPegawai->SATKER_3; //echo $this->Reff->get_kondisi(array("id" => $r->jabatan), "jabatan", "nama"); 
                              ?></td>
                        <td>


                          <input type="hidden" id="hari<?php echo $r->id; ?>" value="<?php echo $r->waktu; ?>">
                          <select class="hari form-control" data_id="<?php echo $r->id; ?>">
                            <option value="0">- 0 -</option>
                            <?php
                            for ($a = 1; $a <= 100; $a++) {

                            ?><option value="<?php echo $a; ?>" <?php echo ($a == $r->waktu) ? "selected" : ""; ?>><?php echo $a; ?> </option><?php
                                                                                                                                            }
                                                                                                                                              ?>


                          </select>

                        </td>

                        <td>
                          <input readonly type="text" class="form-control keychange" value="<?php echo ($r->bst != 0) ? $this->Reff->formatuang2($r->bst) :   $this->Reff->formatuang2($bst); ?>" id="bst<?php echo $r->id; ?>" onkeyup="return FormatCurrency(this)" data_id="<?php echo $r->id; ?>">
                        </td>

                        <td>
                          <?php
                          $transport = $this->db->get_where("sbm_pesawat", array("provinsi_tujuan" => $data->provinsi_id))->row();
                          ?>
                          <input readonly type="text" class="form-control keychange" value="<?php echo ($r->transport != 0) ? $this->Reff->formatuang2($r->transport) :   $this->Reff->formatuang2($transport->ekonomi); ?>" id="transport<?php echo $r->id; ?>" onkeypress="return FormatCurrency(this)" data_id="<?php echo $r->id; ?>">
                        </td>

                        <td>
                          <?php
                          $golongan = explode("/", $dataPegawai->GOL_RUANG);
                          $hotel = $this->db->get_where("sbm_hotel", array("provinsi_id" => $data->provinsi_id, "golongan" => $golongan[0]))->row();

                          ?>
                          <input readonly type="text" class="form-control keychange" value="<?php echo ($r->hotel != 0) ? $this->Reff->formatuang2($r->hotel) :   $this->Reff->formatuang2($hotel->harga); ?>" id="hotel<?php echo $r->id; ?>" onkeypress="return FormatCurrency(this)" data_id="<?php echo $r->id; ?>">
                        </td>

                        <td>
                          <?php
                          $uh = $this->db->get_where("sbm_uh", array("provinsi_id" => $data->provinsi_id))->row();
                          ?>
                          <input readonly type="text" class="form-control keychange" value="<?php echo ($r->uh != 0) ? $this->Reff->formatuang2($r->uh) :   $this->Reff->formatuang2($uh->harga); ?>" id="uh<?php echo $r->id; ?>" onkeypress="return FormatCurrency(this)" data_id="<?php echo $r->id; ?>">
                        </td>
                        <td>
                          <?php if ($dataPegawai->GOL_RUANG == "II/a" or $dataPegawai->GOL_RUANG == "I/a") { ?>
                            <input readonly type="text" class="form-control keychange" value="<?php echo $this->Reff->formatuang2($r->representatif); ?>" id="rep<?php echo $r->id; ?>" onkeypress="return FormatCurrency(this)" data_id="<?php echo $r->id; ?>">
                          <?php } else { ?>
                            <input readonly type="hidden" class="form-control keychange" value="<?php echo $this->Reff->formatuang2($r->representatif); ?>" id="rep<?php echo $r->id; ?>" onkeypress="return FormatCurrency(this)" data_id="<?php echo $r->id; ?>">
                          <?php } ?>

                        </td>
                        <td>
                          <?php echo $this->Reff->formatuang2($r->anggaran); ?>
                        </td>


                      </tr>


                    <?php


                    }
                    ?>

                    <tr style="font-weight:bold">
                      <td colspan="10"> Total Anggaran </th>
                      <td> <?php echo $this->Reff->formatuang2($total); ?> </th>
                    </tr>

                  </tbody>
                </table>
              </div>




            </div>
            <div class="tab-pane fade" id="bordered-justified-contact" role="tabpanel" aria-labelledby="contact-tab">

              <div class="alert alert-warning  alert-dismissible fade show" role="alert">
                <h4 class="alert-heading">Petunjuk !!! </h4>
                <p> Silahkan lakukan tindakan dibawah ini, apabila Anggaran tersedia data kegiatan akan dikirim ke Bagian Umum untuk Penerbitan Surat Tugas
                <p>

              </div>
              <!-- <table class="table">
                <tr>
                  <td> Catatan Approval Irjen </td>
                  <td>:</td>
                  <td> <?php echo $data->approval_catatan ?> </td>
                </tr>

                <tr>
                  <td> Tanggal Approval </td>
                  <td>:</td>
                  <td> <?php echo $this->Reff->formattimestamp($data->approval_tgl); ?> </td>
                </tr>


              </table> -->


              <form class="row g-3" action="javascript:void(0)" method="post" id="simpangambar" name="simpangambarmodal" url="<?php echo site_url("perencanaan/save"); ?>">
                <input type="hidden" class="form-control" name='id' id="kegiatan_id" value="<?php echo (isset($data)) ? $data->id : ""; ?>">
                <div class="col-12">
                  <label for="inputNanme4" class="form-label">Status Anggaran </label>
                  <select class="form-select" aria-label="Default select example" name="f[approval_perencanaan]">

                    <option>Pilih Status Approval</option>
                    <option value="1" <?php echo ($data->status_approval == 1 && $data->groups_id == 6) ? "selected" : ""; ?>>Anggaran Tersedia</option>
                    <option value="2" <?php echo ($data->status_approval == 2 && $data->groups_id == 6) ? "selected" : ""; ?>>Anggaran Tidak Tersedia</option>

                  </select>
                </div>
                <div class="col-12">
                  <label for="inputEmail4" class="form-label">Catatan (opsional)</label>
                  <textarea class="form-control" id="catatan" name="f[approval_catatan]"><?php echo ($data->groups_id == 6) ? $data->notes : ""; ?></textarea>
                </div>

                <div class="text-center">
                  <button type="submit" class="btn btn-outline-primary">Simpan </button>
                  <button type="button" class="btn btn-outline-primary" id="cancel"> <i class="bi bi-box-arrow-in-down-right"></i> Kembali </button>

                </div>
              </form><!-- Vertical Form -->



            </div>


          </div><!-- End Bordered Tabs Justified -->

        </div>
      </div>


    </div>
  </div>
</section>


<script>
  $(document).off("change", ".hari").on("change", ".hari", function() {

    var kegiatan_id = $("#kegiatan_id").val();
    var id = $(this).attr("data_id");
    var hari = $(this).val();
    var bst = HapusTitik($("#bst" + id).val());
    var transport = HapusTitik($("#transport" + id).val());
    var hotel = HapusTitik($("#hotel" + id).val());
    var uh = HapusTitik($("#uh" + id).val());
    var rep = HapusTitik($("#rep" + id).val());


    loading();
    $.post("<?php echo site_url("jadwal/savePeriode"); ?>", {
      id: id,
      hari: hari,
      kegiatan_id: kegiatan_id,
      bst: bst,
      transport: transport,
      hotel: hotel,
      uh: uh,
      rep: rep
    }, function(data) {

      $("#load_sbm").html(data);
      jQuery.unblockUI({});
    })


  });

  $(document).off("change", ".keychange").on("change", ".keychange", function(e) {

    //  if (e.key === 'Enter' || e.keyCode === 13) {
    var kegiatan_id = $("#kegiatan_id").val();
    var id = $(this).attr("data_id");

    var hari = HapusTitik($("#hari" + id).val());
    var bst = HapusTitik($("#bst" + id).val());
    var transport = HapusTitik($("#transport" + id).val());
    var hotel = HapusTitik($("#hotel" + id).val());
    var uh = HapusTitik($("#uh" + id).val());
    var rep = HapusTitik($("#rep" + id).val());

    loading();
    $.post("<?php echo site_url("jadwal/savePeriode"); ?>", {
      id: id,
      hari: hari,
      kegiatan_id: kegiatan_id,
      bst: bst,
      transport: transport,
      hotel: hotel,
      uh: uh,
      rep: rep
    }, function(data) {

      $("#load_sbm").html(data);
      jQuery.unblockUI({});
    })


    // }


  });
</script>