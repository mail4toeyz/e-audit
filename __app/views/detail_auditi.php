                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="overview-tab" data-bs-toggle="tab"
                                data-bs-target="#overview" type="button" role="tab" aria-controls="overview"
                                aria-selected="true">Data Madrasah </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="carriculam-tab" data-bs-toggle="tab"
                                data-bs-target="#carriculam" type="button" role="tab" aria-controls="carriculam"
                                aria-selected="false">Rekapitulasi Madrasah </button>
                        </li>
                       

                     <li class="nav-item" role="presentation">
                            <button class="nav-link" id="danabos-tab" data-bs-toggle="tab" data-bs-target="#danabos"
                                type="button" role="tab" aria-controls="danabos" aria-selected="false">Dana BOS</button>
                        </li>
                        


                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="review-tab" data-bs-toggle="tab" data-bs-target="#review"
                                type="button" role="tab" aria-controls="review" aria-selected="false">Data Siswa</button>
                        </li>
                      

                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="instructor-tab" data-bs-toggle="tab"
                                data-bs-target="#instructor" type="button" role="tab" aria-controls="instructor"
                                aria-selected="false">Lokasi</button>
                        </li>

                        
                    </ul>

                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="overview" role="tabpanel"
                            aria-labelledby="overview-tab">
                            <br>
                            <div class="table-responsive">
                                <table class="table ">
                                    <tr>
                                        <td> NSM </td>
                                        <td> <?php echo $data->nsm; ?> </td>
                                    </tr>
                                    <tr>
                                        <td> NPSN </td>
                                        <td> <?php echo $data->npsn; ?> </td>
                                    </tr>
                                    <tr>
                                        <td>  Madrasah </td>
                                        <td> <?php echo $data->nama; ?> </td>
                                    </tr>
                                    <tr>
                                        <td> Status </td>
                                        <td> <?php echo ucwords($data->status); ?> </td>
                                    </tr>
                                   
                                    <tr>
                                        <td> Akreditasi </td>
                                        <td> <?php echo $data->accreditation_status; ?> </td>
                                    </tr>
                                    <tr>
                                        <td> Alamat </td>
                                        <td> <?php echo $data->alamat; ?> </td>
                                    </tr>
                                    <tr>
                                        <td> Kabupaten/Kota </td>
                                        <td> <?php echo $this->Reff->get_kondisi(array("id"=>$data->kota_id),"kota","nama"); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> Provinsi </td>
                                        <td> <?php echo $this->Reff->get_kondisi(array("id"=>$data->provinsi_id),"provinsi","nama"); ?>
                                        </td>
                                    </tr>

                                </table>

                            </div>
                        </div>
                        <div class="tab-pane fade" id="carriculam" role="tabpanel" aria-labelledby="carriculam-tab">
                            <div class="course-tab-content">
                                <div class="course-curriculam">
                                    <br>

                                <ol class="list-group list-group-numbered">
                                    <li class="list-group-item d-flex justify-content-between align-items-start">
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold">Jumlah Siswa</div>
                                            
                                        </div>
                                        <span class="badge bg-primary rounded-pill"><?php echo $data->jumlah_siswa; ?></span>
                                    </li>

                                    <li class="list-group-item d-flex justify-content-between align-items-start">
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold">Jumlah Rombel</div>
                                            
                                        </div>
                                        <span class="badge bg-primary rounded-pill"><?php echo $data->jumlah_rombel; ?></span>
                                    </li>

                                    <li class="list-group-item d-flex justify-content-between align-items-start">
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold">Siswa Laki - laki</div>
                                            
                                        </div>
                                        <span class="badge bg-primary rounded-pill"><?php echo $data->student_male; ?></span>
                                    </li>

                                    <li class="list-group-item d-flex justify-content-between align-items-start">
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold">Siswa Perempuan</div>
                                            
                                        </div>
                                        <span class="badge bg-primary rounded-pill"><?php echo $data->student_female; ?></span>
                                    </li>

                                    <li class="list-group-item d-flex justify-content-between align-items-start">
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold">Siswa Berkebutuhan Khusus</div>
                                            
                                        </div>
                                        <span class="badge bg-primary rounded-pill"><?php echo $data->student_special_need; ?></span>
                                    </li>

                                    <li class="list-group-item d-flex justify-content-between align-items-start">
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold">Jumlah PTK </div>
                                            
                                        </div>
                                        <span class="badge bg-primary rounded-pill"><?php echo $data->personnel_count; ?></span>
                                    </li>

                                    <li class="list-group-item d-flex justify-content-between align-items-start">
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold">Jumlah Guru </div>
                                            
                                        </div>
                                        <span class="badge bg-primary rounded-pill"><?php echo $data->personnel_teacher; ?></span>
                                    </li>

                                    <li class="list-group-item d-flex justify-content-between align-items-start">
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold">Tenaga Kependidikan </div>
                                            
                                        </div>
                                        <span class="badge bg-primary rounded-pill"><?php echo $data->personnel_education_staff; ?></span>
                                    </li>

                                    <li class="list-group-item d-flex justify-content-between align-items-start">
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold">PTK Laki-laki</div>
                                            
                                        </div>
                                        <span class="badge bg-primary rounded-pill"><?php echo $data->teacher_male; ?></span>
                                    </li>

                                    <li class="list-group-item d-flex justify-content-between align-items-start">
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold">PTK Perempuan</div>
                                            
                                        </div>
                                        <span class="badge bg-primary rounded-pill"><?php echo $data->teacher_female; ?></span>
                                    </li>

                                    <li class="list-group-item d-flex justify-content-between align-items-start">
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold">PTK PNS</div>
                                            
                                        </div>
                                        <span class="badge bg-primary rounded-pill"><?php echo $data->teacher_pns; ?></span>
                                    </li>

                                    <li class="list-group-item d-flex justify-content-between align-items-start">
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold">PTK NON PNS</div>
                                            
                                        </div>
                                        <span class="badge bg-primary rounded-pill"><?php echo $data->teacher_non_pns; ?></span>
                                    </li>

                                   


                                    
                                   
                                </ol>
                                

                                   
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="instructor" role="tabpanel" aria-labelledby="instructor-tab">
                            <br>
                            <div class="course-tab-content">
                                <div class="course-instructor">
                                    <iframe width="100%" height="400px" id="gmap_canvas"
                                        src="https://maps.google.com/maps?q=<?php echo $data->latitude; ?>,<?php echo $data->longitude; ?>&t=&z=13&ie=UTF8&iwloc=&output=embed"
                                        frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                                    </iframe>
                                </div>
                            </div>
                        </div>
                      

                        <div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">
                            <br>
                            <br>

                                    <div class="table-responsive">
                                    <table class="table table-bordered table-striped " id='tableSiswa' width="100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>NIK</th>
                                               
                                                <th>Nama</th>
                                                <th>Gender</th>
                                                <th>Lahir</th>
                                                <th>Kelas</th>
                                                <th> Ayah </th>
                                                <th> Ibu </th>
                                            

                                            </tr>

                                        </thead>
                                        <tbody>
                                            <?php 
                                                $no=1;
                                              $dataSiswa = $this->db->query("SELECT * from tm_siswa where nsm='{$data->nsm}' LIMIT {$data->jumlah_siswa}")->result();
                                              foreach($dataSiswa as $row){

                                              ?>
                                            <tr>
                                                <td><?php echo $no++; ?></td>
                                                <td><?php echo $row->nik; ?></td>
                                               
                                                <td><?php echo $row->full_name; ?></td>
                                                <td><?php echo $row->gender; ?></td>
                                                <td><?php echo $row->birth_date; ?></td>
                                                <td><?php echo $row->level; ?></td>
                                                <td><?php echo $row->father_full_name; ?></td>
                                                <td><?php echo $row->mother_full_name; ?></td>
                                                

                                            </tr>
                                            <?php 
                                              }
                                              ?>

                                        </tbody>


                                    </table>
                                </div>


                           
                        </div>
                       
                    </div>



                    <script>

 $(document).ready(function () {
    $('#tableSiswa').DataTable();
});

</script>