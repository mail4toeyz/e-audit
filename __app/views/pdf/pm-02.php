<?php
$documentroot = $this->Reff->documentroot();
//$documentroot = base_url();
$kantor         = $this->Reff->get_where("tr_kantor",array("linkid"=>$madrasah->kota_id));
?>
<style>

#bodinya { margin-right: 40px;
  margin-left: 60px; 
  font-family: Bookman Old Style;
font-size: 16px;
  }
</style>
<table width="100%"  style="margin-top:0px">
			  
			    <tr>
				  <td width="105px"> <img src="<?php echo $documentroot.'/__statics/img/kemenag.jpg';?>" style="width:100px"></td>
				    <td align="center"   valign="top">
				       <span style="font-size:19px;font-weight:bold;font-family:Bookman Old Style">KEMENTERIAN AGAMA REPUBLIK INDONESIA</span><br/>
					   <span style="font-size:17px;font-weight:bold;font-family:Bookman Old Style"><?php echo strtoupper($kantor->nama); ?></span><br/>

					   <span style="font-size:12px;font-family:Bookman Old Style">  <?php echo ($kantor->alamat); ?></span>
					   <span style="font-size:12px;font-family:Bookman Old Style">  Telepon : (<?php echo ($kantor->prefix); ?>) <?php echo ($kantor->telepon); ?> &nbsp; Faksimili : (<?php echo ($kantor->prefix); ?>) <?php echo ($kantor->fax); ?> &nbsp; Email : <?php echo ($kantor->email); ?> </span>
					   <span style="font-size:12px;font-family:Bookman Old Style">   Website : <?php echo ($kantor->website); ?> </span>
					  </span>
				  </td>
				   <td width="20px">&nbsp;</td>
				</tr>
				
			 </table>
<hr style="height: 2px; border: none; color: #000; background-color: #000;" />
<div id="bodinya">
<?php 
  $surat = $this->db->get_where("tr_surat",array("tmyayasan_id"=>$yayasan->id,"jenis"=>"va"))->row();
  ?>
    <p align="center">SURAT TUGAS<br /> Nomor: <?php echo $surat->nomor;  ?>
    </p>
    <p align="justify">Menindaklanjuti Surat dari&nbsp;
      <?php echo $yayasan->nama; ?>&nbsp;Nomor
      <?php echo $this->Reff->surat_yayasan($yayasan->id); ?>&nbsp;Tanggal
      <?php echo $this->Reff->formattanggalstring($madrasah->tgl_terima); ?>.&nbsp;Perihal Permohonan Izin Pendirian <?php echo $this->Reff->jenjangsingkat($madrasah->jenjang).' '.$madrasah->nama; ?>, dengan ini kami menugaskan :
    </p>
    <table border="1" style="border-collapse: collapse; width: 152mm; line-height: 1;" width="100%">
        <tr>
          <td align="center" style="width: 10mm;">No.</td>
          <td align="center" style="width: 52mm;">Nama / NIP</td>
          <td align="center" style="width: 70mm;">Jabatan Dinas</td>
          <td align="center" style="width: 20mm;">Jabatan Tim</td>
        </tr>
        <tr><td align="center">1.</td>
        <td style="padding: 4px;"><?php echo $surat->kasi_nama; ?><br>
            <?php echo $surat->kasi_nip; ?></td>
        <td style="padding: 4px;"><?php echo $kantor->kasi_jabatan; ?></td>
        <td style="padding: 4px;">Ketua</td>
        </tr>
        <tr><td align="center">2.</td>
        <td style="padding: 4px;"><?php echo $this->Reff->get_kondisi(array("id"=>$surat->pengawas_id),"tm_pegawai","nama") ?><br>
            <?php echo $this->Reff->get_kondisi(array("id"=>$surat->pengawas_id),"tm_pegawai","nip") ?></td>
        <td style="padding: 4px;">Pengawas Madrasah</td>
        <td style="padding: 4px;">Sekretaris</td>
        </tr>
        <tr><td align="center">3.</td>
        <td style="padding: 4px;"><?php echo $this->Reff->get_kondisi(array("id"=>$surat->staff_id),"tm_pegawai","nama") ?><br>
            <?php echo $this->Reff->get_kondisi(array("id"=>$surat->staff_id),"tm_pegawai","nip") ?></td>
        <td style="padding: 4px;">Pelaksana Seksi Pendidikan Madrasah</td>
        <td style="padding: 4px;">Anggota</td>
        </tr>
    </table>
    <br>
    <table style="width: 155mm;">
        <tr valign="top">
          <td style="width: 11mm;">Untuk</td>
          <td style="width: 4mm;">:</td>
          <td style="text-align: justify; width: 140mm;">Melaksanakan verifikasi dokumen persyaratan administratif, teknis, dan kelayakan pendirian
             <?php echo $this->Reff->jenjangsingkat($madrasah->jenjang).' '.$madrasah->nama; ?>  &nbsp;yang berlokasi di&nbsp;
            <?php echo ($madrasah->alamat); ?>&nbsp;Desa/Kelurahan&nbsp;
            <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->desa_id),"desa","nama")));  ?> Kecamatan
            <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->kecamatan_id),"kecamatan","nama")));  ?>&nbsp;
            <?php echo ucwords(strtolower((preg_match('/^KOTA /',$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))?'':'KABUPATEN ').$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))); ?>&nbsp;<?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->provinsi_id),"provinsi","nama"))); ?>   </td>
       </td>
        </tr>
        <tr valign="top">
        <td>Waktu </td>
        <td>:</td>
        <td>Pelaksanaan <?php echo $this->Reff->formattanggalstring($surat->mulai); ?> s/d <?php echo $this->Reff->formattanggalstring($surat->sampai); ?></td>
        </tr>
    </table>
    <p style="text-align: justify;">Demikian Surat Tugas ini agar dilaksanakan dengan penuh tanggung jawab. Pegawai yang diberi tugas agar melaporkan hasil pelaksanaan tugas kepada Kepala
      <?php echo $kantor->nama; ?>
    </p>
    <br />
    <table style="width:100%" class="mceItemTable">
      <tbody>
        <tr>
          <td style="width:50%;">&nbsp;</td>
          <td><?php echo $kantor->nama_kota; ?>,&nbsp;<?php echo $this->Reff->formattanggalstring($surat->tanggal); ?></td>
        </tr>
        <tr>
          <td style="width:50%;">&nbsp;</td>
          <td>Kepala,</td>
        </tr>
        <tr>
          <td style="width:50%;">&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
          <td style="width:50%;">&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
          <td style="width:50%;">&nbsp;</td>
          <td><?php echo $kantor->kepala_nama; ?></td>
        </tr>
        <tr>
          <td style="width:50%;">&nbsp;</td>
          <td>NIP.&nbsp;<?php echo $kantor->kepala_nip; ?></td>
        </tr>
      </tbody>
    </table>
</div>
