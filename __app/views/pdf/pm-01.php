<?php
$documentroot = $this->Reff->documentroot();
//$documentroot = base_url();
$kantor         = $this->Reff->get_where("tr_kantor",array("linkid"=>$madrasah->kota_id));
?>
<style>

#bodinya { margin-right: 40px;
  margin-left: 60px; 
  font-family: Bookman Old Style;
font-size: 16px;
  }
</style>
<table width="100%"  style="margin-top:0px">
			  
			    <tr>
				  <td width="105px"> <img src="<?php echo $documentroot.'/__statics/img/kemenag.jpg';?>" style="width:100px"></td>
				    <td align="center"   valign="top">
				       <span style="font-size:19px;font-weight:bold;font-family:Bookman Old Style">KEMENTERIAN AGAMA REPUBLIK INDONESIA</span><br/>
					   <span style="font-size:17px;font-weight:bold;font-family:Bookman Old Style"><?php echo strtoupper($kantor->nama); ?></span><br/>

					   <span style="font-size:12px;font-family:Bookman Old Style">  <?php echo ($kantor->alamat); ?></span>
					   <span style="font-size:12px;font-family:Bookman Old Style">  Telepon : (<?php echo ($kantor->prefix); ?>) <?php echo ($kantor->telepon); ?> &nbsp; Faksimili : (<?php echo ($kantor->prefix); ?>) <?php echo ($kantor->fax); ?> &nbsp; Email : <?php echo ($kantor->email); ?> </span>
					   <span style="font-size:12px;font-family:Bookman Old Style">   Website : <?php echo ($kantor->website); ?> </span>
					  </span>
				  </td>
				   <td width="20px">&nbsp;</td>
				</tr>
				
			 </table>
			 
<hr style="height: 2px;">
<div id="bodinya">
<p align="center">TANDA TERIMA DOKUMEN<br> Nomor : <?php echo $this->Reff->get_kondisi(array("tmyayasan_id"=>$yayasan->id,"jenis"=>"va"),"tr_surat","nomor");  ?></p>
<b>Telah Terima Dokumen Proposal Pendirian Madrasah : </b>
<table>
<tr>
<td>Nama Madrasah</td>
<td>:</td>
<td><?php echo $this->Reff->jenjangsingkat($madrasah->jenjang).' '.$madrasah->nama; ?></td>
</tr>
<tr>
<td>Dari </td>
<td>:</td>
<td><?php echo $yayasan->nama; ?></td>
</tr>
<tr>
<td>Tanggal Penerimaan</td>
<td>:</td>
<td><?php echo $this->Reff->formattanggalstring($madrasah->tgl_terima); ?></td>
</tr>
</table><br>
<b>Daftar Dokumen yang Diterima :</b>
<table border="1" width="100%" style="border-collapse: collapse; line-height: 1;">
<tr style="height: 32px;">
<th style="padding: 6px;" align="center">No.</th>
<th style="padding: 6px;" align="center">Jenis Dokumen</th>
<th style="padding: 6px;" align="center">Jumlah</th></tr>
<tr style="height: 32px;">
<td align="center" style="padding: 4px;">1.</td>
<td style="padding: 4px;">Surat Pengantar Permohonan Izin</td>
<td style="padding: 4px;" align="center">1 </td>
</tr>
<tr style="height: 32px; ">
<td align="center" style="padding: 4px;">2.</td>
<td style="padding: 4px;">Formulir Permohonan Izin Pendirian Madrasah</td>
<td style="padding: 4px;" align="center">1 </td>
</tr>
<tr style="height: 32px; ">
<td align="center" style="padding: 4px;">3.</td>
<td style="padding: 4px;">Kelengkapan persyaratan dan dokumen pendukung</td>
<td style="padding: 4px;" align="center">1</td>
</tr>
</table>
<br>
<b>Identitas Calon Madrasah :</b>
<table style="width: 152mm;" >
<tr>
<td style="width: 40%">Nama Madrasah</td>
<td style="width: 2%">:</td>
<td style="width: 58%"><?php echo $this->Reff->jenjangsingkat($madrasah->jenjang).' '.$madrasah->nama; ?></td>
</tr>
<tr>
<td>Alamat</td>
<td>:</td>
<td><?php echo ($madrasah->alamat); ?></td>
</tr>
<tr>
<td>Desa/Kelurahan</td>
<td>:</td>
<td><?php echo $this->Reff->get_kondisi(array("id"=>$madrasah->desa_id),"desa","nama");  ?></td>
</tr>
<tr>
<td>Kecamatan</td>
<td>:</td>
<td><?php echo $this->Reff->get_kondisi(array("id"=>$madrasah->kecamatan_id),"kecamatan","nama");  ?></td>
</tr>
<tr>
<td>Kabupaten/Kota</td>
<td>:</td>
<td><?php echo (preg_match('/^KOTA /',$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))?'':'KABUPATEN ').$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"); ?></td>
</tr>
<tr>
<td>Provinsi</td>
<td>:</td>
<td><?php echo $this->Reff->get_kondisi(array("id"=>$madrasah->provinsi_id),"provinsi","nama"); ?></td>
</tr>
<tr>
<td>Penyelenggara Madrasah</td>
<td>:</td>
<td><?php echo $yayasan->nama; ?></td>
</tr>
<tr>
<td>Akte Notaris Penyelenggara</td>
<td>:</td>
<td>No. <?php echo $yayasan->notaris_no.' '.$yayasan->notaris_nama.' Tanggal '.$this->Reff->formattanggalstring($yayasan->notaris_tgl); ?></td>
</tr>
<tr>
<td>Pengesahan Akte Notaris</td>
<td>:</td>
<td>
<?php
$skumhamno = trim($yayasan->menkumham_sk);
$skumhamtgl = $yayasan->menkumham_tgl;
if(!empty($skumhamno) && !empty($skumhamtgl)) {
echo $skumhamno.' Tanggal '.$this->Reff->formattanggalstring(($skumhamtgl));
}
else
{
echo 'BELUM';
}
?></td>
</tr>
</table>
<br>
Pengirim menyatakan bahwa data identitas calon madrasah tersebut telah sesuai<br><br>

<hr style="height: 1px;">

<table style="width: 152mm;"  class="mceItemTable">
<tr>
<td style="width: 15mm;">&nbsp; </td>
<td style="width: 5mm;">&nbsp;</td>
<td style="width: 85mm;">&nbsp;</td>
<td rowspan="4" style="width: 47mm;"> Kode QR : <img src="<?php echo $documentroot; ?>/__statics/upload/qrcode/<?php echo $yayasan->id; ?>.png" style="width: 30mm;"> </td>
</tr>

<tr>
<td colspan="3">&nbsp;</td>
</tr>
<tr>
<td colspan="3">&nbsp;</td>
</tr>
</table>
</body>