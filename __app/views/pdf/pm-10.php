<?php
$documentroot = $this->Reff->documentroot();
//$documentroot = base_url();
$kantor         = $this->Reff->get_where("tr_kantor",array("linkid"=>$madrasah->provinsi_id));
?>

<?php 
  $suratrk = $this->db->get_where("tr_surat",array("tmyayasan_id"=>$yayasan->id,"jenis"=>"rk"))->row();
  $suratnd = $this->db->get_where("tr_surat",array("tmyayasan_id"=>$yayasan->id,"jenis"=>"nd"))->row();
  $piagam = $this->db->get_where("tr_piagam",array("tmyayasan_id"=>$yayasan->id))->row();
 $level_print ="Madrasah";  

  ?>
   <style>
   .page_break { page-break-before: always; }
   
   </style>
     <style>

#bodinya { margin-right: 40px;
  margin-left: 60px; 
  font-family: Bookman Old Style;
font-size: 16px;
  }
</style>
   

  
  <div id="bodinya">
  
    <table style="width: 100%;">
      <tr><td align="center"><img src="<?php echo $documentroot.'/__statics/img/logo.png';?>" width="100" height="100" /><br /></td></tr>
      <tr><td align="center"><b>KEPUTUSAN KEPALA KANTOR WILAYAH KEMENTERIAN AGAMA<br />
       PROVINSI <?php echo ($this->Reff->get_kondisi(array("id"=>$madrasah->provinsi_id),"provinsi","nama")); ?><br />
    NOMOR <?php echo  $piagam->nomor_sk; ?><br /><br style="line-height:8px;" />TENTANG<br style="line-height:2px;" /><br />
    PEMBERIAN IZIN OPERASIONAL PENDIRIAN<br /><?php echo strtoupper($this->Reff->namajenjang($madrasah->jenjang)); ?>
    <?php echo strtoupper($madrasah->nama); ?> <?php echo ((preg_match('/^KOTA /',$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))?'':'KABUPATEN ').$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama")); ?></td></tr>
    
	</b></table>
    <p align="center" style="font-weight:bold">DENGAN RAHMAT TUHAN YANG MAHA ESA</p>
    <p align="center" style="font-weight:bold">KEPALA KANTOR WILAYAH KEMENTERIAN AGAMA<br />
    PROVINSI <?php echo ($this->Reff->get_kondisi(array("id"=>$madrasah->provinsi_id),"provinsi","nama")); ?>,</p>
	
	
    <table style="width:100%; margin-botton: 12px;" class="mceItemTable">
     
        <tr valign="top">
          <td style="width:16%;">Menimbang</td>
          <td style="width:2%;">:</td>
          <td style="width:82%;">
           <ol style="list-style-type: lower-alpha; margin-top: 0;text-align:justify;">
              <li>bahwa dalam rangka meningkatkan akses pendidikan madrasah yang bermutu, perlu memberikan kesempatan masyarakat melalui organisasi berbadan hukum untuk menyelenggarakan madrasah sesuai dengan standar nasional pendidikan;<br style="line-height:6px;" /></li>
              <li>bahwa <?php echo $level_print; ?> sebagaimana tercantum dalam keputusan ini telah memenuhi persyaratan administratif, teknis dan kelayakan sebagaimana mestinya sesuai dengan hasil visitasi dan verifikasi Tim Penilai, Surat Rekomendasi Kepala Kantor Kementerian Agama <?php echo ucwords(strtolower(preg_match('/^KOTA /',$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))?'':'KABUPATEN ').$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama")); ?>
    			  Nomor <?php echo $suratrk->nomor; ?> tanggal
                <?php echo $this->Reff->formattanggalstring($suratrk->tanggal); ?> dan Nota Dinas Pertimbangan Kepala Bidang Pendidikan Madrasah Nomor 
				<?php echo $suratnd->nomor; ?> tanggal
                <?php echo $this->Reff->formattanggalstring($suratnd->tanggal); ?>
				sehingga layak diberikan izin operasional;<br style="line-height:6px;" /></li>
              <li>bahwa berdasarkan pertimbangan sebagaimana dimaksud dalam huruf a dan huruf b perlu menetapkan Keputusan Kepala Kantor Wilayah Kementerian Agama Provinsi <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->provinsi_id),"provinsi","nama"))); ?> tentang Pemberian Izin Operasional <?php echo $this->Reff->jenjangsingkat($madrasah->jenjang).' '.$madrasah->nama; ?>;<br></li>
            </ol></td>
        </tr>
		  <tr valign="top">
          <td style="width:16%;">Mengingat</td>
          <td style="width:2%;">:</td>
          <td style="width:82%;">
            <ol style="margin-top: 0;text-align:justify;" start="1">
              <li><span style="text-align:justify;">Undang-Undang&nbsp; Nomor&nbsp; 20 Tahun 2003&nbsp; tentang&nbsp; Sistem&nbsp; Pendidikan Nasional (Lembaran Negara Republik Indonesia Tahun 2003 Nomor 78, Tambahan Lembaran Negara Republik Indonesia Nomor 4301);</span><br style="line-height:6px;" /></li>
              <li><span style="text-align:justify;">Undang-Undang Nomor 14 Tahun 2005 tentang Guru dan Dosen (Lembaran Negara Republik Indonesia Tahun 2005 Nomor 157, Tambahan Lembaran Negara Republik Indonesia Nomor 4586);</span><br style="line-height:6px;" /></li>
               </ol>
		  
		  
		  </td>
        </tr>
      
        <tr valign="top">
          <td style="width:16%;">&nbsp;</td>
          <td style="width:2%;">&nbsp;</td>
          <td style="width:82%;">
            <ol style="margin-top: 0;text-align:justify;" start="3">
			  <li><span style="text-align:justify;">Peraturan Pemerintah Nomor 19 Tahun 2005 tentang Standar Nasional Pendidikan (Lembaran Negara Republik Indonesia Tahun 2005 Nomor 41, Tambahan Lembaran Negara Republik Indonesia Nomor 4496) sebagaimana telah diubah dengan Peraturan Pemerintah Nomor 32 Tahun 2013 tentang Perubahan atas Peraturan Pemerintah Nomor 19 Tahun 2005 tentang Standar Nasional Pendidikan (Lembaran Negara Republik Indonesia Tahun 2013 Nomor 71, Tambahan Lembaran Negara Republik Indonesia Nomor 5410);</span><br style="line-height:6px;" /></li>
              <li><span style="text-align:justify;">Peraturan Pemerintah Nomor 47 Tahun 2008 tentang Wajib Belajar Pendidikan Dasar (Lembaran Negara Republik Indonesia Tahun 2008 Nomor 90, Tambahan Lembaran Negara Republik Indonesia Nomor 4863);</span><br style="line-height:6px;" /></li>
              <li><span style="text-align:justify;">Peraturan Pemerintah Nomor 48 Tahun 2008 tentang Pendanaan Pendidikan (Lembaran Negara Republik Indonesia Tahun 2008 Nomor 91, Tambahan Lembaran Negara Republik Indonesia Nomor 4864);</span><br style="line-height:6px;" /></li>
              <li><span style="text-align:justify;">Peraturan Pemerintah Nomor 74 Tahun 2008 tentang Guru (Lembaran Negara Republik Indonesia Tahun 2008 Nomor 194, Tambahan Lembaran Negara Republik Indonesia Nomor 4941);</span><br style="line-height:6px;" /></li>
              <li><span style="text-align:justify;">Peraturan Pemerintah Nomor 17 Tahun 2010 tentang Pengelolaan dan Penyelenggaraan Pendidikan (Lembaran Negara Republik Indonesia Tahun 2010 Nomor 23, Tambahan Lembaran Negara Republik Indonesia Nomor 5150) sebagaimana telah diubah dengan Peraturan Pemerintah Nomor 66 Tahun 2010 tentang Perubahan Atas Peraturan Pemerintah Nomor 17 Tahun 2010 tentang Pengelolaan dan Penyelenggaraan Pendidikan (Lembaran Negara Republik Indonesia Tahun 2010 Nomor 112, Tambahan Lembaran Negara Republik Indonesia Nomor 5157);</span><br style="line-height:6px;" /></li>
             <li><span style="text-align:justify;">Peraturan Menteri Pendidikan Nasional Nomor 24 Tahun 2007 &nbsp;Standar Sarana dan Prasarana Untuk Sekolah Dasar/Madrasah Ibtidaiyah, Sekolah Menengah Pertama/Madrasah Tsanawiyah, dan Sekolah Menengah Atas/Madrasah Aliyah;</span><br style="line-height:6px;" /></li>
              <li><span style="text-align:justify;">Peraturan Menteri Pendidikan Nasional Nomor 15 Tahun 2010 tentang Standar Pelayanan Minimal Pendidikan di Kabupaten/Kota sebagaimana telah diubah menjadi Peraturan Menteri Pendidikan dan Kebudayaan Nomor 23 Tahun 2013 tentang Perubahan atas Peraturan Menteri Pendidikan Nasional Nomor 15 Tahun 2010 tentang Standar Pelayanan Minimal Pendidikan di Kabupaten/Kota;</span><br style="line-height:6px;" /></li>
              <li><span style="text-align:justify;">Peraturan Menteri Agama Nomor 2 Tahun 2012 tentang Pengawas Madrasah dan Pengawas Pendidikan Agama Islam pada Sekolah (Berita Negara Republik Indonesia Tahun 2012 Nomor 206) sebagaimana telah diubah dengan Peraturan Menteri Agama Nomor 31 Tahun 2013 tentang Perubahan Atas Peraturan Menteri Agama Nomor 2 Tahun 2012 tentang Pengawas Madrasah dan Pengawas Pendidikan Agama Islam pada Sekolah (Berita Negara Republik Indonesia Tahun 2013 Nomor 684);</span><br style="line-height:6px;" /></li>
           
               </ol>
			
			</td>
        </tr>

    </table>

   <table style="width:100%; margin-botton: 12px;" class="mceItemTable">
      
        <tr valign="top">
          <td style="width:16%;">&nbsp;</td>
          <td style="width:2%;">&nbsp;</td>
          <td style="width:82%;">
          <ol style="margin-top: 0;text-align:justify;" start="11">
                <li><span style="text-align:justify;">Peraturan Menteri Agama Nomor 13 Tahun 2012 tentang Organisasi dan Tata Kerja Instansi Vertikal Kementerian Agama (Berita Negara Republik Indonesia Tahun 2012 Nomor 851);</span><br></li>
              <li><span style="text-align:justify;">Peraturan Menteri Agama Nomor 90 Tahun 2013 tentang Penyelenggaran Pendidikan Madrasah (Berita Negara Republik Indonesia Tahun 2013 Nomor 1382) sebagaimana telah diubah dengan Peraturan Menteri Agama Nomor 60 Tahun 2015 tentang Perubahan Atas Peraturan Menteri Agama Nomor 90 Tahun 2013 tentang Penyelenggaraan Pendidikan Madrasah (Berita Negara Republik Indonesia Tahun 2015 Nomor 1733);</span><br></li>
            </ol></td>
        </tr>
  
    </table>


    <p align="center"><b>MEMUTUSKAN :</b></p>
    <table style="100%; " class="mceItemTable">
      
      <tr valign="top">
          <td style="width:20%;">Menetapkan</td>
          <td style="width:2%;">:</td>
          <td style="width:78%;">
            <ol style="list-style-type: none; margin-top: 0; text-align: justify;">
              <li><b>KEPUTUSAN KEPALA KANTOR WILAYAH KEMENTERIAN AGAMA PROVINSI <?php echo ($this->Reff->get_kondisi(array("id"=>$madrasah->provinsi_id),"provinsi","nama")); ?> TENTANG PEMBERIAN IZIN OPERASIONAL PENDIRIAN <?php echo $this->Reff->jenjangsingkat($madrasah->jenjang).' '.$madrasah->nama; ?>.</b><br style="line-height:6px;" /></li>
            </ol></td>
        </tr>
        <tr valign="top">
          <td style="width:20%;">KESATU</td>
          <td style="width:2%;">:</td>
          <td style="width:78%;text-align: justify;" valign="top" >
           
             Memberikan izin operasional pendirian madrasah kepada <?php echo $level_print; ?> sebagaimana tercantum dalam Lampiran yang merupakan bagian tidak terpisahkan dari Keputusan ini.
        
			
			</td>
        </tr>
        <tr valign="top"><td style="width:20%;">KEDUA</td>
		<td style="width:2%;">:</td>
          <td style="width:78%;">
		  Setelah jangka waktu <?php echo (($madrasah->jenjang==2)?'7':'4'); ?>&nbsp;tahun, Kepala <?php echo $level_print; ?> yang bersangkutan wajib: </span><br style="line-height:6px;" />
           <ol  style="list-style-type: lower-alpha;margin-top: 0; text-align: justify;">
              
             
                <li><span style="text-align:justify;">Menyampaikan laporan perkembangan <?php echo $level_print; ?> kepada Kepala Kantor Kementerian Agama yang memuat paling sedikit perkembangan jumlah peserta didik, pelaksanaan kurikulum, pelaksanaan pemenuhan standar sarana prasarana, dan pelaksanaan pemenuhan standar pendidik dan tenaga kependidikan; dan/atau</span><br style="line-height:6px;" /></li>
                <li><span style="text-align:justify;">Mengajukan pendaftaran visitasi akreditasi <?php echo ($madrasah->jenjang == 1)? 'Paud/RA':'sekolah/madrasah'; ?> kepada <?php echo ($madrasah->jenjang == 1)?'BAP Paud dan PNF':'BAP-S/M'; ?> sesuai ketentuan peraturan perundang-undangan.</span><br></li>
              
            </ol></td>
        </tr>
  
    </table>
    <table style="width:100%; " class="mceItemTable">
      
        <tr valign="top"><td style="width:20%;">KETIGA</td><td style="width:2%;">:</td>
          <td style="width:78%;">
            <ol style="list-style-type: none; margin-top: 0; padding-left:0;">
              <li><span style="text-align:justify;">Dalam hal perkembangan <?php echo $level_print; ?> sebagaimana dimaksud dalam Diktum KEDUA huruf a dinilai memenuhi standar pelayanan minimal penyelenggaraan pendidikan dan/atau hasil akreditasi sebagaimana dimaksud Diktum KEDUA huruf b mendapat peringkat minimal C, maka izin operasional sebagaimana dimaksud dalam Diktum KESATU tetap berlaku.</span><br></li>
            </ol></td>
        </tr>
  
    </table>
	</div>
<div style="page-break-before: always;"></div>
<div id="bodinya">
    <table style="width:100%; " class="mceItemTable">
      
        <tr valign="top"><td style="width:20%;">KEEMPAT</td><td style="width:2%;">:</td>
          <td style="width:78%;">
            <ol style="list-style-type: none; margin-top: 0; padding-left:0;">
              <li><span style="text-align:justify;">Dalam hal perkembangan <?php echo $level_print; ?> sebagaimana dimaksud dalam Diktum KEDUA huruf a dinilai memenuhi standar pelayanan minimal penyelenggaraan pendidikan dan/atau hasil akreditasi sebagaimana dimaksud Diktum KEDUA huruf b tidak mendapat peringkat minimal C, maka izin operasional sebagaimana dimaksud dalam Diktum KESATU dicabut.</span><br></li>
            </ol></td>
        </tr>
  
    </table>
	
	
    <table style="width:182mm; " class="mceItemTable">
      
        <tr valign="top"><td style="width:20%;">KELIMA</td><td style="width:2%;">:</td>
          <td style="width:78%;">
            <ol style="list-style-type: none; margin-top: 0; padding-left:0;">
              <li><span style="text-align:justify;">Keputusan ini mulai berlaku pada tanggal ditetapkan.</span></li>
            </ol></td>
        </tr>
  
    </table>
    <table style="width:180mm;" class="mceItemTable">
      
        <tr valign="top">
          <td style="width:47%;">&nbsp;</td><td>
            <p>Ditetapkan di&nbsp;<?php echo $kantor->nama_kota; ?><br />
              Pada tanggal&nbsp;<?php echo $this->Reff->formattanggalstring($piagam->tanggal); ?><br />
            </p>
          </td>
        </tr>
        <tr valign="top">
          <td align="right" style="width:47%;"></td>
          <td align="justify">KEPALA KANTOR WILAYAH<br />
          KEMENTERIAN AGAMA<br />
          <span>PROVINSI <?php echo ($this->Reff->get_kondisi(array("id"=>$madrasah->provinsi_id),"provinsi","nama")); ?></span>
          </td>
        </tr>
        <tr>
          <td style="width:47%;">&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
          <td style="width:47%;">&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
          <td style="width:47%;">&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
          <td style="width:47%;">&nbsp;</td>
          <td><?php echo strtoupper($kantor->kepala_nama); ?></td>
        </tr>
  
    </table>
    </div>
<div style="page-break-before: always;"></div>
<div id="bodinya">

    </p>
    <p>LAMPIRAN<br /> KEPUTUSAN KEPALA KANTOR WILAYAH KEMENTERIAN AGAMA<br />PROVINSI <?php echo ($this->Reff->get_kondisi(array("id"=>$madrasah->provinsi_id),"provinsi","nama")); ?><br /> NOMOR <?php echo   $piagam->nomor_sk; ?><br /> TENTANG<br /> PEMBERIAN IZIN PENDIRIAN <?php echo strtoupper($this->Reff->namajenjang($madrasah->jenjang)).' '.strtoupper($madrasah->nama); ?>
    </p>
    <p>
    </p>
    <p align="center">IDENTITAS <?php echo strtoupper($level_print); ?> YANG DIBERIKAN IZIN OPERASIONAL
    </p>
    <p>
    </p>
    <table border="1" style="border-collapse: collapse; width: 100%;">
      
        <tr style="line-height: 8mm;" valign="top">
          <td style="width:5%;" align="center">1</td>
          <td style="width:41%;">Nama Madrasah</td>
          <td style="width:54%;"><?php echo strtoupper($this->Reff->namajenjang($madrasah->jenjang)).' '.strtoupper($madrasah->nama); ?></td>
        </tr>
        <tr style="line-height: 8mm;" valign="top">
          <td align="center">2</td>
          <td>Nomor Statistik Madrasah</td>
          <td><?php echo $madrasah->nsm; ?></td>
        </tr>
        <tr valign="top">
          <td align="center">3</td>
          <td>Alamat Madrasah</td>
          <td><?php echo ($madrasah->alamat); ?> <br> Desa/Kelurahan <br>
            <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->desa_id),"desa","nama")));  ?> <br> Kecamatan
            <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->kecamatan_id),"kecamatan","nama")));  ?><br>
            <?php echo ucwords(strtolower((preg_match('/^KOTA /',$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))?'':'KABUPATEN ').$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))); ?><br> Provinsi <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->provinsi_id),"provinsi","nama"))); ?>   </td>
      
        </tr>
        <tr style="line-height: 8mm;" valign="top">
          <td align="center">4</td>
          <td>Nama Organisasi Penyelenggara</td>
          <td><?php echo $yayasan->nama; ?></td>
        </tr>
        <tr style="line-height: 8mm;" valign="top">
          <td align="center">5</td>
          <td>Akte Notaris Organisasi Penyelenggara</td>
          <td>No. <?php echo $yayasan->notaris_no.' '.$yayasan->notaris_nama.' Tanggal '.$this->Reff->formattanggalstring($yayasan->notaris_tgl); ?></td>
        </tr>
        <tr style="line-height: 8mm;" valign="top">
          <td align="center">6</td>
          <td>Pengesahan Akte Notaris Organisasi Penyelenggara</td>
          <td><?php 
$skumhamno = trim($yayasan->menkumham_sk);
$skumhamtgl = $yayasan->menkumham_tgl;

echo $skumhamno.' Tanggal '.$this->Reff->formattanggalstring($skumhamtgl); ?></td>
        </tr>
  
    </table>
	
	
  <br>
  <br>
  <br>
  <br>
     <table border="0" style="border-collapse: collapse; width: 100%;">
        <tr valign="top">
          <td align="right" style="width:47%;"></td>
          <td align="justify">KEPALA KANTOR WILAYAH<br />
          KEMENTERIAN AGAMA<br />
          <span>PROVINSI <?php echo ($this->Reff->get_kondisi(array("id"=>$madrasah->provinsi_id),"provinsi","nama")); ?></span>
          </td>
        </tr>
        <tr>
          <td style="width:47%;">&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
          <td style="width:47%;">&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
          <td style="width:47%;">&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
          <td style="width:47%;">&nbsp;</td>
          <td><?php echo strtoupper($kantor->kepala_nama); ?></td>
        </tr>
      
    </table>

  </div>

