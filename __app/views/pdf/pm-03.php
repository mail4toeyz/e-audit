<?php
$documentroot = $this->Reff->documentroot();
//$documentroot = base_url();
$kantor         = $this->Reff->get_where("tr_kantor",array("linkid"=>$madrasah->kota_id));
?>
<style>

#bodinya { margin-right: 40px;
  margin-left: 60px; 
  font-family: Bookman Old Style;
font-size: 16px;
  }
</style>
<style type="text/css">
table.tabel{
	width: 140mm;
	font-size : 10pt;
	border-spacing: 0px;
	border-collapse: collapse;
	border: 1px solid black;
	border-radius: 5px;
}

table.tabel td{
padding-top: 2mm;
text-align: center;
border: 1px solid black;
table-layout: fixed;
}

table.tabel th{
text-align: center;
padding: 3px 3px;
border: 1px solid black;
table-layout: fixed;
}

.no{
width: 8mm;
vertical-align: top;
}

.persyaratan{
width: 82mm;
table-layout: fixed;
text-align: justify;
vertical-align: top;
padding: 2mm 1mm 2mm 1mm;
}

.checkmark{
width: 10mm;
table-layout: fixed;
vertical-align: top;
}

.keterangan{
width: 30mm;
table-layout: fixed;
text-align: justify;
font-size : 9pt;
vertical-align: top;
}

</style>

<table width="100%"  style="margin-top:0px">
			  
			    <tr>
				  <td width="105px"> <img src="<?php echo $documentroot.'/__statics/img/kemenag.jpg';?>" style="width:100px"></td>
				    <td align="center"   valign="top">
				       <span style="font-size:19px;font-weight:bold;font-family:Bookman Old Style">KEMENTERIAN AGAMA REPUBLIK INDONESIA</span><br/>
					   <span style="font-size:17px;font-weight:bold;font-family:Bookman Old Style"><?php echo strtoupper($kantor->nama); ?></span><br/>

					   <span style="font-size:12px;font-family:Bookman Old Style">  <?php echo ($kantor->alamat); ?></span>
					   <span style="font-size:12px;font-family:Bookman Old Style">  Telepon : (<?php echo ($kantor->prefix); ?>) <?php echo ($kantor->telepon); ?> &nbsp; Faksimili : (<?php echo ($kantor->prefix); ?>) <?php echo ($kantor->fax); ?> &nbsp; Email : <?php echo ($kantor->email); ?> </span>
					   <span style="font-size:12px;font-family:Bookman Old Style">   Website : <?php echo ($kantor->website); ?> </span>
					  </span>
				  </td>
				   <td width="20px">&nbsp;</td>
				</tr>
				
			 </table>
<?php 
  $surat = $this->db->get_where("tr_surat",array("tmyayasan_id"=>$yayasan->id,"jenis"=>"va"))->row();
  ?>	 
			 
<hr style="height: 2px;" />

<div id="bodinya">
<p align="center">BERITA ACARA VERIFIKASI DOKUMEN PERSYARATAN ADMINISTRATIF,<br /> TEKNIS, DAN KELAYAKAN PERMOHONAN IZIN<br /> PENDIRIAN MADRASAH<br /> NOMOR: <?php echo $surat->nomor; ?></p>
<p align="justify">Pada hari ini, <?php echo strtolower($this->Reff->tgl_sebutan($surat->mulai)); ?>, kami yang bertanda tangan di bawah ini, Tim Verifikasi Dokumen Persyaratan Administratif, Teknis, dan Kelayakan Pendirian Madrasah telah mengadakan verifikasi dokumen administratif, teknis, dan kelayakan Permohonan Izin Pendirian Madrasah &nbsp;<?php echo $this->Reff->jenjangsingkat($madrasah->jenjang).' '.$madrasah->nama; ?>&nbsp; berdasarkan Surat Permohonan Izin Pendirian Madrasah Nomor: &nbsp;<?php echo $this->Reff->surat_yayasan($yayasan->id); ?>&nbsp; Tanggal <?php echo $this->Reff->formattanggalstring($madrasah->tgl_terima); ?>&nbsp; yang diajukan oleh:</p>
<table style="width: 152mm; line-height: 1.5;">
<tr valign="top">
<td style="width: 45mm;">Nama Penyelenggara</td>
<td style="width: 5mm;">:</td>
<td style="width: 102mm;"><?php echo $yayasan->nama; ?></td>
</tr>
<tr valign="top">
<td style="width: 45mm;">Alamat Penyelenggara</td>
<td style="width: 5mm;">:</td>
<td style="width: 102mm;"><?php echo $yayasan->alamat; ?>,&nbsp;
<?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$yayasan->desa_id),"desa","nama")));  ?> Kecamatan
<?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$yayasan->kecamatan_id),"kecamatan","nama")));  ?>&nbsp;
<?php echo ucwords(strtolower(preg_match('/^KOTA /',$this->Reff->get_kondisi(array("id"=>$yayasan->kota_id),"kota","nama"))?'':'KABUPATEN ').$this->Reff->get_kondisi(array("id"=>$yayasan->kota_id),"kota","nama")); ?> <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$yayasan->provinsi_id),"provinsi","nama"))); ?>
</td>
</tr>
<tr valign="top">
<td style="width: 45mm;">Akte Notaris</td>
<td style="width: 5mm;">:</td>
<td style="width: 102mm;">No. <?php echo $yayasan->notaris_no.' '.$yayasan->notaris_nama.' Tanggal '.$this->Reff->formattanggalstring($yayasan->notaris_tgl); ?></td>
</tr>
<tr valign="top">
<td style="width: 45mm;">Pengesahan Akte Notaris</td>
<td style="width: 5mm;">:</td>
<td style="width: 102mm;"><?php 
$skumhamno = trim($yayasan->menkumham_sk);
$skumhamtgl = $yayasan->menkumham_tgl;

echo $skumhamno.' Tanggal '.$this->Reff->formattanggalstring($skumhamtgl); ?></td>
</tr>
</table>
<p align="justify">Adapun hasil verifikasi dokumen persyaratan administratif, teknis, dan kelayakan tersebut adalah sebagai berikut:
<ol>
<li style="padding-bottom: 6px;"> Hasil verifikasi dokumen persyaratan administratif permohonan izin pendirian madrasah dinyatakan (TELAH/<s>BELUM</s>*) memenuhi semua persyaratan yang ditetapkan;</li>
<li style="padding-bottom: 6px;"> Hasil verifikasi dokumen persyaratan teknis permohonan izin pendirian madrasah dinyatakan (TELAH/<s>BELUM</s>*) memenuhi semua persyaratan yang ditetapkan;</li>
<li style="padding-bottom: 6px;"> Hasil verifikasi dokumen persyaratan kelayakan permohonan izin pendirian madrasah dinyatakan (TELAH/<s>BELUM</s>*) memenuhi semua persyaratan yang ditetapkan;</li>
<li style="padding-bottom: 6px;">Hasil lengkap verifikasi dokumen persyaratan administratif, teknis, dan kelayakan permohonan izin pendirian madrasah sebagaimana tercantum dalam Lampiran Berita Acara ini.
</li></ol>
</div>
<div style="page-break-before: always;"></div>
<div id="bodinya">

Demikian Berita Acara ini dibuat dengan sebenarnya agar dapat dipergunakan sebagai pertimbangan untuk dilakukan verifikasi lapangan sebagai salah satu syarat pemberian izin pendirian madrasah oleh pejabat yang berwenang.</p>
Tim Verifikasi,
<table style="width: 152mm;">
<tr>
<td width="40%" style="padding: 10px">1.&nbsp;<?php echo $surat->kasi_nama; ?></td>
<td width="35%" style="padding: 10px">(<?php echo $kantor->kasi_jabatan; ?>)</td>
<td style="padding: 10px">..............................</td>
</tr>
<tr>
<td width="40%" style="padding: 10px">2.&nbsp;<?php echo $this->Reff->get_kondisi(array("id"=>$surat->pengawas_id),"tm_pegawai","nama") ?></td>
<td width="35%" style="padding: 10px">(Pengawas Madrasah)</td>
<td style="padding: 10px">..............................</td>
</tr>
<tr>
<td width="40%" style="padding: 10px">3.&nbsp;<?php echo $this->Reff->get_kondisi(array("id"=>$surat->staff_id),"tm_pegawai","nama") ?></td>
<td width="35%" style="padding: 10px">(Pelaksana)</td>
<td style="padding: 10px">..............................</td>
</tr>
</table>

</div>
<div style="page-break-before: always;"></div>
<div id="bodinya">

<p>LAMPIRAN BERITA ACARA<br /> VERIFIKASI DOKUMEN PERSYARATAN ADMINISTRATIF, TEKNIS,<br /> DAN KELAYAKAN PERMOHONAN IZIN PENDIRIAN MADRASAH</p>
<p></p>
<p align="center">HASIL VERIFIKASI DOKUMEN<br />PERSYARATAN ADMINISTRATIF, TEKNIS, DAN KELAYAKAN</p>
<p></p>
<table style="width: 152mm;">
<tr valign="top">
<td style="width: 45mm;">Nama Calon Madrasah</td><td style="width: 5mm;">:</td>
<td style="width: 102mm;"><?php echo $this->Reff->jenjangsingkat($madrasah->jenjang).' '.$madrasah->nama; ?></td>
</tr>
<tr valign="top">
<td style="width: 45mm;">Alamat Calon Madrasah</td><td style="width: 5mm;">:</td>
<td style="width: 102mm;"><?php echo ($madrasah->alamat); ?>,&nbsp;Desa/Kelurahan&nbsp;
            <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->desa_id),"desa","nama")));  ?> Kecamatan
            <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->kecamatan_id),"kecamatan","nama")));  ?>&nbsp;
            <?php echo ucwords(strtolower((preg_match('/^KOTA /',$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))?'':'KABUPATEN ').$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))); ?>&nbsp;<?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->provinsi_id),"provinsi","nama"))); ?>
       </tr>
<tr valign="top">
<td style="width: 45mm;">Nama Penyelenggara</td><td style="width: 5mm;">:</td>
<td style="width: 102mm;"><?php echo $yayasan->nama; ?></td>
</tr>
<tr valign="top">
<td style="width: 45mm;">Alamat Penyelenggara</td><td style="width: 5mm;">:</td>
<td style="width: 102mm;"><?php echo $yayasan->alamat; ?>,&nbsp;
<?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$yayasan->desa_id),"desa","nama")));  ?> Kecamatan
<?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$yayasan->kecamatan_id),"kecamatan","nama")));  ?>&nbsp;
<?php echo ucwords(strtolower(preg_match('/^KOTA /',$this->Reff->get_kondisi(array("id"=>$yayasan->kota_id),"kota","nama"))?'':'KABUPATEN ').$this->Reff->get_kondisi(array("id"=>$yayasan->kota_id),"kota","nama")); ?> <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$yayasan->provinsi_id),"provinsi","nama"))); ?></tr>
<tr valign="top">
<td style="width: 45mm;">Akte Notaris</td><td style="width: 5mm;">:</td>
<td style="width: 102mm;">No. <?php echo $yayasan->notaris_no.' '.$yayasan->notaris_nama.' Tanggal '.$this->Reff->formattanggalstring($yayasan->notaris_tgl); ?></td>
</tr>
<tr valign="top">
<td style="width: 45mm;">Pengesahan Akte Notaris</td><td style="width: 5mm;">:</td>
<td style="width: 102mm;"><?php 
$skumhamno = trim($yayasan->menkumham_sk);
$skumhamtgl = $yayasan->menkumham_tgl;

echo $skumhamno.' Tanggal '.$this->Reff->formattanggalstring($skumhamtgl); ?></td>
</tr>
</table>
<p></p>
<p><b>A. Persyaratan Administratif</b></p>
<table class="tabel">
<tr>
<th style="width: 8mm;">No</th>
<th  style="width: 82mm;">Persyaratan/Dokumen</th>

<th style="width: 30mm;">Keterangan</th>
</tr>

<?php 
					  $p1 = $this->db->get_where("tm_bva",array("type"=>1))->result();
					  $no=1;
					    foreach($p1 as $r){
					  ?>
						<tr>
						<td class="no"><?php echo $no++;?></td>
						<td class="persyaratan"><?php echo $r->nama; ?></td>
						
						<td class="keterangan"></td>
						</tr>
						<?php } ?>

</table>



<p><b>B. Persyaratan Teknis</b></p>
<table class="tabel">
<tr>
<th  style="width: 8mm;">No</th>
<th  style="width: 82mm;">Persyaratan/Dokumen</th>

<th  style="width: 30mm;">Keterangan</th>
</tr>


<?php 
					  $p1 = $this->db->get_where("tm_bva",array("type"=>2))->result();
					  $no=1;
					    foreach($p1 as $r){
					  ?>
						<tr>
						<td class="no"><?php echo $no++;?></td>
						<td class="persyaratan"><?php echo $r->nama; ?></td>
						
						<td class="keterangan"></td>
						</tr>
						<?php } ?>

</table>

<br>
<p>Tim Verifikasi,</p>
<table style="width: 152mm;">
<tr>
<td width="40%" style="padding: 10px">1.&nbsp;<?php echo $surat->kasi_nama; ?></td>
<td width="35%" style="padding: 10px">(<?php echo $kantor->kasi_jabatan; ?>)</td>
<td style="padding: 10px">..............................</td>
</tr>
<tr>
<td width="40%" style="padding: 10px">2.&nbsp;<?php echo $this->Reff->get_kondisi(array("id"=>$surat->pengawas_id),"tm_pegawai","nama") ?></td>
<td width="35%" style="padding: 10px">(Pengawas Madrasah)</td>
<td style="padding: 10px">..............................</td>
</tr>
<tr>
<td width="40%" style="padding: 10px">3.&nbsp;<?php echo $this->Reff->get_kondisi(array("id"=>$surat->staff_id),"tm_pegawai","nama") ?></td>
<td width="35%" style="padding: 10px">(Pelaksana)</td>
<td style="padding: 10px">..............................</td>
</tr>
</table>
</div>
